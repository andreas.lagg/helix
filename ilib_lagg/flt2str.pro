;convert float to string with leading zeros
function flt2str,flt,format=format
  
  if n_elements(format) ne 1 then format='(f10.2)'
  if strupcase(strmid(format,1,1)) ne 'F' then $
    message,'Only works for F format descriptor'
  fsplit=strsplit(format,'.',/extract)
  flen=fix(strmid(fsplit(1),0,strlen(fsplit(1)-2)))
  ilen=fix(strmid(fsplit(0),2,strlen(fsplit(0))))-flen-1
  istr=strtrim(string(long(flt),format='(i'+n2s(ilen)+'.'+n2s(ilen)+')'))
  fstr=strmid(string(flt-long(flt),format=format),fix(ilen),fix(flen)+1)
  
  return,istr+fstr
  
end
