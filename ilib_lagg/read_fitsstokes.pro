;reads in stokes profiles from inversion
;assumes core filename and _prof.fits, _obs.fits and _lam.fits
;returns structure array containing profile(s)
pro read_fitsstokes,fitsfile,dataout,x=x,y=y,fitstype=fitstype, $
                    error=error,full=full,average=average,pixelrep=pixelrep, $
                    reread=reread,iquv=iquv
  @read_fitsstokes_common.pro
  common fits,fits
  common bfitidx,bfitidx
  
  storevar=['dfits0','dfits1','dfits2','maxtype']
  nstore=1                
;  tc_check=1b
    tc_check=0b
  
  if n_elements(oldcnt) eq 0 then oldcnt=0
  dataout=0
  error=1
  if n_elements(fitstype) eq 0 then fitstype=0
  if n_elements(pixelrep) ne 1 then pixelrep=0
  reread=keyword_set(reread)
  if n_elements(oldfile) eq 0 then reread=1 
  if reread eq 0 then begin
    i=-1
    repeat begin
      i=i+1
     reread=min(oldfile(i) ne fitsfile or pixrep(i) ne pixelrep)
    endrep until reread eq 0 or i ge nstore-1
    if reread eq 0 then begin
      is=n2s(i)
      if oldis ne is then begin
        storevar_add=is
        ; for ii=0,n_elements(storevar)-1 do begin
        ;   dummy=execute('if n_elements('+storevar(ii)+is+') ge 1 then '+ $
        ;                 storevar(ii)+'='+storevar(ii)+is)
        ; endfor
      endif
      pixelrep=pixrep(i)
      oldis=is
    endif else oldcnt=(oldcnt+1) mod nstore
  endif

  if n_elements(maxtype) ne 0 then begin
    if maxtype lt fitstype then return
  endif else  maxtype=2
  
  reread=[reread,n_elements(dfits1) eq 0 or reread, $
          n_elements(dfits2) eq 0 or reread]
  if max(reread) eq 1 then begin
    
    code='spinor'
    proffile=get_spinorfitsname(fitsfile,/synth,/silent)
;    if proffile(0) eq '' then begin
    if fits.code eq 'helix' then begin
      proffile=get_helixfitsname(fitsfile,/synth)
      obsfile=''
      code='helix'
      maxtype=1
    endif else begin
      obsfile=get_spinorfitsname(fitsfile,/obs)
      lamfile=get_spinorfitsname(fitsfile,/lamobs)
    endelse

                                ;treat files .1., .2., ...  assume
                                ;that obs, obs_lam and prof have same
                                ;number of files
    for ityp=0,maxtype do if reread(ityp) eq 1 then begin
      dummy=execute('if n_elements(dfits'+n2s(ityp)+') ne 0 then ' + $
                    'dummy=temporary(dfits'+n2s(ityp)+')')
      error2=1
      if ityp eq 0 then tfile=obsfile else tfile=proffile
      case code of
        'spinor': begin
          if tc_check then timecheck,'00',/init          
          for ii=0,n_elements(tfile)-1 do if tfile(ii) ne '' then begin 
            maxtype=1
            error2=0
            case ityp of
              0: begin
;                obs_lam=readfits(lamfile(ii),lam_hdr,/silent)
                print,'Reading wavelength file: ',lamfile(ii)
                obs_lam=read_fits_dlm(lamfile(ii),0,lam_hdr)
                owlref=double(sxpar(lam_hdr,'WLREF'))
                if owlref eq 0 then begin
                  print,'No WLREF found!'
                  print,'   Wavelength file '+lamfile(ii)
                  owlref=add_fitsheader(lamfile(ii),'WLREF')
                endif
                obs_lam=obs_lam+owlref            
                print,'Reading observed profiles: ',obsfile(ii)
;                data=readfits(obsfile(ii),hdr,/silent)
                data=read_fits_dlm(obsfile(ii),0,hdr)
                ;; wlfits=read_fits_dlm(obsfile(ii),1,wlhdr,status=estat)
                ;; if estat eq 0 then begin
                ;;   obs_lam=wlfits
                ;;   owlref=double(sxpar(wlhdr,'WLOFF'))
                ;; endif
;                fits_read,obsfile(ii),data,hdr
              end
              1: begin
                print,'Reading fitted (synthetic) profiles: ',proffile(ii)
;                data=readfits(proffile(ii),hdr,/silent) 
                data=read_fits_dlm(proffile(ii),0,hdr)
;                fits_read,proffile(ii),data,hdr
              end
              2: begin
;                data=mrdfits(proffile(ii),1,hdr,status=status_deconv,/silent)
                print,'Reading deconvolved profiles: ',proffile(ii)
;                data=readfits(proffile(ii),exten_no=1,hdr,/silent) 
;                status_deconv=!ERROR_STATE.CODE
                data=read_fits_dlm(proffile(ii),1,hdr,status=status_deconv)
                if status_deconv ne 0 then begin
                  print,'Cannot find deconvolved data in '+proffile(ii)
                  error2=1
                endif else maxtype=2
              end
            endcase
            if tc_check then timecheck,'01'
            if error2 eq 0 then begin
              nax=sxpar(hdr,'NAXIS*')
              if size(data0,/type) eq 2 or $
                size(obs_lam,/type) eq 2 then begin
                message,/cont, $
                  'Cannot find FITS files containing Stokes parameters'
                reset
              endif
              
              if n_elements(data) ge 2 then begin
                szd=size(data)
                nw=szd(1)
                
                case ityp of 
                  0: begin
                    prof_lam=obs_lam
                    wlref=owlref
                  end
                  else: begin
                    pwlref=double(sxpar(hdr,'WLREF'))
                    if pwlref eq 0 then $
                      pwlref=add_fitsheader(proffile(ii),'WLREF')
                    pwlmin=double(sxpar(hdr,'WLMIN'))
                    if pwlmin eq 0 then $
                      pwlmin=add_fitsheader(proffile(ii),'WLMIN')
                    pwlmax=double(sxpar(hdr,'WLMAX'))
                    if pwlmax eq 0 then $
                      pwlmax=add_fitsheader(proffile(ii),'WLMAX')
                    pnwl=fix(sxpar(hdr,'NWL'))
                    if (pnwl ne 0 and pnwl ne nw and $
                        (min(szd([1,3,4]) eq $
                             (size(obs_lam))([1,2,3])) eq 1)) then begin
                      message,/cont,tfile(ii)+ $
                              ' has same number of WL-points as' + $
                              ' observation. Using observed wavelength.'
                      prof_lam=obs_lam
                      wlref=owlref
                    endif else begin
                      
                      prof_lam=dblarr(szd(1),szd(3),szd(4))
                      plvec=dindgen(szd(1))/(szd(1)-1)*(pwlmax-pwlmin)+ $
                            pwlmin+pwlref
                      if max(plvec-pwlref) le 1e-5 then plvec=dindgen(szd(1))
                      plarr=((intarr(szd(3))+1) ## plvec)
                      for iy=0,szd(4)-1 do prof_lam(*,*,iy)=plarr
                      wlref=pwlref
                    endelse
                  end
                endcase
                if tc_check then timecheck,'02'
                if ii eq 0 then begin
                  ispec=szd(1)
                  wlvec=temporary(prof_lam)
                  datap=temporary(data)
                  wlrefvec=wlref
                endif else begin
                  ispec=[ispec,szd(1)]
                  wlvec=[temporary(wlvec),temporary(prof_lam)]
                  datap=[temporary(datap),temporary(data)]
                  wlrefvec=[wlrefvec,wlref]
                endelse
                if tc_check then timecheck,'03'
              endif

;            icont=fltarr(szd(3),szd(4))+1
            ; if (size(datap))(0) eq 4 then $
            ;   icont=get_qscont(datap) $
            ; else $
            icont=reform(max(dim=1,datap(*,0,*,*)))
            endif

            
            nwl=long(total(ispec))
            nspec=n_elements(tfile)
            order=[0,2,3,1]
            if tc_check then timecheck,'04'
          endif
        end
        'helix':begin          
          
          if tfile ne '' then begin
            read_fits4d,tfile,dst,/nodata,/full
            fpix=[0,0,dst.xrg[0],dst.yrg[0]]
            lpix=[dst.naxis[0]-1,dst.naxis[1]-1,dst.xrg[1],dst.yrg[1]]
            datap=read_fits_dlm(tfile,0,hdr,fpix=fpix,lpix=lpix)
            icont=read_fits_dlm(tfile,1,hdr,fpix=fpix[2:3],lpix=lpix[2:3])
            wlvec=read_fits_dlm(tfile,2,hdr, $
                                fpix=fpix([0,2,3]),lpix=lpix([0,2,3]))
            nax=dst.naxis
;         datap=readfits(proffile,hdr,/silent)
;         icont=readfits(proffile,hdr,/silent,exten=1) 
;         wlvec=readfits(proffile,hdr,/silent,exten=2) 
            nwl=(size(wlvec))(1)
            if pixelrep ge nax[0]/nwl then begin
              print,'Pixelrep out of range. MAX(PIXELREP)=',nax[0]/nwl
              pxrep=nax[0]/nwl-1
            endif else pxrep=pixelrep>0
                                ;best fit map
            if pixelrep eq -1 and $
              min(size(bfitidx,/dim) eq size(icont,/dim)) eq 1 then begin
              datan=fltarr(nwl,nax[1],nax[2],nax[3])
              iwl=[0,nwl-1]
              for ix=0,nax[2]-1 do for iy=0,nax[3]-1 do begin
                bwl=((iwl+nwl*(long(bfitidx(ix,iy))/ $
                               (nax[2]*nax[3]))) mod nax[0])
                datan(*,*,ix,iy)=datap(bwl(0):bwl(1),*,ix,iy)
              endfor
              datap=temporary(datan)
            endif else begin
              iwl=[0,nwl-1]+pxrep*nwl
              datap=datap(iwl(0):iwl(1),*,*,*)
            endelse
            szd=size(datap)
            wlrefvec=0d         ;dblarr(nax[2],nax[3])
;            for iw=0,nwl-1 do datap(iw,0,*,*)=datap(iw,0,*,*)/icont
            nspec=1 & ispec=nwl
            order=dst.order
            error2=0
          endif else error2=1
        end
      endcase
                                ;define datap as pointer
      if error2 eq 0 then begin
        datap=ptr_new(datap,/no_copy)
        if tc_check then timecheck,'05'
        dfits={nw:nwl,nx:nax[2],ny:nax[3],wlref:wlrefvec,order:order, $
               icont:icont,nspec:nspec,ispec:ispec,wl:temporary(wlvec), $
               data:datap}
        if tc_check then timecheck,'06'
        dummy=execute('dfits'+n2s(ityp)+'=temporary(dfits)')
        if tc_check then timecheck,'07'
        if n_elements(oldfile) eq 0 then begin
          oldfile=strarr(nstore>1)
          pixrep=intarr(nstore>1)
        endif
        oldfile(oldcnt)=fitsfile
        pixrep(oldcnt)=pixelrep
        is=n2s(oldcnt)
        if nstore ge 2 then begin
          for ii=0,n_elements(storevar)-1 do begin
                                ;timecheck,'08a'+n2s(ii)
            dummy=execute('if n_elements('+storevar(ii)+') ge 1 then '+ $ $
                          storevar(ii)+is+'='+storevar(ii))
                                ;timecheck,'08b'+n2s(ii)
          endfor
          oldis=is
        endif else oldis=''
      endif
    endif                       ;typ 0,1,2
  endif

  if tc_check then timecheck,'09'
  dummy=execute('nel=n_elements(dfits'+n2s(fitstype)+')')
  if nel ne 0 then begin
    dfstr='dfits'+n2s(fitstype)
    
;    dummy=execute('dfits=dfits'+n2s(fitstype))
;    order=dfits.order
    dummy=execute('order='+dfstr+'.order')
    if keyword_set(full) then begin ;return full map
                                ; dataout={wl:dfits.wl,i:reform(dfits.data(*,order(0),*,*)), $
                                ;          q:reform(dfits.data(*,order(1),*,*)), $
                                ;          u:reform(dfits.data(*,order(2),*,*)), $
                                ;          v:reform(dfits.data(*,order(3),*,*)), $
                                ;          icont:dfits.icont, $
                                ;          nspec:dfits.nspec,ispec:dfits.ispec,wlref:dfits.wlref}      
;     dummy=execute('dataout={wl:'+dfstr+'.wl,i:reform('+dfstr+'.data(*,order(0),*,*)),q:reform('+dfstr+'.data(*,order(1),*,*)),u:reform('+dfstr+'.data(*,order(2),*,*)),v:reform('+dfstr+'.data(*,order(3),*,*)),icont:'+dfstr+'.icont,nspec:'+dfstr+'.nspec,ispec:'+dfstr+'.ispec,wlref:'+dfstr+'.wlref}')
;      dummy=execute('dataout={wl:'+dfstr+'.wl,i:ptr_new(reform('+dfstr+'.data(*,order(0),*,*))),q:ptr_new(reform('+dfstr+'.data(*,order(1),*,*))),u:ptr_new(reform('+dfstr+'.data(*,order(2),*,*))),v:ptr_new(reform('+dfstr+'.data(*,order(3),*,*))),icont:'+dfstr+'.icont,nspec:'+dfstr+'.nspec,ispec:'+dfstr+'.ispec,wlref:'+dfstr+'.wlref}')
      dummy=execute('dataout=dfits'+n2s(fitstype))
      error=0  
                                ;timecheck,'10a'
    endif else begin            ;return x/y position
      if n_elements(x) eq 0 then dummy=execute('x=indgen('+dfstr+'.nx)')
      if n_elements(y) eq 0 then dummy=execute('y=indgen('+dfstr+'.ny)')
      dummy=execute('dnx='+dfstr+'.nx')
      dummy=execute('dny='+dfstr+'.ny')
      if min(x) ge 0 and max(x) le dnx-1 and $
        min(y) ge 0 and max(y) le dny-1 then begin
        dummy=execute('wlarr='+dfstr+'.wl')
        wl2d=n_elements(size(wlarr,/dim)) ge 2
        if wl2d then wl=reform(wlarr(*,min(x):max(x),min(y):max(y))) else wl=wlarr
;        dummy=execute('wl=reform('+dfstr+'.wl(*,min(x):max(x),min(y):max(y)))')
        dummy=execute('iprof=reform((*'+dfstr+'.data)(*,order(0),min(x):max(x),min(y):max(y)))')
        dummy=execute('qprof=reform((*'+dfstr+'.data)(*,order(1),min(x):max(x),min(y):max(y)))')
        dummy=execute('uprof=reform((*'+dfstr+'.data)(*,order(2),min(x):max(x),min(y):max(y)))')
        dummy=execute('vprof=reform((*'+dfstr+'.data)(*,order(3),min(x):max(x),min(y):max(y)))')
        ; dummy=execute('iprof=reform('+dfstr+'.i(*,min(x):max(x),min(y):max(y)))')
        ; dummy=execute('qprof=reform('+dfstr+'.q(*,min(x):max(x),min(y):max(y)))')
        ; dummy=execute('uprof=reform('+dfstr+'.u(*,min(x):max(x),min(y):max(y)))')
        ; dummy=execute('vprof=reform('+dfstr+'.v(*,min(x):max(x),min(y):max(y)))')
        dummy=execute('icont='+dfstr+'.icont(min(x):max(x),min(y):max(y))')
        if keyword_set(average) then begin
          nxy=(max(x)-min(x)+1)*(max(y)-min(y)+1)
          sz=size(iprof)
          if sz(0) eq 3 then begin
            if wl2d then wl=total(total(wl,2),2)/nxy
            iprof=total(total(iprof,2),2)/nxy
            qprof=total(total(qprof,2),2)/nxy
            uprof=total(total(uprof,2),2)/nxy
            vprof=total(total(vprof,2),2)/nxy
          endif else if sz(0) eq 2 then begin
            if wl2d then wl=total(wl,2)/nxy
            iprof=total(iprof,2)/nxy
            qprof=total(qprof,2)/nxy
            uprof=total(uprof,2)/nxy
            vprof=total(vprof,2)/nxy
          endif
          icont=mean(icont)
        endif
;          dnew=ptr_new([[iprof],[qprof],[uprof],[vprof]])
          dnew=ptr_new([[temporary(iprof)],[temporary(qprof)], $
                        [temporary(uprof)],[temporary(vprof)]])
          order=[0,1,2,3]
          dummy=execute('dataout={wl:wl,data:dnew,order:order,icont:icont,nspec:'+dfstr+'.nspec,ispec:'+dfstr+'.ispec,wlref:'+dfstr+'.wlref}') 
        error=0  
      endif 
      
                                ;timecheck,'10b'
    endelse
    if tc_check then timecheck,'11'
    if n_tags(dataout) eq 0 then error=1b
  if error eq 0 then begin
    if keyword_set(iquv) then begin
      order=[0,1,2,3]
      dataout={wl:dataout.wl,icont:dataout.icont,order:order, $
               i:reform((*dataout.data)(*,dataout.order(0),*,*)), $
               q:reform((*dataout.data)(*,dataout.order(1),*,*)), $
               u:reform((*dataout.data)(*,dataout.order(2),*,*)), $
               v:reform((*dataout.data)(*,dataout.order(3),*,*)), $
               nspec:dataout.nspec,ispec:dataout.ispec,wlref:dataout.wlref}
    endif
  endif else begin
    dataout={error:1b}
  endelse
  if tc_check then timecheck,'12'
  endif
end
