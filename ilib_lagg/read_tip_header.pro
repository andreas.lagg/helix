function read_tip_header,obs,verbose=verbose,force=force
  common reduce,reduce
  
  if n_elements(verbose) eq 0 then verbose=1

  hst={error:1}
  istip=check_tipmask(obs,verbose=verbose,obscore=obscore)
;  if istip eq 0 then return,hst
  if istip eq 0 and keyword_set(force) eq 0 then begin
    if verbose ge 1 then $
      message,/cont,'Warning: file does not match TIP mask: eg. ddmmmyy.000-01'
    return,hst
  endif
  
  if strpos(obs,'.sav') ne -1 then begin
    return,hst
  endif

                                ;get path of obs
  opath=strmid(obs,0,strpos(obs,'/',/reverse_search))
  if opath ne '' then opath=opath+'/'
  
  pathadd=''
;  spawn,'uname -n',host
;  if host eq 'lxlagg' or host eq 'vttmps' then $
;    pathadd=['/media/LACIE/*','/media/LACIE/*/*', $
;             '/media/LACIE/*/*/*']
  
  year=strmid(obscore,5,2)
  
  path=[opath,'','./data/','./data/*'+year,pathadd] ;, $
;         '/data/slam/VTT-data/VTT-*'+year+'/*', $
;         '/data/slam/VTT-data/VTT-*'+year+'/*'+year+'/*', $
;         '/data/slam/VTT-data/VTT-*'+year+'/*'+year+'/*'+year+'/*', $
;         '/data/slam/VTT-data/data*'+year+'/*', $
;         '/data/slam/VTT-data/data*'+year+'/*'+year+'/*', $
;         '/data/slam/VTT-data/data*'+year+'/*'+year+'/*'+year+'/*' $
;        ]
  path=remove_multi(path,/no_empty)
  add=''
  
  if strmid(obs,strlen(obs)-2,2) eq 'cc' then $
    add=['cc','-??cc','','-??'] $
  else add=['','-??','cc','-??cc']
  ir=0
  repeat begin
    retfile=find_file_inpath(path=path,file=obscore+add(ir), $
                             error=error,verbose=0)
;    unit=file_open(obscore+add,path=path,error=error,verbose=0,/close, $
;                   retfile=retfile)
    ir=ir+1
  endrep until ir ge 4 or error eq 0
  
  if error ne 0 then begin
    if verbose ge 1 then begin
      message,/cont,'Could not find observation '+obscore
      message,/cont,'in directories '+add_comma(path,sep=':')
    endif
    return,hst
  endif
  
  retfile=(file_search(retfile))(0)
  
  if verbose ge 1 then $
    print,'Reading TIP header of '+retfile

  dummy=readfits(retfile,header,nslice=0,silent=1)
  header1=header
  
                                ;convert strings to structure
  header=replace_header(header)
  header2=header

  hst=fits_header2st(header)

                                ;correct r0 (from 2008 the r0 is
                                ;stored in 1/100 of arcsec)
  tn=strupcase(tag_names(hst))
  if max(tn eq 'R0RADIUS') then begin 
    if hst.r0radius[0] ge 9e4 and hst.r0radius[0] le 1e5 then $
      hst.r0radius=hst.r0radius/100.
  endif
  
  return,hst
end
