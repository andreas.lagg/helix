pro vapor_create,fnr=fnr,savfile=savfile
  
  if savfile eq '' then savfile='filament.sav'
  restore,savfile
  f=get_filepath(savfile)
  
                                ;make average filament
  favg=filament[0]
  nf=n_elements(filament)
  favg.map=total(filament.map,4)/nf
  favg.taumap=total(filament.taumap,5)/nf
  
  if n_elements(fnr) eq 0 then begin
    ff=favg
    add='_avg'
  endif else begin
    ff=filament[fnr]
    add='_'+string(fnr,format='(i3.3)')
  endelse
  filament=ff
  fsav=strmid(f.name,0,strpos(f.name,'.',/reverse_search))+add+'.sav'
  save,/xdr,/compress,filament,file=fsav
  print,'Wrote filament: '+add
  print,'File: ',fsav
  
                                ;check if vapor is installed
  vapordir='/opt/vapor'
  fi=file_search(vapordir+'/bin/vapor-setup.sh',count=cnt)
  if cnt eq 0 then begin
    print,'Vapor not installed in '+vapordir
    print,'Exit.'
    return
  endif
    
  
  
  sz=size(ff.taumap)
  
  
  dim=n2s(sz(1))+'x'+n2s(sz(2))+'x'+n2s(sz(3))
  vname=strlowcase(ff.taupar)
;  vname='var'+n2s(indgen(sz(4))+1)
  par=add_comma(vname,sep=':')
  
  cmd='. '+vapordir+'/bin/vapor-setup.sh '
  vdf=strmid(f.name,0,strpos(f.name,'.',/reverse_search))+add+'.vdf'
  cmd=[cmd,'vdfcreate -dimension '+dim+' -vars3d '+par+' '+vdf]
;  spawn,vps+cmd
  print,cmd
  
  vdir='./vapordata'
  spawn,'mkdir -p '+vdir
  for ip=0,sz(4)-1 do begin
    fdat=vdir+'/data'+string(ip,format='(i3.3)')+'.float'
    openw,unit,/get_lun,fdat
    writeu,unit,reform(ff.taumap[*,*,*,ip])
    free_lun,unit
    
    cmd=[cmd,'raw2vdf -varname '+vname[ip]+' '+vdf+' '+fdat]
;    spawn,vps+cmd
;    spawn,'ls -l '+vdf
;    print,cmd
  endfor
  cmdall=add_comma(cmd,sep=' ; ')
  spawn,cmdall
  stop
end

