pro fillmaps,init=init
  common fdata,cdat,cplot,spl
  common filadata,fsav,file,atm,prof,obs,maptau,ahdr,phdr,ohdr,mthdr
  common filacommon,filament,fidx
  
  if keyword_set(init) eq 0 then begin
    fill_spline
    spl=filament[fidx].spline
    cdat=filament[fidx].cdat
    cplot=filament[fidx].cplot
  endif
  
  ns=n_elements(spl.xspl)
  cdx=spl.xspl([0,ns-1])-cdat(0).center(0)
  cdy=spl.yspl([0,ns-1])-cdat(0).center(1)
  cdist=sqrt(cdx^2+cdy^2)
  rotate=cdist(0) gt cdist(1)
  
                                ;create maps for xygrid for all parameters
  szg=size(spl.xgrid)
  sza=size(atm)
  map=fltarr(szg(1),spl.nperp*2+1,sza(3) + 10)
  xgrd=spl.xgrid(*,0:spl.nperp*2)
  ygrd=spl.ygrid(*,0:spl.nperp*2)
  if rotate then begin
    xgrd=rotate(xgrd,2)
    ygrd=rotate(ygrd,2)
  endif
  ii=where(strpos(ahdr,'LTTOP') eq 0)
  adesc=ahdr(ii:ii+sza(3)-1)
  azigrid=atan(ygrd-shift(ygrd,1),xgrd-shift(xgrd,1))/!dtor
  azigrid(0,*)=azigrid(1,*)
  for i=0,sza(3)-1 do begin
    imap=atm(*,*,i)
                                ;correct azimuth: subtract Filament direction
    if strpos(adesc(i),'AZIMU') eq 0 then begin
      iimap=imap[xgrd,ygrd]
      if file.ambigresolved eq 1 then $
        dummy=max(histogram((iimap+720) mod 360,binsize=1,loc=loc),iloc) $
      else dummy=max(histogram((iimap+360) mod 180,binsize=1,loc=loc),iloc)
            ;most probable azimuth, just to avoid flipping problem
      probazi=loc(iloc)   
      cmap=interpolate(cos((imap-probazi)*!dtor),xgrd,ygrd)
      smap=interpolate(sin((imap-probazi)*!dtor),xgrd,ygrd)
      map(*,*,i)=atan(smap,cmap)/!dtor+probazi - azigrid
      if file.ambigresolved eq 1 then $
        map(*,*,i)=((map(*,*,i)+540) mod 360)-180 $
      else map(*,*,i)=((map(*,*,i)+450) mod 180)-90
      adesc(i)=strmid(adesc(i),0,5)+'-AZIFIL'
    endif else map(*,*,i)=interpolate(imap,xgrd,ygrd)
  endfor

;   iazi=where(strpos(adesc,'AZIMU') eq 0)
;   if iazi(0) ne -1 then begin
;     for ia=0,n_elements(iazi)-1 do begin
;       ii=iazi(ia)
; ;     map(*,*,ii)=((map(*,*,ii)-azigrid+270) mod 180)-90
;       adesc(ii)=strmid(adesc(ii),0,5)+'-AZIFIL'
;     endfor
; endif
  
  aprof=(total(total(obs(*,0,1:10,1:10),3),3))/10/10
  dummy=min(aprof,imin) 
  wlcont=indgen(11)+2
  wlcore=indgen(11)-5+imin
  wlwing=indgen(11)+imin+2
;  mapobs=fltarr(szg(1),spl.nperp*2+1,5)
  map(*,*,sza(3)+0)=interpolate(total(obs(wlcont,0,*,*),1)/11,xgrd,ygrd)
  map(*,*,sza(3)+1)=interpolate(total(obs(wlcore,0,*,*),1)/11,xgrd,ygrd)
  map(*,*,sza(3)+2)=interpolate(total(obs(wlcore,2,*,*),1)/11,xgrd,ygrd)
  map(*,*,sza(3)+3)=interpolate(total(obs(wlcore,3,*,*),1)/11,xgrd,ygrd)
  map(*,*,sza(3)+4)=interpolate(total(obs(wlwing,1,*,*),1)/11,xgrd,ygrd)
  odesc='Obs:'+['Icont','Icore','Q','U','V']
  
  aprof=(total(total(prof(*,0,1:10,1:10),3),3))/10/10
  dummy=min(aprof,imin) 
  wlcont=indgen(21)+4
  wlcore=indgen(21)-10+imin
  wlwing=indgen(21)+imin+4
;  mapprof=fltarr(szg(1),spl.nperp*2+1,5)
  map(*,*,sza(3)+5+0)=interpolate(total(prof(wlcont,0,*,*),1)/21,xgrd,ygrd)
  map(*,*,sza(3)+5+1)=interpolate(total(prof(wlcore,0,*,*),1)/21,xgrd,ygrd)
  map(*,*,sza(3)+5+2)=interpolate(total(prof(wlcore,2,*,*),1)/21,xgrd,ygrd)
  map(*,*,sza(3)+5+3)=interpolate(total(prof(wlcore,3,*,*),1)/21,xgrd,ygrd)
  map(*,*,sza(3)+5+4)=interpolate(total(prof(wlwing,1,*,*),1)/21,xgrd,ygrd)
  pdesc='Synth:'+['Icont','Icore','Q','U','V']
  
                                ;calculate bx,by,bz
  
  lttop=sxpar(mthdr,'LTTOP')
  ltbot=sxpar(mthdr,'LTBOT')
  ltinc=sxpar(mthdr,'LTINC')
  tau=findgen((ltbot-lttop)/ltinc+1)*ltinc+lttop
  ipar=max(where(strpos(mthdr,'LTINC  ') eq 0))+1
;  epar=max(where(strpos(mthdr,'END    ') eq 0))+1
  epar=n_elements(mthdr) 
  taupar=strtrim(strmid(mthdr(ipar:epar-1),0,8))
  taupar=[taupar,'BX','BY','BZ']
  taumap=fltarr(szg(1),spl.nperp*2+1,n_elements(tau),n_elements(taupar))
  ib=(where(taupar eq 'FMAG'))(0)
  ia=(where(taupar eq 'CHI'))(0)
  ii=(where(taupar eq 'GAMMA'))(0)
  
  
  for it=0,n_elements(tau)-1 do begin
                                ;correct azimuth for filament axis        
    mtazi=maptau[*,*,it,ia]
    iimap=mtazi[xgrd,ygrd]
    if file.ambigresolved eq 1 then $
      dummy=max(histogram((iimap+720) mod 360,binsize=1,loc=loc),iloc) $
    else dummy=max(histogram((iimap+360) mod 180,binsize=1,loc=loc),iloc)
                                ;most probable azimuth, just to avoid
                                ;flipping problem
    probazi=loc(iloc)   
    cmap=interpolate(cos((mtazi-probazi)*!dtor),xgrd,ygrd)
    smap=interpolate(sin((mtazi-probazi)*!dtor),xgrd,ygrd)
    mtazigrd=atan(smap,cmap)/!dtor+probazi - azigrid
    if file.ambigresolved eq 1 then $
      mtazigrd=((mtazigrd+540) mod 360)-180 $
    else mtazigrd=((mtazigrd+450) mod 180)-90
    for ip=0,n_elements(taupar)-1 do begin
      case taupar(ip)  of
        'BX': taumap(*,*,it,ip)= $
          +taumap[*,*,it,ib]*sin(taumap[*,*,it,ii]*!dtor)*cos(mtazigrd*!dtor)
        'BY': taumap(*,*,it,ip)=$
          +taumap[*,*,it,ib]*sin(taumap[*,*,it,ii]*!dtor)*sin(mtazigrd*!dtor)
        'BZ': taumap(*,*,it,ip)= $
          -taumap[*,*,it,ib]*cos(taumap[*,*,it,ii]*!dtor)
        'CHI': taumap(*,*,it,ip)=mtazigrd
        else: taumap(*,*,it,ip)=interpolate(maptau[*,*,it,ip],xgrd,ygrd)
      end
    endfor
  endfor
  filacurr={spline:spl,cdat:cdat,cplot:cplot,file:file, $
            map:map,info:[adesc,odesc,pdesc], $
            tau:tau,taupar:taupar,taumap:taumap}
  if keyword_set(init) then begin
    if n_elements(filament) ne 0 then begin
      nfil=replicate(filacurr,n_elements(filament))
     for i=0,n_elements(nfil)-1 do $
        nfil(i)=update_struct(filament[i],filacurr,/nozero)
      filament=temporary(nfil)
    endif else filament=filacurr
  endif else begin
;    filament[fidx]=update_struct(filament[fidx],filacurr,/nozero)
    filament[fidx]=filacurr
  endelse
                                ; cdat=filament[fidx].cdat
                                ; cplot=filament[fidx].cplot
                                ; spl=filament[fidx].spline
end

pro fill_spline
  common filacommon
                                ;do spline computation
  sz=size(filament[fidx].spline.xgrid)
  nspl=sz(1)
  if filament[fidx].spline.nperp eq 0 then filament[fidx].spline.nperp=10
  if filament[fidx].spline.n ge 3 then begin
    xcs=filament[fidx].spline.xnod(0:filament[fidx].spline.n-1)
    ycs=filament[fidx].spline.ynod(0:filament[fidx].spline.n-1)
    rot=abs((filament[fidx].spline.xnod(filament[fidx].spline.n-1)- $
             filament[fidx].spline.xnod(0))) lt $
      abs((filament[fidx].spline.ynod(filament[fidx].spline.n-1)- $
           filament[fidx].spline.ynod(0)))
    if rot then begin
      ycs=filament[fidx].spline.xnod(0:filament[fidx].spline.n-1) 
      xcs=filament[fidx].spline.ynod(0:filament[fidx].spline.n-1)
    endif
    if xcs(filament[fidx].spline.n-1) lt xcs(0) then begin
      xcs=reverse(xcs)
      ycs=reverse(ycs)
    endif
;    filament[fidx].spline.xspl=findgen(nspl)/(nspl-1)*(max(xcs)-min(xcs))+xcs(0)
    filament[fidx].spline.xspl=findgen(nspl)/(nspl-1)*(xcs(filament[fidx].spline.n-1)-xcs(0))+xcs(0)
    filament[fidx].spline.yspl= $
      spline(xcs,ycs,filament[fidx].spline.xspl)
    if rot then begin
      tcs=filament[fidx].spline.xspl 
      filament[fidx].spline.xspl=filament[fidx].spline.yspl
      filament[fidx].spline.yspl=tcs
    endif
                                ;calculate spline normal at every point
    ddx=filament[fidx].spline.xspl-shift(filament[fidx].spline.xspl,1)
    ddy=filament[fidx].spline.yspl-shift(filament[fidx].spline.yspl,1)
    ddx(0)=ddx(1)
    ddy(0)=ddy(1)
    nvec=[[-ddy],[+ddx]]
    nvec=nvec/(sqrt(total(nvec^2,2)) # (intarr(2)+1) ) ;normalize
    filament[fidx].spline.xgrid=0 & filament[fidx].spline.ygrid=0
    for iw=-filament[fidx].spline.nperp,filament[fidx].spline.nperp do begin
      filament[fidx].spline.xgrid(*,iw+filament[fidx].spline.nperp)= $
        filament[fidx].spline.xspl+iw*nvec(*,0)
      filament[fidx].spline.ygrid(*,iw+filament[fidx].spline.nperp)= $
        filament[fidx].spline.yspl+iw*nvec(*,1)
    endfor
  endif
end

pro print_filament,idx
  common filacommon
  common filadata,fsav,file,atm,prof,obs,maptau,ahdr,phdr,ohdr,mthdr
  
  if n_params() eq 0 then idx=indgen(n_elements(filament))
  
  f=get_filepath(file.sav)
  psname=strmid(f.name,0,strpos(f.name,'.',/reverse_search)+1)+'ps'
  psset,size=[18,26],file=f.path+psname,/no_x,ps=1 ;0,win_nr=2
  @greeklett.pro
    
    for ii=0,n_elements(idx)-1 do begin
      fidx=idx(ii)
      fillmaps
      szm=size(filament[fidx].map)
      
      showmaps,/ps       
      xyouts,0.5,1.,/normal,alignment=0.5,'!CFilament #'+n2s(fidx),charsize=1.5
      erase
      xyouts,0.5,1.,/normal,alignment=0.5,'!CFilament #'+n2s(fidx),charsize=1.5
      
      nx=1 & ny=24
      pidx=intarr(nx,ny)-1
;  pidx(0:2,0:5)=indgen(18)+5
      pidx(0,0:17)=indgen(18)+5
      pidx(0,18:20)=[25,26,29]
      pidx(0,21:23)=[30,31,34]
      
      ptau=strmid(filament[fidx].info(2:4),38,5)
      
      rx=[0.1,0.9,.002]         ;x position (left, right, distance)
      ry=[0.05,0.9,0.002]       ;y position (top, bottom, distance)
      dx=(rx(1)-rx(0)-(nx-1)*rx(2))/nx
      dy=(ry(1)-ry(0)-(ny-1)*ry(2))/ny
      xtn=strarr(32)+' '
      for ix=0,nx-1 do begin
        for iy=0,ny-1 do begin
          ip=pidx(ix,iy)
          if ip ge 0 then begin
            pname=(strsplit(filament[fidx].info(ip),/extract))(0)
            ptau=', log('+f_tau+')='+ $
              strmid(filament[fidx].info(2+((ip-5) mod 3)),38,5)
            px=ix*dx + rx(0) +ix*rx(2) + [0,dx]
            py=ry(1)- (iy*dy +iy*ry(2) + [dy,0])
            rg=minmaxp(filament[fidx].map(*,*,ip),perc=99.9)
            case strupcase(strmid(pname,0,3)) of
              'OBS': begin
                userlct,coltab=0,/full,/reverse 
                ptau=''
              end
              'SYN': begin
                userlct,coltab=0,/full,/reverse 
                ptau=''
              end
              'AZI': begin
                userlct,glltab=6,/full,verbose=0, $
                  center=256.*(0.-rg(0)) / (rg(1)-rg(0))
              end
              'VEL': begin
                userlct,neutral=0,glltab=7,/full,verbose=0, $ ;Velocity
                  center=256.*(0.-rg(0)) / (rg(1)-rg(0))
              end
              else: begin
                userlct,/full
              end
            endcase
            if iy eq ny-1 then dummy=temporary(xtn)
;            print,filament[fidx].spline.nperp,[px(0),py(0),px(1),py(1)]
            image_cont_al,filament[fidx].map(*,*,ip), $
              position=[px(0),py(0),px(1),py(1)], $
              contour=0,/no_label,/exact,/aspect,/cut,zrange=rg,/noerase, $
              xtickname=xtn,retpos=rpos,yrange=[-1,1]*fix(szm(2)/2)
            plots,!x.crange,[0,0],linestyle=1
            xyouts,/device,rpos(0),rpos(3),'!C     '+pname+ptau, $
              charsize=0.6
          endif
        endfor
      endfor
    endfor
    
    psset,/close
    
  end

pro showmaps,ps=ps,show=show,press=press
  common filacommon
;  common fdata,cdat,cplot,spl
  common filadata,fsav,file,atm,prof,obs,maptau,ahdr,phdr,ohdr,mthdr
  
;display all maps
  
  print,'Filament #'+n2s(fidx)
  if n_elements(show) eq 0 then show=indgen(n_elements(filament[0].cdat))
  nimg=n_elements(show)
  lpauto=filament[fidx].cplot.landscape
  if keyword_set(ps) eq 0 then begin
    set_plot,'X'
    xysize=[600,1000]
    if lpauto eq 1 then xysize=reverse(xysize)
    if !d.window ne 0 then window,0,xsize=xysize(0),ysize=xysize(1)
    erase
  endif
  !p.position=0
  npx=1 & npy=nimg
  if lpauto eq 1 then begin
    npy=1 & npx=nimg
  endif
  if lpauto eq 2 then begin   ;find optimum nx, ny
                                ;for given window size and aspect ration
    area=fltarr(nimg)
    for ny=1,nimg do begin
      nx=ceil(float(nimg)/ny)
      nrat=nimg/float(nx*ny)
      px=!d.x_size/float(nx)
      py=!d.y_size/float(ny)
      dxp=px
      dyp=float(filament[fidx].cplot.ny)/(filament[fidx].cplot.nx*1.4)*px
      if dyp gt py then begin
        dyp=py
        dxp=float(filament[fidx].cplot.nx*1.4)/filament[fidx].cplot.ny*py
      endif
      area(ny-1)=dxp*dyp*nrat
    endfor
    dummy=max(area,imax)
    npy=imax+1
;    npy=1>(fix(ratio))<nimg
    npx=ceil(float(nimg)/npy)  
  endif
  !p.multi=[0,npx,npy]
  if keyword_set(press) then begin
    ymrg=!y.margin & !y.margin=[1,3]
    xmrg=!x.margin & !x.margin=[1,1]
  endif
  
  valid=bytarr(filament[fidx].cplot.nx,filament[fidx].cplot.ny)
  idx=polyfillv(filament[fidx].cplot.xbox(0:filament[fidx].cplot.np-1)- $
                filament[fidx].cplot.dx(0), $
                filament[fidx].cplot.ybox(0:filament[fidx].cplot.np-1)- $
                filament[fidx].cplot.dy(0),filament[fidx].cplot.nx, $
                filament[fidx].cplot.ny)
  valid(idx)=1b
  
  
  dx=filament[fidx].cplot.dx
  dy=filament[fidx].cplot.dy
  for is=0,n_elements(show)-1 do begin
    i=show(is)
    cdi=filament[fidx].cdat(i)
    case cdi.typ of
      0: img=atm(dx(0):dx(1),dy(0):dy(1),cdi.par)
      1: img=reform(total(obs(cdi.wlidx(0):cdi.wlidx(1), $
                              cdi.par,dx(0):dx(1),dy(0):dy(1)),1)/ $
                    (cdi.wlidx(1)-cdi.wlidx(0)+1))
      2: img=reform(total(prof(cdi.wlidx(0):cdi.wlidx(1), $
                               cdi.par,dx(0):dx(1),dy(0):dy(1)),1)/ $
                    (cdi.wlidx(1)-cdi.wlidx(0)+1))
    endcase
    iimg=img
    if strpos(cdi.name,'AZI') ne -1 then begin
      if file.ambigresolved eq 1 then $
        iimg=((iimg+540) mod 360)-180 $
      else iimg=((iimg+270) mod 180)-90
    endif
    iimg=iimg/valid
    rg=minmaxp(iimg,perc=99.9)
    gc=!p.background
    case cdi.coltab of
      0: userlct,coltab=0,/full,/reverse               ;BW
      1: begin
        userlct,neutral=0,glltab=7,/full,verbose=0, $ ;Velocity
          center=256.*(0.-rg(0)) / (rg(1)-rg(0))
        gc=!p.color
      end
      2: begin
        userlct,glltab=8,/full,verbose=0 ;AZI
        if file.ambigresolved eq 1 then $
          rg=[-180,180] else rg=[-90,90]
       
      end
      else: userlct,/full                 ;normal
    endcase
    if n_elements(xtn) ne 0 then dummy=temporary(xtn)
    if n_elements(ytn) ne 0 then dummy=temporary(ytn)
    if keyword_set(press) then begin
      xtn=strarr(32)+' '
      ytn=strarr(32)+' '
    endif
    image_cont_al,/aspect,/cut,iimg,zrange=rg, $
      xrange=filament[fidx].cplot.dx,yrange=filament[fidx].cplot.dy, $
      title=cdi.name,retpos=rp,contour=0,xtickname=xtn,ytickname=ytn
    if filament[fidx].cplot.showspline eq 1 then $
      if filament[fidx].spline.n ge 3 then begin
      oplot,filament[fidx].spline.xspl,filament[fidx].spline.yspl,linestyle=0
    endif
    if filament[fidx].cplot.showgrid eq 1 then $
      if filament[fidx].spline.n ge 3 then begin
      gdens=10 ;grid density
      grd1=[lindgen(n_elements(filament[fidx].spline.xspl)/gdens)*gdens, $
            n_elements(filament[fidx].spline.xspl)-1]
      grd2=lindgen(filament[fidx].spline.nperp*2+1)
      grdy=grd2 ## (grd1*0+1)
      grdx=(grd2*0+1) ## grd1
      
      oplot,filament[fidx].spline.xgrid(grdx,grdy), $
        filament[fidx].spline.ygrid(grdx,grdy), $
        psym=([3,6])(keyword_set(press)),symsize=0.05, $
        color=gc
    endif
    if filament[fidx].cplot.shownode eq 1 then $
      if filament[fidx].spline.n ge 1 then begin
      oplot,filament[fidx].spline.xnod(0:filament[fidx].spline.n-1), $
        filament[fidx].spline.ynod(0:filament[fidx].spline.n-1),psym=4
    endif
    
    
    filament[fidx].cdat(i).pos=rp
  endfor
  info=['Left Mouse: Add spline point,     Right Mouse: Delete spline point', $
        'Keys:' + $
        ' (s): save filament, (r) refresh display, (p) print filament,' + $
        ' (l) toggle portrait/landscape/auto-grid,'+ $      
        ' (w) enter grid size perp to spline, (x): exit ', $
        ' (f) enter filament number, (+)/(-) next/previous filament,'+ $
        ' (d) delete filament, (o) PDF-print VELOS,INC,TEMP map,'+ $
        ' (m) manual entry of filemant nodes']
  if keyword_set(ps) eq 0 then $
    for i=0,n_elements(info)-1 do $
    xyouts,/normal,0,1,strjoin(replicate('!C',i+1))+info(i),charsize=1.2
  
end

pro save_filament
  common filadata,fsav,file,atm,prof,obs,maptau,ahdr,phdr,ohdr,mthdr
;  common fdata,cdat,cplot,spl
  common filacommon
  
  if n_elements(filament[fidx]) eq 0 then return
  
  if filament[fidx].spline.n ge 3 then begin
    fillmaps
    print
    save,/compress,/xdr,filament,file=fsav
    print,fsav+' now contains ',n_elements(filament),' filament(s).'
  endif
end

pro paperplot
  common filacommon,filament,fidx
  ls=filament[fidx].cplot.landscape
  filament[fidx].cplot.landscape=1
  f=get_filepath(filament[fidx].file.sav)
  psset,size=[15,6],file='Fig2.eps',/no_x,/ps,/encapsulated ;0,win_nr=2
  showmaps,/ps,show=[9,7,5],/press
  psset,/close
  filament[fidx].cplot.landscape=ls
end
        
pro fila,reread=reread,savfile=savfile,xbox=xbox,ybox=ybox,fidx=fidxin, $
         center=center,dir=dirin
  common filadata,fsav,file,atm,prof,obs,maptau,ahdr,phdr,ohdr,mthdr
  common fdata,cdat,cplot,spl
  common filacommon,filament,fidx
  
; filament list of sanjiv:
; 160:160+140,354:354+10        ; inner penumbra (horizontal)
; 382:382+12,530:530+90         ;middle penumbra (vertical)
; 492:492+11,130:130+60         ;outer penumbra (vertical)
; here are some more, all of these are inclined:
; x1 y1:  (303,473)      ;  (262,398); (263,587); (503,276); (540,556)
; x2 y2:  (273,537)      ;  (169,426); (228,637); (531,234); (573,616)
  
  if n_elements(savfile) eq 0 then begin
    savfile='filament.sav'
  endif
  fsav=savfile
  
  newdata=1
  if n_elements(fidxin) eq 0 then $
    fidx=(n_elements(filament)-1)>0 $
  else fidx=fidxin<((n_elements(filament)-1)>0)
  fi=file_info(fsav)
  filest={dir:'',atm:'',obs:'',prof:'',maptau:'',sav:fsav,ambigresolved:0b}
  if fi.exists then begin
    print,'Restoring '+fsav
    restore,fsav
    fidx=(fidx)<(n_elements(filament)-1)
    file=filament[fidx].file
    if n_elements(xbox) eq 0 then begin
      xbox=filament[fidx].cplot.xbox(0:filament[fidx].cplot.np-1)
      ybox=filament[fidx].cplot.ybox(0:filament[fidx].cplot.np-1)
      newdata=0
    endif
    file=update_struct(file,filest)
    file.sav=fsav    
    if file.maptau eq '' then file.maptau='inverted_atmos_maptau.1.fits'
  endif else begin
    if n_elements(dirin) ne 0 then dir=dirin $
    else begin
      print,'Input parameter missing:'
      print,'Specify either a savfile or a directory' + $
            ' to read in the data. Example:'
      print,'fila,savfile=''filament.sav'''
      print,'fila,dir=''/scratch/slam/tiwari/new_inversions/tiwari_ls/' + $
            'mich_runs/2drun/full/'''
      retall
    endelse
    atmfile='inverted_atmos.fits'
    maptaufile='inverted_atmos_maptau.1.fits'
    proffile='inverted_profs.1.fits'
    obsfile='inverted_obs.1.fits'
    file=filest
    file.dir=dir
    file.atm=atmfile
    file.obs=obsfile
    file.prof=proffile
    file.maptau=maptaufile
    file.ambigresolved=0
  endelse
  if n_elements(xbox) eq 2 then xbox=xbox([0,0,1,1])
  if n_elements(ybox) eq 2 then ybox=ybox([0,1,1,0])  
  if n_elements(xbox) ne 4 or n_elements(ybox) ne 4 then begin
    print,'please define analysis region (use xfits to identify pixels):'
    print,'  ...,xbox=[llx,ulx,urx,lrx],ybox=[lly,uly,ury,lry]'
;reset
    xbox=[150,151,330,331]
    ybox=[345,370,371,346]
  endif
  
  if n_elements(atm) eq 0 or keyword_set(reread) then begin
    atm=read_fits_dlm(file.dir+file.atm,0,ahdr)
    maptau=read_fits_dlm(file.dir+file.maptau,0,mthdr)
    prof=read_fits_dlm(file.dir+file.prof,1,phdr) ;deconvolved profiles in ext=1
    obs=read_fits_dlm(file.dir+file.obs,0,ohdr)
  endif
  
  
  sz=size(atm)
  if n_elements(center) eq 0 then center=sz(1:2)/2
  
  
  
  dx=minmax(xbox) & nx=dx(1)-dx(0)+1
  dy=minmax(ybox) & ny=dy(1)-dy(0)+1
  nimg=12 ;number of different images (parameters) used for filament selection 
  np=n_elements(xbox)
  cplot={dx:dx,dy:dy,np:np,xbox:fltarr(20),ybox:fltarr(20), $
         nx:nx,ny:ny,showspline:1,shownode:1,showgrid:1,landscape:2}
  cplot.xbox(0:cplot.np-1)=xbox
  cplot.ybox(0:cplot.np-1)=ybox
  
  cst={name:'',par:0,pos:fltarr(4),coltab:-1,typ:0,wlidx:intarr(2), $
       center:center}
  cdat=replicate(cst,nimg)
  
  cdat(0).name='Temp. at logtau=-0.9'
  cdat(0).par=[6]
  cdat(1).name='B at logtau=-0.9'
  cdat(1).par=[9]
  cdat(2).name='INC at logtau=-0.9'
  cdat(2).par=[12]
  cdat(3).name='AZI at logtau=-0.9'
  cdat(3).par=[15]
  cdat(3).coltab=2
  cdat(4).name='VELOS at logtau=-0.9'
  cdat(4).coltab=1
  cdat(4).par=[18]
  cdat(5).name='Temp. at logtau=0'
  cdat(5).par=[7]
  cdat(6).name='B at logtau=0'
  cdat(6).par=[10]
  cdat(7).name='INC at logtau=0'
  cdat(7).par=[13]
  cdat(8).name='AZI at logtau=0.0'
  cdat(8).par=[16]
  cdat(8).coltab=2
  cdat(9).name='VELOS at logtau=0'
  cdat(9).coltab=1
  cdat(9).par=[19]
  cdat(10).name='ICONT_deconv' 
  cdat(10).coltab=0
  cdat(10).par=[0]
  cdat(10).typ=2                 ;typ 0=atm, 1=obs, 2=synth
  cdat(10).wlidx=[1,20]
  cdat(11).name='V_deconv (red wing)' 
  cdat(11).coltab=0
  cdat(11).par=[1]
  cdat(11).typ=2                 ;typ 0=atm, 1=obs, 2=synth
  cdat(11).wlidx=[85,105]
  nnod=50
  nspl=200
  npmax=100
  spl={n:0,xnod:fltarr(nnod),ynod:fltarr(nnod), $
       xspl:fltarr(nspl),yspl:fltarr(nspl), $
       xgrid:fltarr(nspl,npmax),ygrid:fltarr(nspl,npmax),nperp:10}
  
  if newdata eq 0 then begin
;   cdat =update_struct(filament[fidx].cdat,cdat,/nozero)
    spl  =update_struct(filament[fidx].spline,spl,/nozero)
    cplot=update_struct(filament[fidx].cplot,cplot,/nozero)
  endif
  
  sz=size(spl.xgrid)
  nspl=sz(1)
  npmax=sz(2)
  
  fillmaps,/init
                                ;take cdat NOT from sav-file
  filament.cdat=cdat
  if n_elements(filament) ne 0 then begin
    isame=-1
    for i=0,n_elements(filament)-1 do begin
      if min(filament[i].cplot.xbox(0:np-1) eq xbox and $
             filament[i].cplot.ybox(0:np-1) eq ybox) eq 1 then isame=i
    endfor
    if isame ne -1 then begin
      print,'Update filament #'+n2s(isame)
    endif else begin
      fidx=n_elements(filament)
      fnew=replicate(filament[0],fidx+1)
      for i=0,fidx-1 do fnew(i)=update_struct(filament[i],filament[0],/nozero)
      filament=temporary(fnew)
      filament[fidx].cplot=cplot
      filament[fidx].cdat=cdat
      filament[fidx].spline=spl
      print,'New filament #'+n2s(fidx)
    endelse
  endif
  
  
  showmaps
  
  
                                ;draw actions
  repeat begin
    cursor,xc,yc,/device,/nowait
    image_cursor,xc,yc,szc=10,color=0, $
      x0=filament[fidx].cdat.pos(0),y0=filament[fidx].cdat.pos(1), $
      x1=filament[fidx].cdat.pos(2),y1=filament[fidx].cdat.pos(3)
    show=0
    key=strlowcase(get_kbrd(0))
    if key ne '' then print
    show=1
    case key of 
      's': save_filament
      'f': begin
        read,'Filament number: ',fidx
        fidx=((fidx)<(n_elements(filament)-1))>0
        fillmaps
        show=1
      end
      '+': begin
        fidx=((fidx+1)<(n_elements(filament)-1))>0
        fillmaps
        show=1
      end
      '-': begin
        fidx=((fidx-1)<(n_elements(filament)-1))>0
        fillmaps
        show=1
      end
      'd': begin
        yn=dialog_message(/question,/default_no, $
                          'Delete Filament #'+n2s(fidx)+'?')
        if yn eq 'Yes' then begin
          idx=indgen(n_elements(filament))
          keep=where(idx ne fidx)
          if keep(0) ne -1 then filament=filament[keep] $
          else begin
            print,'No Filament.'
            return
          endelse
          fidx=(fidx-1)>0
        endif
      end
      'w': begin
        print
        ii=''
        read,prompt='grid width perp. to spline (current_value='+ $
          n2s(filament[fidx].spline.nperp)+' pix): ',ii
        if strlen(strcompress(ii,/remove_all)) ge 1 then $
          if fix(ii) ne filament[fidx].spline.nperp then begin
          filament[fidx].spline.nperp=fix(ii)
        endif
      end
      'r': 
      'p': print_filament
      'o': paperplot
      'd': filament[fidx].cplot.showspline= $
        filament[fidx].cplot.showspline eq 0 ;toggle spline display
      'l': filament[fidx].cplot.landscape= $
        (filament[fidx].cplot.landscape+1) mod 3;toggle p/l/auto display
      'g': filament[fidx].cplot.showgrid= $
        filament[fidx].cplot.showgrid eq 0 ;toggle grid display
      'm': begin
        print,'Manual entry of filament nodes for filament #',fidx
        key=''
        np=0
        xs=0. & ys=0.
        
        repeat begin
          print,'Enter x,y position of node #',np,' format: x,y; enter to end input'
          key=''
          read,key
          if key ne '' then begin
            keysplit=strsplit(key,',',/extract)
            if n_elements(keysplit) eq 2 then begin
              xs=[xs,float(keysplit[0])]
              ys=[ys,float(keysplit[1])]
              np=np+1
            endif else print,'invalid entry'
          endif
        endrep until key eq ''
        filament[fidx].spline.n=np
        filament[fidx].spline.xnod(0:filament[fidx].spline.n-1)=xs[1:*]
        filament[fidx].spline.ynod(0:filament[fidx].spline.n-1)=ys[1:*]
        fill_spline
        showmaps
        show=1
      end
      else: show=0              ;do nothing
    endcase
    inrg=(where(filament[fidx].cdat.pos(0) le xc and $
                filament[fidx].cdat.pos(2) ge xc and $
                filament[fidx].cdat.pos(1) le yc and $
                filament[fidx].cdat.pos(3) ge yc))(0)
    if inrg ne -1 then begin
      xdat=(xc-filament[fidx].cdat(inrg).pos(0))/ $
        (filament[fidx].cdat(inrg).pos(2)-filament[fidx].cdat(inrg).pos(0))* $
        (filament[fidx].cplot.dx(1)-filament[fidx].cplot.dx(0))+ $
        filament[fidx].cplot.dx(0)
      ydat=(yc-filament[fidx].cdat(inrg).pos(1))/ $
        (filament[fidx].cdat(inrg).pos(3)-filament[fidx].cdat(inrg).pos(1))* $
        (filament[fidx].cplot.dy(1)-filament[fidx].cplot.dy(0))+ $
        filament[fidx].cplot.dy(0)
      case !mouse.button of
        1: begin                ;add spline point
          if filament[fidx].spline.n ge 1 then begin
            xn=filament[fidx].spline.xnod(0:filament[fidx].spline.n-1)
            yn=filament[fidx].spline.ynod(0:filament[fidx].spline.n-1)
            dist=(xn-xdat)^2+(yn-ydat)^2
            mdist=min(dist,imin)
            in=indgen(filament[fidx].spline.n)
            if dist((imin+1)<(filament[fidx].spline.n-1)) gt $
              dist((imin-1)>0) then begin
              if imin-1 lt 0 then in=[filament[fidx].spline.n,in] $
              else in=[in(0:imin-1),filament[fidx].spline.n, $
                       in(imin:filament[fidx].spline.n-1)]
            endif else begin
              if imin+1 eq filament[fidx].spline.n then $
                in=[in,filament[fidx].spline.n] $
              else in=[in(0:imin),filament[fidx].spline.n, $
                       in(imin+1:filament[fidx].spline.n-1)]
            endelse
            filament[fidx].spline.xnod(0:filament[fidx].spline.n)= $
              ([filament[fidx].spline.xnod(0:filament[fidx].spline.n-1), $
                xdat])(in)
            filament[fidx].spline.ynod(0:filament[fidx].spline.n)= $
              ([filament[fidx].spline.ynod(0:filament[fidx].spline.n-1), $
                ydat])(in)
          endif else begin
            filament[fidx].spline.xnod(0)=xdat
            filament[fidx].spline.ynod(0)=ydat
          endelse
          filament[fidx].spline.n=filament[fidx].spline.n+1
          show=1
        end
        4: if filament[fidx].spline.n ge 1 then begin ;remove spline point
          dist=sqrt((filament[fidx].spline.xnod(0:filament[fidx].spline.n-1)- $
                     xdat)^2+ $
                    (filament[fidx].spline.ynod(0:filament[fidx].spline.n-1)- $
                     ydat)^2)
          mdist=min(dist,imin)
          if mdist le 10. then begin
            filament[fidx].spline.xnod(imin:nnod-2)= $
              filament[fidx].spline.xnod(imin+1:nnod-1)
            filament[fidx].spline.ynod(imin:nnod-2)= $
              filament[fidx].spline.ynod(imin+1:nnod-1)
            filament[fidx].spline.n=filament[fidx].spline.n-1
            show=1
          endif
        endif
        else:
      endcase
    endif
    
    if show then begin
;      fillmaps
      fill_spline
      showmaps
    endif
    wait,.1
    cancel=key eq 'x'
  endrep until cancel
  
  save_filament
end

; filament list of sanjiv:
; 160:160+140,354:354+10        ; inner penumbra (horizontal)
; 382:382+12,530:530+90         ;middle penumbra (vertical)
; 492:492+11,130:130+60         ;outer penumbra (vertical)
; here are some more, all of these are inclined:
; x1 y1:  (303,473)      ;  (262,398); (263,587); (503,276); (540,556)
; x2 y2:  (273,537)      ;  (169,426); (228,637); (531,234); (573,616)

pro create_all
  fila,xbox=[150,151,330,331],ybox=[345,370,371,346]
  fila,xbox=[310,370],ybox=[440,530]
  
  
;  fila,xbox=[350,410],ybox=[470,700]
; fila,xbox=[262,263,540,503],ybox=[398,587,556,276]
  
end

pro merge
  dir='/scratch/slam/tiwari/new_inversions/tiwari_ls/filaments/'
  
  restore,/v,dir+'filament_inner_20.sav'
  fi=filament
  restore,/v,dir+'filament_middle_20.sav'
  fm=filament
  restore,/v,dir+'filament_outer_20.sav'
  fo=filament
  
  filament=[fi,fm,fo]
  save,/xdr,/compress,filament,file='~/data/filaments/60filaments.sav'
  
  
end

pro fig2                        ;for paper
  fila,sav='~/data/filaments/filament_ref.sav'
  paperplot
end
