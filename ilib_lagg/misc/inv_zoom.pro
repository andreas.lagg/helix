pro inv_zoom
  common izdat,prof,phdr,atmos,ahdr
  
  if n_elements(prof)  eq 0 then begin
    dir='/home/lagg/data/Hinode/30nov06/2D_double/'
    prof=read_fits_dlm(dir+'inverted_profs.1.fits',1,phdr)
    atmos=read_fits_dlm(dir+'inverted_atmos.fits',0,ahdr)
  endif
  psz=size(prof,/dim)
  asz=size(atmos,/dim)
  scl=0.08
  tmpdir='/home/lagg/tmp/tmpmovie/'
  outdir='/home/lagg/tmp/'
  movie=outdir+'AR10926_zoom'
  pssize=[14,18]
  
  icmap=reform(total(prof[10:30,0,*,*],1)/21)
  icmap=icmap[100:700,*]
  sz=size(icmap,/dim)
  zrg=minmaxp(icmap,perc=99.5)
  
  zxy=[265,560]
  zfact=6
  center=sz/2.
  
  !x.margin=[6,0]
  !y.margin=[2,1]
  psset,/ps,file=outdir+'show.ps',size=pssize,/no_x,/show,jpg=0
  userlct,coltab=0,/reverse,/full
  image_cont_al,/cut,/aspect,contour=0,icmap,zrange=zrg
  userlct
  plots,psym=1,zxy[0],zxy[1],color=1
  psset,/close
  
                                ;path from center of image to zxy
  n=100
  s=1.
  vec=findgen(n)/(n-1)*s-s/2.
  zvec=erf(vec)
  vec=(vec-min(vec))/(max(vec)-min(vec))
  zvec=(zvec-min(zvec))/(max(zvec)-min(zvec))
  
  xvec=vec*(zxy[0]-center[0])+center[0]
  yvec=vec*(zxy[1]-center[1])+center[1]
  zoom=vec*(zfact-1)+1          
  zvec=(zoom-1)^2
  zvec=(zvec-min(zvec))/(max(zvec)-min(zvec))
  
;  xvec=interpol(xvec,vec,zvec)
;  yvec=interpol(yvec,vec,zvec)
  zoom=interpol(zoom,vec,zvec)
  
  spawn,' mkdir -p '+tmpdir+' ; rm -rf '+tmpdir+'*'
 for i=0,n-1 do begin
    xx=0>(xvec[i]+[-1,1]*sz[0]/2./zoom[i])<(sz[0]-1)
    yy=0>(yvec[i]+[-1,1]*sz[1]/2./zoom[i])<(sz[1]-1)
    img=icmap[xx[0]:xx[1],yy[0]:yy[1]]
    
    tfile=tmpdir+n2s(i,format='(i4.4)')+'.eps'
    psset,/ps,/no_x,size=pssize,jpg=1,file=tfile,/encapsulated,view=0
    userlct,coltab=0,/full,/reverse
    image_cont_al,/cut,/aspect,contour=0,img,xrange=xx*scl,yrange=yy*scl, $
                  title='AR 10926, 30-Nov-2006',zrange=zrg, $
                  xtitle='x [arcsec]',ytitle='y [arcsec]'
    psset,/close
    if i eq 0 then spawn,'cp -p '+tfile+' '+movie+'.eps'
  endfor
  
  
  moviecmd='mencoder mf://'+tmpdir+'/*.jpg -mf fps=10:type=jpg -ovc x264 -x264encopts bitrate=3000 -oac copy -o '+movie+'.avi'
  
  print,'Executing Movie command:'
  print,moviecmd
  spawn,moviecmd
  print,'Movie created: ',movie+'.avi' 
 
  stop
end
