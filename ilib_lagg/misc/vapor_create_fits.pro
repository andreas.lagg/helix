pro vapor_create
                                ;check if vapor is installed
  vapordir='/usr/local/vapor-2.3.0'
  fi=file_search(vapordir+'/bin/vapor-setup.sh',count=cnt)
  if cnt eq 0 then begin
    print,'Vapor not installed in '+vapordir
    print,'Exit.'
    return
  endif
  cmd='. '+vapordir+'/bin/vapor-setup.sh '
    
  
                                ;input: create a  4D array with
                                ;x,y,z,par
  indir='/data/sunrise/2009/SUFI/mhd/MURaM/MySimulations/MURaM_RUN_1/30G.ngrey.20111110.576x576/'
  outdir='./vapordata/'

  
  for i=0,5 do begin
    case i of
      0: begin
        data=readfits(indir+'eosT.290000.fits',hdr) ;temp
        sz0=size(data,/dim)
        dim=n2s(sz0[0])+'x'+n2s(sz0[1])+'x'+n2s(sz0[2])
        varname='TEMP'
      end
      1: begin
        data=readfits(indir+'result_5.290000.fits',hdr) ;bx
        varname='BX'
        bx=data
      end
      2: begin
        data=readfits(indir+'result_6.290000.fits',hdr) ;by
        varname='BY'
        by=data
      end
      3: begin
        data=readfits(indir+'result_7.290000.fits',hdr) ;bz
        varname='BZ'
        bz=data
      end
      4: begin
        data=sqrt(bx^2+by^2+bz^2)
        varname='BTOT'
      end
      5: begin
        data=readfits(indir+'eosP.290000.fits',hdr) ;bx
        varname='PRESS'
      end
    endcase
    
    sz=size(data,/dim)
    if min(sz eq sz0) eq 0 then $
      message,'All data cubes must have same dimensions.'
    
    ;par=add_comma(vname,sep=':')
  
    spawn,'mkdir -p '+outdir
    vdf=outdir+varname+'.vdf'
    cmd=[cmd,'vdfcreate -dimension '+dim+' -vars3d '+varname+' '+vdf]
    print,cmd
    
    fdat=outdir+'data'+string(i,format='(i3.3)')+'.float'
    openw,unit,/get_lun,fdat
    writeu,unit,data
    free_lun,unit
    
    cmd=[cmd,'raw2vdf -varname '+varname+' '+vdf+' '+fdat]
  endfor
  print,'Running all vapor commands:'
  cmdall=add_comma(cmd,sep=' ; ')
  spawn,cmdall
  print,'Results written to directory: '+outdir
  stop
end

