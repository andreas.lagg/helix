pro cut_muram
  common mudat,data,hdr
  
  if n_elements(data) eq 0 then begin
    data=readfits('/data/slam/MURaM/sunspot_1536x384x1536/mhd_atmos_full.fits',hdr)
  endif
  
  map=reform(data[0,*,*,639])   ;temperature cut
  map=transpose(map)
  sz=size(map,/dim)
  
  loadct,3
  tvlct,/get,r,g,b
  img=map[0:639,*]
  img=alog(img)
  zrg=minmaxp(img,perc=99.9)
  img=0>((img-zrg[0])/(zrg[1]-zrg[0])*255)<255
  
  img=congrid(img,sz[0]*4,sz[1]*4,/interp,/cubic)
  write_tiff,'mhd_temp_cut.tif',reverse(img,2),red=r,green=g,blue=b
  print,size(img)
  stop
end
