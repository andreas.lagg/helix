function azi_smooth,azi
  
  n=18
  mm=fltarr(n)
  add=findgen(n)*10+360
  for i=0,n-1 do begin
    amap=(azi+add[i]) mod 90
    mm[i]=max(amap)-min(amap)
  endfor
  dummy=min(mm,imin)
  
  amap=(azi+add[imin]) mod 90
  imap=(azi+add[imin]) mod 180
  
  flip=where(abs(amap-imap) ge 1e-3)
  fmap=amap*0 & fmap(flip)=1
  !p.multi=[0,1,6] & imgal,azi & imgal,amap & imgal,imap & imgal,fmap
stop  
  if flip(0) ne -1 then begin
    imap[flip]=imap[flip]+180
  endif
  imap=imap-(add[imin] mod 180)
  
  print,'add:',add[imin]
  print,'old:',minmax(azi)
  print,'new:',minmax(imap)
  
  return,imap
  
  
end
