pro vapor_create
                                ;check if vapor is installed
  vapordir='/usr/local/vapor-2.3.0'
  fi=file_search(vapordir+'/bin/vapor-setup.sh',count=cnt)
  if cnt eq 0 then begin
    print,'Vapor not installed in '+vapordir
    print,'Exit.'
    return
  endif
  cmd='. '+vapordir+'/bin/vapor-setup.sh '
    
  
                                ;input: create a  4D array with
                                ;x,y,z,par
  indir='/data/slam/MuRAM/sunspot_1536x384x1536/'
  outdir='./vapor_spot/'

  
  for i=0,6 do begin
    case i of
      0: begin
        restore,indir+'eos.sav',/v
        data=T
        sz0=size(data,/dim)
        dim=n2s(sz0[0])+'x'+n2s(sz0[1])+'x'+n2s(sz0[2])
        varname='TEMP'
      end
      1: begin
        data=P
        varname='PRESS'
      end
      2: begin
        data=RHO
        varname='DENS'
      end
      3: begin
        restore,indir+'mag.sav',/v
        data=BX
        varname='BX'
      end
      4: begin
        data=BY
        varname='BY'
      end
      5: begin
        data=BZ
        varname='BZ'
      end
      6: begin
        data=sqrt(bx^2+by^2+bz^2)
        varname='BTOT'
      end
    endcase
    
    sz=size(data,/dim)
    if min(sz eq sz0) eq 0 then $
      message,'All data cubes must have same dimensions.'
    
    ;par=add_comma(vname,sep=':')
  
    spawn,'mkdir -p '+outdir
    vdf=outdir+varname+'.vdf'
    cmd=[cmd,'vdfcreate -dimension '+dim+' -vars3d '+varname+' '+vdf]
    print,cmd
    
    fdat=outdir+'data'+string(i,format='(i3.3)')+'.float'
    openw,unit,/get_lun,fdat
    writeu,unit,data
    free_lun,unit
    
    cmd=[cmd,'raw2vdf -varname '+varname+' '+vdf+' '+fdat]
  endfor
  print,'Running all vapor commands:'
  cmdall=add_comma(cmd,sep=' ; ')
  spawn,cmdall
  print,'Results written to directory: '+outdir
  stop
end

