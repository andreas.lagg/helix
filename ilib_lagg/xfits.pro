; xfits.pro - data analysis tool for inversion results stored in FITS
;             files
pro xfits_clearall
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xpswg,xpswg
  common xdir,xdir
  common xppwg,xppwg
  common xpprof,xpprof
  common xfsswg,xfsswg
  common xss,xss
  common parwindow,pwst
  common geom,geom
  @read_fitsstokes_common.pro
 
    clr=['xfwg','fits','xpwg','xpswg','oldfile','xppwg','xpprof','pwst', $
         'xfsswg','xss','dfits0','dfits1','dfits2','geom']
  
  print,'Clearing all variables...'
  for i=0,n_elements(clr)-1 do $
    dummy=execute('if n_elements('+clr(i)+') ne 0 then' + $
                  ' dummy=temporary('+clr(i)+')')
end

pro xfits_clearid
                                ;delete widget base IDs (if restored
                                ;from sav file they might contain
                                ;values which are not matching the
                                ;actual numbers)
  wid=['xfwg','xpwg','xpswg','xppwg','xpprof','xfsswg','xss']
  for i=0,n_elements(wid) -1 do begin
    exstr='common '+wid[i]+' & if n_tags('+wid[i]+') gt 0 then ' + $
          ' if max(tag_names('+wid[i]+') eq ''BASE'') eq 1 then ' + $
          wid[i]+'.base.id=-1'
    dummy=execute(exstr)
  endfor
end

pro xfits,fitsorsav,update=update,only_settings=only_settings,pick=pick,idl=idl
  common oldfos,oldfos
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xpswg,xpswg
  common old,oldfiletime
  common xdir,xdir
  common xppwg,xppwg
  common xpprof,xpprof
  common xfsswg,xfsswg
  common xss,xss
  common use_idl,use_idl
  
  xfits_dir
  
  if n_params() eq 0 then begin
    print,'xfits - data analysis tool to display inversion results'
    print,'        stored in FITS files'
    print,'Usage:'
    print,'xfits,''atm_archive/fitsout.fits''      - reads in FITS file'
    print,'xfits,''xfits_plots/fitsout.xfits.sav'' - '+ $
      'reads in sav file created with xfits'
    print,'xfits,/pick                       - pick sav-file from list'
    print,'xfits,/idl                        - use IDL version to compute profile'
    if keyword_set(pick) eq 0 then reset
    
    xff=file_search(xdir.save+'*.xfits.sav',count=cnt)
    if cnt eq 0 then reset
    cd,xdir.save,current=dir
    fitsorsav=dialog_pickfile(filter='*.xfits.sav',/read, $
                              title='Restore plot settings')
    cd,dir
    if fitsorsav eq '' then reset
    print
    print,'Calling xfits:'
    print,'xfits,'''+fitsorsav+''''
  endif
  
  if n_elements(idl) eq 0 then use_idl=0 else use_idl=idl
  
  if n_tags(xfwg) gt 0 and keyword_set(update) eq 0 then begin
    widget_control,bad_id=bid,xfwg.base.id,/destroy
;    if bid eq 0 then if xfwg.base.id ne 0 then $
;       widget_control,/destroy,xfwg.base.id
  endif
  
  fi=file_info(fitsorsav)
  update=0
  if strpos(fitsorsav,'.xfits.sav') eq -1 then begin
    fitsfile=fitsorsav 
    if n_elements(oldfiletime) ne 0 then $
      if fi.mtime ne oldfiletime then xfits_clearall
    ; if n_elements(xpprof) ne 0 then dummy=temporary(xpprof)
    ; if n_elements(xppwg) ne 0 then dummy=temporary(xppwg)
  endif else begin
    if n_elements(fits) ne 0 then olddata=fits.file
    xfits_clearall
    restore,fitsorsav
    xfits_clearid
    if keyword_set(only_settings) then $
      if n_elements(olddata) ne 0 then fitsfile=olddata
    update=1
  endelse
  
  read_fitsdata,fitsfile
  if update eq 1 then begin ;use obsfile stored in sav-file, if it exists
    if xfwg.obs.str ne '' then begin
      fi=file_info(xfwg.obs.str)
      if fi.exists then fits.obsfile=xfwg.obs.str
    endif
  endif
  
  if n_elements(oldfos) eq 1 then update=oldfos eq fitsorsav
  oldfos=fitsorsav
  
  
  xfits_widget,update=update,only_settings=keyword_set(only_settings)
  if n_elements(oldfile) ne 0 then close=oldfile ne fitsfile else close=1
  xfits_plot,close=close
  oldfile=fi.mtime
end
