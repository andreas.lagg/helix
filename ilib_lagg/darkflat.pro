;quick reduction using darks and flats
pro darkflat
  
  dir='~/tmp/'
  fdata=dir+'pco1_new_20130611-145904_486.fits'
  fdark=dir+'pco1_dark_20130611-151834_486.fits'
  fflat=dir+'pco1_flat_20130611-151110_486.fits'
  
  dark=read_fits_dlm(fdark,0,hdark)
  flat=read_fits_dlm(fflat,0,hdark)
  obs=read_fits_dlm(fdata,0,hdark)
  
  szd=size(dark,/dim)
  szf=size(flat,/dim)
  szo=size(obs,/dim)
  avgdark=total(dark,3)/szd[2]
  avgflat=total(flat,3)/szf[2] - avgdark
  
  psset,/ps,file=dir+'pco1_new_20130611-145904_486.ps',/no_x,size=[18,20],pdf=0
  userlct,coltab=0,/full,/reverse
;  for i=0,szo[2]-1 do begin
  i=1
    img=(obs[*,*,i]-avgdark)/avgflat
    zrg=minmaxp(img,/nan,perc=99.9)
    image_cont_al,/cut,/aspect,contour=0,img,zrange=zrg, $
                  title='New AR at x=0,y=-200'
;    if i lt szo[2]-1 then erase
;  endfor
  psset,/close
  stop
end
