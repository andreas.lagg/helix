;makes cubic spline fit
;varies the y-value of the spline nodes to optimize chisqr between
;spline fit and original data
;deltax defines the range around the spline nodes. 
;xret defines the x-nodes of the returned spline function

function minchi,xdat,ydat,xspl,yspl
  
;  y=spline(xspl,yspl,xdat,.1)
  y=interpol(yspl,xspl,xdat)
  return,total(abs(ydat^2-y^2))
  
end

function splamoeb,p
  common spamoeba,x,y,xnod,yip,xval,yspl
  
  
;  yspl=interpol(yip+p,xnod,xval)
  yspl=spline(xnod,yip+p,xval,.1)
  chisqr=minchi(x,y,xval,yspl)
  return,chisqr
end

function spline_best,xx,yy,nodes=nodes,deltax=deltax,xret=xret,show=show
  common spamoeba,x,y,xnod,yip,xval,yspl
  
  if n_elements(xret) eq 0 then xret=nodes
  
                                ;remove NANs
  uidx=where(finite(xx) and finite(yy))
  if uidx(0) eq -1 then begin
    message,/cont,'No valid points.'
    return,xret*0+!values.f_nan
  endif
  
  x=xx(uidx) & y=yy(uidx)
  srt=sort(x)
  x=x(srt) & y=y(srt)
  xnod=nodes(sort(nodes))
  xval=xret(where(xret ge min(xnod) and xret le max(xnod)))
;  xval=xret
  
  yip=interpol(y,x,xnod)
;  plot,x,y & oplot,color=1,xnod,yip
  
  nnod=n_elements(xnod)
  p0=fltarr(nnod)
  scale=fltarr(nnod)+(max(y)-min(y))/100.
  res=amoeba(1e-3,function_name='splamoeb',function_value=fval, $
             ncalls=nc,nmax=500,p0=p0,scale=scale)
  
  chi=minchi(x,y,xnod,yip)
  yspl=spline(xnod,yip+res,xret,.1)
  
  
  if keyword_set(show) then begin
    plot,x,y & oplot,color=1,xnod,yip & oplot,color=2,xret,yspl
    wait,.2
  endif
  
  return,yspl
end
