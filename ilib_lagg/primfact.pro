;integer factorization routine.
;print,primfact(N,nfact=nfact)

;SUBROUTINE TO FIND THE PRIME FACTORS OF A NUMBER
;Author : Louisda16th a.k.a Ashwith J. Rego
;Description: 
;Algorithm is quite easy:
;Start with 2, check whether 2 is a factor by seeing if MOD(<input_number>,2)
;is zero. If it is zero, then 2 becomes a factor. If not, check with the next number.
;When a factor is found, divide the given number with the factor found. However,
;donot move to the next possible factor - a number can occur more than once as a factor
function primfact,num,nfact=f
  factors=lonarr(num/2)
  i=2l
  f = 0l                 ;Number of factors
  n = long(num)          ;store input number into a temporary variable
  repeat begin
    IF n mod i eq 0 then begin  ;If i divides 2, it is a factor
      factors(f) = i
      f = f+1
      n = n/i
    endif else begin
      i = i+1                   ;Not a factor. Move to next number
    endelse
  endrep until n eq 1    
  return,factors(0:f-1)
end
