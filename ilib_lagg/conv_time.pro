;+
; NAME:
;    conv_time
;
;
; PURPOSE:
;    converts time between various formats
;
;
; CATEGORY:
;    reading EPD-Data
;
;
; CALLING SEQUENCE:
;    time_new=conv_time(time_old,old_format=old_format,new_format=new_format)
;
; 
; INPUTS:
;    time_old - string containing time in one of the following
;               formats:
;      1 ... doy: 'yyyy.dddhhmmss', example: 1990.3421200
;      2 ... date: 'ddmmyyyy.hhmmss', example: 08121990.1200
;      3 ... sec: 'ss.hhhhhhh', time in seconds since 01/01/1960 (HGA)
;   (                                              or 01/01/2000 (LGA) ??
;      4 ... date_dot: 'dd.mm.yyyy;hh:mm:ss.ss', e.g.: 08.12.1990;01:00:00.00
;                      /hhmmss - only full seconds are returned
;                      /hm - no seconds are returned
;      5 ... zero: 'hhhh:mm:ss.ss' no date is returned, only hours
;    
;    old_format: string ('doy','date','sec') or number (1,2,3)
;    new_format: string ('doy','date','sec') or number (1,2,3)
;
; OPTIONAL INPUTS:
;
;
;	
; KEYWORD PARAMETERS:
;    ret_julian - return date in seconds from julian day 0
;    julian - day zero is julian day 0
;
; OUTPUTS:
;    time_new - string containing time in one of the above formats
;
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;    sec=conv_time('1990.3421200',old_format='doy',new_format='sec')
;
;
; MODIFICATION HISTORY:
;    A. Lagg, 15/04/1996
;-

function conv_time,old_time,old_format=old_format,new_format=new_format, $
                   hm=hm,hhmmss=hhmmss,ret_julian=ret_julian, $
                   julian=julian,idl=idl,date_zero=date_zero
;  common input

  offset=0.  
;   if ipt(0).epd_mode eq 1 then begin
;    date_zero=[01,01,2000,12,00,00-62.500]      ;(??)
;   endif else begin
;     date_zero=[01,01,2000,12,00,00-62.500] ;zero at 01-01-2000, 12 UT
;   endelse
  if n_elements(date_zero) ne 6 then $
    date_zero=[01,01,2000,12,00,00-62.500] ;(??)
;date_zero=[01,00,1958,12,00,00] ;(??)
  
  if keyword_set(julian) then begin
    caldat,0.,m,d,y,hr,min,sec 
    if float(!version.release) lt 5.1 then $
      d=d-1                     ;-1 Tag Offset muss ich mir noch anschauen!!
    date_zero=[m,d,y,hr,min,sec]
  endif
  if keyword_set(idl) or strlowcase(new_format) eq 'zero' then begin
    date_zero=[01,01,1970,00,00,00]
    dt=bin_date() & ts=systime(1)
    os= round((-(ts - 86400d *(julday(dt(1),dt(2),dt(0),0)-julday(date_zero(0),date_zero(1),date_zero(2),date_zero(3)))-3600.*dt(3)-60.*dt(4)-dt(5)))/60.)*60l
    offset=os                 
  endif
  if n_elements(old_format) eq 0 or n_elements (new_format) eq 0 then  $
    message,'keyword old_format or new_format missing'
  t_old=old_time
  sz=size(t_old)
  if sz(sz(0)+1) eq 5 or sz(sz(0)+1) eq 4 then begin
    t_old=string(old_time)
    for i=0l,n_elements(old_time)-1 do $
      t_old(i)=string(old_time(i),format='(f40.20)')
    sz=size(t_old)
  endif
  if sz(sz(0)+1) ne 7 then message,/traceback,'time has to be given as string'

  nr=['doy','date','sec','date_dot','zero']

  sz=size(old_format)
  if sz(1) eq 7 then old_format=1+where(nr eq strlowcase(old_format))
  old_format=old_format(0)
  if old_format lt 1 or old_format gt n_elements(nr) then $
    message,'old_format not valid'

  sz=size(new_format)
  if sz(1) eq 7 then new_format=1+where(nr eq strlowcase(new_format))
  new_format=new_format(0)
  if new_format lt 1 or new_format gt n_elements(nr) then $
    message,'new_format not valid'

  if old_format eq 4 then begin
    add_i=1
    old_format=2
  endif else add_i=0
  if new_format eq 4 or new_format eq 5 then begin
    add_f=[',''.'')',','';'')',','':'')','(f5.2)']
    if new_format eq 4 then new_format=2
  endif else add_f=[')',',''.'')',')','(i2.2)']

  retval=t_old & retval(*)=''
  for n=0l,n_elements(t_old)-1 do begin
    case old_format of
      1: begin
        year=fix(strmid(t_old(n),0,4))
        doy=fix(strmid(t_old(n),5,3))
        hour=fix(strmid(t_old(n),8,2))
        min=fix(strmid(t_old(n),10,2))
        sec=fix(strmid(t_old(n),12,2))
        t_day=(double(julday(01,01,year,0.))+doy-1) $
          + hour/24d + min/1440d + sec/86400d
      end
      2: begin
        day=fix(strmid(t_old(n),0,2))
        month=fix(strmid(t_old(n),2+add_i,2))
        year=fix(strmid(t_old(n),4+2*add_i,4))
        hour=fix(strmid(t_old(n),9+2*add_i,2))
        min=fix(strmid(t_old(n),11+3*add_i,2))
        sec=fix(strmid(t_old(n),13+4*add_i,2))
        t_day=double(julday(month,day,year,0.)) + hour/24d + min/1440d + $
          sec/86400d
      end
      3: begin
        t_day=double(t_old(n))/86400d + $
          double(julday(date_zero(0),date_zero(1),date_zero(2),date_zero(3), $
                        date_zero(4),date_zero(5)))+offset/86400d
      end
      else: message,/traceback,'cannot convert from this format'
    endcase
    case new_format of
      1: begin
        juldat=t_day
        caldat,juldat+1e-2/86400.,month,day,year,hour,min,sec
        doy=julday(month,day,year,00)-julday(01,01,year,00)+1
;        rest=(t_day-long(t_day))*24d
;        hour=fix(rest +1e-4)
;        min=fix((rest-hour)*60d +1e-4)
;        sec=fix((rest-hour)*3600-min*60)
        secstr=string(sec,format='(i2.2)')
        if keyword_set(hm) then secstr=''
        t_ret=string(year,format='(i4.4)')+'.'+string(doy,format='(i3.3)')+ $
          string(hour,format='(i2.2)')+string(min,format='(i2.2)')+secstr
      end
      2: begin
        juldat=t_day
        caldat,juldat+1e-2/86400.,month,day,year,hour,min,sec
;        rest=(t_day-long(t_day))*24d
;        hour=fix(rest +1e-4)
;        min=fix((rest-hour)*60d +1e-4)
;        sec=((rest-hour-min/60.)*3600.)
        sec_str=string(sec,format=add_f(3))
        if strmid(sec_str,0,1) eq ' ' or strmid(sec_str,0,1) eq '-' then $
          strput,sec_str,'0',0
        if keyword_set(hhmmss) then sec_str=strmid(sec_str,0,2)
        if keyword_set(hm) then begin
          sec_str=''
          ad2=')'
        endif else ad2=add_f(2)
        t_ret=string(day,format='(i2.2'+add_f(0))+ $
          string(month,format='(i2.2'+add_f(0))+ $
          string(year,format='(i4.4'+add_f(1))+ $
          string(hour,format='(i2.2'+add_f(2))+ $
          string(min,format='(i2.2'+ad2)+ $
          sec_str
      end
      3: begin
        day_off=double(julday(date_zero(0),date_zero(1),date_zero(2), $
                              date_zero(3),date_zero(4),date_zero(5)))
        s_off=0;offset
        if keyword_set(ret_julian) then begin
          day_off=0.
          s_off=0;date_zero(3)*3600d +date_zero(4)*60d +date_zero(5)
        endif
        t_ret=strcompress(string(double(t_day-day_off)*86400d - $
                             s_off,format='(d30.15)'),/remove_all)
      end
      5: begin
        juldat=t_day-offset/86400d
        caldat,(juldat+1e-2/86400.),month,day,year,hour,min,sec
        sec_str=string(sec,format=add_f(3))
        if strmid(sec_str,0,1) eq ' ' or strmid(sec_str,0,1) eq '-' then $
          strput,sec_str,'0',0
        if keyword_set(hhmmss) then sec_str=strmid(sec_str,0,2)
        if keyword_set(hm) then begin
          sec_str=''
          ad2=')'
        endif else ad2=add_f(2)
        t_ret=strcompress(string(hour,format='(i10.2'+add_f(2)),/remove_all)+ $
          string(min,format='(i2.2'+ad2)+sec_str
      end
      else: message,/traceback,'cannot convert to this format'
    endcase
    retval(n)=t_ret
  endfor

  return,retval

end






