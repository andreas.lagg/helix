;procedure to change helix atm fits file
pro fits_change_prof,file
  
  ff=strsplit(file,'.fits',/regex,/extract)
  fnew=ff[0]+'_new.fits'
  
  data=read_fits_dlm(file,0,header)
  sz=size(data,/dim)
  print,'Size: ',sz
  print,'Change data [y/N]?'
  key=get_kbrd()
  if strupcase(key) eq 'Y' then stop
  write_fits_dlm,data,fnew,header,/create
  print,'Wrote data to: ',fnew
  iext=1
  repeat begin
    data=readfits(file,header,ext=iext)
    error=n_elements(data) eq 1 and data [0] eq -1
    if error eq 0 then begin
      print,'Reading EXT=',iext
      sz=size(data,/dim)
      print,'Size: ',sz
      print
      print,'Change extension #'+n2s(iext)+' [y/N]?'
      key=get_kbrd()
      if strupcase(key) eq 'Y' then stop
      writefits,fnew,data,header,/append
      iext=iext+1
    endif else print,'New fits file written: '+fnew
  endrep until error
  
end
