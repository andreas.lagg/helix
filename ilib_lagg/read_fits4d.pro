;reads in 4D Fits file (NWL, NSTOKES, NX, NY)
;WL in extension
pro read_fits4d,fitsfile,dataout,x=x,y=y,error=error,average=average, $
                full=full,header=retheader,nodata=nodata,order=order
  common readfits4ddat,oldfi,data
  common fits,fits
;
  common input,ipt,cnt          ;,comp_cnt
  
  if n_elements(x) eq 0 or n_elements(y) eq 0 then full=1 $
  else full=keyword_set(full)
  error=1
  fi=file_info(fitsfile)
  if n_elements(oldfi) eq 0 then reread=1 $
  else begin
    reread=fi.name ne oldfi.name or fi.ctime ne oldfi.ctime
    if full eq 0 then $
      reread=reread or $
             max(x-data.xoff ge data.naxis[2] or x-data.xoff lt 0 or $
                 y-data.yoff ge data.naxis[3] or y-data.yoff lt 0)
  endelse
  if reread then begin
    print,'Reading 4D Fits File '+fitsfile
    mrd_head,fitsfile,header
    xoff=sxpar(header,'XOFFSET') ;zero if not present
    yoff=sxpar(header,'YOFFSET')
    naxis=sxpar(header,'NAXIS*')
    if n_elements(naxis) ne 4 then message,'file is not 4D.'
    storder=sxpar(header,'STOKES')
    if size(storder,/type) ne 7 then begin
;      fi=file_info(fitsfile)
;      time_newformat=(julday(02,01,2013)-julday(01,01,1970))*24ul *3600ul
;      if fi.ctime ge time_newformat then storder='IVQU' else storder='IQUV'
      
      message,/cont,'WARNING: Stokes order not specified in FITS file.'
      if n_elements(order) eq 1 then storder=order else begin
        message,/cont,"You can specify the order with the FITS-keyword:"
        message,/cont,"STOKES 'IQUV'"
        storder='IQUV'
      endelse
      message,/cont,'ASSUMING Stokes order: '+storder
    endif
    print,'Stokes order: ',storder
    order=intarr(4)
    order[0]=strpos(storder,'I')
    order[1]=strpos(storder,'Q')
    order[2]=strpos(storder,'U')
    order[3]=strpos(storder,'V')
    xrg=[0,naxis[2]-1]
    yrg=[0,naxis[3]-1]
    ;; if n_elements(fits) ne 0 then begin
    ;;   xrg=[0,fits.nx-1]+fits.xoff-xoff
    ;;   yrg=[0,fits.ny-1]+fits.yoff-yoff
    ;; endif else if n_elements(ipt) ne 0 then begin
    ;;   xrg=[ipt.x[0]<x,ipt.x[1]>x]-xoff
    ;;   yrg=[ipt.y[0]<y,ipt.y[1]>y]-yoff
    ;; endif
    ;; xrg=(xrg>0)<(naxis[2]-1)
    ;; yrg=(yrg>0)<(naxis[3]-1)
    xtoff=xrg[0]>xoff
    ytoff=yrg[0]>yoff
    
    nx=xrg[1]-xrg[0]+1
    ny=yrg[1]-yrg[0]+1
    nwl=naxis(0)
    wlflag=0
    if keyword_set(nodata) eq 0 then begin
      data4d=read_fits_dlm(fitsfile,0,hdr,fpix=[0,0,xrg[0],yrg[0]], $
                           lpix=[naxis[0]-1,naxis[1]-1,xrg[1],yrg[1]])
                                ;check if WL infor is already in the
                                ;header of the data
    wlref=double(sxpar(hdr,'WLREF'))
    wlmin=double(sxpar(hdr,'WLMIN'))
    wlmax=double(sxpar(hdr,'WLMAX'))
    wlarr=dindgen(nwl)/(nwl-1)*(wlmax-wlmin)+wlmin+wlref
    if min(wlarr) ne max(wlarr) then wlflag=1
    endif
    
                                ;check icont / WL extension
                                ;first extension: usually icont
    icflag=0
    hdr1='' & hdr2='' & hdr3=''
    for iext=1,2 do begin
      mrd_head,fitsfile,hdr,extension=iext,err=err
      if strlen(err) eq 0 then begin
        enaxis=sxpar(hdr,'NAXIS*')
;        data1=mrdfits(fitsfile,iext,hdr,status=status_icont,/silent)
        data1=readfits(fitsfile,hdr,exten=iext,/silent)
      case n_elements(enaxis) of 
        1: begin
          wlref=double(sxpar(hdr,'WLREF'))
          wlarr=temporary(data1)
          wlflag=1
          print,'Found WL-vector.'
          hdr1=hdr
                                ;overwrite wl-vector if specified in
                                ;input file
          if n_elements(ipt) ne 0 then $
            if ipt.wl_disp ge 1e-5 and ipt.wl_off ge 1e-4 then begin
            wlref=ipt.wl_off
            wlarr=dindgen(nwl)*ipt.wl_disp+wlref
            print,'Replaced wavelength vector from FITS file with values from input file.'
          endif
        end
        2: begin
          icont=temporary(data1)
          icflag=1
          print,'Found Cont-Image.'
          hdr2=hdr
          icont_hsra=double(sxpar(hdr,'IC_HSRA'))
          icont=icont[xrg[0]:xrg[1],yrg[0]:yrg[1]]
          if n_elements(ipt) ne 0 then $
            if ipt.norm_cont eq 2 and icont_hsra ne 0 then $
              icont[*,*]=icont_hsra
        end
        3: begin                ;can be WL array or continuum image array
          if enaxis(0) eq nwl then begin
            wlref=double(sxpar(hdr,'WLREF'))
            wlarr=temporary(data1)
            if n_elements(size(wlarr,/dim)) eq 3 then begin
              wlflag=2
              print,'Found WL-array.'
            endif else begin
              wlflag=1
              print,'Found WL-vector.'
            endelse
          endif else begin
;            icont=temporary(reform(data1(0,*,*)))
            icont=temporary(reform(data1[0,xrg[0]:xrg[1],yrg[0]:yrg[1]]))
            icont_hsra=double(sxpar(hdr,'ICONT_HSRA'))
            if n_elements(ipt) ne 0 then $
              if ipt.norm_cont eq 2 and icont_hsra ne 0 then $
                icont[*,*]=icont_hsra
            icflag=1
            print,'Found Cont-Images, using spectral region 1'
          endelse
          hdr3=hdr
        end
        else: if wlflag eq 0 then begin
          wlref=sxpar(header,'WLREF') ;zero if not present
          wlmin=sxpar(header,'WLMIN') ;zero if not present
          wlmax=sxpar(header,'WLMAX') ;zero if not present
          nwl=sxpar(header,'NWL')     ;zero if not present
          wlarr=dindgen(nwl)/(nwl-1)*(wlmax-wlmin)+wlmin+wlref
          if min(wlarr) ne max(wlarr) then wlflag=1
        endif
      endcase
    endif
    endfor
    if icflag eq 0 then begin
      if keyword_set(nodata) eq 0 then $
        icmax=max(/nan,data4d[*,0,*,*]) $
      else icmax=1.
      icont=fltarr(nx,ny)+icmax
;      icont=icont[xrg[0]:xrg[1],yrg[0]:yrg[1]]
      print,'No Icont info in '+fitsfile
      print,'Please specify continuum level in input file, e.g.:'
      print,'IC_LEVEL     1.00000'
      print,'Using maxval for continuum: ',icmax
    endif
    if wlflag eq 0 then begin
      print,'No WL-info in '+fitsfile
      wlref=0d
      wlarr=-1
    endif
    if keyword_set(nodata) eq 0 then begin
      print,'read_fits4d - normalize to icont of every profile'
      for iy=0,ny-1 do for ix=0,nx-1 do data4d[*,*,ix,iy]/=icont[ix,iy]

      ;; for is=0,3 do for iwl=0,nwl-1 do $
      ;;   data4d(iwl,is,*,*)=data4d(iwl,is,*,*)/icont
    endif else data4d=!values.f_nan
    data={nw:nwl,nx:nx,ny:ny,xoff:xtoff,yoff:ytoff,order:order, $
          xrg:xrg,yrg:yrg,naxis:naxis, $
          wlref:wlref,icont:temporary(icont), $
          wlflag:wlflag,icflag:icflag,wl:temporary(wlarr), $
          iquv:ptr_new(temporary(data4d)), $
          header:header,headerwl:hdr1,headeric:hdr2,headerwlarr:hdr3}
    oldfi=fi
  endif
  
;  order=[0,1,2,3]
    nwl=data.nw
  order=data.order
  if full then begin
    case data.wlflag of 
      0: wl=dblarr(nwl)
      1: wl=data.wl
      2: wl=total(total(data.wl(*,*,*),2),2)/(data.nx*data.ny)
    endcase
    icont=data.icont
    dataout={wl:wl,wlref:data.wlref,icont:icont,order:order,data:data.iquv, $
             nw:data.nw,nx:data.nx,ny:data.ny, $
             xoff:data.xoff,yoff:data.yoff, $
             naxis:data.naxis,xrg:data.xrg,yrg:data.yrg}
  endif else begin
    xx=x-data.xoff
    yy=y-data.yoff
    if max(xx) lt 0 or min(xx) ge data.naxis[2] or $
      max(yy) lt 0 or min(yy) ge data.naxis[3] then begin
      print,'Could not find position'
      print,' x=',x
      print,' y=',y
      print,'in Fits4D file '+fitsfile
      dataout=-1
      return
    endif
    xx=0>(x-data.xoff)<(data.nx-1)
    yy=0>(y-data.yoff)<(data.ny-1)
    if keyword_set(average) eq 0 then begin
      xx=xx<min(xx)
      yy=yy<min(yy)
    endif
    xx=indgen(max(xx)-min(xx)+1)+min(xx)
    yy=indgen(max(yy)-min(yy)+1)+min(yy)
    ;; if n_elements(xx) eq 1 then xx=[xx(0),xx(0)]
    ;; if n_elements(yy) eq 1 then yy=[yy(0),yy(0)]
    nx=n_elements(xx)
    ny=n_elements(yy)
    x0=min(xx) & x1=max(xx)
    y0=min(yy) & y1=max(yy)
    
    case data.wlflag of 
      0: wl=dblarr(nwl)
      1: wl=data.wl
      2: if ny ge 2 then $
        wl=total(total(data.wl(*,x0:x1,y0:y1),2),2)/(nx*ny) $
      else if nx ge 2 then $ 
        wl=total(data.wl(*,x0:x1,y0:y1),2)/(nx*ny) $
      else $
        wl=reform(data.wl(*,x0:x1,y0:y1))
    endcase
    
    icont=mean(data.icont(x0:x1,y0:y1))
;   dnew=ptr_new(fltarr(nwl,4,fix(max(xx)-min(xx))+1,fix(max(yy)-min(yy))+1))
   dnew=ptr_new(fltarr(nwl,4,1,1))
    for i=0,3 do begin
;      (*dnew)(*,i,*,*)=total(total((*data.iquv)(*,i,xx,yy),3),3)/(nx*ny)
      stk=(*data.iquv)(*,order[i],x0:x1,y0:y1)
       if ny ge 2 then $
         (*dnew)(*,i,*,*)=total(total(stk,3),3)/(nx*ny) $
       else if nx ge 2 then $ 
         (*dnew)(*,i,*,*)=total(stk,3)/(nx*ny) $
       else $
         (*dnew)(*,i,*,*)=stk
     endfor
    dataout={wl:wl,wlref:data.wlref,icont:icont,iquv:dnew, $
             nw:data.nw,nx:data.nx,ny:data.ny, $
             xoff:data.xoff,yoff:data.yoff, $
             naxis:data.naxis,xrg:data.xrg,yrg:data.yrg, $
             order:[0,1,2,3]}
  endelse
;print,keyword_set(full),data.wlflag,minmax(wl),size(data.wl)
  if n_elements(retheader) ne 0 then $
    retheader={main:data.header,wl:data.headerwl, $
               ic:data.headeric,wlarr:data.headerwlarr}
  
  error=0
end
