pro align_hinode_sp_fg,reread=reread
  common olddata,index_sp,data_sp,hinic,spf,bfi,nfi
;tipimage
  
  if n_elements(bfi) eq 0 or keyword_set(reread) then begin
    dir='/data/slam/Hinode/sotfg20070430/'
    dir='~/tmp/sot/level1/2007/04/30/FG/'
;    mdifile='/data/slam/MDI/20080510/fd_M_96m_01d.5608.0006.fits'
    bfifile=dir+'FG20070430_185457.8.Gband4305.fits'
    fits_read,bfifile,data,header
    hst=fits_header2st(header)
    bfi={data:data,header:hst,file:bfifile}
    nfifile=dir+'FG20070430_185451.4.TFHI6563base.fits'
    fits_read,nfifile,data,header
    hst=fits_header2st(header)
    nfi={data:data,header:hst,file:nfifile}
    
;data_sp=readfits('/scratch/slam/vannoort/spinor2d/plage/fullmap.0.08.fits')
    dir_inv='/scratch/slam/vannoort/spinor2d/plage/helios2/run25b/'
    dir_inv='/home/lagg/data/spinor2d/plage/helios2/run25b/'
    data_sp=readfits(dir_inv+'inverted_profs.1.fits')
;     mask='/data/slam/Hinode/sotsp20070430/SP3D20070430_1[89]*.fits'
;     spf = findfile(mask,count=n)
;     if n eq 0 then message,'No files found in '+mask
;     read_sot_corr,spf,index_sp,data_sp
  endif

                                ;preselect bfi region
  imgbfi=bfi.data[*,*]
  
                                ;average hinode i profile
;  imgsp=reform(transpose(total(data_sp[1:10,*,0,*],1)/10))
  imgsp=reform(transpose(total(data_sp[1:10,0,*,*],1)/10))
  imgsp=transpose(imgsp)
;  imgsp=imgsp[400:700,*]
;  imgsp=imgsp[350:550,200:800]
;  imgsp=imgsp[0:120,100:300]
  
  
  fi=get_filepath(bfi.file)
  sav=fi.name+'_align.sav'
  fi=file_info(sav)
  if fi.exists then begin
    restore,sav
    guess={p:trans.p,q:trans.q}
  endif
  
  my_align,imgbfi,imgsp,trans=trans,flip=flip,mirror=mirror,box=box, $
    xarr=xarr_tr,yarr=yarr_tr,guess=guess,/norot
  trans.tipnam=bfi.file
  trans.hinnam=spf(0)+' - '+spf(n_elements(spf)-1)
  
  print,'Store transformation to sav-file...'
  fi=file_info(sav)
  if fi.exists then begin
    print,'Overwrite '+sav+' [y/N]?'
    repeat key=get_kbrd() until key ne ''
    if strupcase(key) ne 'Y' then read,'New filename: ',sav 
  endif 
                                ;test write access
  spawn,'touch '+sav+' ; test -w '+sav+' ; echo $?',res
  if res ne '0' then read,'Enter filename: ',sav
  save,trans,bfi,nfi,/xdr,/compress,file=sav
  print,'Image transformation stored in '+sav
  
  
  align_show,sav
  
  if n_elements(nfi) ne 0 then begin
    print,'BFI / NFI alignment (from BFI/NFI header):'
    print,'BFI number pixels: ',bfi.header.naxis1,bfi.header.naxis2
    print,'NFI number pixels: ',nfi.header.naxis1,nfi.header.naxis2
    print,'BFI pix scale (arcsec): ',bfi.header.xscale,bfi.header.yscale
    print,'NFI pix scale (arcsec): ',nfi.header.xscale,nfi.header.yscale
    print,'BFI central pix (arcsec): ',bfi.header.crpix1,bfi.header.crpix2
    print,'NFI central pix (arcsec): ',nfi.header.crpix1,nfi.header.crpix2
  endif
  
  
  stop
end

