;plotting routine for xfits
function get_plotidx,x,y
  common xfwg,xfwg
  common xpwg,xpwg
  
  if xpwg.multipage.val eq 0 then begin
    pos=xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).posframe
    ix=where(pos[0,*,0] le x and pos[2,*,0] ge x)
    iy=where(pos[1,0,*] le y and pos[3,0,*] ge y)
    ixy=fix([ix(0),xfwg.ny.val-1-iy(0)])
  endif else begin
    ixy=[fix(xpwg.pagepm.val) mod fix(xfwg.nx.val), $
         fix(xpwg.pagepm.val)  /  fix(xfwg.nx.val)]
  endelse
  ixy[0]=ixy[0]<(xfwg.nx.val-1)
  ixy[1]=ixy[1]<(xfwg.ny.val-1)
  
  return,ixy
  
end

pro clickhelp
  common xpwg,xpwg
  
  hlp=['plot profile to X-window', $
       'plot profile to PS-file', $
       'click on LowLeft & UpRight to zoom', $
       'delete map from list']
  widget_control,xpwg.clickhelp.id,set_value=hlp(xpwg.click.val)
end

pro xfits_parwindow,xyin,value=parvalout,name=parnamout,nowidget=nowidget, $
                    ix=ixin,iy=iyin
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common parwindow,pwst
  common xupwg,xupwg
  
 if n_elements(pwst) eq 0 then begin
    subst={id:0l,val:0.,str:''}
    pwst={base:subst,text:subst,xysz:intarr(2)}
    new_wg=1
  endif else begin
    widget_control,pwst.base.id,bad_id=bad_id
    new_wg=pwst.base.id eq 0 or bad_id ne 0
  endelse
  xy=xyin-[fits.xoff,fits.yoff]
  if min(xy) lt 0 or xy(0) ge fits.nx or xy(1) ge fits.ny then return
  
  if n_elements(xupwg) eq 0 then dummy=xfits_userpar(/call)
  
  pararr=strarr(xfwg.nx.val,xfwg.ny.val*3)
  parvalout=fltarr(xfwg.nx.val,xfwg.ny.val)+!values.f_nan
  parnamout=strarr(xfwg.nx.val,xfwg.ny.val)
  parvec=strarr(xfwg.ny.val*3)
  if n_elements(ixin) eq 0 then ixin=indgen(xfwg.nx.val)
  if n_elements(iyin) eq 0 then iyin=indgen(xfwg.ny.val)
  for iiy=0,n_elements(iyin)-1 do begin
    iy=iyin[iiy]
    for iix=0,n_elements(ixin)-1 do begin
      ix=ixin[iix]
      ip=xfwg.p(ix,iy).par.val
      if ip lt fits.npar then begin
        data=fits.data(xy(0),xy(1),ip)
        valid=fits.valid(xy(0),xy(1),ip)
        parname=fits.short(ip)
      endif else case xfwg.p(ix,iy).par.str of
        'Stokes Par': begin
          parname='Stokes '+(['I','Q','U','V','Ic'])(xfwg.p(ix,iy).stokes)
;          data=xfits_getstokespar(ix,iy,xrange=xy([0,0]),yrange=xy([1,1]), $
;                                  valid=valid,title=title)
          data=xfits_getstokespar(ix,iy,xrange=xyin([0,0]),yrange=xyin([1,1]),$
                                  valid=valid,title=title)
        end
        'User': begin
          data=xfits_userpar(ix,iy,xrange=xy([0,0])+fits.xoff, $
                             yrange=xy([1,1])+fits.yoff, $
                             valid=valid,title=title)
          parname=xfwg.p(ix,iy).user.name
        end
        else: ;print,'Undefined parameter: '+xfwg.p(ix,iy).par.str
      endcase
      pararr(ix,iy*3)=string(parname,format='(a18)')
      parnamout(ix,iy)=strcompress(string(parname,format='(a18)'))
      if abs(data) ge 1e5 then fmt='(e18.4)' else fmt='(f18.4)'
      if valid ne 0 then begin
        parvalout(ix,iy)=data/valid
        padd=string(data/valid,format=fmt) 
      endif else padd=string('NaN',format='(a18)')
      pararr(ix,iy*3+1)=padd
    endfor
    for i=0,2 do begin
      parvec(iy*3+i)=strjoin(pararr(*,iy*3+i))
    endfor
  endfor 
  if keyword_set(nowidget) then return
  parvec=['x='+n2s(xyin(0))+', y='+n2s(xyin(1)),parvec(0:n_elements(parvec)-2)]
  xysz=[max(strlen(parvec)),n_elements(parvec)]
  
  if new_wg then begin
    geom=widget_info(/geometry,xfwg.base.id)
    pwst.base.id=widget_base(group_leader=xpwg.base.id,col=1, $
                             title='Parameters',yoffset=geom.yoffset+geom.ysize)
;    pwst.text.id=widget_text(pwst.base.id,value='--------------------',/scroll)
    pwst.text.id=widget_text(pwst.base.id,value=parvec, $
                             xsize=xysz(0),ysize=xysz(1))
    widget_control,pwst.base.id,/realize
  endif  
  if min(xysz eq pwst.xysz) eq 0 then $
   widget_control,pwst.text.id,xsize=xysz(0),ysize=xysz(1)
  widget_control,pwst.text.id,set_value=parvec
  pwst.xysz=xysz
end

pro xfits_zoom,xdat,ydat,dir=dir,shift=xyshift
  common xpwg,xpwg
  common fits,fits
  
  oldzoom=xpwg.zoomwin.val
  
  if n_elements(xyshift) ne 2 then begin
    fct=1.5
    if dir eq +1 then fct=1./fct
    if total(abs(xpwg.zoomwin.val)) eq 0 then xpwg.zoomwin.val= $
      fix([fits.xoff,fits.yoff,fits.xoff+fits.nx,fits.yoff+fits.ny])
    xfrac=(xdat-xpwg.zoomwin(0).val)/ $
      ((xpwg.zoomwin(2).val-xpwg.zoomwin(0).val)>1)
    yfrac=(ydat-xpwg.zoomwin(1).val)/ $
      ((xpwg.zoomwin(3).val-xpwg.zoomwin(1).val)>1)
    ; dx=fct*0.5*[-1,1]*((xpwg.zoomwin(2).val-xpwg.zoomwin(0).val)>0.5)
    ; dy=fct*0.5*[-1,1]*((xpwg.zoomwin(3).val-xpwg.zoomwin(1).val)>0.5)
    dx=fct*[-xfrac,1-xfrac]*((xpwg.zoomwin(2).val-xpwg.zoomwin(0).val)>0.5)
    dy=fct*[-yfrac,1-yfrac]*((xpwg.zoomwin(3).val-xpwg.zoomwin(1).val)>0.5)
    xpwg.zoomwin([0,2]).val=xdat+dx
    if xpwg.zoomwin(0).val lt fits.xoff then xpwg.zoomwin([0,2]).val= $
      (xpwg.zoomwin([0,2]).val-(xpwg.zoomwin(0).val-fits.xoff))< $
      (fits.xoff+fits.nx-1)
    if xpwg.zoomwin(2).val gt fits.xoff+fits.nx then xpwg.zoomwin([0,2]).val= $
      (xpwg.zoomwin([0,2]).val-(xpwg.zoomwin(2).val-(fits.xoff+fits.nx)))>$
      (fits.xoff)
    xpwg.zoomwin([1,3]).val=ydat+dy
    if xpwg.zoomwin(1).val lt fits.yoff then xpwg.zoomwin([1,3]).val= $
      (xpwg.zoomwin([1,3]).val-(xpwg.zoomwin(1).val-fits.yoff))< $
      (fits.yoff+fits.ny-1)
    if xpwg.zoomwin(3).val gt fits.yoff+fits.ny then xpwg.zoomwin([1,3]).val= $
      (xpwg.zoomwin([1,3]).val-(xpwg.zoomwin(3).val-(fits.yoff+fits.ny)))>$
      (fits.yoff)
    if dir eq -1 then begin
      xpwg.zoomwin(2).val=xpwg.zoomwin(2).val>(xpwg.zoomwin(0).val+2)
      xpwg.zoomwin(3).val=xpwg.zoomwin(3).val>(xpwg.zoomwin(1).val+2)
    endif    
  endif else begin
    xpwg.zoomwin([0,2]).val=xpwg.zoomwin([0,2]).val+xyshift(0)
    xpwg.zoomwin([1,3]).val=xpwg.zoomwin([1,3]).val+xyshift(1)
  endelse
  
  xpwg.zoomwin([0,2]).val=fix(xpwg.zoomwin([0,2]).val>0)<(fits.xoff+fits.nx)
  xpwg.zoomwin([1,3]).val=fix(xpwg.zoomwin([1,3]).val>0)<(fits.yoff+fits.ny)
  for ii=0,3 do $
    widget_control,xpwg.zoomwin(ii).id,set_value=xpwg.zoomwin(ii).val
  if min(fix(oldzoom) eq fix(xpwg.zoomwin.val)) eq 0 then xfits_plotupdate
end

pro xpwg_event,event
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xpswg,xpswg
  common xppwg,xppwg
  common llflag,llflag
  common xfbutton,xyshift0,inshiftmode,autoupdate
  common geom,geom
  
  widget_control,event.id,get_uvalue=uval
  if max(tag_names(event) eq 'X') eq 1 then begin
    eventx=event.x
    eventy=event.y
  endif
  
  case uval of 
    'base': begin
      if (tag_names(event,/structure_name))(0) ne 'WIDGET_TLB_MOVE' then begin
        xpwg.scrollsize=[eventx,eventy]-xpwg.frame
        xpwg.scrollsize=xpwg.scrollsize>200
        xpwg.scrollsize(0)=xpwg.scrollsize(0)>xpwg.width
        xpwg.size=xpwg.scrollsize
        xpwg.fitwidth.val=0
        widget_control,xpwg.fitwidth.id,set_value=xpwg.fitwidth.val
;        for i=0,1 do xpwg.size(i)=xpwg.size(i)>xpwg.scrollsize(i)
;        widget_control,xpwg.draw.id,xsize=xpwg.size(0),ysize=xpwg.size(1), $
        widget_control,xpwg.draw.id, $
          xsize=xpwg.scrollsize(0)-xpwg.sbar, $
          ysize=xpwg.scrollsize(1)-xpwg.sbar, $
          draw_xsize=xpwg.size(0), $
          draw_ysize=xpwg.size(1)
        xfits_plotupdate
      endif
    end
    'control': begin
      case event.value of
        'stokesset': profile_settings
        'refresh': begin
          xfits_plotupdate
        end
        'printall': begin
          xfits_plotupdate,/ps
        end
        'printpage': begin
          xfits_plotupdate,/ps,/singlepage
        end
        else:
      end
    end
    'flag': begin
      xpwg.flagval(event.value)=event.select
      autoupdate=xpwg.flagval(1) eq 1
    end
    'arcsec': xpwg.arcsec.val=event.value
    'charsize': xpwg.charsize.val=event.value
    'smooth': xpwg.smooth.val=event.value
    'pixelscale0': begin
      xpwg.pixelscale[0].val=event.value
    end
    'pixelscale1': begin
      xpwg.pixelscale[1].val=event.value
    end
    'click': begin              ;define click on plot action
      oval=xpwg.click.val
      xpwg.click.val=event.index
;      clickhelp
      if xpwg.click.val eq 2 then begin
        llflag=1b
        print,'Click on lower left corner'
      endif
      if xpwg.click.val eq 4 then begin ;spline select widget
        xfits_splineselect
      endif else  xfits_splineselect,/destroy
    end
    'multipage': begin
      xpwg.multipage.val=(xpwg.multipage.val+1) mod 2
      widget_control,xpwg.multipage.id, $
        set_value=(['Single Plot','All Plots'])(xpwg.multipage.val)
      widget_control,xpwg.pagepm.id,sensitive=xpwg.multipage.val eq 1
      widget_control,xpwg.pageselect.id,set_combobox_select=xpwg.pagepm.val, $
        sensitive=xpwg.multipage.val eq 1
      xfits_plotupdate
    end
    'pageselect': begin
      xpwg.pagepm.val=event.index
      nptot=fix(xfwg.nx.val*xfwg.ny.val)
      xpwg.pagepm.val=(xpwg.pagepm.val+nptot) mod nptot
      if n_elements(xpswg) ne 0 then if xpwg.multipage.val eq 1 then begin
        xpswg.ixy=[fix(xpwg.pagepm.val) mod fix(xfwg.nx.val), $
                   fix(xpwg.pagepm.val)  /  fix(xfwg.nx.val)]
      endif
      xfits_plotupdate    
    end
    'pagepm': begin
      nptot=fix(xfwg.nx.val*xfwg.ny.val)
      add=1
      if xfwg.transpose.val eq 1 then add=xfwg.nx.val
      if event.value eq 'next' then xpwg.pagepm.val=xpwg.pagepm.val+add
      if event.value eq 'prev' then xpwg.pagepm.val=xpwg.pagepm.val-add
      if xfwg.transpose.val eq 1 and xpwg.pagepm.val lt 0 then $
        xpwg.pagepm.val=xpwg.pagepm.val-1
      if xfwg.transpose.val eq 1 and xpwg.pagepm.val ge nptot then $
        xpwg.pagepm.val=xpwg.pagepm.val+1
      xpwg.pagepm.val=(xpwg.pagepm.val+nptot) mod nptot
      if n_elements(xpswg) ne 0 then if xpwg.multipage.val eq 1 then begin
        xpswg.ixy=[fix(xpwg.pagepm.val) mod fix(xfwg.nx.val), $
                   fix(xpwg.pagepm.val)  /  fix(xfwg.nx.val)]
                                ;update plot settings widget in Single
                                ;Plot
        widget_control,xpswg.base.id,bad_id=bad_id    
        if bad_id eq 0 then xfits_plotsetting,xpswg.ixy
      endif
      widget_control,xpwg.pageselect.id,set_combobox_select=xpwg.pagepm.val
      xfits_plotupdate
    end
    'fitwidth': begin
      xpwg.fitwidth.val=event.select
      if xpwg.fitwidth.val eq 1 then begin
        xpwg.size(1)=xpwg.size(1)*xfwg.xy_aspect/1.0
      endif else begin
        xpwg.size=xpwg.scrollsize
      endelse
      widget_control,xpwg.draw.id, $
          xsize=xpwg.scrollsize(0)-xpwg.sbar, $
          ysize=xpwg.scrollsize(1)-xpwg.sbar, $
          draw_xsize=xpwg.size(0), $
        draw_ysize=xpwg.size(1)
      xfits_plotupdate
    end
    'plotsperpage': xpwg.plotsperpage.val=event.index+1
    'zoomwin00': xpwg.zoomwin(0).val=fix(event.value)
    'zoomwin01': xpwg.zoomwin(1).val=fix(event.value)
    'zoomwin02': xpwg.zoomwin(2).val=fix(event.value)
    'zoomwin03': xpwg.zoomwin(3).val=fix(event.value)
    'zoominout': begin
      if total(abs(fix(xpwg.zoomwin.val))) eq 0 then xpwg.zoomwin.val= $
        fix([fits.xoff,fits.yoff,fits.xoff+fits.nx,fits.yoff+fits.ny])
     xfits_zoom,mean(xpwg.zoomwin([0,2]).val),mean(xpwg.zoomwin([1,3]).val), $
        dir=fix(event.value)
    end
    'zoomreset': begin
      xpwg.zoomwin.val= $
        fix([fits.xoff,fits.yoff,fits.xoff+fits.nx,fits.yoff+fits.ny])
      for i=0,3 do widget_control,xpwg.zoomwin(i).id, $
        set_value=fix(xpwg.zoomwin(i).val)
      xfits_plotupdate
    end
    'draw': begin
      tev=tag_names(event)      
      if max(tev eq 'X') ne 0 then begin
        ixy=get_plotidx(eventx,eventy)
        
        
                                ;get mouse position in data coordinates
        ixy=ixy>0
        posdev=xfwg.p(ixy(0),ixy(1)).posdev         
        xrg=xfwg.p(ixy(0),ixy(1)).xrg
        yrg=xfwg.p(ixy(0),ixy(1)).yrg
        if xpwg.flagval[6] eq 0 then begin
          if xpwg.flagval[4] eq 1 then yrg=reverse(yrg)
          if xpwg.flagval[5] eq 1 then xrg=reverse(xrg)
        endif
        if xpwg.flagval[6] eq 1 then begin
          xrg=reverse(xrg)
          if xpwg.flagval[4] eq 1 then xrg=reverse(xrg)
          if xpwg.flagval[5] eq 1 then yrg=reverse(yrg)
        endif
        xscl=(eventx-posdev(0))/(posdev(2)-posdev(0))*(xrg(1)-xrg(0))+xrg(0)
        yscl=(eventy-posdev(1))/(posdev(3)-posdev(1))*(yrg(1)-yrg(0))+yrg(0)
        
        xdat=scaleaxis(/x,xscl,/reverse)
        ydat=scaleaxis(/y,yscl,/reverse)
        
        if xpwg.flagval[6] eq 1 then begin
          dummy=xdat & xdat=ydat & ydat=dummy
        endif
        xdat=(xdat>fits.xoff)<(fits.xoff+fits.nx-1) ;+fits.xoff    ;+xpwg.xoff
        ydat=(ydat>fits.yoff)<(fits.yoff+fits.ny-1) ;+fits.yoff    ;+xpwg.yoff 
        
        if (xscl ge min(xrg) and xscl le max(xrg) and $
            yscl ge min(yrg) and yscl le max(yrg)) then begin        
          device,/cursor_crosshair      
          if xfwg.p(ixy(0),ixy(1)).plotmode eq 0 then begin
          if xpwg.multipage.val eq 0 then begin
            image_cursor,eventx,eventy,szc=10,color=0, $
              x0=xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).posframe(0), $
              x1=xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).posframe(2), $
              y0=xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).posframe(1), $
              y1=xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).posframe(3), $
              show=xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).plotmode eq 0
          endif
                                ;middle mouse button to shift window
          if event.press eq 2 and max(tev eq 'CLICKS') ne 0 then begin
            if event.clicks ne 0 and event.type ne 7 then begin
              xyshift0=[xdat,ydat]
              inshiftmode=1
            endif
          endif else if (event.release eq 2 and $
                         n_elements(xyshift0) eq 2) then begin
            xfits_zoom,xdat,ydat,dir=0,shift=xyshift0-[xdat,ydat] 
            inshiftmode=0
          endif
          if n_elements(inshiftmode) eq 0 then inshiftmode=0
                                ;check mouse wheel for zooming
          if inshiftmode eq 0 then begin
            dir=0
            case event.type of
              7: if max(tev eq 'CLICKS') ne 0 then dir=event.clicks
              1: if event.release eq 8 then dir=1 $
              else if event.release eq 16 then dir=-1
              else:
            endcase
            if dir ne 0 then xfits_zoom,xdat,ydat,dir=dir
          endif
          endif
          doplot=xpwg.flagval(1) eq 1
          psflag=0
          force=0
          xfps_update=0
          if xpwg.click.val eq 4 then begin ;treat spline select mode
            if event.press ne 0 then xss_point,xdat,ydat,event
          endif else case event.press of
            1: begin
              if xfwg.p(ixy(0),ixy(1)).plotmode eq 0 then begin
                case fix(xpwg.click.val) of
                  0: begin
                    doplot=1
                    force=1
                    autoupdate=1
                  end
                  1: begin
                    doplot=1
                    psflag=1
                    force=1
                  end
                  2: begin
                    if n_elements(llflag) eq 0 then llflag=1b
                    if llflag eq 1 then begin
                      llflag=0
                      xpwg.zoomwin(0:1).val=fix([xdat,ydat])
                      print,'Click on upper right corner'
                    endif else begin
                      xpwg.zoomwin(2:3).val=fix([xdat,ydat])
                      llflag=1
                      xfits_plotupdate
                    endelse
                  end
                  3: begin      ;delete this map from list
                    parstr=([fits.short,'User','Stokes Par']) $
                      (xfwg.p(ixy(0),ixy(1)).par.val)
                    yn=dialog_message(/question,dialog_parent=xpwg.base.id, $
                                      'Delete map "'+parstr+'"   Pos: x=' $
                                      +n2s(ixy(0))+',y='+n2s(ixy(1))+'?', $
                                      title='Delete Map')
                    if yn eq 'Yes' then begin
                      ii=ixy(0)+ixy(1)*xfwg.nx.val
                      pval=xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).par.val
                      pstr=xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).par.str
                      if n_elements(pval) ge 2 then begin
                        pval(ii:n_elements(pval)-2)=pval(ii+1:n_elements(pval)-1)
                        pstr(ii:n_elements(pval)-2)=pstr(ii+1:n_elements(pval)-1)
                        pval(n_elements(pval)-1)=0
                        pstr(n_elements(pval)-1)=fits.short(0)
                        for iw=ii,n_elements(pval)-1 do begin
                          iwx=iw mod fix(xfwg.nx.val) & iwy=iw / fix(xfwg.nx.val)
                          widget_control,xfwg.p(iwx,iwy).par.id, $
                            set_combobox_select=pval(iw)
                        endfor
                        xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).par.val=pval
                        xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).par.str=pstr
                        xfits_plotupdate
                      endif
                    endif
                  end
                  else:
                endcase
              endif
            end
            4: begin
              xfits_plotsetting,ixy
            end
            else:
          endcase
          if n_elements(xppwg) ne 0 then begin
            widget_control,xppwg.base.id,bad_id=bid
            if bid eq 0 then $
              if widget_info(xppwg.base.id,/managed) eq 1 then begin
              xppwg.showflags(0)= $
                xfwg.p(ixy(0),ixy(1)).par.str eq 'Stokes Par' or $
                xfwg.p(ixy(0),ixy(1)).par.str eq 'User'
              widget_control,xppwg.flags.id,set_value=xppwg.showflags
            endif
          endif
          
          ifit=(where(fits.par eq 'FITNESS' or fits.par eq 'CHISQ' or $
                      fits.par eq 'Total Chi-Squared'))(0)
          add=fits.par(ifit)+'='+ $
              n2s(fits.data(xdat-fits.xoff,ydat-fits.yoff,ifit), $
                  format='(f10.2)') 
          ip=xfwg.p(ixy(0),ixy(1)).par.val
;          if ip lt fits.npar then add=add+', '+fits.short(ip)+'='+ $
;            n2s(fits.data(xdat-fits.xoff,ydat-fits.yoff,ip),format='(f14.4)')
          xfits_parwindow,fix([xdat,ydat]),/nowidget,ix=ixy[0],iy=ixy[1], $
                          value=parval,name=parnam
          addstat=''
          if n_elements(fits.par) gt ip then if max(fits.pixrep) gt 1 then begin
            istd50=where(fits.par eq fits.par[ip] and $
                       strpos(fits.short,'STD50') ne -1)
            if istd50[0] ne -1 then $
              addstat=' (STD50='+ $
                      n2s(fits.data(xdat-fits.xoff, ydat-fits.yoff,istd50), $
                          format='(f10.2)')+')'
          endif
          add=add+', '+parnam[ixy[0],ixy[1]]+'='+ $
              n2s(parval[ixy[0],ixy[1]],format='(f14.4)')
          xystr=string(fix([xdat,ydat]),format='(''x='',i4.4,'' y='',i4.4)')
          info=addstat[0]+', mean='+n2s(xfwg.p(ixy(0),ixy(1)).info.mean)+ $
               ' RMS='+n2s(xfwg.p(ixy(0),ixy(1)).info.contrast)+'%'
          widget_control,xpwg.xyinfo.id,set_value=xystr+' '+add+info
          if n_elements(autoupdate) eq 0 then autoupdate=0
          if doplot or autoupdate then begin
            xpwg.xy.x=xdat
            xpwg.xy.y=ydat
            xpwg.xy.xoff=fits.xoff
            xpwg.xy.yoff=fits.yoff
            xfits_stokesplot,xdat,ydat,ps=psflag eq 1,force=force
            if max(fits.pixrep) ge 1 and xpwg.flagval[7] eq 1 then $
              xfits_pixelstat,fix([xdat,ydat]),ixy
          endif
          if doplot or autoupdate or xpwg.flagval(2) eq 1 then $
            xfits_parwindow,fix([xdat,ydat])            
        endif else begin
          device,/cursor_standard
          if event.press eq 1 then begin ;move window by dx or dy
            xfwgpdev=xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).posdev
            ixy=where(total(abs(xfwgpdev),1) ge 1e-5)
            if ixy[0] ne -1 then begin
              xfwgpdev= $
                (reform(xfwgpdev,4,xfwg.nx.val*xfwg.ny.val))[*,ixy]

            dx=xpwg.zoomwin(2).val-xpwg.zoomwin(0).val
            dy=xpwg.zoomwin(3).val-xpwg.zoomwin(1).val
            mmdevx=[min(xfwgpdev(0,*)),max(xfwgpdev(2,*))]
            mmdevy=[min(xfwgpdev(1,*)),max(xfwgpdev(3,*))]
            einx=eventx ge mmdevx(0) and eventx le mmdevx(1)
            einy=eventy ge mmdevy(0) and eventy le mmdevy(1)
            if eventx lt mmdevx(0) and einy then $
              xpwg.zoomwin([0,2]).val= $
              xpwg.zoomwin([0,2]).val-(dx<(xpwg.zoomwin(2).val-dx))
            if eventx gt mmdevx(1) and einy then $
              xpwg.zoomwin([0,2]).val= $
              xpwg.zoomwin([0,2]).val+(dx<((fits.nx+fits.xoff-1)- $
                                           xpwg.zoomwin(2).val))
            if eventy lt mmdevy(0) and einx then $
              xpwg.zoomwin([1,3]).val= $
              xpwg.zoomwin([1,3]).val-(dy<(xpwg.zoomwin(3).val-dy))
            if eventy gt mmdevy(1) and einx then $
              xpwg.zoomwin([1,3]).val= $
              xpwg.zoomwin([1,3]).val+(dy<((fits.ny+fits.yoff-1)- $
                                           xpwg.zoomwin(3).val))
            xfits_plotupdate
          endif
          endif
        endelse
        if event.release eq 1 then autoupdate=0
      endif
    end
    else: begin
      print,uval
      help,/st,event    
    end
  end
  geom=widget_info(xpwg.base.id,/geometry)
end

pro xfits_plotwg
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common geom,geom
  
  subst={id:0l,val:0.,str:''}
  oflagval=[0,0,0,0,0,0,0,0]
  xy={x:0,y:0,xoff:0,yoff:0}
  oxpwg={base:subst,control:subst,draw:subst,size:[0,0],frame:[0,0], $
         flag:subst,flagval:oflagval,charsize:subst,smooth:subst, $
         click:subst,clickhelp:subst,zoomwin:replicate(subst,4),xoff:0,yoff:0,$
         arcsec:subst,xyinfo:subst,zoominout:subst,multipage:subst, $
         pagepm:subst,pageselect:subst,width:0,scrollsize:[800,600],sbar:15., $
         fitwidth:subst,pixelscale:replicate(subst,2),plotsperpage:subst, $
         xy:xy}
  oxpwg.plotsperpage.val=1.
  oxpwg.zoomwin.val=[fits.xoff,fits.yoff,fits.xoff+fits.nx,fits.yoff+fits.ny]
  oxpwg.pixelscale.val=fits.pix2arcsec
  if total(abs(oxpwg.pixelscale.val)) lt 1e-4 then oxpwg.pixelscale.val=1.
  oxpwg.multipage.val=1
  if n_elements(xpwg) eq 0 then begin
    xpwg=oxpwg
  endif else xpwg=update_struct(xpwg,oxpwg)
  
  if n_elements(geom) eq 0 then begin
    gbase=widget_info(/geometry,xfwg.base.id)
    xoff=gbase.xoffset+gbase.xsize
    yoff=0
  endif else begin
    xoff=geom.xoffset
    yoff=geom.yoffset
  endelse
  xpwg.base.id=widget_base(title='XFITS Plot - '+fits.file, $
                           col=1,/tlb_size_events, $
                           uval='base',group_leader=xfwg.base.id, $
                           xoffset=xoff,yoffset=yoff, $
                           space=-5,/tlb_move_events)
  sub1=widget_base(xpwg.base.id,row=1,/frame,space=0)
  sub2a=widget_base(xpwg.base.id,row=1,/frame,space=0)
  sub3=widget_base(xpwg.base.id,col=1,/frame,space=0)
  
  xpwg.control.id=cw_bgroup(sub1,[' Refresh ',' Print Single Page ', $
                                  ' Print All '], $
                            uvalue='control',row=1,space=0, $
                            button_uvalue=['refresh','printpage','printall'])
  xpwg.plotsperpage.id=widget_combobox(sub1,uvalue='plotsperpage', $
                                       value=['1 plot/page', $
                                              n2s(indgen(19)+2)+' plots/page'])
  widget_control,xpwg.plotsperpage.id, $
                 set_combobox_select=xpwg.plotsperpage.val-1
  
  lbl=['LLx','LLy','URx','URy']
  sub=widget_base(sub2a,row=1,space=0)
  suba=widget_base(sub,col=2,space=0)
;  lab=widget_label(sub,value='data zoom:')
  for iz=0,3 do begin
    is=n2s(iz,format='(i2.2)')
    xpwg.zoomwin(iz).id=cw_field(suba,uvalue='zoomwin'+is,title=lbl(iz), $
                                 value=fix(xpwg.zoomwin(iz).val), $
                                 /all_events,/integer,xsize=3)
  endfor
  ; xpwg.zoominout.id=cw_bgroup(sub,[' + ',' - '],button_uvalue=['+1','-1'], $
  ;                            uvalue='zoominout',row=1,frame=0)
  suba=widget_base(sub,col=1,space=0)
  subb=widget_base(suba,row=1,space=0)
  dummy=widget_button(subb,value='Full Size',uvalue='zoomreset')
  xpwg.multipage.id=widget_button(subb,uvalue='multipage', $
                                  value=(['Single Plot', $
                                          'All Plots'])(xpwg.multipage.val))
  pagesel=add_parxy()
; if xfwg.transpose.val eq 1 then pagesel=transpose(pagesel)
  xpwg.pageselect.id=widget_combobox(suba,uvalue='pageselect', $
                                     value=pagesel[*],/dynamic_resize)
  widget_control,xpwg.pageselect.id,set_combobox_select=xpwg.pagepm.val, $
    sensitive=xpwg.multipage.val eq 1
  xpwg.pagepm.id=cw_bgroup(suba,['prev page','next page'], $
                           button_uvalue=['prev','next'], $
                           uvalue='pagepm',col=2,frame=0)
  widget_control,xpwg.pagepm.id,sensitive=xpwg.multipage.val eq 1
  
  
  suba=widget_base(sub,col=1,space=0)
  subaa=widget_base(suba,row=1,space=0)
  dummy=widget_label(subaa,value='Left-Click:',/align_left)
  xpwg.click.id=widget_combobox(subaa,uvalue='click', $
                                value=['Plot IQUV','Print IQUV', $
                                       'Zoom','Delete Map','Spline Mark'])
  widget_control,xpwg.click.id,set_combobox_select=xpwg.click.val
;  xpwg.clickhelp.id=widget_label(sub1,value='',/dynamic)
;  clickhelp
  dummy=widget_label(suba,value='Middle-Click: zoom/shift',/align_left)
  dummy=widget_label(suba,value='Right-Click: plot settings',/align_left)
  
  sub=widget_base(sub2a,col=1,space=0)
  allflags=['interp. NAN','auto Stokes','auto Param', $
            'int. Zoom','flip U/D','flip L/R', $
            'rot 90','']
  if max(fits.pixrep) ge 1 then allflags[7]='Stat-window'
  xpwg.flag.id=cw_bgroup(sub,/nonexclusive, $
                         allflags, $
                         set_value=xpwg.flagval,uval='flag',col=2,space=-5)
  sub=widget_base(sub2a,col=1,space=0)
;  if total(abs(fits.pix2arcsec)-1) ne 0 then begin
    xpwg.arcsec.id=cw_bgroup(sub,/exclusive,['pix','arcsec'], $
                             set_value=xpwg.arcsec.val,uval='arcsec',row=1)
;  endif
  xpwg.fitwidth.id=cw_bgroup(sub,uvalue='fitwidth','Fit Width', $
                             set_value=xpwg.fitwidth.val,/nonexclusive,col=1)
  sub=widget_base(sub2a,row=2,space=0)
  if total(abs(xpwg.pixelscale.val)) lt 1e-4 then xpwg.pixelscale.val=1.
  for i=0,1 do begin
    xpwg.pixelscale[i].id=cw_field(sub,xsize=4,/all_events, $
                                   uvalue='pixelscale'+n2s(i), $
                                   title=(['Pix.size: x','y'])(i), $
                                   value=xpwg.pixelscale[i].val)
  endfor
  xpwg.charsize.id=cw_field(sub,xsize=4,/all_events,title='charsize', $
                            uvalue='charsize',value=xpwg.charsize.val, $
                            /floating)
;  xpwg.smooth.id=cw_field(sub,xsize=2,/all_events,title='smooth',/integer, $
;                            uvalue='smooth',value=fix(xpwg.smooth.val))
  xpwg.smooth.val=0
  
  
  sz_sub1=widget_info(sub1,/geometry)
  xpwg.width=sz_sub1.xsize
  xpwg.scrollsize(0)=xpwg.scrollsize(0)>xpwg.width
  
  xpwg.size=xpwg.scrollsize
  
  sub3a=widget_base(sub3,row=1,space=0)
  xpwg.xyinfo.id=widget_label(sub3a,value='-',/align_right,/dynamic)
  xpwg.draw.id=widget_draw(sub3,uvalue='draw', $
                           xsize=xpwg.size(0),ysize=xpwg.size(1), $
                           x_scroll_size=xpwg.scrollsize(0)-xpwg.sbar, $
                           y_scroll_size=xpwg.scrollsize(1)-xpwg.sbar,$
                           /motion_events,/track,/view,/button_events, $
                           /wheel_events)  
  widget_control,xpwg.base.id,/realize
  xmanager,'xpwg',xpwg.base.id,no_block=1
  
  geom=widget_info(xpwg.base.id,/geometry)
  xpwg.frame=[geom.xsize-xpwg.size(0),geom.ysize-xpwg.size(1)]
end


pro xfits_plot,close=close
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  
  if n_elements(xpwg) ne 0 then begin
    widget_control,xpwg.base.id,bad_id=bad_id
    if bad_id eq 0 then begin
      if keyword_set(close) then widget_control,xpwg.base.id,/destroy
      managed=widget_info(xpwg.base.id,/managed)
      if managed then return
    endif
  endif
  
  xfits_splineselect,/init
  xfits_plotwg
  xfits_plotupdate
end
