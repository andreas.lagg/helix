;widget for displaying fits inverions data
pro xw_parupdate
  common xfwg,xfwg
  common xpwg,xpwg
  
  if n_elements(xpwg) ne 0 then $
    widget_control,xpwg.pageselect.id,bad_id=bad_id, $
    set_value=add_parxy(),set_combobox_select=xpwg.pagepm.val
  
end

pro xw_parmodeset
  common xfwg,xfwg
  common fits,fits
  
  select=['PR0','BEST','AVG','AVG50','STD','STD50']
  for is=0,n_elements(select)-1 do $
    for ix=0,xfwg.nx.val-1 do for iy=0,xfwg.ny.val-1 do begin
    splt=strsplit(/extract,/regex,xfwg.p(ix,iy).par.str,select(is))
    if splt(0) ne xfwg.p(ix,iy).par.str then begin
      xfwg.p(ix,iy).par.str=splt(0)+select(xfwg.parmode.val)
      xfwg.p(ix,iy).par.val=where(fits.short eq xfwg.p(ix,iy).par.str)
      widget_control,xfwg.p(ix,iy).par.id,bad_id=bad_id, $
        set_combobox_select=xfwg.p(ix,iy).par.val
    endif
  endfor
end

pro setnewobs,obsfile
  common xfwg,xfwg
  common fits,fits
  
  fits.obsfile=obsfile
  xfwg.obs.str=obsfile(0)
  if fits.code eq 'helix' then begin
    slpos=strpos(/reverse_search,fits.obsfile,'/')
    fits.ipt.dir.profile=strmid(fits.obsfile,0,slpos+1)
    fits.ipt.observation=strmid(fits.obsfile,slpos+1,strlen(fits.obsfile))
  endif
  if strcompress(xfwg.obs.str) eq '' then for i=0,n_elements(xfwg.p)-1 do begin
    if xfwg.p(i).obsfit eq 0 then xfwg.p(i).obsfit=1
    
  endfor
  
end

pro setnewsynth,synthfile
  common xfwg,xfwg
  common fits,fits
  
  fits.synthfile=synthfile
  xfwg.synth.str=synthfile(0)
end

function cutlongstr,str
  
  maxlen=40
  if strlen(str) ge maxlen then $
    return,'... '+strmid(str,strlen(str)-40,strlen(str)) $
  else return,str
  
end

pro xfits_event,event
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xpswg,xpswg
  common xdir,xdir
  common xpprof,xpprof
  common xppwg,xppwg
  common xfsswg,xfsswg
  common xss,xss
  common old,oldfile
  
  widget_control,event.id,get_uvalue=uval
  if strmid(uval,0,3) eq 'pwg' then begin
    splt=strsplit(/extract,uval,'.')
    uval=splt(1)
    ix=fix(splt(2))
    iy=fix(splt(3))
  endif
  
    widget_control,xfwg.base.id,hourglass=1
  update_wg=0
  update_plot=0
  case uval of 
    'control': begin
      case event.value of
        'exit': begin
;          xfits_plot,/close
          widget_control,xfwg.base.id,/destroy
          reset
        end
        'reset': begin
          widget_control,xfwg.base.id,/destroy
          if n_elements(oldfile) ne 0 then oldsav=oldfile
          xfits_clearall
          if n_elements(oldsav) ne 0 then xfits,oldsav
;          xfits_widget
;          xfits_plot
        end
        'save': begin
          savfile=(strsplit(/regex,/extract,fits.file,'.fits'))(0)+'.xfits.sav'
          slpos=strsplit(savfile,'/',count=cnt)
          if cnt ge 2 then dir=strmid(savfile,0,slpos(cnt-1)) else dir='./'
          dir=xdir.save
          savfile=strmid(savfile,slpos(cnt-1),strlen(savfile))
          if xfwg.savfile ne '' then savfile=xfwg.savfile
          cd,dir,current=odir
          repeat begin
            error=0
            fsav=dialog_pickfile(dialog_parent=xfwg.base.id,/write, $
                                    default_extension='.xfits.sav', $
                                    file=savfile,/overwrite_prompt)
            cd,odir
            if fsav ne '' then begin
              openw,unit,/get_lun,/delete,fsav,error=error
              if error eq 0 then begin
                free_lun,unit
                fitsfile=fits.file
                save,/compress,/xdr,file=fsav,xfwg,xpwg,xpswg,fitsfile, $
                  xppwg,xpprof,xfsswg,xss, $
                     description='XFITS Parameter file for '+fits.file
                xfwg.savfile=fsav
                print,'Saved plot layout to '+fsav
              endif else begin
                print,'Cannot write to '+dir
              endelse
            endif
          endrep until error eq 0
        end
        'load': begin
          dir=xdir.save
          cd,dir,current=odir
          savfile=dialog_pickfile(filter='*.xfits.sav',/read, $
                                  dialog_parent=xfwg.base.id, $
                                  title='Load data set')
          cd,odir
          if savfile ne '' then begin
            widget_control,xfwg.base.id,/destroy
            print,'Loading new data set and settings from '+savfile
            xfits,savfile,/update
            reset
          endif
        end
        'loadset': begin
          dir=xdir.save
          cd,dir,current=odir
          savfile=dialog_pickfile(filter='*.xfits.sav',/read, $
                                  dialog_parent=xfwg.base.id, $
                                  title='Load plot settings')
          cd,odir
          if savfile ne '' then begin
            widget_control,xfwg.base.id,/destroy
            print,'Loading only plot settings from '+savfile
            xfits,savfile,/update,/only_settings
            reset
          endif
        end
        else:
      end
    end
    'nx': begin
      oval=xfwg.nx.val
      xfwg.nx.val=event.index+1
      if oval ne xfwg.nx.val then update_wg=1
    end
    'ny': begin
      oval=xfwg.ny.val
      xfwg.ny.val=event.index+1
      if oval ne xfwg.ny.val then update_wg=1
    end
    'parmode': begin
      oval=xfwg.parmode.val
      xfwg.parmode.val=event.index
      xfwg.parmode.str=event.str
      if oval ne xfwg.parmode.val then begin
        xw_parmodeset
        update_wg=1
      endif
      widget_control,xfwg.synth.id,set_value='Write out FITS-files ' + $
                     'containing '''+xfwg.parmode.str+''''
    end
    'transpose': begin
      xfwg.transpose.val=not xfwg.transpose.val
      dummy=xfwg.nx.val & xfwg.nx.val=xfwg.ny.val & xfwg.ny.val=dummy
      px=xfwg.p
      for ix=0,xfwg.nx.val-1 do for iy=0,xfwg.ny.val-1 do $
        xfwg.p(ix,iy)=px(iy,ix)
      update_wg=1
    end
    'defselect': begin
      xfwg.defselect.val=event.index
      xfits_defaultlayout,index=xfwg.defselect.val
     update_wg=1
    end
    'default': begin
      xfwg.defaultlayout.val=event.index
      xfits_defaultlayout
      update_wg=1
    end
    'writeout': begin
      yn=dialog_message(/question,dialog_parent=xfwg.base.id, $
                        ['Write out fits files containing only',''''+ $
                         xfwg.parmodestring(xfwg.parmode.val)+''' pixels?'], $
                        title='Write FITS files')
      if yn eq 'Yes' then begin
        xfits_writebest
      endif
    end
    'par': begin
      oval=xfwg.p(ix,iy).par.val
      xfwg.p(ix,iy).par.val=event.index
      xfwg.p(ix,iy).par.str=event.str
      if oval ne xfwg.p(ix,iy).par.val then update_plot=1
      xw_parupdate
    end
    'obs': begin
      obsfile=dialog_pickfile(/read,dialog_parent=xfwg.base.id, $
                              title='Select observation')
      if obsfile ne '' then begin
        setnewobs,obsfile
        widget_control,xfwg.obs.id,set_value=cutlongstr(fits.obsfile)
      endif
    end
    'synth': begin
      fi=get_filepath(fits.obsfile)
      synthfile=dialog_pickfile(/read,dialog_parent=xfwg.base.id,path=fi.path, $
                                title='Select synthetic profiles')
      if synthfile ne '' then begin
        setnewsynth,synthfile
        widget_control,xfwg.synth.id,set_value=cutlongstr(fits.synthfile)
      endif
    end
    else: help,/st,event
  end
  
  if update_wg then begin
    xfits_widget,/update
    update_plot=1
  endif
  if update_plot or n_elements(xpwg) eq 0 then begin
    xfits_plot
  endif
  widget_control,xfwg.base.id,hourglass=0
end


pro xfits_widget,update=update,only_settings=only_settings
  common xfwg,xfwg
  common fits,fits
  common xf_group,xf_group
  
  if n_tags(xfwg) gt 0 then begin
    widget_control,bad_id=bid,xfwg.base.id,/destroy
  endif
  
  pmax=20                       ;max number of plots in x and y direction
  
  subst={id:0l,val:0.,str:''}
  user={oper:'',title:'',unit:'',name:'',colortable:''}
  histo={nbins:30,xrange:fltarr(2),norm:0}
  scatter={par:0,str:'',xrange:fltarr(2),yrange:fltarr(2),zrange:fltarr(2), $
           color:0,nbins:200,hnorm:0,xlog:0,ylog:0}
  img={smooth_apply:0b,smooth:3, $
       unsharp:[1,3.],unsharp_only:1,unsharp_apply:0}
  info={contrast:0.,mean:0.}
  azipar={flag:0,length:1.,scale:0,color:9,thick:1,comp:0,bin:2,ambi:0}
  azi={rotsens:0b,offset:0.}
  contpar={index:-1,smooth:0,col:9,thick:1,str:'',val:''}
  condst={mm:fltarr(2),par:-1,id:0l,layer:'',str:''}
  cond=replicate(condst,3)
  cond.layer=['map','contour','AZI-vec']
  pwg={par:subst,posdev:fltarr(4),posframe:fltarr(4),range:fltarr(2), $
       xrg:fltarr(2),yrg:fltarr(2),contpar:contpar, $
       stokes:0,obsfit:0,wlrg:dblarr(2), $
       coltab:subst,ic:subst,user:user,plotmode:0,absval:subst, $
       histo:histo,scatter:scatter,shorttitle:0b,zlog:0,condspline:0, $
       conditionstr:'',img:replicate(img,4),info:info,maxval:subst, $
       range_current:fltarr(2),azipar:azipar,cond:cond};, $
;       condlayer:[1,1,1],condpar:-1,condmm:fltarr(2)}   ; $
;       imgsmooth_apply:bytarr(4)+1b,imgsmooth:0, $
;       imgunsharp:[0.,3.],imgunsharp_only:1,imgunsharp_apply:bytarr(4)+1b}
  
  oxfwg={base:subst,control:subst,nx:subst,ny:subst,obs:subst,synth:subst, $
         p:replicate(pwg,pmax,pmax),defaultlayout:subst,parmode:subst, $
         transpose:subst,writeout:subst,defselect:subst,savfile:'', $
         parmodestring:strarr(6),parmodeshort:strarr(6),xy_aspect:1.,azi:azi}
  oxfwg.nx.val=1 & oxfwg.ny.val=1
  oxfwg.defaultlayout.val=1
  
  ; if max(fits.comp) ge 2 then begin
  ;   for ix=0,max(fits.comp)-1 do for iy=0,pmax-1 do $
  ;     oxfwg.p(ix,iy).par.val=(iy+min(where(fits.comp eq ix+1)))>0
                                ; endif
  ishow=where(fits.show eq 1)>0
  ns=n_elements(ishow)
  nsq=(fix(sqrt(ns))+1)<(pmax-1)
  oxfwg.p(0:nsq-1,0:nsq-1).par.val= $
    reform(([ishow,ishow,ishow,ishow])(0:nsq*nsq-1),nsq,nsq)
  todefault=0
  if (keyword_set(update) eq 0 or n_elements(xfwg) eq 0) and $
    (keyword_set(only_settings) eq 0) then begin    
    oxfwg.obs.str=fits.obsfile(0)
    oxfwg.synth.str=fits.synthfile(0)
    xfwg=temporary(oxfwg)
    todefault=1
  endif else begin
    xfwg=update_struct(xfwg,oxfwg)
  endelse
  xfwg.parmode.id=-1
  
  xfwg.base.id=widget_base(title='XFITS Analyzer - '+fits.file, $
                           /col,space=0)
  sub1=widget_base(xfwg.base.id,col=1,/frame)
  sub1a=widget_base(xfwg.base.id,col=1,/frame)
  sub2=widget_base(xfwg.base.id,col=1,/frame)
  sub2a=widget_base(sub2,row=1)
  sub3=widget_base(xfwg.base.id,row=1,/frame)
  
  
  xfwg.control.id=cw_bgroup(sub1,[' Save ',' Load ',' Load Settings ', $
                                  ' Reset ',' Exit '], $
                            uvalue='control',row=1,space=0, $
                            button_uvalue=['save','load','loadset', $
                                           'reset','exit'])
  
  if fits.code eq 'helix' then begin
    lab=widget_label(sub1a,value='Stokes Observed Profiles: ',/align_left)
    xfwg.obs.id=widget_button(sub1a,uvalue='obs', $
                              value=cutlongstr(xfwg.obs.str),/dyn)
;  if xfwg.obs.str ne '' then setnewobs,xfwg.obs.str
    filall=get_helixfitsname(fits.file,/all)
  endif else filall=get_spinorfitsname(fits.file,/all)
  xfwg.synth.str=fits.synthfile(0)
  setnewobs,fits.obsfile
  lab=widget_label(sub1a,value='Used FITS files:',/align_left)
  filshow=filall
  if n_elements(filshow) ge 4 then $
    filshow=[filshow[0],'  ...',filshow[n_elements(filshow)-1]]
  for i=0,n_elements(filshow)-1 do begin
    fs=filshow[i]
    fs=(reverse(strsplit(fs,'/',/extract)))(0)
    lab=widget_label(sub1a,value='   '+fs,/align_left)
  endfor
  
                                ;synth file must follow filename convention.
                                ;--> disable user input
  if 1 eq 0 then begin
    lab=widget_label(sub1a,value='Stokes Synthetic Profiles: ',/align_left)
;  if xfwg.synth.str eq '' then $
    xfwg.synth.str=fits.synthfile(0)
    xfwg.synth.id=widget_button(sub1a,uvalue='synth', $
                                value=cutlongstr(xfwg.synth.str),/dyn)
  endif
  
  lab=widget_label(sub2a,value='# of plots: ')
  xfwg.nx.id=widget_combobox(sub2a,uvalue='nx',value='x='+n2s(indgen(pmax)+1))
  widget_control,xfwg.nx.id,set_combobox_select=xfwg.nx.val-1
  xfwg.ny.id=widget_combobox(sub2a,uvalue='ny',value='y='+n2s(indgen(pmax)+1))
  widget_control,xfwg.ny.id,set_combobox_select=xfwg.ny.val-1
  if max(fits.pixrep) ge 1 then begin
    sub2a=widget_base(sub2,row=1)
    sub2b=widget_base(sub2,col=1)
    xfwg.parmodestring=['normal','best fitness','mean value', $
                        'mean (best 50%)','STDEV','STDEV (best 50%)']
    xfwg.parmodeshort=['normal','best','mean', $
                        'mean_b50','stdev','stdev_b50']
    parmode=xfwg.parmodestring
    parmode(0)='PixRep: '+parmode(0)
    xfwg.parmode.id=widget_combobox(sub2a,uvalue='parmode',value=parmode)
    xfwg.parmode.str=parmode(xfwg.parmode.val)
    widget_control,xfwg.parmode.id,set_combobox_select=xfwg.parmode.val
    xfwg.synth.id=widget_button(sub2b,uvalue='writeout', $
                                value='Write out FITS-files ' + $
                                'containing '''+xfwg.parmode.str+'''')
  endif
  if todefault eq 1 then xfits_defaultlayout
;  xfwg.defaultlayout.id=widget_button(sub2a,value='default',uvalue='default')
  xfwg.defaultlayout.id=widget_combobox(sub2a,value= $
                                        ['default','no Stokes','B & VLOS'], $
                                        uvalue='default')
  widget_control,xfwg.defaultlayout.id, $
                 set_combobox_select=xfwg.defaultlayout.val
  xfwg.transpose.id=widget_button(sub2a,value='transpose',uvalue='transpose')
  xfits_pargroups
  xfwg.defselect.id=widget_combobox(sub2a,uvalue='defselect', $
                                    value=xf_group.name)
  widget_control,xfwg.defselect.id,set_combobox_select=xfwg.defselect.val
  
  psub=widget_base(sub3,col=xfwg.nx.val)
  addpar=['User','Stokes Par']
  parstr=[fits.short,addpar]
  for ix=0,xfwg.nx.val-1 do begin
    for iy=0,xfwg.ny.val-1 do begin
      frm=psub    
      
      xy=string(ix,format='(i3.2)')+'.'+string(iy,format='(i3.2)')
        xfwg.p(ix,iy).par.id=widget_combobox(frm,uvalue='pwg.par.'+xy, $
                                             value=parstr)
         widget_control,xfwg.p(ix,iy).par.id, $
           set_combobox_select=xfwg.p(ix,iy).par.val
        xfwg.p(ix,iy).par.str= $
          parstr(xfwg.p(ix,iy).par.val<(n_elements(parstr)-1))
    endfor
  endfor
  
  widget_control,/realize,xfwg.base.id
  xmanager,'xfits',xfwg.base.id,no_block=1
  
  widget_control,xfwg.base.id
end
