pro load16,reset=reset,verbose=verbose
  common oldclt,oldclt
  
  verbose=keyword_set(verbose)
  if keyword_set(reset) and n_elements(oldclt) ne 0 then begin
    if verbose then print,'Reset to old color table'
    tvlct,oldclt.r,oldclt.g,oldclt.b
    return
  endif
  
  tvlct,/get,red,green,blue
  clt={r:red,g:green,b:blue}
  if n_elements(oldclt) eq 0 then oldclt={r:red,g:green,b:blue}
  
  clt.r(1:8)= [255,0  ,0  ,255,0  ,255,255,100]
  clt.g(1:8)= [0  ,255,0  ,0  ,255,200,127,100]
  clt.b(1:8)= [0  ,0  ,255,255,255, 50,127,100]
  clt.r(9:16)=clt.r(1:8)/3*2
  clt.g(9:16)=clt.g(1:8)/3*2
  clt.b(9:16)=clt.b(1:8)/3*2
  clt.r(16)=220 & clt.g(16)=220 & clt.b(16)=220
  clt.r(9)=0 & clt.g(9)=0 & clt.b(9)=0 ;black foreground color
  clt.r(17)=255 & clt.g(17)=255 & clt.b(17)=0
  clt.r(18)=255 & clt.g(18)=255 & clt.b(18)=255
  nc=18
  
  if min(red(1:nc)   eq clt.r(1:nc) and $
         green(1:nc) eq clt.g(1:nc) and $
         blue(1:nc)  eq clt.b(1:nc)) eq 0 then $
    oldclt={r:red,g:green,b:blue}
  
  
  defsysv,'!col',exists=exist
  if exist eq 0 then begin
    c={red:1,green:2,blue:3,magenta:4,cyan:5,orange:6,pink:7,gray:8,black:9, $
       darkgreen:10,darkblue:11,darkmagenta:12,darkcyan:13,brown:14, $
       lightred:15,lightgray:16,yellow:17,white:18}
    defsysv,'!col',c
  endif
  
  if verbose then print,'Loading 16 discrete colors into indices 1-16'
  tvlct,clt.r,clt.g,clt.b
end
