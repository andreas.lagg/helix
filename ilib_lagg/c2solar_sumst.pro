function c2solar_sumst,sum,img=img,solar=solar,soho=soho
  
  restore,sum,/verbose
  
;  b0vec=[-2.4307, -2.3158, -2.2002, -2.0842]
  dat=['050517','050518','050519','050520']
  dat_cds=['17-may-05','18-may-05','19-may-05','20-may-05']
  for i=0,3 do if strpos(sum,dat(i)) ne -1 then $
    eph=pb0r(dat_cds(i), /soho,/arcsec)
  if n_elements(eph) eq 0 then message,'unknown date for sumer obs'
  r0=eph(2)
  b0=eph(1)
  p0=eph(0)
  
  print,'Use B0-Angle: ',b0,'  Radius: ',r0
  
  add=''
  case 1 of
    strpos(sum,'Mg_X') ge 0: add='MG10'
    strpos(sum,'C_II') ge 0: add='C2_A'
    strpos(sum,'C_III') ge 0: add='C3_A'
    strpos(sum,'C_IV') ge 0: add='C4_1'
    strpos(sum,'Ne_VIII') ge 0: add='NE8'
    strpos(sum,'O_V') ge 0: add='O5'
    strpos(sum,'Si_II') ge 0: add='SI2'
    else: begin
      xx=sol_x & yy=sol_y
      img=transpose(total(datall,1))
    end
  endcase
  if add ne '' then begin
    dummy=execute('xx=X_'+add)
    dummy=execute('yy=Y_'+add)
    dummy=execute('tt=T_'+add)
    img=int
  endif

  xarr=transpose(yy*0+1.) ## xx
  yarr=transpose(yy)      ## (xx*0+1.)
  nrx=n_elements(xx)
  nry=n_elements(yy)
  
  xyssw=transpose([[xarr(*)],[yarr(*)]])
  lonlat=xy2lonlat(xyssw,b0=b0*!dtor,radius=r0)
  
                                ;calculate xy in solar coords for
                                ;standard values of b0=0. and r0=950.
  soho=keyword_set(soho)
  solar=keyword_set(solar)
  if soho eq 0 and solar eq 0 then solar=1
  if solar then begin
    print,'Converting to true solar coords (B0=0, R=950'')'   
    xystd=lonlat2xy(lonlat,b0=0.,radius=950.)
  endif
  if soho then begin
    print,'Using SOHO coordinates'   
    xystd=xyssw
  endif
  
  xstd=reform(xystd(0,*),nrx,nry)
  ystd=reform(xystd(1,*),nrx,nry)
  lon=reform(lonlat(0,*),nrx,nry)
  lat=reform(lonlat(1,*),nrx,nry)
  
  time=strcompress(strsec([min(tt),max(tt)]),/remove_all)
  date=strcompress(strmid(sum,13,6),/remove_all)
  
  if n_elements(readme) eq 0 then begin
    print,'No header info for '+sum
    readme=''
  endif
  st={nx:nrx,ny:nry,xstd:xstd,ystd:ystd,lon:lon,lat:lat, $
      header:readme,file:sum,time:time,date:date}
  
  
                                ;treat zero pixels as bad data
  zero=where(img eq 0)
  if zero(0) ne -1 then img(zero)=!values.f_nan
  
  return,st
end

