;search for file in different places and open it
;close - immediately close file again
;retfile - return filename of opened file
function file_open,file,error=error,path=path,verbose=verbose, $
                   close=close,retfile=retfile
  
  retfile=''
  error=1
  unit=-1
  if n_elements(verbose) eq 0 then verbose=0
  np=n_elements(path)
  ip=-1
  ioerr=0
  repeat begin
    ip=ip+1
    files=file_search(path(ip)+'/'+file,count=cnt,/test_regular)
                                ;open first file matching the filename
    if cnt ne 0 then begin
      if cnt ge 2 then begin
        if verbose ge 1 then begin
          message,/cont,'Found more than one file matching'+path(ip)+'/'+file
          print,files
          message,/cont,'opening '+files(0)
        endif
      endif
      error=0
      openr,unit,/get_lun,files(0),error=ioerr 
      if ioerr eq 0 then begin
        retfile=files(0)
        if keyword_set(close) then free_lun,unit
      endif
    endif else error=1
  endrep until error eq 0 or ip eq np-1
  
  if verbose ge 1 then begin
    if ioerr eq 1 then print,'Error opening '+files(0) $
    else if error eq 1 then  $
      print,'File '+file+' not found in '+add_comma(path,sep=', ') $
    else begin
      if verbose ge 1 then print,'Opening file '+files(0)
    endelse
  endif

  return,unit
end
