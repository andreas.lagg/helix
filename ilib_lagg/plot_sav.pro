pro plot_sav,sav,ps=ps
  common line,line,cl_idx
  common wgst,wgst
  
  if n_elements(wgst) ne 0 then if !d.name eq 'X' then $
    widget_control,wgst.plot.id,get_value=widx
  
  ns=n_elements(sav)
  x=strarr(ns)
  y=strarr(ns)
  for i=0,ns-1 do begin
    restore,sav(i),/v
    dummy=execute('p'+n2s(i)+'=profile')
    x(i)=xstr
    y(i)=ystr
  endfor
  
  if n_elements(line) eq 0 then line=line_list()
  
  profile_settings
  file='./ps/profiles.ps'
  psset,ps=ps,file=file
  dummy=execute('plot_profiles,/icont,line=line,charsize=!p.charsize,' $
                +add_comma(sep=',','p'+n2s(indgen(ns))))
  
  for i=0,ns-1 do begin
    if i eq 0 then color=9 else color=i
    xyouts,0,.03,/normal,strjoin(replicate('!C',i+1))+ $
      '----  '+'x'+x(i)+'y'+y(i),color=color,charsize=!p.charsize*.7
  endfor
  
  if n_elements(wgst) ne 0 then if !d.name eq 'X' then wset,widx
  
  psset,/close
end
