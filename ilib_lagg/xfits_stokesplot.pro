;procedure to plot stokes profiles for a given pixel (observed + fitted)
pro xfits_stokesplot,xdatin,ydatin,ps=ps,force=force,ixy=ixy
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xppwg,xppwg
  common xpprof,xpprof
  common xpswg,xpswg
  common xyold,xyold
  common xdir,xdir
  @comm_comp.pro
 
  xdat=((fix(xdatin)>fits.xoff)<(fits.nx+fits.xoff-1))
  ydat=((fix(ydatin)>fits.yoff)<(fits.ny+fits.yoff-1))
  
  doplot=keyword_set(force)
  if n_elements(xyold) eq 0 then doplot=1 $
  else begin
    doplot=doplot or max(xyold ne [xdat,ydat])
  endelse
;  if min(fits.valid(xdat-fits.xoff,ydat-fits.yoff,*)) eq 0 then doplot=0
  if doplot eq 0 then return
  
  xyold=[xdat,ydat]

  
  fcore=(reverse(strsplit(fits.file,/extract,'/')))(0)
  xystr=string([xdat,ydat],format='(''x'',i4.4,''y'',i4.4)')
  psfile=(strsplit(fcore,/extract,/regex,'.fits'))(0)+'.'+xystr+'.ps'
  psfile=xdir.ps+psfile
  
  error=0
  plotdone=0
  pradd=''
  case fits.code of
    'helix': begin
        tipt=fits.ipt
        tipt.ncalls=0
                                ;get atmosphere of pixel x,y to input
                                ;file structure
        tipt.display_profile=1
        tipt.verbose=0
        
      if fits.synthfile eq '' then begin
;        fits_compute_helix,x=xdat-fits.xoff,y=ydat-fits.yoff, $
        fits_compute_helix,x=xdat,y=ydat, $
          fitprof=fdat1,obsprof=fdat0
        
        ; par=reform(fits.data(xdat-fits.xoff,ydat-fits.yoff,*))
        ; pushd,'.'
        ; cd,xdir.helix
        ; if keyword_set(ps) eq 0 then dummy=temporary(psfile)
        ; helix,struct_ipt=tipt,x=xdat,y=ydat,fitprof=fitprof,obsprof=obsprof, $
        ;   /noresult,parin=par,ps=psfile,group_leader=xpwg.base.id
        ; popd
                                ; plotdone=1
      endif 
      if fits.synthfile ne '' then begin
        if n_elements(xpswg) ne 0 then begin
          pixelrep= $
            fits.pixrep(long(xfwg.p(xpswg.ixy(0),xpswg.ixy(1)).par.val) mod $
                        fits.npar);>0
          pradd=', PR'+n2s(pixelrep)
        endif else pixelrep=fits.pixrep(long(xfwg.p(0,0).par.val) mod $
                                        fits.npar)
        read_fitsstokes,fits.file,fdat1,/iquv,fitstype=1,error=error1, $
          x=fix(xdat-fits.xoff)/fix(tipt.stepx)*tipt.stepx, $
          y=fix(ydat-fits.yoff)/fix(tipt.stepy)*tipt.stepy, $
          pixelrep=pixelrep,reread=reread_ret
        label=['observed','fitted']
        fdat0=read_observation(tipt,x=xdat,y=ydat, $
                               dir='',observation=fits.obsfile, $
                               icont=icont,error=error2) ;,/original)
        error=error1 or error2
;        if error2 eq 0 then fdat0.i=fdat0.i/fdat0.ic
        
                                ;compute helix profile from parameters
                                ;stored in atm file
;        calchelix=reread_ret eq 1 or n_elements(xppwg) eq 0 or $
;                  n_elements(xpprof) eq 0
        calchelix=n_elements(xppwg) eq 0 or n_elements(xpprof) eq 0
        if n_elements(xppwg) ne 0 and n_elements(xpprof) ne 0 then begin
          widget_control,xppwg.base.id,bad_id=bad_id
          calchelix=calchelix or bad_id ne 0
          nprof=max(where(xpprof.nwl ge 2))+2
          show=xppwg.profsel(0:nprof-1)
          if nprof ge 2 and n_elements(show) ge 3 then $
            calchelix=calchelix or max(show(2:nprof-1)) ge 1
        endif
        if max(calchelix) then begin
;          fits_compute_helix,x=xdat-fits.xoff,y=ydat-fits.yoff,fitprof=fdat2
          fits_compute_helix,x=xdat,y=ydat,fitprof=fdat2
;          fdat2.i=fdat2.i/cprof_icont
          label=[label,'HeLIx+']
          for ii=1,19 do begin
            dummy=execute('ncp=n_elements(cprof'+n2s(ii-1)+')')
            if ncp ne 0 then begin
              dummy=execute('fdat'+n2s(ii+2)+'=cprof'+n2s(ii-1))
;     dummy=execute('fdat'+n2s(ii+2)+'.i=fdat'+n2s(ii+2)+'.i/cprof_icont')
              
              label=[label,'comp'+n2s(ii)]
            endif
          endfor
        endif
      endif
    end
    'spinor': begin
      is=0
      lbl=['observed','fitted','deconvolved']      
      label=''
      terror=bytarr(3)
      for it=0,2 do begin
        read_fitsstokes,fits.file,fdat,/iquv,fitstype=it,error=thiserror, $
          x=xdat-fits.xoff,y=ydat-fits.yoff
        terror(it)=thiserror
        if terror(it) eq 0 then begin
          dummy=execute('fdat'+n2s(is)+'=fdat')
          label=[label,lbl(it)]
          is=is+1
        endif
      endfor
      error=min(terror)
      if is ge 1 then label=label(1:*)
    end
    else:
  endcase
  
;  if plotdone eq 0 then if error eq 0 then begin
  if plotdone eq 0 then begin
    title='x='+n2s(fix(xdat))+' y='+n2s(fix(ydat))
    xyinfo={x:xdat,y:ydat,xoff:fits.xoff,yoff:fits.yoff}
    cancel=0
    if keyword_set(ps) then $
      psset,ps=ps,file=psfile,size=[18.,26.],win_nr=10, $
      group_leader=xpwg.base.id,/modal,cancel=cancel
    if cancel eq 0 then begin
      psym=intarr(20)
      for j=0,19 do begin
        dummy=execute('ntags=n_tags(fdat'+n2s(j)+')')
        if ntags ge 5 then begin ;only then the profile is valid
          dummy=execute('nprof=n_elements(fdat'+n2s(j)+')')
          if nprof eq 1 then begin
            dummy=execute('nwl=n_elements(fdat'+n2s(j)+'.wl)')
            psym(j)=(nwl gt 10) -1
          endif
        endif
      endfor
      xfits_plotprof,fdat0,fdat1,fdat2,fdat3,fdat4,fdat5, $
        fdat6,fdat7,fdat8,fdat9,fdat10,fdat11,fdat12,fdat13,fdat14,fdat15, $
        fdat16,fdat17,fdat18,fdat19, $
        title=title+pradd,file=psfile,ps=ps,label=label
    endif
  endif
  
  
  widget_control,xpwg.draw.id,get_value=windex
  wset,windex

end
