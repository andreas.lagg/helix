pro cmwg_event,event
  common cmst,cmst,cm_buttons,cm_retst
  
  widget_control,event.id,get_uvalue=uval
  case uval of 
    'buttons': begin
      cm_retst.button=event.value
      widget_control,/destroy,cmst.base.id
    end
    else: help,/st,event
  endcase
  
end

;creates a widget with user defined buttons
function context_menu,buttons=buttons,group_leader=group_leader
  common cmst,cmst,cm_buttons,cm_retst
  
  if n_elements(cmst) gt 0 then begin
    widget_control,bad_id=bid,cmst.base.id
    if bid eq 0 then if cmst.base.id ne 0 then $
      widget_control,/destroy,cmst.base.id
    dummy=temporary(cmst)
  endif
  
    
  widget_control,group_leader,sensitive=0,tlb_get_size=xy
  subst={id:0l,val:0.,str:''}
  cmst={base:subst,buttons:subst}
  
  cmst.base.id=widget_base(group_leader=group_leader,title='Action', $
                           /ALIGN_CENTER,xoffset=xy(0)/2,yoffset=xy(1)/2)
  cmst.buttons.id=cw_bgroup(cmst.base.id,'  '+buttons+'  ', $
                            uvalue='buttons',col=1,space=0, $
                            button_uvalue=buttons)
  cm_buttons=buttons
  cm_retst={button:''}
  
  widget_control,/realize,cmst.base.id
  xmanager,'cmwg',cmst.base.id,group_leader=group_leader
  widget_control,group_leader,sensitive=1
  
  return,cm_retst
end
