pro align_tip_mdi,tip=tip,mdi=mdi
  
   if n_elements(tip) eq 0 then tip='/media/LACIE/VTT-May05/19May05/19may05.002-0?cc'
   if n_elements(mdi) eq 0 then mdi='/media/LACIE/MDI-May05/fd_M_01h.108513/0005.fits'
;   if n_elements(tip) eq 0 then tip='/media/LACIE/VTT-May05/19May05/19may05.007-0?cc'
;   if n_elements(mdi) eq 0 then mdi='/media/LACIE/MDI-May05/fd_M_01h.108514/0030.fits'
;   if n_elements(tip) eq 0 then tip='/media/LACIE/VTT-May05/19May05/19may05.009-0?cc'
;   if n_elements(mdi) eq 0 then mdi='/media/LACIE/MDI-May05/fd_M_01h.108515/0040.fits'
   
                                ;get mdi from tip image box-size
   img_solar,ps=0,box=box,tip=tip,/soho,/silicon,/magnetogram, $
     imgtip=tipimg,ret_tipst=tipst
                                ;add 200%
   add=2
   mdibox=[box(0)-add*(box(2)-box(0)),box(1)-add*(box(3)-box(1)), $
        box(2)+add*(box(2)-box(0)),box(3)+add*(box(3)-box(1))]
   img_solar,ps=0,box=mdibox,mdi=mdi,/soho,imgmdi=mdiimg,ret_mdist=mdist
   xycut=cut_box(mdist,box=mdibox,/arcsec)
                                ;get tip image in TIP coordinates
   magtip=get_tipimg(tip,/silicon,/magnetogram)
   tipimg=reverse(magtip,2)
   sztip=size(tipimg)

   
                                 ;remove NAN from tip image
  nofin=where(finite(tipimg) eq 0)
  if nofin(0) ne -1 then tipimg(nofin)=min(tipimg,/nan)
  
  
;scale image to correct solar x/y
                                ;aspect ratio with resolution 0. 5 pix
                                ;per arcsec
   reso=0.25
   tnx=fix((box(2)-box(0))/reso)
   tny=fix((box(3)-box(1))/reso)
   mnx=fix((mdibox(2)-mdibox(0))/reso)
   mny=fix((mdibox(3)-mdibox(1))/reso)
   mdiimg_scl=congrid(mdiimg,mnx,mny,/interp)
   
   window,0,xsize=max([tnx,mnx]),ysize=tny+mny
   loadct,0
   
   flip=1
   ok=0
   repeat begin                 ;flip tip image?
     erase
     if flip eq 1 then tipimg=reverse(magtip,2) else tipimg=magtip
     tipimg_scl=congrid(tipimg,tnx,tny,/interp)
     tvscl,mdiimg_scl
     tvscl,tipimg_scl,0,mny,/device
     print,'Press ''F'' to flip image, ''Y'' to continue'
     gk=strupcase(get_kbrd())
     if gk eq 'F' then flip=~flip          ;NOT operator
     if gk eq 'Y' then ok=1
   endrep until ok
   
   
                                ;select region from mdi image
   ok=0
   repeat begin
     erase
     tvscl,mdiimg_scl
     tvscl,tipimg_scl,0,mny,/device
     userlct
     plots,color=1,[0,max([tnx,mnx])],[mny,mny]
     print,'Click on Lower Left boundary'
     cursor,llx,lly,/down,/device
     lly=lly<(mny-1)
     print,'Click on Upper Right boundary'
     cursor,urx,ury,/down,/device
     ury=ury<(mny-1)
     plots,color=1,[llx,llx,urx,urx,llx],[lly,ury,ury,lly,lly],/device
     print,'Box: x=[',llx,lly,'], y=[',urx,ury,']'
     print,'Happy with selection?'
     gk=get_kbrd()
     if strupcase(gk) eq 'Y' then ok=1
   endrep until ok
   mdiimg_scl=mdiimg_scl(llx:urx,lly:ury)
   
   loadct,0
  
  trans_img=mdiimg_scl
  ref_img=tipimg_scl
  
                                ;remove NAN from tip image
  nofin=where(finite(trans_img) eq 0)
  if nofin(0) ne -1 then trans_img(nofin)=min(trans_img,/nan)

;  pp = setpts_roi(trans_img,ref_img)
  setpts,pp,trans_img,ref_img,plotonly=plotonly, $
    noscale=noscale,append=append,trima=trima,key=key
 
  tt = caltrans(pp)
  pin = tt[*,0]
  qin = tt[*,1]
  
  img_shift=auto_align_images(trans_img,ref_img,pin,qin,pout,qout,/amoeba, $
                                trans=trans,missing=!values.f_nan)
  
                                ;create x and y vectors of TIP image
                                ;use shifted/rotated mdi coordinates
  xx=[llx,urx]/float(mnx)*(xycut(2)-xycut(0))+xycut(0)
  yy=[lly,ury]/float(mny)*(xycut(3)-xycut(1))+xycut(1)
  xmdi=congrid(mdist.xstd(xx(0):xx(1),yy(0):yy(1)),urx-llx+1,ury-lly+1,/interp)
  ymdi=congrid(mdist.ystd(xx(0):xx(1),yy(0):yy(1)),urx-llx+1,ury-lly+1,/interp)
  xmdi_tr=poly_2d(xmdi,pout,qout,2,tnx,tny,cubic=-0.5,missing=!values.f_nan)
  ymdi_tr=poly_2d(ymdi,pout,qout,2,tnx,tny,cubic=-0.5,missing=!values.f_nan)
                                ;convert to original scale
  xtip=congrid(xmdi_tr,sztip(1),sztip(2),/interp)
  ytip=congrid(ymdi_tr,sztip(1),sztip(2),/interp)
  if flip then ytip=reverse(ytip,2)

                                ;store coordinates to sav-file
  tipfile=(file_search(tip))(0)
  tfroot=strmid(tipfile,strpos(tipfile,'/',/reverse_search)+1,11)
  sav='./sav/align_mdi_'+tfroot+'.sav'
  mdi_tref=mdist.header.tref
  save,/xdr,/compress,file=sav,xtip,ytip,mdi_tref
  print,'Wrote new TIP coordinates to '+sav
  
  
  userlct,/full
  slpos=where(byte(mdi) eq (byte('/'))(0),nsl)
  if nsl gt 1 then mdiroot=strmid(mdi,slpos(nsl-2),strlen(mdi)) $
  else mdiroot=mdi
;  xr=[min(tipst.xstd,/nan),max(tipst.xstd,/nan)]
;  yr=[min(tipst.ystd,/nan),max(tipst.ystd,/nan)]
  xr=[min(xtip(*,0),/nan),max(xtip(*,0),/nan)] ;use 'lowest' x-row
  yr=[min(ytip(0,*),/nan),max(ytip(0,*),/nan)] ;use 'leftest' y-col
  
  image_cont_al,tipimg,zrange=[min(tipimg,/nan),max(tipimg,/nan)],xrange=xr,yrange=yr,xtitle='x [arcsec]',ytitle='y [arcsec]',title=tfroot+', aligned with '+mdiroot,cont=0
  
  
  
                                ;blink images to test alignment
  print,'Press key to quit'
  xs=800.
  window,0,xsize=xs,ysize=float(tny)/tnx*xs
  noval=where(finite(img_shift) eq 0) 
  if noval(0) ne -1 then img_shift(noval)=min(img_shift,/nan)
  repeat  begin
    tvscl,congrid(img_shift,512,float(tny)/tnx*xs,/interp)
    wait,.5
    tvscl,congrid(tipimg_scl,512,float(tny)/tnx*xs,/interp)
    wait,.5
  endrep until get_kbrd(0) ne ''
  
  
  stop
end
