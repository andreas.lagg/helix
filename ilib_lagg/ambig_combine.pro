;combine inverted_atmos.fits with the ambiguity resolved bfield-fits
;files
pro ambig_combine,fatmos,fmag,fout,show=show,ps=ps,nlayer=nlayer
  
  if n_params() ne 3 then begin
    print,'Usage:'
    print,'   ambig_combine,''inverted_atmos.fits'',''magfield_ltau*.fits'',''inverted_atmos_ambig.fits'',/show,/ps'
    return
  endif
  
  if n_elements(show) eq 0 then show=1
  
  print,'Reading '+fatmos
  atmos=read_fits_dlm(fatmos,0,hatmos)
  ilt=where(strpos(hatmos,'LGTRF') eq 0)
  ntau=n_elements(ilt)
  lgtau=strarr(ntau)
  for i=0,n_elements(ilt)-1 do $
    lgtau[i]=float((strsplit(hatmos[ilt[i]],/extract))[4])
  iaz=where(strpos(hatmos,'AZIMU') eq 0)
  
  
                                ;add new variables: solar bx,by,bz for
                                ;every height node
  
  
  sza=size(atmos,/dim)
  
  if show eq 1 then begin
    !p.position=0
    if keyword_set(ps) then begin
      !p.multi=0
    endif else begin
      !p.multi=[0,3,1]
    endelse
    psset,ps=keyword_set(ps),file='aziambig.ps',size=[18,12],/no_x
  endif
  
  fin=file_search(fmag,count=cnt)
  if cnt eq 0 then message,'No files found: '+fmag
  fin=fin[[2,0,1]]
  for i=0,cnt-1 do begin
    print,'Reading '+fin[i]
    bf=read_fits_dlm(fin[i],0,hbf)
    if i eq 0 then begin
                                ;check if file contains bx,by,bz
      par=sxpar(hbf,'PAR*')
      bxyzmode=max(par eq 'BXSOL') eq 1
      if bxyzmode then begin
        atmos_new=fltarr(sza[0],sza[1],sza[2]+ntau*3)
        atmos_new[*,*,0:sza[2]-1]=atmos
      endif else atmos_new=atmos
      bfst={lgt:0.,ia:0,it:0,flip:bytarr(sza[0],sza[1])}
      b=replicate(bfst,cnt)
    endif
    lgtrf=float(sxpar(hbf,'LGTAU'))
    it=(where(abs(lgtau-lgtrf) le 1e-3))[0]
    if it eq -1 then message,fmag[i]+': LGTAU layer not found in '+fatmos
    ia=sxpar(hatmos[iaz[it]:iaz[it]],'AZIMU')-1
    flip=round(abs(atmos[*,*,ia]-bf[*,*,2])/180) eq 1
    b[i].lgt=lgtrf
    b[i].ia=ia
    b[i].it=it
    b[i].flip=flip
  endfor
                                ;define conditions for good flip
                                ;(needs flip in not only one
                                ;tau-layer)
  b=b[reverse(sort(b.lgt))]
  print,b.lgt
  if n_elements(nlayer) eq 0 then begin
    nlayer=fix(cnt)/2+1
    nlayer=2
  endif
  goodflips=total(b.flip,3) ge nlayer ; or b[0].flip or b[1].flip or b[2].flip
  for i=0,cnt-1 do begin
    ia=b[i].ia
    it=b[i].it
    
    atmos_new[*,*,ia]=bf[*,*,2]*goodflips + atmos[*,*,ia]*(goodflips eq 0)
                                ;add solar bx,by,bz
    if bxyzmode then begin
      atmos_new[*,*,sza[2]+it+0*ntau]=bf[*,*,3] ;BXSOL
      atmos_new[*,*,sza[2]+it+1*ntau]=bf[*,*,4] ;BYSOL
      atmos_new[*,*,sza[2]+it+2*ntau]=bf[*,*,5] ;BZSOL
    endif
    
    if show eq 1 then begin
      lts=', log(tau)='+n2s(b[i].lgt,format='(f10.2)')
      userlct,/nologo,glltab=8,/full,verbose=0
      image_cont_al,/cut,/aspect,contour=0,atmos[*,*,ia], $
                    title='Original'+lts,ztitle='AZI [deg]',$
                    zrange=[-180,180]
      image_cont_al,/cut,/aspect,contour=0,atmos_new[*,*,ia], $
                    title='Ambiguity removed'+lts,ztitle='AZI [deg]', $
                    zrange=[-180,180]
      if !d.name ne 'PS' then begin
        print,'Press key to continue...'
        key=get_kbrd()
      endif
    endif
  endfor
  userlct,verbose=0,coltab=0,/full
  image_cont_al,/cut,/aspect,contour=0,goodflips, $
                title='Good Flips: Require flip in '+n2s(nlayer)+ $
                ' of '+n2s(cnt)+' layers'
  
  
  if show then psset,/close
  
  if bxyzmode then begin
    ii=1
    parafter='CHISQ'
    for ip=0,2 do begin
      pos=max(where(strpos(hatmos,parafter) eq 0))
      pp='B'+(['X','Y','Z'])[ip]+'SOL'
      ppfull=pp+'   =                   '+ $
             n2s(sza[2]+ip*ntau+indgen(3)+1,format='(i2.2)')+ $
             ' /ambiguity resolved'
      hnew=[hatmos[0:pos],ppfull]
      if n_elements(hatmos) gt pos+1 then $
        hnew=[hnew,hatmos[pos+1:*]]
      hatmos=hnew
      parafter=pp 
      ii=ii+1
    endfor
  endif
  
  keep=['DATE_OBS','XCEN','YCEN','PIXX','PIXY','LON','LAT', $
        'RADIUS','B0ANGLE','PANGLE']
  add=''
  for i=0,n_elements(keep)-1 do begin
    ipos=(where(strpos(hbf,keep[i]) eq 0))[0]
    if ipos ne -1 then add=[add,hbf[ipos]]
  endfor
  if n_elements(add) ge 2 then begin
    ipos=max(where(strpos(hatmos,'EXTEND') eq 0))
    hatmos=[hatmos[0:ipos-1],add[1:*],hatmos[ipos:*]]
  endif
    
  print,'Writing ambiguity removed FITS file: ',fout
  time=anytim((systime(0,/jul)-julday(01,01,1979,00,00,00))*86400d,/atime)
  sxaddpar,hatmos,'AZI_AMBI',1,'If 1, Ambig. removed using Leka/Metcalf ambig'
  sxaddpar,hatmos,'AZI_DATE',(strsplit(time,/extract))[0], $
           'date of ambig. removal'
  sxaddpar,hatmos,'AZI_TIME',(strsplit(time,/extract))[1], $
           'time of ambig. removal'
  sxaddpar,hatmos,'AZI_NLAY',nlayer, $
           'number of tau-layers necessary for flipping'
  write_fits_dlm,atmos_new,fout,hatmos,/create
  print,'Done.'
end
