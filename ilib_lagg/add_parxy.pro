function add_parxy
  common xfwg,xfwg
  
  par=xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).par.str
  nx=xfwg.nx.val
  ny=xfwg.ny.val
  retpar=par
  for ix=0,nx-1 do for iy=0,ny-1 do begin
    word0=(strsplit(par(ix,iy),/extract))(0)
    xystr=' x='+n2s(ix)+',y='+n2s(iy)
    if strupcase(word0) eq 'USER' then retpar(ix,iy)=word0+ $
      ' '+strmid(xfwg.p(ix,iy).user.name,0,4)+'.. '+xystr
    if strupcase(word0) eq 'STOKES' then retpar(ix,iy)=word0+ $
      (['I','Q','U','V','Ic'])(xfwg.p(ix,iy).stokes)+xystr
  endfor
  return,retpar
end

