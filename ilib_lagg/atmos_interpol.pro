;read in spinor atmosphere and output it to another logtau grid
pro atmos_interpol,atmos,lttop=lttop,ltbot=ltbot,ltinc=ltinc
  
  if n_params() eq 0 then error=1 $
  else openr,unit,/get_lun,atmos,error=error
  
  if error ne 0 then begin
    print,'Atmosphere not found.'
    print,'Usage:'
    print,'  atmos_interpol,''hsrasp.dat'',lttop=-5.0,ltbot=-1.5,ltinc=0.1'
    reset
    
  endif
  
  line=''
  repeat begin
    a='' & readf,unit,a
    line=[line,a]
  endrep until eof(unit)
  free_lun,unit
  
  line=line(1:*)
  first=strsplit(line(0),' ',/extract)
  nold=fix(first(0))
  print,atmos,': ',nold,' elements'
  
  nnew=fix(abs(lttop-ltbot)/ltinc+1)
  ltnew=(findgen(nnew))*ltinc+lttop
  
  npar=n_elements(strsplit(line(1),' ',/extract))
  
  arr=fltarr(npar,nold)
  reads,line(1:nold),arr
  
  
  arrnew=fltarr(npar,nnew)
  arrnew(0,*)=ltnew
  for i=1,npar-1 do begin
    arrnew(i,*)=interpol(arr(i,*),arr(0,*),ltnew,/spline)
  endfor
  
  dpos=(strpos(atmos,'.',/reverse_search))(0)
  atmnew=strmid(atmos,0,dpos)+'_ip'+strmid(atmos,dpos,strlen(atmos))
  
  openw,unit,/get_lun,atmnew,error=error
  if error ne 0 then begin
    message,'Error writing interpolated atmosphere to '+atmnew
  endif
  
  head=n2s(nnew)
  if n_elements(first) ge 2 then head=head+strjoin(' '+first(1:*))
  printf,unit,head
  for i=0,nnew-1 do begin
    printf,unit,arrnew(*,i),format='(f8.3,'+n2s(npar-1)+'(e12.4))'
  endfor
  printf,unit
  if n_elements(line) ge nold+1 then printf,unit,line(nold+1:*)
  
  free_lun,unit
  print,'Wrote interpolatet atmosphere: ',atmnew
  print,atmnew,': ',nnew,' elements'
  stop
  
  
end
