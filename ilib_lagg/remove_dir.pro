;return filename without path
function remove_dir,file,path=path
  
  slpos=(strpos(file,'/',/reverse_search))(0)
  
  path=strmid(file,0,slpos+1)
  return,strmid(file,slpos+1,strlen(file))
  
end
