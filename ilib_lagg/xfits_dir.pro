pro xfits_dir
  common xdir,xdir
  
  xdir={home:'',ps:'',tmp:'',helix:'',save:''}
  xdir.home=getenv('XFITSHOME')
  xdir.ps=getenv('XFITSPS')
  xdir.tmp=getenv('XFITSTMP')
  xdir.save=getenv('XFITSSAVE')
  xdir.helix=getenv('HELIXDIR')
  if xdir.home ne '' then cd,xdir.home
  if xdir.ps eq '' then xdir.ps='./ps/'
  if xdir.tmp eq '' then xdir.tmp='./xfits_tmp/'
  if xdir.save eq '' then xdir.save='./xfits_plots/'
  for i=0,n_tags(xdir)-1 do if strlen(xdir.(i)) ge 1 then $
    if strmid(xdir.(i),strlen(xdir.(i))-1,1) ne '/' then xdir.(i)=xdir.(i)+'/'
  file_mkdir,xdir.ps,xdir.tmp,xdir.save
end
