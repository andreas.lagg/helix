function check_tipmask,obs,verbose=verbose,obscore=obscore,iscc=iscc
  
  notip=0
  if n_elements(verbose) eq 0 then verbose=1
  
                                ;determine core filename:
                                ;format yymmmyy.nnn
  fp=get_filepath(obs)
  
  len=strlen(obs)
  mask='00aaa00.000'
  pos=0
  isgood=0
  repeat begin
    if fp.ext eq 'fits' then notip=1 else $
    if pos gt len-strlen(mask) then begin
      if verbose ge 3 then $
        message,/cont,'Observation '+obs+ $
        ' does not match the TIP / TIP2 mask: '+mask
      notip=1
    endif else begin
      isgood=1
      for i=0,strlen(mask)-1 do begin
        mlb=(byte(strmid(mask,i,1)))(0)
        case 1 of
          mlb ge 48b and mlb le 57b: ss=bindgen(10)+48b
          mlb eq 46b: ss=46b
          else: ss=[bindgen(26)+97b,bindgen(26)+65b]
        endcase
        isgood=isgood and max(ss eq (byte(strmid(obs,pos+i,1)))(0))
      endfor
      pos=pos+1
    endelse
  endrep until isgood or notip eq 1
  
  iscc=strlowcase(strmid(obs,len-2,2)) eq 'cc'
  
  istip=notip eq 0
  if istip eq 1 then begin
    obscore=strmid(obs,pos-1,strlen(mask))
                                ;check for special derivate files (e.g. detrend)
    dtadd=['detrend','sdetrend','s','sin','lin','off','sinlin','sinoff']
    dtadd=dtadd(reverse(sort(strlen(dtadd))))
    found=0 & id=0
    repeat begin
      if strlowcase(strmid(obs,pos-1+strlen(mask),strlen(dtadd(id)))) eq $
        dtadd(id) then begin
        obscore=obscore+dtadd(id)
        found=1
      endif
    endrep until id++ ge n_elements(dtadd)-1 or found
;     if strlowcase(strmid(obs,pos-1+strlen(mask),7)) eq 'detrend' then $
;       obscore=obscore+'detrend'
;     if strlowcase(strmid(obs,pos-1+strlen(mask),8)) eq 'sdetrend' then $
;       obscore=obscore+'sdetrend' $
;     else if strlowcase(strmid(obs,pos-1+strlen(mask),1)) eq 's' then $
;       obscore=obscore+'s'
  endif else obscore=obs
  return,istip
end
