pro box,x=x,y=y,_extra=_extra,background=bg, $
        data=data,device=device,normal=normal
  
  
  nx=n_elements(x)
  if nx eq 2 then begin
    xi=[0,1,1,0] 
    yi=[0,0,1,1]
  endif else if nx eq 4 then begin
    xi=[0,1,2,3] 
    yi=[0,1,2,3]
  endif
  
  if n_elements(_extra) ne 0 then begin
    tn=tag_names(_extra)
  endif
  
  if n_elements(bg) ne 0 then begin
    polyfill,_extra=_extra,[x([xi])],[y([yi])], $
      color=bg,data=data,device=device,normal=normal;,noclip=0
  endif
  
  plots,_extra=_extra,[x([xi]),x(0)],[y([yi]),y(0)], $
    data=data,device=device,normal=normal;,noclip=0
end
