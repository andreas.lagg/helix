pro pco_quick
  
  loadct,0
  file=file_search('/dat/hathi/MPS/VTT-May13/15Jun13/gregor/PCO1/pco1_meas1_20130615-080334-00008_???.fits',count=cnt)
  ff=get_filepath(file[0])
  print,file
  
  for ii=0,cnt-1 do begin
    
    data=readfits(file[ii],hdr)
    
    sz=size(data,/dim)
    
    if ii eq 0 then sdev=fltarr(cnt,sz[2])
    for i=0,sz[2]-1 do begin
      img=data[*,*,i]
      mom=moment(img)
      sdev[ii,i]=sqrt(mom[1])
    endfor
    
    
    plot,sdev[ii,*],/xst,/yst
  endfor
  
  dummy=max(sdev,imax)
  ibest=array_indices(sdev,imax)
  
  nsel=30
  
  data=readfits(file[ibest[0]],hdr)
  imin=(ibest[1]-nsel/2)>0
  imax=(imin+nsel-1)<[sz[2]-1]
  if imax-imin lt nsel-1 then imin=imax-(nsel-1)
  
  datbest=data[*,*,imin:imax]
  
  
  fout=ff.path+(strsplit(ff.name,/regex,/extract,'.fits'))[0]+ $
    '_'+n2s(nsel)+'.fits'
  writefits,fout,datbest,hdr,append=0

end
