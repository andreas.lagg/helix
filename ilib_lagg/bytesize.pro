;return size of array in bytes
function bytesize,data
  
  typ=size(data,/type)
  nel=n_elements(data)
  
  case typ of
    1: bit=8
    2: bit=16
    12: bit=16
    3: bit=32
    13: bit=32
    14: bit=64
    15: bit=64
    4: bit=32
    5: bit=64
    6: bit=64
    9: bit=128
    else: bit=8
  endcase
  return,long64(nel)*bit/8
end
