pro fits_plotprof,fits=fits,x=x,y=y,ps=ps,_extra=_extra,retprofile=profile,profile1=profile1,profile2=profile2,profile3=profile3,return_yrange=return_yrange
  common fpp_data,oldfits,data,icont,wlvec,hdr
  
  if n_elements(fits) eq 0 or n_elements(x) eq 0 or n_elements(y) eq 0 then begin
    print,'quick plotting of profiles in FITS 4d files'
    print,'Syntax: '
    print,'fits_plotprof,fits=''~/data/MuRAM/sunspot_1536x384x1536/5250_mhd_6lines_profs.fits'',x=30,y=26,/ps'
    retall
  endif
  
  reread=n_elements(data) eq 0 or n_elements(oldfits) eq 0
  if reread eq 0 then reread=oldfits ne fits
  if reread then begin
    data=readfits(fits,hdr)
    wlref=double(sxpar(hdr,'WLREF'))
    wlmin=double(sxpar(hdr,'WLMIN'))
    wlmax=double(sxpar(hdr,'WLMAX'))
    nwl=fix(sxpar(hdr,'NWL'))
    if nwl ge 1 then begin
      wlvec=findgen(nwl)/(nwl-1)*(wlmax-wlmin)+wlmin
      icont=get_qscont(data)
      print,'Determined ICONT=',icont
    endif else begin
      wlvec=readfits(fits,hdrext,ext=1)
      icont=float(sxpar(hdr,'ICONT'))
    endelse
    oldfits=fits
  endif
  
  profile={wl:wlvec,ic:1., $
           i:reform(data(*,0,x,y))/icont,q:reform(data(*,1,x,y))/icont, $
           u:reform(data(*,2,x,y))/icont,v:reform(data(*,3,x,y))/icont}
  
  root=strmid(fits,strpos(fits,'/',/reverse_search)+1,strlen(fits))
  rootoe=(strsplit(root,'.fits',/regex,/extract))(0)
  desc=rootoe+'.x'+string(fix(x),format='(i4.4)')+ $
    '.y'+string(fix(y),format='(i4.4)')
  if size(ps,/type) eq 7 then begin
    psfile=ps
    ps=1
  endif else begin
    ps=keyword_set(ps)
    psfile=desc+'.ps'
  endelse
  
  if n_elements(title) eq 0 then title=desc
  print,desc
  
  psset,ps=ps,_extra=_extra,file=psfile,/no_x
  
  plot_profiles,profile,profile1,profile2,profile3,profile4,profile5,profile6,_extra=_extra,title=title,/norm2ic,icont=1,return_yrange=return_yrange
  psset,/close
end
