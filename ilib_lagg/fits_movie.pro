pro fits_movie,zoom=zoom,pssize=pssize
  
  
  
  file='~/Downloads/20130614_486_meas1.fits.gz'
  ff=get_filepath(file)
  data=readfits(file,hdr)
  sz=size(data,/dim)
  
  if n_elements(zoom) ne  4 then rg=[0,0,sz[0]-1,sz[1]-1] else rg=zoom
  if n_elements(pssize) ne 2 then pssize=[20,18]
  movie=ff.path+(strsplit(ff.name,'.fits',/regex,/extract))[0]
  if n_elements(zoom) eq 4 then movie=movie+'_zoom'
  
  tmpdir='/home/lagg/tmp/tmpmovie/'
  spawn,' mkdir -p '+tmpdir+' ; rm -rf '+tmpdir+'*'
  for i=0,sz[2]-1 do begin
    img=float(data[rg[0]:rg[2],rg[1]:rg[3],i])
    if i eq 0 then begin
      icont=mean(data[10:100,10:100,i])
      zrg=minmaxp(img/icont,perc=99.8)
      zrg=[0.15,1.3]
    endif
    tfile=tmpdir+n2s(i,format='(i4.4)')+'.eps'
    psset,/ps,/no_x,size=pssize,jpg=1,file=tfile,/encapsulated,view=0
    image_cont_al,img/icont,/cut,/aspect,contour=0, $
                  title='GREGOR BBI AR11768 (trailing) 08:15 UT,' + $
                  ' frame #'+string(i,format='(i3.3)'), $
                  xrange=rg[[0,2]]*.027, $
                  yrange=rg[[1,3]]*.027,xtitle='x [arcsec]', $
                  ytitle='y [arcsec]',zrange=zrg
;    xyouts,0,0,/normal,'RAW data - Flat and Dark corrected, approx. realtime'
    psset,/close
    if i eq 0 then spawn,'cp -p '+tfile+' '+movie+'.eps'
  endfor
  moviecmd='mencoder mf://'+tmpdir+'/*.jpg -mf fps=10:type=jpg -ovc x264 -x264encopts bitrate=3000 -oac copy -o '+movie+'.avi'
  
  print,'Executing Movie command:'
  print,moviecmd
  spawn,moviecmd
  print,'Movie created: ',movie+'.avi' 
end
