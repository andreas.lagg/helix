pro photonflux_solarc,ps=ps
  
  psset,ps=ps,file='~/tmp/photflux.eps',size=[26,20],/no_x,/encaps
  userlct & erase 
  @greeklett
;  !p.multi=[0,2,1]
  

;----------------------------------------------------------
; INPUT PARAMETERS
;----------------------------------------------------------
;

  lambda    = 10830.            ; [Angstroem]
;  lambda    = 8500.             ; [Angstroem]
;  lambda    = 3950.             ; [Angstroem]
;  lambda    = 6301.             ; [Angstroem]
  fwhm      = 30.               ; Filter width [mA]
  dnom      = 70.               ; nominal aperture diameter [cm]
  rnom      = 1.                ; nominal distance from the Sun [AU]
  texnom    = 4*4.0/4.            ; nominal exposure time [s]
  
;  pixelsize = 75.               ; km on the solar surface
                                ;pixel size is half of diff. limit res. at 1 AU
  pixelarcsec_difflim=1.22/(dnom*1e-2)*(lambda*1e-10)/!dtor*3600./2.
  
;  pixelarcsec=pixelarcsec_difflim
  pixelarcsec=0.36
  pixelsize_difflim=700.*pixelarcsec_difflim
  pixelsize=700.*pixelarcsec
  print,'Pixelsize (arcsec): ',pixelarcsec
;
;----------------------------------------------------------
; Light reduction
;----------------------------------------------------------
;

  lenses  = 0.98^9              ; optical lenses
  mirrors = 0.95^6              ; mirrors
  entr    = 1.0                 ; entrance window
  pref    = 0.90                ; prefilter
  polbs   = 0.5*0.98            ; polarizing beam splitter 
  ppmp    = 0.96^2              ; polarization modulation package
  pfo     = 0.7*0.75^2          ; FO 
  pfo     = 0.5                 ;grating efficiency
  issbs   = 0.95                ; ISS Beam Splitter
  poleff  = 1./sqrt(3)          ;max. polarization efficiency

; total efficiency without detector

  eta = lenses * mirrors * entr * pref * polbs * ppmp * pfo * issbs * poleff
;use predefined efficiency, negletcting all of the above  
;  eta=0.10*poleff
  eta=0.01
  
;detector efficiency  
  qf      = 0.60                ; Detector efficiency

;-------------------------------------------------------------------------
; Krivova Daten
;-------------------------------------------------------------------------

  wl = dblarr(936)
  flux = dblarr(936)

  str1 = ''
  str2 = ''
  str3 = ''

  openr,1,'~/idl/ilib_lagg/SolSp.23.8.98.dat' 

  readf,1,str1
  readf,1,str2
  readf,1,str3

  readf,1,wl,flux

  close,1

  flux = flux * 1.e-9

  wg =min(abs(wl - 617.3),index)

  flux = flux/flux[index] * 2.404 ;  [W / cm^2 nm sterad] 2.404 = Planck 5778K

  wg = min(abs(lambda-wl*10),index)
  solflux = flux[index]

;window,2,xsize=500,ysize=400
  !p.position=[.08,.1,.35,.9]
  plot,wl,flux,xra=[200,2200],/xst,xtitle=f_lambda+' [nm]',$
    ytitle='!5Solar Flux [W / cm!e2!n nm sterad]',charsize=1.5
  oplot,[200,2200],[1,1]*solflux,linestyle=1,color=1
  oplot,[1,1]*lambda/10.,[0,5],linestyle=1,color=2

  print,''
  print,'Lambda : ',lambda,' Angstroem'
  print,'Solar Flux : ',solflux,' W / cm^2 nm sterad'

;
;----------------------------------------------------------
; solar data
;----------------------------------------------------------
; 

  ares = (pixelsize * 1.e5)^2 
  r = rnom * 1.49e13
  d = dnom
  omeganom = d^2 * !pi / (4 * r^2)

  tex = texnom
  pt = ares * solflux * omeganom * fwhm * 1.e-4
  wt = pt * tex

  hnu = 6.626e-34 * 2.9979e8 / (lambda * 1.e-10)

  nphot0 = wt / hnu
  nphot = nphot0 * eta
  snr = sqrt(nphot)

  print,''
  print,'Telescope Efficiency : ',eta
  print,'Detector Efficiency : ',qf
  print,'Total Efficiency : ',eta*qf
  print,''
  print,'Number of incoming photons per pixel :  ',nphot0
  print,'Number of detected photons per pixel : ',nphot
  print,'Photon noise ratio :                   ',snr
  print,''

  nelectr = nphot * qf
  esnr = sqrt(nelectr)

  print,''
  print,'Number of detected electrons per pixel : ',nelectr
  print,'Electron noise ratio :                   ',esnr
  print,''
;
;----------------------------------------------------------
; variation with \mu
;----------------------------------------------------------

  mu = cos(findgen(91)*!pi/180.)
  ired = sqrt((2.+3*mu)/5. * nphot)

  if !d.name eq 'X' then wait,1
; erase
  !p.position=[.45,.1,.9,.9]
  ytg=0
  plot,mu,ired,xstyle=1,xra=[1,0],/ynozero,/noerase,$
    xtitle='!5cos!N'+f_theta,ytitle='!5Photon SNR per pixel',thick=3,$
    title='!5SNR variation across solar disk',charsize=1.5,yst=8,ytick_get=ytg
  axis,yax=1,0,!y.crange(1),/yst,yrange=!y.crange,charsize=1.5,ytickv=ytg,ytickname=n2s(1/ytg,format='(e15.1)')

  ff='(e15.1)'
  info='Lambda : '+n2s(lambda)+' '+f_angstrom
  info=[info,'Solar Flux : '+n2s(solflux,format='(f15.4)')+' W / (cm^2 nm sterad)']
  info=[info,'']
  info=[info,'Pixel size: '+n2s(fix(pixelsize))+' km (='+ $
       n2s(pixelarcsec,format='(f15.2)')+'")']
  info=[info,'Diff. limit pixel size: '+n2s(fix(pixelsize_difflim))+' km (='+ $
       n2s(pixelarcsec_difflim,format='(f15.2)')+'")']
  info=[info,'Exp. Time: '+n2s(texnom,format='(f15.2)')+' s (x4 for IQUV)']
  info=[info,'Distance from Sun: '+n2s(rnom,format='(f15.2)')+' AU']
  info=[info,'Telescope Aperture: '+n2s(fix(dnom))+' cm']
  info=[info,'Filter width: '+n2s(fix(fwhm))+' m'+f_angstrom]
  info=[info,'']
  info=[info,'Telescope Efficiency: '+n2s(eta*100,format='(f15.1)')+'%']
  info=[info,'Detector Efficiency: '+n2s(qf*100,format='(f15.1)')+'%']
  info=[info,'Total Efficiency: '+n2s(eta*qf*100,format='(f15.1)')+'%']
  info=[info,'']
  info=[info,'Number of incoming photons per pixel: '+n2s(nphot0,format=ff)]
  info=[info,'Number of detected photons per pixel: '+n2s(nphot,format=ff)]
  info=[info,'Photon noise ratio: '+n2s(snr,format=ff)]
  info=[info,'']
  info=[info,'Number of detected electrons per pixel: '+n2s(nelectr,format=ff)]
  info=[info,'Electron noise ratio: '+n2s(esnr,format=ff)]

  x0=!x.crange(0)+0.30*(!x.crange(1)-!x.crange(0))
  y0=!y.crange(0)+0.91*(!y.crange(1)-!y.crange(0))
  x1=!x.crange(0)+0.05*(!x.crange(1)-!x.crange(0))
  y1=!y.crange(0)+0.36*(!y.crange(1)-!y.crange(0))
  info0=add_comma(info(0:6),sep='!C')
  info1=add_comma(info(7:*),sep='!C')

  xyouts,/data,x0,y0,info0,charsize=1.2
  xyouts,/data,x1,y1,info1,charsize=1.2


;----------------------------------------------------------

  psset,/close

END
