function getuparname,upar
  nu=n_elements(upar)
  rpar=strarr(nu)
  nasym=['+','-','*','.','[',']']
  rep=  ['p','m',' ',' ',' ',' ']
  for i=0,nu-1 do begin
    bu=byte(upar(i))
    for is=0,n_elements(nasym)-1 do begin
      isym=where(bu eq (byte(nasym(is)))(0))
      if isym(0) ne -1 then bu(isym)=(byte(rep(is)))(0)
    endfor
    rpar(i)=strcompress(string(bu),/remove_all)
  endfor
  
  return,rpar
end

function xu_examples,ixy,define=define,set=set
  common xfwg,xfwg
  common xupwg,xupwg
  common xuex,xuex
  common fits,fits
  
  if keyword_set(define) then begin
    st={name:'',user:xfwg.p(0).user}
    xuex=replicate(st,14)
    icnt=0
                                ;abs(b-flux)
    par=getuparname(xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).par.str)
    pp=remove_multi(par)
    ia=max(where(strpos(pp,'AZIMU') eq 0))
    ib=max(where(strpos(pp,'BFIEL') eq 0))
    ig=max(where(strpos(pp,'GAMMA') eq 0))
    if ib ne -1 and ig ne -1 then begin
      xuex(icnt).name='B-Flux'
      xuex(icnt).user.title='Abs. Magnetic Flux'
      xuex(icnt).user.name='|B*cos(INC)|'
      xuex(icnt).user.unit='Mx/cm^2'
      xuex(icnt).user.oper= $
        'abs('+pp(ib)+'*cos('+pp(ig)+'/180*!pi))'
      icnt=icnt+1
    endif
    if ib ne -1 and ig ne -1 and ia ne -1 then begin
      xuex(icnt).name='BX (solar)'
      xuex(icnt).user.title='BX (solar reference frame)'
      xuex(icnt).user.name='BX (solar)'
      xuex(icnt).user.unit='G'
      xuex(icnt).user.oper= $
        'reform((bincazi2xyz(transpose([[['+pp[ib]+']],' + $
        '[['+pp[ig]+']],[['+pp[ia]+']]],[2,0,1]),' + $
        'lon=fits.lon,lat=fits.lat,' + $
        'b0angle=fits.b0angle,pangle=fits.pangle))[0,*,*])'
      icnt=icnt+1
    endif
    if ib ne -1 and ig ne -1 and ia ne -1 then begin
      xuex(icnt).name='BY (solar)'
      xuex(icnt).user.title='BY (solar reference frame)'
      xuex(icnt).user.name='BY (solar)'
      xuex(icnt).user.unit='G'
      xuex(icnt).user.oper= $
        'reform((bincazi2xyz(transpose([[['+pp[ib]+']],' + $
        '[['+pp[ig]+']],[['+pp[ia]+']]],[2,0,1]),' + $
        'lon=fits.lon,lat=fits.lat,' + $
        'b0angle=fits.b0angle,pangle=fits.pangle))[1,*,*])'
      icnt=icnt+1
    endif
    if ib ne -1 and ig ne -1 and ia ne -1 then begin
      xuex(icnt).name='BZ (solar)'
      xuex(icnt).user.title='BZ (solar reference frame)'
      xuex(icnt).user.name='BZ (solar)'
      xuex(icnt).user.unit='G'
      xuex(icnt).user.oper= $
        'reform((bincazi2xyz(transpose([[['+pp[ib]+']],' + $
        '[['+pp[ig]+']],[['+pp[ia]+']]],[2,0,1]),' + $
        'lon=fits.lon,lat=fits.lat,' + $
        'b0angle=fits.b0angle,pangle=fits.pangle))[2,*,*])'
      icnt=icnt+1
    endif
    ib=(where(strpos(pp,'BFIEL') eq 0))
    if n_elements(ib) ge 2 then begin
      xuex(icnt).name='B1/B2'
      xuex(icnt).user.title='Ratio: B-Field'
      xuex(icnt).user.name='B1/B2'
      xuex(icnt).user.unit=''
      xuex(icnt).user.oper=pp(ib(0))+'/'+pp(ib(1))
      icnt=icnt+1
    endif
    it=(where(strpos(pp,'TEMPE') eq 0))
    if n_elements(it) ge 2 then begin
      xuex(icnt).name='TEMP1/TEMP2'
      xuex(icnt).user.title='Ratio: Temperature'
      xuex(icnt).user.name='B1/B2'
      xuex(icnt).user.unit=''
      xuex(icnt).user.oper=pp(it(0))+'/'+pp(it(1))
      icnt=icnt+1
    endif
    iv=(where(strpos(pp,'VELOS') eq 0))
    if n_elements(it) ge 2 and n_elements(iv) ge 2 then begin
      xuex(icnt).name='V1/V2'
      xuex(icnt).user.title='Ratio: LOS-velocity'
      xuex(icnt).user.name='VELOS1/VELOS2'
      xuex(icnt).user.unit=''
      xuex(icnt).user.oper=pp(iv(0))+'/'+pp(iv(1))
      icnt=icnt+1
    endif
    xuex(icnt).name='LP'
    xuex(icnt).user.title='Linear Polarization'
    xuex(icnt).user.name='sqrt(Q^2+U^2)'
    xuex(icnt).user.unit=''
    xuex(icnt).user.oper='sqrt(StokesQ^2+StokesU^2)'
    xuex(icnt).user.colortable='stokes'
    icnt=icnt+1
    
    xuex(icnt).name='TP'
    xuex(icnt).user.title='Total Polarization'
    xuex(icnt).user.name='sqrt(Q^2+U^2+V^2)'
    xuex(icnt).user.unit=''
    xuex(icnt).user.oper='sqrt(StokesQ^2+StokesU^2+StokesV^2)'
    xuex(icnt).user.colortable='stokes'
    icnt=icnt+1
    
    xuex(icnt).name='THRESHOLD'
    xuex(icnt).user.title='noise threshold'
    xuex(icnt).user.name='(Q or U)>0.003 or V>0.0045'
    xuex(icnt).user.unit=''
    xuex(icnt).user.oper='((abs(StokesQ) ge 0.003) OR (abs(StokesU) ge 0.003)) OR (abs(StokesV) ge 0.0045)'
    xuex(icnt).user.colortable='stokes'
    icnt=icnt+1
    
    xuex(icnt).name='MLR'
    xuex(icnt).user.title='Stokes V magnetic lineratio'
    xuex(icnt).user.name='MLR'
    xuex(icnt).user.unit='ratio'
    xuex(icnt).user.oper='lineratio(wl=[15648.535,15652.889],gf=[3.00,1.53],dwl=4.,smooth=3)'
    icnt=icnt+1
    
    xuex(icnt).name='Empty'
    xuex=xuex(0:icnt)
    return,xuex.name
  endif
  
  if keyword_set(set) and n_elements(xupwg) ne 0 then begin  
    i=xupwg.example.val
    if i ge 0 then begin
      xfwg.p(ixy(0),ixy(1)).user=xuex(i).user
      widget_control,xupwg.base.id,bad_id=bad_id   
      widget_control,xupwg.title.id,set_value=xuex(i).user.title
      widget_control,xupwg.name.id,set_value=xuex(i).user.name
      widget_control,xupwg.unit.id,set_value=xuex(i).user.unit
      widget_control,xupwg.oper.id,set_value=xuex(i).user.oper
      retstr=xuex(i).name
    endif else retstr='No examples.'
    return,retstr
  endif else return,'No examples.'
end

pro xupwg_event,event
  common fits,fits
  common xfwg,xfwg
  common xpwg,xpwg
  common xupwg,xupwg
  
  widget_control,event.id,get_uvalue=uval
  ixy=xupwg.ixy
  case uval of 
    'done': widget_control,xupwg.base.id,/destroy
    'title': xfwg.p(ixy(0),ixy(1)).user.title=event.value
    'name': begin
      xfwg.p(ixy(0),ixy(1)).user.name=event.value
      xw_parupdate
    end
    'unit': xfwg.p(ixy(0),ixy(1)).user.unit=event.value
    'example': begin
      xupwg.example.val=event.index
      examples=xu_examples(ixy,/set)
    end
    'oper': xfwg.p(ixy(0),ixy(1)).user.oper=strjoin(event.value)
    else: help,/st,event
  end
  
end

pro xfits_setuserpar,ixy
  common fits,fits
  common xfwg,xfwg
  common xpwg,xpwg
  common xupwg,xupwg
  common xopwgold,ixyold
  
   if n_elements(xupwg) ne 0 then begin
    widget_control,xupwg.base.id,bad_id=bad_id    
    if bad_id eq 0 then begin
      widget_control,xupwg.base.id,/destroy
    endif
  endif
   
   
  subst={id:0l,val:0.,str:''}
  oxupwg={base:subst,done:subst,title:subst,name:subst,oper:subst,unit:subst, $
          ixy:ixy,example:subst}
  if n_elements(xupwg) eq 0 then xupwg=temporary(oxupwg) $
  else xupwg=update_struct(xupwg,oxupwg)
  
  info=widget_info(xfwg.base.id,/geometry)
  ixs=n2s(fix(ixy(0))) & iys=n2s(fix(ixy(1)))
  xupwg.base.id=widget_base(title='User Defined Parameter for plot x='+ixs+ $
                            ',y='+iys, $
                            group_leader=xpwg.base.id,col=1, $
                            xoffset=info.xoffset, $
                            yoffset=info.yoffset+info.ysize+150)
  sub1=widget_base(xupwg.base.id,col=1)
  sub2=widget_base(xupwg.base.id,row=1)
  sub3=widget_base(xupwg.base.id,col=1)
  sub30=widget_base(sub3,col=1)
  sub3a=widget_base(sub30,col=1)
  sub3b=widget_base(sub30,col=2)
  xupwg.title.id=cw_field(sub1,xsize=40,/all_events,/string,title='Title: ', $
                          uvalue='title', $
                          value=xfwg.p(ixy(0),ixy(1)).user.title)
  xupwg.name.id=cw_field(sub2,xsize=20,/all_events,/string,title='Name: ', $
                          uvalue='name', $
                          value=xfwg.p(ixy(0),ixy(1)).user.name)
  xupwg.unit.id=cw_field(sub2,xsize=10,/all_events,/string,title='Unit: ', $
                          uvalue='unit', $
                          value=xfwg.p(ixy(0),ixy(1)).user.unit)
  dummy=widget_label(sub3a,value='Operation (use standard IDL syntax):', $
                     /align_left)
  ; dummy=widget_label(sub3a,/align_left, $
  ;                    value='example: abs(BFIELC1*cos(GAMMAC1*!dtor))')
  examples=xu_examples(ixy,/define)
  dummy=widget_label(sub3b,value='Examples:',/align_left)
  xupwg.example.id=widget_combobox(sub3b,uvalue='example',value=examples)
  xupwg.oper.id=cw_field(sub3,xsize=50,/all_events,/string,title='', $
                         uvalue='oper',ysize=4, $
                         value=xfwg.p(ixy(0),ixy(1)).user.oper)
  avpar=remove_multi(xfwg.p.par.str)
  val=where(avpar ne '' and avpar ne 'Stokes Par' and avpar ne 'User')
  avparadd=add_comma('Stokes'+['I','Q','U','V','Ic'],sep=' ')
  if val(0) ne -1 then $
    avpar=[add_comma(getuparname(avpar(val)),sep=' '),avparadd] $
  else avpar=avparadd;'None. Add some plots.'
  dummy=widget_label(sub3,value='Available Parameters: ', $
                     /align_left)
  dummy=widget_text(sub3,value=avpar,/align_left,/wrap,/scroll, $
                    xsize=50,ysize=8)
  xupwg.done.id=widget_button(xupwg.base.id,value=' Done ',uvalue='done')
  
  xupwg.ixy=ixy
  ixyold=ixy
  widget_control,xupwg.base.id,/realize
  xmanager,'xupwg',xupwg.base.id,no_block=1
end

function xfits_getuserpar,ix,iy,valid=valid,xrange=xrange,yrange=yrange
  common xpwg,xpwg
  common fits,fits
  common xfwg,xfwg
  common xupwg,xupwg
  
  nx=max(xrange)-min(xrange)+1
  ny=max(yrange)-min(yrange)+1
  data=fltarr(nx,ny)
  valid=bytarr(nx,ny)+1b
  xsel=xrange-fits.xoff
  ysel=yrange-fits.yoff
  
                                ;define all variables:
  for ixx=0,xfwg.nx.val-1 do for iyy=0,xfwg.ny.val-1 do begin
    pname=(getuparname(xfwg.p(ixx,iyy).par.str))(0)
    if strpos(xfwg.p(ix,iy).user.oper,pname) ne -1 then begin
      dat=pudata(xfwg.p(ixx,iyy).par.val,ix,iy,xsel,ysel,val,parname)
      exstr=pname+'=dat'
      exok=execute(exstr)
      valid=valid and val
      if exok eq 0 then begin
        message,/cont,'Error in Variable: '+pname
        valid(*)=0
      endif
    endif
  endfor
  
  exok=0
  if strcompress(/remove_all,xfwg.p(ix,iy).user.oper) ne '' then begin
    isp=strsplit(strupcase(xfwg.p(ix,iy).user.oper),'STOKES',/regex)
    if n_elements(isp) ge 2 then begin
      data=xfits_getstokespar(ix,iy,xrange=xsel+fits.xoff, $
                              yrange=ysel+fits.yoff, $
                              valid=valid,title=title,/allstokes)
      for i=1,n_elements(isp)-1 do begin
        spar2=strupcase(strmid(xfwg.p(ix,iy).user.oper,isp[i],2))
        spar=strupcase(strmid(xfwg.p(ix,iy).user.oper,isp[i],1))
        if spar2 eq 'IC' then stokesic=reform(data[4,*,*]) $
        else if spar eq 'I' then stokesi=reform(data[0,*,*]) $
        else if spar eq 'Q' then stokesq=reform(data[1,*,*]) $
        else if spar eq 'U' then stokesu=reform(data[2,*,*]) $
        else if spar eq 'V' then stokesv=reform(data[3,*,*])
      endfor
    endif
    exok=execute('data='+xfwg.p(ix,iy).user.oper)
    if exok eq 0 then begin
      message,/cont,'Error in USER-Parameter: '+xfwg.p(ix,iy).user.oper
      valid(*)=0
    endif
    sz=size(data,/dim)
    if n_elements(sz) eq 2 then $
      if (sz[0] eq fits.nx and sz[1] eq fits.ny and $
          (sz[0] ne nx or sz[1] ne ny)) then begin
      data=data[xsel[0]:xsel[1],ysel[0]:ysel[1]]
    endif
    valid=valid and finite(data)
  endif else begin
    message,/cont,'Empty operation in USER-Parameter x='+n2s(ix)+',y='+n2s(iy)
    valid(*)=0
  endelse

  return,data
end
  
;get stokes parameter for plot position ix,iy
function xfits_userpar,ix,iy,valid=valid,xrange=xrange,yrange=yrange, $
                       title=title,init=init,call=call
  common xfwg,xfwg
  
  if keyword_set(call) then return,0
  
  if strcompress(xfwg.p(ix,iy).user.oper) eq '' $
    or keyword_set(init) then begin
    xfits_setuserpar,[ix,iy]
    if n_elements(xrange) ne 2 or n_elements(yrange) ne 2 then return,0
  endif
  
  data=xfits_getuserpar(ix,iy,valid=valid,xrange=xrange,yrange=yrange)
  
  return,data
  
end
