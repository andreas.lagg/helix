;defines physical constants as system variables
pro physical_constants
  
  DEFSYSV,'!DDTOR',!dpi/180.0d,1

  DEFSYSV,'!c_light',299792.5d *1e3 ;speed of light [m/s]
  DEFSYSV,'!e_charge',1.6021d-19 ;elemental charge [C]
  DEFSYSV,'!amu',1.67d-27        ;atomic mass unit [kg]
  DEFSYSV,'!me',9.10938188d-31   ;electron mass [kg]
  DEFSYSV,'!kb',1.3806505d-23   ;k_boltzmann J/K
  DEFSYSV,'!rc',10973731.6d     ;rydberg constant [1/m]
  DEFSYSV,'!NL',6.02214199d23   ;avogadro [1/mol] 
  DEFSYSV,'!h',6.62606896d-34   ;Planck constant [J/s]
  
  
  DEFSYSV,'!MSAT',5.685e26      ;Saturn mass in kg
  DEFSYSV,'!G',6.67428e-11      ;gravitational constant N m^2/kg^2
  
  DEFSYSV,'!AU',1.5e11          ;astronomical unit in m
  DEFSYSV,'!RE',6.38e6          ;earth radius in m
  DEFSYSV,'!RJ',71492.0e3       ;jupiter radius in m
  DEFSYSV,'!RIo',1821.3e3       ;Io radius in m
  DEFSYSV,'!REur',1565.0e3      ;Europa radius in m
  DEFSYSV,'!RGan',2634.0e3      ;Ganymede radius in m
  DEFSYSV,'!RCal',2403.0e3      ;Callisto radius in m
  DEFSYSV,'!DIPOLE_TILT_JUP',9.6 ;dipole tilt in degrees
  DEFSYSV,'!PERIOD_JUP',9.9250*3600. ;siereal rotation period [s]
  DEFSYSV,'!PAR_DIPOL_JUP',8.07e15*20000.
  DEFSYSV,'!PAR_DIPOL_EARTH',8.07e15
  DEFSYSV,'!RS',120536.e3/2.    ;saturn radius in m
  DEFSYSV,'!RT',5150.e3/2.      ;titan radius in m
end
