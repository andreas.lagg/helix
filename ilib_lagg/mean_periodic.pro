;calc
function mean_periodic,arrin,valid=valin,dimension=dimension,period=period,sdev=sdevarr
  
  dim=(size(arrin))(0)
  if n_elements(period) ne 2 then begin
    if n_elements(dimension) eq 0 then $
      meanarr=total(arrin*valin)/total(valin) $
    else begin
      if dim ge dimension then $
        meanarr=total(arrin*valin,dimension)/total(valin,dimension) $
      else meanarr=arrin*valin
    endelse
    return,meanarr
  endif
  dp=max(period)-min(period)
  arrscl=(arrin-min(period))/dp*2*!pi
  
  ival=where(valin eq 1)
  if ival(0) eq -1 then return,!values.f_nan
  
  if n_elements(dimension) ne 0 then begin
    sz=size(arrin)
    if (dim ne 3 and dim ne 2) or dimension ne 3 then $
      message,'Currently only 3D arrays and dim=3 supported.'
    if dim ge dimension then begin
      meanarr=total(valin,dimension)
      sdevarr=total(valin,dimension)
    endif else begin
      meanarr=valin
      sdevarr=valin
    endelse
    meanarr(*)=!values.f_nan
    sdevarr(*)=!values.f_nan
    for ix=0,sz(1)-1 do for iy=0,sz(2)-1 do begin
      ival=where(valin(ix,iy,*) eq 1)
      if ival(0) ne -1 then begin
        carr=complex(cos(arrscl(ix,iy,ival)),sin(arrscl(ix,iy,ival)))
        if n_elements(carr) ge 2 then mom=moment(carr) $
        else mom=[carr,!values.f_nan]
        meanval=((atan(imaginary(mom(0)), $
                       float(mom(0)))/2/!pi*dp+2*dp) mod dp)+min(period)
;        sdev=sqrt(sqrt(float(mom(1))^2+imaginary(mom(1))^2))*360./2/!pi/2
        sdev=asin(sqrt(sqrt(float(mom(1))^2+imaginary(mom(1))^2)))*360./2/!pi/2
        meanarr(ix,iy)=meanval
        sdevarr(ix,iy)=sdev
      endif
    endfor
    return,meanarr
  endif else begin
    carr=complex(cos(arrscl(ival)),sin(arrscl(ival)))
    mom=moment(carr)
    meanval=((atan(imaginary(mom(0)),float(mom(0)))/2/!pi*dp+2*dp) mod dp)+min(period)
    return,meanval
  endelse
end
