;create fits wgt files for spinor
pro spinor_wgt
  
  n=221                          ;number of WL points 
  wlrg=[-2d,20d] 
  wl=[[-5.0d,-1.5d, 3.1d,12.3d,15.5d], $ ;wl range for "good" weights
      [-2.0d, 1.3d, 5.6d,14.7d,18.0d]]
  outdir='/scratch/slam/lagg/inversions/GRIS/17sep15.2d.lor0.8_gauss0.25_binned/'
  
  wlv=dindgen(n)/(n-1)*(wlrg[1]-wlrg[0])+wlrg[0]
  flagsig=bytarr(n)
  for i=0,n_elements(wl[*,0])-1 do begin
    iin=where(wlv ge wl[i,0] and wlv le wl[i,1])
    if iin[0] ne -1 then flagsig[iin]=1b   
  endfor
  
                                ;set sigma level:
                                ;=1 outside "good" range
                                ;=0.0xx inside "good" range
  isig= (flagsig eq 0)*1. + flagsig*0.0003
  vsig= (flagsig eq 0)*1. + flagsig*0.0001
  qusig=(flagsig eq 0)*1. + flagsig*0.0001
  
  writefits,outdir+'iweights.fits', [[wlv],[ isig]]
  writefits,outdir+'vweights.fits', [[wlv],[ vsig]]
  writefits,outdir+'quweights.fits',[[wlv],[qusig]]
  
end
