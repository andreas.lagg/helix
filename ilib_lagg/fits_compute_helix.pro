;use data in fits structure and compute helix profile
pro fits_compute_helix,x=x,y=y,fitsfile=fitsfile, $
                       fitprof=fitprof,obsprof=obsprof,compprof=compprof
  common fits,fits
  common xdir,xdir
  common oldchelix,oldfile,ipt
  common use_idl,use_idl

  @comm_comp.pro
  
  if n_elements(fitsfile) ne 0 then read_fitsdata,fitsfile
  if n_elements(fits) eq 0 then begin
    print,'No fits data. Use read_fitsdata first.'
    reset
  endif
  if n_elements(use_idl) eq 0 then idl=0 else idl=use_idl

  reread=n_elements(oldfile) eq 0
  if reread eq 0 then begin
    reread=oldfile ne fits.file
  endif
  reread=1  
  if reread then begin
                                ;replace the atom, wgt, convolve directories:
    ipt=fits.ipt
    ipt.dir.ps=xdir.ps
    ipt.dir.sav=xdir.save
    ipt.dir.atom=xdir.tmp
    ipt.dir.wgt=xdir.tmp
    
;   if ipt.prefilter ne '' then ipt.prefilter=xdir.tmp+remove_dir(ipt.prefilter)
;   if ipt.conv_func ne '' then ipt.conv_func=xdir.tmp+remove_dir(ipt.conv_func)
    
    ipt.nmi=1
    ipt.ncalls=0
    ipt.fitsout=0
    ipt.verbose=0
    ipt.display_profile=0
    ipt.display_comp=1
    ipt.save_fitprof=0
    ipt.pixelrep=0
  endif
  if idl eq 1 then ipt.code='IDL'
  
;  if x ge 0 and x lt fits.nx and y ge 0 and y lt fits.ny then begin
  if (x-fits.xoff ge 0 and x-fits.xoff lt fits.nx and $
      y-fits.yoff ge 0 and y-fits.yoff lt fits.ny) then begin
    
    par=reform(fits.data(x-fits.xoff,y-fits.yoff,*))
    helix,struct_ipt=ipt,x=x,y=y,parin=par,/comp_store, $
          fitprof=fitprof,obsprof=obsprof,compprof=compprof; ,/idl
  endif else print,'fits_compute_helix: pixel ',x,y,' not found.'
;plot,fitprof.i & print,max(fitprof.i)&   stop
  
end
