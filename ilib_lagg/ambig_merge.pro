;called from ambig_multi:
; combines the ambig results from several runs with different seed
; values contained in the directories 001, 002, ...
; and writes a final result depending on how reliable the flipping was
pro ambig_merge,percent=flipperc
                                ;combine solutions to get best
                                ;solution
  mf0=file_search('noflip/magfield_ltau*.fits',count=cnt0)
  if n_elements(flipperc) eq 0 then flipperc=90.
  for i=0,cnt0-1 do begin
    tmf=strmid(mf0[i],strpos(mf0[i],'noflip/')+7,100)
    print,'Checking flips for '+tmf
    mf=file_search('???/'+tmf,count=cnt)
    data=readfits(mf0[i],header)
    sz=size(data,/dim)
    flip=intarr(sz[0],sz[1])
    for j=0,cnt-1 do begin
      fdata=readfits(mf[j])
      flip=flip + (abs(fdata[*,*,2]-data[*,*,2]) gt 90)
    endfor
                                ;flipperc% of all runs must have flip
    flipnum=fix(cnt*flipperc/100.)
    goodflip=flip ge flipnum
    aziflip=(data[*,*,2]+goodflip*180)
    im=where(aziflip ge 180)
    if im[0] ne -1 then aziflip[im]=aziflip[im]-360
    data[*,*,2]=aziflip
    ofile=(strsplit(tmf,'.fits',/regex,/extract))[0]+'_ambig.fits'
    writefits,ofile,data,header,append=0
    print,'Wrote ambiguity resolved data to '+ofile
    print,'Total flips: ',total(flip ge 1)
    print,'Total good flips: ',total(goodflip)
    print,'Required flip percentage per pixel: ',flipperc
    print,'Required # of flips per pixel: ',flipnum
    print
  endfor
  
  print,'Now run IDL routine ''ambig_combine'' to create a ambiguity-resolved inverted_atmos.fits'
  print,'Usage: '
  print,'   ambig_combine,''inverted_atmos.fits'',''magfield_ltau*_ambig.fits'',''inverted_atmos_ambig.fits'',/ps' 
  
end
