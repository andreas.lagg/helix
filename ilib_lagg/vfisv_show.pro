;example:
;vfisv_show,fits='/data/sunrise/ScienceFlight20090608/Imax/level2/tino/Vfisv_163_208_FieldStrength.fits.gz',window=[250,250,450,450]
pro vfisv_show,ps=ps,fits=fits,window=window
  
  
  psfile=strmid(fits,strpos(fits,'/',/reverse_search)+1,strlen(fits))
  psfile='/scratch/lagg/ps/'+strmid(psfile,0,strpos(psfile,'.fits'))+'.ps'
  psset,ps=ps,size=[20,16],file=psfile
  !p.charsize=1.2
  @greeklett
  
  img=readfits(fits,header)
  types=['DopplerWidth','Eta0','FieldAzim','FieldIncl','FieldStrength','LosVelocity','MacroTurb','MagnFillFac','SrcFuncCont','SrcFuncGrad','Damping']
  unit=['m'+f_angstrom,'',f_grad,f_grad,'G','km/s','km/s','','','','']
  name=[f_delta+f_lambda+'!LD!N',f_eta+'!L0!N','Azimuth','Inclination','B','v!LLOS!N',f_xi+'!LMac','FF','S!L0!N','S!LGrad!N','Damping']
  scl=fltarr(n_elements(types))
  scl(*)=1.
  scl(5)=1e-5
  scl(6)=1e-5

  
  
  idx=-1 & zt=''
  title=strmid(fits,strpos(fits,'level2/')+7,strlen(fits))
  for i=0,n_elements(types)-1 do if strpos(fits,types(i)) ge 0 then idx=i
  if idx ne -1 then begin
    zt=name(idx)+' ['+unit(idx)+']'
  endif
  
  
  sz=size(img)
  if n_elements(window) ne 4 then window=[0,0,sz(1)-1,sz(2)-1]
  img=img(window(0):window(2),window(1):window(3))
  img=img*scl(idx)
  
  sz=size(img)
  rg=minmaxp(img,perc=99.5)
  if abs(min(rg)-max(rg)) le 1e-5 then rg=rg(0)+[-1.,1.]
  
  if idx eq 5 then $
    userlct,neutral=0,glltab=7,center=256.*(0.-rg(0))/(rg(1)-rg(0)),/full $
  else if idx eq 2 then $
    userlct,/nologo,glltab=8,/full $
  else userlct,/full
  
  image_cont_al,img,zrange=rg,/aspect,title=title,ztitle=zt,contour=0, $
    /cutaspect,barpos=0,xrange=window([0,2]),yrange=window([1,3])
  
  psset,/close
end
