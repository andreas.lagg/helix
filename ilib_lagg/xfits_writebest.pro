pro writenewfits,new=new,old=old,data=data,cancel=cancel
  
  cancel=1
  repeat begin
    openw,unit,/get_lun,new,error=error
    if error ne 0 then begin
      yn=dialog_message(/question,['No permission to write file',new, $
                                   'Save in different location?'])
      if yn eq 'No' then return else begin
        f=get_filepath(new)
        pth=dialog_pickfile(file=f.name,path=f.path,/directory)
        if pth eq '' then return
        fnew=get_filepath(pth)
        new=fnew.path+f.name
      endelse
    endif else free_lun,unit
  endrep until error eq 0
  cancel=0
  
  mrd_head,old,header
  write_fits_dlm,data,new,header,append=0,/create
  
  iext=1
  repeat begin
    exdata=readfits(old,exhead,ext=iext)
    error=n_elements(exdata) eq 1 and exdata(0) eq -1
    if error eq 0 then $
;      write_fits_dlm,exdata,new,exhead,/append
      writefits,new,exdata,exhead,/append
    iext=iext+1
  endrep until error ne 0
  print,'Wrote new FITS file: '
  print,'    ',new
  print,'    Dimension: ',size(data,/dim)
end

pro xfits_writebest
  common fits,fits
  common xfwg,xfwg
  common bfitidx,bfitidx
  @read_fitsstokes_common.pro

  xfits_stokesplot,0,0    ;make sure that the profile data are read in
  
                                ;stop
  print,' Write out best fit data to single fits file - not yet implemented!'
  
  typ=xfwg.parmodestring(xfwg.parmode.val)
  print,'Writing out fits files containing only '''+typ+''' pixels...'
  ext=xfwg.parmodeshort(xfwg.parmode.val)
  
  ipidx=where(fits.pixrep eq 0)
  npar=n_elements(ipidx)
  par=fits.par(ipidx)
  
  
  atm_file=strmid(fits.file,0, $
                  strpos(fits.file,'.fits',/reverse_search))+ext+'.fits'
  prof_file=strmid(xfwg.synth.str,0, $
                   strpos(xfwg.synth.str,'.fits',/reverse_search))+ext+'.fits'
  
  
  data=fltarr(fits.nx,fits.ny,npar)
;  szp=size(dfits1.i)
  szp=size(*dfits1.data)
  prof=fltarr(szp(1),4,fits.nx,fits.ny)
  
  bfi=bfitidx / (fits.nx*fits.ny)
  nwl=szp(1)
  for ix=0,fits.nx-1 do for iy=0,fits.ny-1 do begin
    data(ix,iy,*)=fits.data(ix,iy,bfi(ix,iy)*npar:bfi(ix,iy)*npar+npar-1)
  endfor
;  prof(*,0,*,*)=dfits1.i
; prof(*,1,*,*)=dfits1.q
; prof(*,2,*,*)=dfits1.u
; prof(*,3,*,*)=dfits1.v
  for i=0,3 do $
    prof(*,i,*,*)=reform((*dfits1.data)(*,dfits1.order(i),*,*))
;  prof(*,0,*,*)=reform((*dfits1.data)(*,dfits1.order(0),*,*))
  if mean(prof(*,0,*,*)) le 2 then $
  for iw=0,nwl-1 do prof(iw,0,*,*)=prof(iw,0,*,*)*dfits1.icont

  writenewfits,new=atm_file,old=fits.file,data=data,cancel=cancel
  if cancel eq 0 then begin
    fatm=get_filepath(atm_file)
    fprof=get_filepath(prof_file)
    prof_file=fatm.path+fprof.name
    
    writenewfits,new=prof_file,old=xfwg.synth.str,data=prof
  endif
  
end
