pro hinode_pointing,file,dtmin=dt
  
  if n_elements(file) eq 0 then begin
    print,'Plot Hinode SOT pointing'
    print,'Download pointing files here: '
    print,'  '+ $
         'http://www.isas.jaxa.jp/home/solar/hinode_op/hinode_daily_events.php'
    print,'Usage: '
    print,'hinode_pointing,''re-point_201306010100.txt'''
  endif
  dir='~/work/polarimeter/hinode/pointing/'
  
  
                                ;current time
  cdat=systime(0,/utc,/julian)-julday(01,01,1979,0,0,0)+1
  
    tday={time:(cdat-long(cdat))*86400d*1000,day:long(cdat)}
    ctime=anytim(tday,/yohkoh)
                                ;search for pointing files
    if n_elements(file) eq 0 then begin
      fs=file_search(dir+'re-point*.txt',count=cnt)
    endif else fs=file_search(file,count=cnt)
    if cnt eq 0 then message,'No pointing files found.'
  
                                ;use all future files and one past
                                ;file
  ff=get_filepath(fs)
  fdat=strmid(ff.name,11,2)+'/'+strmid(ff.name,13,2)+'/'+strmid(ff.name,15,2)
  fday=anytim(fdat,/ints)
  fday=fday[sort(fday.day)]
  ff=ff[sort(fday.day)]
  good=fday.day ge tday.day 
  
  dayback=0
  repeat begin
    iold=max(where(fday.day lt (tday.day-dayback)))
    if iold[0] ne -1 then good[iold[0]]=1
    igood=where(good eq 1)
    if igood[0] eq -1 then message,'No Pointing file for current date found.'
                                ;check if first file contains pointing info
    solb_read_repoint, fs[igood[0]], repoint 
    pinfo=max(strpos(repoint.timeline.activity,'ORe-point') eq 0)
    dayback=dayback+1
  endrep until pinfo eq 1 or iold eq 0
  if pinfo eq 0 then message,'First pointing file does not contain pointing information.'
  
  ff=ff[igood]
  fday=fday[igood]
  
  print,'Used Hinode pointing files: '
  print,ff.name,format='(a)'
  wait,2
  
  
                                ;search for *.tim files to get offset
  fftim=file_search(dir+'SOT-*.tim',count=cnt)
  nooff=1
  if cnt ge 1 then begin
    ftim=get_filepath(fftim)
    tim=strmid(ftim.name,11,2)+'/'+strmid(ftim.name,13,2)+'/'+ $
         strmid(ftim.name,15,2)
    tim=anytim(tim,/ints)
    offset=fltarr(2,cnt)+!values.f_nan
    for i=0,cnt-1 do begin
      openr,unit,/get_lun,fftim[i]
      repeat begin
        a='' & readf,unit,a
        str='estimated SOT offset, add to S/C position to get solar:'
        pos=strpos(a,str)
        if pos ne -1 then begin
          offstr=strmid(a,pos+strlen(str)+1,100)
          offset[*,i]=float(strsplit(offstr,/extract,'(,)'))
          nooff=0
        endif
      endrep until eof(unit) or finite(offset[0,i])
      free_lun,unit
    endfor
  endif
  
  
  if nooff then begin
    message,/cont,'WARNING - no tim file found. SOT-*.tim files are required for the offset between SC and SOT pointing.'
    print,'Press key to continue'
    key=get_kbrd()
  endif
  
  
  if n_elements(dt) eq 0 then dt=10. ;time step in minutes
  
  nff=n_elements(ff)
  time0=strarr(nff)
  time1=strarr(nff)
  for i=0,nff-1 do begin
    file=ff[i].path+ff[i].name
  
    openr,unit,/get_lun,file
    repeat begin
      a='' & readf,unit,a
      if strpos(a,'Fm:') eq 0 then time0[i]=strmid(a,4,100)
      if strpos(a,'To:') eq 0 then time1[i]=strmid(a,4,100)
    endrep until eof(unit) or time1[i] ne ''
    free_lun,unit
  endfor
  
  time=[min(time0),max(time1)]
  t0=anytim(time0,/ints)
  t1=anytim(time1,/ints)
  tint=anytim(time,/ints)
  nt=1440.*(tint[1].day-tint[0].day+1)/dt+1
  tsec=long(lindgen(nt)/1440.*dt*86400)+(tint[0].day-1)*86400d
  tvec=anytim(tsec,/ints)
  solxy=fltarr(2,nt)
  
  
  for i=0,nff-1 do begin
    file=ff[i].path+ff[i].name
    
  ;find best offset
    if nooff eq 0 then begin
      dummy=min(tim.day-t0[i].day,ioff)
      off=offset[*,ioff]
    endif else off=0
      solb_read_repoint, file, repoint 
      tl=repoint.timeline
      tlsec=anytim(tl.t)
;      iup=where(strmid(tl.activity,0,15) EQ 'AOCS Mem-Upload',nt)
;      nt=nt+1      
      nt=min(where(strpos(tl.activity,'Re-point') ne -1))
      if nt[0] ne -1 then begin
        tt=anytim([tl[nt].t,repoint.tend])
        intime=where(tsec ge tt[0] and tsec le tt[1])
        if intime[0] ne -1 then for it=0,n_elements(intime)-1 do begin
          itt=max(where(tlsec le tsec[intime[it]]))>0
          cxy=solb_spacecraft_pointing(file,anytim(tvec[intime[it]],/yohkoh))
          if tl[itt].tc eq 0 then cxy=reverse(cxy)
          solxy[*,intime[it]]=cxy+off
          print,anytim(tvec[intime[it]],/yohkoh),' ',solxy[*,intime[it]]
        endfor
      endif
  endfor
  
  
  pfile=strarr(tint[1].day-tint[0].day+1)
  for i=0,nff-1 do begin
  for day=tint[0].day,tint[1].day do begin
    if day ge t0[i].day and day le t1[i].day then begin
      id=day-tint[0].day
      tday={time:0l,day:day}
      date=anytim(tday,/ccsds)
      pfile[id]=ff[i].path+strmid(date,0,10)+'.txt'
      openw,unit,/get_lun,pfile[id]
      printf,unit,'Hinode pointing file    Created: '+systime(0)
      printf,unit,'pointing file: '+file
      printf,unit,'Offset: ',off
      printf,unit,'Time','Solar-X','Solar-Y',format='(a16,2a10)'
      intime=where(tvec.day eq tday.day)
      if intime[0] ne -1 then for it=0,n_elements(intime)-1 do begin
        printf,unit,anytim(tvec[intime[it]],/ccsds),solxy[*,intime[it]], $
               format='(a16,2f10.2)'
      endfor
      free_lun,unit
    endif
  endfor
  endfor
  print,'Pointing information written to:'
  print,transpose(pfile)
;endif 
;endfor 
end
