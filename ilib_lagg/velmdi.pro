;MDI like velocity calibration. Based on vel4.pro from SSW


FUNCTION mdivel,I1,I2,I3,I4,lambda=lambda,dlambda=dlambda
;Creates line center and velocity images
;Updated & revised by TDT  10/30/90
;Increased efficiency by IZ 20.9.92
;6768 -120, -40, +40, +120:  I1, I2, I3, I4
;LAMBDA=6767.8  
;DLAMBDA=0.080
FACT=(2.*2.997925E8*DLAMBDA)/(3.1415926535*LAMBDA)
;
;print,'Starting Velocity'
V=ATAN(-float(I1)-float(I2)+float(I3)+float(I4),$
  float(I1)-float(I2)-float(I3)+float(I4))*FACT
RETURN,V
END
