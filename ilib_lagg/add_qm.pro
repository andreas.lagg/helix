;adds '?' to filename in TIP format
function add_qm,file
  fqm=file
  ccpos=max(strpos(file,'-01cc'))
  if ccpos ne -1 then $
    fqm=strmid(file,0,ccpos+1)+'??'+strmid(file,ccpos+3,strlen(file))
  
  return,fqm
end
