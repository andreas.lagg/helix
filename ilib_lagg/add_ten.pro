function add_ten,rg,perc=perc
  
  if n_elements(perc) eq 0 then perc=10.
  dr=rg(1)-rg(0)
  return,rg + perc*[-0.01,0.01]*dr
end

