
FUNCTION Coord,r,r_c,Offset				   
 						       
  x1D=dindgen(r)			   ;  0. <= x < +r. (r pxl)     
  x = dblArr(2*r,2*r)			   ; 2D index in x-direction.

  x1D = [-Reverse(x1D+1.0),x1D]	       	   ; -r. <= x < +r. (2r pxl)
  x1D = x1D - Offset
  x1D = x1D / r_c			   ;-r/rc<= x <+r/rc (2r pxl)

  FOR i=0,2*r-1 DO BEGIN
    x(0,i) = x1D     
  END
  
  x1D = 0					   ; Save space!
  
  Return, x 

END

FUNCTION Radius,r,r_c,xOffset,yOffset		   ; Zero at the origin, goes
   						   ; linearly to unity at r_c.  
   x = coord(r,r_c,xOffset)
   y = coord(r,r_c,yOffset)
   y = transpose(y)					   ; 2D index in y-direction.

   rr = Sqrt(x*x+y*y)

   x = 0					   ; Save space!
   y = 0

   Return, rr

END

function UnitRadius,r,r_c,xOffset,yOffset	   ; Radius(r,r_c) but in unit circle

  Return, radius(r,r_c,xOffset,yOffset)
  
  x = coord(r,r_c)				   ; x coordinates...
  x = x(r:*,r:*)				   ; ...in first quadrant
  y = transpose(x)					   ; Ditto y
  
  th = angle(r,r_c)
  th = th(r:*,r:*)
  
  indx = IndGen(long(x))
  Size = Dimen(x,0)
  PixelLength = 1.0 / r_c
  
  ReDim,x,num_elem(x)				   ; Work in 1D array
  ReDim,y,num_elem(y)
  
  ur = Sqrt(x*x+y*y)				   ; Set to radial coordinate
  
  xl = x - PixelLength / 2.0			   ; Corners
  xh = x + PixelLength / 2.0   
  yl = y - PixelLength / 2.0
  yh = y + PixelLength / 2.0
  
  rl = Sqrt(xl*xl+yl*yl)
  rh = Sqrt(xh*xh+yh*yh)				   
  
  pixel_inside  = rh LT 1.0
  pixel_outside = rl GT 1.0
  pixel_on_border = icke(pixel_inside OR pixel_outside)
  
  N_inside    = Long(Total(pixel_inside))
  N_on_border = Long(Total(pixel_on_border))
  N_outside   = Long(Total(pixel_outside))
;  Type,'Inside, on border, and outside: ',N_inside,N_on_border,N_outside
  
  indx_inside    = sieve(indx,pixel_inside)
  
  IF N_outside GT 0 THEN BEGIN
    indx_outside   = sieve(indx,pixel_outside)
    ur(indx_outside) = 0.0			   ; Completely outside = 0
  END
  
;  stats,ur,'ur - 000'
  
  IF N_on_border GT 0 THEN BEGIN
    indx_on_border = sieve(indx,pixel_on_border)
    xl = xl(indx_on_border)			   ; From this point only 
    xh = xh(indx_on_border)			   ; points on the border 
    yl = yl(indx_on_border)			   ; are interesting.
    yh = yh(indx_on_border)
    
    stats,xl,'xl'
    stats,xh,'xh'
    stats,yl,'yl'
    stats,yh,'yh'
    
    stats,rl,'rl'
    stats,rh,'rh'

    
    y1 = sqrt(1.0 - xl*xl)			   ; Crossing points
    x2_sq = 1.0 - yh*yh
    y3_sq = 1.0 - xh*xh
    x4 = sqrt(1.0 - yl*yl)
    
;    stats,1.0 - xl*xl,'1.0 - xl*xl'
;    stats,1.0 - xh*xh,'1.0 - xh*xh'
;    stats,1.0 - yl*yl,'1.0 - yl*yl'
;    stats,1.0 - yh*yh,'1.0 - yh*yh'
    
    a = xl					   ; Center of gravity coordinates
    ;    b = xl > x2				   ; max[xl,x2]
    b = sqrt(xl*xl > x2_sq)			   ; max[xl,x2]
    c = x4 < xh					   ; min[x4,xh]
    
;    stats,b,'b'
    
    a2 = a*a
    b2 = b*b
    c2 = c*c
    b3 = b2*b
    c3 = c2*c
    
    xT = (yh-yl)*(b2-a2)/2.0 + ( x4*(c2-b2)/2.0 - (c3-b3)/3.0 )*(y1-yl)/(x4-xl)
    
    xT = xT / ((yh-yl)*(xh-xl))
    
    a = yl
    ;    b = yl > y3				   ; max[yl,y3]
    b = sqrt(yl*yl > y3_sq)			   ; max[yl,y3]
    c = y1 < yh					   ; min[y1,yh]
    
    stats,a-b,'a-b'
    stats,b-c,'b-c'
    
    a2 = a*a
    b2 = b*b
    c2 = c*c
    b3 = b2*b
    c3 = c2*c
    yT = (xh-xl)*(b2-a2)/2.0 + ( y1*(c2-b2)/2.0 - (c3-b3)/3.0 )*(x4-xl)/(y1-yl)
    
    yT = yT / ((xh-xl)*(yh-yl))
    
    rT = Sqrt(xT*xT+yT*yT)
    
    stats,xT,'xT'
    stats,yT,'yT'
    stats,rT,'rT'
    
    stats,xT-abs(xl),'xT-|xl|'
    stats,yT-abs(yl),'yT-|yl|'
    
    ur(indx_on_border) = rT
    
  END
  
;  stats,ur,'ur - 123'
  
  ReDim,ur,Size,Size
  
  ur = ur * (th LT !pi/4) + transpose(ur) * (th GE !pi/4) 

  uur = Zero(FltArr(2*Size,2*Size))		   ; All quadrants in output
  Insert,uur,ur,Size,Size
  Insert,uur,Reverse(ur,1),Size,1
  Insert,uur,Reverse(ur,0),1,Size
  Insert,uur,Reverse(Reverse(ur,0),1),1,1
  
;  stats,uur,'uur'
  
  
  a = 0	
  a2 = 0        			   ; Save memory space
  b = 0	
  b2 = 0       
  b3 = 0
  c = 0	
  c2 = 0       
  c3 = 0
  
  xT = 0	
  yT = 0       
  rT = 0
  
  xl = 0	
  y1 = 0
  xh = 0	
  x2 = 0
  yl = 0	
  y3 = 0
  yh = 0	
  x4 = 0
  
  indx = 0
  
  pixel_inside    = 0	   
  indx_inside    = 0
  pixel_outside   = 0	   
  indx_outside   = 0
  pixel_on_border = 0	   
  indx_on_border = 0
  
  Return, uur
end

function Zernike_mn,j
  m = [0]
  n = [0]
  i = 0
  REPEAT BEGIN
    i += 1
    d = 0.0*IntArr(i+1)
    IF (i mod 2) eq 1 THEN FOR ii=0,i/2 DO BEGIN
      d(2*ii) = 2*ii+1
      d(2*ii+1) = 2*ii+1
    END ELSE FOR ii=1,i/2 DO BEGIN
      d(2*ii-1) = 2*ii
      d(2*ii) = 2*ii
    END
    m = [m,d]
    n = [n,0.0*d+i]
  END UNTIL (size(n))[1] GE j
  
  Return, [m(j-1),n(j-1)]
  
end

FUNCTION Angle,r,r_c,xOffset,yOffset,rotangle		
						   
  x = coord(r,r_c,xOffset)			   ; x coordinates
  y = coord(r,r_c,yOffset)			   ; y coordinates
  y = transpose(y) 
  
  IF rotangle THEN BEGIN
    xold = x
    yold = y
    x = xold*cos(rotangle) + yold*sin(rotangle)
    y = yold*cos(rotangle) - xold*sin(rotangle)
  END
  
  mask = abs(x) LT 1e-40
  x = x - mask*(x - 1e-40)
  ang = atan(y,x)
  
  x = 0					   ; Save space!
  y = 0
  
  Return, ang

END

function zernike,r,r_c,j 
  rotangle=0.0D0
  xOffset=0.0D0
  yOffset=0.0D0

  rd = UnitRadius(double(r),double(r_c),xOffset,yOffset)
  th = angle(r,r_c,xOffset,yOffset,rotangle)
  support_plus = rd NE 0			   ; NEW <-----------

  print,'Zernike_function -- Zernike function '+string(j)
    
  mn = Zernike_mn(j)
  m = mn(0)
  n = mn(1)

  s = IndGen((n-m)/2+1)
  
  IF m EQ 0 THEN BEGIN
    c = sqrt(n+1)
  END ELSE BEGIN
    c = sqrt(2*(n+1))
    IF (j mod 2) eq 1 THEN c = c * sin(m*th) ELSE c = c * cos(m*th)
  END
  z = 0.0*(rd)
  FOR s=0,(n-m)/2 DO BEGIN
    rx = rd^(n-2*s)
    tmp = ((-1)^s*Factorial(n-s)) / (Factorial(s)*Factorial((n+m)/2-s)*Factorial((n-m)/2-s))
    z += rx * tmp
  END 
  z = z*c
  c = 0
  rx = 0

;  z = z / Sqrt(Total(support * z*z)/Total(support))
  return,z
END
