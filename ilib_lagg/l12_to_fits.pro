;convert l12 sav files from Valentin to 4D fits files for use with
;Spinor / Helix
;example:
;l12_to_fits,'/data/sunrise/ScienceFlight20090608/Imax/level2/2009y_06m_10d/l12_2/data_l12-2/redu*.save',dir_out='~/tmp/2009y_06m_10d_fits4D/'
pro l12_to_fits,sav,dir_out=dir_out
  
  if n_elements(dir_out) eq 0 then dir_out='~/tmp/'
  
  if strlen(dir_out) ge 1 then $
    if strmid(dir_out,strlen(dir_out)-1,1) ne '/' then dir_out=dir_out+'/'
  spawn,'mkdir -p '+dir_out
  
  file=file_search(sav,count=nsav)
  if nsav eq 0 then message,'No sav file found: ',sav
  
  for is=0,nsav-1 do begin
    print,'Processing '+file(is)
    if n_elements(iid) ne 0 then dummy=temporary(iid)
    restore,file(is)
    sz=size(iid)
    if sz(0) eq 0 then begin
      print,'sav file does not contain IMaX spectra.'
    endif else begin
      lslpos=strpos(file(is),'/',/reverse_search)
      fits=strmid(file(is),lslpos+1,strlen(file(is)))
      fits=strmid(fits,0,strpos(fits,'.sav'))
      id=strsplit(fits,'_',/extract)
      id=string(id(2),format='(i3.3)')+'_'+string(id(3),format='(i3.3)')
      fits4d=dir_out+'imax_'+id+'.fits'
      
;XTALK V->I      
;      iid(*,*,*,0)=iid(*,*,*,0)+0.342424*iid(*,*,*,1)
;      fits4d=dir_out+'imax_'+id+'.xtalkV2I'+'.fits'
      
      writefits,fits4d,iid,append=0
      spawn,'gzip -f '+fits4d
      print,'Wrote FITS file: ',fits4d+'.gz'
    endelse
    
  endfor
  
end
