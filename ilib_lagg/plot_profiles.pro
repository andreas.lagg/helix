pro plot_profiles,p0,p1,p2,p3,p4,p5,p6,p7,p8,p9, $
                  p10,p11,p12,p13,p14,p15,p16,p17,p18,p19, $
                  weight=weight, $
                  unno_weight=unno,line=line,title=title,blend=blend,gen=gen, $
                  ipt=ipt,atm=atm,scale=scale,win_nr=win_nr, $
                  fitness=fitness,x=x,y=y,lthick=lthick_in, $
                  lstyle=lstyle_in,color=clr_in,psym=psym_in,_extra=extra, $
                  iquv=iquv,iic=iic,bw=bw,symsize=symsize,range=yrange, $
                  wl_range=wl_range,fraction=fraction,charsize=charsize, $
                  icont=icont,markers=markers,label=label,normi=normi, $
                  wintitle=wintitle,return_yrange=ret_yrg, $
                  mstyle=mstyle,mcol=mcol,position=position, $
                  titx=xtitle_in,tity=ytitle_in
  common color,color
  common prof_set,prof_set
  common sclwg,sclwg,wgtfile

  och=!p.charsize
  if n_elements(charsize) ne 0 then !p.charsize=charsize $
  else if n_elements(prof_set) ne 0 then !p.charsize=prof_set.charsize
  
  npp=n_params()  
  np=0
  for i=0,npp-1 do dummy=execute('np=np+n_elements(p'+n2s(i)+')')
;  np=npp
  if np eq 0 then begin
    if n_elements(color) ne 0 then ocolor=color
    if n_elements(yrange) ne 0 then oyrange=yrange
    if n_elements(prof_set) ne 0 then oprof_set=prof_set
    if n_elements(wl_range) ne 0 then owlrange=wl_range
    fi=file_info('plottmp.sav')
    if fi.exists then restore,'plottmp.sav'
    if n_elements(ocolor) ne 0 then color=temporary(ocolor)
    if n_elements(oprof_set) ne 0 then prof_set=temporary(oprof_set)
    if n_elements(oyrange) ne 0 then yrange=temporary(oyrange)
    if n_elements(owlrange) ne 0 then wl_range=temporary(owlrange)
  endif

  
  if n_elements(icont) eq 0 then show_icont=0 $
  else show_icont=keyword_set(icont)
  @greeklett.pro
;  greek
;  common greek_letters
  
  
  pflag=n_elements(prof_set) eq 1
  if pflag then show_icont=prof_set.show_cont
  if pflag then iic=prof_set.norm2ic eq 1
  if pflag then wlunit=prof_set.wlunit else wlunit=0
  
  if keyword_set(bw) eq 1 then color=0 else color=1
  if n_elements(symsize) eq 0 then symsize=0.3
  
  if n_elements(iquv) eq 0 then iquv='IQUV'
  if pflag then iquv=prof_set.show
  
  iquv=strcompress(strupcase(iquv(0)),/remove_all)
  npr=strlen(iquv)
  
  nwlmax=0
  for i=0,np-1 do begin
    dummy=execute('nelp=n_elements(p'+n2s(i)+')')
    if nelp gt 0 then dummy=execute('nwlmax=n_elements(p'+n2s(i)+'.wl)>nwlmax')
  endfor
  if nwlmax eq 0 then return
  
  prf={wl:dblarr(nwlmax),i:dblarr(nwlmax),v:dblarr(nwlmax), $
       q:dblarr(nwlmax),u:dblarr(nwlmax),nwl:0l}
  profile=replicate(prf,np)
  
  if n_elements(lstyle_in) ne np then lstyle=0+intarr(np) else lstyle=lstyle_in
  if n_elements(clr_in) ne np then begin
    clr=indgen(np)
    clr(0)=9
  endif else clr=clr_in
  
  if n_elements(psym_in) eq 0 then psym=0 else psym=psym_in
  if n_elements(psym) ne np then psym=0+intarr(np)
  if n_elements(prof_set) ne 0 then lthick=prof_set.thick else lthick=1
  if n_elements(lthick_in) ne 0 then lthick=lthick_in
  if n_elements(lthick) ne np then lthick=lthick(0)+intarr(np)
  
  
  
  
  icnt=fltarr(np)+1.
  for i=0,np-1 do begin
    dummy=execute('nelp=n_elements(p'+n2s(i)+')')
    if nelp ne 0 then begin
      dummy=execute('cp=p'+n2s(i))
      struct_assign,cp,prf
      
      if keyword_set(iic) and n_elements(line) ne 0 then begin
        prf.i=prf.i/line(0).icont 
        icnt(i)=1.
      endif else begin
        if max(tag_names(cp) eq 'IC') then begin
          icnt(i)=cp.ic
          if keyword_set(iic) then begin
            prf.i=prf.i/cp.ic
            icnt(i)=1.
          endif
        endif else iic=0
      endelse
      prf.nwl=n_elements(cp.wl)
      profile(i)=prf
    endif
  endfor
  ipmax=(where(profile.nwl eq nwlmax))(0)
  
  tn=tag_names(prf)
  tprof='WL'
  for i=0,npr-1 do tprof=[tprof,strmid(iquv,i,1)]
  ntn=n_elements(tprof)
  
  if n_elements(win_nr) ne 1 then begin
    if n_elements(win_nr) eq 0 then win_nr=2
    if n_elements(prof_set) ne 0 then if prof_set.win_nr ne -1 then $
      win_nr=prof_set.win_nr
  endif
  userlct
  if n_elements(wintitle) eq 0 then wintitle='HeLIx+ Profiles'
  if !d.name ne 'PS' then begin
    screen=get_screen_size()
    ys=(screen(1)*0.80)<720
    xs=ys/26.*18.
    catch,error
    if error ne 0 then window,win_nr,xsize=xs,ysize=ys,title=wintitle
    wset,win_nr
    catch,/cancel
    if n_elements(prof_set) ne 0 then prof_set.win_nr=win_nr
    erase
  endif 
  greek
  
  atmf=0
  if n_elements(atm) ne 0 and n_elements(scale) ne 0 then atmf=1
  if pflag then if prof_set.show_info eq 0 then atmf=0
  
  pos=[0,0,1,.99-n_elements(title)*0.005]
  pos(1)=atmf*0.2
  if n_elements(position) ne 4 then position=pos
  newmask=1

  wl_cent=(total(profile(0).wl)/n_elements(profile(0).wl))(0) ;range for heI  
  if wl_cent gt 10828 and wl_cent lt 10833 then he_rg=1 else he_rg=0
  nell=n_elements(line)
  if nell ne 0 then begin
    name=strarr(nell)
    if max(tag_names(line) eq 'ID') then begin
      if max(strpos(line.id,'He') ne -1) eq 1 then he_rg=1 else he_rg=0
      for il=0,nell-1 do $
        name(il)=(strsplit(string(line(il).id),' ',/extract))(0)
      telluric=intarr(n_elements(name))
    endif
    if max(tag_names(line) eq 'NAME') then begin
      for il=0,nell-1 do $
        name(il)=(strsplit(line(il).name,' ',/extract))(0)
      telluric=line.telluric
    endif
  
                                ;special treatment of He triplet
  if max(tag_names(line) eq 'ID') eq 1 then begin
    if he_rg then for  il=0,nell-1 do begin
      case string(line(il).id) of
        'HeI 10829.1': name(il)='Tr1'
        'HeI 10830.2': name(il)='Tr2'
        'HeI 10830.3': name(il)='!CTr3'
        'He1 10829.09' : name(il)='Tr1'
        'He1 10830.25': name(il)='Tr2'
        'He1 10830.34': name(il)='!CTr3'
        else:
      endcase
    endfor
  endif
  endif
  
  yrg=fltarr(2,npr)
  ytit=strarr(npr)
  icadd='/I!LC!N'
  if keyword_set(normi) then icadd='/I'
  for it=1,ntn-1 do begin
    for ip=0,n_elements(profile)-1 do if profile(ip).nwl gt 0 then begin
      i=(where(tn eq tprof(it)))(0)
      if tn(i) eq 'I' then $
        rg=add_ten([min(profile.(i)), $
                    max([(profile.(i))(*),icnt(*)])]) $
      else begin
        rg=add_ten([-1,1]*(max(abs(profile(ip).(i))) >0.00001 ))
      endelse
      if n_elements(ipt) ne 0 then begin
        get_wgt,obs=profile(ip),wgt=sclwg,/silent
        usewl=where(sclwg.i gt 1e-5 or sclwg.q gt 1e-5 or $
                    sclwg.u gt 1e-5 or sclwg.v gt 1e-5)
        if usewl(0) ne -1 then rg=[min((profile(ip).(i))(usewl)), $
                                   max([(profile(ip).(i))(usewl)])]
        if tn(i) ne 'I' then rg=[-max(abs(rg)),max(abs(rg))] ;else rg(1)=rg(1)>max(icnt(*))        
        rg=add_ten(rg)
      endif
      if (total(abs(rg)) le 1e-8) then rg=[-1e-8,1e-8]
      if ip eq 0 then yrg(*,it-1)=rg  $
      else yrg(*,it-1)=[min([rg,yrg(*,it-1)],/nan),max([rg,yrg(*,it-1)],/nan)]
      case tn(i) of
        'I': if keyword_set(iic) then ytit(it-1)='I'+icadd else ytit(it-1)='I'
        'V': ytit(it-1)='V'+icadd
        'Q': ytit(it-1)='Q'+icadd
        'U': ytit(it-1)='U'+icadd
        else:
      endcase
    endif
  endfor
  
  if min((size(yrange))(0:2) eq (size(yrg))(0:2)) eq 1 then $
    for i=0,npr-1 do begin
    if abs(yrange(1,i)-yrange(0,i)) gt 1e-6 then yrg(*,i)=yrange(*,i)
  endfor

  xrg=[min(profile(ipmax).wl(0:profile(ipmax).nwl-1)), $
       max(profile(ipmax).wl(0:profile(ipmax).nwl-1))]
  for ip=1,np-1 do if profile(ip).nwl gt 0 then $
    xrg=[min(profile(ip).wl(0:profile(ip).nwl-1))<xrg(0), $
         max(profile(ip).wl(0:profile(ip).nwl-1))>xrg(1)]
  rgok=1
  if n_elements(wl_range) eq 2 then begin
    if total(wl_range) gt 1e-5 then xrg=wl_range $
    else rgok=0
  endif
  if n_elements(ipt) ne 0 and rgok eq 0 then begin
    if total(ipt.wl_range) gt 1e-5 then xrg=ipt.wl_range 
  endif
  if pflag then begin
    if prof_set.title ne '' then title=prof_set.title
    if prof_set.show_title eq 0 then title=''
    if prof_set.autorange eq 0 then begin
      if total(abs(prof_set.wlrange)) gt 1e-5 then xrg=prof_set.wl_range
      for i=0,npr-1 do begin
        if abs(prof_set.range(i,1)-prof_set.range(i,0)) gt 1e-5 then $
          yrg(*,i)=prof_set.range(i,*)
      endfor
    endif
  endif
  
  
  unit=([f_angstrom,'nm'])(wlunit)
  xscl=([1.,0.1])(wlunit)
  xscl=xscl(0)
  
  fmargin=[0.15,0.98-0.08*(n_elements(weight) ne 0)]
  
  if n_elements(xtitle_in) eq 1 then xtit=xtitle_in $
  else xtit='Wavelength ['+unit+']'
  if n_elements(ytitle_in) eq 1 then ytit=ytitle_in
  
  ochsz=!p.charsize
  msk=mask_al([1,npr],headtitle=title, $ ;subtitle=sav,headtitle=hdt, $
           xformat='(f15.1)',yformat='(f15.5)', $
           xrange=xrg*xscl, $
           yrange=yrg, $
           fraction=fraction, $
           fix_margins=fmargin, $
           xtitle=xtit, $
           dytitle=ytit, $
           charsize=ochsz,pos=position)
;  !p.multi=[ntn-1+atmf,1,ntn-1+atmf]
  !p.charsize=ochsz
  ret_yrg=yrg                    ;return range
  
  npp=np
  if n_elements(prof_set) ne 0 then if prof_set.show_comps eq 0 then npp=npp<2
  
  first=1 & lcnt=1
  for it=1,ntn-1 do begin
    i=(where(tn eq tprof(it)))(0)
    for ip=0,npp-1 do if profile(ip).nwl gt 0 then begin
      
      
      nwl=profile(ip).nwl
      if lstyle(ip) ne -1 then begin
        doit=1
        if pflag then if prof_set.show_obs eq 0 and ip eq 0 then doit=0
        if pflag then if prof_set.show_fit eq 0 and ip eq 1 then doit=0
        if pflag then if ip ge 2 then doit=prof_set.compnr(ip-2) eq 1
        if doit then begin
          ppsym=-0
          if pflag then if prof_set.fitpsym eq 1 and ip eq 1 then begin
            usym,'circle',charsize=sqrt(!p.charsize*1.5),/fill,color=clr(ip)
            ppsym=-8
          endif
          msk_plot,[[profile(ip).wl(0:nwl-1)*xscl], $
                    [profile(ip).(i)(0:nwl-1)]], $
            msk.plot(0,it-1),/onedim,color=clr(ip),line_thick=lthick(ip), $
            line_style=lstyle(ip),psym=ppsym
        endif
      endif
      
                                ;plot 0-level
      oplot,color=([13,9])(color),xrg*xscl,[0,0],linestyle=1
                                ;plot central WL
      doit=1
      if pflag then if prof_set.show_lines eq 0 then doit=0
      if doit then for il=0,nell-1 do begin
        col=9 & pre=''
        if telluric(il) eq 1 then begin
          pre='!C'
          tladd='!C(telluric)'
          col=3
        endif else tladd=''
        oplot,linestyle=1,[0,0]+line(il).wl*xscl,!y.crange,color=col
        if it eq 1 then $
          xyouts,/data,alignment=0.5,line(il).wl*xscl,!y.crange(1), $
          '!C!C'+pre+name(il)+tladd,charsize=!p.charsize*0.8,color=col
      endfor 
                                ;plot markers
      nmark=n_elements(markers)
      if nmark ge 1 then begin
        if n_elements(mcol) eq 0 then mcol=3
        if n_elements(mcol) ne nmark then mcol=mcol(0)+intarr(nmark)
        if n_elements(mstyle) eq 0 then mstyle=2
        if n_elements(mstyle) ne nmark then mstyle=mstyle(0)+intarr(nmark)
        for im=0,nmark-1 do begin
          oplot,linestyle=mstyle[im],[0,0]+markers(im)*xscl, $
                !y.crange,color=mcol[im]
        endfor
      endif
                                ;plot continuum
      if show_icont eq 1 and it eq 1 then begin
        oplot,linestyle=2,!x.crange,icnt(ip)+[0,0],color=clr(ip)
      endif
      
      
      
                                ;do seperate plot for psyms if too dense
;    nwl=n_elements(profile(0).wl)
      nmax=125
      if nwl lt nmax then symidx=indgen(nwl) $
      else symidx= $
        fix(findgen(nmax)/nmax*nwl+(ip)*float(nwl/((np-1)>1)/nmax))<(nwl-1)
      
      if max(symidx) gt n_elements(profile(ip).(i)) then $
        message,/cont,'not enough WL points' $
      else begin
        msk_plot,[[profile(ip).wl(symidx)*xscl],[(profile(ip).(i))(symidx)]], $
          msk.plot(0,it-1),/onedim,color=clr(ip),psym=psym(ip), $
          line_style=-1, $
          symsize=symsize
      endelse
;      endelse
      
                                ;plot label
      if n_elements(label) eq np then if first then begin
        xlpos=!x.crange(0)+[0.05,0.15]*(!x.crange(1)-!x.crange(0))
        ychsz=(convert_coord(/device,/to_data,[0,0],[0,!d.y_ch_size]))
        ychsz=!p.charsize*abs(ychsz(1,1)-ychsz(1,0))
        ylpos=!y.crange(1)-.1*(!y.crange(1)-!y.crange(0))
        label,/data,xlpos(0),xlpos(1),ylpos-ychsz*1.2*lcnt,label(ip), $
          color=clr(ip),linestyle=lstyle(ip),psym=psym(ip),symsize=symsize, $
          thick=lthick(ip)
        lcnt=lcnt+1
      endif
    endif
    first=0
;     plot,profile(0).wl,profile(0).(i),/xst,/yst, $
;       yrange=rg,ytitle=tn(i),/noerase,title=tit,/nodata
;     for ip=0,np-1 do oplot,profile(ip).wl,profile(ip).(i), $
;       color=clr(ip),linestyle=lstyle(ip),psym=psym(ip),_extra=extra

    
    if n_elements(weight) ne 0 then begin
      dummy=execute('wgt=weight.'+tn(i))
      if total(finite(wgt)) ge 2 then begin
        
        newrg=[0,max(wgt)*1.1]
;        msk.plot(0,it-1).yrange=newrg
        !y.crange=newrg
        
;         msk_plot,[[profile(0).wl],[wgt]],msk.plot(0,it-1), $
;           /onedim,color=2,line_style=2
        
        nn=where(wgt gt 1e-5 or shift(wgt,1) gt 1e-5 or shift(wgt,-1) gt 1e-5)

        if n_elements(nn) gt 2 then begin
          val=bytarr(n_elements(wgt))
          val(nn)=1
          nv=where(val eq 0)
          if nv(0) ne -1 then wgt(nv)=!values.f_nan
;           if nn(0) gt 1 then nn=[nn(0)-1,nn]
;           if nn(n_elements(nn)-1) lt n_elements(wgt)-2 then $
;             nn=[nn,nn(n_elements(nn)-1)+1]
;          plot,profile(0).wl(nn),wgt(nn),xst=5,yst=5, $
          doit=1
          if pflag then if prof_set.show_wgt eq 0 then doit=0
          if doit then begin 
            if max(tag_names(weight) eq 'WL') eq 1 then wwl=weight.wl $
            else wwl=profile(0).wl
            plot,wwl*xscl,wgt,xst=5,yst=5, $
              color=([2,3])(!d.name eq 'PS'), $
              /noerase,linestyle=2,position=msk.plot(0,it-1).n_pos, $
              xrange=!x.crange,yrange=newrg
            
            axpos=(convert_coord(/normal,/to_data,[position(2),0]))(0)
            
            axis,msk.plot(0,it-1).n_pos(2)+0.02,0.,/normal,yax=1, $
              color=([2,3])(!d.name eq 'PS'),/yst, $
              charsize=0.75*!p.charsize,ytitle='weight '+tn(i)
          endif
        endif

        
                                ;plot weighting for unno solution
        if tn(i) ne 'I' then if n_elements(unno) ne 0 then begin
          if tn(i) eq 'V' then uw=unno.inc $
          else uw=unno.azi
          uw=(uw)/(max(uw))*!y.crange(1) ;scale to wgt-axis
;          uw=(uw-min(uw))/(max(uw)-min(uw))* $ ;scale to wgt-axis
;           (!y.crange(1)-!y.crange(0))+!y.crange(0)
          oplot,color=([3,11])(!d.name eq 'PS'),linestyle=1,unno.wl*xscl,uw
        endif
      endif
    endif
    

    !p.multi(0)=(!p.multi(0)-1)    
  endfor
  !p.charsize=och
  
  if atmf then begin            ;output of atmosphere
    ypos=1./(ntn-1-.4+atmf)
    print_atm,/x,atm,scale=scale,ypos=ypos,ipt=ipt,line=line,blend=blend,gen=gen
  endif
  
  doit=1
  if pflag then if prof_set.show_info eq 0 then doit=0
  if doit then begin
    add=''
    if n_elements(fitness) eq 1 then $
      add=add+'Fitness: '+n2s(fitness,format='(f10.2)')
    if n_elements(x) ne 0 and n_elements(y) ne 0 then begin
      xstr=add_comma(n2s(x),sep='-')
      ystr=add_comma(n2s(y),sep='-')
      add=add_comma([add_comma([xstr,ystr] ,sep=' | '),add],sep=', ',/no_empty)
    endif
    if n_elements(ipt) ne 0 then $
      if ipt.wl_bin gt 1 then add=add+' WL-bin: '+n2s(ipt.wl_bin)
    xyouts,/normal,alignment=0,0,0,add,charsize=1.2*!p.charsize
  endif
  
  if !d.name eq 'PS' then begin
    doit=n_elements(ipt) eq 1
    if n_elements(prof_set) eq 1 then $
      if prof_set.show_input eq 0 then doit=0
    if doit then print_ipt,ipt,/erase
  endif  
                                ;test write access
  saveplot=1
  if saveplot then begin
  ptmp='plottmp.sav'
  openw,unit,/get_lun,ptmp, error=err
  if err eq 0 then begin
    free_lun,unit
    save,file=ptmp,/variables,/xdr,/compress
  endif else print,'Cannot store plot.'
  endif
end
