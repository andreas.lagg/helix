;+
; routine msk_plot.pro - 
;         creates tv and contour plot in window created with mask
;INPUTS:
;	a:	      2-dim. array
;		      if a=0  -->  array for labeling is created
;	mapvar:       1-d structure (defined in mask.pro - maskout)
;
;PARAMETERS:
;	[xy]coord:    vector with [xy]-coords for a (increasing values!)
;	onedim:	      if set, data are plotted one-dimensional a(0) vs a(1)
;       zeroline:     if set, a line at x,y=0 is drawn (only if onedim is set)
;       color:	      vector with color indices for contour lines / 1d-plot
;       nlevels:      number of contour levels (default: 6)
;       levels:       vector with values for contour levels (overrides nlevels)
;       line_thick:   thickness of line
;       line_style:   style of line
;       psym:         symbol for plot
;       symsize:      size of symbol
;       label_frame:  if set, frame around labels is drawn (use at last item!)
;	c_labels:     vector specifies which contour levels should be labeled
; nocontour:    if set, no contour lines are drawn
;	noimage:      if set, no tv image is drawn
;	contour_tv:   if set, contour lines with tv colors are drawn
;	novalid:      array [[xnv1,ynv1],[..]] with [xy] coordinates for
;		      not valid data
; nv_color: background color for novalid data
; bg_white: if set, data background is white
; connect: if set, connect valid data points over no valid points
; data_gap: array(2,nr) containing xstart and xend for nr data gaps
; gap_transparent: if set, data are drawn over data gap in onedim-mode
;	ps_bw:      =  -->  if set, colors for tv are inverted (bw output)
;		      else colors are not inverted
; exact: if x or ycoord is specified, the tv plot is not made from an
;        interpolated array but calculated exact (slower)
; exact_ps_quality: res. for ps-output of exact mode tv-plot (min. 16x16)
;                   0 ... full resolution          (e.g. 26700x18500)
;                   n ... reduce resolution by 2^n (n=1: 13350x 9250)
; exact_ps_quality_min: minimum size for tv-image if exact_ps_quality is set
; smooth: smooth array to be displayed as tv
; x_periodic: assume periodicity in x direction for smoothing
; y_periodic: assume periodicity in y direction for smoothing
; vertline: array of values where a vertical line should be drawn (data units)
; vert_string: array of strings to label vertical lines
; vert_color: array of color indices for this vertical lines
; headtitle: seperate head-titles for horizontal plots
; tick_pos: position for ticks on line-plot (hour ticks, ...) in
;           indices of xcoord
; tick_str: string to place next to these ticks
; tick_color: color for tick marks
; mark_val: array of [x,y] coordinates for points to be marked
; mark_sym: vector with symbol numbers for mark
;           ['CIRCLE','TRIANGLE','SQUARE','DIAMOND','MPAE']
; mark_fill: vector with flags for filling mark symbols (1=fill, 0=open)
; mark_col: vector with colors for mark
; mark_label: vector with labels for marks
; mark_region_value: array(2,nr) containing a values for regions to mark
; mark_region_color: background color for marked region
; mark_region_text: label marked regions with 'text'
; vector_pos: position (x,y) of vector to be drawn at line mode in
;             data coordinates
; vector_comp: vector (x,y) components to be drawn at line mode
; vector_system: 0: plot data coord system is used
;                1: xaxis as data coord syste
;                2: yaxis as data coord syste
; vector_length: length of maximum vector in % of y-height
; vector_max: max. length of vector
; vector_color: color of vector
; error_val: array (nr_x,x) containing error bar values (data+err,data-err)
;            for line plot
; error_col: color for error bars (default: same as plot)
; error_width: parameter for errplot (width of error bar)
; error_noline: if set, error bar has no vertical line
; back_fore: if set, changes color index at min/max x-axis value
; bkfr_step: step for increasing color index when back_fore is set
; label_range: specifies z-range used for tv-mode, overrides value in 
;              maskvar.zrange
; additional_text: additional text to be displayed in lower left corner
; box_val: draw color boxes as symbol for line-plot
; box_size: size of color boxes for line-plot in data coord.
; box_mode:  mode for box:  average / overlay
; box_weight: weighting parameter for box in average mode
; box_iso: isotropic z-boxes using data coordinates (defaults), if 0
;           -> device coordinates
;
;OUTPUT:
;	none
;
;COLORS: 
;	Index 0: Image Background,
;       Index 1,..,15: Not used
;	Index 16: No_Valid Data
;	Index 17: Data Background
;	Index !d.table_size-1: Labeling
;
;VORSICHT: mit [xyz]log erstellte Masken haben nur logarithmische Achsen, das
;          Image/Contour wird linear eingezeichnet!!
;-

;labeling line plots
pro outtext,pos,index,color,string,chsz,symbol=symbol,inside=inside, $
            bottom=bottom

  xw=!d.x_ch_size*chsz
  yh=!d.y_ch_size*chsz
  if n_elements(symbol) eq 0 then symbol='-'
  sz=size(symbol)
  if keyword_set(inside) then xp=pos(0)+xw $
  else xp=pos(2)+xw
  yp=pos(3)-yh*(index+0.1)
  if keyword_set(bottom) then yp=pos(1)+yh*(index+0.1)
  if sz(sz(0)+1) eq 7 then begin
    for i=0,n_elements(color)-1 do $
      xyouts,xp+i*xw*1.1,yp,symbol,color=color(i),/device,charsize=.8*chsz
    xyouts,xp+(i*1.1+0.4)*xw,yp,string,color=!p.color,/device,charsize=.8*chsz
  endif else begin
    for i=0,n_elements(color)-1 do $
      plots,xp+i*xw*1.1,yp+0.25*yh,psym=fix(symbol(0)),color=color,/device, $
      symsize=.8*chsz
    xyouts,xp+(i*1.1+0.4)*xw,yp,string,color=!p.color,/device,charsize=.8*chsz
  endelse
end

;output of text
pro box_text,text,maskvar=maskvar,xdev=xdev,ydev=ydev,arrow=arrow, $
             charsize=charsize,center=center,text_color=text_color
  
  if n_elements(charsize) eq 0 then charsize=maskvar.charsize
  if n_elements(text_color) eq 0 then text_color=!p.color
  
  wx=get_textwidth(text,charsize)
;   exc=[where(byte(text+'!C') eq 33)+1, $
;        where(byte(text+'!C') eq 67)]
;   lines=n_elements(exc)-n_elements(remove_multi(exc))
  lines=count_substring(text+'!C','!C')
  cx=!d.x_ch_size*charsize
  cy=!d.y_ch_size*charsize
  xs=wx+.5*cx & ys=(lines+0.2)*cy
  if n_elements(xdev) eq 2 then xc=(xdev(0)+xdev(1))/2. $
  else if keyword_set(center) then xc=xdev(0)  $
  else xc=xdev+xs/2.+2*cx
;        yc=maskvar.position(3)-!d.y_ch_size*charsize*1.5
  if n_elements(ydev) eq 0 then $
    yc=maskvar.position(3)-0.1*(maskvar.position(3)-maskvar.position(1)) $
  else if keyword_set(center) then yc=ydev+0.5*cy $
  else yc=ydev+ys/2.+0.5*cy
  
  xs=xs>1 & ys=ys>1
  if (!d.flags and 1) ne 0 then $ ;scalable pixels ?
    tv,bytarr(2,2)+!p.background,xc-xs/2.,yc-ys/2.,xsize=xs,ysize=ys $
  else tv,bytarr(xs,ys)+!p.background,xc-xs/2.,yc-ys/2.
  
  plots,/device,color=!p.color, $
    [xc-xs/2.,xc+xs/2.,xc+xs/2.,xc-xs/2.,xc-xs/2.], $
    [yc-ys/2.,yc-ys/2.,yc+ys/2.,yc+ys/2.,yc-ys/2.]
  
  xyouts,/device,alignment=0.5,xc,yc-cy/2.3, $
    charsize=charsize,text,color=text_color
  
  if keyword_set(arrow) then $
    arrow,color=!p.color,/device,xc-xs/2.,yc-ys/2.,xdev+1,ydev+1
end

;drawing data gap
pro make_data_gap,data_gap,maskvar,color=color,lines=lines,no_fill=no_fill, $
                  text=text,only_text=only_text
  
  if n_elements(data_gap) eq 2 then nr_gaps=1 $
  else nr_gaps=(size(data_gap))(2)
  if not keyword_set(color) then color=16
  if n_elements(color) ne nr_gaps then color=bytarr(nr_gaps)+color
  if not keyword_set(lines) then lines=0
  if n_elements(lines) ne nr_gaps then lines=bytarr(nr_gaps)+lines
  

  for ig=0,nr_gaps-1 do if n_elements(data_gap(*,ig)) eq 2 then  begin
    xd=(convert_coord(data_gap(*,ig)-maskvar.xoffset,fltarr(2), $
                      /data,/to_device))(0,*)
    xd=xd(sort(xd))
    xd(0)=xd(0)>maskvar.position(0)
    xd(1)=xd(1)<maskvar.position(2)
    if xd(0) le xd(1) then begin
      if keyword_set(only_text) eq 0 then begin
        xs=(xd(1)-xd(0)-2)>0
        ys=(maskvar.position(3)-maskvar.position(1)-1)
        if keyword_set(no_fill) eq 0 then begin
                                ;swap color
          if ig lt nr_gaps-1 then if data_gap(1,ig) eq data_gap(0,ig+1) then $
            if color(ig+1) eq color(ig) then color(ig+1)=!p.background      
          if xs gt 2 then $
            if (!d.flags and 1) ne 0 then begin ;scalable pixels
            tv,bytarr(2,2)+color(ig),xd(0)+1,maskvar.position(1)+1, $
              xsize=xs,ysize=ys
          endif else begin      ;not scalable pixels
            tv,bytarr(xs,ys)+color(ig), $
              xd(0)+1,maskvar.position(1)+1
          endelse
        endif
        
        if lines(ig) eq 1 then begin
          plots,/device,color=!p.color,[xd(0),xd(0)], $
            [maskvar.position(1)+1,maskvar.position(3)-1]
          plots,/device,color=!p.color,[xd(1),xd(1)], $
            [maskvar.position(1)+1,maskvar.position(3)-1]
        endif
      endif
;       if xs lt 2 then begin
;         if lines(ig) eq 1 then cl=!p.color else cl=color(ig)
;         plots,/device,color=cl,[xd(0),xd(0)], $
;           [maskvar.position(1)+1,maskvar.position(3)-1]       
;       endif
      
                                ;output of text
      if ig lt n_elements(text) then if text(ig) ne '' then begin
        box_text,text(ig),maskvar=maskvar,xdev=xd
      endif
      
    endif
  endif
end

;finding all minima and maxima in vector
function find_minmax,vec,nvl

  idx=0l
  ln0=0
  vl=vec & vl(*)=1
  if n_elements(nvl) gt 0 then if nvl(0) ne -1 then vl(nvl)=0
  ivl=where(vl eq 1)
  if ivl(0) ne -1 then mm=max(vec(ivl))-min(vec(ivl)) else mm=[0.,0.]
  last=-1
  for i=1l,n_elements(ivl)-2 do begin
    prev=vec(ivl(i))-vec(ivl(i-1))
    next=vec(ivl(i+1))-vec(ivl(i))
    if prev ne 0 then ln0=prev
    if (ln0)*(next) lt 0 and last ne i-1 then begin
      idx=[idx,ivl(i)]
      last=i
    endif
  endfor
  idx=[idx,n_elements(vec)-1]
  if n_elements(nvl) gt 0 then nvl=nvl(sort(nvl))
  return,idx
end

;setting map set
pro set_map,maskvar,back=back,xs,ys

  p=maskvar.position
  vp=maskvar.view_pos
  if maskvar.back eq 1 then begin ;split plot region in two equal
                                ;sized parts if maskvar.back is not zero,
                                ;change viewpoint 180� if keyword back is set
    dx=0.                       ;(p(2)-p(0))/40.
    if keyword_set(back) then begin
      x0=(p(2)-p(0))/2.+p(0)+dx & x1=p(2)
      vp(0)=-vp(0)
      vp(1)=vp(1)+180.
      if vp(1) gt 180 then vp(1)=vp(1)-360.
      vp(2)=vp(2)+180.
      if vp(2) gt 180 then vp(2)=vp(2)-360.
    endif else begin
      x0=p(0) & x1=(p(2)-p(0))/2.+p(0)-dx
    endelse
  endif else begin
    x0=p(0) & x1=p(2)
  endelse
  xs=x1-x0
  ys=p(3)-p(1)
  p=convert_coord([[x0,p(1)],[x1,p(3)]],/device,/to_normal)
  p=reform(p(0:1,*),4)
  !p.position=p
  lim=maskvar.limit
  if total(abs(lim)) eq 0 then dummy=temporary(lim)
  map_set,vp(0),vp(1),vp(2),/ortho,/noerase,/noborder, $
    limit=lim,/horizon
;  map_set,vp(0),vp(1),/satellite,sat_p=[100,0,0],/noerase,/noborder, $
;    limit=lim,/horizon
;  map_set,0,0,/merc,/noborder,limit=lim,/noerase
end

;check if position lies inside plot window
function pos_valid,pos,vrange,x=x,y=y

  val=1
  if keyword_set(x) then $
    if min(pos) lt vrange(0)-1 or max(pos) gt vrange(2)+1 then val=0
  if keyword_set(y) then $
    if min(pos) lt vrange(1)-1 or max(pos) gt vrange(3)+1 then val=0
  return,val
end

;output of additional text
pro add_text,maskvar,additional_text
  
  pos=(convert_coord([maskvar.position(0)+(maskvar.position(2)- $
                                           maskvar.position(0))*0.05, $
                      maskvar.position(1)+(maskvar.position(3)- $
                                           maskvar.position(1))*0.10], $
                     /device,/to_normal))(0:1)
  wi=0. & wi_old=0.
  for j=0,n_elements(additional_text)-1 do begin
    adt=additional_text(j) & pst=strlen(adt)
    nad=''
    repeat begin
      nl_pst=strpos(/reverse_search,strupcase(strmid(adt,0,pst)),'!C')
      if nl_pst eq -1 then nl_pst=-2
      nad=[nad,strmid(adt,nl_pst+2,pst-nl_pst-2)]
      pst=nl_pst
    endrep until nl_pst lt 0
    nad=reverse(nad(1:*))
    dy=(convert_coord([0,!d.y_ch_size*!p.charsize*.9], $
                      /device,/to_normal))(1)
    dx=wi+wi_old & wi_old=wi & wi=0.
    for i=0,n_elements(nad)-1 do begin
      xyouts,pos(0)+dx,pos(1)+(n_elements(nad)-1-i)*dy*1.2,/normal,nad(i), $
        alignment=0,color=!p.color,width=w,charsize=maskvar.charsize*.7
      wi=[wi,w]
    endfor
    wi=max(wi)
  endfor
end


pro msk_plot,a,maskvar,onedim=onedim,color=color,nlevels=nlevels,$
             novalid=novalid,levels=levels,nocontour=nocontour,xcoord=xcoord,$
             noimage=noimage,c_labels=c_labels,ps_bw=ps_bw,c_thick=c_thick,$
             ycoord=ycoord,zeroline=zeroline,c_annotation=c_annotation,$
             lab_string=lab_string,exact=exact,vertline=vertline, $
             vert_color=vert_color,headtitle=headtitle,tick_pos=tick_pos, $
             tick_str=tick_str,contour_tv=contour_tv,mark=mark, $
             vector_pos=vector_pos, $
             vector_comp=vector_comp,vector_length=vector_length, $
             vector_max=vector_max,error_val=error_val,error_col=error_col, $
             error_width=error_width,error_noline=error_noline, $
             label_range=label_range,ex_ps_quality=ex_ps_quality, $
             line_thick=line_thick,psym=psym,vector_color=vector_color, $
             smooth=smooth,additional_text=additional_text, $
             vert_string=vert_string,data_gap=data_gap, $
             gap_transparent=gap_transparent,symsize=symsize, $
             line_style=line_style,tick_color=tick_color, $
             x_periodic=x_periodic,y_periodic=y_periodic,bkfr_step=bkfr_step, $
             back_fore=back_fore,box_novalid=box_novalid, $
             vector_system=vector_system,vector_box=vector_box, $
             connect=connect,nv_color=nv_color,bg_white=bg_white, $
             box_val=box_val,box_size=box_size,box_mode=box_mode, $
             box_weight=box_weight,label_frame=label_frame,xbar=xbar, $
             code_val=code_val,code_def=code_def,box_orbit=box_orbit, $
             box_iso=box_iso,flag=flag

  common verbose,verbose
  
  
  if n_elements(mark) ne 0 then begin
    dummy=cat_mark(mark_struct=mark,mark_val=mark_val,mark_sym=mark_sym, $
                   mark_col=mark_col,mark_point=mark_point, $
                   mark_fill=mark_fill,mark_label=mark_label, $
                   mark_region_value=mark_region_value, $
                   mark_region_lines=mark_region_lines, $
                   mark_region_color=mark_region_color, $
                   mark_region_text=mark_region_text, $
                   mark_region_idx=mark_region_idx)
    
                                ;change orientation of map-plot
    if maskvar.map eq 1 then begin
;      mark_val(0,*)=-mark_val(0,*)
      mark_val(1,*)=-mark_val(1,*)
    endif
  endif
  
  dg_proc=0
  mr_done=0
  vb_mode=0
  vb_orbit=1
  
  if n_elements(box_val) ne 0 then begin
    vb_orbit=0
    if n_elements(box_orbit) eq 1 then vb_orbit=box_orbit(0) eq 1
    if n_elements(box_iso) eq 0 then box_iso=1
  endif
  
  if n_elements(vector_box) eq 1 then if n_tags(vector_box) ge 4 then $
    if vector_box.mode ne 0 then begin
    vb_mode=1
    vb_orbit=vector_box.orbit eq 1
  endif
  
  if n_elements(vector_color) eq 0 then vector_color=1
  if n_elements(verbose) eq 0 then verbose=1
  if n_elements(novalid) gt 0 then if novalid(0) eq -1 then $
    dummy=temporary(novalid)
  if not(keyword_set(psym)) then psym=0
  if not(keyword_set(label_frame)) then label_frame=0
  if not(keyword_set(nv_color)) then nv_color=16.1
  a_orig=a
  sz=size(a)
  if sz(0) ne 0 then a=reform(a)
  sz=size(a)
  if not keyword_set(onedim) and sz(0) ne 0 then if sz(0) ne 2 then begin
    message,/cont,'variable has to be 2 dim. array'
    return
  endif    
  ifct=3
  exact_mode=0
  if keyword_set(xcoord) then begin
    dummy=size(xcoord)
    x_ex=0                      ;for tv-plot: expand to full range if size
                                ;of xcoord=number(0)+1
    if sz(0) eq 2 and not keyword_set(onedim) and  $
      dummy(1) eq sz(1)+1 then x_ex=1 $
    else if dummy(1) ne sz(1) then message,'xcoord has wrong size'
    xcrd=xcoord-maskvar.xoffset
    xrange_tv=maskvar.xrange
    if keyword_set(exact) eq 0 then begin
      anew=a
      sz=size(a)
    endif else begin
      exact_mode=1
      if maskvar.xtype eq 1 then begin
        xcrd=alog10(xcrd)
        xrange_tv=alog10(maskvar.xrange)
      endif
    endelse
  endif
  if keyword_set(ycoord) then begin
    dummy=size(ycoord)
    ycrd=ycoord-maskvar.yoffset
    y_ex=0                      ;for tv-plot: expand to full range if size
                                ;of ycoord=number(1)+1
    if sz(0) eq 2 and not keyword_set(onedim) and  $
      dummy(1) eq sz(2)+1 then y_ex=1 $
    else if dummy(1) ne sz(2) then message,'ycoord has wrong size'
    yrange_tv=maskvar.yrange
    if keyword_set(exact) eq 0 then begin
      anew=a
    endif else begin
      exact_mode=1
      if maskvar.ytype eq 1 then begin
        ycrd=alog10(ycrd)
        yrange_tv=alog10(maskvar.yrange)
      endif
    endelse
  endif
  if n_elements(mark_val) gt 1 then begin
    sz=size(mark_val)
    nel=sz(2)
    if n_elements(mark_val) gt 2 then begin
      if sz(0) ne 2 or sz(1) ne 2 then  $
        message,'mark has to be an (2,n) array'
    endif else nel=1
    if n_elements(mark_sym) eq 0 then mark_sym=1
    if n_elements(mark_sym) eq 1 then mark_sym=bytarr(nel)+mark_sym
    if n_elements(mark_fill) eq 0 then mark_fill=1
    if n_elements(mark_fill) eq 1 then mark_fill=bytarr(nel)+mark_fill
    if n_elements(mark_col) eq 0 then mark_col=1
    if n_elements(mark_col) eq 1 then mark_col=bytarr(nel)+mark_col
    if n_elements(mark_label) eq 0 then mark_label=''
    if n_elements(mark_label) eq 1 then mark_label=strarr(nel)+mark_label
    if n_elements(mark_sym) ne nel then $
      message,'mark_sym not compatible with mark'
    if n_elements(mark_fill) ne nel then $
      message,'mark_fill not compatible with mark'
    if n_elements(mark_col) ne nel then $
      message,'mark_col not compatible with mark'
    if n_elements(mark_label) ne nel then $
      message,'mark_label not compatible with mark'
    mark_flag=1
  endif else mark_flag=0
  if keyword_set(vector_pos) then begin
    vsz=size(vector_pos)
    if vsz(0) ne 2 and (vsz(2) ne 2 or vsz(1) ne 2) then  $
      message,'vector_pos has to be array with (n,2) elements)'
    vcsz=size(vector_comp)
    if min(where(vsz(0:2) eq vcsz(0:2))) eq -1 then $
      message,'vector_comp has to be array ' + $
      'with same no of elements as vector_pos'
    if not keyword_set(vector_length) then vector_length=10
    if not keyword_set(vector_max) then  $
      vector_max=sqrt(total(vector_comp^2,2))
    if vsz(2) eq 2 then begin
      vector_pos=transpose(vector_pos)
      vector_comp=transpose(vector_comp)
    endif
  endif

  if keyword_set(label_range) then maskvar.zrange=label_range
  
  if maskvar.vis ne 0 then begin
                                ; resolution for PS-Device
    if exact_mode eq 1 then ps_fact=15. else ps_fact=15.
    psquality=[maskvar.position(2)-maskvar.position(0),$
               maskvar.position(3)-maskvar.position(1)]/ps_fact

    lbl_flag=0
    sz=size(a) & sz=sz(0)
    okay=1
    if sz eq 0 then if a eq 0 then begin
      lbl_flag=1
      fg=findgen(256)
      if max(maskvar.zrange) lt 0 then fg=reverse(fg)
      a=transpose([[fg],[fg]]/256.*(maskvar.zrange(1)-maskvar.zrange(0))+ $
                  maskvar.zrange(0))
      sz=size(a) & sz=sz(0)
      okay=0
      maskvar.yrange=maskvar.zrange
    endif
    pos_dev=maskvar.position+[1,1,-1,-1] ;
    
    if n_elements(color) eq 0 then color=[1,2,3,4,5,6]
    if keyword_set(line_thick) eq 0 then line_thick=1
    if keyword_set(symsize) eq 0 then symsize=0.6
    ls_orig=n_elements(line_style)
    if keyword_set(line_style) eq 0 then line_style=0
    if keyword_set(nlevels) eq 0 then nlevels=6
    if n_elements(levels) gt 0 then nlevels=n_elements(levels)
    if keyword_set(contour_tv) then begin
      c_color=17.1+(!d.table_size-17.1-1)/(nlevels-1)*findgen(nlevels)
      cthick=3
    endif else begin
      cthick=1
      c_color=!p.color
    endelse
    if n_elements(c_thick) eq 1 then cthick=c_thick(0)
    if not keyword_set(c_labels) then c_labels=fltarr(nlevels) $
    else begin
      dummy=c_labels
      while n_elements(dummy) lt nlevels do dummy=[dummy,c_labels]
      c_labels=dummy(0:nlevels-1)
    endelse

    if sz eq 2 then if not keyword_set(onedim) then begin
      if lbl_flag eq 0 then if maskvar.ztype eq 1 then begin
        a=alog10(a>maskvar.zrange(0))
        maskvar.zrange=alog10(maskvar.zrange)
      endif

      asz=size(a_orig)
      if n_elements(xcrd) eq 0 then xcrd=indgen(asz(1))
      if n_elements(ycrd) eq 0 then ycrd=indgen(asz(2))

                                ;smoothing data
      if n_elements(novalid) gt 0 then nv=novalid else nv=-1
      if keyword_set(smooth) then begin
        data_smooth,a,nv,xcrd,ycrd,pos_dev,smooth,x_periodic=x_periodic, $
          y_periodic=y_periodic
        a_cont=a
      endif else a_cont=a_orig

      if n_elements(novalid) gt 0 then novalid=nv
      
      disp_range=0
      if maskvar.zrange(0) eq maskvar.zrange(1) then begin
        vld_arr=a & vld_arr(*)=1
        if n_elements(novalid) gt 0 then $
          if novalid(0) ne -1 then vld_arr(novalid)=0
        vld=where(vld_arr ne 0)
        maskvar.zrange(0)=min(a(vld>0))
        maskvar.zrange(1)=max(a(vld>0))
        if verbose eq 1 then $
          message,/cont,'no z range specified - range ['+ $
          strcompress(/remove_all, $
                      string(maskvar.zrange,format='('+ $
                             string(n_elements(maskvar.zrange))+ $
                             '(a,:,'',''))'))+']'
        disp_range=1
      endif else begin
        indx=where(a lt maskvar.zrange(0))
        if indx(0) ne -1 then a(indx)=maskvar.zrange(0)
        indx=where(a gt maskvar.zrange(1))
        if indx(0) ne -1 then a(indx)=maskvar.zrange(1)
      endelse
      maxa=maskvar.zrange(1)
      mina=maskvar.zrange(0)
      xs=maskvar.position(2)-maskvar.position(0)
      ys=maskvar.position(3)-maskvar.position(1)
;      if maxa lt 0 and maskvar.ztype eq 0 then sign=-1 else sign=1
      sign=1
      if !d.name eq 'PS' and keyword_set(ps_bw) then sign=-sign
      if sign eq -1 then min=-maxa else min=mina
      if abs(maxa-mina) eq 0 then diff=1 else diff=maxa-mina
      ascl=(sign*a-min)/(float(diff))*(!d.table_size-19.1)+17.1
      if keyword_set(bg_white) then begin
        wt=where(ascl eq 17.1)
        if wt(0) ne -1 then ascl(wt)=!p.background
      endif
      if not keyword_set(levels) then $
        levels=(1+findgen(nlevels))/(nlevels+1)*(maxa-mina)+mina
      sz=size(ascl)
      six=float(sz(1))
      siy=float(sz(2))
      if !d.name eq 'PS' then cc=psquality $
      else cc=[xs,ys]-[2,2]
      
      if maskvar.map eq 1 then begin ;map plot
        if not keyword_set(noimage) then begin
          if n_elements(novalid) gt 0 then if novalid(0) ge 0 then begin
            nv_map=ascl & nv_map(*)=0.
            nv_map(novalid)=1
            nv_map=where(nv_map ne 0)
            if max(nv_map) ge 0 then ascl(nv_map)=nv_color
          endif
            ascl=ascl;(reverse(indgen(n_elements(ascl(*,0)))),*)
          for i_b=0,(maskvar.back ne 0) do begin ;if 'back' is set, draw
                                ;image from both sides
            set_map,maskvar,back=i_b,xs,ys
            atv=map_image(ascl,xpos,ypos,/whole_map,latmin=90.,latmax=-90., $
;                          lonmin=-180.,lonmax=180., $
                          lonmin=-180.,lonmax=180., $
                          scale=.05,compress=1,missing=!p.background) 
;atv=atv(*,reverse(indgen(n_elements(atv(0,*)))))
;atv=atv(reverse(indgen(n_elements(atv(*,0)))),*)
            if (!d.flags and 1) ne 0 then begin ;Scalable pixels?
              tv,atv,xpos,ypos,xsize=xs,ysize=ys, /device 
            endif else begin
              tv,atv,xpos,ypos
            endelse
            lats=fix(findgen(7)*30)-90
            latn=reverse(n2s(lats+90))
            lons=fix(findgen(13)*30)-180
            lonn=reverse(n2s(lons+180))
            map_grid,/label,lats=lats,lons=lons,color=15,glinestyle=0, $
              charsize=maskvar.charsize*0.8,latnames=n2s(latn), $
              lonnames=n2s(lonn)
          endfor          
        endif
      endif else begin          ;no map plot
        maskvar.back=0
        if exact_mode eq 0 then begin ;poly_2d tv mode
          if cc(0) le 0 or cc(1) le 0 then begin
            message,/cont,'plot area too small'
            cc=cc>1
          endif
          if not keyword_set(noimage) then begin
            if (!d.flags and 1) ne 0 then begin ;Scalable pixels?
              spx=six*3.<1000 & spy=spx/six*siy
              apoly=poly_2d(ascl,[[0,0],[(six-1)/spx,0]], $
                            [[0,(siy-1)/spy],[0,0]],1,spx,spy)
             if n_elements(novalid) gt 0 then begin
                if min(novalid) ge 0 then begin
                  nvarr=fltarr(six,siy)
                  nvarr(novalid)=1
                  nvarr=poly_2d(nvarr,[[0,0],[(six-1)/spx,0]], $
                                [[0,(siy-1)/spy],[0,0]],0,spx,spy)
                  novalid=where(nvarr ne 0)
                  if max(novalid) ge 0 then apoly(novalid)=nv_color
                endif
              endif
                tv,apoly,pos_dev(0),pos_dev(1),xsize=xs,ysize=ys,/device
            endif else begin
              apoly=poly_2d(ascl,[[0,0],[(six-1)/cc(0),0]], $
                            [[0,(siy-1)/cc(1)],[0,0]],1,cc(0),cc(1))
              if n_elements(novalid) gt 0 then begin
                if min(novalid) ge 0 then begin
                  nvarr=fltarr(six,siy)
                  nvarr(novalid)=1
                  nvarr=poly_2d(nvarr,[[0,0],[(six-1)/cc(0),0]], $
                                [[0,(siy-1)/cc(1)],[0,0]],0,cc(0),cc(1))
                  novalid=where(nvarr ne 0)
                  if max(novalid) ge 0 then apoly(novalid)=nv_color
                endif
              endif
              tv,apoly,pos_dev(0),pos_dev(1),/device
            endelse
          endif
        endif else begin        ;exact tv mode
          if not keyword_set(noimage) then begin
            minx=xrange_tv(0) & maxx=xrange_tv(1)
            dx=maxx-minx
            if dx eq 0 then dx=1
            fx=cc(0)/double(dx)
            miny=yrange_tv(0) & maxy=yrange_tv(1)
            dy=maxy-miny & fy=cc(1)/double(dy)

            if not keyword_set(ex_ps_quality) then ex_ps_quality=0
            
                                ;decrease ps_resolution-> use z-buffer
            zbuff=0
            if !d.name ne 'Z' then $
              if (!d.flags and 1) ne 0 then if ex_ps_quality ne 0 then begin
                                ;set resolution according to
                                ;regularity of gridding
              lx=lindgen(n_elements(xcrd)) & par=linfit(lx,xcrd)
              dev=max(par(1)*lx+par(0)-xcrd)/(max(xcrd)-min(xcrd))* $
                n_elements(xcrd)
              factx=((dev*1e2)>2.)<6.
              if finite(factx) eq 0 then factx=6.
              ly=lindgen(n_elements(ycrd)) & par=linfit(ly,ycrd)
              dev=max(par(1)*ly+par(0)-ycrd)/(max(ycrd)-min(ycrd))* $
                n_elements(ycrd)
              facty=((dev*1e2)>2.)<6.
              if finite(facty) eq 0 then facty=6.
              
              ex_ps_quality=1
;             eps_minx=((n_elements(xcrd)*factx/ex_ps_quality))>256
;             eps_miny=((n_elements(ycrd)*facty/ex_ps_quality))>256
              eps_minx=(((n_elements(xcrd)*factx/ex_ps_quality))> $
                (n_elements(xcrd))<1000)>128
              eps_miny=(((n_elements(ycrd)*facty/ex_ps_quality))> $
                (n_elements(ycrd))<1000)>128

;              if eps_minx ge 256 or eps_miny ge 256 then begin
                zbuff=1
                pd_old=pos_dev
                old_dev=!d.name
                set_plot,'Z'
                tvlct,/get,ro,go,bo & fgo=!p.color & bgo=!p.background
                erase              
                device,set_resolution=[eps_minx,eps_miny]
                pos_dev=convert_coord([[0.,0.],[1.,1.]],/to_device,/normal)
                pos_dev=reform(pos_dev(0:1,0:1),4)
                if verbose eq 1 then $
                  message,/cont,'setting ps-size to '+ $
                  strcompress(string(!d.x_size,!d.y_size, $
                                     format='(i,''x'',i)'),/remove_all)
;              endif else begin
;                pos_dev=maskvar.position
;              endelse

              pos_dev(0:1)=pos_dev(0:1)>1
              pos_dev(2)=pos_dev(2)<(pos_dev(2)-1)
              pos_dev(3)=pos_dev(3)<(pos_dev(3)-1)
              fx=(pos_dev(2)-pos_dev(0))/double(dx)
              fy=(pos_dev(3)-pos_dev(1))/double(dy)
            endif
            

            if n_elements(novalid) gt 0 then if novalid(0) gt -1 then  $
              ascl(novalid)=nv_color
            
                                ;
            if n_elements(flag) eq n_elements(ascl) then begin
              flidx=where(flag ne 0)
              if flidx(0) ne -1 then begin
;                print,'Image Flag'
                ascl(flidx)=flag(flidx)
              endif
            endif
            
            
            for ix=0l,n_elements(xcrd)-1-x_ex do begin
              if x_ex eq 0 then begin
                xn=(xcrd(ix)-xcrd(ix-1>0))/2.
                xp=(xcrd(ix+1<(n_elements(xcrd)-1))-xcrd(ix))/2.
                                ;reduce boxsize to max. 5 x min. width
                bx_max=min(abs([xp,xn])) * 5
                xn=xn<(bx_max*((xn gt 0)*2-1))
                xp=xp<(bx_max*((xp gt 0)*2-1))
              endif else begin
                xn=0.
                xp=xcrd(ix+1)-xcrd(ix)
              endelse
              xpos=([xcrd(ix)-xn,xcrd(ix)+xp]-minx)*fx
              xpos=xpos(sort(xpos))
              xdiff=abs(xpos(1)-xpos(0)) ;>1
              for iy=0l,n_elements(ycrd)-1-y_ex do begin
;                if ascl(ix,iy) ge 16 then begin
                  if y_ex eq 0 then begin
                    yn=(ycrd(iy)-ycrd(iy-1>0))/2.
                    yp=(ycrd(iy+1<(n_elements(ycrd)-1))-ycrd(iy))/2.
                                ;reduce boxsize to max. 5 x min. height
;                    by_max=min(abs([yp,yn]))/2. * 5
;                    yn=yn<(abs(yn)*5*((yn gt 0)*2-1))
;                    yp=yp<(abs(yp)*5*((yp gt 0)*2-1))
                  endif else begin
                    yn=0.
                    yp=ycrd(iy+1)-ycrd(iy)
                  endelse
                  ypos=([ycrd(iy)-yn,ycrd(iy)+yp]-miny)*fy
                  ypos=ypos(sort(ypos))
                  ydiff=abs(ypos(1)-ypos(0)) ;>1
                  if finite(xdiff) eq 1 and finite(ydiff) eq 1 then $
                  if pos_valid(xpos+pos_dev(0),pos_dev,/x) and  $
                    pos_valid(ypos+pos_dev(1),pos_dev,/y) then begin
                    is_nv=where(ascl(ix,iy) eq nv_color)
                    if is_nv(0) eq -1 then begin
                      el=fltarr(2,2)+ascl(ix,iy)                   
                    endif else begin
                      el=[[nv_color,nv_color],[nv_color,nv_color]]
                    endelse
                    if (!d.flags and 1) ne 0 then begin ;Scalable pixels?
                      tv,el,pos_dev(0)+xpos(0), $
                        pos_dev(1)+ypos(0), $
                        xsize=xdiff+1,ysize=ydiff+1,/device
                    endif else begin
                      el_poly=fltarr(xdiff+1,ydiff+1)+el(0)
                      tv,el_poly,xpos(0)+pos_dev(0),ypos(0)+pos_dev(1),/device
                    endelse
                  endif
;                endif
              endfor     
            endfor
            if zbuff eq 1 then begin ;set back to orig. device
              img=tvrd(pos_dev(0),pos_dev(1), $
                       pos_dev(2)-pos_dev(0),pos_dev(3)-pos_dev(1))
              set_plot,old_dev
              tvlct,ro,go,bo & !p.color=fgo & !p.background=bgo
              pos_dev=pd_old
              tv,img,pos_dev(0)+1,pos_dev(1)+1, $
                xsize=pos_dev(2)-pos_dev(0)-2, $
                ysize=pos_dev(3)-pos_dev(1)-2,/device
            endif
          endif
        endelse
      endelse
      if not keyword_set(nocontour) or keyword_set(contour_tv) then begin
        asz=size(a_orig)
        if n_elements(xcrd) eq 0 then xcrd=indgen(asz(1))
        if n_elements(ycrd) eq 0 then ycrd=indgen(asz(2))
        if n_elements(novalid) gt 0 then $
          if novalid(0) ne -1 then a_cont(novalid)=!values.f_nan
        if maskvar.map eq 1 then begin
          overplot=1
          ycrd=reverse(ycrd)
          a_cont_plot=a_cont;(reverse(indgen(n_elements(a_cont(*,0)))),*)
        endif else begin
          overplot=0
          a_cont_plot=a_cont
        endelse
        for i_b=0,(maskvar.back ne 0) do begin ;if 'back' is set, draw
                                ;image from both sides
          if maskvar.map eq 1 then set_map,maskvar,back=i_b,xs,ys
          
          xcl=xcrd & ycl=ycrd
                                ;change xcl if xcrd is set for every bin
          if n_elements(xcrd) eq (size(a_cont_plot))(1)+1 then $
            xcl=((xcl+shift(xcl,1))/2.)(1:*)
          if n_elements(ycrd) eq (size(a_cont_plot))(2)+1 then $
            ycl=((ycl+shift(ycl,1))/2.)(1:*)
          
          
          if exact_mode eq 1 then begin
            if maskvar.xtype eq 1 then xcl=10^(xcrd)
            if maskvar.ytype eq 1 then ycl=10^(ycrd)
          endif
          
         if n_elements(color) ne nlevels then ccol=color(0) else ccol=color
           contour,a_cont_plot,xcl,ycl,c_charsize=maskvar.charsize(0)*.8, $
            pos=pos_dev,c_color=ccol,levels=levels(sort(levels)),$
            zrange=maskvar.zrange,/device,/noerase,xst=5,$
            xtype=maskvar.xtype,ytype=maskvar.ytype, $
            yrange=maskvar.yrange,xrange=maskvar.xrange,$
            yst=5,c_labels=c_labels,overplot=overplot,c_thick=cthick, $
            color=!p.color,c_annotation=c_annotation
        endfor
      endif
                                ;display changed z-range in plot
      if disp_range eq 1 then begin
        str='Min: '+strcompress(string(maskvar.zrange(0)),/remove_all)+ $
          '!CMax: '+strcompress(string(maskvar.zrange(1)),/remove_all)
        xyouts,/dev,alignment=0,charsize=maskvar.charsize*.8,color=!p.color, $
          pos_dev(0)+(pos_dev(2)-pos_dev(0))*0.05, $
          pos_dev(3)-(pos_dev(3)-pos_dev(1))*0.15,str
      endif
    endif else begin            ;1-dimensional plot

      code_ok=0
      if n_elements(gap_transparent) gt 0 then $
        if n_elements(data_gap) ge 2 then $
        if dg_proc eq 0 then make_data_gap,data_gap,maskvar

      sz=size(a)
      if sz(0) eq 2 then begin
        if (sz(1) eq 2) and sz(2) ne 2 then begin
          a=transpose(a)
          sz=size(a)
        endif
        if sz(2) ne 2 then okay=0
      endif else if sz(0) eq 1 then begin
        a=[[a],[findgen(sz(1))]]
      endif else okay=0
      a(*,0)=a(*,0)-maskvar.xoffset
      a(*,1)=a(*,1)-maskvar.yoffset

                                ;mark regions
      if n_elements(mark_point) eq 1 then if mark_point eq 1 then mr_done=1
      if mr_done eq 0 then if n_elements(mark_region_value) gt 1 then begin
        if n_elements(mark_region_color) eq 0 then mark_region_color=14
        mr_v=reform(mark_region_value)
        sz_reg=size(mr_v)
        if sz_reg(1) ne 2 then $
          message,/cont,'mark_region_value: array (2,nr) required' $
        else begin          
          dummy=msk_setrange(maskvar)
          make_data_gap,mr_v,maskvar,color=mark_region_color, $
            lines=mark_region_lines,text=mark_region_text
        endelse
        mr_done=1
      endif

      if keyword_set(tick_pos) then begin
        if max(tick_pos) ge n_elements(a(*,0)) then $
          message,/traceback,'tick_pos vector is not valid'
        if n_elements(tick_str) eq 0 then  $
          tick_str=strarr(n_elements(tick_pos)) else begin
          tick_str=[tick_str,strarr(n_elements(tick_pos))]
          tick_str=tick_str(0:n_elements(tick_pos)-1)
        endelse
      endif
      if not keyword_set(tick_color) then tick_color=!p.color

      min_xcr=min(!x.crange) & max_xcr=max(!x.crange)
      min_ycr=min(!y.crange) & max_ycr=max(!y.crange)
      if okay then begin        
        if n_elements(box_val) eq 0 and vb_orbit eq 1 then begin ;normal line-plot
;          or vb_orbit eq 1 then begin
          
          
          if n_elements(bkfr_step) ne 1 then bkfr_step=1
          if keyword_set(back_fore) then mm_idx=find_minmax(a(*,0),novalid) $
          else begin
            mm_idx=[0l,n_elements(a(*,0))-1]
          endelse
          
          if n_elements(novalid) gt 0 then if novalid(0) ne -1 then begin
            if keyword_set(connect) then begin ;connecting all valid points
              valid=a(*,0) & valid(*)=1
              valid(novalid)=0
              valid_idx=where(valid eq 1)
              if valid_idx(0) ne -1 then begin
                a=[[a(valid_idx,0)],[a(valid_idx,1)]]
                if keyword_set(error_val) then $
                  error_val=[[error_val(valid_idx,0)], $
                             [error_val(valid_idx,1)]]
              endif
              if keyword_set(back_fore) then $
                mm_idx=find_minmax(a(*,0),novalid) $
              else begin
                mm_idx=[0l,n_elements(a(*,0))-1]
              endelse
            endif else begin 
                                ;setting no valid points to f_nan
              a(novalid,0)=!values.f_nan
              if keyword_set(error_val) then $
                error_val(novalid,0:1)=!values.f_nan
            endelse
          endif
          
                                ;setting mm_idx for coded values (psd)
          if n_elements(code_def) gt 0 then $
            if n_elements(code_val) eq n_elements(a(*,0)) then begin
            if min(strupcase(tag_names(code_def)) eq $
                   ['VAL','TEXT','LINESTYLE', $
                    'COLOR','PSYM','THICK']) ne 1 then $
              message,'wrong definition for code_def'
            mm_idx=where(code_val ne shift(code_val,1))-1>0       
            if mm_idx(0) ge 0 then begin
              for i=0l,n_elements(mm_idx)-1 do begin
                if code_val((mm_idx(i)+1)<(n_elements(code_val)-1)) lt $
                  code_val(mm_idx(i)) then mm_idx(i)=mm_idx(i)+1
              endfor
            endif else mm_idx=0
            mm_idx=remove_multi([0,mm_idx,n_elements(code_val)-1])
            code_ok=1
          endif

          for i=0l,n_elements(mm_idx)-2 do begin
            if psym ne 0 and ls_orig ne 0 then psm=[0,psym] $
            else psm=psym
            if i eq 0 then mlow=mm_idx(i) else mlow=mm_idx(i)+1
            mhigh=mm_idx(i+1)
            for ip=0,n_elements(psm)-1 do begin
              plotx=a(mlow:mhigh,0)
              ploty=a(mlow:mhigh,1)
              if mlow eq mhigh then begin
                plotx=[plotx,plotx] 
                ploty=[ploty,ploty] 
              endif
              fx=finite(plotx) & fy=finite(ploty)
              sp_idx=where(((fx and fy) - shift(fx and fy,1)) eq 1 and $
                           ((fx and fy) - shift(fx and fy,-1)) eq 1)
              if sp_idx(0) ne -1 then begin
                nid=-1l
                nsp=n_elements(sp_idx)
                for is=0,nsp do begin
                  if is eq 0 then ib=0 else ib=sp_idx(is-1)
                  if is eq nsp then ie=n_elements(plotx)-1 else ie=sp_idx(is)
                  nid=[nid,lindgen(ie-ib+1)+ib]
                endfor
                nid=nid(1:*)
                plotx=plotx(nid)
                ploty=ploty(nid)
              endif
              if code_ok eq 1 then begin
                cv=max(code_val(mm_idx(i):mm_idx(i+1)))
                if code_def(cv).psym ge 0 then $
                  psmb=code_def(cv).psym else psmb=psm(ip)
                if code_def(cv).thick ge 0 then $
                  thk=code_def(cv).thick else thk=line_thick(0)
                if code_def(cv).linestyle ge 0 then $
                  styl=code_def(cv).linestyle else styl=line_style(0)
                if code_def(cv).color ge 0 then $
                  clr=code_def(cv).color $
                else clr=(color(0)+i*bkfr_step) mod 256
              endif else begin
                styl=line_style(0)
                clr=(color(0)+i*bkfr_step) mod 256
                thk=line_thick(0)
                psmb=psm(ip)
              endelse
              if n_elements(where(finite(plotx) eq 1)) ge 2 and $
                n_elements(where(finite(ploty) eq 1)) ge 2 then begin
;                plx=plotx(where(finite(plotx) eq 1)) 
;                ply=ploty(where(finite(ploty) eq 1))
;                if min(plx) ne max(plx) and min(ply) ne max(ply) then begin
                  if (styl(0) ne 0 or (psmb(0) eq 0 and styl(0) eq 0)) $
                    and styl(0) ne -1 then $
                    plot,plotx,ploty,pos=pos_dev,/device, $
                    xst=5,yst=5,/noerase,$
                    color=clr,linestyle=styl,thick=thk, $
                    xtype=maskvar.xtype,ytype=maskvar.ytype,$
                    xrange=maskvar.xrange,yrange=maskvar.yrange, $
                    symsize=0.6*maskvar.charsize*symsize
                  if psmb(0) ne 0 then $
                    plot,plotx,ploty,pos=pos_dev,/device, $
                    xst=5,yst=5,/noerase,$
                    color=clr,psym=psmb,thick=thk, $
                    xtype=maskvar.xtype,ytype=maskvar.ytype,$
                    xrange=maskvar.xrange,yrange=maskvar.yrange, $
                    symsize=0.6*maskvar.charsize*symsize
;                endif
              endif
            endfor
            if keyword_set(xbar) then $ ;bar in x-direction
              if n_elements(xbar) eq n_elements(a) then begin
              for ml=mlow,mhigh do begin
                oplot,color=clr,thick=1, $
                  [xbar(ml,0),xbar(ml,1)],[a(ml,1),a(ml,1)],noclip=0
                mkusym,'VLINE',fill=0,color=(color(0)+i*bkfr_step) mod 256
                plots,/data,psym=8,symsize=maskvar.charsize*symsize, $
                  [xbar(ml,0),xbar(ml,1)],[a(ml,1),a(ml,1)],noclip=0
              endfor
            endif
            if keyword_set(error_val) then begin ;draw error bars
              if not keyword_set(error_col) then error_col=color(0)
              errplot,a(mlow:mhigh,0), $
                error_val(mlow:mhigh,0), $
                error_val(mlow:mhigh,1),noclip=0, $
                color=(error_col+i*bkfr_step) mod 256, $
                width=error_width,noline=error_noline
            endif
          endfor
        endif
        if n_elements(box_val) ne 0 then begin ;draw color box symbols
          if maskvar.ztype eq 1 then begin
            box_val=alog10(box_val>maskvar.zrange(0))
            maskvar.zrange=alog10(maskvar.zrange)
          endif
          nela=n_elements(a(*,0))
          valid=a(*,0) & valid(*)=1
          vb_valid=valid
          if n_elements(box_novalid) gt 0 then if box_novalid(0) ne -1 then $
            valid(box_novalid)=0
          bxval=(box_val-maskvar.zrange(0))/ $
            (maskvar.zrange(1)-maskvar.zrange(0))*(!d.table_size-19.1)+17.1
          bxval=(bxval>17)<(!d.table_size-2)
          if keyword_set(box_size) eq 0 then box_size=1
          if keyword_set(box_weight) eq 0 then box_weight=.5
                                ;minimum weighting value
          box_weight=box_weight>0.01
          b_avg=0
          if n_elements(box_mode) gt 0 then $ ;box-mode average
            if strmid(box_mode,0,2) eq 'av' then begin
            b_avg=1
            dbx=max_xcr-min_xcr
            dby=max_ycr-min_ycr
            reso=20.
            maxpix=1500.
            bnx=(( (dbx)/(box_size>1e-5) )>1) ;<200.
            bny=(( (dby)/(box_size>1e-5) )>1) ;<200.
            if box_iso ne 1 then $
              bny=bnx/(maskvar.position(2)-maskvar.position(0))* $
              (maskvar.position(3)-maskvar.position(1))
            nelb_x=bnx*reso
            if nelb_x gt maxpix then begin
              nelb_x=maxpix
              reso=nelb_x/bnx
            endif
            nelb_y=bny*reso
            if nelb_y gt maxpix then begin
              nelb_y=maxpix
              reso=nelb_y/bny
              nelb_x=bnx*reso
            endif
            barr_v=fltarr(nelb_x,nelb_y)
            dat_x=findgen(nelb_x+1)/long(nelb_x)*(dbx)+min_xcr
            dat_y=findgen(nelb_y+1)/long(nelb_y)*(dby)+min_ycr
            dev_x=((convert_coord([transpose(dat_x),fltarr(1,nelb_x+1)+ $
                                   min_ycr],/data,/to_device))(0,*))(*)
            dev_y=((convert_coord([fltarr(1,nelb_y+1)+min_xcr, $
                                   transpose(dat_y)], $
                                  /data,/to_device))(1,*))(*)
            barr_n=fltarr(nelb_x,nelb_y)
            
          endif else begin      ;box_mode overlay
            bs=box_size
          endelse
          for i=0l,nela-1 do begin
            if valid(i) eq 1 then begin
              if b_avg eq 0 then begin ;do not average boxes
                xp=[(a(i,0)-bs/2),(a(i,0)+bs/2)]
                yp=[(a(i,1)-bs/2),(a(i,1)+bs/2)]
                if a(i,0) ge maskvar.xrange(0) and $
                  a(i,0) le maskvar.xrange(1) and $
                  a(i,1) ge maskvar.yrange(0) and $
                  a(i,1) le maskvar.yrange(1) then begin
                  xp=[(a(i,0)-bs/2)>maskvar.xrange(0), $
                      (a(i,0)+bs/2)<maskvar.xrange(1)]
                  yp=[(a(i,1)-bs/2)>maskvar.yrange(0), $
                      (a(i,1)+bs/2)<maskvar.yrange(1)]
                  if (!d.flags and 1) ne 0 then begin ;Scalable pixels?
                    szx=max(xp)-min(xp)
                    szy=max(yp)-min(yp)
                    if szx gt 0 and szy gt 0 then $
                      tv,bytarr(2,2)+bxval(i),xp(0),yp(0),/data, $
                      xsize=szx,ysize=szy
                  endif else begin
                    szd=transpose((convert_coord(transpose([[xp],[yp]]), $
                                                 /data,/to_device))(0:1,*))
                    szx=abs(szd(1,0)-szd(0,0))>1
                    szy=abs(szd(1,1)-szd(0,1))>1
                    tv,bytarr(szx,szy)+bxval(i),xp(0),yp(0),/data
                  endelse
                endif
              endif else begin  ;average box mode
                ix=(((a(i,0)-min_xcr)/(dbx)*bnx)) ;<(bnx-1))>0
                iy=(((a(i,1)-min_ycr)/(dby)*bny)) ;<(bny-1))>0
                if box_iso eq 1 then $
                  iy=(((a(i,1)-min_ycr)/(dbx)*bnx)) ;<(bny-1))>0
                s2=sqrt(2.)
                wh=box_weight   ;weighting factor
                for ixs=(float(ix)*reso-reso/s2)>0, $
                  (float(ix)*reso+reso/s2)<(nelb_x-1) do $
                  for iys=(float(iy)*reso-reso/s2)>0, $
                  (float(iy)*reso+reso/s2)<(nelb_y-1) do begin
                  r_center=sqrt((ixs-ix*reso)^2+(iys-iy*reso)^2)
                                ;Gauss function for weighting

                  if r_center lt reso/s2 then begin
                    weight=exp(-r_center^2/(reso*wh)^2)
                    barr_v(ixs,iys)=barr_v(ixs,iys)+bxval(i)*weight
                    barr_n(ixs,iys)=barr_n(ixs,iys)+weight
                  endif
                endfor
              endelse
            endif            
          endfor

          if b_avg eq 1 then begin ;draw average boxval values
            ne0=where(barr_n ne 0)
            if ne0(0) ne -1 then barr_v(ne0)=(barr_v(ne0)/barr_n(ne0))>17.1
            dc=convert_coord([[min_xcr,min_ycr], $
                              [max_xcr,max_ycr]],/data,/to_device)
            dcx=dc(0,1)-dc(0,0)
            dcy=dc(1,1)-dc(1,0)
            
            if (!d.flags and 1) ne 0 then begin ;Scalable pixels?
              dcxp=dcx/25. & dcyp=dcy/25.
              cbarr_v=congrid(barr_v,dcxp<(nelb_x),dcyp<(nelb_y))
            endif else begin
              cbarr_v=congrid(barr_v,dcx,dcy)
            endelse
            
            
            if vb_orbit eq 1 then begin
              csz=size(cbarr_v)
              iix=(((a(*,0)-min_xcr)/dbx*(csz(1)))>0)<(csz(1)-1)
              iiy=(((a(*,1)-min_ycr)/dby*(csz(2)))>0)<(csz(2)-1)
              for ia=0l,n_elements(iix)-1 do begin
                if cbarr_v(iix(ia),iiy(ia)) eq 0 then $
                  if finite(iix(ia)) and finite(iiy(ia)) then $
                  cbarr_v(iix(ia),iiy(ia))=color(0)
              endfor
;              data_x=findgen(csz(1)+1)/long(csz(1))*(dbx)+min_xcr
;              data_y=findgen(csz(2)+1)/long(csz(2))*(dby)+min_ycr
;               for ix=0,csz(1)-1 do for iy=0,csz(2)-1 do begin
;                 dxr=[data_x(ix),data_x(ix+1)]
;                 dyr=[data_y(iy),data_y(iy+1)]
;                 dxr=dxr(sort(dxr))
;                 dyr=dyr(sort(dyr))
;                 x_in=where(a(*,0) ge dxr(0) and a(*,0) le dxr(1))
; stop                
;                 if x_in(0) ne -1 then begin
;                   y_in=where(a(x_in,1) ge dyr(0) and a(x_in,1) le dyr(1))
;                   if y_in(0) ne -1 then begin
;                     if max(vb_valid(x_in) eq 1) eq 1 and $
;                       max(vb_valid(x_in(y_in)) eq 1) eq 1 then $
;                       if max(finite(a(x_in,0))) eq 1 and $
;                       max(finite(a(x_in(y_in),1))) eq 1 then $
;                       if cbarr_v(ix,iy) eq 0 then $
;                       cbarr_v(ix,iy)=color(0)
;                   endif
;                 endif
;               endfor
              
             endif
            
            
            bg=where(cbarr_v eq 0)
            if bg(0) ne -1 then cbarr_v(bg)=!p.background
;            if vb_orbit eq 1 then begin ;simply use tv-routine if no data
                                ;is underlying image 
            if (!d.flags and 1) ne 0 then begin ;Scalable pixels?
              dcxp=dcx/25. & dcyp=dcy/25.
              tv,cbarr_v,min_xcr,min_ycr,xsize=dbx,ysize=dby,/data
            endif else begin
              tv,cbarr_v,min_xcr,min_ycr,/data
            endelse
;             endif else begin    ;use exact tv-mode if orbit is plotted under
;                                 ;tv-image
;               for ix=0,nelb_x-1 do for iy=0,nelb_y-1 do begin
;                 if barr_v(ix,iy) ne 0 then begin
;                   el=fltarr(2,2)+barr_v(ix,iy)
;                   if (!d.flags and 1) ne 0 then begin ;Scalable pixels?
;                     tv,el,dev_x(ix),dev_y(iy),/device, $
;                       xsize=dev_x(ix+1)-dev_x(ix)+1, $
;                       ysize=dev_y(iy+1)-dev_y(iy)+1
;                   endif else begin
;                     el_poly=fltarr(dev_x(ix+1)-dev_x(ix)+1, $
;                                    dev_y(iy+1)-dev_y(iy)+1)+el(0)
;                     tv,el_poly,dev_x(ix),dev_y(iy),/device
;                   endelse
;                 endif 
;               endfor
;             endelse            
          endif
        endif
        if keyword_set(vector_pos) then begin ;draw vectors
          vl=vector_length/100.
          vector_pos(0,*)=vector_pos(0,*)-maskvar.xoffset
          vector_pos(1,*)=vector_pos(1,*)-maskvar.yoffset
          if n_elements(vector_system) eq 0 then vector_system=0
          dxtody=(convert_coord([0.,1.],[0.,1.],/data,/to_device))(0:1,*)
          dxtody=[dxtody(0,1)-dxtody(0,0),dxtody(1,1)-dxtody(1,0)]
          vmaxlen=max(sqrt(total(vector_comp^2,1)))
          case vector_system of
            0: begin            ;data coordinates
              dxtody=norm_vect(dxtody)
              vscale=vector_max
            end
            1: begin            ;xaxis coords coordinates
              dxtody=norm_vect([1.,1.])
              vl=dxtody(0)/(max_xcr-min_xcr)
              vscale=(max_xcr-min_xcr)/(pos_dev(3)-pos_dev(0))
            end
            2: begin            ;yaxis coords coordinates
              dxtody=norm_vect([1.,1.])
              vl=dxtody(1)/(max_ycr-min_ycr)
              vscale=(max_ycr-min_ycr)/(pos_dev(3)-pos_dev(1))
            end
            else: message,/traceback,'vector_system not valid'
          endcase
          
          if n_elements(bxval) eq n_elements(vector_pos(0,*)) then vcol=bxval $
          else vcol=vector_color(0)+intarr(n_elements(vector_pos(0,*)))

          if vb_mode eq 0 then begin
            d_len=1./vscale *dxtody
            if vector_system eq 0 then d_len=d_len*vl*(pos_dev(3)-pos_dev(1))
            for i=0,n_elements(vector_pos(0,*))-1 do begin
              pos=convert_coord(vector_pos(*,i),/data,/to_device)
              vc_len=sqrt(total(vector_comp(*,i)^2))
              if vc_len gt 0 then begin
                veccol=vcol(i)
                
                                ;einf�rben der Vektoren f�r JGR plot
                jgr=0
                if jgr eq 1 then begin
                  np=norm(vector_pos(*,i))
                  vrigid=[-np(1),np(0)] * $
                    sqrt(total(vector_pos(*,i)^2))*2* $
                    !pi/(9.9250*3600.)*7.14920e+07/1e3
                  
                  angle=acos(total(norm((vector_comp(*,i))(*)) * $
                                       norm(vrigid)))
                  r2vrig=sqrt(total(vector_comp(*,i)^2))*cos(angle) / $
                    sqrt(total(vrigid^2))

                  if abs(angle/!dtor) gt 45. then veccol=!p.color $
                  else begin
                    case 1 of
                      r2vrig lt 0.2: veccol=5
                      r2vrig ge 0.2 and r2vrig lt 0.8: veccol=3
                      r2vrig ge 0.8 and r2vrig le 1.2: veccol=2
                      r2vrig gt 1.2: veccol=1
                      else: veccol=4
                    endcase
                  endelse
                endif
                
                comp=vector_comp(*,i)*d_len
                plots,/device,color=veccol,[pos(0),pos(0)+comp(0)], $
                  [pos(1),pos(1)+comp(1)],thick=line_thick(0),noclip=1
              endif
            endfor
          endif else begin      ;average vectors in rectangular boxes
            
            dc=(convert_coord([[min_xcr,min_ycr], $
                               [max_xcr,max_ycr]], $
                              /data,/to_device))([0,1,3,4])
            dbx=max(!x.crange)-min(!x.crange)
            dby=max(!y.crange)-min(!y.crange)
            reso=1.0
            vb_size=vector_box.size/2. 
            bnx=(( (dbx)/(vb_size>1e-5) )>1)<1000.
            bny=(( (dby)/(vb_size>1e-5) )>1)<1000.
            nelb_x=bnx*reso
            nelb_y=bny*reso
            bnxr=nelb_x & bnyr=nelb_y
            vbarr_comp=fltarr(2,bnxr,bnyr)
            vbarr_n=fltarr(bnxr,bnyr)
            fctx=(bnxr)/dbx
            fcty=(bnyr)/dby
            xyoff=[min(!x.crange),min(!y.crange)]
            edge=abs(vb_size)*[-1,-1,1,1]*[fctx,fcty,fctx,fcty]
;           szw=long([edge(2)-edge(0)+1.01,edge(3)-edge(1)+1.01])
            szw=[1,1]*vector_box.nrinbox
            weight_arr=fltarr(szw(0),szw(1))
            if vector_box.efold le 0 then efold=vb_size/2. $
            else efold=vector_box.efold*vb_size
            for wix=0,szw(0)-1 do for wiy=0,szw(1)-1 do begin
              rcenter2=(((wix-szw(0)/2.)/fctx)^2 + ((wiy-szw(1)/2.)/fcty)^2)
              weight_arr(wix,wiy)=exp(-rcenter2/efold^2)
            endfor

            for i=0,n_elements(vector_pos(0,*))-1 do begin
                                ;position of vector in vbarr
                                ;(lower left corner of box)
              vc_len=sqrt(total(vector_comp(*,i)^2))
              
                                ;remove burst vectors for JGR plot
              burst=0
              if burst eq 1 then begin
                np=norm(vector_pos(*,i))
                vrigid=[-np(1),np(0)] * $
                  sqrt(total(vector_pos(*,i)^2))* $
                  2*!pi/(9.9250*3600.)*7.14920e+07/1e3
                
                angle=acos(total(norm((vector_comp(*,i))(*)) * $
                                 norm(vrigid)))
                                ;set vc_len to zero if deviation >45� 
                                ;-> vector will not be used for boxing
                if abs(angle/!dtor) gt 45. then vc_len=0
              endif
            
              if vc_len gt 0 then begin
                posll=((vector_pos(*,i)-xyoff) * [fctx,fcty] - $
                       fix(szw/2.)+ $
                       0.5*((szw mod 2) eq 0))
                lloff=fix(abs(posll) * (posll lt 0))<(szw-1)
                uroff=(([bnxr,bnyr]-(posll+szw))>0)<(szw-1)
                uroff=(fix(szw+uroff * (uroff lt 0)-1))>0
                px=([posll(0)+lloff(0),posll(0)+uroff(0)]<long(bnxr-1))>0
                py=([posll(1)+lloff(1),posll(1)+uroff(1)]<long(bnyr-1))>0
                if max(px) lt (size(vbarr_comp))(2) and $
                  max(py) lt (size(vbarr_comp))(3) then begin
                  for j=0,1 do $
                    vbarr_comp(j,px(0):px(1),py(0):py(1))= $
                    vbarr_comp(j,px(0):px(1),py(0):py(1)) + $
                    weight_arr(lloff(0):uroff(0),lloff(1):uroff(1)) * $
                    vector_comp(j,i)
                  vbarr_n(px(0):px(1),py(0):py(1))= $
                    vbarr_n(px(0):px(1),py(0):py(1))+ $
                    weight_arr(lloff(0):uroff(0),lloff(1):uroff(1))
                endif
              endif
              
            endfor
            max_vclen=0
            d_len=1./vscale*dxtody
                if vector_system eq 0 then $
                  d_len=d_len*vl*(pos_dev(3)-pos_dev(1))
            for ix=0,bnxr-1 do for iy=0,bnyr-1 do begin              
              vc_len=sqrt(total(vbarr_comp(*,ix,iy)^2))
              if vc_len gt 0 and vbarr_n(ix,iy) gt 0 then begin
                vclr=vector_color(0)
                vc_len=vc_len/vbarr_n(ix,iy)
                max_vclen=max_vclen>vc_len
                posdata=([ix,iy]+0.5) / [fctx,fcty] +xyoff
                pos=(convert_coord(posdata,/data,/to_device))(0:1)
                comp=vbarr_comp(*,ix,iy)/vbarr_n(ix,iy)*d_len
                arrow,color=vclr,thick=line_thick(0), $
                  hthick=line_thick(0),hsize=-0.1, $
                  pos(0),pos(1),pos(0)+comp(0),pos(1)+comp(1) 
              endif
            endfor
            vmaxlen=max_vclen
          endelse 
          
          
          
          
                                ;draw norm vector outside lower right
                                ;corner
          if vector_system eq 0 then begin
            
            xw=!d.x_ch_size*maskvar.charsize*.8
            yh=!d.y_ch_size*maskvar.charsize*.8
            
            ypos=maskvar.screen_pos_dev(1)+2.5*yh
            ypos=maskvar.position(1)-0*2.5*yh
            
                                ;(0,0) position of plot
            vlab_pos=[maskvar.screen_pos_dev(0)+4.0*xw,ypos]
            
                                ;device coordinate size of plot
            vlab_devx=pos_dev(0)-vlab_pos(0)-4*xw
            vlab_devy=vlab_devx*dxtody(1)/dxtody(0)
            
                                ;xrange/yrange in data coordinates
            xdata_range= $
              abs(vlab_devx/dxtody(0)*vscale/(vl*(pos_dev(3)-pos_dev(1))))
            ydata_range= $
              abs(vlab_devy/dxtody(1)*vscale/(vl*(pos_dev(3)-pos_dev(1))))
            
            pold=!p & xol=!x & yol=!y
            plot,/nodata,/noerase,xst=5,yst=5,/device, $
              xtick_get=xtg,ytick_get=ytg, $
              position=[vlab_pos(0),vlab_pos(1), $
                        vlab_pos(0)+vlab_devx,vlab_pos(1)+vlab_devy], $
              [0,xdata_range],[0,ydata_range], $
              title='vector scaling:',charsize=0.6*maskvar.charsize
            nxt=n_elements(xtg) & xtstr=strarr(nxt)+' ' & xtstr([0,nxt-1])=''
            nyt=n_elements(ytg) & ytstr=strarr(nyt)+' ' & ytstr([0,nyt-1])=''
            
            maxstr=n2s(vmaxlen,format='(f15.3)')
            arvec=vscale*[1.,1.]/sqrt(2.)
            if arvec(0) gt max(xtg) or arvec(1) gt max(ytg) then begin
              arvec=arvec<([max(xtg),max(ytg)])
            endif
            arlen=sqrt(total(arvec^2))
            
                                ;reduce length of scaling vector to
                                ;nearest nice number
            fixlog=fix(alog10(arlen)-(alog10(arlen) le 0))
            fixarl=fix(arlen/10.^fixlog)
            newlen=float(fixarl)*10.^fixlog
            arvec=arvec/arlen*newlen
            arlen=newlen
            ac_digits=fix(-fixlog)>0
            
            lenstr=n2s(arlen,format='(f15.'+n2s(ac_digits)+')')
            
            maxstr='max = '+maxstr
            
            axis,0,0,yax=0,yst=1,charsize=0.6*maskvar.charsize,yminor=1, $
              ytickname=ytstr,color=!p.color
            axis,0,0,xax=0,xst=1,charsize=0.6*maskvar.charsize,xminor=1, $
              xtitle=maxstr,xtickname=xtstr,color=!p.color
            
            arrow,0,0,arvec(0),arvec(1),hsize=-0.1,color=vector_color(0), $
              hthick=line_thick(0),thick=line_thick(0),/data
            dvec=convert_coord(/data,/to_device,[[0.,0.],[arvec]])

            angle=atan(dvec(4)-dvec(1),dvec(3)-dvec(0))/!dtor
            xyouts,/data,arvec(0)/2.,arvec(1)/2., $
              '!C  '+lenstr,orientation=angle, $
              color=!p.color,charsize=0.6*maskvar.charsize,alignment=0.5

            
            npc=36
            phi=findgen(npc)/(npc-1)*!pi/2. & cp=cos(phi) & sp=sin(phi)
            xcirc=arlen*cp & ycirc=arlen*sp
            val_circ=where(xcirc le xdata_range and ycirc le ydata_range)
            if val_circ(0) ne -1 then $
              plots,psym=3,color=!p.color,xcirc(val_circ),ycirc(val_circ)

            !p=pold  & !x=xol & !y=yol
          endif
        endif
        if keyword_set(zeroline) then begin
          if maskvar.yrange(0)*maskvar.yrange(1) lt 0 then $
            axis,0,0,xaxis=0,xticks=1,xtickname=[' ',' '],xminor=1, $
            xticklen=.001,color=!p.color       ;,color=255
          if maskvar.xrange(0)*maskvar.xrange(1) lt 0 then $
            axis,0,0,yaxis=0,yticks=1,ytickname=[' ',' '],yminor=1, $
            yticklen=.001,color=!p.color       ;,color=255
        endif
        if n_elements(lab_string) ne 0 then if max(lab_string) gt '' then begin
          styl=line_style(0)
          if n_elements(mm_idx) gt 2 and code_ok eq 0 then begin
            clr=(color(0)+indgen(n_elements(mm_idx)-1)*bkfr_step) mod 256 
          endif else if code_ok eq 0 then clr=color(0) $
          else begin
            clr=remove_multi(code_def.color)
            if code_def(0).psym ge 0 then $
              psmb=code_def(0).psym else psmb=psym(0)
            if code_def(0).thick ge 0 then $
              thk=code_def(0).thick else thk=line_thick(0)                
            if code_def(0).linestyle ge 0 then $
              styl=code_def(0).linestyle else styl=line_style(0)
          endelse
          if total(abs(maskvar.lab_position)) eq 0 then begin
            lbpos=long([maskvar.position(2),maskvar.position(1), $
                        maskvar.screen_pos_dev(2),maskvar.position(3)])
          endif else begin
            lbpos=maskvar.lab_position
            if maskvar.lab_sys ne 'device' then $
              lbpos=(convert_coord([[lbpos(0),lbpos(1)], $
                                    [lbpos(2),lbpos(3)]], $
                                   /data,/to_device))([0,1,3,4])
          endelse
          
          lst=styl
;          if ls_orig eq 0 then dummy=temporary(lst)
          if min(strcompress(lab_string,/remove_all) eq '') eq 0 then $
            label_box,position=lbpos,color=clr, $
            charsize=0.8*maskvar.charsize,ps_bw=ps_bw, $
;            thick=line_thick(0),psym=psym(0),linestyle=line_style(0), $
            thick=thk,psym=psmb,linestyle=lst,bottom=maskvar.lab_dir eq 0, $
            name=lab_string,symsize=0.8*maskvar.charsize*symsize, $
            frame=label_frame,linespace=maskvar.lab_linespace,/blank
          endif
        if keyword_set(tick_pos) then begin ;creating ticks on plot
          if n_elements(novalid) eq 0 then nvld=-1 else nvld=novalid
          for it=0l,n_elements(tick_pos)-1 do $
            if max(where(nvld eq long(tick_pos(it)))) eq -1 and $
            max(where(nvld eq long(tick_pos(it)+1))) eq -1 then $
            if tick_pos(it) le n_elements(a(*,0))-1 then begin
            xtp=a(tick_pos(it),0)+(tick_pos(it)-long(tick_pos(it)))* $
              (a((tick_pos(it)+1)<(n_elements(a(*,0))-1),0)-a(tick_pos(it),0))
            ytp=a(tick_pos(it),1)+(tick_pos(it)-long(tick_pos(it)))* $
              (a((tick_pos(it)+1)<(n_elements(a(*,1))-1),1)-a(tick_pos(it),1))
;            cd=convert_coord([a(tick_pos(it),0),a(tick_pos(it),1)], $
;                             /data,/to_device)
            cd=convert_coord([xtp,ytp],/data,/to_device)
            stp=1 & stm=1 & dirx=0. & diry=0. & swtch=0
            repeat begin        ;calculating perp. direction
              if max(where(nvld eq long(tick_pos(it)+stp))) eq -1 and $
                max(where(nvld eq long(tick_pos(it)-stm))) eq -1 then begin
                dirx=dirx+a((tick_pos(it)+stp)<(n_elements(a(*,0))-1),0)- $
                  a((tick_pos(it)-stm)>0,0)
                diry=diry+a((tick_pos(it)+stp)<(n_elements(a(*,0))-1),1)- $
                  a((tick_pos(it)-stm)>0,1)
              endif
              if swtch eq 0 then stp=stp+1 else stm=stm+1
              swtch=swtch eq 0
            endrep until abs(dirx) gt 0 and abs(diry) gt 0  $
              or tick_pos(it)+stp ge n_elements(a(*,0)) $
              or tick_pos(it)-stm le 0
            if dirx eq 0 and diry eq 0 then dirx=1
            nrm=[-diry/(!y.crange(1)-!y.crange(0)), $
                 dirx/(!x.crange(1)-!x.crange(0))]
            len=(pos_dev(3)-pos_dev(1))/80. ;ticklength=1/80 of plot height
            nrm=nrm/sqrt(total(nrm^2))*len
            if cd(1) lt pos_dev(3) and cd(1) gt pos_dev(1) and $
              cd(0) lt pos_dev(2) and cd(0) gt pos_dev(0) then begin
              if max(byte(tick_str(it))) gt 32 then th=2. else th=1.
              plots,/device,[cd(0)-nrm(0),cd(0)+nrm(0)], $
                [cd(1)-nrm(1),cd(1)+nrm(1)], $
                color=tick_color(0),noclip=0,thick=th
              xyouts,alignment=0.5,/device,cd(0)+nrm(0)*1.5,cd(1)+nrm(1), $
                color=tick_color(0),tick_str(it), $
                charsize=maskvar.charsize*0.8
            endif               
          endif
        endif
      endif else message,/traceback, $
        '1-d parameter is not valid, not enough valid points to plot'
    endelse else message,/cont, $
      'parameter not valid, not enough valid points to plot'
    plot,xst=5,yst=5,maskvar.xrange,maskvar.yrange,xtype=maskvar.xtype, $
      ytype=maskvar.ytype,/nodata,pos=maskvar.position,/noerase,/device, $
      xrange=maskvar.xrange,yrange=maskvar.yrange
    if mark_flag eq 1 then begin
      for i_b=0,(maskvar.back ne 0) do begin ;if 'back' is set, draw
                                ;image from both sides
        xs=maskvar.position(2)-maskvar.position(0)
        ys=maskvar.position(3)-maskvar.position(1)
        if not keyword_set(onedim) then if maskvar.map eq 1 then $
          set_map,maskvar,back=i_b,xs,ys
        type=['CIRCLE','TRIANGLE','SQUARE','DIAMOND','MPAE','DOT']
        for i=0,n_elements(mark_col)-1 do begin
          mkusym,type(mark_sym(i)),fill=mark_fill(i),color=mark_col(i)
          if mark_val(0,i) ge min(maskvar.xrange) and $
            mark_val(0,i) le max(maskvar.xrange) and $
            mark_val(1,i) ge min(maskvar.yrange) and $
            mark_val(1,i) le max(maskvar.yrange) then $
            plots,/data,psym=8,mark_val(*,i), $
            noclip=1,symsize=maskvar.charsize*symsize
          if strcompress(mark_label(i),/remove_all) ne '' then begin
            if maskvar.number(0) lt maskvar.total(0)-1 then inside=1 $
            else inside=0
;            outtext,maskvar.position,i+1,mark_col(i),mark_label(i), $
;              maskvar.charsize,inside=inside,symbol=8,/bottom
            if i_b eq 0 then begin
              ll=min([maskvar.position(0)*.0,maskvar.position(1)*1.0])
              
              ch=!d.y_ch_size*maskvar.charsize
              pos=[maskvar.screen_pos_dev(0), $
                   (maskvar.position(1)*1.-2*ch)>(maskvar.screen_pos_dev(1)),$
                   maskvar.position(0)*(1+(maskvar.map eq 1)*0.4), $
                   maskvar.position(3)]
              if i lt n_elements(mark_col)-1 then frf= $
                min(strcompress(mark_label(i+1:*),/remove_all) eq '') eq 1 $
              else frf=1
              label_box,psym=8,color=mark_col(i),linespace=1.2,ps_bw=ps_bw,$
                position=pos,/blank, $
                frame=frf,name=mark_label(i),/bottom, $
                symsize=maskvar.charsize*.7*symsize, $
                charsize=maskvar.charsize*.7
            endif
          endif
        endfor
      endfor
    endif

    if n_elements(data_gap) ge 2 then begin
      if dg_proc eq 0 then make_data_gap,data_gap,maskvar
    endif

    if n_elements(vertline) gt 0 then begin ;drawing vertical lines
      nel_vl=n_elements(vertline)
      if n_elements(vert_color) eq 0 then  $
        vert_color=1+bytarr(nel_vl)
      if n_elements(vert_string) eq nel_vl then vls_ok=1 else vls_ok=0
      occ=[0.,0.,0.,0.]
      bt={text:'',xdev:0.,ydev:0.}
      btpar=replicate(bt,nel_vl)
      for i=0,nel_vl-1 do begin
        dc=convert_coord([vertline(i)-maskvar.xoffset,0d],/data,/to_device)
;        dc(1)=min(pos_dev(2:3))
        dc(1)=pos_dev(3)
        if dc(0) ge pos_dev(0) and dc(0) le pos_dev(2) then begin
          plots,[dc(0),dc(0)],[pos_dev(1),pos_dev(3)], $
            thick=1 + (!d.name eq 'PS'), $
            color=vert_color(i mod n_elements(vert_color)),/device,noclip=0
          if vls_ok eq 1 then begin
            vs='!C'+vert_string(i)
            repeat begin
              
              xyouts,-10,-10,vs,alignment=0.5, $
                color=!p.background,noclip=0,charthick=1 + (!d.name eq 'PS'), $
                charsize=maskvar.charsize*.8,/normal,width=wd
              
                                ;plot box around label
              
              
              wd=(convert_coord(/normal,/to_device,[wd,0]))(0)
              bc=-1 & ss='!C'
              repeat begin
                pos=strpos(strupcase(vs),ss)
                bc=bc+1 & ss=ss+'!C'
              endrep until pos eq -1
;              ic=where(where(byte(vs) eq 33) eq where(byte(vs) eq 67)-1 eq 1)
;              if ic(0) ne -1 then ic=n_elements(ic)+1
              ic=count_substring(vs,'!C')+1
              
              
              occ=[[occ],[dc(0)-wd/2.-!d.x_ch_size*maskvar.charsize*.5, $
                          dc(0)+wd/2.+!d.x_ch_size*maskvar.charsize*.5, $
                          dc(1)-(bc-1)*!d.y_ch_size*maskvar.charsize*1.0-.1, $
                          dc(1)-(ic-1)*!d.y_ch_size*maskvar.charsize*1.0]]
              ok=1
              no=n_elements(occ(0,*))
              for k=1,no-2 do begin
                if ((min([(occ(0,k)-occ(0,no-1))*(occ(0,k)-occ(1,no-1)), $
                          (occ(1,k)-occ(1,no-1))* $
                          (occ(1,k)-occ(0,no-1))]) le 0) $
                    or (occ(0,k) lt occ(0,no-1) and occ(1,k) gt occ(1,no-1))) $
                  and $
                  ((min([(occ(2,k)-occ(2,no-1))*(occ(2,k)-occ(3,no-1)), $
                         (occ(3,k)-occ(3,no-1))* $
                         (occ(3,k)-occ(2,no-1))]) le 0) $
                   or (occ(2,k) lt occ(2,no-1) and occ(3,k) gt occ(3,no-1))) $
                  then ok=0
              endfor
              if ok eq 1 then begin
                vsp=strmid(vs,(bc)*2,strlen(vs))
;                 xyouts,dc(0),occ(3,no-1),vsp,alignment=0.5, $
;                   color=vert_color(i mod n_elements(vert_color)),noclip=0, $
;                   charsize=maskvar.charsize*.8,/device
                                ;store parameters for later labeling
                btpar(i).text=vsp 
                btpar(i).xdev=dc(0)
                btpar(i).ydev=occ(3,no-1)
              endif else begin
                vs='!C'+vs
                occ=occ(*,0:no-2)
              endelse
            endrep until ok
          endif
        endif
      endfor
    endif
    if maskvar.map ne 1 then begin ;redraw frames for plot
      dummy=msk_setrange(maskvar)
    endif    
                                ;mark regions in line plots (point mode)    
    if n_elements(mark_point) ne 0 then if mark_point eq 1 then $
      for i=0,n_elements(mark_region_idx)/2-1 do begin
      new_a=a(mark_region_idx(0):mark_region_idx(1),*)
      if n_elements(new_a) gt 2 then begin
        msk_plot,new_a,maskvar,/onedim,color=color+(n_elements(color)>1), $
          tick_pos=0,tick_str=0,line_thick=line_thick*3        
        if n_elements(mark_region_text) gt 0 then begin
          pos=convert_coord(/data,/to_device, $
                            reform(new_a(n_elements(new_a(*,0))/2.,*),2))
          box_text,mark_region_text(i),maskvar=maskvar, $
            xdev=pos(0),ydev=pos(1),/arrow
        endif
      endif
      mr_done=1
    endfor
                                ;mark regions for tv plots
    if mr_done eq 0 then if n_elements(mark_region_value) gt 1 then begin
      if n_elements(mark_region_color) eq 0 then mark_region_color=1
      mr_v=reform(mark_region_value)
      sz_reg=size(mr_v)
      if sz_reg(1) ne 2 then $
        message,/cont,'mark_region_value: array (nr,2) required' $
      else make_data_gap,mr_v,maskvar,lines=mark_region_lines,/no_fill, $
        text=mark_region_text
      mr_done=1
    endif
    
    
  endif  

                                ;output of additional text
  if n_elements(additional_text) eq 1 then add_text,maskvar,additional_text
  
   ; output of headtitle
  if keyword_set(headtitle) and $
    maskvar.number(1) eq maskvar.total(1)-1  then begin
    pos=convert_coord([0.,maskvar.charsize*!d.y_ch_size], $
                      /device,/to_normal)
    tw=get_textwidth(headtitle,0.8*maskvar.charsize)
    scale=((maskvar.position(2)-maskvar.position(0))/tw)<1.
    xyouts,maskvar.head_pos(0),maskvar.head_pos(3)-pos(1)*0.8, $
      headtitle,alignment=0,/normal,charsize=0.8*maskvar.charsize*scale, $
      color=!p.color            ;,color=255
  endif
  
  ;vertical line labels
  for i=0,n_elements(btpar)-1 do if btpar(i).text ne '' then begin
    box_text,btpar(i).text,maskvar=maskvar,xdev=btpar(i).xdev, $
      ydev=btpar(i).ydev,charsize=maskvar.charsize*.8,/center
  endif

end
