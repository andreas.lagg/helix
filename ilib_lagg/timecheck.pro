pro timecheck,label,init=init,global=global
  common timecheck,cnt,ltime,gtime
  
  ttime=systime(1)
  if n_elements(cnt) eq 0 or keyword_set(init) then cnt=-1
  
  if n_params() eq 0 then label=''
  if keyword_set(global) then gtime=ttime 
  if n_elements(gtime) ne 0 then $
    addglobal='  dttot='+string(ttime-gtime,format='(f12.5)')+' s' $
  else addglobal=''

  
  if cnt eq -1 then begin
    print,'TC '+label+' (init): ',systime(0,ttime),addglobal
  endif else begin
    print,'TC '+label+' '+string(cnt,format='(i3.3)')+': ', $
      systime(0,ttime),'  dt='+string(ttime-ltime,format='(f12.5)')+' s'+ $
      addglobal
  endelse
  ltime=ttime
  cnt=(cnt+1) mod 1000
  
end
