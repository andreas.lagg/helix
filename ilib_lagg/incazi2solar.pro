;transform inc/azi 2 solar coordinates
pro incazi2solar,incsol,azisol,ambiguity=ambig
  common pikaia
    
                                ;get position from header of pikaia_result
  header=pikaia_result.header
  telescop=strcompress(header.telescop,/remove_all)
  case telescop of
    'VTT': begin
      nel=n_elements(header.slitposx)
      xcen=total(header.slitposx)/nel
      ycen=total(header.slitposy)/nel
      
    end
    'HINODE':  begin
      xcen=header.xcen
      ycen=header.ycen
    end
    else: begin
      message,/cont,'only works for VTT and Hinode'
      reset
    end
  endcase
    
  
  hhmmss=strmid(header.dateobs,11,100)
  date=strmid(header.dateobs,0,10)
  date=strmid(date(0),8,2)+'-'+ $
    (['jan','feb','mar','apr','may','jun','jul', $
      'aug','sep','oct','nov','dec'])(fix(strmid(date,5,2))-1)+'-'+ $
    strmid(date,2,2)
  atime=anytim2ints(hhmmss+' '+date)
  
  if telescop eq 'VTT' then begin
    if (atime.day)(0) lt 10348l then begin
      print,date+': Coordinates in header are in VTT system.' 
    endif else begin
      print,date+': Coordinates in header are in SOLAR system.' 
    endelse
  endif
  
  out=get_rb0p(atime,/deg)
  r0=total(out(0))
  b0=total(out(1))
  p0=total(out(2))
  
  th=acos(sqrt(xcen^2+ycen^2)/r0) ;heliocentric angle
  ae=atan(ycen,xcen)              ;rotation
  
;VASILYs ROUTINE
;++++++++++++++++++read results of inversions++++++++++++++++++++++++++++++++++
;------------Let's compute new azimuth and inclination----------------
    

  
  rv=[cos(ae),sin(ae),0]        ;vector-axis of rotation
  M=[[cos(th)+(1-cos(th))*rv(0)^2,(1-cos(th))*rv(0)*rv(1)-(sin(th))*rv(2), $
      (1-cos(th))*rv(0)*rv(2)+(sin(th))*rv(1)],$
     [(1-cos(th))*rv(0)*rv(1)+(sin(th))*rv(2),cos(th)+(1-cos(th))*rv(1)^2, $
      (1-cos(th))*rv(1)*rv(2)-(sin(th))*rv(0)],$ 
     [(1-cos(th))*rv(2)*rv(0)-(sin(th))*rv(1), $
      (1-cos(th))*rv(2)*rv(1)+(sin(th))*rv(0),cos(th)+(1-cos(th))*rv(2)^2]]

  sz=size(pikaia_result.fit.atm.par.inc)
  xx=fltarr(pikaia_result.input.ncomp,sz(sz(0)-1),sz(sz(0)))
  inclos=xx & inclos(*,*,*)=pikaia_result.fit.atm.par.inc
  azilos=xx & azilos(*,*,*)=pikaia_result.fit.atm.par.azi
  sz=size(xx)
  
  if n_elements(ambig) ne 0 then if keyword_set(ambig) or ambig eq 180 then $
    azilos=azilos+180.
  
  x=sin(inclos*!dtor)*cos(azilos*!dtor)
  y=sin(inclos*!dtor)*sin(azilos*!dtor)
  z=cos(inclos*!dtor)
  x_new=x*0
  y_new=y*0
  z_new=z*0
  
  for ic=0,pikaia_result.input.ncomp-1 do begin
    for j=0,sz(3)-1 do begin
      for i=0,sz(2)-1 do begin
        temp=[x(ic,i,j),y(ic,i,j),z(ic,i,j)] ;observed vector
        t=M##temp                   ;derotated (local) vector
        x_new(ic,i,j)=t(0)
        y_new(ic,i,j)=t(1)
        z_new(ic,i,j)=t(2)
      endfor
    endfor
  endfor
    
incsol=reform(acos(z_new)/!dtor)
azisol=reform(acos(x_new/sqrt(x_new^2+y_new^2))/!dtor)


;------------------------------------------------------------------

end
