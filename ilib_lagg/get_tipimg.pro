function get_tipimg,tip,helium=helium,silicon=silicon, $
                    magnetogram=magnetogram,continuum=continuum, $
                    cwl=cwl,wlr=wlr,i=i,q=q,u=u,v=v
  
  read_ccx,tip+'x',wl_disp=wl_disp,wl_off=wl_off,icont=icont,wl_num=wl_num, $
    error=error
  if error ne 0 then begin
    print,' --> No WL-info, using average image'
    bin=-1
  endif
  
  if n_elements(cwl) eq 0 then begin
    cwl=0d
    if keyword_set(helium) then cwl=10830.38d 
    if keyword_set(silicon) then cwl=10827.088d
  endif else print,'Using CWL=',cwl
  
  if n_elements(wl_num) eq 0 then begin
    xy_fits,tip,x=0,y=0,i=i
    wl_num=n_elements(i)
  endif
  wlvec=wl_off+dindgen(wl_num)*wl_disp
  
  if n_elements(wlr) eq 0 then begin
    if keyword_set(magnetogram) then begin
      if cwl lt 1. then begin
        print,'Average V image'
        bin=-1 
      endif else begin
        print,'Average V image from -0.2 A to CWL of line '+n2s(cwl)
        bin=where(wlvec ge cwl-0.2 and wlvec le cwl)
      endelse
      xy_fits,tip,bin=bin,v=tipimg
    endif else begin
      if cwl lt 1. then begin
        print,'Average I image'
        bin=-1 
      endif else begin
        print,'Average I image +- 0.05 A around '+n2s(cwl)
        bin=where(wlvec ge cwl-0.05 and wlvec le cwl+0.05)
      endelse
      xy_fits,tip,bin=bin,i=tipimg
    endelse
  endif else begin
    print,'Reading image between (',cwl,',',cwl+wlr,')'
    bin=where(wlvec ge cwl and wlvec le cwl+wlr)
    if keyword_set(i) then xy_fits,tip,bin=bin,i=tipimg
    if keyword_set(q) then xy_fits,tip,bin=bin,q=tipimg
    if keyword_set(u) then xy_fits,tip,bin=bin,u=tipimg
    if keyword_set(v) then xy_fits,tip,bin=bin,v=tipimg
  endelse
  
  tipimg=total(tipimg,1)
  
  
  return,tipimg
  
end
