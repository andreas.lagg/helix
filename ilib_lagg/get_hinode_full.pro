function get_hinode_full,hindir,reread=reread,error=error
  common hinodecom,curhin,oldhin,data
  
  curhin=hindir
  newhin=n_elements(oldhin) eq 0
  error=0
  if newhin eq 0 then newhin=oldhin ne curhin
  if newhin or keyword_set(reread) then begin
    ffits=file_search(hindir+'/*.fits',count=fcnt)
    if fcnt eq 0 then begin
      message,/cont,'No FITS file found: '+hindir+'/*.fits'
      error=1
      return,-1
    endif else print,'Found '+n2s(fcnt)+' Hinode FITS-files.'
                                ;get header for size of data set
    for i=0,fcnt-1 do begin
      tdat=readfits_ssw(ffits(i),thdr,/silent,/nodata)
      thst=fits_header2st(thdr)      
      if i eq 0 then fhst=replicate(thst,fcnt) else begin
        newst=fhst(i)
        struct_assign,thst,newst
        fhst(i)=newst
      endelse
    endfor
    for i=0,fcnt-1 do begin
        dat=readfits_ssw(ffits(i),hdr,/silent)
        dat=hinode_corr(dat,hdr)
        hst=fhst(i)
        if i eq 0 then begin
          sz=size(dat)
          nwl=sz(1)
          ny=sz(2)
          nscan=minmaxp(fhst.scn_step)
          nsidx=minmaxp(fhst.slitindx)
          nsi=nsidx(1)-nsidx(0)+1
          nsc=nscan(1)-nscan(0)+1
          nx=(nsi)*(nsc)
          wlvec=dblarr(nx,nwl)
          icont=fltarr(nx,ny)+!values.f_nan
          if n_elements(data) ne 0 then dummy=temporary(data)
          data_dir=strmid(ffits(i),0,strpos(ffits(i),'/',/reverse_search)+1)
          data= $
            {dir:data_dir,sp:lonarr(nwl,ny,4,nx),header:replicate(hst,nx),$
             fits:strarr(nx),nfits:nx,nwl:nwl,nx:nx,ny:ny, $
             wl:temporary(wlvec),icont:temporary(icont),name:'', $
             valid:bytarr(nx),idx:intarr(nx)-1,imin:-1,imax:-1}
        endif
        it=(hst.scn_step-min(nscan))*nsi+hst.slitindx-nsidx(0)
;        data.header(i)=hst
        tmp=data.header(it)
        struct_assign,hst,tmp
        data.header(it)=tmp

                                ;check if ccx is present
        cx=ffits(i)+'.ccx'
        fi=file_info(cx)
        if fi.exists then begin
          read_ccx,cx,icont=icimg,error=xerr,wl_vec=wl,verbose=0
          data.icont(it,*)=icimg
          data.wl(it,*)=wl
        endif else begin
          message,/cont,'No ccx-file found for '+ffits(i)
          data.icont(it,*)=1.
          data.wl(it,*)=findgen(nwl)
        endelse
        data.sp(*,*,*,it)=dat
        data.fits(it)=ffits(i)
        data.valid(it)=1b
        data.idx(it)=i
        if i eq 0 then data.imin=it
        if i eq fcnt-1 then data.imax=it
        
                                ;norm to icont
;      for is=0,3 do data.sp(*,*,is,it)= $
;        data.sp(*,*,is,it)/(reform(data.icont(it,*)) ## (fltarr(nwl)+1.))
        
        
        if fix(i*80./fcnt) ne fix((i-1.)*80./fcnt) then print,'.',format='(a,$)'
      endfor
      print
      data.name='SOTSP_'+strmid(data.header(data.imin).date_obs,0,10)+ $
        'T'+strmid(data.header(data.imin).date_obs,11,2)+ $
        strmid(data.header(data.imin).date_obs,14,2)+'-'+ $
        strmid(data.header(data.imax).date_obs,11,2)+ $
        strmid(data.header(data.imax).date_obs,14,2)    
      oldhin=curhin
    endif
  return,data
end
