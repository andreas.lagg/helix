pro he_analyze,reread=reread,ps=ps,sigma=sigma
  common observation1,obs
  
  if n_elements(sigma) eq 0 then sigma=4.0 ;count pixels above this sigma level
  nmin=3                        ;# of wl-pixels which must ly above sigma level
                                ;define binning
  binx=1
  biny=2
  binwl=4
  print,'Binning: binx=',binx,', biny=',biny,', binwl=',binwl
  dx=0.32
  dy=0.18
  
  wlrgn=[10820d,10825d]
  inwln=where(obs.wl ge wlrgn(0) and obs.wl le wlrgn(1))
    
  if n_elements(obs) eq 0 or keyword_set(reread) then begin
    file='/home/lagg/data/VTT-data/VTT-Apr08/07May08/07may08.004detrend-0?cc'
    xy_fits,file,i=dati,q=datq,u=datu,v=datv,struct=xyfst
    read_ccx,file+'x',wl_disp=wl_disp,norm_cont=norm_cont, $
      wl_off=wl_off,icont=icimg,error=xerr,wl_vec=wl_vec    
    sz=size(dati)
    obs={i:temporary(dati),q:temporary(datq), $
         u:temporary(datu),v:temporary(datv),wl:wl_vec,ic:icimg, $
         noiseq:fltarr(sz(2),sz(3)),noiseu:fltarr(sz(2),sz(3)), $
         noisev:fltarr(sz(2),sz(3))}
    
                                ;compute noise level
    obs.noiseq=getrms(obs.q(inwln,*,*))
    obs.noiseu=getrms(obs.u(inwln,*,*))
    obs.noisev=getrms(obs.v(inwln,*,*))
  endif
  
  nq=mean(obs.noiseq)
  nu=mean(obs.noiseu)
  nv=mean(obs.noisev)
  
  
  
  
                                ;identify He-WL Pos and cut data cube
  wlrg=10830.3d + [-0.5,0.8]
  inwl=where(obs.wl ge wlrg(0) and obs.wl le wlrg(1))
  cubei=obs.i(inwl,*,*)
  cubeq=obs.q(inwl,*,*)
  cubeu=obs.u(inwl,*,*)
  cubev=obs.v(inwl,*,*)
  sz=size(cubei)
  nwl=sz(1)/binwl
  nx=sz(2)/binx
  ny=sz(3)/biny
  nfact=sqrt(float(binx)*biny*binwl)
  nq=nq/nfact
  nu=nu/nfact
  nv=nv/nfact
  
  
  
  nq=getrms(congrid(obs.q(inwln,*,*),n_elements(inwln)/binwl,nx,ny))
  nu=getrms(congrid(obs.u(inwln,*,*),n_elements(inwln)/binwl,nx,ny))
  nv=getrms(congrid(obs.v(inwln,*,*),n_elements(inwln)/binwl,nx,ny))
  nq=mean(nq)
  nu=mean(nu)
  nv=mean(nv)
  
  
  
  print,'Noise level Q: ',nq
  print,'Noise level U: ',nu
  print,'Noise level V: ',nv
  
  cubei=congrid(cubei,nwl,nx,ny)
  cubeq=congrid(cubeq,nwl,nx,ny)
  cubeu=congrid(cubeu,nwl,nx,ny)
  cubev=congrid(cubev,nwl,nx,ny)
  
  above_sigmaq=total(cubeq ge sigma*nq,1)
  above_sigmau=total(cubeu ge sigma*nu,1)
  above_sigmav=total(cubev ge sigma*nv,1)
  
  print,'Fraction of pixels above sigma*'+n2s(sigma,format='(f6.1)')+' (at least '+n2s(nmin)+' pix in WL-range of He)'
  ntot=float(nx*ny)
  fraq=total(above_sigmaq ge nmin)/ntot
  frau=total(above_sigmau ge nmin)/ntot
  frav=total(above_sigmav ge nmin)/ntot
  print,'Q: ',fraq
  print,'U: ',frau
  print,'V: ',frav
  
  !p.charsize=1.2
  psset,ps=ps,file='he_stat_sig'+n2s(sigma,format='(f5.1)')+'.ps',/encaps,/no_x,size=[18.,22],/pdf
  userlct,coltab=0,/reverse,/full
  image_cont_al,obs.ic,xrange=[0,nx*binx]*dx,yrange=[0,ny*biny]*dy, $
    cont=0,/aspect,/cut,xtitle='x [arcsec]',ytitle='y [arcsec]', $
    ztitle='Intensity (counts)', $
    title='Pixels above '+n2s(sigma,format='(f6.1)')+ $
    '*RMS-noise, (for >='+n2s(nmin)+' WL-pixels)',position=[0.1,0.30,0.95,0.95]
  
                                ;mark pixels above noise
  color=((above_sigmaq ge nmin)*2^0+ $
         (above_sigmau ge nmin)*2^1+ $
         (above_sigmav ge nmin)*2^2)
  icol=where(color ge 1)
  
  userlct
  rgb=intarr(8,3)
  rgb(0,*)=[255,  0,  0]
  rgb(1,*)=[  0,255,  0]
  rgb(2,*)=[  0,  0,255]
  rgb(3,*)=[255,255,  0]
  rgb(4,*)=[255,  0,255]
  rgb(5,*)=[  0,255,255]
  rgb(6,*)=[255,170, 20]
  tvlct,/get,r,g,b
  for i=1,7 do begin
    r(i+1)=rgb(i,0)
    g(i+1)=rgb(i,1)
    b(i+1)=rgb(i,2)
  endfor
  tvlct, r,g,b
  ax=indgen(binx)
  ay=indgen(biny)
  for i=0,binx-1 do for j=0,biny-1 do begin
    plots,((icol mod nx)*binx+ax(i))*dx, $
      (fix(icol/nx)*biny+ay(j))*dy,color=color(icol),psym=3
  endfor
  ypos=ny*biny*dy*0.30
  ypos=0.25
  xyouts_shadow,/normal,0.02,ypos,color=9,shadow_color=16,'  Colors:'
  quv=['Q','U','V']
  lspc='!C'
  for i=1,7 do begin
    idx=[i mod 2,i/2 mod 2, i/4 mod 2]
    wi=where(idx eq 1)
    lspc=lspc+'!C'
    xyouts_shadow,/normal,0.02,ypos,color=i,shadow_color=9, $
      lspc+add_comma(sep=' AND ',quv(wi))
    xyouts_shadow,/normal,.35,ypos,color=i,shadow_color=9,align=1, $
      lspc+string(format='(f6.2)',total(color eq i)*100./nx/ny)+'%'
  endfor
  lspc='!C!C'+lspc
  xyouts,/normal,0.02,ypos,lspc+'Total'
  xyouts,/normal,0.35,ypos,align=1, $
    lspc+string(format='(f6.2)',total(color ge 1)*100./nx/ny)+'%'
  xyouts,0,0.02,/normal,'Noise Level (Q,U,V) = ('+add_comma(sep=',',string([nq,nu,nv],format='(e8.1)'))+')' 
  xyouts,0,0.00,/normal,'Fraction of pixels above '+n2s(sigma,format='(f6.1)')+'*RMS-noise (Q,U,V) = ('+add_comma(sep=',',n2s([fraq,frau,frav],format='(f8.3)'))+')' 
  
  psset,/close
  
  stop
end
