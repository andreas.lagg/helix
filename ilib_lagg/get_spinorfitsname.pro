function get_spinorfitsname,file,obs=obs,synth=synth,atmos=atmos, $
                            lamobs=lamobs,error=error,silent=silent,all=all
  common fits,fits
  
  error=1
;  typ=['_atmos','_obs','_profs','_obs_lam']
  typ=['_atmos_ambig','_atmos','_obs','_profs','_obs_lam'];'_atmos'
  i=0
  repeat begin
    fsplit=strsplit(/regex,/extract,file,typ(i),count=nsp)
    i=i+1
  endrep until nsp eq 2 or i ge n_elements(typ)
  if nsp ne 2 then begin
    if keyword_set(silent) eq 0 then begin
      print,'Cannot determine filename for spinor.'
      print,'The spinor atmosphere filename must be:  *_atmos*.fits'
    endif
    return,''
  endif
  if keyword_set(synth) then add='_profs'
  if keyword_set(atmos) then add=['_atmos_ambig','_atmos']
  if keyword_set(obs) then add='_obs'
  if keyword_set(lamobs) then add=['_lam','_obs_lam']
  if keyword_set(all) then $
    add=['_profs','_atmos_ambig','_atmos','_obs','_lam','_obs_lam']
  
                                ;check if multiple spectra do exist
  ispec=0
  repeat begin
    if ispec eq 0 then addspec='' else addspec='.'+n2s(ispec)
    ia=0
    exists=0
    repeat begin
      rf=fsplit(0)+add(ia)+addspec+fsplit(1)      
      fi=file_info(rf)
      ia=ia+1
      if fi.exists then begin
        if n_elements(retfile) eq 0 then $
          retfile=rf else retfile=[retfile,rf]
      endif
      exists=exists or fi.exists
    endrep until ia eq n_elements(add) ;or fi.exists eq 1
    ispec=ispec+1
  endrep until exists eq 0 and ispec ge 2
  
  
  ; if n_elements(fits) ne 0 then begin
  ;   if keyword_set(obs) then base=fits.obsfile 
  ;   if keyword_set(synth) then base=fits.synthfile
  ;   if n_elements(base) ne 0 then if base(0) ne '' then begin
  ;     dpos=reverse(where(byte(base(0)) eq (byte('.'))(0)))
  ;     if n_elements(dpos) ge 2 then begin
  ;       retfile=file_search(strmid(base,0,dpos(1)+1)+'[0-9].fits')
  ;     endif else retfile=base
  ;   endif
  ; endif
  
  
  if n_elements(retfile) eq 0 then retfile='' $
  else error=0
  return,retfile
end

