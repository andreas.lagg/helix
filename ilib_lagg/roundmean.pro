function roundmean, u, maxradius=maxradius
;
; returns angular average along radius out to maxradius
;
  if n_params() lt 1 then begin
     message, /info, 'rndmn = roundmean(image, maxradius=maxradius)'
     retall
  endif

imageSize = size(u)
nx = imageSize(1)
ny = imageSize(2)
 
  if (n_elements(maxradius) ne 0) then begin  ; cut out central part image
    v = u(nx/2-maxradius:nx/2+maxradius,ny/2-maxradius:ny/2+maxradius)
  endif else begin
    v = u
  endelse

imageSize = size(v)
nx = imageSize(1)
ny = imageSize(2)

c = nx/2
;r = radius(c)    ; matrix with radial coordinates to center
r = shift(dist(nx), nx/2, nx/2)
r = round(r)     ; integers

rndmn = fltarr(c)
for i = 0, c-1 do begin 
  indx = where(r eq i, count)
  if count ne 0 then rndmn(i) = total(v(indx))/float(count) $
  else rndmn(i) = 0
endfor
return, rndmn
end
