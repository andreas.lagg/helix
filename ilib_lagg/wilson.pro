;compute wilson depression using method in shibu et al., 2004

function wilson,reread=reread,show=show,quietsun=quietsun,hsra=hsra,verbose=verbose
  common wilsondat,data,hdr,hambig,hsrasp,zwhsra,zwqs
  
  verbose=keyword_set(verbose)
  show=keyword_set(show)
  quietsun=keyword_set(quietsun)
  hsra=keyword_set(hsra)
  if quietsun eq 0 and hsra eq 0 then hsra=1
    ltmin=-3.0
  
  dir='~/data/Hinode/30nov06/2D_double/'
  if n_elements(data) eq 0 or keyword_set(reread) then begin
    hsrafile='/home/lagg/work/papers/2014_lightbridge/data/hsrasp630.dat'
    print,'Reading '+hsrafile
    readatm,hsrafile,hsrasp,info,ndpts=ndpts
    hsrasp=reform(hsrasp[0,*,0:ndpts-2])
    print,info
    
    print,'Reading '+dir+'inverted_atmos_ambig_maptau.1.fits'
    data=read_fits_dlm(dir+'inverted_atmos_ambig_maptau.1.fits',0,hdr)
    mrd_head,dir+'inverted_atmos_ambig.fits',hambig
    
    
    sz=size(data,/dim)
    ih=sxpar(hdr,'HEIGHT')-1
    ip=sxpar(hdr,'PRESS')-1
    ib=sxpar(hdr,'FMAG')-1
    ig=sxpar(hdr,'GAMMA')-1
    ia=sxpar(hdr,'CHI')-1
    
    nt=sz[2]
    nx=sz[0]
    ny=sz[1]
    
    lttop=sxpar(hdr,'LTTOP')
    ltbot=sxpar(hdr,'LTBOT')
    ltinc=sxpar(hdr,'LTINC')
    ltvec=findgen(nt)*ltinc+lttop
    
    qsx=[50,150]                ;QS area (for atmosphere outside spot)
    qsy=[50,150]
    atm_qs=(total(total(data[qsx[0]:qsx[1],qsy[0]:qsy[1],*,*],1),1)/ $
            (qsx[1]-qsx[0]+1.)/(qsy[1]-qsy[0]+1.))
    
    
                                ;compute bx,by,bz
    lon=sxpar(hambig,'LON')
    lat=sxpar(hambig,'LAT')
    b0angle=sxpar(hambig,'B0ANGLE')
    pangle=sxpar(hambig,'PANGLE')
    
    bxyz=fltarr(nx,ny,nt,3)
    bxyz_qs=fltarr(nt,3)
    for it=0,nt-1 do begin
      bia=transpose([[[data[*,*,it,ib]]], $
                     [[data[*,*,it,ig]]], $
                     [[data[*,*,it,ia]]]],[2,0,1])
      bxyz[*,*,it,*]=transpose(bincazi2xyz(bia,lon=lon,b0angle=b0angle, $
                                           lat=lat,pangle=pangle),[1,2,0])
      bxyz_qs[it,*]=bincazi2xyz(reform(atm_qs[it,[ib,ig,ia]]), $
                                lon=lon,lat=lat,b0angle=b0angle,pangle=pangle)
    endfor
    
    zwqs=fltarr(nx,ny)+!values.f_nan
    dummy=min(abs(ltvec),ilt0)
    p0=data[*,*,*,ip]+bxyz[*,*,*,2]^2/8/!pi
    p0_qs=atm_qs[*,ip]+bxyz_qs[*,2]^2/8/!pi
                                ;determine height where pressure of
                                ;(x,y) pixel is equal to the pressure
                                ;of the average quiet Sun reference
                                ;atmosphere at logtau=-1
    
                                ;find pressure of QS at logtau=0
    plt0=p0_qs[ilt0]
      
    for ix=0,nx-1 do for iy=0,ny-1 do begin
      dummy=min(abs(p0[ix,iy,*]-plt0),imin)
      if imin ge 1 and imin lt nt-2 and ltvec[imin] ge ltmin then begin
        zwqs[ix,iy]=-data[ix,iy,imin,ih]
      endif
    endfor
                                ;determine height where the pressure
                                ;of the pixel (x,y) at logtau=0 is
                                ;equal to the pressure in the
                                ;reference (HSRASP) atmosphere
    zwhsra=fltarr(nx,ny)+!values.f_nan
    lt_hsra=reform(hsrasp[0,*])
    z_hsra=reform(hsrasp[1,*])
    p0_hsra=reform(hsrasp[3,*])
    for ix=0,nx-1 do for iy=0,ny-1 do begin
      dummy=min(abs(p0_hsra-p0[ix,iy,ilt0]),imin)
      if imin ge 1 and imin lt n_elements(z_hsra)-2 then begin
        zwhsra[ix,iy]=z_hsra[imin]
;plot,z_hsra,p0_hsra,/ytype &
;plots,color=1,z_hsra[imin],p0[ix,iy,ih],psym=4 &  wait,.05
      endif
    endfor
                                ;shift zscale to have average QS
                                ;Wilson depression equal to zero
    dummy=min(abs(p0_hsra-p0_qs[ilt0]),imin)
    dz=z_hsra[imin]-atm_qs[ilt0,ih]
    zwhsra=zwhsra-dz
  endif  
      
  @greeklett.pro
    if hsra then begin
      zw=zwhsra
      if verbose then print,'Using HSRASP as reference atmosphere'
      desc='ref: HSRASP'
    endif
    if quietsun then begin
      zw=zwqs
      if verbose then print,'Using average Quiet Sun as reference atmosphere'
      desc='ref: avg. QS (log'+f_tau+'!Lmin!N='+n2s(ltmin,format='(f10.1)')+')'
    endif
  
  
    zw=zw*1e-5                  ;cm->km
    
    if show then begin
      rx=[0,797]
      ry=[0,899]
      p2a=0.08
      p2a=1
      
      userlct,/full,verbose=0,/reverse
      image_cont_al,/cut,/aspect,contour=0,zw[rx[0]:rx[1],ry[0]:ry[1]], $
                    xrange=rx*p2a,yrange=ry*p2a, $
                    title='Wilson Depression '+desc,$
                    ztitle='depression [km]', $
                    xtitle='x [arcsec]',ytitle='y [arcsec]'
    endif
    
  return,zw
end
