function azi_qu2xy,azi,headerst=headerst,test=azi_test,text=text
  common azimsgfirst,first
  common pikaia,pikaia_result,oldsav,prpar
  
  verbose=1
  if n_elements(pikaia_result) ne 0 then begin
    if n_elements(headerst) eq 0 then headerst=pikaia_result.header
    verbose=pikaia_result.input.verbose
    iflag=1 
  endif else iflag=0
  
  head_flag=n_elements(headerst) ne 0
  if head_flag then head_flag=n_tags(headerst) ge 1
  if head_flag then begin ;header information available
                                ;check if VTT
    htag=tag_names(headerst)
    vtt=0
    if max(htag eq 'TELESCOP') then $
      vtt=strpos(headerst.telescop,'VTT') ge 0
    hinodesp=0
    if max(htag eq 'TELESCOP') then if max(htag eq 'INSTRUME') then $
      hinodesp=(strpos(headerst.telescop,'HINODE') ge 0 and $
                strpos(headerst.instrume,'SOT/SP') ge 0)
    case 1 of
      vtt eq 1: begin           ;VTT
                                ;compare header and input-file values
                                ;for relevant entries to write out
                                ;warning if values do not agree
        hcheck=['SLITORIE','M1ANGLE']
        icheck=['SLIT_ORIENTATION','M1ANGLE']
        tol=['1.','1.']
        for i=0,n_elements(icheck)-1 do begin
          dummy=execute('hval=(headerst.'+hcheck(i)+')(0)')
          dummy=execute(hcheck(i)+'=hval') ;take header value
          if iflag eq 1 then begin
            dummy=execute('ival=pikaia_result.input.obs_par.'+icheck(i))
            if abs(hval-ival) gt tol(i) then begin
              message,/cont,'WARNING: '+icheck(i)+ $
                ' in input-file ('+n2s(ival,format='(f10.2)')+ $
                ') differs from header value ('+n2s(hval,format='(f10.2)')+ $
                '). Using value from input-file: '+n2s(ival,format='(f10.2)')
              dummy=execute(hcheck(i)+'=ival')
            endif; else dummy=execute(hcheck(i)+'=hval')
          endif; else dummy=execute(hcheck(i)+'=hval')
        endfor
                                ;get P0 angle, declination of sun
        if max(tag_names(headerst) eq 'DATEOBS') eq 1 then $
          dateobs=headerst.dateobs $
        else if max(tag_names(headerst) eq 'DATE_OBS') eq 1 then $
          dateobs=strmid(headerst.date_obs,0,10)+' '
        utime=strmid(headerst.utime(0),0,8)
        atime=anytim2utc(dateobs+utime,/ecs)
        p0=get_rb0p(atime,/deg,/pangle)
        sun_pos,(anytim(atime)/86400d + julday(01,01,1979,00,00,00) $
                 -julday(01,01,1900,00,00,00)), $
          longitude, ra, dec, app_long, obliq
        
                                ;calculate image rotation caused by
                                ;orientation of M1
        dec_rad=dec*!dtor
        az_rad=m1angle*!dtor
        phi_rad=(28.+18./60)*!dtor    ;28deg 18� 00" North, VTT location
        im_rot = (-asin( cos(phi_rad)*sin(az_rad)/cos(dec_rad))+ az_rad)*180./!pi
;wobei phirad die geographische Breite (28 deg) in radian ist, azrad
;der Azimuth vom ersten Spiegel in radian, und declsun die Deklination
;der Sonnen relativ zur Aequatorebene (max. +-23 deg). im_rot ist
;maximal sowas wie 20 Grad (fuer 8.12.07 waren es 16 Grad bei 90 Grad
;Azimuth).
;        print,'AZI_QU2XY:',slitorie,im_rot,m1angle
        
                                ;scan direction: defined by STEPANGL in header
        
                                ;slit orientation is in TNS (terr. N-S)
                                ;+Q is along CNS (celestial NS)
                                ;im_rot is angle between TNS and CNS
        reverse=0
        year=fix(strmid(dateobs,0,4))
        ; month=fix(strmid(dateobs,5,2))
        ; case 1 of
        ;   year le 2005: if headerst.stepangl gt 0 then reverse=1
        ;   else: if headerst.stepangl le 0 then reverse=1
        ; endcase
                                ;scan direction: check if it is
                                ;negative (slitorie-stepangle=0) or
                                ;positive (slitorie-stepangle=180)
        slitorie=headerst.slitorie(0)
        asum=(slitorie-headerst.stepangl +720) mod 360
        case 1 of
          asum ge 170 and asum le 190: reverse=0
          asum le 10 or asum ge 350: reverse=1
          else: begin
            message,/cont,'Scan direction not perpendicular to slit.' + $
              ' Assume normal scan direction.'
;            reverse=1
            reverse=0
          end
        endcase
                                ;don't know why, but in 2004 the sign
                                ;is opposite:
;        if year eq 2004 then reverse=reverse eq 0
;reverse=0
        
                                ;rotation angle:
        if reverse then rstr='-' else rstr='+'
                                ;analyis of a large number of data
                                ;sets showed that mode 0 is the
                                ;correct one.
;       if n_elements(azi_test) eq 0 then azi_test=0
       if n_elements(azi_test) eq 0 then azi_test=11
;       im_rot=0
;       slitorie=0 
       
        
        if azi_test/10 ge 1 then a90='+90' else a90=''
        case azi_test mod 10 of 
          0: cmd=rstr+'(azi+im_rot+slitorie+180'+a90+')'
          1: cmd=rstr+'(azi-im_rot+slitorie+180'+a90+')'
          2: cmd=rstr+'(azi+im_rot-slitorie+180'+a90+')'
          3: cmd=rstr+'(azi-im_rot-slitorie+180'+a90+')'
          4: cmd=rstr+'(azi+im_rot+slitorie'+a90+')'
          5: cmd=rstr+'(azi-im_rot+slitorie'+a90+')'
          6: cmd=rstr+'(azi+im_rot-slitorie'+a90+')'
          7: cmd=rstr+'(azi-im_rot-slitorie'+a90+')'
        endcase
        
        dummy=execute('ret_azi='+cmd)
        text=[cmd,'SLIT='+n2s(slitorie,format='(f10.1)')+$
              ', M1='+n2s(m1angle,format='(f10.1)')+$
              ', IMROT='+n2s(im_rot,format='(f10.1)')+$
              ', STPANG='+n2s(headerst.stepangl,format='(f10.1)')]
        message,/cont,add_comma(text)        

;        ret_azi=-(azi-slitorie+im_rot+0.)
        
 ; 1) Spalt im header ist 132 Grad.
 ; 2) Spalt im TNS ist -180+132 = -48 Grad.
 ; 3) Spalt im CNS ist -48+P0 = -34.8 Grad
 ; 4) Bild ist rotiert wegen Spiegel M1 bei 90 Grad, und zwar um 16 Grad, daher:
 ;     Spalt im Solaren System ist -34.8+16 = -18.8 Grad
 ; 5) +Q ist entlang CNS
       
      end
      hinodesp eq 1: begin      ;Hinode SP: +Q is along +X solar
        ret_azi=+azi
      end
      else: begin
        istip=check_tipmask(pikaia_result.input.observation)
        if istip then begin
          yn=dialog_message(['Observation is TIP-format,', $
                             'but no TIP-header Information could be found.', $
                             '','Make the data file accessible to xdisplay', $
                             'by setting the correct link for', $
                             './profile_archive','','','Continue ?'], $
                            /default_no,/question)
          if yn ne 'Yes' then reset
        endif
        message,/cont,'Unknown telescope. Returning +azi'
        ret_azi=+azi
      end
    endcase
  endif else begin              ;no header infromation
    message,/cont,'No valid header information. Returning +azi'
    ret_azi=+azi
  endelse
  
  return,ret_azi
  
end

;=============================================================================
;Implementation (Feb 05), A. Lagg
; - 2001 and 2002 data: azi_map =  azi_uq + slit_ori
; - 2003 data:          azi_map =  azi_uq + slit_ori +90
; - 2004 data: azi_map = -azi - slitori   if scandir=180� (=slitori-stepangle) 
;              azi_map = +azi + slitori   if scandir=  0� (=slitori-stepangle) 


;=============================================================================
;e-mail to Manolo, date: Oct 05, 2004
; Hi Manolo,

; I am somehow still confused. I did some tests and I cannot find a unique conversion from the azimuth derived by atan(U/Q) to the azimuth converted to the scan direction (map). I attach a files with a table showing the possible azimuth conversions.
; The conversions work like follows: I use the azimuthal angle derived directly from the inversion (basically atan(U/Q)) and then I add slit orientation angle, 90� or I invert the angle. I try to match observations of sunspot to find out which conversion to use:

; These are the conversions which work (in the attached tabel they are marked in green - a ps-file showing the spot and the derived azimuth angle using this conversion is attached):

; - 2001 and 2002 data: azi_map =  azi_uq + slit_ori
; - 2003 data:          azi_map =  azi_uq + slit_ori +90
; - 2004 data:          azi_map = -azi_uq + slit_ori +90

; I reduced all data with the same acum2iquv11 and xtalk2e routines. I used the same inversion code for all data sets.

; This is the current status of my investigation - I am a little bit lost now. Maybe you have an idea?

; Thanks,
; Andreas


;=============================================================================
;e-mail  on azimuth information:

; Dear Shibu

; the direction of positive Q in the TIP data is a little bit tricky:

; i) Q positive direction is always the terrestrial N-S line.

; ii) the slit direction is variable, because the whole spectrograph
;     can be rotated. The slit direction is always vertical in the maps.
;    This means that to find out the direction of positive Q you
;    must know the spectrograph (slit) orientation.

; In your case, if you look at the spot, the slit is vertical and positive Q is along a line which forms 333.2 degrees with the slit (positive angles clockwise) and the limb direction is at 322.4 degrees. So, positive Q is almost parallel to the limb (10.8 degrees).


;     \        _
;      \     /   \     |
;       \    |    |    |
;        \    \   /    |
;         \    \_/     |
;          \    SPOT        SLIT
;           \             DIRECTION
;            \            (0 DEGREES)
;             \
;        LIMB (322.4 DEGREES)

; In this diagram positive Q direction is 10.8 degrees more vertical than the limb.

; Please tell me if you have any other doubt

; Regards

; Manolo

; From mcv@ll.iac.es Fri May 16 15:58:53 2003
; Date: Tue, 12 Mar 2002 12:14:55 +0000 (WET)
; From: Manolo Collados Vera <mcv@ll.iac.es>
; To: mathew@linmpi.mpg.de
; Subject: Re: your mail

; Hi Shibu


; >> I have few questions about the 27Se99, NOAA 8706 observation,
; >>
; >> 1) In the data header, SLITPOSX and SLITPOSY are indicated at two 
; >> places, which are slightly differ in values (x=155, y=357, in the 
; >> begining and x=157, y=361 at the end), as I understood, these two 
; >> co-ordinates tells the x and y position of the slit center for center 
; >> of the whole image frame, am I right?
; >>


; Yes, you are right. And they should be the same. In fact, they
; represent the real coordinates of the slit center before the
; scanning is started (before going to the first scanning position)
; and after it has finished (when the telescope returns to the central
; position of the scan). They are slightly different because some
; roundoff errors may acumulate when moving the telescope at small
; steps to produce the scan.


; >> 2) Does the scaning is done always in the EW (earth's) direction, and 
; >> what does the value for SLITORIE (slit orientation, in this data it is 
; >> 206.77) mean?
; >>


; No. The scanning is always done perpendicularly to the slit. And the slit can be oriented at any angle on the sky. A slit orientation of 180 degrees means the slit is oriented NS and the scan is done E->W. Increasing the slit angle means rotating it N--> W (and equivalently with the scanning
; direction)

; Regards,

; Manolo


; From mcv@ll.iac.es Fri May 16 15:59:26 2003
; Date: Fri, 5 Apr 2002 19:55:16 +0100 (WET DST)
; From: Manolo Collados Vera <mcv@ll.iac.es>
; To: mathew@linmpi.mpg.de
; Subject: Re: your mail

; Hi Shibu

; Yes, the +Q direction in the 27SEP99 data is the same as in 21SEP99.

; To find that out, get the keyword SLITORIE (slit orientation) from the header and evalaute

; +Q direction in the map = 360 - SLITORIE

; (Note that changing this value by � 180 degrees, the orientation is the same)

; This angle is with respect to the slit, which is always vertical in the maps.

; Regards

; Manolo



; >> Dear Manolo,
; >>
; >> I have appended a mail, in which you explained slit and +Q direction 
; >> for the 21 SEP 1999 data set. Is this the same for the 27th SEP 99 
; >> data set also ? Is this information is available in the header file?
; >>
; >> regards,
; >> shibu
; >>
; >>
; >>
; >> On Wed, 25 Apr 2001, Manolo Collados Vera wrote:
; >>
; >
; >>> >
; >>> > Dear Shibu
; >>> >
; >>> > the direction of positive Q in the TIP data is a little bit tricky:
; >>> >
; >>> > i) Q positive direction is always the terrestrial N-S line.
; >>> >
; >>> > ii) the slit direction is variable, because the whole spectrograph
; >>> >     can be rotated. The slit direction is always vertical in the maps.
; >>> >    This means that to find out the direction of positive Q you
; >>> >    must know the spectrograph (slit) orientation.
; >>> >
; >>> > In your case, if you look at the spot, the slit is vertical and 
; >>> > positive Q is along a line which forms 333.2 degrees with the slit 
; >>> > (positive angles clockwise) and the limb direction is at 322.4 
; >>> > degrees. So, positive Q is almost parallel to the limb (10.8 
; >>> > degrees).
; >>> >
; >>> >
; >>> >     \        _
; >>> >      \     /   \    |
; >>> >       \   |     |   |
; >>> >        \   \    /   |
; >>> >         \   \__/    |
; >>> >          \    SPOT        SLIT
; >>> >           \             DIRECTION
; >>> >            \            (0 DEGREES)
; >>> >             \
; >>> >        LIMB (322.4 DEGREES)
; >>> >
; >>> > In this diagram positive Q direction is 10.8 degrees more vertical 
; >>> > than the limb.
; >>> >
; >>> > Please tell me if you have any other doubt
; >>> >
; >>> > Regards
; >>> >
; >>> > Manolo
; >>> >
; >
; >>
; >>



; From mcv@ll.iac.es Fri May 16 15:59:44 2003
; Date: Mon, 8 Apr 2002 18:33:03 +0100 (WET DST)
; From: Manolo Collados Vera <mcv@ll.iac.es>
; To: mathew@linmpi.mpg.de
; Subject: Re: your mail

; Dear Shibu

; due to another thing I am revising the positive Q direction
; in the data and I am not quite sure the information I sent you is correct. There might be a bug in the reduction program, and the positive Q direction may be different from what I told you. Do you have problems to interpret the data with the reference system I gave you? To understand some other data, I need to make some change in the reference system. I know what I have to do but do not understand why. It would help me if you tell me whether you find some inconsistency with the reference system I gave you.

; Best regards

; Manolo


; From mathew@linmpi.mpg.de Fri May 16 16:00:05 2003
; Date: Fri, 12 Apr 2002 16:35:50 +0200 (MEST)
; From: Shibu Mathew <mathew@linmpi.mpg.de>
; To: Manolo Collados Vera <mcv@ll.iac.es>
; Subject: Re: your mail

; Dear Manolo,

; Could you fix the problem with the positive Q-direction? In that case does I have to do a correction for 27Sep99 data also?

; regards,
; shibu
; -------------------------------------------------------------------------
; Shibu K. Mathew              Phone: +49-5556-979-408 (o)
; Post-Doctoral Fellow           : +49-5556-979-193 (r)
; Max Planck Institute fur Aeronomie      e-mail: mathew@linmpi.mpg.de
; Max Planck Str. 2, D-37191
; Katlenburg-Lindau
; Germany
; --------------------------------------------------------------------------



; From mcv@ll.iac.es Fri May 16 16:00:14 2003
; Date: Fri, 12 Apr 2002 20:07:38 +0100 (WET DST)
; From: Manolo Collados Vera <mcv@ll.iac.es>
; To: mathew@linmpi.mpg.de
; Subject: Re: your mail

; Hi Shibu

; I still have not found the solution to the problem of the Q-positive direction. I have been working almost full time on that during the last three days. I have revised all the equations and the data reduction. For me everything is right, but it works with some files but it does not with others. I must confess I feel lost, because I cannot guess where the problem is. I will continue until next monday. On Tuesday I am flying for a meeting for the whole week. I will tell you in case I find the mistake.

; Regards

; Manolo


; From mathew@linmpi.mpg.de Fri May 16 16:00:40 2003
; Date: Thu, 23 Jan 2003 23:48:04 +0100 (CET)
; From: Shibu Mathew <mathew@linmpi.mpg.de>
; To: Dr. Manolo Collados <mcv@ll.iac.es>
; Cc: Shibu Mathew <mathew@linmpi.mpg.de>

; Dear Manolo,

; Wishing you a very happy new year!

; Did you make any new changes in the reduction routine regarding the +Q direction? It seems when we use your new reduction program, on the old data set,(which we already have reduced data with your old program), compairing with reduced data using your old version of  reduction programs, the Q and U signals change sign.

; waiting for your reply,

; with regards,
; shibu

; -------------------------------------------------------------------------
; Shibu K. Mathew              Phone: +49-5556-979-408 (o)
; Post-Doctoral Fellow           : +49-5556-979-193 (r)
; Max Planck Institute fur Aeronomie      e-mail: mathew@linmpi.mpg.de
; Max Planck Str. 2, D-37191
; Katlenburg-Lindau
; Germany
; --------------------------------------------------------------------------


; From mcv@ll.iac.es Fri May 16 16:01:14 2003
; Date: Fri, 24 Jan 2003 10:52:19 +0000 (WET)
; From: Manolo Collados Vera <mcv@ll.iac.es>
; To: mathew@linmpi.mpg.de
; Subject: Re: Your Message Sent on Thu, 23 Jan 2003 23:48:04 +0100 (CET)

; Dear Shibu



; >> Wishing you a very happy new year!


; The same for you and your family.


; >>
; >> Did you make any new changes in the reduction routine regarding the +Q 
; >> direction? It seems when we use your new reduction program, on the old 
; >> data set,(which we already have reduced data with your old program), 
; >> compairing with reduced data using your old version of  reduction 
; >> programs, the Q and U signals change sign.


; Yes, there was a wrong rotation in the old programs. Now it is OK (or, at least, I think it is)


; Best wishes

; Manolo


