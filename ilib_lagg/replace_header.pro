function do_hreplace,header,var,pos=pos,replace=replace,tolerance=tolerance,val=val
  
  pos=strpos(header,var)
  val=''
  if pos eq -1 then return,header
  line=strmid(header,pos,strlen(header))
  val=(strmid(line,11,20))
  
  rethead=header
  if n_elements(replace) eq 1 then begin
    typ=size(replace,/typ)
    case typ of 
      1: replace=string(replace,format='(i20)')
      2: replace=string(replace,format='(i20)')
      3: replace=string(replace,format='(i20)')
      4: replace=string(replace,format='(e20.6)')
      5: replace=string(replace,format='(e20.6)')
      7: replace=replace+strjoin(replicate(' ',20-strlen(replace)))
      else: replace=string(replace)
    endcase
    if n_elements(tolerance) eq 1 then $
      if abs(double(val)-double(replace)) gt tolerance then begin
      print,'Replace Header Value: ',var
      print,'        OLD=',val,' NEW=',replace
      rethead=strmid(rethead,0,pos+10)+replace+ $
        strmid(rethead,pos+31-1,strlen(rethead))
    endif
  endif
  
  if strmid(val,0,1) eq '''' then val=strmid(val,1,strlen(val))
  if strmid(val,strlen(val)-1,1) eq '''' then val=strmid(val,0,strlen(val)-2)
  
  return,rethead
end

function replace_header,header
  common reduce,reduce
  
  if n_elements(reduce) eq 0 then return,header
  
  ifile=(where(strpos(header,'FILENAME') ne -1))(0)
  if ifile ne -1 then begin
    dummy=do_hreplace(header(ifile),'FILENAME',val=file)
    if strmid(file,0,strlen(reduce.obs)) ne reduce.obs then begin
      message,/cont,'reduce structure does not match observation file.'
      return,header
    endif
  endif
  
  rethead=header
  for i=0,n_elements(rethead)-1 do begin
    if reduce.slitposx ge -9998 then $
      rethead(i)=do_hreplace(rethead(i),'SLITPOSX', $
                             replace=reduce.slitposx,tolerance=5.)
    if reduce.slitposy ge -9998 then $
      rethead(i)=do_hreplace(rethead(i),'SLITPOSY', $
                             replace=reduce.slitposy,tolerance=5.)
    rethead(i)=do_hreplace(rethead(i),'SLITORI', $
                           replace=reduce.slitori,tolerance=0.5)
    rethead(i)=do_hreplace(rethead(i),'M1ANGLE', $
                           replace=reduce.angle,tolerance=0.5)
  endfor
  return,rethead
end
