;read in inverted_atmos.fits and create a FITS file to be used for the
;ambiguity routines in ~/work/polarimeter/aziambig (Leka, Metcalf).

pro ambig_prepare,filename,date_obs=date_obs,xcen=xcen,ycen=ycen, $
                  pixx=pixx,pixy=pixy,iseed=iseed,write_par=write_par
  
  f=get_filepath(filename)
  iafile=f.name
  dir=f.path
  
  data=read_fits_dlm(dir+iafile,0,hdr)
  nax=sxpar(hdr,'NAXIS*')
  
  rbp0=get_rb0p(date_obs,/deg)
  ll=xy2lonlat([xcen,ycen],date_obs)
  
  ilt=where(strpos(hdr,'LGTRF') eq 0)
  ibf=where(strpos(hdr,'BFIEL') eq 0)
  iaz=where(strpos(hdr,'AZIMU') eq 0)
  igm=where(strpos(hdr,'GAMMA') eq 0)
  
  nlt=n_elements(ilt)
  print,'Writing FITS files for ambig-routine:'
  for i=0,nlt-1 do begin
    bag=fltarr(nax[0],nax[1],3)
    lgtrf=(strsplit(hdr[ilt[i]],/extract))[4]
    ib=sxpar(hdr[ibf[i:i]],'BFIEL')-1
    bag[*,*,0]=data[*,*,ib]
    ig=sxpar(hdr[igm[i:i]],'GAMMA')-1
    bag[*,*,1]=data[*,*,ig]
    ia=sxpar(hdr[iaz[i:i]],'AZIMU')-1
    bag[*,*,2]=data[*,*,ia]
                                ;create header
    mkhdr,hdrout,bag
    sxaddpar,hdrout,'DATE_OBS',date_obs,'date of observation'
    sxaddpar,hdrout,'LGTAU',float(lgtrf),'log(tau) value'
    sxaddpar,hdrout,'PAR1','BFIEL','magnetic field strength [G]'
    sxaddpar,hdrout,'PAR2','GAMMA','magnetic field inclination [deg]'
    sxaddpar,hdrout,'PAR3','AZIMU','magnetic field azimuth [deg]'
    sxaddpar,hdrout,'XCEN',xcen,'x-position [arcsec]'
    sxaddpar,hdrout,'YCEN',ycen,'y-position [arcsec]'
    sxaddpar,hdrout,'PIXX',pixx,'pixel size in x direction [arcsec]'
    sxaddpar,hdrout,'PIXY',pixy,'pixel size in y direction [arcsec]'
    sxaddpar,hdrout,'LON',ll[0],'solar longitide [deg]'
    sxaddpar,hdrout,'LAT',ll[1],'solar latitide [deg]'
    sxaddpar,hdrout,'RADIUS',rbp0[0],'solar radius [arcsec]'
    sxaddpar,hdrout,'BANGLE',rbp0[1],'B angle [deg]'
    sxaddpar,hdrout,'P0ANGLE',rbp0[2],'P0 angle [deg]'
    
                                ;write file
    lgs=strcompress(/remove_all,string(lgtrf,format='(f6.2)'))
    fout=dir+'magfield_ltau'+lgs+'.fits'
    write_fits_dlm,bag,fout,hdrout,/create
    print,'   ',fout
    
    if n_elements(iseed) ne 1 then iseed=1
    istr=n2s(fix(iseed))
    print,'Write the following information to the par file for ''ambig'':'
    l=(get_filepath(fout)).name
    l=[l,'1  1  0          irflag   ibflag   iaflag       ']
    l=[l,'400 20 20 20     npad     nap      ntx      nty ']
    l=[l,'4e2              bthresh                        ']
    l=[l,'1   1  0         iaunit   ipunit   incflag      ']
    l=[l,istr+'   2  20        iseed    iverb    neq          ']
    l=[l,'1.  2. 0.995     lambda   tfac0    tfactr       ']
    l=[l,'1  2  3          iblong   ibtrans  ibazim         ']
    l=[l,'LON              longitude keyword                ']
    l=[l,'LAT              latitude keyword                 ']
    l=[l,'P0ANGLE          p-angle keyword                  ']
    l=[l,'BANGLE           b-angle keyword                  ']
    l=[l,'RADIUS           radius keyword                   ']
    l=[l,'PIXX             pixel size in x-direction keyword']
    l=[l,'PIXY             pixel size in y-direction keyword']
    print
    print,transpose(l)
    par=l
    if keyword_set(write_par) then begin
      openw,unit,/get_lun,'par_ltau'+lgs
      for j=0,n_elements(par)-1 do printf,unit,par[j]
      free_lun,unit
    endif
  endfor
  print
  print,'Run ''ambig'' to solve the azimuth ambiguity.'
  print,'After successfull ''ambig'' run IDL routine ''ambig_combine'' to create a ambiguity-resolved inverted_atmos.fits'
  print,'Usage: '
  print,'   ambig_combine,''inverted_atmos.fits'',''magfield_ltau*.fits'',''inverted_atmos_ambig.fits'''
  print
  
  
  
end

pro call
  
  ambig_prepare,'/home/lagg/data/Hinode/30nov06/2D_double/inverted_atmos.fits', $
                pixx=0.08,pixy=0.08, $
                date_obs='2006-11-30T23:30:00',xcen=-190.,ycen=-170.
end
