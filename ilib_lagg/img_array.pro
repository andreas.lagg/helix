pro img_array
  
  
  nx=3
  ny=2
  data=fltarr(nx,ny,300,1003)   ;dummy data array
  
  
  psset,/ps,/pdf,size=[14,23],file='~/tmp/img.pdf',/no_x
  userlct,/full
  
  
  xmargin=[.12,.86]             ;define plot region
  ymargin=[.05,.99]
  xsep=0.01                     ;separation of plots
  ysep=0.01
  
  xpos=findgen(nx+1)/nx*(xmargin[1]-xmargin[0])+xmargin(0)
  ypos=findgen(ny+1)/ny*(ymargin[1]-ymargin[0])+ymargin(0)
  
  yrange=[0,1002]*.16 ;in arcsec?
  xrange=[0,300]*.16 ;in arcsec?
  
  for ix=0,nx-1 do for iy=ny-1,0,-1 do begin
                                ;create dummy data set
    data[ix,iy,*,*]=dist(300,1003)
    
                                ;position of plot
    pos=[xpos[ix],ypos[iy],xpos[ix+1]-xsep,ypos[iy+1]-ysep]
    
                                ;x/y title
    ytitle='' & xtitle=''
    if ix ge 1 then ytn=strarr(32)+' ' $
    else begin 
      if n_elements(ytn) ne 0 then dummy=temporary(ytn)
      ytitle='y [arcsec]'
    endelse
    if iy ge 1 then xtn=strarr(32)+' ' $
    else begin
      if n_elements(xtn) ne 0 then dummy=temporary(xtn)
      xtitle='x [arcsec]'
    endelse
    
                                ;ztitle
    if iy eq 1 then ztitle='Magnetic Field [G]' $
    else ztitle='Inclination [deg]'
    
                                ;do plotting
    zrange=minmax(data[ix,iy,*,*])
    image_cont_al,reform(data[ix,iy,*,*]), $
                  /cut,/aspect,contour=0,label=ix eq  (nx-1), $
                  position=pos,/outside, $
                  xtickname=xtn,ytickname=ytn,zrange=zrange, $
                  xrange=xrange,yrange=yrange,/noerase, $
                  xtitle=xtitle,ytitle=ytitle,ztitle=ztitle
    
    
  endfor
  
  
  
  psset,/close
  
  
end
