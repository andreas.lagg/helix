;smooth surface of periodic values, eg. azimuth or inclination
;DO MEDIAN!
function smooth_periodic,array,sm,period=period,_extra=_extra
  
  if n_elements(period) ne 2 then return,smooth(array,sm,_extra=_extra)
  
  
  
  typ=size(array,/type)
  sz=size(array)
  arr=double(array);-period(0)
  dp=period(1)-period(0)
  
  for ii=2,1,-1 do begin
    for ixy=0,sz(ii)-1 do begin
      if ii eq 2 then arrc=arr(*,ixy) else arrc=arr(ixy,*)
      for i=1l,n_elements(arrc)-1 do begin
        n1=long(arrc(i-1)/dp)
        n=long(arrc(i)/dp)
        dummy=min(abs(arrc(i-1)-n1*dp-[arrc(i) mod dp,(arrc(i)+dp) mod dp]),imin)
        arrc(i)=arrc(i)+(n1-n+imin)*dp    
      endfor
      if sm gt 0 then arrc(*)=smooth(arrc(*),sm,_extra=_extra)
      if ii eq 2 then arr(*,ixy)=arrc else arr(ixy,*)=arrc
    endfor
    arr=((arr+dp) mod dp)       ;+period(0)
    gp1=where(arr gt max(period))
    if gp1(0) ne -1 then arr(gp1)=arr(gp1)-dp
  endfor
  
  if typ eq 4 then arr=float(arr)
  
  return,arr
end
