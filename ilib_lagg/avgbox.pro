;do a box-avaraging of the input array.
;cycle keyword: when upper and lower bound of an array is cyclic
;(eg. azimuth, where +90 is -90) then a normal averaging gives wrong
;results. A cyclic averaging looks for the 'true' average
; example: cycle=[-90,+90]
function avgbox,arr,nx=nx,ny=ny,binx=binx,biny=biny,cycle=cycle,weight=weight
  
  sza=size(arr)
  if sza(0) ne 2 then return,arr
  if n_elements(nx) eq 1 then binx=1>(float(sza(1))/nx)<(sza(1)) $
  else if n_elements(binx) eq 1 then $
    nx=long(float(sza(1))/binx+((sza(1) mod binx) ne 0)) $ 
  else message,'Specify NX or BINX'
  if n_elements(ny) eq 1 then biny=1>(float(sza(2))/ny)<(sza(2)) $
  else if n_elements(biny) eq 1 then $
    ny=long(float(sza(2))/biny+((sza(2) mod biny) ne 0)) $ 
  else message,'Specify NY or BINY'
  
  if nx eq sza(1) and ny eq sza(2) then return,arr
  
  if n_elements(weight) eq 0 then weight=fltarr(sza(1),sza(2))+1.
  
  docycle=n_elements(cycle) eq 2
  if docycle then begin
    range=max(cycle)-min(cycle)
    center=total(cycle)/2.
    mincy=min(cycle)
  endif
  
  retarr=fltarr(nx,ny)
  for ix=0,nx-1 do begin
    idx=0>(long([ix,ix+1]*binx))<(sza(1)-1)
    for iy=0,ny-1 do begin
      idy=0>(long([iy,iy+1]*biny))<(sza(2)-1)
      aa=arr(idx(0):idx(1),idy(0):idy(1))
      wgt=weight(idx(0):idx(1),idy(0):idy(1))
      ifin=where(finite(wgt) eq 0)
      if ifin(0) ne -1 then wgt(ifin)=0.
      if total(abs(wgt)) le 1e-8 then wgt(*)=1.
      wgt=wgt/(total(wgt))*n_elements(wgt)
      mv=moment(aa*wgt)
      if docycle then begin
        flip=where(aa lt center)
        if flip(0) ne -1 then begin
;          print,'OLD',aa
          aa(flip)=aa(flip)+range
          mv2=moment(aa*wgt)
                                ;if variance of cyclic data is better
                                ;then take the cyclic value
          if mv2(1) lt mv(1)*.99 then  begin
;            print,'NEW',aa
;            print,'VAR',ix,iy,[mv(0:1),mv2(0:1)]
            mv(0)=((mv2(0)+mincy+range) mod range) -mincy
          endif
        endif
      endif
      retarr(ix,iy)=mv(0)
    endfor
  endfor

  return,retarr
end
