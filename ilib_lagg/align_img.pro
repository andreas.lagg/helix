;routine to align two images.
;images amy have different size, scaling and rotation
pro align_img,transformed_image,reference_image

  
                                ;remove NANs
  nn=where(finite(transformed_image) eq 0)
  if nn(0) ne -1 then transformed_image(nn)= $
    total(transformed_image,/nan)/total(finite(transformed_image))
  nn=where(finite(reference_image) eq 0)
  if nn(0) ne -1 then reference_image(nn)= $
    total(reference_image,/nan)/total(finite(reference_image))
  
  pp = setpts_roi(transformed_image,reference_image)
  tt = caltrans(pp)
  pin = tt[*,0]
  qin = tt[*,1]
  
  retimg=aalign_images(transformed_image,reference_image, $
                       pin,qin,pout,qout)           
  stop
end
;    rotation =  -0.850298 degrees
;    scale x =  0.0514962
;    scale y =  0.251337
;    shift x =  -124.599
;    shift y =  -342.777

;    rotation =  4.04825 degrees
;    scale x =  0.213085
;    scale y =  0.120030
;    shift x =  -45.7209
;    shift y =  -215.651
