pro pco_movie
  
  
  file='~/tmp/pco1_meas1_20130615-080334-00008_486_100.fits'
  ff=get_filepath(file)
  data=readfits(file)
  sz=size(data,/dim)
  dark=readfits(ff.path+'pco1_dark_20130615-074646_486.fits',hdr)
  szd=size(dark,/dim)
  dimg=total(dark,3)/szd[2]
  flat=readfits(ff.path+'pco1_flat_20130615-074521_486.fits')
  szf=size(flat,/dim)
  fimg=total(flat,3)/szf[2]
  
  tmpdir='/home/lagg/tmp/tmpmovie/'
  spawn,' mkdir -p '+tmpdir+' ; rm -rf '+tmpdir+'*'
  for i=0,sz[2]-1 do begin
    img=float(data[*,*,i])
    img=(img-dimg)/(fimg-dimg)
    if i eq 0 then zrg=minmaxp(img,perc=99.8)
    psset,/ps,/no_x,size=[20,18],jpg=1, $
          file=tmpdir+n2s(i,format='(i4.4)')+'.ps',view=0
    image_cont_al,img,/cut,/aspect,contour=0, $
                  title='AR11768 (leading) 08:03 UT,' + $
                  ' frame #'+string(i,format='(i3.3)'), $
                  xrange=[0,sz[0]]*.027, $
                  yrange=[0,sz[1]]*.027,xtitle='x [arcsec]', $
                  ytitle='y [arcsec]',zrange=zrg
    xyouts,0,0,/normal,'RAW data - Flat and Dark corrected, approx. realtime'
    psset,/close
    
  endfor
  movie=ff.path+(strsplit(ff.name,'.fits',/regex,/extract))[0]+'.wmv'
  moviecmd='mencoder mf://'+tmpdir+'*.jpg -mf fps=6:type=jpg -ovc ' + $
           'lavc -lavcopts vcodec=wmv2:vbitrate=10000 -oac copy -o '+movie
  
  print,'Executing Movie command:'
  print,moviecmd
  spawn,moviecmd
  print,'Movie created: ',movie 
end
