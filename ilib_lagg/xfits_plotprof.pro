;plot profiles for xfits software
function set_specplot,wlin,ispec,data=data,device=device
  common xppwg,xppwg
  
  wl=wlin(0)
  
  if keyword_set(device) then xx=xppwg.xcrdev(0:xppwg.nxcr-1,*) $
  else xx=xppwg.xcr(0:xppwg.nxcr-1,*) 
  wl=min(xx)>wl<max(xx)
  idx=(where(wl ge xx(*,0) and wl le xx(*,1)))(0)>0
  ypos=xppwg.ycrdev(ispec,*)
  xpos=xppwg.xcrdev(idx,*)
  
   plot,/nodata,/noerase,xst=5,yst=5,[0,1], $
     xrange=xppwg.xcr(idx,*),yrange=xppwg.ycr(ispec,*), $
     position=[xpos(0),ypos(0),xpos(1),ypos(1)],/device
  
  
  return,idx
end

pro xfits_ppfill,ps=ps,title=title,file=psfile
  common xppwg,xppwg
  common xpswg,xpswg
  common xpprof,xpprof
  common xppfill,xpf
  common xfwg,xfwg
  common xpwg,xpwg
  common fits
  
  if n_elements(xppwg) ne 0 then begin
    widget_control,xppwg.base.id,bad_id=bid
    if bid ne 0 then return
  endif else return
  
  if n_elements(xpf) eq 0 then $
    xpf={title:'' ,psfile:''}
  if n_elements(title) ne 0 then xpf.title=title
  if n_elements(psfile) ne 0 then xpf.psfile=psfile
  
  pold=!p
  widget_control,xppwg.draw.id,get_value=windex
  if !d.name ne 'PS' then wset,windex
  !p.multi=0
  !p.position=0
  !p.charsize=xppwg.charsize.val
  
  if keyword_set(ps) then begin
    psset,/ps,file=xpf.psfile,/oldsettings, $ ;no_x=xpf.psfile ne '', $
      size=[18.,18./xppwg.size(0)*xppwg.size(1)] 
  endif
  
@greeklett.pro  
  userlct
  
  stokes=xppwg.iquvsel
  nprof=max(where(xpprof.nwl ge 2))+1
  
  show=xppwg.profsel(0:nprof-1)
  for ip=0,nprof-1 do if show(ip) eq 1 then begin
    if long(total(abs(minmax(xpprof(ip).wl)*1e3))) eq 0 then show(ip)=0
  endif
                                ;show parameters
  xychsz=(convert_coord([!d.x_ch_size,!d.y_ch_size],/device,/to_normal))(0:1)
  if !p.charsize ne 0 then xychsz=xychsz*!p.charsize
  if xppwg.showflags(2) eq 1 then begin
    xyinfo=xpwg.xy
    ixy=[xyinfo.x-xyinfo.xoff,xyinfo.y-xyinfo.yoff]
    info='x='+n2s(xyinfo.x)+',y='+n2s(xyinfo.y)
    noshow=['LTTOP','LTINC','LGTRF']
    for i=0,fits.npar-1 do begin
      if max(strpos(noshow,fits.par[i]) eq 0) eq 0 then begin
        cadd='C'+n2s(fits.comp[i])
        if fits.code eq 'spinor' then $
          ladd=',LT'+string(fits.logtau[i],format='(f5.2)') $
        else ladd=''
        info=[info,string(fits.par[i],cadd+ladd,fits.data[ixy[0],ixy[1],i], $
                          format='(a8,'' ('',a10,'') = '',f10.3)')]
      endif
    endfor
    mlen=max(strlen(info),imax)+5
    ncol=(1./(xychsz[0]*mlen))>1
    infoline=info[0]
    ninfo=n_elements(info)
    for i=1,ninfo-1,ncol do $
      infoline=[infoline,add_comma(info[i:(i+ncol-1)<(ninfo-1)],sep=' | ')]
    ninfo=n_elements(infoline)
  endif else ninfo=0
    
  nshow=total(show)
  spar=['I','Q','U','V']
  
  xreg=[0.02+xychsz(0)*8,0.98]
  yreg=[0.02+xychsz(1)*(4+ninfo*.9),1.-xychsz(1)*2]
  yreg[0]=yreg[0]<(yreg[1]-.1)
  xy0=[xreg[0],yreg[0]]
  ysep=0.005
  nstokes=total(stokes)>1
  ypos=fltarr(2,nstokes)
  dy=(yreg(1)-yreg(0)-ysep*(nstokes-1))/nstokes
  ypos(0,*)=reverse(findgen(nstokes)*dy + yreg(0) +findgen(nstokes)*ysep)
  ypos(1,*)=ypos(0,*)+dy
  ypos=ypos*!d.y_size           ;to device coords
  
  wlrg=[+1e10,-1e10]
  yrg=fltarr(4,2)
  yrg(*,0)=1e10 & yrg(*,1)=-1e10
  
  maxspec=max(xpprof.nspec)
  regidx=lonarr(nprof,maxspec)
  regwl=dblarr(maxspec,2)
  ir=0
  for ip=0,nprof-1 do begin
    for is=0,xpprof(ip).nspec-1 do begin
      if xppwg.spectrumsel(is) eq 1 then begin
        if is eq 0 then i0=0 $
        else i0=total(xpprof(ip).ispec(0:is-1))
        i1=total(xpprof(ip).ispec(0:is))-1
        twl=minmax(xpprof(ip).wl(i0:i1))
        twlc=mean(twl)
        if abs(twlc) ge 1e-5 then begin
                                ;check if this WL range is already present
        ireg=(where( (twl(0) ge regwl(*,0)  and twl(0) le regwl(*,1)) or $
                     (twl(1) ge regwl(*,0)  and twl(1) le regwl(*,1)) or $
                     (twlc ge regwl(*,0)  and twlc le regwl(*,1))))(0)
        if ireg(0) ne -1 then begin
          regwl(ireg,0)=regwl(ireg,0)<min(twl)
          regwl(ireg,1)=regwl(ireg,1)>max(twl)
          regidx(ip,is)=ireg
        endif else if ir lt maxspec then begin
          regwl(ir,*)=twl
          regidx(ip,is)=ir
          ir=ir+1
        endif
        endif
      endif
    endfor
  endfor
  maxspec=ir
  
  ip=0
  is=0
  repeat begin
    if show(ip) eq 1 then begin
      for ii=0,xpprof(ip).nspec-1 do begin
        if ii eq 0 then i0=0 $
        else i0=total(xpprof(ip).ispec(0:ii-1))
        i1=total(xpprof(ip).ispec(0:ii))-1
        iwlrg=minmax(xpprof(ip).wl(i0:i1))
        if xppwg.spectrumsel(ii) eq 1 then begin
          wlrg=[wlrg(0)<iwlrg(0),wlrg(1)>iwlrg(1)]
          for is=0,3 do begin
            it=(where(tag_names(xpprof(0)) eq spar(is)))(0)
            yrg(is,*)=[yrg(is,0)<min((xpprof(ip).(it))(i0:i1)), $
                       yrg(is,1)>max((xpprof(ip).(it))(i0:i1))]
          endfor        
        endif
      endfor
    endif
    ip=ip+1
  endrep until ip ge nprof
  for is=0,3 do begin
    if spar(is) ne 'I' then  yrg(is,*)=[-1,1]*max(abs(yrg(is,*)))
    yrg(is,*)=yrg(is,*)+0.05*[-1,1]*(yrg(is,1)-yrg(is,0)) ;add +-5%
    if spar(is) eq 'I' then yrg(is,*)=yrg(is,*)>0
    if total(abs(xppwg.iquvrgval(is,*))) ne 0 then $
      yrg(is,*)=xppwg.iquvrgval(is,*)
  endfor
  
  if total(abs(xppwg.iquvrgval(4,*))) ne 0 then wlrg=xppwg.iquvrgval(4,*)
  
  iy=0
  ttitle=xpf.title
  if xppwg.title.str ne '' then ttitle=xppwg.title.str
  xppwg.xcrdev=0
  
                                ;show full spectral range
  xreg=xreg*!d.x_size           ; to device coords
  if xppwg.specdisplay.val eq 1 or maxspec eq 0 then begin
    xppwg.xcrdev(0,*)=xreg
    xppwg.nxcr=1
    regwl(0,*)=wlrg
  endif else begin              ;plot individual spectral regions
    xppwg.nxcr=maxspec
    xsep=0.0*(xreg(1)-xreg(0))
    xfrac=(regwl(*,1)-regwl(*,0))/total(regwl(*,1)-regwl(*,0))* $
      (xreg(1)-xreg(0))-xsep;*(maxspec-1)
    xfract=[0,xfrac]
    for i=0,maxspec-1 do xfract(i+1)=total(xfrac(0:i))
    xppwg.xcrdev(0:maxspec-1,0)=xfract + xreg(0) +findgen(maxspec)*xsep
    xppwg.xcrdev(0:maxspec-1,1)=0    
    xppwg.xcrdev(0:maxspec-1,1)=xppwg.xcrdev(0:maxspec-1,0)+xfrac
  endelse
  nsp1=xppwg.nxcr-1
  xppwg.ycr=yrg
  
  ifirst=1
  for is=0,3 do if stokes(is) eq 1 then begin
    it=(where(tag_names(xpprof(0)) eq spar(is)))(0)
    
    xppwg.ycrdev(is,*)=ypos(*,iy)
    for ii=0,nsp1 do begin
      if ii eq maxspec/2 and iy eq 0 then tit=ttitle else tit=''
      if total(abs(xppwg.wlrgval)) le 1e-5 then pwlrg=regwl(ii,*) $
      else pwlrg=xppwg.wlrgval
      plot,/nodata,pwlrg,xppwg.ycr(is,*),xst=1,yst=5, $
        position=[xppwg.xcrdev(ii,0),xppwg.ycrdev(is,0), $
                  xppwg.xcrdev(ii,1),xppwg.ycrdev(is,1)],/device, $
        xtickname=strarr(32)+' ',noerase=ifirst eq 0,title=tit
      xppwg.xcr(ii,*)=!x.crange
      if iy eq nstokes-1 then begin
        if ii eq maxspec/2 then xtit='Wavelength ['+f_angstrom+']' else xtit=''
        xtix=strarr(32)+''
        dpx= (xppwg.xcrdev(ii,1)-xppwg.xcrdev(ii,0))/!d.x_size
        if dpx le 0.2 then nn=24 $
        else if dpx le 0.5 then nn=16 $
        else nn=0
        if nn ge 1 then begin
          delidx=fix(findgen(nn)/nn*32)
          xtix(delidx)=' '
        endif
        axis,xax=0,0,!y.crange(0),/xst,xtitle=xtit,xtickname=xtix
      endif
      if ii eq 0 then axis,yax=0,!x.crange(0),0,/yst,/xst
      if ii eq nsp1 then $
        axis,yax=1,!x.crange(1),0,/yst,/xst,ytickname=strarr(32)+' '
      ifirst=0
    endfor    
                                ;show image wavelength
    if xppwg.showflags(0) eq 1 then begin
      if n_elements(xpswg) eq 0 then ixy=[0,0] else ixy=xpswg.ixy
      xppwg.imgwl=xfwg.p(ixy(0),ixy(1)).wlrg
      if total(abs(xppwg.imgwl)) eq 0 then $
        xppwg.imgwl=[min(xppwg.xcr(0:nsp1,0)),max(xppwg.xcr(0:nsp1,1))]
      
      for iw=0,1 do begin
        isp=set_specplot(xppwg.imgwl(iw),is)
        dx=(xppwg.xcr(isp,1)-xppwg.xcr(isp,0))*.02
        xbox=xppwg.imgwl(iw)+(iw*2-1)*[dx,0,0,dx]
        ybox=!y.crange([0,0,1,1])
        plots,color=3,xbox,ybox
        polyfill,xbox,ybox,/line_fill,orientation=45,color=3
      endfor
    endif
    
    for ii=0,nsp1 do begin
      if spar(is) eq 'I' then oplot,linestyle=1,xppwg.xcr(ii,*),[1,1] $
      else oplot,linestyle=1,xppwg.xcr(ii,*),[0,0]
    endfor
      
    for ip=0,nprof-1 do if show(ip) eq 1 then begin
      for ii=0,xpprof(ip).nspec-1 do if xppwg.spectrumsel(ii) eq 1 then begin
                                ;show reference wavelengths
        if ii eq 0 then i0=0 $
        else i0=total(xpprof(ip).ispec(0:ii-1))
        i1=total(xpprof(ip).ispec(0:ii))-1
        
        isp=set_specplot(median(xpprof(ip).wl(i0:i1)),is)
        if xppwg.showflags(1) eq 1 then begin
          oplot,xpprof(ip).wlref(ii)+[0,0],!y.crange, $
            linestyle=2,color=xpprof(ip).plot.color
        endif
        
        ;; ladd=20./3e5*xpprof(ip).wlref(ii)
        ;; oplot,color=3,6302.65d +ladd+[0,0],!y.crange, $
        ;;     linestyle=3
        
        oplot,color=xpprof(ip).plot.color,psym=xpprof(ip).plot.psym, $
          linestyle=xpprof(ip).plot.linestyle,thick=xpprof(ip).plot.thick,$
          xpprof(ip).wl(i0:i1),(xpprof(ip).(it))(i0:i1)
      endif
                                ;display noise level
      if xppwg.showflags(3) eq 1 then begin
        rms=string(determ_rms((xpprof(ip).(it))(i0:i1),status=status),format='(e10.2)')
        if status eq  0 then $
          xyouts,!x.crange[1],!y.crange[1],/data,alignment=1,color=xpprof(ip).plot.color, $
                 strjoin(replicate('!C',ip+2))+rms+'  '
      endif
    endif
    xyouts,0.,mean(ypos(*,iy)),'!C'+spar(is), $
      alignment=0.5,orientation=90,/device
    iy=iy+1
  endif
  if xppwg.showflags(0) eq 1 then $
    xyouts,0,1,/normal,'!CWL for map x='+n2s(ixy(0))+',y='+n2s(ixy(1)), $
    charsize=0.8*!p.charsize,color=3
  
  if ninfo ge 1 then begin
    fnt=!p.font
    if !d.name eq 'PS' then begin
      !p.font=0
      device,/courier,/tt
    endif else begin
      !p.font=-1
    endelse
    x0=0.02 & y0=xy0[1]
    if !p.charsize eq 0 then chsz=0.9 else chsz=0.9*!p.charsize
    xyouts,x0,y0,/normal,'!C!C!C!C'+add_comma(infoline,sep='!C'), $
           charsize=chsz
    if !d.name eq 'PS' then device,set_font='Helvetica'
    !p.font=fnt
  endif
  
                                ;legend
  for ip=0,nprof-1 do if show(ip) eq 1 then begin
    ypos=0.
    xpos=0.01
    ypos=(total(show(0:ip))-0.8)*xychsz(1)*1.1
    xlen=0.07
    plots,/normal,xpos+[0.,xlen*.8],ypos+xychsz(1)*0.3+[0,0], $
      color=xpprof(ip).plot.color,psym=xpprof(ip).plot.psym, $
      linestyle=xpprof(ip).plot.linestyle,thick=xpprof(ip).plot.thick
    xyouts,/normal,alignment=0,xpos+xlen,ypos,xpprof(ip).plot.label
  endif
  
  if !d.name eq 'PS' then psset,/close
  !p=pold
  widget_control,xpwg.draw.id,get_value=windex
  wset,windex
end

pro xpp_profset
  common xppwg,xppwg
  common xpprof,xpprof
  
  ip=xppwg.profnr.val
  widget_control,xppwg.psym.id,set_value=xpprof(ip).plot.psym
  widget_control,xppwg.linestyle.id,set_value=xpprof(ip).plot.linestyle
  widget_control,xppwg.thick.id,set_value=xpprof(ip).plot.thick
  widget_control,xppwg.color.id,set_value=xpprof(ip).plot.color
  for i=0,1 do widget_control,xppwg.range(i).id, $
    set_value=xppwg.iquvrgval(xppwg.iquvrg.val,i)
  for i=0,1 do widget_control,xppwg.wlrg(i).id, $
    set_value=xppwg.wlrgval(i)
end

pro xppwg_event,event
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xppwg,xppwg
  common xpswg,xpswg
  common xpprof,xpprof
  
  widget_control,event.id,get_uvalue=uval
  ppupdate=0
  ps=0
  case uval of 
    'base': begin
      xppwg.size=[event.x,event.y]-xppwg.frame
      xppwg.size=xppwg.size>200
      screen=get_screen_size()
      widget_control,xppwg.draw.id,xsize=xppwg.size(0),ysize=xppwg.size(1)
      ppupdate=1
    end
    'control': begin
      case event.value of
        'print': begin
          ps=1 
          ppupdate=1
        end
        'save':
        'refresh': ppupdate=1
        'writesynth': begin
          write_synthprof
        end
        'exit': begin
          widget_control,xppwg.base.id,/destroy
          widget_control,xpwg.draw.id,get_value=windex
          wset,windex
        end
      endcase
    end
    'showflags': xppwg.showflags(event.value)=event.select
    'profsel': xppwg.profsel(event.value)=event.select
    'iquvsel': xppwg.iquvsel(event.value)=event.select
    'spectrumsel': xppwg.spectrumsel(event.value)=event.select
    'specdisplay': xppwg.specdisplay.val=event.value
    'title': xppwg.title.str=event.value
    'charsize': xppwg.charsize.val=event.value
    'psym': xpprof(xppwg.profnr.val).plot.psym=event.value
    'linestyle': xpprof(xppwg.profnr.val).plot.linestyle=event.value
    'color': xpprof(xppwg.profnr.val).plot.color=event.value
    'thick': xpprof(xppwg.profnr.val).plot.thick=event.value
    'range0': xppwg.iquvrgval(xppwg.iquvrg.val,0)=event.value
    'range1': xppwg.iquvrgval(xppwg.iquvrg.val,1)=event.value
    'autorange': begin
      xppwg.iquvrgval(xppwg.iquvrg.val,*)=0
      xpp_profset
    end
    'iquvrg': begin
      xppwg.iquvrg.val=event.index
      xpp_profset
    end
    'wlrange0': xppwg.wlrgval(0)=event.value
    'wlrange1': xppwg.wlrgval(1)=event.value
    'wlautorange': begin
      xppwg.wlrgval(*)=0
      xpp_profset
    end
    'wlrg': begin
      xppwg.wlrg.val=event.index
      xpp_profset
    end
    'profnr': begin
      xppwg.profnr.val=event.index
      xpp_profset
    end
    'draw': begin
      if max(tag_names(event) eq 'X') eq 1 then begin
        isp=set_specplot(event.x,0,/device)
        xdat=(event.x-xppwg.xcrdev(isp,0))/ $
          (xppwg.xcrdev(isp,1)-xppwg.xcrdev(isp,0))* $
          (xppwg.xcr(isp,1)-xppwg.xcr(isp,0))+xppwg.xcr(isp,0)
;        xdat=(convert_coord([double(event.x),0],/device,/to_data))(0)
        if xppwg.showflags(0) eq 1 then begin
          common cwlimsel,hilo,presscount,xpressed
          shiftpressed=event.modifiers eq 1
          if event.press eq 1 then begin ;select left or right region
            xpressed=xdat
            dummy=min(abs(xppwg.imgwl-xdat),hilo)
            presscount=1
          endif
          if event.release eq 1 then begin ;select left or right region
            if n_elements(presscount) eq 1 then if presscount eq 1 then begin
              if shiftpressed then $
                xppwg.imgwl=xppwg.imgwl+(xdat-xpressed) $
              else xppwg.imgwl(hilo)=xdat
              xppwg.imgwl= $
                (xppwg.imgwl>min(xppwg.xcr(0:xppwg.nxcr-1,*)))< $
                max(xppwg.xcr(0:xppwg.nxcr-1,*))
              xppwg.imgwl(0)=xppwg.imgwl(0)<(xppwg.imgwl(1)-1e-6)
              xppwg.imgwl(1)=xppwg.imgwl(1)>(xppwg.imgwl(0)+1e-6)
              if n_elements(xpswg) eq 0 then ixy=[0,0] $
              else ixy=xpswg.ixy
              xfwg.p(ixy(0),ixy(1)).wlrg=xppwg.imgwl
              xfits_plotsetting,ixy
              for i=0,1 do widget_control,xpswg.wlrg(i).id, $
                set_value=xfwg.p(xpswg.ixy(0),xpswg.ixy(1)).wlrg(i)
              ppupdate=1
              presscount=presscount+1
            endif
          endif
        endif
      endif
    end
    else: begin
;      help,/st,event
    end
  endcase
  
  if ppupdate then xfits_ppfill,ps=ps
end

pro xfits_ppcreate
  common xpwg,xpwg
  common xppwg,xppwg
  common fits,fits
  common xpprof,xpprof
  
  subst={id:0l,val:0.,str:''}
  geom=widget_info(/geometry,xpwg.base.id)
  xoff=geom.xoffset+geom.xsize
  screen=get_screen_size()
  
  oxppwg={base:subst,control:subst,draw:subst,tab:subst,prof:subst, $
          size:[0,0],frame:[0,0],profsel:bytarr(20), $
          profnr:subst,psym:subst,linestyle:subst,thick:subst,color:subst, $
          iquv:subst,iquvsel:bytarr(4),charsize:subst,title:subst, $
          range:replicate(subst,2),autorange:subst,iquvrg:subst, $
          iquvrgval:fltarr(5,2),spectrum:subst,spectrumsel:bytarr(100), $
          flags:subst,showflags:bytarr(4),imgwl:dblarr(2), $
          xcr:fltarr(100,2),xcrdev:fltarr(100,2), $
          ycrdev:fltarr(4,2),ycr:fltarr(4,2),nxcr:1,wlautorange:subst, $
          specdisplay:subst,wlrg:replicate(subst,2),wlrgval:dblarr(2)}
  if n_elements(xppwg) eq 0 then begin
;    oxppwg.size(0)=((screen(0)*0.25)>(screen(0)-xoff)<(screen(0)*0.35))
;    oxppwg.size(1)=oxppwg.size(0)*sqrt(2.)
    oxppwg.size(1)=screen[1]*0.5
    oxppwg.size(0)=oxppwg.size(1)/sqrt(2.)
    oxppwg.profsel(0:1)=1
    oxppwg.iquvsel=1
    oxppwg.spectrumsel=1
    oxppwg.specdisplay.val=0.
    oxppwg.showflags[3]=1b
    xppwg=oxppwg
  endif else xppwg=update_struct(xppwg,oxppwg)
  
  xppwg.base.id=widget_base(title='Profiles - '+fits.code, $
                            col=1,/tlb_size_events, $
                            uval='base',group_leader=xpwg.base.id, $
                            xoffset=xoff<(screen(0)-xppwg.size(0)),space=0)
  control=widget_base(xppwg.base.id,row=1,/frame,space=0)
  tab=widget_base(xppwg.base.id,row=1,/frame,space=0);,ysize=100)
  draw=widget_base(xppwg.base.id,row=1,/frame,space=0)
  
  xppwg.control.id=cw_bgroup(control,uvalue='control',row=1,space=0, $
                             [' Refresh ',' Print ',' Save ','Write Synth.Prof',' Close '], $
                             button_uvalue=['refresh','print','save','writesynth','exit'])
  xppwg.tab.id=widget_tab(tab,uvalue='tab',location=0)
  prof=widget_base(xppwg.tab.id,title='Select Profiles',col=1,space=0)
  spectra=widget_base(xppwg.tab.id,title='Select Spectra',col=1,space=0)
;  stokes=widget_base(xppwg.tab.id,title='IQUV',space=0)
  layout=widget_base(xppwg.tab.id,title='Layout',col=1,space=0)
  range=widget_base(xppwg.tab.id,title='Range',col=1,space=0)
  flags=widget_base(xppwg.tab.id,title='Flags & More',space=0,col=1)
  
  nprof=max(where(xpprof.nwl ge 2))+1
  profname='Prof '+n2s(indgen(nprof)+1)
  for ip=0,nprof-1 do if xpprof(ip).plot.label ne '' then $
    profname(ip)=xpprof(ip).plot.label
  xppwg.prof.id=cw_bgroup(prof,/nonexclusive, $
                         profname, $
                         set_value=xppwg.profsel(0:nprof-1), $
                         uval='profsel',col=6,space=0)
  xppwg.iquv.id=cw_bgroup(prof,/nonexclusive, $
                         'Stokes '+['I','Q','U','V'], $
                          set_value=xppwg.iquvsel,uval='iquvsel',row=1,space=0)
  sub=widget_base(spectra,col=1,space=0)
  dummy=widget_label(sub,value='Spectrum:',/align_left)
  xppwg.spectrum.id=cw_bgroup(sub,/nonexclusive,col=10,space=0, $
                              uval='spectrumsel',$
                              n2s(indgen(xpprof(0).nspec)+1), $
                              set_value=xppwg.spectrumsel(0:xpprof(0).nspec-1))
  xppwg.specdisplay.id=cw_bgroup(sub,/exclusive,row=1,space=0, $
                                 uval='specdisplay', $
                                 ['Spectral Regions','Full Spectrum'], $
                                 set_value=xppwg.specdisplay.val)
  
  sub=widget_base(layout,row=1,space=0)
  xppwg.charsize.id=cw_field(sub,uvalue='charsize',title='Charsize:',/float,$
                             /all_ev,xsize=5)
  widget_control,xppwg.charsize.id,set_value=xppwg.charsize.val
  xppwg.title.id=cw_field(sub,uvalue='title',title='Title:',/string,$
                          /all_ev,xsize=30)
  widget_control,xppwg.title.id,set_value=xppwg.title.str
  
  sub=widget_base(layout,row=1,space=0)
  xppwg.profnr.id=widget_combobox(sub,uvalue='profnr',value=profname)
  widget_control,xppwg.profnr.id,set_combobox_select=xppwg.profnr.val
  xppwg.psym.id=cw_field(sub,uvalue='psym',title='psym',/int,/all,xsize=2)
  xppwg.linestyle.id=cw_field(sub,uvalue='linestyle',title='style',/int, $
                              /all,xsize=2)
  xppwg.thick.id=cw_field(sub,uvalue='thick',title='thick',/int, $
                          /all,xsize=2)
  xppwg.color.id=cw_field(sub,uvalue='color',title='color',/int,/all,xsize=2)
  
  sub=widget_base(range,row=1)
  xppwg.iquvrg.id=widget_combobox(sub,uvalue='iquvrg', $
                                  value=['Stokes '+['I','Q','U','V'], $
                                         'Wavelength'])
  widget_control,xppwg.iquvrg.id,set_combobox_select=xppwg.iquvrg.val
  lab=widget_label(sub,value='  Range:')
  for i=0,1 do $
    xppwg.range(i).id=cw_field(sub,xsize=8,/all_events,/floating,title='', $
                               uvalue='range'+n2s(i), $
                               value=xppwg.iquvrgval(xppwg.iquvrg.val,i))
  xppwg.autorange.id=widget_button(sub,value='auto-range',uvalue='autorange')
  
  sub=widget_base(range,row=1)
  lab=widget_label(sub,value='  WLRange:')
  for i=0,1 do $
    xppwg.wlrg(i).id=cw_field(sub,xsize=8,/all_events,/floating,title='', $
                              uvalue='wlrange'+n2s(i),value=xppwg.wlrgval(i))
  xppwg.wlautorange.id=widget_button(sub,value='WL auto-range', $
                                     uvalue='wlautorange')
  
  xppwg.draw.id=widget_draw(draw,uvalue='draw', $
                            xsize=xppwg.size(0),ysize=xppwg.size(1), $
                            /motion_events,/track,/view,/button_events, $
                            /wheel_events)
  
  xppwg.flags.id=cw_bgroup(flags,/nonexclusive,col=4,space=0, $
                           ['Image WL','WLREF','ATM-Par','noise level'], $
                           set_value=xppwg.showflags,uval='showflags')
  lab=widget_label(flags,value='How to change the Image-Wavelength:', $
                   /align_left)
  lab=widget_label(flags,value='  - select Stokes image (right-click on maps)',$
                  /align_left)
  lab=widget_label(flags,value='  - drag blue markers',/align_left)
  lab=widget_label(flags,value='  - use ''shift''-drag to move the region',/align_left)
  
  xpp_profset
  widget_control,xppwg.base.id,/realize
  xmanager,'xppwg',xppwg.base.id,no_block=1
 
  bst=widget_info(xppwg.base.id,/geometry)
  xppwg.frame=[bst.xsize-xppwg.size(0),bst.ysize-xppwg.size(1)]
  
end
  
pro xfits_plotprof,prof0,prof1,prof2,prof3,prof4,prof5,prof6,prof7,prof8,prof9,$
      prof10,prof11,prof12,prof13,prof14,prof15,prof16,prof17,prof18,prof19,$
                   title=title,file=psfile,ps=ps,label=label
  common xpwg,xpwg
  common xppwg,xppwg
  common xpswg,xpswg
  common xpprof,xpprof
  
  nmax=0l
  icflag=0
  nprof=0
  ende=0
  isprof=bytarr(20)
  for nprof=0,19 do begin
;  repeat begin
    dummy=execute('n=n_tags(prof'+n2s(nprof)+')')
    if n ge 5 then begin
      dummy=execute('prof=prof'+n2s(nprof))
      nmax=n_elements(prof.wl)>nmax
      isprof[nprof]=1b
    endif
;  endrep until n lt 5
  endfor
  if total(isprof) eq 0 then begin
    message,/cont,/trace,'No profiles found.'
    return
  endif
  nprof=total(isprof)
  wisprof=where(isprof)
  if n_elements(xyinfo) eq 0 then xyinfo=0
  plt={psym:0,color:0,thick:1,linestyle:0,label:'',symsize:2}
  st={nwl:0l,wl:dblarr(nmax),ic:0.,plot:plt, $
      i:fltarr(nmax),q:fltarr(nmax),u:fltarr(nmax),v:fltarr(nmax),icflag:0, $
      nspec:1,ispec:lonarr(100),wlref:dblarr(100)}
  oxpprof=replicate(st,20)
  oxpprof.plot.color=indgen(20)
  if n_elements(xpprof) ne 0 then begin
    plotold=update_struct(xpprof.plot,oxpprof.plot) 
    xpprof=update_struct(xpprof,oxpprof)
  endif else xpprof=temporary(oxpprof)
  if nprof lt n_elements(xpprof)-1 then xpprof(nprof:*).nwl=0
  for i=0,nprof-1 do begin
    dummy=execute('prof=prof'+n2s(wisprof[i]))
    xpprof(i).nwl=n_elements(prof.wl)
    xpprof(i).wl(0:xpprof(i).nwl-1)=prof.wl
    xpprof(i).i(0:xpprof(i).nwl-1)=prof.i
    xpprof(i).q(0:xpprof(i).nwl-1)=prof.q
;    xpprof(i).q(0:xpprof(i).nwl-1)=prof.q-0.08*(max(prof.i)-prof.i)
;print,max(0.02*(max(prof.i)-prof.i))
    xpprof(i).u(0:xpprof(i).nwl-1)=prof.u
    xpprof(i).v(0:xpprof(i).nwl-1)=prof.v
    if max(tag_names(prof) eq 'IC') eq 1 then begin
      xpprof(i).ic=prof.ic
      xpprof(i).icflag=1
    endif
    if max(tag_names(prof) eq 'NSPEC') eq 1 then begin
      xpprof(i).nspec=prof.nspec
      xpprof(i).ispec(0:xpprof(i).nspec-1)=prof.ispec(0:xpprof(i).nspec-1)
    endif else begin
      xpprof(i).nspec=1
      xpprof(i).ispec=xpprof(i).nwl
    endelse
    if max(tag_names(prof) eq 'WLREF') eq 1 then $
      xpprof(i).wlref(0:xpprof(i).nspec-1)=prof.wlref(0:xpprof(i).nspec-1)
    if n_elements(plotold) ne 0 then xpprof(i).plot=plotold(i) $
    else begin                  ;some default settings
      if xpprof(i).nwl lt 10 then xpprof(i).plot.psym=-1
      xpprof(0).plot.color=9
    endelse
    if n_elements(label) ge i+1 then xpprof(i).plot.label=label(i)
  endfor
  
  create=0
  if n_elements(xppwg) ne 0 then begin
    widget_control,xppwg.base.id,bad_id=bad_id
    if bad_id eq 0 then begin
      managed=widget_info(xppwg.base.id,/managed)
      if managed eq 0 then create=1
    endif else create=1
  endif else create=1
  
  if create then xfits_ppcreate
  
  xfits_ppfill,title=title,file=psfile,ps=ps
  widget_control,xpwg.draw.id,get_value=windex
  wset,windex
  
end
