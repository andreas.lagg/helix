;+
; NAME:
;    label_box
;
;
; PURPOSE:
;    creates box with labels for line plot
;
;
; CATEGORY:
;    displaying data (msk_plot)
;
;
; CALLING SEQUENCE:
;    label_box,position=position,linestyle=linestyle,psym=psym,color=color, $
;              charsize=charsize,thick=thick,name=name,frame=frame, $
;              symsize=symsize,bottom=bottom,linespace=linespace,
;              nomargin=nomargin
; 
; INPUTS:
;    self-explaining parameters 
;
;
; OPTIONAL INPUTS:
;
;
;	
; KEYWORD PARAMETERS:
;
;
;
; OUTPUTS:
;
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;    A. Lagg, 12.9.1997
;-

;puts labels to line plots in box, position in device coordiantes
pro label_box,position=position,linestyle=linestyle,psym=psym,color=color, $
              charsize=charsize,thick=thick,name=name,frame=frame, $
              symsize=symsize,bottom=bottom,linespace=linespace,blank=blank, $
              new=new,ps_bw=ps_bw,only_text=only_text,nomargin=nomargin, $
              text_color=text_color
  common label_box,old_pos
  common label_par,curr_y,cx,cy,dx,dy,bord_x,bord_y,symb_x,text_x,tot_x, $
    max_x,ratio,frame_y
  common verbose,verbose
  
  if keyword_set(columns) eq 0 then columns=1
  if columns lt 1 then columns=1
  bg_col=!p.background
  
  if keyword_set(new) then new=1 else new=0
  if n_elements(old_pos) eq 0 then new=1
  if new eq 0 then if min(old_pos eq position) eq 0 then new=1
  old_pos=position
  nel=n_elements(name)
  if n_elements(color) eq 0 then color=bytarr(nel)+!p.color
  if n_elements(color) ne nel then $
    color=(bytarr(nel)+1)#reform(color(*))
  if n_elements(text_color) eq 0 then text_color=bytarr(nel)+!p.color
  if n_elements(text_color) ne nel then $
    text_color=(bytarr(nel)+1)#reform(text_color(*))
  if n_elements(linestyle) ne nel and n_elements(linestyle) gt 0 then $
    linestyle=bytarr(nel)
  if n_elements(thick) ne nel then thick=bytarr(nel)+1
  if n_elements(psym) ne nel then psym=bytarr(nel)
  if n_elements(charsize) eq 0 then charsize=!p.charsize
  if n_elements(symsize) ne nel then symsize=bytarr(nel)+0.8*charsize
  if n_elements(linespace) eq 0 then linespace=1.2
  if linespace eq 0 then linespace=1.2
  
  old_charsize=!p.charsize
  old_ch=charsize
  
  if keyword_set(bottom) then begin
    pys=position(1)
    dir=-1 
  endif else begin
    pys=position(3)
    dir=1
  endelse
  
  if new eq 1 then begin
    max_x=0.
    if n_elements(frame_y) ne 0 then dummy=temporary(frame_y)
    dx=abs(position(2)-position(0))
    dy=abs(position(3)-position(1))
    reduced=0
    repeat begin                ;loop for reducing character size if necessary
      !p.charsize=charsize
      cx=!d.x_ch_size*charsize
      cy=!d.y_ch_size*charsize*0.86
      
      if keyword_set(nomargin) then begin
        bord_x=0.
      endif else begin
        bord_x=cx
      endelse
      bord_y=cy
      if keyword_set(only_text) then ratio=1. else ratio=0.75
      symb_x=max([10.*cx,(dx-5*bord_x)]*(.99-ratio))
      if keyword_set(only_text) then begin
        text_x=-1
        for in=0,n_elements(name)-1 do $
          text_x=max([text_x,get_textwidth(name(in),charsize)])
      endif else text_x=max([10.*cx,(dx-5*bord_x)]*ratio)

      tot_x=5*bord_x+symb_x+text_x
      reduce=0
      if tot_x gt dx then begin
        reduce=1 & reduced=1
        charsize=charsize-0.05
        if charsize lt 0.8*old_charsize then reduce=0
      endif
    endrep until reduce eq 0
    if reduced then if verbose eq 1 then $
      message,/cont,'label box too small - reducing character size'
    ratio=float(charsize)/old_ch
  endif else charsize=charsize*ratio
  !p.charsize=charsize
  text_x=(tot_x<dx)-(5*bord_x+symb_x)
  
  if new eq 1 then curr_y=pys-bord_y*1.15*dir
  
  
  for i=0,nel-1 do begin
    
    nm=name(i)
    lb_chars=byte([' ',':','/','-','+',')',','])
    lb_pos=[-1]
    for j=0,n_elements(lb_chars)-1 do $
      lb_pos=[lb_pos,where(byte(nm) eq lb_chars(j))]
    lb_pos=remove_multi([0,lb_pos,strlen(nm)])
    lb_pos=(lb_pos(sort(lb_pos)))(1:*)
    nel_lb=n_elements(lb_pos)
    
    wx=get_textwidth(nm,charsize)
    if wx le text_x then text_ok=1 else begin ;insert linebreaks
      line='' & newline=0 & pn=0 & lstart=0
      ns='' & add_c=''
      repeat begin
        add_c=add_c+strmid(nm,pn,1)
        if add_c eq '!' then pn=pn+0 else begin
          line=line+add_c
          add_c=''
          lx=get_textwidth(line+'!X',charsize)
          if lx gt text_x then begin
            pos=max(lb_pos(where(lb_pos le pn)))
            if pos le lstart then pos=pn
            if pos+1 ge strlen(nm) then add='' else add='!C'
            ns=ns+strmid(line,0,pos-lstart+1)+add
            pn=pos
            lstart=pos+1 & line=''
          endif
        endelse
        pn=pn+1
      endrep until pn ge strlen(nm)
      if lstart lt strlen(nm) then ns=ns+strmid(nm,lstart,strlen(nm))
      nm=ns
    endelse
    
    lines=count_substring(nm,'!C')+1

;   exc=[where(byte(nm+'!C') eq 33)+1,where(byte(nm+'!C') eq 67)]
;   lines=n_elements(exc)-n_elements(remove_multi(exc))
    if keyword_set(only_text) then add=1*bord_x else add=3*bord_x+symb_x
    max_x=max([max_x,get_textwidth(nm,charsize)+add])
    
    y0=-(cy)*linespace*(lines-.05)
    y1=cy*linespace*.3
    yfill=curr_y-([y0,y0,y1,y1,y0])-((lines-1)*cy*linespace*(dir eq 1))

   if dir eq -1 and n_elements(frame_y) eq 5 then yfill=yfill>max(frame_y)
   if dir eq +1 and n_elements(frame_y) eq 5 then yfill=yfill<min(frame_y)
    if keyword_set(blank) then begin ;clear background
      polyfill,/device,position(0)+bord_x+[0.,max_x,max_x,0.,0.],yfill, $
        color=bg_col
    endif
    if dir eq -1 then begin
      curr_y=curr_y-linespace*cy*(lines-1)*dir
      lines=1
    endif
    
    nelc=n_elements(color(0,*))
    off_y=cy/2.8-0.5*cy*(findgen(nelc)/((nelc-1)>1)-0.5*(nelc gt 1))
    if keyword_set(only_text) eq 0 then begin
      for j=0,nelc-1 do begin
        if n_elements(linestyle) gt 0 then if linestyle(i) ne -1 then $ 
          plots,/device,[position(0)+2*bord_x,position(0)+2*bord_x+symb_x], $
          [curr_y,curr_y]+off_y(j),linestyle=linestyle(i),color=color(i,j), $
          thick=thick(i)
        if psym(i) ne 0 then plots,/device, $
          [position(0)+2*bord_x+position(0)+2*bord_x+symb_x]/2., $
          [curr_y]+off_y(j),color=color(i,j), $
          thick=thick(i),psym=psym(i),symsize=symsize(i)
      endfor
      xtext=position(0)+3*bord_x+symb_x
    endif else xtext=position(0)+1.5*bord_x
    
    xyouts,/device,alignment=0,xtext,curr_y,nm,charsize=charsize, $
      color=text_color(i)
    curr_y=curr_y-cy*(lines-1)*dir-linespace*cy*dir
  endfor
  yo=cy*(1 eq dir) *0.25
  yu=cy*(-1 eq dir)*0.25
;   if dir eq -1 then $
;     yframe=[curr_y+yo,curr_y+yo,min(yfill),min(yfill),curr_y+yo] $
;   else yframe=[min(yfill),min(yfill),pys+yu,pys+yu,min(yfill)]
; ady=cy/3.
ady=0.  
  yframe=[min(yfill)-ady,min(yfill)-ady,max(yfill)+ady,max(yfill)+ady, $
          min(yfill)-ady]  
  if n_elements(frame_y) eq 0 then frame_y=yframe $
  else begin
    frame_y([0,1,4])=min([frame_y,yframe])
    frame_y([2,3])=max([frame_y,yframe])
  endelse

  if keyword_set(frame) then begin
    xw=max_x
;    if keyword_set(blank) then begin
;       if dir eq -1 then $
;       polyfill,/device,position(0)+bord_x+[0.,max_x,max_x,0.,0.], $
;         pys+cy-([cy,cy,0.,0.,cy])*.25/2.3,color=bg_col
;       if dir eq 1 then $
;       polyfill,/device,position(0)+bord_x+[0.,max_x,max_x,0.,0.], $
;         pys-([0.,0.,cy,cy,0.])*.25/2.3,color=bg_col
;    endif
    plots,/device,color=!p.color, $
      [position(0),position(0)+xw,position(0)+xw, $
       position(0),position(0)]+bord_x,frame_y
;      [curr_y+yo,curr_y+yo,pys+yu,pys+yu,curr_y+yo]
  endif
  !p.charsize=old_charsize
  xyouts,0,0,'!3'
end
