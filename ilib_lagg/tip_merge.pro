;read split tip files (*.000-0?) and merge it into one file
pro tip_merge,tiproot
  
  tip=file_search(tiproot+'-??',count=cnt)
  if cnt eq 0 then begin
    print,'No TIP-2 files found: '
    print,'  '+tiproot+'-??'
    print,'Please specify root-filename (without the -01)'
    retall
  endif  
  print,'Root filename: '+tiproot
  print,'# of split files: ',cnt
  
                                ;get size information
  nimg=0l
  idx=0
  for i=0,cnt-1 do begin
    mrd_head,tip[i],hdr
    naxes=sxpar(hdr,'NAXIS*')
    nimg=nimg+naxes[2]
    idx=[idx,idx[i]+naxes[2]]
    if i le cnt-2 then begin
                                ;remove header entries after last AODUMP entry
                                ;if more fits files are coming
      iad=max(where(strpos(hdr,'AODUMP') eq 0))
      hdr=hdr[0:iad+1]
    endif
    if i eq 0 then begin
      header=hdr
    endif else begin
      iut=min(where(strpos(hdr,'UT      =') eq 0))
      if iut[0] ne -1 then hdr=hdr[iut:*]
      header=[header,hdr]
    endelse
  endfor
                                ;create new data array
  print,'Output Dimension: ',naxes[0],naxes[1],nimg
  data=lonarr(naxes[0],naxes[1],nimg)
  
  for i=0,cnt-1 do begin
    print,'Reading '+tip[i]
    tdat=read_fits_dlm(tip[i],0,hdr)
    data[*,*,idx[i]:idx[i+1]-1]=temporary(tdat)
  endfor
  

  fp=get_filepath(tiproot)
                                ;check if output file already exists
  repeat begin  
    tipout=fp.path+fp.name
    fi=file_info(tipout)
    print,'Writing '+fi.name
    fip=file_info(fp.path)
    if fp.path eq '' then fip=file_info('./')
    write=1b & cancel=0b
    if fi.exists eq 1 and fi.write eq 1 then begin
      print,tipout+' already exists. Overwrite [Y]es / [n]o / [c]ancel? '
      key=strupcase(get_kbrd())
      print,key
      if key eq 'N' then write=0b
      if key eq 'C' then cancel=1b
    endif else begin
      if fip.write eq 0 then begin
        print,'No write permission.'
        outdir=''
        read,'Enter new output directory: ',outdir
        if strlen(outdir) ge 1 then outdir=outdir+'/'
        fp.path=outdir
        write=0b
      endif
    endelse
  endrep until write or cancel
  
  if write then begin
    print,'Writing single TIP file:'
    print,tipout
;    write_fits_dlm,data,fi.name,header,/create
    writefits,fi.name,data,header,append=0
    print,'Done.'
  endif else print,'No file written.'
  
end
  
  
