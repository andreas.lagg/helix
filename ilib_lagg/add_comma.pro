;+
; NAME:
;    add_comma
;
;
; PURPOSE:
;    converts array of string to one string seperated with commas
;
;
; CATEGORY:
;    processing EPD-Data
;
;
; CALLING SEQUENCE:
;    string=add_comma(arr_str,seperator=seperator,/no_empty)
;
; 
; INPUTS:
;    arr_str - array of string which should be converted
;
;
; OPTIONAL INPUTS:
;    seperator - set seperator string, default comma ','
;    max_len - maximum length for one line, add !C at seperator after
;              max. length is reached
;    multiple_enclose - 2-el vector containing enclose characters if
;                       arr_str has more than one element
;	
; KEYWORD PARAMETERS:
;    no_empty - only array elements <>'' are treated
;
;
; OUTPUTS:
;    string - string containing array elements seperated with commas
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;    A. Lagg, 25/04/1996
;-

function add_comma,arr_str,seperator=seperator,no_empty=no_empty, $
                   max_len=max_len,multiple_enclose=multiple_enclose

  ok=1
  if n_elements(seperator) eq 0 then seperator=','
  empty=where(arr_str ne '')
  if keyword_set(no_empty) then begin
    if empty(0) ne -1 then a=arr_str(empty) else ok=0
  endif else a=arr_str
  
  if ok then begin
    nel=n_elements(a)
    str=string(a,format='('+string(n_elements(arr_str))+ $
               '(a,:,'''+seperator+'''))')
    if keyword_set(max_len) then begin
      pos=-1
      lf=0
      repeat begin
        pos=strpos(str,seperator,pos+1)
        if pos ne -1 then if pos-lf gt max_len then begin
          str=strmid(str,0,pos)+'!C'+strmid(str,pos+strlen(seperator), $
                                            strlen(str))
          pos=pos+2
          lf=pos
        endif
      endrep until pos eq -1
    endif
    if nel gt 1 then if keyword_set(multiple_enclose) then $
      str=multiple_enclose(0)+str+multiple_enclose(1)
  endif else str=''
  return,str
end










