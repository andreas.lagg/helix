;routine required by mask.pro

function cat_mark,mark_val=mark_val,mark_sym=mark_sym,mark_col=mark_col, $
                  mark_fill=mark_fill,mark_label=mark_label, $
                  mark_region_value=mark_region_value, $
                  mark_region_color=mark_region_color, $
                  mark_region_lines=mark_region_lines, $
                  mark_region_text=mark_region_text,mark_point=mark_point, $
                  mark_region_idx=mark_region_idx,mark_struct=mark_struct
  
  el=['val','sym','col','fill','label','region_value','region_color', $
      'region_text','region_lines','region_idx','point']
  exstr=''
  if n_elements(mark_struct) eq 0 then begin
    for i=0,n_elements(el)-1 do begin
      dummy=execute('nel=n_elements(mark_'+el(i)+')')
      if nel ne 0 then exstr=exstr+','+el(i)+':mark_'+el(i)
    endfor
    exstr=strmid(exstr,1,strlen(exstr))
    dummy=execute('mark_struct={'+exstr+'}')
    return,mark_struct
  endif else begin
    for i=0,n_elements(el)-1 do begin
      if max(strlowcase(tag_names(mark_struct)) eq el(i)) eq 1 then begin
        dummy=execute('mark_'+el(i)+'=mark_struct.'+el(i))
      endif
    endfor
    return,1
  endelse
end


