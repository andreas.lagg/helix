function read_fits_dlm,fitsfile,iext,header,fpix=fpix,lpix=lpix,status=status
  
  status=0
                                ;check if FITS DLM is available:
  if float(!version.release) ge 8 then dlmmode=0b else begin
    dlmmode=0b
    help,/dlm,output=dlmout
    if max(strpos(dlmout,'** FITS')) ne -1 then begin
      dlm_load,'fits'
      help,/dlm,output=dlmout
      ii=(where(strpos(dlmout,'** FITS') ne -1))[0]
      dlmmode=0b
      if ii ne -1 then begin
        if max(strpos(dlmout[ii],'(not loaded)')) eq -1 then begin
          dlmmode=1b
        endif
      endif
    endif
    if dlmmode eq 0 then message,/cont,'IDL FITS DLM not found.' $
    else if strpos(fitsfile,'.gz') eq strlen(fitsfile)-3 then begin
      message,/cont,'IDL FITS DLM cannot deal with .gz files.'
      dlmmode=0b
    endif
  endelse
  
  dlmerror=0b
  if dlmmode then begin         ;use execute:
                                ;otherwise the file cannot be compiled
                                ;if DLM is miressing
    dummy=execute('data=ftsrd(fitsfile,iext,header,' + $
                  'fpix=fpix,lpix=lpix,status=status)')
    naxis=sxpar(header,'NAXIS*')
    if n_elements(fpix) eq 0 then fpix=naxis*0l
    if n_elements(lpix) eq 0 then lpix=naxis-1l
    if status ne 0 then begin
      print
      print,'Error reading FITSFILE in DLM Mode:'
      print,'File: ',fitsfile
      print,'Size of data: ',size(data,/dim)
      print,'fpix=',fpix
      print,'lpix=',lpix
      print,'Trying to read data with IDL routine.'
      dlmerror=1b
    endif
  endif
  if dlmmode eq 0 or dlmerror then begin
;   data4d=ftsrd(fitsfile,0,hdr)
;      mrd_head,fitsfile,hdr,ext=iext
;      enaxis=sxpar(hdr,'NAXIS*')
;      nfl=(n_elements(fpix)<(max(where(enaxis ge 2))+1))>1
;      data=mrdfits(fitsfile,iext,range=[fpix[nfl-1],lpix[nfl-1]],status=status)
     data=readfits(fitsfile,ext=iext,header)
     status=0
     if n_elements(data) eq 1 then if data[0] eq -1 then status=-1
    naxis=sxpar(header,'NAXIS*')
    if n_elements(fpix) eq 0 then fpix=naxis*0l
    if n_elements(lpix) eq 0 then lpix=naxis-1l
    dim=size(data,/dim)
    nfl=(n_elements(fpix)<(max(where(naxis ge 2))+1))>1
    case nfl of
      2:  data=data[fpix[0]:lpix[0],*]
      3:  data=data[fpix[0]:lpix[0],fpix[1]:lpix[1],*]
      4:  data=data[fpix[0]:lpix[0],fpix[1]:lpix[1],fpix[2]:lpix[2],*]
      else:
    endcase
  endif
  return,temporary(data)
end
