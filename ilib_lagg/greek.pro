;+
; NAME:
;    greek
;
;
; PURPOSE:
;    defines greek letters for vector & hardware fonts
;
;
; CATEGORY:
;
;
;
; CALLING SEQUENCE:
;   greek
;
; 
; INPUTS:
;
;
;
; OPTIONAL INPUTS:
;
;
;	
; KEYWORD PARAMETERS:
;
;
;
; OUTPUTS:
;
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;   greek_letters - contains string variables alpha, beta, ...
;                   containing greek letters
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;    A. Lagg, May 16, 1997
;-

pro greek
  common greek_letters,f_alpha,f_beta,f_gamma,f_lambda,f_sigma,f_tau,f_kappa, $
    f_theta,f_phi,f_grad,f_pi,f_chi,f_delta,f_mu,f_plusminus,f_angstrom

  case 1 of
    !p.font eq 0 or !p.font eq 1: begin ;hardware / TT fonts
      f_alpha='!9a!X'
      f_beta='!9b!X'
      f_gamma='!9g!X'
      f_delta='!9D!X'
      f_lambda='!9l!X'
      f_mu='!9m!X'
      f_kappa='!9k!X'
      f_sigma='!9s!X'
      f_tau='!9t!X'
      f_theta='!9q!X'
      f_phi='!9j!X'
      f_chi='!9c!X'
      f_pi='!9p!X'
      f_grad='!Uo!N'
      f_plusminus='!9'+string(177b)+'!X'
      f_angstrom=string(197b)
    end
    else: begin                   ;vector fonts
      f_alpha='!7a!X'
      f_beta='!7b!X'
      f_gamma='!7c!X'
      f_delta='!7D!X'
      f_lambda='!7k!X'
      f_mu='!7l!X'
      f_kappa='!7j!X'
      f_sigma='!7r!X'
      f_tau='!7s!X'
      f_theta='!7h!X'
      f_chi='!7v!X'
      f_pi='!7p!X'
      f_phi='!7u!X'
      f_plusminus='!9+!X'
      f_grad=string(154b)
      f_angstrom=string(197b)
    end
  end
  if !d.name eq 'PS' then device,/isolatin1
end
