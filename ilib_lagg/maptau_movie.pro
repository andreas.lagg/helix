pro calls
  maptau_movie,par='FMAG',zrange=[0,3000]
  maptau_movie,par='TMP'
  maptau_movie,par='VEL'
  maptau_movie,par='GAMMA',zrange=[0,180]
  maptau_movie,par='CHI',zrange=[-90,90]
  
  maptau_movie,par='TMP',taurange=[-5.,+.5],/qs

end

pro maptau_movie,par=par,zrange=zrange,taurange=taurg,full=full,spot=spot,qs=qs
  common data,fitsdata,hdr
  
  fatmos='~/data/Hinode/30nov06/2D_merged/inverted_atmos_maptau.1.fits'
  if n_elements(data) eq 0 then $
    fitsdata=read_fits_dlm(fatmos,0,hdr)         
  
  
  if keyword_set(spot) then begin
    data=fitsdata[*,0:899,*,*]  & add='_spot' ;only spot
  endif else if keyword_set(qs) then begin
    data=fitsdata[298:583,970:1289,*,*] & add='_qs' ; only QS
  endif else begin
    data=fitsdata & add=''      ;everything
  endelse
  
  szd=size(data,/dim)
  out='/home/lagg/work/papers/2014_lightbridge/movies/'
  
  if n_elements(zrange) ne 2 then zrange=0
  if n_elements(taurg) ne 2 then taurg=[-3.5,0.5]
  
  
  tmp='/home/lagg/tmp/maptau_tmp/'
  spawn,'mkdir -p '+tmp+' ; rm -rf '+tmp+'*'
  
  ipar=sxpar(hdr,par)-1
  pi=get_parinfo(par,'spinor')
  if par eq 'VEL' then pi.scaling=1e-5
  xs=0.08
  ys=0.08
  lttop=sxpar(hdr,'LTTOP')
  ltbot=sxpar(hdr,'LTBOT')
  ltinc=sxpar(hdr,'LTINC')
  
  tau=findgen(szd[2])*ltinc+lttop
  intau=where(tau ge taurg[0] and tau le taurg[1])
  nt=n_elements(intau)
  
  reverse=1
  for i=0,nt-1 do begin
    it=intau[i]
    fjpg1=tmp+'img_'+string(     i,format='(i4.4)')+'.ps'
    fjpg2=tmp+'img_'+string(2*nt-i,format='(i4.4)')+'.ps'
    psset,size=[18,16],/jpg,ps=1,/no_x,view=0,file=fjpg1
    @greeklett.pro
    img=data[*,*,it,ipar]*pi.scaling
    if min(zrange) eq max(zrange) then zrg=minmaxp(img,perc=99.9) else zrg=zrange
    xfits_setcoltab,pi,zrg
    image_cont_al,img,/aspect,/cut,contour=0,zrange=zrg, $
                  xrange=[0,szd[0]-1]*xs,yrange=[0,szd[1]-1]*ys, $
                  title=pi.shortname,ztitle=pi.unit, $
                  xtitle='x [arcsec]',ytitle='y [arcsec]'
    userlct,verbose=0
    polyfill,/normal,[0,0,.05,0.05],.9-0.8*(i+1.)/nt*[0,1,1,0],color=6
    xyouts,charsize=1.5,.04,.88,/normal,orientation=90.,alignment=1., $
           'log '+f_tau+' = '+string(lttop+it*ltinc,format='(f5.1)')
    psset,/close
    if reverse then spawn,'cp -p '+fjpg1+'.jpg '+fjpg2+'.jpg'
  endfor
  
  movie=out+'maptau_'+par+add+'.avi'
  moviecmd='mencoder mf://'+tmp+'*.jpg -mf fps=5:type=jpg -ovc x264 -x264encopts bitrate=6000 -oac copy -o '+movie
  
  print,'Executing Movie command:'
  print,moviecmd
  spawn,moviecmd,result
  print,'Movie created: ',movie 
 
end
