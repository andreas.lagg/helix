pro image_cont_example,ps=ps
  common imaxdata,iid,datafile
  
  file='/data/sunrise/ScienceFlight20090608/Imax/level2/2009y_06m_09d_final/reduc_rnr_163_343.save'
  
                                ;IMaX Daten: nur Einlesen wenn nicht
                                ;bereits geschehen
  reread=n_elements(iid) eq 0
  if reread eq 0 then reread=file ne datafile
  if reread then begin
    restore,/v,file
    datafile=file
  endif
  sz=size(iid)
  nx=sz(1) & ny=sz(2)
  nwl=sz(3)           ;5 Wellenlängenpunkte (bei Hinode haben wir 112)
  nstokes=sz(4)       ;4 Stokes-Vektoren, Reihenfolge I,Q,U,V
  
  
  icimg=reform(iid(*,*,4,0))    ;Kontinuums-Bild
  vimg=reform(iid(*,*,1,3))     ;Stokes-V Bild im 2. WL Punkt
  vimg=vimg/icimg               ;normiere Stokes V aud Ic
  
                                ;öffne Fenster bzw. PS-file: ps flag
                                ;wird vom Programmaufruf übergeben
                                ;(zB: 'david,/ps' macht ps-output).
                                ;size: in cm bei ps, wird entsprechend
                                ;in Pixel umgerechnet für X-output
                                ;es hat noch einige keywords, zB:
                                ;no_x=1 macht kein widget auf
                                ;encaps=1 macht encapsulated
  psset,ps=ps,size=[20.,24],file='david.ps' ;,/encaps,/no_x
  
  !p.multi=[0,1,2]              ;Macht 2 Plots auf eine Seite
                                ;(1 Spalten, 2 Reihen) 
  
  loadct,0                      ;bw color table
  !p.charsize=1.1               ;größere Schrift (Default: 1.0)
  
                                ;definiere ein paar Standardfarben:
  colset                        ;siehe colset.pro
  device,decomposed=0           ;color modus: indexed
  
                                ;nur ein Teil des IC-Bildes soll
                                ;dargestellt werden:
  xrg=[100,500]
  yrg=[120,380]
  img=icimg(xrg(0):xrg(1),yrg(0):yrg(1))
  
                                ;berechne Z-range
  zrg=minmax(img)               ;nimmt min-max des Bildausschnittes
  
                                ;meine routine: hat noch mehr keywords
                                ;als die hier angegebenen, siehe
                                ;image_cont_al.pro außerdem gehen die
                                ;meisten keywords, die auch für die
                                ;IDL routine 'plot' gelten.
  image_cont_al,img,xrange=xrg,yrange=yrg,zrange=zrg, $
    xtitle='X [pixel]',ytitle='Y [pixel]',ztitle='I!LC!N [counts]', $
    title='IMaX - I!LC!N', $
    contour=0, $         ;per default werden contour-linien geplottet 
;    position=[0.1,0.1,0.5,0.7], $ ;in Normal-Koordinaten
    exact=1, $                  ;jedes Pixel wird geplottet
    aspect=1, $                   ;wenn 1 ==> aspect ratio wird bewahrt
    cutaspect=1, $     ;plot wird entsprechend aspect verkleinert
    barpos=0           ;position z-skala (0=links,1=oben,2=rechts,3=unten)   
    
                                ;markiere ein paar punkte in rot: Um
                                ;die direkte Farbskala ansprechen zu
                                ;können muss man das graphics device
                                ;auf decomposed stellen:
  device,decomposed=1         ;color modus: direkt
  for i=0,50 do begin         ;!col.red in colset.pro definiert
    xrandom=randomu(seed)*(xrg(1)-xrg(0))+xrg(0)
    yrandom=randomu(seed)*(yrg(1)-yrg(0))+yrg(0)
    plots,psym=4,xrandom,yrandom,color=!col.red
  endfor
  
                                ;nun legen wir 2 Contour plots von
                                ;Stokes V darüber (Level -100 in grün,
                                ;level +100 in blau). Da sich durch
                                ;das cutaspect-keyword und durch die
                                ;colorbar die Plot-Position änderte,
                                ;müssen wir die hier angeben:
  vvimg=vimg(xrg(0):xrg(1),yrg(0):yrg(1))
  contour,vvimg,xst=5,yst=5,/noerase,c_color=!col.blue,level=.01,c_labels=1
  contour,vvimg,xst=5,yst=5,/noerase,c_color=!col.green,level=-.01,c_labels=1
  
  
                                ;Stokes-V Map darunter (!p.multi)
  zrg=[-1,1]*1e-2
  image_cont_al,vimg,xrange=xrg,yrange=yrg,zrange=zrg, $
    xtitle='X [pixel]',ytitle='Y [pixel]',ztitle='V [counts]', $
    title='IMaX - Stokes V', $
    contour=0, $         ;per default werden contour-linien geplottet 
    exact=1, $                  ;jedes Pixel wird geplottet
    aspect=1, $               ;wenn 1 ==> aspect ratio wird bewahrt
    cutaspect=1, $     ;plot wird entsprechend aspect verkleinert
    barpos=0           ;position z-skala (0=links,1=oben,2=rechts,3=unten)   
  
                                ;IC contour drüber
  contour,img,xst=5,yst=5,/noerase,c_color=!col.red,level=7100.,c_thick=3
 
                                ;PS file muss geschlossen werden:
  psset,/close
end
