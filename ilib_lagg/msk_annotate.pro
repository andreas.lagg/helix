pro msk_annotate,annotate
  
  for ia=0,n_elements(annotate)-1 do if annotate(ia).text ne '' then begin
    xyouts,/data,annotate(ia).xdata,annotate(ia).ydata, $
      color=annotate(ia).color,charsize=annotate(ia).size,annotate(ia).text
  endif
  
end
