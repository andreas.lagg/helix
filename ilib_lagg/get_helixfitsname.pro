function get_helixfitsname,file,synth=synth,atmos=atmos,error=error,all=all
  
  error=1
  typ=['atm_','prof_']
  i=0
  fp=get_filepath(file)
  up=strpos(fp.name,'_')
  if up ne -1  then begin
    filepost=strmid(fp.name,up,strlen(fp.name))
  endif else begin
    print,'Cannot determine filename for helix.'
    print,'The helix atmosphere filename must be:  */atm_*.fits'
    return,''
  endelse
  if keyword_set(synth) then add='prof'
  if keyword_set(atmos) then add='atm'
  if keyword_set(all) then add=['prof','atm']
  
  for i=0,n_elements(add)-1 do begin
    rf=fp.path+add(i)+filepost
    fi=file_info(rf)
    if fi.exists ne 0 then begin
      if n_elements(retfile) eq 0 then retfile=rf else retfile=[retfile,rf]
    endif
  endfor
  error=0
  if n_elements(retfile) eq 0 then begin
    error=1
    retfile=''
  endif
  return,retfile
end

