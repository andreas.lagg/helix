pro spinor_converge,file
  
  fits_read,file,data,hdr
  sz=size(data)
  pth=strmid(file,0,strpos(file,'/',/reverse_search)+1)
  out=pth+'input_atmos.fits'
  par=['TEMPE','BFIEL','GAMMA','VELOS','TGRAD','VGRAD','VMICI']
  
  for j=0,n_elements(hdr)-1 do begin
    if max(strpos(par,strmid(hdr(j),0,5))) eq 0 then begin
      pidx=fix(strmid(hdr(j),9,22))-1
      data(*,*,pidx)=smooth(data(*,*,pidx),2,/edge)
      if strmid(hdr(j),0,5) eq 'VMICI' then begin ;VMICI: new boundaries
        ii=where(data(*,*,pidx) gt 1)
        if ii(0) ne -1 then (data(*,*,pidx))(ii)=0.5
        data(*,*,pidx)=0.5; set all to 0.5 ;>data(*,*,pidx)<1.5
        print,'VMICI:',minmax(data(*,*,pidx))        
      endif
    endif
  endfor
  
                               
  
  
  fits_write,out,data,hdr
  print,'Wrote FITS: ',out
  
                                ;check if input file contains 'FILE' line
  openr,unit,/get_lun,pth+'inv.inp'
  line=''
  repeat begin
    a='' & readf,unit,a
    line=[line,a]
  endrep until eof(unit)
  free_lun,unit
  line=line(1:*)
  
                                ;delete FILE entry if present
  idx=where(strpos(strtrim(line,1),'''FILE''') ne 0)
  line=line(idx)
  
                                ;add 'FILE' entry before CLOSE
  idx=(where(strpos(strtrim(line,1),'''CLOSE''') eq 0))(0)
  line=[line(0:idx-1), $
        '  ''FILE''  ''input_atmos.fits''',$
        line(idx:*)]
  
  openw,unit,/get_lun,pth+'inv.inp'
  for i=0,n_elements(line)-1 do printf,unit,line(i)
  free_lun,unit
  
  print,'Wrote input file with ''FILE'' parameter: '
  print,pth+'inv.inp'
  
end
