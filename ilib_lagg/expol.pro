;extrapolate array
function expol,arr
  
  fin=finite(arr)
  nf=where(fin eq 0)
  if nf(0) eq -1 then return,arr
  
  tarr=arr
  minrat=0.9
  repeat begin
    ok=0
    for k=0,1 do begin
      sz=size(tarr)
      fin=finite(tarr)
      nfin=total(fin,1)
      xv=findgen(sz(1))
      for i=0,sz(2)-1 do if float(nfin(i))/sz(1) gt minrat then begin
        nv=where(fin(*,i) eq 0)
        if nv(0) ne -1 then begin
          frow=where(fin(*,i))
          fpar=poly_fit(xv(frow),tarr(frow,i),2)
;          tarr(nv,i)=fpar(0)+xv(nv)*fpar(1)
          tarr(*,i)=fpar(0)+xv*fpar(1)+xv^2*fpar(2)
;plot,xv,tarr(*,i),/xst,/yst & oplot,color=1,xv(nv),tarr(nv,i),psym=4 & stop
        endif
      endif
      tarr=transpose(tarr)
    endfor
    if ok eq 0 then minrat=minrat-.1
  endrep until min(fin) eq 1 or minrat le 0.
  
  return,tarr
end
