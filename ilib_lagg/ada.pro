pro arrow3d,x,y,z,azi,inc,len,_extra=_extra
  
  x1=x+len*cos(azi)*sin(inc)
  y1=y+len*(sin(azi)*sin(inc))
  z1=len*(cos(inc))
  
                                ;draw the arrow
  plots,/data,/t3d,[x,x1],[y,y1],[0.,z1],_extra=_extra
  
  head=0.1
  angle=45.*!dtor
  
                                ;define end positions of the lines
                                ;defining the arrowhead:
                                ;arrowhead starts x1,y1,z1
                                ;
  xa=x1+head*len*sin(azi)*sin(inc+[angle,-angle])
  ya=y1+head*len*(cos(azi)*sin(inc+[angle,-angle]))
  za=z1-head*len*(cos(inc+[angle,-angle]))
  
  for i=0,1 do $
    plots,/data,/t3d,[x1,xa(i)],[y1,ya(i)],[z1,za(i)],_extra=_extra
  
  
end


pro ada
  
  loadct,0
  
;create some dummy image  
  sx=60
  sy=50
  map=(dist(80))(0:sx-1,0:sy-1)
                                ;scale map to colors 1-255
  mapscl=(map-min(map))/(max(map)-min(map))*254+1
  
  
                                ;define angles for 3D transformation
                                ;(change values to change view direction)
  angle_x=50.
  angle_z=30.
  
                                ;define zrange for arrows
  zrange=[0.,10.]
  xr=[0,sx-1]
  yr=[0,sy-1]
  
                                ;define 3D transformation
  scale3,xrange=xr,yrange=yr,zrange=zr,ax=angle_x,az=angle_z
  
                                ;draw the axes, define x, y range for the plot
  contour,/t3d,map,xrange=xr,yrange=yr,zvalue=0,/xst,/yst,/zst
  
  
                                ;overplot the image
  xarr=(intarr(sy)+1) ## findgen(sx)
  yarr=findgen(sy) ## (intarr(sx)+1)
  for ix=0,xr(1) do for iy=0,yr(1) do begin
    x=xarr([ix,ix+1,ix+1,ix],[iy,iy,iy+1,iy+1])
    y=yarr([ix,ix+1,ix+1,ix],[iy,iy,iy+1,iy+1])
    ncorners=remove_multi(n2s(x)+'.'+n2s(y))
    if n_elements(ncorners) eq 4 then begin
      polyfill,x,y,color=mapscl(ix,iy),/t3d,z=0.
    endif
  endfor
  
                                ;overdraw contours again
  contour,/t3d,map,xrange=xr,yrange=yr,zvalue=0,/xst,/yst,/zst,/noerase
  
  
                                ;now draw arrows:
                                ;create some random arrows
  n=50
  x0=randomu(seed,n)*sx
  y0=randomu(seed,n)*sy
  len=randomu(seed,n)*40
  azi=randomu(seed,n)*2*!pi
  inc=randomu(seed,n)*!pi/2
  
  
  
  loadct,12
  for i=0,n-1 do begin
    arrow3d,x0(i),y0(i),0.,azi(i),inc(i),len(i),color=50,thick=2
  endfor
  
  stop
end
