;replaces bad pixel by closest value
function remove_badpix,img,desc=desc,badpix=badpix
  common badpixlist,badpx,ibad,medimg,bp_par
  common rbp_message,show_nomatch
  
;   common tout1,tout,cnt
;   t=systime(1)
  
  if n_elements(bp_par) ne 0 then bpmode=bp_par.mode $
  else begin
    return,img
  endelse
  
  if bpmode gt 2 or bpmode le -2 then return,img
  
  if bpmode eq 2 then begin ;MCV mode
    end_character=strmid(desc.filename,strlen(desc.filename)-1,1)
    if n_elements(badpix) eq 0 then badpix=end_character ne 'c'
    if keyword_set(badpix) then begin
      rot=0
      if(end_character ne 'c' and $
         desc.date ge 20051001 and desc.date le 20060101) then rot=1
      if rot then img=rotate(img,3)
      if(desc.date lt 20060420) then $
        imgret=badpixels(img,desc) else $
        imgret=badpixels2006(img,desc)
      if rot then imgret=rotate(imgret,1)
    endif else imgret=img
  endif else begin
    sz=size(img)
    if n_elements(badpx) eq 0 then begin
                                ;get bad pixel list from manolos
                                ;badpixel.pro routine:
      imc=badpixels2006(img,desc,xbad=xbad,ybad=ybad,/only_list)
      badpx=byte(img*0)
      badpx(xbad,ybad)=1b
      ibad=where(badpx,complement=igd)
      szb=size(badpx)
      if min(sz(1:2) eq szb(1:2)) eq 0 then begin
        message,/cont,'Bad pixel list does not match size of image.' + $
          ' Returning original image.'
        return,img
      endif
    endif
    if min((size(img))(1:2) eq (size(badpx))(1:2)) eq 0 then begin
      if n_elements(show_nomatch) eq 0 then show_nomatch=1
      if show_nomatch eq 1 then $
        message,/cont,'Bad pixel list does not match size of image.' + $
        ' Returning original image.' + $
        ' Keeping bad pixel list for next matching image.'
      show_nomatch=0
      return,img
    endif else show_nomatch=1
    
    
    if ibad(0) eq -1 then return,img
    nib=n_elements(ibad)
    imgret=img
    
    case bpmode of
      0: begin
        
        maxdist=3l
        for i=0l,nib-1 do begin
          ix=((ibad(i) mod sz(1)))
          iy=ibad(i)  /  sz(1)
          dist=2
          ok=0b
          repeat begin
            ixmin=(ix-dist)>0 & ixmax=(ix+dist)<(sz(1)-1)
            iymin=(iy-dist)>0 & iymax=(iy+dist)<(sz(2)-1)
            igood=where(badpx(ixmin:ixmax,iymin:iymax) eq 0)
            nig=n_elements(igood)
            if nig gt 5 and float((ixmax-ixmin+1)*(iymax-iymin+1))/nig lt 2 then begin
;              iii=median((img(ixmin:ixmax,iymin:iymax))(igood(0:nig-1)),4)
;              imgret(ix,iy)=total(iii)/nig
              imgret(ix,iy)=total((img(ixmin:ixmax,iymin:iymax))(igood(0:nig-1)))/nig
;              print,imgret(ix,iy),total((img(ixmin:ixmax,iymin:iymax))(igood(0:nig-1)))/nig,imgret(ix,iy)-total((img(ixmin:ixmax,iymin:iymax))(igood(0:nig-1)))/nig
              ok=1
            endif else dist=dist+1 or dist gt maxdist
          endrep until ok
;print,ixmin,ixmax,dist,(ixmax-ixmin+1)*(iymax-iymin+1),nig,float((ixmax-ixmin+1)*(iymax-iymin+1))/nig& if dist eq 3 then stop            
        endfor
      end
      1: begin                  ;quick & dirty mode
        sz=size(img)
        replacerow=fltarr(sz(2))
        for j=0,sz(2)-1 do replacerow(j)=median(img(*,j))
        ix=ibad mod sz(1)
        iy=ibad  /  sz(1)
        imgret(ix,iy)=replacerow(iy)
      end
      3: begin                  ;simply set bad pixels to NAN
        avg=total(imgret(igd))/n_elements(igd)
        imgret(ibad)=avg
      end
      -1: begin                 ;simply set bad pixels to NAN
        imgret(ibad)=!values.f_nan
      end
      else: message,'Undefined mode for removal of bad pixels.'
    endcase
  endelse
  
;  if n_elements(tout) ne 0 then print,systime(1)-t,t-tout
;  tout=systime(1)
  return,imgret
end
