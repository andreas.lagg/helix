;define default layout sttings
pro xfits_defaultlayout,index=index
  common xfwg,xfwg
  common fits,fits
  common xf_group,xf_group
  
  xfwg.p.range=0                ;autorange
  xfwg.p.xrg=0 
  xfwg.p.yrg=0 
  
  xfwg.p.contpar.index=-1             ;no contour
  xfwg.p.azipar.flag=0
  
  xfwg.nx.val=4                 ;first row: plot 4 stokes
  case xfwg.defaultlayout.val of
    0: begin
      isp=fits.npar+1           ;
      xfwg.p.par.str=''
      xfwg.p(0:3,0).par.str='Stokes Par'
      xfwg.p(0:3,0).par.val=isp
      xfwg.p(0:3,0).stokes=indgen(4)
      xfwg.p(0:3,0).obsfit=0
      nsadd=1
    end
    1: nsadd=0
    2: begin                    ;B, INC, AZI, VLOS
      ipr=where(fits.pixrep eq 0)
      nc=max(fits.comp[ipr])+1
      if min(xfwg.p[0:3,0].par.str eq 'Stokes Par') eq 1 then off=1 else off=0
      ny=off
      par=['BFIEL','GAMMA','AZIMU','VELOS']
      for ic=0,nc-1 do begin
        hasb=0
        for ip=0,n_elements(par)-1 do begin
          ib=where(fits.comp[ipr] eq ic and fits.par[ipr] eq par[ip])
          if ib[0] ne -1 then begin
            xfwg.p[ip,ny].par.str=par[ip]
            xfwg.p[ip,ny].par.val=ib[0]
            hasb=1
          endif
        endfor
        if hasb eq 1 then ny=ny+1
      endfor
      xfwg.ny.val=ny
    end
  endcase
  
  if xfwg.defaultlayout.val lt 2 then begin
                                ;check all parameters for minmax and
                                ;display only the ones which show
                                ;variations along the diagonal (to
                                ;make it faster)
  nxy=sqrt(fits.nx^2+fits.ny^2)
  diag=long(findgen(nxy*2)/(nxy*2)*(fits.nx*fits.ny))
  diagx=diag mod fits.nx
  diagy=diag/fits.nx
  ishow=-1
  parstr=[fits.short,'User','Stokes Par']
  for ip=0,fits.npar-1 do begin
    tdiag=diag+fits.nx*fits.ny*ip
    mm=minmaxp(/nan,fits.data[tdiag]/fits.valid[tdiag],perc=99.9)
    if fits.pixrep(ip) eq 0 then $
      if max(finite(mm)) ne 0 then $
      if (mm(1) ne mm(0) or fits.nx*fits.ny eq 1) then ishow=[ishow,ip]
  endfor
  if n_elements(ishow) ge 2 then begin
    ishow=ishow(1:*)
    if n_elements(index) ne 0 then begin
      iuse=bytarr(xf_group(index).n)
      for i=0,xf_group(index).n-1 do $
        iuse(i)=max(ishow eq xf_group(index).index(i)) eq 1
      ishow=xf_group(index).index((where(iuse eq 1))>0)
    endif
    ns=n_elements(ishow)
    xfwg.ny.val=(ns/4 + ((ns mod 4) ne 0))+nsadd
    ix=indgen(ns) mod 4
    iy=indgen(ns)/4 +nsadd
    for i=0,ns-1 do begin
      xfwg.p(ix(i),iy(i)).par.val=ishow(i)
      xfwg.p(ix(i),iy(i)).par.str=parstr(xfwg.p(ix(i),iy(i)).par.val)
    endfor
  endif else xfwg.ny.val=1
  endif
  
;  xfwg.parmode.val=0
  xw_parmodeset
  if xfwg.parmode.id ne -1 then $
    widget_control,xfwg.parmode.id,set_combobox_select=xfwg.parmode.val
  
  xfwg.p.plotmode=0             ;set all to mapmode
  
                                ;set AZI range to +-90 or 0,180
  
  iazi=where(strpos(xfwg.p.par.str,'AZI') eq 0)
;  if strupcase(fits.code) eq 'SPINOR' then azirg=[0.,180.] else 
  azirg=[-90.,90]
  if fits.azi_ambi eq 1 then azirg=[-180,180]
  if iazi(0) ne -1 then xfwg.p(iazi).range=azirg
end
