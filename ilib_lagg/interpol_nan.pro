;return an array where NAN values are replaced by average of surrounding pixels
function interpol_nan,data
  
  sz=size(data)
  nel=n_elements(data)
  x=findgen(nel) mod sz(1)
  y=float(lindgen(nel)  /  sz(1))
  
  val=where(finite(data))
  if (abs(min(x)-max(x)) le 1e-4 or abs(min(y)-max(y)) le 1e-4) or $
    val(0) eq -1 then return,data
  

  triangulate,x(val),y(val),tr,b
  
  retdata=trigrid(x(val),y(val),data(val),tr,$
               xout=findgen(sz(1)),yout=findgen(sz(2)))
  
  return,retdata
end
