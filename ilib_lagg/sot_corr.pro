pro sot_corr,data,index

;  routine to read in and properly normalize the level1 SP data

;  adjust for intensity wraparound
  
  n=n_elements(index)
  data=temporary(float(data))
  
  for i=0,n-1 do begin
  
    temp = float(data(*,*,0,i))
  
    whr = where(temp lt 0., countwrap)
    if countwrap ne 0 then temp(whr) = temp(whr) + 65536.
    data(*,*,0,i) = temp

;  bit shifting for SP data
    bitshft = index(i).SPBSHFT
;  account for bit shifting of the data by multiplying the appropriate
;  Stokes images by 2
    case bitshft of
      3: data(*,*,1:2,i) = 2.*data(*,*,1:2,i)
      2: data(*,*,3,i) = 2.*data(*,*,3,i)
      1: data(*,*,0,i) = 2.*data(*,*,0,i)
      0: 
      else: message,'unknown value for SPBSHFT'
    endcase
  endfor
end
