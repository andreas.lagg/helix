function hinode_corr,datin,header
  
  if size(header,/type) eq 7 then begin
    hst=fits_header2st(header) 
  endif else hst=header
  
;  routine to read in and properly normalize the level1 SP data
    
;  adjust for intensity wraparound
  sz=size(datin)
  nx=sz(3)/4
  
  datout=float(datin)
  for i=0,nx-1 do begin
    
    hstidx=(i*4)
  
    temp = float(datin(*,*,i*4))
  
    whr = where(temp lt 0., countwrap)
    if countwrap ne 0 then temp(whr) = temp(whr) + 65536.
    datout(*,*,i*4) = temp

;  bit shifting for SP data
    
;    bitshft = sxpar(hst(hstidx),'SPBSHFT')
;  account for bit shifting of the data by multiplying the appropriate
;  Stokes images by 2
    if hst(hstidx).spbshft ge 1 then $
      datout(*,*,i*4+0) = 2.*datout(*,*,i*4+0)
    if hst(hstidx).spbshft ge 2 then $
      datout(*,*,i*4+3) = 2.*datout(*,*,i*4+3)
    if hst(hstidx).spbshft ge 3 then $
        datout(*,*,i*4+1:i*4+2) = 2.*datout(*,*,i*4+1:i*4+2)
    if hst(hstidx).spbshft lt 0 or hst(hstidx).spbshft ge 4 then $
      message,'unknown value for SPBSHFTT'
  
  endfor
  return,datout
end
