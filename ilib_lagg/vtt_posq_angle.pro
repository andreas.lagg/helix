;routine to calculate +Q angle needed for Hanle computation.
;INPUT required:
;solx, soly    - solar position of observation in arcsec (solar coordinates)
;c1_angle      - coelostat mirror 1 angle in degrees
;date          - date of observation (eg. 31-oct-09)
;returns posQ to limb angle in degrees
function vtt_posQ_angle,solx=solx,soly=soly,c1_angle=c1_angle,date=date
  
  posq_angle=!values.f_nan
  
                                ;angle of observed region with
                                ;respect to the solar NS line:
  SNS_angle=(atan(soly,solx)/!dtor +360) mod 360
  limb_angle=(SNS_angle+90. +360) mod 360
  
  uts=anytim2utc(date,/ecs)
  jd=anytim(date)/86400d + julday(01,01,1979,00,00,00)
                                ;calculate P0 angle to convert from
                                ;CNS (Celestial Nort South) to SNS
                                ;(Solar North South)
  p_deg=get_rb0p(date,/deg)
  
print,'P=',p_deg[2],'  Limb-Angle=',limb_angle  ,'  SNS-Angle=',SNS_angle,'  solar radius=',p_deg[0],'  B0=',p_deg[1]
                                ;now just rotate: and subtract 90
                                ;because the reference for the limb
                                ;angle is solar +x direction and the
                                ;reference for p0 angle is NS
                                ;direction
  posQ_angle=(limb_angle+p_deg[2]-90+720) mod 360
  return,posq_angle
end


                                ;calculate vector parallel to limb in
                                ;solar coordinates:
;  par2limb_vec=[-soly,solx]
;  sun_pos, jd-julday(01,01,1900,00,00,00), longitude, ra, dec, app_long, obliq
;  print,'longitude, ra, dec, app_long, obliq', $
;    longitude, ra, dec, app_long, obliq
  
                                ;calculate image rotation angle caused
                                ;by non-zero position of C1 mirror:
  ; dec_rad=dec*!dtor
  ; az_rad=c1_angle*!dtor
  ; phi_rad=(28.+18./60)*!dtor    ;28deg 18´ 00" North
  ; im_rot = (-asin( cos(phi_rad)*sin(az_rad)/cos(dec_rad))+az_rad)*180./!pi
;wobei phirad die geographische Breite (28 deg) in radian ist, azrad
;der Azimuth vom ersten Spiegel in radian, und declsun die Deklination
;der Sonnen relativ zur Aequatorebene (max. +-23 deg). im_rot ist
;maximal sowas wie 20 Grad (fuer 8.12.07 waren es 16 Grad bei 90 Grad
;Azimuth).
  ; print,'Image rotation: ',im_rot
  
                               ;The +Q angle at VTT is always along
                                ;CNS. In order to get the +Q to limb
                                ;direction we take the limb direction
                                ;vector in solar coordinates and
                                ;rotate it back to the CNS system:
  ; rot_angle=-(p_deg)            ;rotation angle
  ; cr=cos(rot_angle*!dtor)
  ; sr=sin(rot_angle*!dtor)
  ; rmat=[[cr,sr],[-sr,cr]]
  
  ; posQ2limb_vec=rmat ## transpose(par2limb_vec)
  ; posq_angle=atan(posQ2limb_vec(1),posQ2limb_vec(0))/!dtor
  
