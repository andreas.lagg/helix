pro mdi_list,data_dir=dir,dir_levels=dir_levels,ret_structure=headst, $
             data_mask=data_mask
  
  if n_elements(dir) eq 0 then begin
    print,'Usage:'
    print,'  mdi_list,data_dir=''/media/LACIE/VTT-May05/soi-ftp.stanford.edu/data/lagg@mps.mpg.de/'',dir_levels=1,data_mask=''fd_M_01h*'''
    print,'creates list of data files from this directory containing time and location info.'
    print,'Note: program descends dir_levels directory levels!'
    reset
  endif
  
  if n_elements(dir_levels) eq 0 then dir_levels=1
  dir_levels=dir_levels>1
  if n_elements(data_mask) eq 0 then data_mask='*'
  
  ladd=strjoin(replicate('/*',dir_levels))
  fadd='.fits'
  print,'Checking MDI files in '+dir+'/'+data_mask+ladd+fadd
  files=file_search(dir+'/'+data_mask+ladd+fadd,count=cnt)
  
  if cnt eq 0 then message,'No files found'
  
  print,'# of MDI files: ',cnt
  
  mlog='mdi_info.txt'
  print,'writing info to '+mlog
  
  openw,unit,/get_lun,mlog,error=err
  if err ne 0 then message,'Error opening '+mlog
  
  sep=strjoin(replicate('=',80))
  printf,unit,'MDI list for search mask '+dir+ladd+fadd
  printf,unit,sep
  printf,unit,'File','Date/Time Start-Stop','xcen','ycen','observation',$
    format='(a28,a30,2a10,a16)'
  printf,unit,sep
  
  t0=systime(1)
  for i=0,cnt-1 do begin
    slpos=where(byte(files(i)) eq (byte('/'))(0))
    if n_elements(slpos) ge 2 then $
      slpos=slpos(n_elements(slpos)-2:n_elements(slpos)-1)
    name=strmid(files(i),slpos(0)+1,strlen(files(i)))
    
    mdidat=readfits(files(i),header,/silent)
    
    
    if max(strmid(header,0,4) eq 'XCEN') eq 1 then begin
      mhead=fits_header2st(header)
      tn=tag_names(mhead)
      tstop=dat2jul(dat2jul(mhead.trec)+fix(mhead.tstep)/86400d,/reverse)
      mhead.tstop=tstop
      printf,unit,name,mhead.trec+'-'+strmid(mhead.tstop,11,8), $
        mhead.xcen,mhead.ycen,$
        mhead.dpcobsr,$
        format='(a28,a30,2f10.4,a16)'
      thisst={name:files(i), $
              tstart:mhead.tstart,tstop:mhead.tstop,trec:mhead.trec, $
              cadence:mhead.cadence, $
              x:mhead.xcen + mhead.naxis1/2.*mhead.cdelt1*[-1,1], $
              y:mhead.ycen + mhead.naxis2/2.*mhead.cdelt2*[-1,1], $
              centerx:mhead.xcen,centery:mhead.ycen}
      if n_elements(headst) eq 0 then headst=thisst else headst=[headst,thisst]
    endif
    t1=systime(1)
    tend=(t1-t0)/(i+1)*cnt+t0
    if fix((i-1)/100.) ne fix(i/100.) then $
      print,n2s(i)+'/'+n2s(cnt)+', End Time: '+systime(0,tend)
  endfor
  printf,unit,sep
  free_lun,unit
  
  print,'Wrote file '+mlog
end
