;cut region from structure
;used from img_solar 
function cut_box,st,box=box,arcsec=arcsec,lonlat=lonlat,error=error
  
  error=0
  if keyword_set(arcsec) then begin
    xyin=where(st.xstd ge box(0) and st.xstd le box(2) and $
               st.ystd ge box(1) and st.ystd le box(3))
  endif else if keyword_set(lonlat) then begin
    xyin=where(st.lon ge box(0) and st.lon le box(2) and $
               st.lat ge box(1) and st.lat le box(3))
  endif else message,'Select /arcsec or /lonlat.'
  
  if xyin(0) eq -1 then begin
    print,'Map does not contain any pixel in ['+ $
      add_comma(n2s(box),sep=' | ')+']'
    error=1
    return,[0,0,0,0]
  endif
  xin=xyin mod st.nx
  yin=xyin  /  st.nx
  x0=min(xin) & x1=max(xin)
  y0=min(yin) & y1=max(yin)
  nrx=x1-x0+1
  nry=y1-y0+1
  
;  retst={nx:nrx,ny:nry,xstd:st.xstd(x0:x1,y0:y1),ystd:st.ystd(x0:x1,y0:y1), $
;         lon:st.lon(x0:x1,y0:y1),lat:st.lat(x0:x1,y0:y1), $
;         header:st.header,file:st.file,img:st.img(x0:x1,y0:y1)}

  return,[x0,y0,x1,y1]
end

