;laptop battery test program
pro battery,noload=noload
  
  tstart=systime(1)
  bst={perc:-1.,est_hhmmss:'',est_sec:0.,toff:0.,temp:0.,step:0l,cpuload:0., $
       power:0.}
  pm=10.
  batt=replicate(bst,24.*60*pm)
  i=0
  tlast=tstart
  step=0l
  repeat begin
    step=step+1
    if keyword_set(noload) then wait,1 else time_test,'~/tt.txt'
    spawn,'acpi -V -s',result
    tt=systime(1)
    if long((tt-tlast)/60*pm) ne long((tt-tlast-1)/60*pm) or i eq 0 then begin
      batt(i).toff=tt-tstart
      batt(i).step=step
      for j=0,n_elements(result)-1 do begin
        respos=strpos(result(j),'discharging,')
        if respos(0) ne -1 then $
          batt(i).perc=float(strmid(result(j),respos(0)+13, $
                                    strpos(result(j),'%')-respos(0)-13))
        rempos=strpos(result(j),'remaining')
        if rempos(0) ne -1 then begin
          hhmmss=strmid(result(j),rempos-9,8)
          batt(i).est_hhmmss=hhmmss
          batt(i).est_sec=fix(strmid(hhmmss,0,2))*3600. + $
            fix(strmid(hhmmss,3,2))*60. + $
            fix(strmid(hhmmss,6,2))
        endif
        degpos=strpos(result(j),'degrees')
        if degpos(0) ne -1 then $
          batt(i).temp=float(strmid(result(j),degpos-6,5))
      endfor
      spawn,'cat /proc/loadavg',load
      batt(i).cpuload=float((strsplit(load,/ext))(0))
      spawn,'cat /proc/acpi/battery/BAT0/state | grep ''present rate''',power
      batt(i).power=float((strsplit(power,/ext))(2))
      print,batt(i).perc,batt(i).est_hhmmss,batt(i).toff/60., $
        batt(i).power/1e3,batt(i).cpuload, $
        format='(''Perc: '',f4.1,'', Est. time: '',a,' + $
        ''', time (min):'',f6.1,'', Power:'',f6.2,'', Avg.load: '',f4.2)'
      tlast=tt
      i=i+1
    endif
  endrep until batt(i-1).perc le 10.
  im=min(where(batt.est_hhmmss eq ''))
  if im eq 0 then begin
    message,/cont,'No measurement done. Switch Laptop to battery power.'
    retall
  endif
  
  batt=batt(0:im-1)
  
  
  host=getenv('HOST')
  set_plot,'ps'
  device,file='~/battery_'+host+'.ps',xsize=19.,ysize=26.,yoff=1.,xoff=1,color=1
  tvlct,/get,r,g,b & r(1)=255 & b(1)=0 & g(1)=0 & tvlct,r,g,b
  !p.multi=[0,1,2]
  plot,/xst,yrange=[0,200],/yst,batt.toff/60,batt.perc, $
    title='Battery Capacity / avg. CPU load '+host,xtitle='min'
  oplot,linestyle=1,color=1,batt.toff/60,batt.cpuload*100
  plot,/xst,/yst,batt.toff/60,batt.est_sec/60., $
    title='Predicted / Actual Lifetime',xtitle='min',ytitle='min', $
    yrange=[min(batt(1:*).est_sec),max(batt(1:*).est_sec)]/60.
  oplot,linestyle=1,color=1,batt.toff/60, $
    (batt(im-1).est_sec+batt(im-1).toff-batt.toff)/60
  !p.multi=[0,1,3]
  plot,batt.toff/60.,batt.step,/xst,/yst,xtitle='min', $
    ytitle='Step #',title='Loop Steps'
  plot,batt.toff/60.,batt.temp,/xst,/yst,xtitle='min', $
    ytitle='Temperature',title='Temperature'
  plot,batt.toff/60.,batt.power/1e3,/xst,/yst,xtitle='min', $
    ytitle='Watt',title='Power Consumption'
  device,/close
  set_plot,'x'
stop  
end
