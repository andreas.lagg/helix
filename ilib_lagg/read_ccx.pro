pro read_ccx,filex,wl_disp=wl_disp,wl_off=wl_off,icont=icont,error=error, $
             wl_num=wl_num,wl_vec=wl_vec,icntslt=icntslt,icntavg=icntavg, $
             norm_cont=norm_cont,verbose=vrb
  common verbose,verbose
  common readccx,fccx
  
  if n_elements(verbose) eq 0 then verbose=1
  if n_elements(vrb) ne 0 then verbose=vrb
  if n_elements(fccx) eq 0 then fccx=''
  
  fi=file_search(filex,count=cnt)
  if cnt eq 0 then begin
      ccxdir=getenv('CCX')
      fp=get_filepath(filex)
      filex=ccxdir+fp.name
      fi=file_search(filex,count=cnt)
  endif

  error=0
  if cnt eq 0 or strcompress(filex,/remove_all) eq '' then begin
    message,/cont,'AUX (ccx) file '+filex+' not found.'
    error=1
    return
  endif
  ffx=fi
  
  noext=0
  for i=0,n_elements(ffx)-1 do begin
    icc=readfits_ssw(ffx(i),header,/silent)
    wl_vec=readfits_ssw(ffx(i),head_ext,exten_no=1,/silent)
    noext=noext or wl_vec(0) eq -1
    if i eq 0 then begin
      headst=fits_header2st(header)
      tn=tag_names(headst)
      if  max(tn eq 'WL_NUM') eq 0 then begin
        fcc=strmid(ffx(i),0,strlen(ffx(i))-1)
        if verbose gt 0 and fccx ne filex then if i eq 0 then $
          print,'Reading wl_num from '+fcc+': ',format='(a,$)'
        dummy=readfits_ssw(fcc,hcc,/nodata,/silent)
        hccst=fits_header2st(hcc)
        wl_num=hccst.naxis1
      endif
    endif
                                ;check for ICNTSLT and ICNTAVG
    if  max(tn eq 'ICNTSLT') eq 1 then icntslt=headst.icntslt else icntslt=0.
    if  max(tn eq 'ICNTAVG') eq 1 then icntavg=headst.icntavg else icntavg=0.
    if n_elements(norm_cont) eq 1 then begin
      case norm_cont(0) of
        0: nmode='LOCAL'
        1: begin
          nmode='SLIT'
          if icntslt(0) gt 0 then icc(*)=icntslt $
          else begin
            message,/cont,'NORM_CONT SLIT not possible. No info in '+ffx(i)
            reset
          endelse
        end
        2: begin
          nmode='IMAGE'
          if icntavg(0) gt 0 then icc(*)=icntavg $
          else begin
            message,/cont,'NORM_CONT IMAGE not possible. No info in '+ffx(i)
            reset
          endelse
        end
      endcase
      if i eq 0 then $
        if verbose ge 1 then message,/cont,'NORM_CONT '+nmode
    endif 
    if i eq 0 then icont=icc else icont=[icont,icc]
  endfor
  wl_disp=headst.wl_disp
  wl_off=headst.wl_off
  if wl_vec(0) eq -1 then begin
    if max(tn eq 'WL_NUM') eq 1 then begin
      wl_num=headst.wl_num
    endif
    wl_vec=dindgen(wl_num)*wl_disp+wl_off 
    if verbose ge 1 and fccx ne filex then  $
      message,/cont,'Using WL_OFF='+n2s(wl_off)+' and WL_DISP='+n2s(wl_disp)
  endif else begin
    extst=fits_header2st(head_ext)
    wl_num=extst.naxis1
    if verbose eq 1 and fccx ne filex then  $
      message,/cont,'Using WL-Vector defined in FITS Extension #1: WL_NUM='+ $
      n2s(wl_num)
;    wl_disp=0d
;    wl_off=0d
  endelse
  fccx=filex
end
