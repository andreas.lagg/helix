;+
; NAME:
;  readatm
;
; PURPOSE:
;  read  (multiple) atmosphere(s) from outputfile of invert
;
; CATEGORY:
;  utility for invert/stopro
;
; CALLING SEQUENCE:
;   readatm, FNAME, ATM, INFO [, NDPTS]
;
; INPUTS:
;   string  FNAME     :  atm-file to be read
;
; OUTPUT
;   float   ATM(*,*,*):  array which holds the atm(s) read
;               | | |
;               | | Depth point index
;               | Param: 
;               |  'LGTAU' =0,  'DEPTH' =1,  'TEMPE' =2
;               |  'PRESS' =3,  'ELPRE' =4,  'KAPPA' =5
;               |  'DENS', =6   'BFIELD'=7,  'VMICRO'=8
;               |  'VELOC' =9,  'GAMMA' =10, 'CHI'   =11
;               No of atm block
;
;   string INFO(*)    :  additional info for each block
;
; OPTIONAL OUTPUT:
;   NDPTS  = Number of depth poinnts
;
; KEYWORDS:
;   /INFO : Additional info displayed
;
; MODIFICATION HISTORY:
;   created 21. Jan. 1997 by Ch. Frutiger
;-
;===========================================================
PRO n_atmos, FNAME, NPOINTS, NPARAMS
;
; Find number of depth points and params in each atm
;
  N=0
  U=0
  NPT=INTARR(2000)
  NPA=INTARR(2000)
  LL=-1
  S=''

  ON_IOERROR, IOERR  
  CLOSE, 1
  OPENR, 1, FNAME
;  PRINT,'Reading ',fname,' ...'

  REPEAT BEGIN
;   read number of depth pts in atms
    READF, 1, N
;   read data first line   
    READF, 1, S
    M=1
;   count #colums from spaces
    c=strcompress(s)
    for i=1,strlen(c)-1 do begin
      if (strmid(c,i,1) eq ' ') then m=m+1
    endfor
    LL=LL+1
    NPT(LL)=N
    NPA(LL)=M
    
    FOR I=1,N-1 DO BEGIN
      READF, 1, S
    ENDFOR
    IF NOT EOF(1) THEN READF,1, S
  ENDREP UNTIL EOF(1)

IOERR:   
  CLOSE, 1
  IF (LL EQ -1) THEN LL=0
  NPOINTS=NPT(0:LL)
  NPARAMS=NPA(0:LL)
  RETURN

  END

;-------------------------------------------------------------
PRO readatm, FNAME, ATM, INFO, NDPTS=NDPTS, QS=QS, SILENT=SILENT

  ; On-line help
  H=GETHELP('FNAME')
  HS=STRPOS(H,'STRING')
  N=N_PARAMS()
  IF ((N NE 3) OR (HS(0) EQ -1)) THEN BEGIN
    DOC_LIBRARY, 'RDATMOS'
    RETURN
  ENDIF

  N_ATMOS, FNAME, NPT, NPA
  S=SIZE(NPT)
  NATM=S(1)
  NPTMAX=MAX(NPT)
  NPAMAX=MAX(NPA)+1
  IF (NPTMAX EQ 0) THEN BEGIN
    PRINT,'NO ATMs read.'
    GOTO, FINISH
  ENDIF

  IF keyword_set(info) AND NOT keyword_set(silent) THEN BEGIN
    PRINT,'Number of atmospheres:',NATM,FORMAT='(A,I4)'
    PRINT,'Max number of points :',NPTMAX,FORMAT='(A,I4)'
    PRINT,'Max number of params :',NPAMAX,FORMAT='(A,I4)'
  ENDIF
  
  ATM=FLTARR(NATM,NPAMAX,NPTMAX)
  INFO=STRARR(NATM)
    
  I=0
;  ON_IOERROR, IOERR
  err=1
  ON_IOERROR, IOERR1
  CLOSE, 1  
  OPENR, 1, FNAME

  FOR I=0,NATM-1 DO BEGIN
    J=-1
    M=0
    S=''
    READF, 1, M, S
    INFO(I)=STRING(I,FORMAT='(I4)')+': '+S
    IF (M NE NPT(I)) THEN BEGIN
      PRINT,'INTERNAL ERROR CHECK READATM.PRO!'
      PRINT,'ATM:',I,' #PTS=',M,' EXPECTED:',NPT(I)
      GOTO, FINISH
    ENDIF
    j=0
    repeat begin
;    FOR J=0,M-1 DO BEGIN
      NP = NPA(I)
      AL=FLTARR(NP)
      err=1
      READF, 1, AL
      ATM(I,0:NP-1,J)=AL
      err=0
      ioerr1:
;    ENDFOR    
      j=j+1
    endrep until eof(1) or j eq m-1 or err eq 1
    IF NOT EOF(1) THEN READF, 1, S
  ENDFOR
  CLOSE, 1
  NDPTS=NPT
  RETURN

; IOERR:  
;   CLOSE,1
;   PRINT, INFO(I)
;   PRINT, J
;   PRINT, 'FILE I/O ERROR. READATM terminated.'

; set all output variables to zero
FINISH: 
  ATM=0.
  INFO=0
  NDPTS=0
  RETURN

END
