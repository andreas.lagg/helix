pro xss_setspline
  common xfsswg,xfsswg
  common xss,xss
  
  if n_elements(xfsswg) ne 0 then if xfsswg.base.id ne 0 then begin
    widget_control,xfsswg.base.id,bad_id=bad_id    
    if bad_id eq 0 then begin
      widget_control,xfsswg.spline.id,set_combobox_select=xfsswg.spline.val
      n=xfsswg.spline.val
;      widget_control,xfsswg.tension.id,set_value=xss.par[n].tension
      widget_control,xfsswg.color.id,set_value=xss.par[n].color
      widget_control,xfsswg.inner.id,set_value=xss.par[n].inner
      widget_control,xfsswg.fill.id,set_value=xss.par[n].fill
    endif
  endif
end

pro xss_splineplot,calc=calc
  common xfsswg,xfsswg
  common xss,xss
  common fits,fits
  
  xfits_splineselect,/check
  userlct
  for i=0,xss.n-1 do begin
    np=xss.par[i].np
    xdp=scaleaxis(/x,xss.par[i].x[0:np-1])
    ydp=scaleaxis(/y,xss.par[i].y[0:np-1])
    if i eq xfsswg.spline.val and !d.name ne 'PS' then begin
      xydev=convert_coord(/data,/to_device,xdp,ydp)
      plots,/device,color=4,xydev[0,*]+1,xydev[1,*]+1,psym=-4,noclip=0
    endif
    plots,color=xss.par[i].color,xdp,ydp, $
          psym=-4,noclip=0,symsize=([1,0.2])[!d.name eq 'PS']
    
    if np ge 3 then begin
      inside=polyfillv(xss.par[i].x[0:np-1]-fits.xoff, $
                       xss.par[i].y[0:np-1]-fits.yoff,fits.nx,fits.ny)
    endif else inside=-1
    if inside[0] ne -1 then begin
      if keyword_set(calc) then begin
        flag=bytarr(fits.nx,fits.ny)
        flag[inside]=1b
        if xss.par[i].inner eq 0 then flag=flag eq 0b
        xss.par[i].flag=flag
      endif
      inidx=where(xss.par[i].flag eq 1b)
      if inidx[0] ne -1 then begin
        if xss.par[i].fill eq 1 then begin
          xfill=xss.par[i].x[0:np-1]-fits.xoff
          yfill=xss.par[i].y[0:np-1]-fits.yoff
          if xss.par[i].inner eq 0 then begin
            xfill=[xfill,!x.crange([0,0,1,1,0])]
            yfill=[yfill,!y.crange([0,1,1,0,0])]
          endif
          xfp=scaleaxis(/x,xfill)
          yfp=scaleaxis(/y,yfill)
          polyfill,color=xss.par[i].color,/line_fill,noclip=0, $
                   orientation=45+i*22.5,xfp,yfp
;        xy=array_indices(flag,inidx)
;        plots,psym=3,color=color,xy[0,*],xy[1,*],noclip=0
                                ;fill statistics array
        endif
        if keyword_set(calc) then begin
        xss.par[i].stat.n=n_elements(inidx)
        for ip=0,fits.npar-1 do begin
          mom=moment((fits.data[*,*,ip])[inidx])
          xss.par[i].stat.mean[ip]=mom[0]
          xss.par[i].stat.sdev[ip]=sqrt(mom[1])
        endfor
        endif
      endif else for it=0,n_tags(xss.par[i].stat)-1 do xss.par[i].stat.(it)=0.
    endif
  endfor
                                ;compute statistics for all splies
  if keyword_set(calc) then begin
  isin=xss.par[0].flag
  for i=1,xss.n-1 do isin=isin or xss.par[i].flag
  inidx=where(isin eq 1b)
  if inidx[0] ne -1 then begin
    xss.allstat.n=n_elements(inidx)
    for ip=0,fits.npar-1 do begin
      mom=moment((fits.data[*,*,ip])[inidx])
      xss.allstat.mean[ip]=mom[0]
      xss.allstat.sdev[ip]=sqrt(mom[1])
    endfor
  endif else xss.allstat.n=0
  print,'Calculated spline statistics.'
  endif
end

pro xss_point,xdat,ydat,mouse
  common xfsswg,xfsswg
  common xss,xss
  
  xfits_splineselect,/check
  n=fix(xfsswg.spline.val)
  ip=xss.par[n].np
  oldip=ip
  update_plot=0
  case mouse.press of
    1: begin                    ;add/close
      if mouse.clicks eq 2 then begin ;close
        xss.par[n].x[ip]=xss.par[n].x[0]
        xss.par[n].y[ip]=xss.par[n].y[0]
        xss.par[n].np++        
        update_plot=1
      endif else begin          ;add
        xdp=scaleaxis(/x,xdat)
        ydp=scaleaxis(/y,ydat)
        plots,psym=4,xdp,ydp
        xss.par[n].x[ip]=xdat
        xss.par[n].y[ip]=ydat
        xss.par[n].np++
      endelse
    end
    4: if ip ge 1 then begin    ;delete
                                ;delete pixel closest to mouse
      xdp=scaleaxis(/x,xss.par[n].x[0:ip-1])
      ydp=scaleaxis(/y,xss.par[n].y[0:ip-1])
      dist=sqrt((xss.par[n].x[0:ip-1]-xdat)^2+(xss.par[n].y[0:ip-1]-ydat)^2)
      dummy=min(dist,imin)
;      imin=ip-1                 ;delete last pixel (not closest)
      plots,color=255,psym=4,xdp[imin],ydp[imin]
      xss.par[n].x[imin:ip-1]=xss.par[n].x[imin+1:ip]
      xss.par[n].y[imin:ip-1]=xss.par[n].y[imin+1:ip]
      xss.par[n].flag=0b
      xss.par[n].np--
      update_plot=1
    endif
    else:
  endcase
  ip=xss.par[n].np
  if (ip eq 1 and oldip eq 0) or (ip eq 0 and oldip eq 1) then begin
    widget_control,xfsswg.base.id,/destroy
    xfits_splineselectwg
  endif
  xss_remove
  
  if update_plot then xfits_plotupdate
  xss_splineplot,calc=update_plot
end

pro xss_printstat,latex=latex
  common xss,xss
  common fits,fits
  common xdir,xdir
 
  if xss.n eq 0 then return
  
  xfits_plotupdate
  s='' & e=''
  if keyword_set(latex) then begin
    s=' & ' & e=' \\'
  endif
  fmt='(i7)'
  amt=strmid(fmt,2,1)
  line=''
  tss=(transpose([['Spl. '+n2s(indgen(xss.n)+1)], $
                  ['SDev '+n2s(indgen(xss.n)+1)]]))[*]
  line=[line, $
        
        string(format='(a8,'+n2s(xss.n+1)+'('''+s+''',a'+amt+','''+s+''',a'+amt+'),'''+e+''')', $
               ['Param',tss,'Total','Sdev'])]
  tss=(transpose([[n2s(xss.par[0:xss.n-1].stat.n)],[strarr(xss.n)]]))[*]
  line=[line, $
        string(format='(a8,'+n2s(xss.n+1)+'('''+s+''',a'+amt+','''+s+''',a'+amt+'),'''+e+''')',               ['N=',tss,n2s(long(total(xss.par[0:xss.n-1].stat.n))),''])]
  
  for ip=0,fits.npar-1 do begin
    tline=string(fits.par[ip],format='(a8,$)')
    if (fits.par[ip] eq 'VELOS' or $
        fits.par[ip] eq 'VMICI' or $
        fits.par[ip] eq 'VMACI') then scl=1e3 else scl=1.
    for i=0,xss.n-1 do begin
      val=s+string(scl*xss.par[i].stat.mean[ip],format=fmt)+ $
          s+string(scl*xss.par[i].stat.sdev[ip],format=fmt)
      tline=tline+val
    endfor
    val=s+string(scl*xss.allstat.mean[ip],format=fmt)+ $
        s+string(scl*xss.allstat.sdev[ip],format=fmt)
    tline=tline+val+e
    line=[line,tline]
  endfor
  
  if keyword_set(latex) then begin
    openw,unit,/get_lun,xdir.tmp+'spline.tex'
    printf,unit,line
    free_lun,unit
    print,'Table written to: '
    print,'   '+xdir.tmp+'spline.tex'
  endif else print,line,format='(a)'

  xym=fltarr(xss.n,2)
  for i=0,xss.n-1 do $
    xym[i,*]=[mean(xss.par[i].x[0:xss.par[i].np-1]), $
              mean(xss.par[i].y[0:xss.par[i].np-1])]
  print,'XY-coordinates: [['+ $
        add_comma(string(xym[*,0],format='(f8.2)'),sep=',')+'],['+$
        add_comma(string(xym[*,1],format='(f8.2)'),sep=',')+']]'
  
  
;print out table in Latex format

end

pro xss_store
  common xss,xss
  common fits,fits
  common xdir,xdir
  common xfsswg,xfsswg
 
  if xss.n eq 0 then return
  ff=get_filepath(xss.file)
  if ff.name eq '' then begin
    ff.path=xdir.save
    ff.name='selection.spline.sav'
  endif
  
  fsav=dialog_pickfile(dialog_parent=xfsswg.base.id,/write, $
                       title='Write Spline Parameters', $
                       default_extension='.spline.sav',path=ff.path, $
                       file=ff.path+ff.name,/overwrite_prompt)
  if fsav eq '' then return
  save,file=fsav,xss,/compress,/xdr
  print,'Wrote sav file: '+fsav
  xss.file=fsav
end
  
pro xss_load
  common xss,xss
  common fits,fits
  common xdir,xdir
  common xfsswg,xfsswg
 
  ff=get_filepath(xss.file)
  if ff.name eq '' then begin
    ff.path=xdir.save
  endif
  fsav=dialog_pickfile(dialog_parent=xfsswg.base.id,/read, $
                       title='Read Spline Parameters', $
                       default_extension='.spline.sav',path=ff.path, $
                       /must_exist)
  if fsav eq '' then return
  xssold=xss
  print,'Reading sav file: '+fsav
  restore,fsav
  xssnew=xss
  xssnew=update_struct(xssnew,xssold)
  xss=xssnew
  xss.file=fsav
  xss.n=xssnew.n+xssold.n
  if xssold.n ge 1 then $
    xss.par[0:xssold.n-1]=xssold.par[0:xssold.n-1]
  if xssnew.n ge 1 then begin
    xss.par[xssold.n:xssold.n+xssnew.n-1]=xssnew.par[0:xssnew.n-1]
    xfits_splineselect
  endif
end
  
pro xfsswg_event,event
  common xfwg,xfwg
  common xpwg,xpwg
  common fits,fits
  common xfsswg,xfsswg
  common xss,xss

  widget_control,event.id,get_uvalue=uval
  
  n=fix(xfsswg.spline.val)
  case uval of 
    'control': begin
      case event.value of
        'done': widget_control,xfsswg.base.id,/destroy
        'printstat': begin
          xss_splineplot,/calc
          xss_printstat
          xss_printstat,/latex
        end
        'store': xss_store
        'load': xss_load
      end
    end
    'spline': begin
      oval =xfsswg.spline.val
      xfsswg.spline.val=event.index
      if xfsswg.spline.val eq xss.n or xss.n eq 0 then begin ;add button
      endif
      n=fix(xfsswg.spline.val)
      if oval ne xfsswg.spline.val then begin
        xss_setspline
        if xss.n ge 1 then xfits_plotupdate
      endif
    end
    'delete': begin
      xss_remove,n
      widget_control,xfsswg.base.id,/destroy
      xfits_splineselectwg
    end
    'tension': xss.par[n].tension=event.value
    'color': xss.par[n].color=event.value
    'inner': xss.par[n].inner=event.value
    'fill': xss.par[n].fill=event.select
    else: help,/st,event
  endcase
;  help,/st,event
  xss_splineplot;,/calc
end

pro xss_remove,n
  common xss,xss
  
  if n_params() eq 1 then xss.par[n].np=0
  
  neq0=where(xss.par.np ge 1,cnt)
  if cnt ge 1 then begin
    xss.par[0:cnt-1]=xss.par[neq0]
    xss.par[cnt:*].np=0
    xss.par[cnt:*].x=0
    xss.par[cnt:*].y=0
    xss.par[cnt:*].flag=0b
  endif
  
  xss.n=cnt
  xss_setspline
end
  
pro xfits_splineselectwg,init=init
  common xpwg,xpwg
  common xfsswg,xfsswg
  common xss,xss
  common fits,fits
  
  subst={id:0l,val:0.,str:''}
  oxfsswg={base:subst,control:subst,info:subst,spline:subst, $
           inner:subst,tension:subst,delete:subst,fill:subst,color:subst}
  
  if n_elements(xfsswg) eq 0 then xfsswg=temporary(oxfsswg) $
  else xfsswg=update_struct(xfsswg,oxfsswg)
  
                                ;spline settings variable
  npmax=100
  stat={n:0l,mean:fltarr(fits.npar),sdev:fltarr(fits.npar)}
  sspar={np:0,x:fltarr(npmax),y:fltarr(npmax),inner:1b,tension:3, $
         flag:bytarr(fits.nx,fits.ny),fill:0b,stat:stat,color:0}
  oxss={n:0,par:replicate(sspar,100),allstat:stat,file:''}
  if n_elements(xss) eq 0 then xss=temporary(oxss) $
  else xss=update_struct(xss,oxss)
  xss_remove
  if keyword_set(init) then return
  
  info=widget_info(xpwg.base.id,/geometry)
  xfsswg.base.id=widget_base(title='Spline Select Tool', $
                             group_leader=xpwg.base.id,col=1, $
                             xoffset=info.xoffset+info.xsize, $
                             yoffset=info.yoffset)
  dummy=widget_label(xfsswg.base.id,/align_left, $
                     value='Left click: add points, right click: remove points')
  dummy=widget_label(xfsswg.base.id,/align_left, $
                     value='Double-click: close region')
  sub=widget_base(xfsswg.base.id,row=1,/frame)
  xfsswg.spline.id=widget_combobox(sub,uvalue='spline', $
                                   value=[n2s(indgen(xss.n>1)+1),'New'])
  xfsswg.inner.id=cw_bgroup(sub,/exclusive,['outside','inside'], $
                            uval='inner',row=1)
  xfsswg.fill.id=cw_bgroup(sub,/nonexclusive,['fill'], $
                            uval='fill',row=1)
;  xfsswg.tension.id=cw_field(sub,xsize=2,/all_events,title='tension:', $
;                             uvalue='tension',/int)
  xfsswg.color.id=cw_field(sub,xsize=2,/all_events,title='color:', $
                             uvalue='color',/int)
  xss_setspline
  
  xfsswg.delete.id=widget_button(sub,value='Delete',uvalue='delete')
  
  sub=widget_base(xfsswg.base.id,row=1,/frame)
  
  xfsswg.control.id=cw_bgroup(sub,[' Print Statistics ',' Store ', $
                                   ' Load ',' Done '], $
                              uvalue='control',row=1,space=0, $
                              button_uvalue=['printstat','store', $
                                             'load','done'])   
  
  
  widget_control,xfsswg.base.id,/realize
  xmanager,'xfsswg',xfsswg.base.id,no_block=1
  
end


pro xfits_splineselect,destroy=destroy,init=init,check=check
  common xfsswg,xfsswg
  common xpwg,xpwg
  
  if n_elements(xfsswg) ne 0 then if xfsswg.base.id ne 0 then begin
    widget_control,xfsswg.base.id,bad_id=bad_id
    if bad_id eq 0 then begin
      if keyword_set(check) then return ;widgt is okay
      widget_control,xfsswg.base.id,/destroy
    endif
  endif
  if keyword_set(destroy) then return
  
  xfits_splineselectwg,init=init
    
end
