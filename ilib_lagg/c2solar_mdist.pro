function c2solar_mdist,mdifits,img=img,solar=solar,soho=soho
  
  mdidat=readfits(mdifits,header)
  mhead=fits_header2st(header)
  
  fits2map,mdifits,iii
  iii=rot_map(iii,iii.roll_angle)
  
  
  b0=mhead.obsb0
  r0=mhead.obsr0
  print,'MDI: R0=',r0,'  b0=',b0
  
  pixx=mhead.xscale
  pixy=mhead.yscale
  
  nrx=mhead.naxis1
  nry=mhead.naxis2
  
;    xvec=(indgen(nrx)-nrx/2.)*mhead.xscale+mhead.x0-mhead.centerx
;    yvec=(indgen(nry)-nry/2.)*mhead.yscale+mhead.y0-mhead.centery
;    xvec=xvec*mhead.imscale
;    yvec=yvec*mhead.imscale
  
;  xvec=(findgen(nrx)-mhead.centerx)*mhead.imscale
;  yvec=(findgen(nry)-mhead.centery)*mhead.imscale
  xvec=(findgen(nrx)-nrx/2)*iii.dx+iii.xc
  yvec=(findgen(nry)-nry/2)*iii.dy+iii.yc
  
  xarr=transpose(yvec*0+1.) ## xvec
  yarr=transpose(yvec)      ## (xvec*0+1.)
  
  xyssw=transpose([[xarr(*)],[yarr(*)]])
  lonlat=xy2lonlat(xyssw,b0=b0*!dtor,radius=r0)
  
                                ;calculate xy in solar coords for
                                ;standard values of b0=0. and r0=950.
  soho=keyword_set(soho)
  solar=keyword_set(solar)
  if soho eq 0 and solar eq 0 then solar=1
  if solar then begin
    print,'Converting to true solar coords (B0=0, R=950'')'
    xystd=lonlat2xy(lonlat,b0=0.,radius=950.)
  endif
  if soho then begin
    print,'Using SOHO coordinates'
    xystd=xyssw
  endif
  
  xstd=reform(xystd(0,*),nrx,nry)
  ystd=reform(xystd(1,*),nrx,nry)
  lon=reform(lonlat(0,*),nrx,nry)
  lat=reform(lonlat(1,*),nrx,nry)
  
  date=strcompress(strmid(mhead.tstart,0,10),/remove_all)
  time=strcompress([strmid(mhead.tstart,11,8),strmid(mhead.tstop,11,8)], $
                   /remove_all)
  st={nx:nrx,ny:nry,xstd:xstd,ystd:ystd,lon:lon,lat:lat, $
      header:mhead,file:mdifits,time:time,date:date}
  
;  img=mdidat
  img=iii.data
  return,st
  
end
