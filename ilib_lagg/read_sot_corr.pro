pro read_sot_corr,spf,index,data,_extra=_extra
  
  read_sot,spf,index,data,_extra=_extra
  sz=size(data)
  if sz[0] eq 3 then begin      ;3D data
    data=reform(data,sz[1],sz[2],4,sz[3]/4)
  endif
  data=float(data)
  sz=size(data)
  nx=sz(4)  
  data[*,*,0,*]=data[*,*,0,*]+65536.*(data[*,*,0,*] lt 0.)
  for i=0,nx-1 do begin
    if index[i*4+0].spbshft eq 1 then data[*,*,0,i] = 2.*data[*,*,0,i]
    if index[i*4+3].spbshft eq 2 then data[*,*,3,i] = 2.*data[*,*,3,i]
    if index[i*4+1].spbshft eq 3 then data[*,*,1,i] = 2.*data[*,*,1,i]
    if index[i*4+2].spbshft eq 3 then data[*,*,2,i] = 2.*data[*,*,2,i]
  endfor
end
  
