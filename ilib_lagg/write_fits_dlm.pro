pro write_fits_dlm,data,fitsfile,header,append=append,create=create
  
  if keyword_set(append) then create=0 else create=1
  if keyword_set(create) then append=0 else append=1
  
                                ;check if FITS DLM is available:
  if float(!version.release) ge 8 then dlmmode=0b else begin
    dlmmode=0b
    help,/dlm,output=dlmout
    if max(strpos(dlmout,'** FITS')) ne -1 then begin
      dlm_load,'fits'
      help,/dlm,output=dlmout
      ii=(where(strpos(dlmout,'** FITS') ne -1))[0]
      dlmmode=0b
      if ii ne -1 then begin
        if max(strpos(dlmout[ii],'(not loaded)')) eq -1 then begin
          dlmmode=1b
        endif
      endif
    endif
    if dlmmode eq 0 then message,/cont,'IDL FITS DLM not found.' $
    else if strpos(fitsfile,'.gz') eq strlen(fitsfile)-3 then begin
      message,/cont,'IDL FITS DLM cannot deal with .gz files.'
      dlmmode=0b
    endif
  endelse
  
  if dlmmode then begin  
                                ;remove the tilde. ftswr does not like
                                ;it.
    fp=get_filepath(fitsfile)
    fi=file_info(fp.path)
    fitsfilenotilde=fi.name+'/'+fp.name
                                ;use execute:
                                ;otherwise the file cannot be compiled
                                ;if DLM is missing
    dummy=execute('ftswr,data,fitsfilenotilde,header,create=create')
  endif else begin
    nbyt=bytesize(data)
    if nbyt ge 2e9 and float(strmid(!version.release,0,1)) le 7 then begin
      message,/cont,'WARNING: Large FITS files must be written with the FTSWR dlm. Do you want to continue using the standard IDL writefits routine [N/y]?'
      key=get_kbrd()
      if strupcase(key) ne 'Y' then stop
    endif
    
    writefits,fitsfile,data,header,append=create eq 0
    if nbyt ge 2e9 then $
      message,/cont,'Wrote '+fitsfile+' using standard IDL routine.'
  endelse
end
