pro coordtrans_zakharov
;++++++++++++++++++read results ofinversions++++++++++++++++++++++++++++++++++
restore,'t6.3.inversion.sav'
v=rotate(v,2)
b=rotate(b,2)
gamma=rotate(gamma,2)
ff=rotate(ff,2)
wb=readfits('imarr.6.3.fts')
wb=rotate(wb(*,*,0),6)
restore,'t6.3.azimuth_resolved.sav'

;------------Let's compute new azimuth and inclination----------------
x=sin(gamma*(3.1415926/180.))*cos(alpha*(3.1415926/180.))
y=sin(gamma*(3.1415926/180.))*sin(alpha*(3.1415926/180.))
z=cos(gamma*(3.1415926/180.))

ae=68.8*(3.1415926/180.)
th=40.15*(3.1415926/180.)      ;heliocentric angle
rv=[cos(ae),sin(ae),0]         ;vector-axis of rotation
M=[[cos(th)+(1-cos(th))*rv(0)2,(1-cos(th))*rv(0)*rv(1)-(sin(th))*rv(2),(1-cos(th))*rv(0)*rv(2)+(sin(th))*rv(1)],$
 
[(1-cos(th))*rv(0)*rv(1)+(sin(th))*rv(2),cos(th)+(1-cos(th))*rv(1)2,(1-cos(th))*rv(1)*rv(2)-(sin(th))*rv(0)],$
 
[(1-cos(th))*rv(2)*rv(0)-(sin(th))*rv(1),(1-cos(th))*rv(2)*rv(1)+(sin(th))*rv(0),cos(th)+(1-cos(th))*rv(2)2]]

s=size(x)
x_new=fltarr(s(1),s(2))
y_new=fltarr(s(1),s(2))
z_new=fltarr(s(1),s(2))
for j=0,s(2)-1 do begin
 for i=0,s(1)-1 do begin
  temp=[x(i,j),y(i,j),z(i,j)]    ;observed vector
  t=M##temp                      ;derotated (local) vector
  x_new(i,j)=t(0)
  y_new(i,j)=t(1)
  z_new(i,j)=t(2)
 endfor
endfor

gamma_new=acos(z_new)*180./3.1415926
alpha_new=acos(x_new/sqrt(x_new2+y_new2))*180./3.1415926
;------------------------------------------------------------------
end
