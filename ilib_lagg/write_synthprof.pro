;write out synthetic profiles with all components
pro write_synthprof
  common fits,fits
  common xfwg,xfwg
  
  case fits.code of
    'helix': begin
      ipt=fits.ipt
      if ipt.ncomp eq 1 then begin
        message,/cont,'Atmosphere has only one component. ' + $
                'There is no need to write out the individual component sof the fit.' + $
                ' returning.'
        return
      endif
      ffname='ALPHA'
      for ix=0,fits.nx-1 do begin
        for iy=0,fits.ny-1 do begin
          fits_compute_helix,x=ix+fits.xoff,y=iy+fits.yoff, $
                             fitprof=fitprof,obsprof=obsprof,compprof=compprof
          if ix eq 0 and iy eq 0 then begin
            nwl=n_elements(fitprof.wl)
            profarr=fltarr(nwl,4,fits.nx,fits.ny,ipt.ncomp+1)
            icont=fltarr(fits.nx,fits.ny)
            wlvec=fitprof.wl+fitprof.wlref
          endif
          profarr[*,0,ix,iy,0]=fitprof.i
          profarr[*,1,ix,iy,0]=fitprof.q
          profarr[*,2,ix,iy,0]=fitprof.u
          profarr[*,3,ix,iy,0]=fitprof.v
          profarr[*,0,ix,iy,1:*]=compprof.i
          profarr[*,1,ix,iy,1:*]=compprof.q
          profarr[*,2,ix,iy,1:*]=compprof.u
          profarr[*,3,ix,iy,1:*]=compprof.v
          icont[ix,iy]=fitprof.ic
        endfor
      endfor
    end
    else: message,/cont,'Writing out synthetic profiles is only supported for HeLIx data.'
  endcase
  
                                ;create filename
  fp=get_filepath(fits.file)
  if strmid(fp.name,0,4) eq 'atm_' then fp.name='profC_'+strmid(fp.name,4,strlen(fp.name)) $
  else fp.name='profC_'+fp.name
  fitsout=fp.path+fp.name
  print,'Write profiles for all components to '+fitsout
  
  hdr=fits.header
  mrd_head,fits.synthfile,hdr,err=err
  if err ne 0 then mkhdr,hdr,profarr
  
  check_fits,profarr,hdr,/update
  
  sxaddpar,hdr,'COMMENT','this file contains synthetic HeLIx+ profiles for all components'
  sxaddpar,hdr,'STOKES','IQUV','orderof Stokes vector'
  sxaddpar,hdr,'CTYPE1','wavelength'
  sxaddpar,hdr,'CTYPE2','stokes parameter'
  sxaddpar,hdr,'CTYPE3','x'
  sxaddpar,hdr,'CTYPE4','y'
  sxaddpar,hdr,'CTYPE5','atm-comp of omputed profile (0=all, 1=C1, 2=C2, ...'
  sxaddpar,hdr,'COMMENT','EXTENSION 1: Continuum image'
  sxaddpar,hdr,'COMMENT','EXTENSION 2: Wavelength Vector'
  sxaddpar,hdr,'COMMENT','EXTENSION 3: Filling Factors for the components'
  
  write_fits_dlm,profarr,fitsout,hdr
  
                                ;write out continuum image to first extension
  mkhdr,hdr,icont,/image
  sxaddpar,hdr,'EXTNAME','Continuum Image'
  write_fits_dlm,icont,fitsout,hdr,append=1
  
                                ;write WL-info second extension
  mkhdr,hdr,wlvec,/image
  sxaddpar,hdr,'EXTNAME','Wavelength Vector'
  write_fits_dlm,wlvec,fitsout,hdr,append=1
  
                                ;write out filling factors to third
                                ;extesnsion
  iff=where(fits.par eq ffname)
  if iff[0] eq -1 then begin
    message,/cont,'Problems in retrieving FF. Returning.'
    return
  endif
  ffarr=reform(fits.data[0:fits.nx-1,0:fits.ny-1,iff])
  mkhdr,hdr,ffarr,/image
  sxaddpar,hdr,'EXTNAME','filling factors'
  write_fits_dlm,ffarr,fitsout,hdr,append=1
  
end
