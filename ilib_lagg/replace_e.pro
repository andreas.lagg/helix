;replace exponent with IDL string control characters
function replace_e,str,ticks
 
  new=str & vor=strarr(n_elements(str))+'1'
  if ticks gt 0 then begin
    tp1=(ticks+1)<(n_elements(str)-1)
    expflag=where(strpos(str(1:tp1),'e+') ne -1 or $
                  strpos(str(1:tp1),'e-') ne -1 )
    if n_elements(expflag) lt tp1 then begin
                                ;set all values to exp values if at
                                ;least one is an exp value
      bt=byte(str(1:ticks))
      allow=byte('0123456789.-+eE ')
      schar=0
      for i=0,n_elements(allow)-1 do schar=schar or max(bt eq allow(i)) eq 0
      if schar eq 0 then begin
        if min(float(str(1:ticks))) gt 0 then $
          if max(alog10(float(str(1:tp1))))- $
          min(alog10(float(str(1:tp1)))) gt 2 then expflag=1
        if expflag(0) ne -1 then if n_elements(expflag) ne tp1 then begin
          str=n2s(format='(e15.5)',float(str))
        endif
      endif
    endif
  endif

  for i=0,(n_elements(str)<(ticks+2))-1 do begin
    ep=strpos(str(i),'e+')
    if ep(0) eq -1 then ep=strpos(str(i),'e-')
    if ep(0) ne -1 then begin
      vor(i)=strmid(str(i),0,ep(0))
      if strpos(/reverse_search,vor(i),'.') eq strlen(vor(i))-1 then $
        vor(i)=strmid(vor(i),0,strlen(vor(i))-1)
      nach=strmid(str(i),ep(0)+1,strlen(str(i)))
      nach=n2s(fix(nach))
      new(i)='10!U'+nach+'!N'
    endif else new(i)=str(i)
  endfor
  
  if max(float(vor(1:*))) ne 1 or min(float(vor(1:*))) ne 1 then $
    new=vor+'x'+new

  return,new
end

