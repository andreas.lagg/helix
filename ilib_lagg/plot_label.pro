;usage: Initalize with 
;  plot_label,/init
;add labels with commands
;  plot_label,psym=4,color=9,label='v_LOS / cos(INC)'
;  ...
;  plot_label,psym=1,color=1,label='v_LOS'
;draw labels with
;  plot_label,/close,/frame


pro plot_label,init=init,label=label,close=close,position=pos,frame=frame, $
               color=color,thick=thick,linestyle=linestyle,psym=psym, $
               normal=normal,device=device,data=data
  common pl,pl
  
   tpl={color:1,thick:1,psym:0,linestyle:0,label:'label',init:1}
  if keyword_set(init) then begin
    pl=tpl
    return
  end
  if keyword_set(close) then begin
    if n_elements(pos) ne 2 then begin
      pos=[0.6,0.8]
      normal=1
    endif
    if keyword_set(normal) then $
      pos=(convert_coord(/normal,/to_device,pos))(0:1)
    if keyword_set(data) then $
      pos=(convert_coord(/data,/to_device,pos))(0:1)
    
    if !p.charsize eq 0 then !p.charsize=1
    dcx=!d.x_ch_size*!p.charsize
    dcy=!d.y_ch_size*!p.charsize
    xr=(convert_coord(/data,/to_device,!x.crange,[0,0]))
    dx=(xr(0,1)-xr(0,0))*0.075>5
    done=0
    repeat begin
      y=pos(1)
      mxwd=0.
      for i=0,n_elements(pl)-1 do begin
        plots,pos(0)+findgen(4)/4*dx,fltarr(4)+y-dcy*0.7,/device, $
          psym=pl(i).psym,linestyle=pl(i).linestyle,thick=pl(i).thick, $
          color=pl(i).color
        xyouts,pos(0)+dx+dcx*2,y,/device,'!C'+pl(i).label,width=wd
        wd=(convert_coord(/normal,/to_device,[wd,0]))(0)
        mxwd=mxwd>wd
        y=y-dcy*1.2
      endfor
      if keyword_set(frame) and done eq 0 then begin
        frx=[pos(0),pos(0)+dx+dcx*2+mxwd] + [-1,1]*dcx*2
        fry=[pos(1),y]                    + [1,-1]*dcy*1
        box,/device,x=frx,y=fry,background=!p.background
        done=2
      endif else done=1
    endrep until done
    return
  endif
  
  if n_elements(color) ne 0 then tpl.color=color
  if n_elements(psym) ne 0 then tpl.psym=psym
  if n_elements(linestyle) ne 0 then tpl.linestyle=linestyle
  if n_elements(thick) ne 0 then tpl.thick=thick
  if n_elements(label) ne 0 then tpl.label=label
  
  if pl(0).init eq 1 then pl=tpl else pl=[pl,tpl]
  pl.init=0
  
end
