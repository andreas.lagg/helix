pro showct
  tvlct,red,green,blue,/get
  
  if !d.name ne 'PS' then if !d.window eq -1 then window,0
  for i=0,n_elements(red)-1 do begin
    ix=i mod 16 & iy=15-i/16
    xs=!d.x_size/16 & ys=!d.y_size/16
    if (!d.flags and 1) ne 0 then begin ;scalable pixels ?
      tv,bytarr(2,2)+i,ix*xs,iy*ys,xsize=xs,ysize=ys
    endif else begin
      tv,bytarr(xs,ys)+i,ix*xs,iy*ys
    endelse
    xyouts,/device,ix*xs,(iy+1)*ys,'!C '+n2s(i)
  endfor      
end

