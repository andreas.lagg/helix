;find file in a path list, or using wildcards
;example:
;file=find_file_inpath(file='profile_archive/obs.004cc',path=['*/'])
;looks for the file 'obs.004cc' in profile_archive/ and all
;subdirectories in profile_archive/

function find_file_inpath,path=path,file=file,verbose=verbose,error=error
  
  error=0
  if n_elementS(verbose) eq 0 then verbose=1
  
  fpath=strmid(file,0,strpos(file,'/',/reverse_search)+1)
  froot=strmid(file,strpos(file,'/',/reverse_search)+1,strlen(file))
  spath=fpath
  abspath=0
  repeat begin    
  for i=0,n_elements(path)-1 do begin
    if abspath eq 0 then fs=file_search(fpath+path(i),count=cnt) $
      else  fs=file_search(path(i),count=cnt)
    if cnt gt 0 then spath=[spath,fs+'/']
  endfor
  abspath=abspath+1
  endrep until abspath eq 2
  ok=0
  
  i=0
  dir='./'
  repeat begin
    ftest=spath(i)+froot
    fd=file_info(spath(i))
    if fd.directory then dir=[dir,spath(i)]
    fi=file_info(ftest)
    if fi.exists eq 1 and fi.regular eq 1 and fi.read eq 1 then begin
      fret=ftest 
      ok=1
    endif else begin
      i=i+1      
    endelse
  endrep until ok or i ge n_elements(spath)
  
  if ok eq 0 then begin
    if verbose then begin
      message,/cont,'Could not find '+froot
      print,'in directories '+add_comma(dir,sep=';')
    endif
    fret=''
    error=1
  endif
  
  return,fret
end
