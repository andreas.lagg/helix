;displays the information from an alignment procedure
pro align_show,sav,ps=ps,top=top,bottom=bottom,title=title
  
  restore,/v,sav
  
  psset,ps=ps,file=strmid(sav,0,strlen(sav)-4)+'.ps',size=[18,26],/no_x,win_nr=2
  userlct,/full,coltab=0,/reverse
  
  nfiflag=n_elements(nfi) eq 1
  
  if keyword_set(top) or keyword_set(bottom) then !p.multi=0 $
  else !p.multi=[0,1,2+nfiflag]
  
  
  
                                ;display TIP image with Hinode Box
  himg=trans.hinimg
  timg=trans.tipimg
  if trans.tipflip then timg=reverse(timg,2)
  if trans.tipmirror then timg=reverse(timg,1)
  if trans.tiprotate then timg=rotate(timg,1)
  szt=size(timg)
  szh=size(himg)
  if nfiflag then begin
    nimg=nfi.data
    szn=size(nimg)
  endif
  
                                 ;I do not know why but I have to add
                                ;the y-shift to x and vice versa! strange.
  xtrans=([0.,0,szh(1),szh(1)]);/trans.xscale-trans.xoff
  ytrans=([0.,szh(2),szh(2),0]);/trans.yscale-trans.yoff
  xxyy=[[xtrans],[ytrans]]
  tr=trans.rot
  cp=cos(tr*!dtor) & sp=sin(tr*!dtor)
  rmat=[[cp,sp],[-sp,cp]]
  for i=0,3 do xxyy(i,*)=rmat ## xxyy(i,*)
;   xxyy(*,0)=xxyy(*,0)/trans.xscale-trans.xoff
;   xxyy(*,1)=xxyy(*,1)/trans.yscale-trans.yoff
  xxyy(*,0)=(xxyy(*,0)-trans.xoff)/trans.xscale
  xxyy(*,1)=(xxyy(*,1)-trans.yoff)/trans.yscale
  
  if n_elements(title) eq 0 then tit=trans.tipnam else tit=title
  if keyword_set(bottom) eq 0 then begin
    image_cont_al,/aspect,contour=0,zrange=minmaxp(timg,perc=99.5), $
      timg,title=tit,xtitle='TIP x-pixel',ytitle='TIP y-pixel'
    load16
    plots,[xxyy(*,0),xxyy(0,0)],[xxyy(*,1),xxyy(0,1)],color=!col.red
    xyouts,/data,xxyy(1,0),xxyy(1,1),'!C  Hinode',color=!col.red
    load16,/reset
  endif
                                ;now do the same, for Hinode
  xtrans=([0.,0,szt(1),szt(1)])*trans.xscale+trans.xoff
  ytrans=([0.,szt(2),szt(2),0])*trans.yscale+trans.yoff
  xxyy=[[xtrans],[ytrans]]
  cp=cos(-trans.rot*!dtor) & sp=sin(-trans.rot*!dtor)
  rmat=[[cp,sp],[-sp,cp]]
  for i=0,3 do xxyy(i,*)=rmat ## xxyy(i,*)
  
  tiptrans=img_tip2hin(trans.tipimg,sav)
  sztt=size(tiptrans)
  xtip=indgen(sztt(1))+trans.xoff
  ytip=indgen(sztt(2))+trans.yoff
  mima=max(abs(minmaxp(tiptrans,perc=99)))*[-1,1]
  nlev=6 & levels=(findgen(nlev)/(nlev-1))*(mima(1)-mima(0))+mima(0)
  
  if n_elements(title) eq 0 then tit=trans.hinnam else tit=title
  if keyword_set(top) eq 0 then begin
    image_cont_al,/aspect,contour=0,zrange=minmaxp(himg,perc=99.5), $
      himg,title=tit,xtitle='SOT x-pixel',ytitle='SOT y-pixel'
    
    load16
    plots,[xxyy(*,0),xxyy(0,0)],[xxyy(*,1),xxyy(0,1)],color=!col.red
    xyouts,/data,xxyy(1,0),xxyy(1,1),'!C  TIP',color=!col.red
    load16,/reset
    load16
    contour,tiptrans,xtip,ytip,/overplot,levels=levels,color=!col.blue,noclip=1
    load16,/reset
  endif
  
  if nfiflag then begin
    nimg=nfi.data
    szn=size(nimg)
    xnfia=(intarr(nfi.header.naxis2)+1) ## findgen(nfi.header.naxis1)
    ynfia=findgen(nfi.header.naxis2) ## (intarr(nfi.header.naxis1)+1)
    image_cont_al,/aspect,contour=0,zrange=minmaxp(nimg,perc=99.5),nimg, $
;                  xrange=[100,900],yrange=[200,800],xnfia,ynfia, $
                  xtitle='NFI x-pixel',ytitle='NFI y-pixel',title=tit
    ;; xbfi=findgen(bfi.header.naxis1)
    ;; ybfi=findgen(bfi.header.naxis2)
    ;; xb2nfi=(xbfi-bfi.header.crpix1)/nfi.header.xscale*bfi.header.xscale + $
    ;;      nfi.header.crpix1
    ;; yb2nfi=(ybfi-bfi.header.crpix2)/nfi.header.yscale*bfi.header.yscale + $
    ;;      nfi.header.crpix2-15
    xbfi=findgen(bfi.header.naxis1)
    ybfi=findgen(bfi.header.naxis2)
    xb2nfi=(xbfi-bfi.header.crpix1)/nfi.header.xscale*bfi.header.xscale + $
         nfi.header.crpix1
    yb2nfi=(ybfi-bfi.header.crpix2)/nfi.header.yscale*bfi.header.yscale + $
         nfi.header.crpix2
    load16
    mima=max(abs(minmaxp(timg,perc=99)))*[-1,1]
    nlev=4 & levels=(findgen(nlev)/(nlev-1))*(mima(1)-mima(0))+mima(0)
    contour,timg,xb2nfi,yb2nfi,/xst,/yst,/overplot,levels=levels, $
            color=!col.blue,noclip=1
    load16,/reset
    
    blink=1
    if blink then begin
    tshift=congrid(timg,bfi.header.naxis1/nfi.header.xscale*bfi.header.xscale,$
                   bfi.header.naxis2/nfi.header.yscale*bfi.header.yscale)
    window,xsize=nfi.header.naxis1,ysize=nfi.header.naxis2
    !p.multi=0 & !p.position=0
    repeat begin
      tvscl,nimg & wait,.5
      tvscl,tshift, $
            (-bfi.header.crpix1)/nfi.header.xscale*bfi.header.xscale+nfi.header.crpix1, $
            (-bfi.header.crpix2)/nfi.header.yscale*bfi.header.yscale+nfi.header.crpix2            & wait,.5
    endrep until 1 eq 0
  endif
      
  endif
  
  
  psset,/close
  stop
  print,'Instructions to use the transformation:'
  print
  print,'(1) Create a plot in SOT coordinates'
  print,'(2) Use img_tip2hin.pro to create the transformed TIP image:'
  print,'    tiptrans=img_tip2hin(tipimg,'''+sav+''')'
  print,'    This transformed TIP image has the same pixel scale as the Hinode image'
  print,'(3) To display the TIP data apply the offset [trans.xoff,trans.yoff] '
  print,'    before plotting (see contour example in this routine). These'
  print,'    offsets are in units of SOT pixels.'
  
  if nfiflag then begin
    print
    print,'(4) To align NFI and BFI see the ''blink'' example in this routine'
    print,'    or the image / contour overplot.'
    print,'    Note: BFI/NFI alignment is based on FITS header values. This'
    print,'    can contain an error of approx 2 arcsec.'
    print,'    Alignment offsets here:'
    print,'    http://solar-b.nao.ac.jp/hinode_wiki/'+ $
          'index.php?FG%20pixel%20offset%20and%20scale'
    
  endif
  
end
