pro spawn_edit,file,_extra=_extra
  
  case strlowcase(!version.os_family) of
    'unix': begin
      spawn,'emacs '+file+' &'
    end
    'windows': begin
      message,/cont,'EDIT spawn not enabled for Windows.'
      message,/cont,'Use your Windows Editor to open '+file
    end
    else: message,'Unsupported OS: '+!version.os
  endcase
end
