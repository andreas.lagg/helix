;get QS continuum level:
;identify most quiet region and then take average over an area 1/10 of
;the image size
function get_qscont,data
  
  totsig=total(abs(data),1)
  maxi=reform(max(data(*,0,*,*),dim=1))
  lp=reform(sqrt(totsig(1,*,*)^2+totsig(2,*,*)^2+totsig(3,*,*)^2))/maxi
  
  
  smlp=smooth(lp,7,/edge)
  minpol=min(smlp,idx)
  ixy=array_indices(smlp,idx)
  
  sz=size(maxi)
  nx=sz(1) & ny=sz(2)
  xbox=0>(ixy(0)+[-nx,nx]*0.05)<(nx-1)
  ybox=0>(ixy(1)+[-ny,ny]*0.05)<(ny-1)
  
  icont=mean(maxi(xbox(0):xbox(1),ybox(0):ybox(1)))
  return,icont
end
