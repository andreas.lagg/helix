;+
; NAME:
;    find_xpos
;
;
; PURPOSE:
;    finds x-position to a corresponding x-value
;
;
; CATEGORY:
;    processing EPD-Data
;
;
; CALLING SEQUENCE:
;    yval=find_xpos(xvar,xall)
;
; 
; INPUTS:
;    xvar - variable containing the x values for which the positions
;           are desired
;    xall - variable containing all x-values
;
; OPTIONAL INPUTS:
;
;
;	
; KEYWORD PARAMETERS:
;
;
;
; OUTPUTS:
;    position of the xvar-values
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;    A. Lagg, 24/05/1996
;-

function find_xpos,xvar,xall,margin=margin

  n_all=n_elements(xall)
  n_var=n_elements(xvar)
;  pos=lonarr(n_var)
  pos=long(xvar*0l)
  
  i=0l
  repeat begin
    if xall(0) le xall(n_all-1) then mp=max(where(xall le xvar(i)+1e-4)) $
    else mp=max(where(xall ge xvar(i)-1e-4))
    if not(keyword_set(margin)) then if mp eq n_all-1 then mp=-1
    pos(i)=mp
    i=i+1l
  endrep until i ge n_var

  if n_elements(pos) ne n_elements(xvar) then $
    message,/traceback,'xvar exceeds range of xall'
  return,pos
end

