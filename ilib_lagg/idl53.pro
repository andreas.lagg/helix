;set path for obsolete routines
pro idl53,prog_dir=prog_dir

  if n_elements(prog_dir) eq 0 then prog_dir='.'
  version=float(!version.release)
  case 1 of
    version ge 5.3: begin
      case strlowcase(!version.os_family) of
        'unix': add_path=prog_dir+'/obsolete:'
        'windows': add_path=prog_dir+'\obsolete;'
        else: message,'unknown OS-family: '+!version.os_family
      endcase
    end
    else: add_path=''
  endcase

  if strpos(!path,add_path) eq -1 then !path=add_path+!path
end
