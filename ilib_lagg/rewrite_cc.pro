;opens fits file and writes it to temp file. If successfull the
;original file is overwritten if /replace is set
pro rewrite_cc,ccfile,replace=replace,verbose=verbose,ccorrfunc=ccorrfunc, $
               error=error
  
  error=1
  if n_elements(verbose) eq 0 then verbose=1
  
  
                                ;check if file is really a cc-file
  iscc=strmid(ccfile,strlen(ccfile)-2,2) eq 'cc'
  if iscc eq 0 then begin
    message,/cont,ccfile+' is no cc-file'
    return
  endif
  
  tmp=ccfile+'.tmp'
  
                                ;open original file
  if verbose ge 1 then print,'Opening FITS for read: '+ccfile
  dum=rfits_im(ccfile,1,dd,hdr,nrhdr)
  if verbose ge 1 then print,'Replacing header in: '+ccfile
  hdr=replace_header(hdr)

  if n_elements(ccorrfunc) ne dd.naxis1 then begin
    message,/cont,'Incompatible ccorrfunc'
    return
  endif
  if total(abs(ccorrfunc-1.)) le 1e-6 then begin
    message,/cont,'ccorrfunc is unity'
    res=wg_message(['Continue with Continuum Correction unity?', $
                    'This might be okay if you just want to',  $
                    're-write an already reduced cc file.'], $
                   title='Cont.Corr is Unity',/yes,/no)
    if res ne 'yes' then  return
  endif

  openr,unit,ccfile,/get_lun,error=err
  if err ne 0 then begin
    message,/cont,'Error opening cc-file for read: '+ccfile
    return
  endif
  if(dd.bitpix eq 8) then begin
    datos=assoc(unit,bytarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
  endif else if(dd.bitpix eq 16) then begin   
    datos=assoc(unit,intarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
  endif else if(dd.bitpix eq 32) then begin   
    datos=assoc(unit,lonarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
  endif
  npos=dd.naxis3/4
  
  
  
                                ;open tmp file
  if verbose ge 1 then print,'Opening FITS for write: '+tmp
  openw,unit_out,tmp,/get_lun,error=err
  if err ne 0 then begin
    message,/cont,'Error opening cc-file for write: '+tmp
    free_lun,unit
    return
  endif
  res=wg_message(['Do you really want to replace the file ',ccfile], $
                 title='Replace CC',/yes,/no)
  if res ne 'yes' then begin
    free_lun,unit
    free_lun,unit_out
    return
    xy_fits,ccfile,/close
  endif
  
  writeu,unit_out,byte(hdr)
  if(dd.bitpix eq 8) then begin
    dat_out=assoc(unit_out,bytarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
  endif else if(dd.bitpix eq 16) then begin   
    dat_out=assoc(unit_out,intarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
  endif else if(dd.bitpix eq 32) then begin   
    dat_out=assoc(unit_out,lonarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
  endif
  format=['(i2,$)','(i3,$)','(i4,$)','(i5,$)','(i6,$)']
  
  ccorrarr=(fltarr(dd.naxis2)+1) ## ccorrfunc

  for j=0,npos-1 do begin
    if((j+1)/10*10 eq j+1) then print,j+1,format=format(fix(alog10(j+1)))
    imi=rfits_im2(datos,dd,4*j+1)
    imq=rfits_im2(datos,dd,4*j+2)
    imu=rfits_im2(datos,dd,4*j+3)
    imv=rfits_im2(datos,dd,4*j+4)
    
                                ;changes to file
    imi=imi/ccorrarr
    imq=imq/ccorrarr
    imu=imu/ccorrarr
    imv=imv/ccorrarr
    
    if (!version.arch eq "alpha" or $
        strmid(!version.arch,0,3) eq "x86") then begin
      if(dd.bitpix eq 8) then begin
        dat_out(4*j)=byte(imi)
        dat_out(4*j+1)=byte(imq)
        dat_out(4*j+2)=byte(imu)
        dat_out(4*j+3)=byte(imv)
      endif else if(dd.bitpix eq 16) then begin
        dum = fix(imi)
        byteorder,dum
        dat_out(4*j)=dum
        dum = fix(imq)
        byteorder,dum
        dat_out(4*j+1)=dum
        dum = fix(imu)
        byteorder,dum
        dat_out(4*j+2)=dum
        dum = fix(imv)
        byteorder,dum
        dat_out(4*j+3)=dum
      endif else if(dd.bitpix eq 32) then begin   
        dum = long(imi)
        byteorder,dum,/lswap
        dat_out(4*j)=dum
        dum = long(imq)
        byteorder,dum,/lswap
        dat_out(4*j+1)=dum
        dum = long(imu)
        byteorder,dum,/lswap
        dat_out(4*j+2)=dum
        dum = long(imv)
        byteorder,dum,/lswap
        dat_out(4*j+3)=dum
      endif
    endif else begin
      if(dd.bitpix eq 8) then begin
        dat_out(4*j)=byte(imi)
        dat_out(4*j+1)=byte(imq)
        dat_out(4*j+2)=byte(imu)
        dat_out(4*j+3)=byte(imv)
      endif else if(dd.bitpix eq 16) then begin   
        dat_out(4*j)=fix(imi)
        dat_out(4*j+1)=fix(imq)
        dat_out(4*j+2)=fix(imu)
        dat_out(4*j+3)=fix(imv)
      endif else if(dd.bitpix eq 32) then begin   
        dat_out(4*j)=long(imi)
        dat_out(4*j+1)=long(imq)
        dat_out(4*j+2)=long(imu)
        dat_out(4*j+3)=long(imv)
      endif
    endelse
  endfor   
  print,' '
  free_lun,unit
  free_lun,unit_out
  
  if keyword_set(replace) then begin
    print,'Replacing '+ccfile
    spawn,'mv '+tmp+' '+ccfile
    create_m,strmid(ccfile,0,strlen(ccfile)-1)
    error=0
  endif
end
