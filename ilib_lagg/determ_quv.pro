function determ_quv,obs,index=nn
  
  if n_elements(nn) eq 0 then nn=indgen(n_elements(obs.v))
  nel=n_elements(nn)
  qmed=total(obs.q(nn))/nel
  umed=total(obs.u(nn))/nel
  vmed=total(obs.v(nn))/nel
  quvn=sqrt((obs.q(nn)-qmed)^2+(obs.u(nn)-umed)^2+(obs.v(nn)-vmed)^2)
                                ;take average of 10 highest values
  quv=total((quvn(reverse(sort(quvn))))(0:3<(n_elements(quvn)-1)))/ $
    (4<(n_elements(quvn)-1))
  return,quv
end
