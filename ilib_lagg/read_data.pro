pro read_data,dir=data_dir,mask=mask,wl=wl,format=format
  common data,data
  
  
  
  
                                ;check if data are present
  ffits=file_search(data_dir+'/'+mask,count=fcnt)
  if fcnt eq 0 then begin
    message,/cont,'No FITS file found: '+data_dir+'/'+mask
    retall
  endif else print,'Found '+n2s(fcnt)+' FITS-files.'
  
                                ;check if data is Hinode or TIP
  if n_elements(format) eq 1 then begin
    mode=strlowcase(format)
    print,'Use '+mode+' format.'
  endif else begin
  if strpos(ffits(0),'SP3D') ne -1 then mode='hinode' $
  else if strpos(ffits(0),'SP4D') ne -1 then mode='hinode' $
  else if strpos(ffits(0),'4d.fits') ne -1 then mode='4d' $
  else if check_tipmask(ffits(0),obscore=tipcore) then mode='tip' $
  else begin
    message,/cont,'Could not determine data format. Specify the format using the keyword fomat=''4d'' (other formats are ''hinode'',''tip'')'
    print,'TIP-mode:' 
    print,'data_dir=''/data/gbso/VTT-data/VTT-Apr07/01May07/'',mask=''01may07.007*cc'''
    print,'Hinode-mode:'
    print,'~/tmp/hinode/sot/level0/2007/05/01/SP4D/H1000/CAL/'',mask=''SP3D20070501_120*.fits'''
    retall
  endelse
  endelse
  if n_elements(data) ne 0 then dummy=temporary(data)
  
  case mode of
    'hinode': begin
      print,'Data format: Hinode'
      for i=0,fcnt-1 do begin
        dat=readfits_ssw(ffits(i),hdr,/silent)
        dat=hinode_corr(dat,hdr)
        hst=fits_header2st(hdr)      
        if i eq 0 then begin
          sz=size(dat)
          nwl=sz(1)
          ny=sz(2)
          nx=fcnt
          wlvec=dblarr(nx,nwl)
          icont=fltarr(nx,ny)
          data={dir:data_dir,sp:lonarr(nwl,ny,4,nx), $
                header:replicate(hst,fcnt),format:mode,$
                fits:ffits,nfits:fcnt,mask:mask,nwl:nwl,nx:nx,ny:ny, $
                slitindx:intarr(2)+hst.slitindx,$
                wl:temporary(wlvec),icont:temporary(icont),name:'', $
                imgcont:0.}
        endif
;        data.header(i)=hst
        tmp=data.header(i)
        struct_assign,hst,tmp
        data.header(i)=tmp
        data.slitindx=[min([hst.slitindx,data.slitindx]), $
                       max([hst.slitindx,data.slitindx])]
        
                                ;check if ccx is present
        cx=ffits(i)+'.ccx'
        fi=file_info(cx)
        if fi.exists then begin
          read_ccx,cx,icont=icimg,error=xerr,wl_vec=wl,verbose=0
          data.icont(i,*)=icimg
          data.wl(i,*)=wl
        endif else begin
          message,/cont,'No ccx-file found for '+ffits(i)
          data.icont(i,*)=1.
          data.wl(i,*)=findgen(nwl)
        endelse
        data.sp(*,*,*,i)=dat
        data.imgcont=get_histocont(data.icont)

                                ;norm to icont
;      for is=0,3 do data.sp(*,*,is,i)= $
;        data.sp(*,*,is,i)/(reform(data.icont(i,*)) ## (fltarr(nwl)+1.))
        
        if fix(i*80./fcnt) ne fix((i-1.)*80./fcnt) then print,'.',format='(a,$)'
      endfor
      print
      
      data.name='SOTSP_'+strmid(data.header(0).date_obs,0,10)+ $
                'T'+strmid(data.header(0).date_obs,11,2)+ $
                strmid(data.header(0).date_obs,14,2)+'-'+ $
                strmid(data.header(data.nx-1).date_obs,11,2)+ $
                strmid(data.header(data.nx-1).date_obs,14,2)
    end
    'tip': begin
      print,'Data format: TIP/GRIS'
                                ;estimate size:
      xy_fits,data_dir+'/'+mask,struct=tst
      nx=tst(0).nxtot
      ny=tst(0).ny
      nwl=tst(0).nbin
                                ;if too big reduce WL 
      mem=nx*ny*nwl/1.e6*4.*4.
      if mem ge 768.*10 then begin
        nnew=fix(nwl*768.*10/mem)
        bin=fix(findgen(nnew)/nnew*nwl)
        nwl=n_elements(bin)
        print,'Reducing # of WL-bins from '+n2s(tst(0).nbin)+' to '+n2s(nwl)+' to conserve memory.'
      endif else bin=-1
      xy_fits,data_dir+'/'+mask,i=imap,u=umap,q=qmap,v=vmap,bin=bin

      read_ccx,data_dir+'/'+mask+'x',wl_disp=wl_disp,norm_cont=norm_cont, $
               wl_off=wl_off,icont=icont,error=xerr,wl_vec=wl_vec
                                ;check for WL information
      mrd_head,tst[0].file[0],hdr
      ff1wloff=sxpar(hdr,'FF1WLOFF')
      ff1wldsp=sxpar(hdr,'FF1WLDSP')
      ff2wloff=sxpar(hdr,'FF2WLOFF')
      ff2wldsp=sxpar(hdr,'FF2WLDSP')
      ffwl=0b
      if ff1wloff ge 1. then begin
        wloff=ff1wloff
        wldisp=ff1wldsp
        ffwl=1b
      endif
      if ff2wloff ge 1. then begin
        wloff=(ff1wloff+ff2wloff)/2.
        wldisp=(ff1wldsp+ff2wldsp)/2.
        ffwl=1b
      endif
      if ffwl eq 1 then begin
        wl_vec=dindgen(nwl)*wldisp+wloff
      endif

      
      if n_elements(wl_vec) eq 0 then wl_vec=findgen(nwl) 
      if bin(0) ne -1 then begin
        wl_vec=wl_vec(bin)
      endif
      if n_elements(icont) eq 0 then icont=reform(imap(0,*,*))
      wl_vec=wl_vec ## (fltarr(nx)+1) ;make array similar to hinode data
      sp=[[[temporary(reform(temporary(imap),nwl,nx,1,ny))]], $
          [[temporary(reform(temporary(qmap),nwl,nx,1,ny))]], $
          [[temporary(reform(temporary(umap),nwl,nx,1,ny))]], $
          [[temporary(reform(temporary(vmap),nwl,nx,1,ny))]]]
      sp=temporary(transpose(temporary(sp),[0,3,2,1]))
      imgcont=get_histocont(icont)
      
      data={dir:data_dir,sp:temporary(sp),header:tst.header,format:mode,$
            fits:ffits,nfits:fcnt,mask:mask,nwl:nwl,nx:nx,ny:ny, $
            wl:temporary(wl_vec),icont:temporary(icont),name:'', $
            slitindx:intarr(2),imgcont:imgcont}
      data.name='VTT_'+tipcore
    end
    '4d': begin
      print,'data format is 4D fits file'
      
      dat=readfits_ssw(ffits[0],hdr)
;      Dat=temporary(transpose(dat,[2,3,1,0]))
;      stop
;      dat=temporary(transpose(dat,[0,3,1,2]))
      szd=size(dat,/dim)
      nwl=szd[0]
      nx=szd[2]
      ny=szd[3]
      wl_vec=dindgen(nwl)
      
                                ;check if wl vector is in 1st
                                ;extension
      wl=readfits_ssw(ffits[0],whdr,ext=1)
      if wl[0] ne -1 then wl_vec=wl
      wl_vec=wl_vec ## (fltarr(nx)+1) ;make array similar to hinode data
      
                                ;check if icont is in 2nd extension
      icont=readfits_ssw(ffits[0],ihdr,ext=2)
      if icont[0] ne -1 then begin
        imgcont=sxpar(ihdr,'IC_HSRA')
      endif else begin
;      icont=transpose(minmax(dat[*,0,*,*]*1.,dim=1))
        icont=reform(max(dat[*,0,*,*]*1.,dim=1))
        imgcont=get_histocont(icont)
      endelse
        
      dat=temporary(transpose(dat,[0,3,1,2]))
      data={dir:data_dir,sp:temporary(dat),header:hdr,format:mode,$
            fits:ffits[0],nfits:1,mask:mask,nwl:nwl,nx:nx,ny:ny, $
            wl:temporary(wl_vec),icont:temporary(icont),name:'', $
            slitindx:intarr(2),imgcont:imgcont}
      data.name=ffits[0]


    end
  endcase
end
