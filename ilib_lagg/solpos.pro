;date='17:47 21-dec-09'          ;winter solstice
;  date='06:45 20-mar-09'        ;spring equinox  
  date='08:37 01-jun-09'
  date='10:37 20-mar-09'
  lat=67.+51/60.
  lon=20.+13./60+12./3600.
  lat=0.
  
  warning=''
  utc = anytim2utc(date, /ecs, err=warning)
  if warning ne '' then get_utc, utc, /ecs
  print,'UTC: ',utc  
  
  jd=anytim(date)/86400d + julday(01,01,1979,00,00,00)
  
;  sun_pos, jd-2415020d, longitude, ra, dec, app_long, obliq
  sun_pos, jd-julday(01,01,1900,00,00,00), longitude, ra, dec, app_long, obliq
  print,'longitude, ra, dec, app_long, obliq'
  print,longitude, ra, dec, app_long, obliq
  
  eq2hor, ra, dec, jd, alt, az,ha,lat=lat,lon=lon,altitude=0,/verb
  print,'alt,az,ha'
  print,alt,az,ha
  
                                ;now get p angle  
  pb0r=pb0r(date,/arcsec,/earth)
  print,'p,b0,r'
  print,pb0r
  
  
  print,'position angle of solar north pole: ',pb0r[0]+(alt+(90-lat))*cos(az*!dtor)
  
  
  
;   alt=asin(sin(lat*!dtor)*sin(dec*!dtor)+cos(lat*!dtor)*cos(dec*!dtor)*cos(HA*!dtor))/!dtor
;   AZ=atan(cos(lat*!dtor)*sin(dec*!dtor)-sin(lat*!dtor)*cos(dec*!dtor)*cos(HA*!dtor),-cos(dec*!dtor)*sin(HA*!dtor))/!dtor
  stop
end
