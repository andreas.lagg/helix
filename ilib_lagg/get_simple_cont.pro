;calculate continuum level
function get_simple_cont,profile
  
  bdry=(n_elements(profile)*0.05)>5
  allidx=lindgen(n_elements(profile))
  goodidx=where(allidx gt bdry and allidx lt n_elements(allidx)-bdry)
  if n_elements(goodidx) gt 5 and goodidx(0) ne -1 then idx=goodidx $
  else idx=allidx
  if total(finite(profile(idx))) ge 5 then $
    c=max(median((profile(idx))(*),3),/nan) $
  else c=max(profile(idx),/nan)
  return,c
end

