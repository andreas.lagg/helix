;create spinor 4D fitscube from MURAM sav files
pro muram2spinor,xregion=xr,yregion=yr,cubesize=cubesize,type=type, $
                 numberstr=numberstr,dir=dir,outputdir=outputdir, $
                 gridxy=gridxy,gridd=gridd,maxtemp=maxtemp
  
  if float(!version.release) lt 8 then begin
  fi=file_search(strsplit(!dlm_path,':',/extract)+'/libfits.dlm',count=cnt)
  if cnt eq 0 then begin
    print,'DLM_PATH needs to be defined for FITS write function:'
    print,'  export IDL_DLM_PATH=~/idl/dlm-x86_64:$IDL_DLM_PATH'
    return
  endif
  DLM_LOAD,'fits'
  endif
  
 
  
  showhelp=1
  if (n_elements(cubesize) eq 3 and n_elements(type) eq 1 and $
      n_elements(gridxy) eq 1 and n_elements(gridd) eq 1) then showhelp=0
  if showhelp then begin
    print,"The CUBE format in spinor requires a 4D cube with dimensions [nx,ny,ndepth,npar]. The parameters are (in that order): temperature, density, pressure, vx, vy, vz, bx, by, bz (with z being the depth dimension). The units are CGS."
    print
    
    print,'Usage:'
    print,'muram2spinor,cubesize=[1536,128,1536],type=''eos'',numberstr=''001000'',dir=''/home/lagg/data/MuRAM/rempel_qs_dynamo/'',gridxy=16.,gridd=16. [,outputdir=''./'',xregion=[0,50],yregion=[0,20] ]'
    retall
  endif
  szf=cubesize
  
  if n_elements(dir) eq 0 then dir='./'
  if n_elements(outputdir) eq 0 then outputdir=dir
  if strpos(dir,'~') ne -1 or strpos(outputdir,'~') ne -1 then $
    message,'Replace ''~'' in your path with full path.'

  gridxy=float(gridxy)
  gridd=float(gridd)
                                ;limit the output cube to the z-layers
                                ;with average temperature less than maxtemp
  if n_elements(maxtemp) eq 0 then maxtemp=20000.
  print,'Maxtemp: ',maxtemp
  print,'Gridsize xy=',gridxy,', depth=',gridd
  
  print,'Cubesize: ',szf
  
  case strupcase(type) of 
    'T': begin
      vars=['t','d','p','vx','vz','vy','bx','bz','by']
      sep='_'
      ext='.float'
      normmode=0
    end
    'EOS': begin
      vars=['eosT','result_0','eosP', $ ;t,dens,p
            'result_1','result_2','result_3', $ ;vx,vy,vz
            'result_5','result_6','result_7'] ;bx,by,bz
      vars=['eosT_med','result_prim_med_0','eosP_med', $ ;t,dens,p
            'result_prim_med_1','result_prim_med_2','result_prim_med_3', $ ;vx,vy,vz
            'result_prim_med_5','result_prim_med_6','result_prim_med_7'] ;bx,by,bz
      sep='.'
      ext=''
      normmode=2
    end
    'EOS.FITS': begin
      vars=['eosT','result_0','eosP', $ ;t,dens,p
            'result_1','result_2','result_3', $ ;vx,vy,vz
            'result_5','result_6','result_7'] ;bx,by,bz
      sep='.'
      ext='.fits'
      normmode=1
    end
    'TEMP': begin
      vars=['temp','dens','pres','velx','velz','vely','magx','magz','magy'] 
      sep='_'
      ext='.float'    
      normmode=0
    end
    else: begin
      print,'Unknown type. Known types are ''t'',''eos'',''eos.fits'',''temp'''
      retall
    end
  endcase
  filenames=vars+sep+numberstr+ext
  print,'Looking for files in directory: ',dir
  print,filenames+', '
  for i=0,n_elements(vars)-1 do begin
    dat=fltarr(szf[0],szf[1],szf[2])
    file=dir+filenames[i]
    print,'Reading '+file
    if strpos(strupcase(type),'.FITS') eq -1 then begin
      openr,unit,/get_lun,file
      readu,unit,dat
      free_lun,unit
    endif else begin
      dat=read_fits_dlm(file,0,hdr)
    endelse
    dat=transpose(temporary(dat),[1,2,0])
    if i eq 0 then begin        ;

      
      sz=size(dat)
      
                                ; temp: check maxtemp
      avgtemp=total(total(dat,2),2)/sz[2]/sz[3]
      dummy=min(abs(avgtemp-maxtemp),iz0)
      dat=dat[iz0:*,*,*]
      sz=size(dat)
      
      if n_elements(xr) ne 2 then xr=[0,sz(3)-1]
      if n_elements(yr) ne 2 then yr=[0,sz(2)-1]
      
      nx=xr(1)-xr(0)+1
      ny=yr(1)-yr(0)+1
      nz=sz(1)
      npar=9
      mhdcube=fltarr(npar,nz,ny,nx)
      print,'Size of output cube: ',size(mhdcube)
    endif else dat=dat[iz0:*,*,*]
    mhdcube(i,*,*,*)=dat(*,yr(0):yr(1),xr(0):xr(1))
    dat=0 & dummy=temporary(dat)
  endfor
  
  add=''
  if nx ne sz(3) then add=add+'.x'+strjoin(string(xr,format='(i4.4)'),'-')
  if ny ne sz(2) then add=add+'.y'+strjoin(string(yr,format='(i4.4)'),'-')
  mhdfile=outputdir+'/mhd_atmos'+add+'.'+numberstr+'.fits'
  
  case normmode of
    0: begin                    ;do nothing
      print,'No normalization of values done! All values in the cubes are assumed to be in CGS'
    end
    1: begin
      print,'Conversion to CGS: velocity-variable/dens, bfieldvar*sqrt(4pi)'
;      mhdcube[3:5,*,*,*]=mhdcube[3:5,*,*,*]*1e5 velocity in cm/s
                                ;velocity in density units
      mhdcube[3,*,*,*]=mhdcube[3,*,*,*]/mhdcube[1,*,*,*]
      mhdcube[4,*,*,*]=mhdcube[4,*,*,*]/mhdcube[1,*,*,*]
      mhdcube[5,*,*,*]=mhdcube[5,*,*,*]/mhdcube[1,*,*,*]
                                ;bfield
      mhdcube[6,*,*,*]=mhdcube[6,*,*,*]*sqrt(4*!pi)
      mhdcube[7,*,*,*]=mhdcube[7,*,*,*]*sqrt(4*!pi)
      mhdcube[8,*,*,*]=mhdcube[8,*,*,*]*sqrt(4*!pi)
    end
    2: begin
      print,'Conversion to CGS: velocity-variable/dens, bfieldvar*sqrt(4pi)'
;      mhdcube[3:5,*,*,*]=mhdcube[3:5,*,*,*]*1e5 velocity in cm/s
                                ;velocity in density units
      mhdcube[3,*,*,*]=mhdcube[3,*,*,*]*mhdcube[1,*,*,*]
      mhdcube[4,*,*,*]=mhdcube[4,*,*,*]*mhdcube[1,*,*,*]
      mhdcube[5,*,*,*]=mhdcube[5,*,*,*]*mhdcube[1,*,*,*]
    end
  endcase
  
  
  mkhdr,header,mhdcube
  sxaddpar,header,'COMMENT','MuRAM 4D cube for SPINOR'
  sxaddpar,header,'COMMENT','Created from directory '+dir
  sxaddpar,header,'COMMENT','Cube dimensions: NPAR x NDEPTH x NY x NX'
  fxaddpar,header,'PAR1','TEMP','temperature [K]'
  fxaddpar,header,'PAR2','DENS','density [g/cm^3]'
  fxaddpar,header,'PAR3','PRESS','pressure  [dyn/cm^2]'
  fxaddpar,header,'PAR4','VX','x-velocity [cm/s]'
  fxaddpar,header,'PAR5','VY','y-velocity [cm/s]'
  fxaddpar,header,'PAR6','VZ','z-velocity [cm/s] (=vertical comp.)'
  fxaddpar,header,'PAR7','BX','B-Field X [G]'
  fxaddpar,header,'PAR8','BY','B-Field Y [G]'
  fxaddpar,header,'PAR9','BZ','B-Field Z [G] (=vertical comp.)'
  fxaddpar,header,'T_X',float(gridxy*1e5*nx),'x total size [cm]'
  ;; if order eq 'xyz' then begin
  ;;   fxaddpar,header,'T_Y',float(gridxy*1e5*ny),'y total size [cm]'
  ;;   fxaddpar,header,'T_Z',float(gridd*1e5*nz),'total depth [cm]'
  ;; endif else begin
    fxaddpar,header,'T_Z',float(gridxy*1e5*(ny)),'y total size [cm] (must be T_Z!)'
    fxaddpar,header,'T_Y',float(gridd*1e5*(nz-1)),'total depth [cm] (must be T_Y!)'
;  endelse
    
  
    
  
  hdr=header
;  for i=0,n_elements(header)-1 do hdr[i]=trim(header[i])
  iend=where(strpos(hdr,'END') eq 0)
  header=hdr[0:iend]
  
;  mwrfits,mhdcube,mhdfile,header,/create
  ftswr,mhdcube,mhdfile,header,/create
  print,'Wrote 4D-FITS cube with MHD data: ',mhdfile
  
  print,'WARNING: Check the units of B. Is it Gauss? Check the MHD CUBE module in atmos.f90 if a division by sqrt(4*pi) is necessary!'
  
  stop
end
