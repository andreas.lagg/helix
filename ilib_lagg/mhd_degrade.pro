;PF and etalon simulation, based on Johanns filter_simulations_double_up.pro
;A. Lagg, Aug 09 2011

;read computed prefilter curces of Stefan Meining
function read_pf_ascii,file,wl
  
  if file eq '' then return,0.
  openr,/get_lun,unit,file
  i=0l
  repeat begin
    line=''
    readf,unit,line
                                ;replace ',' by '.'
    bline=byte(line)
    kpos=where(bline eq (byte(','))(0))
    if kpos(0) ne -1 then bline(kpos)=(byte('.'))(0)
    line=string(bline)
    line=strtrim(strcompress(line),2)
    if i ge 1 and n_elements(strsplit(line)) eq 4 then begin ;skip header
      d=dblarr(4)
      reads,line,d
      if i eq 1 then arr=transpose(d) else arr=[arr,[transpose(d)]]
    endif
    i=i+1
  endrep until eof(unit)
  free_lun,unit
  
                                ;Stefan Meinings prefilter curves are
                                ;not centered at reference wavelength.
  offset=6173.3412d - 6174.0d
                                ;convert nm to \AA
  arr(*,0)=arr(*,0)*10d + offset
  arr(*,2)=arr(*,2)*10d + offset
  
  
                                ;convert transmission from % to 1
  arr(*,1)=arr(*,1)/100d
  arr(*,3)=arr(*,3)/100d
  
  prefilter=interpol(arr(*,1),arr(*,0),wl)
  outside=where(wl lt min(arr(*,0)) or wl gt max(arr(*,0)))
  if outside(0) ne -1 then prefilter(outside)=0.

  return,prefilter
end

;predefined parameters
pro loadpar,load,g15=g15,mhdwlvec=mhdwlvec,mhdwlref=mhdwlref
  common imax_par,imax
  
;define default values  
  pf_fwhm=10.         ;define FWHM of prefilter
  ncav=2              ;cavities for prefilter
  netalon=2           ;number of etalons (max 2)
  velo=0.             ;wl-shift in km/s
  bin=1               ;pixel binning
  finesse=50.         ;define finesse and
  FSR=5.0             ;FSR of etalon Angstrom
  load_pf=''          ;load prefilter curve from file (Stefan Meining)
  load_etalon='/data/slam/home/lagg/data/Sunrise/PSF/IMaX_spec_psf_spinor.dat' ;file containing fabry perot curve
  degrade=''                    ;string describing spatial degradation
  vshift=0.                     ;velocity shift of prefilter
  pfshift=0.                    ;prefilter shift
  wlref=5250.208d               ;ref. wavelength
  noise=0.0
  wlrange=dblarr(2)
  
  if keyword_set(g15) then wlref=5250.645d
  
  
  case strupcase(load) of 
    'V5-6': begin
      print,'Loading predefined parameter set: '+load
      wl=[-80d, -40d, +40d, +80d, +227d]/1e3
    end
    'V6-6': begin
      print,'Loading predefined parameter set: '+load
      wl=[-80d, -40d, 0d, +40d, +80d, +227d]/1e3
    end
    'V7-4': begin
      print,'Loading predefined parameter set: '+load
      wl=[-140d, -90d, -40d, +40d, +90d, +140d, +227d]/1e3
    end
    'V8-4': begin
      print,'Loading predefined parameter set: '+load
      wl=[-140d, -90d, -40d, 0d, +40d, +90d, +140d, +227d]/1e3
    end
    '100': begin
      wl=dindgen(100)/100*1.0-0.4 ;same range as in MHD simul
    end
    'HINODE_V6':begin
      wlref=6301.500d
      wl=[-100d, -50d, 0d, +50d, +100d, +300d]/1e3
      wl=[wl,wl+6302.500d -wlref]
      load_etalon=''
      finesse=30
      FSR=3.0
      wlrange=[6300d,6304d]
      mhdwlref=wlref
      mhdwlvec=wl
    end
    '5250_V6': begin
      wlref=5250.2080d
      wl=[-100d, -50d, 0d, +50d, +100d, +300d]/1e3
      load_etalon=''
      finesse=30
      FSR=3.0
      wlrange=[5248.2d,5252.2d]
      mhdwlref=wlref
      mhdwlvec=wl
    end
    'FULL': begin
      wl=mhdwlvec
      wlref=mhdwlref
    end
    '100LARGE': begin
      wl=dindgen(100)/100*2.0-0.8 ;extended WL range
    end
    else: begin
      print,'Unknown Parameter Set: '+load
      print,'Available sets are: '
      print,'V5-6, V6-6, V7-4, V8-4, 100, 100large, full, Hinode_V6'
      print,'add keyword : e.g. ''load=''V5-6'''''
      reset
    end
  endcase
;-----------------------------------------------
;Parameters: Here for IMaX
;-----------------------------------------------
;  wl=[-80d, -40d, +40d, +80d, +227d]/1e3
;  wl=[-80d, -40d,0d, +40d, +80d, +227d]/1e3
;  wl=[-140d, -90d, -40d, +40d, +90d, +140d, +227d]/1e3
;  wl=[dindgen(7)/6*0.2-0.1,.227d]
;  wl=dindgen(100)/100*1.0-0.4   ;same range as in MHD simul
  imax={pf_fwhm:pf_fwhm,$       ;define FWHM of prefilter
        ncav:ncav, $            ;cavities for prefilter
        netalon:netalon,$       ;number of etalons (max 2)
        velo:velo,$             ;wl-shift in km/s
        bin:bin,$               ;pixel binning
        finesse:finesse,$       ;define finesse and
        FSR:FSR,$               ;FSR of etalon Angstrom
        load_pf:load_pf,$ ;load prefilter curve from file (Stefan Meining)
        load_etalon:load_etalon, $ ;file containing fabry perot curve
        degrade:degrade, $     ;string describing spatial degradation
        vshift:vshift, $       ;velocity shift of prefilter
        pfshift:pfshift, $     ;prefilter shift
        noise:noise, $         ;add noise (reference: continuum level)
        wl:wl,$                ;wavelength vector
        wlrange:wlrange, $     ;WL range to be used from data file
        wlref:wlref $           ;ref. wavelength
       }
end

pro mhd_degrade,mhd=mhdfile,load=load,show=show,dirout=dirout,noise=noise, $
                g15=g15,datform=datform
  common imax_par,imax
  common fitsdata,mhdfileold,dat4d,mhdprof,icont,hdr,icimg
  
  if n_elements(mhdfile) eq 0 then begin
    print,'IDL program to degrade MHD data'
    print,'Keywords:'
    print,'  mhd=''mhd.fits'' ;MuRAM filename created with spinor'
    print,'  dir=''~/data''   ;Output Directory'
    print,'  datform=''MHD'' or ''4D''    ;input FITS data format'
    print,'Optional:'
    print,'  /show          ;create some plots'
    print,'  load=''V5-6''    ;load some predefined parameter settings'
    print,'  /g15           ;Use Fe I 5250.6 g=1.5 line'
    print,'Example: '
    print,'mhd_degrade,mhd=''~/data/ISSI_inversions/ISSI_SIRsynth_4d.fits'',dirout=''~/data/ISSI_inversions/'''
    retall
  endif
  
  if n_elements(datform) eq 0 then begin
    print,'Please specify input data format: datform=''MHD'' or ''4D'''
    reset
  endif
;-----------------------------------------------
; MHD cube:
;-----------------------------------------------
  if n_elements(mhdfileold) eq 0 then reread=1 $
  else reread=mhdfile ne mhdfileold
  if reread then begin
    print,'Restoring fits file: ',mhdfile
    case datform of
      'MHD': begin
        fits_read,mhdfile,mhdprof,hdr
        icimg=reform(max(mhdprof(*,0,*,*),dim=1))
      end
      '4D': begin
        read_fits4d,mhdfile,dat4d
        n=n_elements(dat4d.wl)
        mhdprof=*(dat4d.data)
        icimg=dat4d.icont
        for i=0,3 do for j=0,n-1 do mhdprof(j,i,*,*)=mhdprof(j,i,*,*)*icimg
        icimg=reform(max(mhdprof(*,0,*,*),dim=1))
      end
      else: begin
        print,'Unknown input data format.'
        reset
      end
    end
  endif
;-----------------------------------------------
  szp=size(mhdprof)
                                ;find index of quiet pixel
  dummy=min(total(total(sqrt(mhdprof(*,1:3,*,*)^2),2)/ $
                  reform(mhdprof(*,0,*,*)),1),ii)
                                ;assume that an area of 200 pixel
                                ;around this is the QS.
  ixy=[ii mod szp(3),ii / szp(3)]
  px=100
  ix=0>(ixy(0)+[-1,1]*px)<(szp(3)-1)
  iy=0>(ixy(1)+[-1,1]*px)<(szp(4)-1)
  icont=total(icimg(ix(0):ix(1),iy(0):iy(1)))/ $
        n_elements(icimg(ix(0):ix(1),iy(0):iy(1)))
;-----------------------------------------------
  if n_elements(load) eq 0 then load='V5-6'
;  loadpar,load,g15=g15,mhdwlvec=mhdwlvec,mhdwlref=mhdwlref
  loadpar,load,g15=g15,mhdwlvec=wl,mhdwlref=wlref
  order_mhd =['I','V','Q','U']
  order_fits=['I','Q','U','V']
  
  case datform of
    'MHD': begin
      stokes_sm=transpose(mhdprof,[1,0,2,3])
      wlref=double(sxpar(hdr,'WLREF'))
      wlmin=double(sxpar(hdr,'WLMIN'))
      wlmax=double(sxpar(hdr,'WLMAX'))
      nwl=fix(sxpar(hdr,'NWL'))
      wl=dindgen(nwl)/(nwl-1)*(wlmax-wlmin)+wlmin
    end
    '4D': begin
      order_mhd =['I','Q','U','V']
      stokes_sm=transpose(mhdprof,[1,0,2,3])
      wl=dat4d.wl+dat4d.wlref
      wl=wl-imax.wlref
      wlref=imax.wlref
      nwl=n_elements(wl)
    end
  endcase
  if total(abs(imax.wlrange[1]-imax.wlrange[0])) ge 1e-5 then begin
    print,'Selected data wl-range: ',imax.wlrange
    inwl=where(wl+wlref ge min(imax.wlrange) and wl+wlref le max(imax.wlrange))
    if inwl(0) eq -1 then message,'WL range not in data.'
    wl=wl(inwl)
    nwl=n_elements(inwl)
    stokes_sm=stokes_sm(*,inwl,*,*)
  endif
  sz_mhd=size(stokes_sm)
  mhdfileold=mhdfile
  wlres_mhd=(wl(1)-wl(0))
  print,'WL-resolution of MHD data: ',wlres_mhd*1e3,' m\AA'
  wlrg_mhd=minmax(wl)
  nwl_mhd=n_elements(wl)
  print,'WL-Region of MHD data: ',wlrg_mhd+wlref
  if keyword_set(show) then begin
    userlct,coltab=0,/full 
    image_cont_al,reform(mhdprof(0,0,*,*)),contour=0,/aspect
    userlct
  endif
;-- Binning ------------------------------------
  imax.bin=1
  if imax.bin ge 2 then imax.degrade='bin'+n2s(imax.bin)+'x'+n2s(imax.bin)
  if n_elements(noise) eq 1 then imax.noise=noise
;-----------------------------------------------
  
;-----------------------------------------------
;Output  
;-----------------------------------------------
  
  hwb=imax.fsr/imax.finesse*1e3              ;fwhm of etalon in m\AA
  wlshift=imax.velo*1e3/2.99792e8*imax.wlref ;wl shift in \AA
  d1 = (imax.wlref*1e-10)^2/(2*hwb*1.e-13*imax.finesse) ;etalon 1
  d2 = d1*0.15                                          ;etalon 2
  
;  wlvshift=vshift/2.99792e8*imax.wlref ;wl shift in \AA
  
  if n_elements(dirout) eq 0 then begin
    dirout=strmid(mhdfile,0,strpos(mhdfile,'/',/reverse_search)+1)
  endif
  out_dir=dirout
  if out_dir eq '' then out_dir='./'
  print,'Output directory: ',out_dir
  if strmid(out_dir,strlen(out_dir)-1,1) ne '/' then out_dir=out_dir+'/'
  spawn,'mkdir -p '+out_dir
;-----------------------------------------------
  !p.multi=0 & !p.position=0
  
;-----------------------------------------------------
;spatially degrade MHD profiles
;-----------------------------------------------------
  if imax.bin ge 2 then begin
    stokes_bin= $
      fltarr(sz_mhd(1),sz_mhd(2),sz_mhd(3)/imax.bin,sz_mhd(4)/imax.bin)
    icimg_bin=fltarr(sz_mhd(3)/imax.bin,sz_mhd(4)/imax.bin)
    
    for i=0,sz_mhd(1)-1 do $
      stokes_bin(i,*,*,*)= $
      congrid(/interp,reform(stokes_sm(i,*,*,*)), $
              sz_mhd(2),sz_mhd(3)/imax.bin,sz_mhd(4)/imax.bin)
    icimg_bin=congrid(/interp,icimg,sz_mhd(3)/imax.bin,sz_mhd(4)/imax.bin)
    stokes_sm=temporary(stokes_bin)
  endif else begin
    icimg_bin=icimg
  endelse
  stokesnew=transpose(mhdprof,[1,0,2,3])
  sz_mhd=size(stokes_sm)
;-----------------------------------------------------
;file name creation
;-----------------------------------------------------
  mhdroot=strmid(mhdfile,strpos(mhdfile,'/',/reverse_search)+1,strlen(mhdfile))
  root=strmid(mhdroot,0,strpos(mhdroot,'.fits'))
  if load eq '' then add='_NWL'+n2s(n_elements(imax.wl)) $
  else add='_'+load
  if keyword_set(g15) then add=add+'_g15'
  if imax.degrade ne '' then add=add+'_'+strcompress(/remove_all,imax.degrade)
  if imax.noise ge 1e-8 then add=add+'_n'+string(imax.noise,format='(e7.1)')
  
  file_pre=out_dir+root+add+'_pre.dat'
  file_fp=out_dir+root+add+'_fp.dat'
  file_4d=out_dir+root+add+'.4d.fits'
  
;-----------------------------------------------------
; Berechnung Etalon
;-----------------------------------------------------
  f1=imax.finesse
  f2=imax.finesse
  
                                ;wl-range for pf + fp curve should
                                ;contain all WL points of the desired
                                ;result (sample points), and should
                                ;cover the full mhd wavelength range,
                                ;but at least +-1 \AA around the
                                ;sample points.
                                ;lam must be equidistant!
                                ;check if imax.wl is already equidistant
  dwl=imax.wl(1:n_elements(imax.wl)-1)-imax.wl(0:n_elements(imax.wl)-2)
  mm=minmax(dwl)
  if abs(mm(0)-mm(1)) lt 1e-6 then begin
    print,'IMaX.wl is equidistant.'
    nlam=n_elements(imax.wl)
    lam=imax.wl-imax.wlref
    wlres_ip=mm(0)
  endif else begin
    accuracy=0.1d-3             ;accuracy in \AA for interpolation
    wlres_ip=gcd_al(round(([imax.wl,0d]+min(imax.wl))/accuracy))*accuracy
    wlres_ip=wlres_ip<wlres_mhd
    nlam=(max(imax.wl)-min(imax.wl))/wlres_ip
    lam=min(imax.wl)+dindgen(nlam+1)*wlres_ip
  endelse
  
  fullrg=0
  if nlam eq n_elements(imax.wl) and nlam eq nwl_mhd then begin
    if max(abs(lam-imax.wl)) le 1e-6 then fullrg=1
  endif

  if fullrg eq 0 then begin
    mmlam=minmax(imax.wl)+[-2d, 2d]
    if min(lam) gt mmlam(0) then begin
      nadd=((min(lam)-mmlam(0))/wlres_ip)>1
      lam=[lam,-(dindgen(nadd)+1)*wlres_ip+min(lam)]
    endif
    if max(lam) lt mmlam(1) then begin
      nadd=((mmlam(1)-max(lam))/wlres_ip)>1
      lam=[lam,(dindgen(nadd)+1)*wlres_ip+max(lam)]
    endif
    nlam=n_elements(lam)
    srt=sort(lam)
    lam=lam(srt)
  endif
  wlref=imax.wlref
  
  lam=lam*1d-10                 ;pf, cf3 calc in SI units
  inwl=lonarr(n_elements(imax.wl))
  mdiff=dblarr(n_elements(imax.wl))
  for i=0,n_elements(imax.wl)-1 do begin
    mdiff(i)=min(abs(imax.wl(i)-lam*1d10+imax.wlref-wlref)) & inwl(i)=!c
  endfor
  
  print,'WL-Res of output data: ',wlres_ip*1e3,' m\AA'
  print,'Max. diff between IMaX sample points and interpolated WL: ', $
    max(mdiff*1e3),' m\AA'
  dummy=min(abs(lam*1d10-0d),iwlref) ;index of ref wavelength (at WL=0d)
  dummy=min(abs(lam*1d10),iwlref) ;index of ref wavelength (at WL=0d)
  
  
;  lam0=wlref*1.d-10 
  lam0=imax.wlref*1.d-10 
;-----------------------------------------------------
; Berechnung ETALON
;-----------------------------------------------------
  
  if imax.load_etalon eq '' then begin
    hw1=imax.fsr*1d-10/imax.finesse
    hw2=hw1*1.5
    d1=(imax.wlref*1d-10)^2/(2*hw1*imax.finesse)
    d2=(imax.wlref*1d-10)^2/(2*hw2*imax.finesse)
    ff1 = 1./(sin(2.*!pi*d1*hw1/(2.*imax.wlref^2)))^2
    ff2 = 1./(sin(2.*!pi*d2*hw2/(2.*imax.wlref^2)))^2
    i1 = 1./(1+ff1*(sin(2.*!pi*d1*lam/imax.wlref^2))^2)
    i2 = 1./(1+ff2*(sin(2.*!pi*d2*lam/imax.wlref^2))^2)
    
    case imax.netalon of
      1: cf3=i1
      2: cf3=i1*i2
      else: message,'Only 1 or 2 etalons allowd.'
    endcase
  endif else begin             ;load predefined etalon and interpolate
                                ;curve to lam 
    etalon=read_ascii(imax.load_etalon)
    eta_wl=double(etalon.field1(0,*))
    eta_val=double(etalon.field1(1,*))

    cf3=interpol(eta_val,eta_wl,lam*1d10)    
  endelse

                                ;normalize FP function
                                ;check for FP maxima: sum up over one FSR
  cmax=0.95*max(cf3)
  normval=0.
  dosum=0
  for iw=1,nlam-1 do begin
    if cf3(iw) ge cmax and cf3(iw-1) lt cmax then dosum=dosum+1
    if dosum eq 1 then begin
      normval=normval+cf3(iw)
    endif
  endfor
  if dosum eq 1 then normval=total(cf3(0:nlam-1))
  cf3=cf3/normval
  
  plot,lam,cf3,/xst 
  
;  oplot,color=1,lam,cf3
;-----------------------------------------------------
; Berechnung Prefilter
;-----------------------------------------------------
  
  
  vf2 = 1./(1.+(2.*(lam+imax.pfshift*1d-10)/(imax.pf_fwhm*1.d-10))^(2.*imax.ncav))
                                ;simulate effect of SC velocity by shifting prefilter curve
  dwl=(lam(1)-lam(0))*1e10
  ishft=round(wlshift/dwl)
  vf2=shift(vf2,ishft)
  pf = vf2                      ; Vorfilter
                                ;max(pf) should be 1
  if imax.load_pf ne '' then begin
    pf_litho=read_pf_ascii(imax.load_pf,lam*1d10+imax.pfshift-wlshift+imax.wlref)
    !p.multi=0 & !p.position=0
    psset,/ps,size=[18.,12.],/encaps,/no_x,pdf=0, $
      file='/home/lagg/work/projects/PHI/2011_ErrorStudy/figures/pf_litho.eps'
  @greeklett.pro
    userlct
    plot,lam,pf_litho,/nodata,xtitle=f_lambda+'-'+f_lambda+'!L0!N ['+f_angstrom+']',ytitle='Transmission',charsize=1.3,xrange=[-3,3],/xst
    oplot,color=1,lam,pf_litho,thick=3
    oplot,color=3,lam,vf2,thick=3
    psset,/close
    pf=pf_litho
  endif
;    pf[0:i1] = pf[i1]             *0
;    pf[i2:nlam-1] = pf[i2]     *0
;-----------------------------------------------------
;simulate measurement (without convolution, use multiplication)  
;-----------------------------------------------------
  userlct
  nwl_scan=n_elements(imax.wl)
  stokes_scan=fltarr(sz_mhd(1),nwl_scan,sz_mhd(3),sz_mhd(4))
  ntot=nwl_scan*4l*sz_mhd(3)*sz_mhd(4)
  i=0l
  timecheck,'1',/init
  if keyword_set(show) then $
    psset,/ps,size=[18.,12.],/no_x,file='imax_measurement.ps',pdf=0
  userlct
                                ;create interpolated stokes data
  if fullrg eq 0 then begin
    print,'Creating interpolated Stokes data ...',format='(a,$)'
    stokes_sm_wl=fltarr(4,nlam,sz_mhd(3),sz_mhd(4))
    for ix=0,sz_mhd(3)-1 do for iy=0,sz_mhd(4)-1 do for is=0,3 do begin
      stokes_sm_wl(is,*,ix,iy)= $
        interpol(reform(stokes_sm(is,*,ix,iy)),wl,lam*1d10) 
    endfor
    print,' Done.'
  endif else stokes_sm_wl=stokes_sm

  for iw=0l,nwl_scan-1 do begin
    ishift=inwl(iw)-iwlref     ;Ref-WL minus current position
    cf3s=shift_interp(cf3,ishift)
    cf3s=cf3s/total(cf3s) ;the FP should not change the number of photons,
                                ;onyl the prefilter should!
    cfp=cf3s*pf
    for ix=sz_mhd(3)-1,0,-1 do $
      for iy=sz_mhd(4)-1,0,-1 do $
      for is=3,0,-1 do begin
      stokes_scan(is,iw,ix,iy)=stokes_scan(is,iw,ix,iy)+$
        total(stokes_sm_wl(is,*,ix,iy)*cfp(*))
      i=i+1
      if i*80/ntot ne (i-1)*80/ntot then print,'.',format='(a,$)'
    endfor    
    plot,lam*1d10,stokes_sm_wl(0,*,0,0)/icont,/xst,/yst,yrange=[0,1.5], $
      title='iwl='+n2s(iw)
    oplot,color=1,wl,stokes_sm(0,*,0,0)/icont
    oplot,color=2,lam*1d10,cfp/max(cfp)
    plots,color=3,psym=1,lam(inwl(0:iw))*1d10,stokes_scan(0,0:iw,0,0)/icont
    oplot,linestyle=1,[0,0]+min(lam(inwl)*1d10),!y.crange
    oplot,linestyle=1,[0,0]+max(lam(inwl)*1d10),!y.crange
  endfor
  if keyword_set(show) then psset,/close
  timecheck,'2'
  print
  
  if imax.noise ge 1e-8 then begin
    print,'Adding noise, level: ',imax.noise,' of Icont'
    for ix=sz_mhd(3)-1,0,-1 do $
      for iy=sz_mhd(4)-1,0,-1 do begin
                                ;make sure that line intensity is not zero
      iline=(sqrt((stokes_scan(0,*,ix,iy)>0)/icont))>1e-2
      for is=3,0,-1 do begin
                                ;scale noise to intensity level at
                                ;every wavelength position
        noise=(randomu(seed,nwl_scan,/normal))*imax.noise*(1./iline)*icont
        stokes_scan(is,*,ix,iy)=stokes_scan(is,*,ix,iy)+noise
      endfor
                                ;make sure to not have negative counts
                                ;in I profile:
      stokes_scan(0,*,ix,iy)=stokes_scan(0,*,ix,iy)>0
      if min(finite(stokes_scan(*,*,ix,iy))) eq 0 then begin
        print,'... this is not good.'
        stop
      endif
    endfor
  endif
  
  if keyword_set(show) then begin  
    !p.charsize=1
    !p.position=0 & !p.multi=0
    xy=[0,0]
    xystr=string(xy,format='(''x'',i2.2,''y'',i2.2)')
    file_eps=out_dir+root+add+'_'+xystr+'.eps'
    psset,/encaps,/ps,file=file_eps,size=[18,14],/no_x
    !p.multi=[0,2,2]
  @greeklett.pro
    userlct
    xrg=[-0.3,0.45]
    xrg=minmax(imax.wl)+[-1,1]*.03*(max(imax.wl)-min(imax.wl))
    for is=0,3 do begin
      sn=order_mhd(is)
      yrg=[0.5,1.2]      
      if is ge 1 then $
        yrg=1.1*[-1,1]*max(abs(stokes_sm(is,*,xy(0),xy(1))))/icont
      tit='Stokes '+sn
      plot,linestyle=3,wl,stokes_sm(is,*,xy(0),xy(1))/icont, $
        xrange=xrg,/xst,yrange=yrg,/yst,thick=3, $
        title=tit,ytitle=sn+'/Ic',xtitle='WL-WLREF ['+f_angstrom+']'
      oplot,imax.wl,stokes_scan(is,*,xy(0),xy(1)),psym=-1,color=1,thick=3
      if is eq 0 then oplot,color=3,linestyle=0,lam,pf/max(pf),thick=3
    endfor
    xyouts,/normal,alignment=0.5,0.5,1.0,'!Cx='+n2s(xy(0))+', y='+n2s(xy(1))
    psset,/close
    
    window,2,xs=(sz_mhd(3)+2)*nwl_scan,ys=(sz_mhd(4)+2)*2*4
    userlct,/full,coltab=0,/reverse
    for is=0,3 do for i=0,nwl_scan-1 do begin
      xy=[(i)*(sz_mhd(3)+2)+1,(sz_mhd(4)+2)*is*2+1]
      tvscl,stokes_scan(is,i,*,*),xy(0),xy(1)
      if i eq 0 then xyouts,xy(0),xy(1),order_mhd(is)+'_scan',/device
      dummy=min(abs(wl-imax.wl(i)),ip)
      xy=[(i)*(sz_mhd(3)+2)+1,(sz_mhd(4)+2)*(is+.5)*2+1]
      tvscl,stokes_sm(is,ip,*,*)/icont,xy(0),xy(1)
      if i eq 0 then xyouts,xy(0),xy(1),order_mhd(is)+'_MHD',/device
      if is eq 3 then $
        xyouts,xy(0),xy(1)+sz_mhd(4),'!C  '+n2s(fix(imax.wl(i)*1e3))+' mA',/device
;    if is eq 0 then print,'Prefilter:',pf(ip)    
    endfor
  endif
  
  print,'Write out data and filter curves for helix+ [Y/n]?'
;  key=strupcase(get_kbrd())
  key='Y'
  if key ne 'N' then begin
    
;-----------------------------------------------------
;write out simulated observations in 4D fits file
;-----------------------------------------------------
    data4d=transpose(stokes_scan,[1,0,2,3])
    io=indgen(4)
    for i=0,3 do io(i)=where(order_mhd eq order_fits(i))
    data4d=temporary(data4d(*,io,*,*)) ;sort I,V,Q,U -> I,Q,U,V
    mkhdr,header,data4d,/extend
    fxaddpar, header, 'CODE','MHD'
;    fxaddpar, header, 'NSR',1   ;for spinor. Not needed if only one spectrum
;    fxaddpar, header, 'WLI1',1  ;for spinor. Not needed if only one spectrum
    fxaddpar, header, 'XOFFSET',0
    fxaddpar, header, 'XOFFSET',0
    fxaddpar, header, 'YOFFSET',0
    fxaddpar, header, 'ICIMG',icont
    fxaddpar, header, 'ICONT',icont
    tn=tag_names(imax)
    for it=0,n_tags(imax)-1 do begin
      if tn(it) ne 'WL' and tn(it) ne 'WLREF' and tn(it) ne 'WLRANGE' then $
        fxaddpar, header, tn(it),imax.(it)
    endfor
    sxaddpar,header,'COMMENT ','created with mhd_degrade.pro'
    writefits,file_4d,data4d,header
    
    mkhdr,header,imax.wl,/image ;first extension: WL vector
    fxaddpar, header, 'WLREF',imax.wlref
    writefits,file_4d,imax.wl+imax.wlref,header,/append
    
    
;    icimg_bin=reform(stokes_sm(0,0,*,*))
    mkhdr,header,icimg_bin,/image   ;second extension: Continuum image   
    writefits,file_4d,icimg_bin,header,/append
    print,'Wrote FITS 4D File: ',file_4d
;-----------------------------------------------------
;write out filter profiles for helix
;-----------------------------------------------------
    openw,/get_lun,unit_pre,file_pre,error=err
    if err ne 0 then message,'unable to open filter file: '+file_pre
    openw,/get_lun,unit_fp,file_fp,error=err
    printf,unit_fp,' ;convolution file, from '+mhdroot
    printf,unit_fp,' ;simulates a Fabry-Perot measurement at 617.3 nm '
    printf,unit_fp,' ;Contains only FP'
    printf,unit_fp,' ;WL(A)       Intensity '
    printf,unit_pre,' ;convolution file, from '+mhdroot
    printf,unit_pre,' ;simulates a Fabry-Perot measurement at 617.3 nm '
    printf,unit_pre,' ;Contains only prefilter'
    printf,unit_pre,' ;WL(A)       Intensity '
    for i=0l,nlam-1 do begin
      printf,unit_fp,(lam(i)+lam0)*1d10,cf3(i),format='(f15.6,e15.6)'
      printf,unit_pre,(lam(i)+lam0)*1d10,pf(i),format='(f15.6,e15.6)'
    endfor
    free_lun,unit_fp
    free_lun,unit_pre
    print,'Wrote out filter functions:'
    print,transpose([file_fp,file_pre])

  endif
  stop
end
