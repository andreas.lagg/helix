;
; Function PowerSpectrum accepts a square 2D array as input
; and returns a 1D array that includes the Fourier power spectrum
; as a function of the waveVector magnitude, |K| = sqrt(kX*kX + kY*kY)
;
function PowerSpectrum_AL, inputArray,azi=azi,column=col,row=row
  
  iarr=reform(inputArray)
  if keyword_set(col) then iarr=transpose(iarr)
  sZ = SIZE(iarr)
  twoD=sz[0] eq 2
  keyword=keyword_set(azi) or keyword_set(col) or keyword_set(row)
  if twoD then if keyword eq 0 then message,'For 2D-Array specify one of the keywords: /azi, /col, /row'
if sz[0] ne 1 and keyword eq 0 and (sZ[0] ne 2 or (sZ[0] eq 2 and sZ[1] ne sZ[2])) then begin
   void = DIALOG_MESSAGE(/Error, ['Error in Power_Spectrum.',$
                         'Input needs to be a 1D array or a square 2D array.',$
                         'Calling sequence:',$
                         'outputSpectrum = PowerSpectrum(iarr)'])                      
   RETURN, inputArray
 endif


nX = sZ[1] & nWaveNumber = nX / 2

if twoD and keyword_set(azi) or twoD eq 0 then begin
  if twoD eq 0 then begin
  waveNumberDist = SHIFT(DIST(nX,1), nWaveNumber)
  fftArray = SHIFT(FFT(iarr, -1), nWaveNumber)
  endif else begin
    waveNumberDist = SHIFT(DIST(nX), nWaveNumber, nWaveNumber)
    fftArray = SHIFT(FFT(iarr, -1), nWaveNumber, nWaveNumber)
  endelse
  fftArray = FLOAT(fftArray*CONJ(fftArray))
  outputSpectrum = FLTARR(nWaveNumber)
  for waveNumber = 0, nWaveNumber-1 do outputSpectrum[waveNumber] = $
    TOTAL(fftArray[WHERE((waveNumberDist ge (waveNumber-0.5)) and $
                         (waveNumberDist lt (waveNumber+0.5)))])
endif else begin
  waveNumberDist = SHIFT(DIST(nX,1), nWaveNumber)
  outputSpectrum = FLTARR(nWaveNumber,sz[2])
  for i=0,sz[2]-1 do begin
    fftArray = SHIFT(FFT(iarr[*,i], -1), nWaveNumber)
    fftArray = FLOAT(fftArray*CONJ(fftArray))
    for waveNumber = 0, nWaveNumber-1 do outputSpectrum[waveNumber,i] = $
      TOTAL(fftArray[WHERE((waveNumberDist ge (waveNumber-0.5)) and $
                           (waveNumberDist lt (waveNumber+0.5)))])
  endfor
  outputspectrum=total(outputspectrum,2)/sz[2]
endelse
RETURN, outputSpectrum
end
