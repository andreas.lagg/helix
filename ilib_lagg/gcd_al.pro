;find greatest common divisor of elements in a vector
function gcd_al,vec
  
  ivec=long(abs(vec))
  maxv=max(ivec)
  
  gcd=maxv+1
  repeat begin
    gcd=gcd-1
  endrep until gcd eq 1 or max(ivec mod gcd) eq 0
  
  return,gcd
end
