function straylight,s,psf
  
  print,'Computing Local Straylight ... '
  p=s*0.0
  w=s*0.0
  dim=size(s)
  pd=size(psf)
  ntot=n_elements(s)
  for l=0,dim[1]-1 do begin
    for i=0,dim[2]-1 do begin
      ls=reform(s[l,i,*,*])
      for x=0,dim[3]-1 do begin
        xl=x-pd[1]/2>0
        xh=x+pd[1]/2-1<(dim[3]-1)
        xxl=xl-x+pd[1]/2
        xxh=xh-x+pd[1]/2
        for y=0,dim[4]-1 do begin
          n=y+dim[4]*(x+dim[3]*(i+dim[2]*l))
          prc=float([n+1,n])/ntot*100
          if fix(prc[1]*100) ne fix(prc[0]*100) then $
            print,string(prc[0],format='(f6.2)')+'%',format='(a,%"\r",$)'
                                ;print,'.',format='(a,$)'
                                                    
;
          yl=y-pd[2]/2>0
          yh=y+pd[2]/2-1<(dim[4]-1)
          yyl=yl-y+pd[2]/2
          yyh=yh-y+pd[2]/2
;
          p[l,i,x,y]=total(ls[xl:xh,yl:yh]*psf[xxl:xxh,yyl:yyh])- $
                     ls[x,y]*psf[pd[1]/2,pd[2]/2] ;

          w[l,i,x,y]=total(psf[xxl:xxh,yyl:yyh])-psf[pd[1]/2,pd[2]/2] ;
          p[l,i,x,y]/=w[l,i,x,y] ;
;        if x mod 5 eq 0 then begin & image_cont_al,contour=0,/aspect,/cut,  psf[xxl:xxh,yyl:yyh],title=add_comma(n2s([x,y]))+' '+add_comma(n2s([xl,xh,yl,yh]))+' '+add_comma(n2s([xxl,xxh,yyl,yyh]))+' '+n2s(w[l,i,x,y])        & wait,.01 & endif
        end
      end
    end
  end
  print
  print,'Done.'
  return,p
end
