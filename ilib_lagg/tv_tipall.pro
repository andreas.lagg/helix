pro tv_tipall,ps=ps,tip,coord_sav=sav,cwl=cwl
  
                                ;restore coordinates
  restore,/v,sav
  
  psset,ps=ps,size=[20,16]
  userlct,/full,glltab=0,coltab=0,/reverse
  heflag=[0,0,0,1,1]
  siflag=[0,1,1,0,0]
  contflag=[1,0,0,0,0]
  magflag=[0,0,1,0,1]
  for i=0,4 do begin
  
    tipimg=get_tipimg(tip,silicon=siflag(i),magnetogram=magflag(i), $
                      helium=heflag(i),continuum=contflag(i),cwl=cwl)
    
    image_cont_al,tipimg,xtip,ytip,xtitle='x [arcsec]',ytitle='y [arcsec]', $
      zrange=[min(tipimg,/nan),max(tipimg,/nan)],title=tip,cont=0,/aspect
  endfor
  
  psset,/close
end
