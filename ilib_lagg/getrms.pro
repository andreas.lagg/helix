;get rms noise for 3D data cube:
;Dim 1: WL (over which noise is computed)
;Dim2+3: spatial
function getrms,data
  
  sz=size(data)
  rms=fltarr(sz(2),sz(3))
  for i=0l,sz(2)-1 do for j=0l,sz(3)-1 do $
    rms(i,j)=sqrt((moment(data(*,i,j)))(1))
  return,rms
end
