;directly read data from TIP fits 
;example to read images:
;xy_fits,'data/13may01/13may01.014cc',i=i,q=q,v=v,u=u,bin=indgen(20)+30
;example to read profiles:
;xy_fits,'data/18May05/18may05.007-0?cc',x=[0,100],y=100,i=imi
;keywords: 
;reset   = close and reopen associated file unit. Reread header
;          information. Create output structure xyfst
;norm2ii = normalize Q,U,V to I
;norm2ic = normalize Q,U,V to Ic
;average = return only 1 averaged profile for the selected x and y-range
;struct  = return structure holding info on associated data file
;median  = perform median filter on output profile
;close   = close associated file unit. Old unit is automatically
;          closed if a new filename is opened
;nodark  = do not subtract dark images (index 0 and 1 of every file)
;mergebeams = data is flatfield file (raw data). The raw data are still
;            split in two beams, this keyword merges the beams.
;new_cont = recalculate continuum image
;save_memory - if this keyword is st, wl-binning is done
;              immediately. This saves memory and is necessary to
;              obtain images of large data sets. It is much slower,
;              since it reads every wavelength bin separately and does
;              the binning 'on the fly'
;maxi,..,maxv - return image of max value for abs(iquv)

pro xy_fits,filein,x=xin,y=yin,i=i,q=q,u=u,v=v,bin=binin, $
            reset=reset,silent=silent, $
            close=close,nodark=nodark,mergebeams=mergebeams,structure=struct, $
            norm2ii=norm2ii,norm2ic=norm2ic,median=medval, $
            average=average,wlbin=wlbin,new_cont=new_cont,force=force, $
            save_memory=save_memory,norm_cont=norm_cont, $
            continue_error=continue_error,data_error=data_error, $
            maxi=maxi,maxq=maxq,maxu=maxu,maxv=maxv,absval=absval
  common xyfts,xyfst,datos0,datos1,datos2,datos3,datos4,datos5,datos6,datos7, $
    datos8,datos9,datos10,datos11,datos12,datos13,datos14,datos15,datos16, $
    datos17,datos18,datos19,datos20,datos21,datos22,datos23,datos24,datos25, $
    datos26,datos27,datos28,datos29,datos30,datos31,datos32,datos33,datos34, $
    datos35,datos36,datos37,datos38,datos39,datfull
  
  if keyword_set(reset) then if n_params() eq 0 then begin
    if n_elements(xyfst) ne 0 then begin
      for ic=0,xyfst(0).nfiles-1 do begin
      if keyword_set(silent) eq 0 then $
        print,'Closing '+xyfst(ic).file(ic)
      free_lun,xyfst(ic).unit
      xyfst.ic=0
    endfor
    dummy=temporary(xyfst)
    return
    endif
  endif
  
  data_error=0
  iuse=arg_present(i) or keyword_set(norm2ii) or keyword_set(norm2ic)
  quse=arg_present(q)
  uuse=arg_present(u)
  vuse=arg_present(v)
  
  file=file_search(filein,count=fcnt)
  if fcnt eq 0 then begin
    message,/cont,filein+' not found.'
    reset
  endif
  
  if n_elements(xin) ne 0 then x=xin
  if n_elements(yin) ne 0 then y=yin
  if n_elements(binin) ne 0 then bin=binin
  
  new=keyword_set(reset)
  if n_elements(xyfst) eq 0 then new=1 $
  else if fcnt eq 1 then begin
    new=new or strmatch(xyfst(0).fmask(0),file(0)) eq 0
  endif else new=new or max(xyfst.fmask ne file)
  if n_elements(silent) eq 0 then silent=1
  silent=keyword_set(silent)
  
  if keyword_set(nodark) then df=0. else df=1.
  
  if new eq 0 then begin
    fst=fstat(xyfst(0).unit)
    if fst.open eq 0 then new=1
  endif
  
  if new then begin
                                ;close old file
    if n_elements(xyfst) ne 0 then $
      for ic=0,xyfst(0).nfiles-1 do begin
      if keyword_set(silent) eq 0 then $
        print,'Closing '+xyfst(ic).file(ic)
      free_lun,xyfst(ic).unit
      xyfst.ic=0
    endfor
    
    if fcnt eq 1 then $
      fff=file_search(remove_multi(file),count=cnt) $
    else begin
      fff=remove_multi(file)
      cnt=n_elements(fff)
      for ic=0,cnt-1 do if file_search(fff(ic)) eq '' then $
        message,fff(ic)+' not found.'
    endelse
    if cnt eq 0 then begin
      message,/cont,file+' not found.'
      reset
    endif
    if cnt gt 40 then begin
      message,'Only 40 files allowed (datos0-39).'
      reset
    endif
    
    unit=lonarr(cnt)
    fullheader=read_tip_header(fff(0),verbose=0)
    for ic=0,cnt-1 do begin
      ics=n2s(ic)
      
                                ;do not get darks for files -02, -03
                                ;use data determined from file -01
                                ;(only for raw data files, when no *c
                                ;file is present)
      no_dark_data=0
      lthree=strmid(fff(ic),strlen(fff(ic))-3,3)
      if ic eq 0 then if strmid(lthree,0,1) eq '-' then begin
        if fix(strmid(lthree,2,1)) ge 2 then begin ;-02, -03 ...
          xy_fits,strmid(fff(ic),0,strlen(fff(ic))-3)+'-01',structure=xyftmp
          darki=xyftmp.darki
          darkq=xyftmp.darkq
          darku=xyftmp.darku
          darkv=xyftmp.darkv
          data=xyftmp.data_margins
          no_dark_data=1
          dummy=temporary(xyftmp)
        endif
      endif
      
      
      if silent eq 0 then print,'Assoc '+fff(ic)
      dum=rfits_im_al(fff(ic),1,dd,hdr,nrhdr)
      
      openr,un,/get_lun,fff(ic),error=err,/swap_if_little_endian
      if err ne 0 then begin
        data_error=1
        print,'Error in reading '+fff(ic)
        return
      endif
      unit(ic)=un
      if(dd.bitpix eq 8) then ba='bytarr' $
      else if(dd.bitpix eq 16) then ba='intarr' $
      else if(dd.bitpix eq -32) then ba='fltarr' $        
      else if(dd.bitpix eq 32) then ba='lonarr' $
      else message,'Unknown bitpix: '+n2s(dd.bitpix)
      
      
      excmd='datos'+ics+'=assoc(unit(ic),'+ba+ $
            '(dd.naxis1,dd.naxis2),long(2880)*nrhdr) &' + $
            ' datos=datos'+ics
      dummy=execute(excmd)
      
      dummy=readfits_ssw(fff(ic),header,nslice=0,silent=1)
      header=replace_header(header)
      hst=fits_header2st(header)
      
      end_character=strmid(dd.filename,strlen(dd.filename)-1,1)
      if(end_character ne 'c' and $
         dd.date ge 20051001 and dd.date le 20060101) then begin
        nbin=dd.naxis2
        nx=dd.naxis3/4
        ny=dd.naxis1
      endif else begin
        nbin=dd.naxis1
        nx=dd.naxis3/4
        ny=dd.naxis2
      endelse
      
      if no_dark_data eq 0 then begin
                                ;get darks
        dark=fltarr(2*4,nbin,ny)
        
        
        
                                ;remove darks only when no c or cc at
                                ;end of filename
        if strmid(fff(ic),strlen(fff(ic))-1,1) ne 'c' and $
          strmid(fff(ic),strlen(fff(ic))-2,1) ne 'c' then begin
          for i1=0,7 do begin
            dark(i1,*,*)=(rfits_im_al2(datos,dd,i1+1))
          endfor
        endif
        darki=total(dark([0,4],*,*),1)/2.
        darkq=total(dark([1,5],*,*),1)/2.
        darku=total(dark([2,6],*,*),1)/2.
        darkv=total(dark([3,7],*,*),1)/2.
                                ;create margins only for no c or cc in
                                ;filename
        if strmid(fff(ic),strlen(fff(ic))-1,1) ne 'c' and $
          strmid(fff(ic),strlen(fff(ic))-2,1) ne 'c' then begin
          im1=(rfits_im_al(fff(ic), 9)>0)-darki
          im2=(rfits_im_al(fff(ic),10)>0)-darkq
          im3=(rfits_im_al(fff(ic),11)>0)-darku
          im4=(rfits_im_al(fff(ic),12)>0)-darkv
          data=limits_tip(median((im1+im2+im3+im4)/4.,3))
          if((data(3)-data(2)) ne (data(7)-data(6))) or $
            (data(3) le data(2)) or (data(7) le data(6)) then begin
            print,'**********************************************************'
            print,'PROBLEMS IN THE AUTOMATIC DETERMINATION OF THE BEAM LIMITS'
            print,'File: ',fff(ic)
            print,'Limits (lower beam): ',data(2:3)
            print,'Limits (upper beam): ',data(6:7)
            print,'Run the manual routine (acum2iquv4)'
            print,'**********************************************************'
            reset
          endif
        endif else data=[0,nbin-1,0,ny-1,0,nbin-1,0,ny-1]
      endif
      
      xy={fmask:file,file:fff,nfiles:cnt,nbin:nbin, $
          nx:nx,nxtot:0l,nx0:0l,nytot:0l,ny:ny,rotate:0, $
          header:hst,fullheader:fullheader,dd:dd,unit:unit(ic), $
          data_margins:data,nrhdr:nrhdr, $
          darki:darki,darkq:darkq,darku:darku,darkv:darkv, $
          ic:fltarr(nx>1,ny)+1.}
      end_character=strmid(dd.filename,strlen(dd.filename)-1,1)
      xy.rotate=(end_character ne 'c' and $
                 dd.date ge 20051001 and dd.date le 20060101)
      
      if ic eq 0 then begin
        xyfst=replicate(xy,cnt)        
      endif else begin
        xytmp=xyfst(ic)
        struct_assign,xy,xytmp
        xyfst(ic)=xytmp
        xyfst(ic).nx0=total(xyfst(0:ic-1).nx)
                                ;test compatibility
        bad=xy.ny ne xyfst(0).ny or xy.nbin ne xyfst(0).nbin
        if bad then message,'Icompatible files: '+fff
      endelse
    endfor
    xyfst.nxtot=total(xyfst.nx)
  endif
  

                                ;recursively call xy_fits to get i for
                                ;full image to fill ic 
                                ;not for data where beams still need
                                ;to be merged.
  if keyword_set(mergebeams) eq 0 then begin
    recalc_cc=keyword_set(norm2ic) and max(xyfst.ic) le 1+1e-5
    recalc_cc=recalc_cc or keyword_set(new_cont)
    if n_elements(new_cont) ne 0 then recalc_cc = keyword_set(new_cont)
    if recalc_cc then begin
                                ;check presence of aux-file
      newccx=0
      xffile=remove_multi(xyfst.file)
      nxf=n_elements(xffile)
      
      xyfst_orig=xyfst
      for id=0,nxf-1 do begin
        fidx=where(xyfst_orig(0).file eq xffile(id))
        if (max(xyfst_orig(fidx(0)).ic,/nan) le 1+1e-5 or $
            keyword_set(new_cont)) then begin
          filex=xffile(id)+'x'
          if keyword_set(new_cont) then xerr=1 $
          else read_ccx,filex,icont=icimg,norm_cont=norm_cont,error=xerr
          if xerr ne 0 then begin
            xfc=[0,xyfst_orig(0).nx-1]
            yfc=[0,xyfst_orig(0).ny-1]
            if silent eq 0 then $
              print,'Get continuum image for '+xffile(id) $
              +' ... ',format='(a,$)'
            xy_fits,xffile(id),bin=-1,i=ifl,/silent,x=xfc,y=yfc,new_cont=0
            
            for ix=xfc(0),xfc(1)<((size(ifl))(2)-1) do $
              for iy=yfc(0),yfc(1) do begin
              xyfst_orig(fidx(0)).ic(ix,iy)= $
                get_cont(ifl(*,ix-xfc(0),iy-yfc(0)))
;              xyfst(fidx(0)).ic(ix,iy)=xyfst_orig(fidx(0)).ic(ix,iy)
            endfor
            newccx=1
;plot,ifl(*,100,100) & oplot,color=1,!x.crange,[0,0]+xyfst(id).ic(100,100) & oplot,color=2,!x.crange,[0,0]+get_cont(ifl(*,100+xyfst(id).nx0,100))
          endif else begin
            if silent eq 0 then $
              print,'Get Continuum from AUX (ccx) file '+filex            
            szic=size(icimg)
            xyfst_orig(fidx(0)).ic(0:szic(1)-1,0:szic(2)-1)=icimg
;            xyfst(fidx(0)).ic(0:szic(1)-1,0:szic(2)-1)=icimg
          endelse
          for ii=1,n_elements(fidx)-1 do begin
            xyfst_orig(fidx(ii)).ic=xyfst_orig(fidx(0)).ic
;            xyfst(fidx(ii)).ic=xyfst_orig(fidx(0)).ic
          endfor
        endif
      endfor
      xyfst=xyfst_orig
       if newccx then begin
         xy_fits,xyfst(0).file,/reset
         xyfst.ic=xyfst_orig.ic
 ;        if n_elements(force) eq 0 then force=1
 ;        write_ccx,xyfst,force=force
       endif
      if silent eq 0 then print,'Done.'
    endif
  endif

                               ;return if no i,q,u,v argument is given
  if (iuse eq 0 and quse eq 0 and uuse eq 0 and vuse eq 0) or $
    arg_present(struct) then begin
    struct=xyfst
    if (iuse eq 0 and quse eq 0 and uuse eq 0 and vuse eq 0) then return
  endif
  
  nbin=n_elements(bin)
  nlx=n_elements(x)
  nly=n_elements(y)
;   if nbin ne 0 and (nly ne 0 or nlx ne 0) then $
;     if bin(0) ne -1 then  message,'Select either bin or x/y value.' 
  
  fullimg_flag=1
  if nlx eq 1 then x=[x,x]
  if nly eq 1 then y=[y,y]
  if nlx eq 0 then begin
    x=[0,xyfst(0).nxtot]
  endif else fullimg_flag=0
  if nly eq 0 then begin
    y=[0,xyfst(0).ny]
  endif else fullimg_flag=0
  nlx=n_elements(x)
  nly=n_elements(y)
  if nlx ne 2 or nly ne 2 then $
    message,'x and y must be a scalar or a vector with 2 elements'
  if (min(x) ge xyfst(0).nxtot or max(x) lt 0 or $
      min(y) ge xyfst(0).ny or max(y) lt 0) then begin
    if keyword_set(continue_error) eq 0 then begin
      message,/cont,'No profiles found: x'+ $
        add_comma(sep='-',remove_multi(n2s(x,format='(i4.4)')))+'y'+ $
        add_comma(sep='-',remove_multi(n2s(y,format='(i4.4)')))
      message,/cont,'The dimensions in the FITS file '+xyfst(0).file(0)+ $
        ' are: '
      message,/cont,'x:'+n2s(xyfst(0).nxtot)+' y:'+n2s(xyfst(0).ny)
      reset
    endif else begin            ;return even if no data were found
      data_error=1
      return
    endelse
  endif
  x=(x>0)<(xyfst(0).nxtot-1) & x=x(sort(x))
  y=(y>0)<(xyfst(0).ny-1) & y=y(sort(y))
  nx=x(1)-x(0)+1
  ny=y(1)-y(0)+1
  
  if nbin eq 0 then bin=-1
  if bin(0) eq -1 then begin
    bin=indgen(xyfst(0).nbin)
    nbin=n_elements(bin)
  endif else fullimg_flag=0
  if ny eq 1 then addy=1 else addy=0
;save_memory=1  
  if keyword_set(save_memory) then begin
    nn=nbin
    nbin=1
  endif else begin
    if long(nbin)*nx*(ny+addy) ge 100e6 then begin
;      help,/traceback
      message,/cont,'You try to read a very large array from: '+file[0]+ $
        ', consider the use of the ''/save_memory'' keyword!'
    endif
    nn=nbin
  endelse
                                ;make array 1 pixel larger in y to
                                ;make sure that it is always 3-dimensional!
  if iuse then begin
    i=fltarr(nbin,nx,ny+addy) 
    maxi=fltarr(nx,ny+addy) 
    itmp=fltarr(nn,1,ny+addy)
  endif
  if quse then begin
    q=fltarr(nbin,nx,ny+addy)
    maxq=fltarr(nx,ny+addy) 
    qtmp=fltarr(nn,1,ny+addy)
  endif
  if uuse then begin
    u=fltarr(nbin,nx,ny+addy)
    maxu=fltarr(nx,ny+addy) 
    utmp=fltarr(nn,1,ny+addy)
  endif
  if vuse then begin
    v=fltarr(nbin,nx,ny+addy)
    maxv=fltarr(nx,ny+addy) 
    vtmp=fltarr(nn,1,ny+addy)
  endif
  odoi=-1
  dm=xyfst(0).data_margins

  for dix=x(0),x(1) do begin
    dx0=dix-x(0)
    doi=fix(total(xyfst.nx0 le dix)-1)
    if odoi ne doi then dummy=execute('datos=datos'+n2s(doi))
    odoi=doi
    ix=dix-xyfst(doi).nx0
    if iuse then $
      itmp(*,0,0:y(1)-y(0)) = (rfits_im_al2(datos,xyfst(doi).dd,4*ix+1)- $
              xyfst(doi).darki*df)(bin,y(0):y(1))
    if quse then $
      qtmp(*,0,0:y(1)-y(0)) = (rfits_im_al2(datos,xyfst(doi).dd,4*ix+2)- $
              xyfst(doi).darkq*df)(bin,y(0):y(1))
    if uuse then $
      utmp(*,0,0:y(1)-y(0)) = (rfits_im_al2(datos,xyfst(doi).dd,4*ix+3)- $
              xyfst(doi).darku*df)(bin,y(0):y(1))
    if vuse then $
      vtmp(*,0,0:y(1)-y(0)) = (rfits_im_al2(datos,xyfst(doi).dd,4*ix+4)- $
                               xyfst(doi).darkv*df)(bin,y(0):y(1))

    if keyword_set(norm2ic) then for iy=0,ny-1 do begin
      if quse then qtmp(*,0,iy)= $
        qtmp(*,0,iy)/xyfst(doi).ic(dix-xyfst(doi).nx0,iy+y(0))
      if uuse then utmp(*,0,iy)= $
        utmp(*,0,iy)/xyfst(doi).ic(dix-xyfst(doi).nx0,iy+y(0))
      if vuse then vtmp(*,0,iy)= $
        vtmp(*,0,iy)/xyfst(doi).ic(dix-xyfst(doi).nx0,iy+y(0))
    endfor
                                ;do merging only for pixels where
                                ;it is possible
    if keyword_set(mergebeams) then if ny lt xyfst(0).ny then begin
      iniy=indgen(y(1)-y(0)+1)+y(0)
      inlowerbeam=where(iniy ge dm(2) and iniy le dm(3))
      if inlowerbeam(0) ne -1 then begin
        iniy_up=iniy(inlowerbeam)+(dm(6) - dm(2))
        iniy_lo=iniy(inlowerbeam)-y(0)
        if iuse then itmp(*,0,iniy_lo) = $
          (itmp(*,0,iniy_lo) + (rfits_im_al2(datos,xyfst(doi).dd,4*ix+1)- $
                               xyfst(doi).darki*df)(*,iniy_up))/2.
        if quse then qtmp(*,0,iniy_lo) = $
          (qtmp(*,0,iniy_lo) - (rfits_im_al2(datos,xyfst(doi).dd,4*ix+2)- $
                               xyfst(doi).darkq*df)(*,iniy_up))/2.
        if uuse then utmp(*,0,iniy_lo) = $
          (utmp(*,0,iniy_lo) - (rfits_im_al2(datos,xyfst(doi).dd,4*ix+3)- $
                               xyfst(doi).darku*df)(*,iniy_up))/2.
        if vuse then vtmp(*,0,iniy_lo) = $
          (vtmp(*,0,iniy_lo) - (rfits_im_al2(datos,xyfst(doi).dd,4*ix+4)- $
                               xyfst(doi).darkv*df)(*,iniy_up))/2.
      endif
    endif                       ;merge beams, not full image
    if iuse then maxi(dx0,0:ny-1)=max(abs(itmp(*,0,0:ny-1)),dim=1)
    if quse then maxq(dx0,0:ny-1)=max(abs(qtmp(*,0,0:ny-1)),dim=1)
    if uuse then maxu(dx0,0:ny-1)=max(abs(utmp(*,0,0:ny-1)),dim=1)
    if vuse then maxv(dx0,0:ny-1)=max(abs(vtmp(*,0,0:ny-1)),dim=1)
    if keyword_set(absval) then begin
      if iuse then itmp(*,0,0:ny-1)=abs(itmp(*,0,0:ny-1))
      if quse then qtmp(*,0,0:ny-1)=abs(qtmp(*,0,0:ny-1))
      if uuse then utmp(*,0,0:ny-1)=abs(utmp(*,0,0:ny-1))
      if vuse then vtmp(*,0,0:ny-1)=abs(vtmp(*,0,0:ny-1))
    endif
    if keyword_set(save_memory) then begin
      if iuse then i(0,dx0,0:ny-1)=total(itmp(*,0,0:ny-1),1)/nn
      if quse then q(0,dx0,0:ny-1)=total(qtmp(*,0,0:ny-1),1)/nn
      if uuse then u(0,dx0,0:ny-1)=total(utmp(*,0,0:ny-1),1)/nn
      if vuse then v(0,dx0,0:ny-1)=total(vtmp(*,0,0:ny-1),1)/nn
    endif else begin
      if iuse then i(*,dx0,0:ny-1)=itmp(*,*,0:ny-1)
      if quse then q(*,dx0,0:ny-1)=qtmp(*,*,0:ny-1)
      if uuse then u(*,dx0,0:ny-1)=utmp(*,*,0:ny-1)
      if vuse then v(*,dx0,0:ny-1)=vtmp(*,*,0:ny-1)
    endelse
  endfor
  
                                ;merge beams, full image
  if keyword_set(mergebeams) and ny eq xyfst(0).ny then begin
    if iuse then i=(i(*,*,dm(2):dm(3))+i(*,*,dm(6):dm(7)))/2.
    if quse then q=(q(*,*,dm(2):dm(3))-q(*,*,dm(6):dm(7)))/2.
    if uuse then u=(u(*,*,dm(2):dm(3))-u(*,*,dm(6):dm(7)))/2.
    if vuse then v=(v(*,*,dm(2):dm(3))-v(*,*,dm(6):dm(7)))/2.    
    ny=dm(3)-dm(2)+1
  endif else begin
                                ;reduce size again to original size
    if iuse then if (size(i))(3) ne ny then i=temporary(i(*,*,0:ny-1))
    if quse then if (size(q))(3) ne ny then q=temporary(q(*,*,0:ny-1))
    if uuse then if (size(u))(3) ne ny then u=temporary(u(*,*,0:ny-1))
    if vuse then if (size(v))(3) ne ny then v=temporary(v(*,*,0:ny-1))
  endelse

  
  if n_elements(medval) ne 0 then if medval lt xyfst(0).nbin+2 then begin
    if iuse then i=median(i,medval,dimension=1)
    if quse then q=median(q,medval,dimension=1)
    if uuse then u=median(u,medval,dimension=1)
    if vuse then v=median(v,medval,dimension=1)
  endif
  
  if keyword_set(mergebeams) then $
    if nx eq xyfst(0).nxtot and nbin eq xyfst(0).nbin then begin
    dm=xyfst(0).data_margins
    if iuse then i=i(dm(0):dm(1),*,*)
    if quse then q=q(dm(0):dm(1),*,*)
    if uuse then u=u(dm(0):dm(1),*,*)
    if vuse then v=v(dm(0):dm(1),*,*)
    nx=dm(1)-dm(0)+1
  endif
  

  if keyword_set(norm2ii) and keyword_set(norm2ic) eq 0 then begin
    if quse then q=q/i
    if uuse then u=u/i
    if vuse then v=v/i
  endif
  
  if keyword_set(absval) then begin
    i=abs(i)
    q=abs(q)
    u=abs(u)
    v=abs(v)
  endif

                                ;binning in WL
  if n_elements(wlbin) ne 0 then begin
    if wlbin gt 1 and wlbin lt xyfst(0).nbin-1 then begin
      if keyword_set(save_memory) then $
        message,/trace,'Incompatible keywords: /save_memory and wlbin'
      for j=0,xyfst(0).nbin-1,wlbin do begin
        j1=(j+wlbin-1)<(xyfst(0).nbin-1)
        i(j/wlbin,*,*)=total(i(j:j1,*,*),1)/wlbin
        q(j/wlbin,*,*)=total(q(j:j1,*,*),1)/wlbin
        u(j/wlbin,*,*)=total(u(j:j1,*,*),1)/wlbin
        v(j/wlbin,*,*)=total(v(j:j1,*,*),1)/wlbin
      endfor
      i=i(0:(j-1)/wlbin,*,*)
      q=q(0:(j-1)/wlbin-1,*,*)
      u=u(0:(j-1)/wlbin-1,*,*)
      v=v(0:(j-1)/wlbin-1,*,*)
    endif
  endif

                                ;return averaged profile
  if keyword_set(average) then if nx*ny gt 1 then begin
    np=nx*ny
    if ny eq 1 then begin
      if iuse then i=total(i,2)/np
      if quse then q=total(q,2)/np
      if uuse then u=total(u,2)/np
      if vuse then v=total(v,2)/np
    endif else begin
      if iuse then i=total(total(i,2),2)/np
      if quse then q=total(total(q,2),2)/np
      if uuse then u=total(total(u,2),2)/np
      if vuse then v=total(total(v,2),2)/np
    endelse
  endif
  
  if keyword_set(close) then $
    for ic=0,xyfst(0).nfiles-1 do free_lun,xyfst(ic).unit
end
