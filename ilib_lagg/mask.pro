;+
; routine mask.pro,number
;	creates mask for multiple plots
; INPUTS:
;	number:		number of plots in vertical and horizontal direction
;			2 element vector [rows,columns]
;
; PARAMETERS:
;	pos:		4-d vector [xl,yb,xr,yt] with coordinates of plot 
;			region in normal units.
;	title:		main title of mask
;	subtitle:	subtitle of mask
; headtitle[i]: seperate head-titles for horizontal plots
;	[xyz]title:	title for z-axis, common title for x,y axis
;	d[xyz]title[i]:	titles for different vertical [horizontal] plots
;	[xy]range[i]:	2-el. vector with min and max of [xy]-axis for plots
;               if range=(0,0) -> range is set in msk_plot routine
;	zrange:		2-el. vector with min and max of label-axis for plots
;			if set, keyword 'label' is set automatically
; seperation: seperation between plots in normal units
;	label[i]:		0 for no label, 1 for label for different horizontal plots
; nolabel: extend margin to the right (no z-label plot present)
;	[xyz]log:	1 for logarithmic, 0 for nonlogarithmic scaling
;	log_min:	minimum for logarithmic plots for autorange
;	charsize:	size of characters for mask
; additional_xlabel: array(nr of x-elements,nr of add. labels),
;                    defines additional labels for x-axis
; add_xl_timeflag: if set to on1, additional x-label is time
; big_xlabel: enlarge the region of xlabels by (nr) lines
;	[xyz]ticks:	number of [xyz]-ticks (default: 6)
;	[xy]tickname:	names of [xy]-ticks (default: numbers)
;	d[xy]tickname:	names of [xy]-ticks for different vertical [horizontal]
;	                (default: numbers)
;	[xyz]format:	format for [xyz]-ticks (default: e15.2)
;	[xyz]time:	if set, [xyz]-ticknames are converted to HH:MM format
; lb_pos: [nrx,nry,4] element array with coordinates for label box
;
;for map-projection:
; limit: 4-element vector specifies limit of map (see IDL ref. guide, map_set)
; view_pos: vector containing viewing position [latitude,longitude,rot]
;
;
; KEYWORDS:
; map: plot should be drawn on sphere (map projection, defined in msk_plot)
; back: if map is set, draw front and rear side
; spherical: if set, map projection is spherical independent on size
;            of plot window
; fix_margins: if set, margins are not adapted to charsize
;              if two-element vector - position of plot in x-dir
;                                      (normal units)
;
; OUTPUT:
;	mask		structure of: 
;	.plot(i,j)
;	  .position:	4-elem. vector of coords for plot i,j in device units
;	  .lab_position:	4-elem. vector of coords for label box i,j in device units
;   .lab_linespace: linespacing for label
;	  .[xy]range:	2-elem. vector of ranges for [xyz]-axis of plot i,j
;         .[xy]offset:  double precision number with time offset for large
;                       times (julday), in connection with [xy]time
;	  .[xy]tickv:	vector with tick values for [xy]-axis (max. 30)
;	  .[xy]ticks:	number of [xy]-ticks
;	.label
;	  .position:	4-elem. vector of coords for label in device units
;	  .[xy]range:	2-elem. vector of ranges for [xy]-axis of label
;   .label: 1 for label, 0 for no label
;	.charsize:	charsize used for title, subtitle, ...
;
;	the values of the legend plot are in maskout(number(0),number(1))
;-
;

function add_line_space,ytitle,chsz,yrange=yrange
  
  cn=(convert_coord(chsz*[!d.x_ch_size,!d.y_ch_size], $
                    /device,/to_normal))(0:1)
  if cn(1)/cn(0) le 1.  then clsp=1 else clsp=0 ;changing linespace in ytitle
  sp=strlen(ytitle)  & spold=-1
  
  repeat begin
    sp=(strpos(/reverse_search,strupcase(strmid(ytitle,0,sp)),'!C'))(0)
    if sp ne -1 and spold-sp ne 2 then begin
      nxsp=(strpos(/reverse_search, $
                   strmid(strupcase(ytitle),sp+2,strlen(ytitle)),'!C'))(0)
      if nxsp eq -1 then nxsp=strlen(ytitle)
      add=0
      for i=0,n_elements(uplo)-1 do $
        if strpos(strmid(strupcase(ytitle),sp,nxsp), $
                  uplo(i)) ne -1 then add=1
      if n_elements(yrange) eq 2 then begin ;concat lines
        nyt=strmid(ytitle,0,sp)+' '+strmid(ytitle,sp+2,strlen(ytitle)+2)
        xyouts,-20,-20,/normal,noclip=0,nyt,width=w,charsize=chsz
        w=w*!d.x_size/!d.y_size
        if w lt abs(yrange(1)-yrange(0)) then begin
          ytitle=nyt
          add=0
        endif
      endif 
      if add eq 1 and clsp eq 1 then ytitle= $
        strmid(ytitle,0,sp+2)+'!C'+strmid(ytitle,sp+2,strlen(ytitle)+2)
    endif
    spold=sp
  endrep until sp lt 0

  return,ytitle
end

function mask,number,title=title,subtitle=subtitle,xtitle=xtitle,$
              ytitle=ytitle,ztitle=ztitle,dxtitle=dxtitle,label=label,$
              xrange=xrange,yrange=yrange,zrange=zrange,xlog=xlog,ylog=ylog,$
              zlog=zlog,charsize=charsize,xticks=xticks,yticks=yticks,$
              xformat=xformat,yformat=yformat,xtime=xtime,ytime=ytime,$
              zticks=zticks,zformat=zformat,ztime=ztime,dytitle=dytitle,$
              pos=pos,xtickname=xtickname,ytickname=ytickname, $
              dxtickname=dxtickname,dytickname=dytickname,dztitle=dztitle, $
              headtitle=headtitle,map=map,view_pos=view_pos,limit=limit, $
              spherical=spherical,back=back,log_min=log_min, $
              seperation=seperation,additional_xlabel=additional_xlabel, $
              big_xlabel=big_xlabel,fix_margins=fix_margins, $
              add_xl_timeflag=add_xl_timeflag,nolabel=nolabel, $
              small_label=small_label,lb_pos=lb_pos,short_label=short_label, $
              fraction=fraction,lb_linespace=lb_linespace, $
              time_offset=time_offset,isotropic=isotropic, $
              zbar_position=zbar_position,lbl_position=lb_str
  
  common label_box,old_pos
  common maskret,maskret
  
                                ;delete label box varaible
  if n_elements(old_pos) ne 0 then dummy=temporary(old_pos)
  
;sets font size
  if !p.charsize eq 0 then !p.charsize=1
  chszreset=!p.charsize
  if keyword_set(charsize) then !p.charsize=charsize
;   chszold=1.
;   if !d.x_size gt 100 then $
;     !p.charsize=charsize*chszold*(!d.x_size/80./!d.x_ch_size)
  
  
;check input parameters
  if n_elements(additional_xlabel) eq 0 then additional_xlabel=0
  
  if n_elements(lb_str) ne 0 then begin
    if n_elements(lb_str) eq 1 or n_elements(lb_str) ne number(1) then $
      lb_str=strarr(number(1))+lb_str(0)
  endif
  
  if n_elements(lb_pos) eq 4 then begin
    lbd=fltarr(number(0),number(1),4)
    for i=0,number(0)-1 do for j=0,number(1)-1 do lbd(i,j,*)=lb_pos
    lb_pos=lbd
  endif
  if n_elements(lb_pos) gt 0 then $
    if n_elements(lb_pos) ne number(0)*number(1)*4 then dummy=temporary(lb_pos)
  if n_elements(zbar_position) ne 0 then begin
    szpl=size(zbar_position)
    ok=0
    if szpl(0) eq 2 then $
      if szpl(1) eq 4 and szpl(2) eq number(1) then ok=1
    if ok eq 0 then if szpl(1) eq 4 then begin
      zbar_position=zbar_position # (intarr(number(1))+1)
    endif else begin
      message,/cont,'Wrong zbar_position format: '+string(zbar_position(*))
      dummy=temporary(zbar_position)
    endelse
  endif
  
  if n_elements(seperation) gt 0 then sep=seperation(0) else sep=0.006d
  if keyword_set(pos) then begin
    posset=1
    dummy=size(pos)
    if dummy(0) ne 1 and dummy(1) ne 4 then $
      message,'position has to be 4-d vector [xl,yb,xr,yt]'
  endif else begin
    posset=0
    plot,[0,1],/nodata,/noerase,xst=5,yst=5
    pos=[!x.region(0),!y.region(0),!x.region(1),!y.region(1)]
  endelse
  
  screen_pos_norm=pos
  screen_pos_dev=(convert_coord([pos(0),pos(2)],[pos(1),pos(3)], $
                                /normal,/to_device))([0,1,3,4])
  if keyword_set(xlog) then begin
    if n_elements(xlog) eq 1 then xtype=intarr(number(0))+xlog $
    else begin
      if n_elements(xlog) ne number(0) then  $
        message,'XLOG has to be scalar or vector with (no. of columns) el.' $
      else xtype=xlog
    endelse
  endif else xtype=intarr(number(0))
  if keyword_set(ylog) then begin
    if n_elements(ylog) eq 1 then ytype=intarr(number(1))+ylog $
    else begin
      if n_elements(ylog) ne number(1) then  $
        message,'YLOG has to be scalar or vector with (no. of rows) elements' $
      else ytype=ylog
    endelse
  endif else ytype=intarr(number(1))
  if keyword_set(zlog) then begin
    if n_elements(zlog) eq 1 then ztype=intarr(number(1))+zlog $
    else begin
      if n_elements(zlog) ne number(1) then  $
        message,'ZLOG has to be scalar or vector with (no. of rows) elements' $
      else ztype=zlog
    endelse
  endif else ztype=intarr(number(1))
  
  if keyword_set(log_min) then begin
    if n_elements(log_min) eq 1 then logmin=fltarr(number(1))+log_min(0) $
    else begin
      if n_elements(log_min) ne number(1) then message, $
        'LOG_MIN has to be scalar or vector with (no. of rows) elements' $
      else logmin=log_min
    endelse    
  endif else logmin=fltarr(number(1)) + 1e-4
  
  xtype=reverse(xtype)
  ytype=reverse(ytype)
  ztype=reverse(ztype)
  logmin=reverse(logmin)
  nr_size=size(number)
  if nr_size(0) ne 1 and nr_size(1) ne 2 then $
    message,'NUMBER of plots has to be a vector with 2 elements [x,y]'
  if not keyword_set(xtitle) then xtitle=''
  if not keyword_set(ytitle) then ytitle=''
  if not keyword_set(ztitle) then ztitle=''
  ytitle=ytitle+'!C'
  if keyword_set(dxtitle) then begin
    xtitle='!C'+xtitle
    dummy=size(dxtitle)
    if dummy(0) eq 0 then begin
      xt=dxtitle
      for i=0,number(0)-2 do xt=[[xt],[dxtitle]] 
      dxtitle=xt
    endif else $ 
      if (dummy(0) ne 1) or (dummy(1) ne number(0)) then $
      message,'DXTITLE has to be vector with (no of rows) elements'
  endif else dxtitle=strarr(number(0))
  if keyword_set(dytitle) then begin
    ytitle=ytitle+'!C'
    dummy=size(dytitle)
    if dummy(0) eq 0 then begin
      yt=dytitle
      for i=0,number(1)-2 do yt=[[yt],[dytitle]] 
      dytitle=yt
    endif else $ 
      if (dummy(0) ne 1) or (dummy(1) ne number(1)) then $
      message,'DYTITLE has to be vector with (no of columns) elements'
  endif else dytitle=strarr(number(1))
  ztitle=ztitle+'!C'
  if keyword_set(dztitle) then begin
;  ztitle=ztitle+'!C'
    dummy=size(dztitle)
    if dummy(0) eq 0 then begin
      zt=dztitle
      for i=0,number(1)-2 do zt=[[zt],[dztitle]] 
      dztitle=zt
    endif else $ 
      if (dummy(0) ne 1) or (dummy(1) ne number(1)) then $
      message,'DZTITLE has to be vector with (no of columns) elements'
  endif else dztitle=strarr(number(1))
  dxtitle=reverse(dxtitle)
  dytitle=reverse(dytitle)
  dztitle=reverse(dztitle)
  if keyword_set(headtitle) then begin
    head_titflag=1
    if n_elements(headtitle) eq 1 then headtitle=strarr(number(0))+headtitle
    dummy=size(headtitle)
    if dummy(1) ne number(0) then $
      message,'HEADTITLE has to be vector with (no of columns) elements'
  endif else head_titflag=0
  if keyword_set(title) then begin
    dummy=size(title)
    if dummy(0) ne 0 or dummy(1) ne 7 then $
      message,'TITLE has to be a string variable'
  endif
  if keyword_set(subtitle) then begin
    dummy=size(subtitle)
    if dummy(0) ne 0 or dummy(1) ne 7 then $
      message,'SUBTITLE has to be a string variable'
  endif
  if keyword_set(ztitle) then begin
    dummy=size(ztitle)
    if dummy(0) ne 0 or dummy(1) ne 7 then $
      message,'ZTITLE has to be a string variable'
  endif
  if keyword_set(xrange) then begin
    dummy=size(xrange)
    if (dummy(0) eq 1 and dummy(1) eq 2) then begin
      xr=xrange
      for i=0,number(0)-2 do xr=[[xr],[xrange]] 
      xrange=xr
    endif else if (number(0) gt 1 and (dummy(0) ne 2 or dummy(1) ne 2 $
                                       or dummy(2) ne number(0))) or (number(0) eq 1 and (dummy(0) ne 1 $
                                                                                          or dummy(1) ne 2)) then $
      message,$
      'XRANGE has to be an array of 2-el. vectors with (no. of columns) elem.'
  endif else xrange=fltarr(2,number(0))
  if keyword_set(yrange) then begin
    dummy=size(yrange)
    if (dummy(0) eq 1 and dummy(1) eq 2) then begin
      yr=yrange
      for i=0,number(1)-2 do yr=[[yr],[yrange]]
      yrange=yr
    endif else if (number(1) gt 1 and (dummy(0) ne 2 or dummy(1) ne 2 $
                                       or dummy(2) ne number(1))) or (number(1) eq 1 and (dummy(0) ne 1 $
                                                                                          or dummy(1) ne 2)) then $
      message,$
      'YRANGE has to be an array of 2-el. vectors with (no. of rows) elem.'
  endif else yrange=fltarr(2,number(1))
  xrange=double(xrange)
  yrange=double(yrange)
  xrang=xrange
  yrang=yrange
  xoffset=dblarr(number(0))
  yoffset=dblarr(number(1))
  if n_elements(time_offset) eq 0 then time_offset=0.5
  if keyword_set(xtime) then begin
;    xoffset=((long(xrange(0,*))/24))(0)+0d
    xoffset=((long(xrange(0,*))/24)-time_offset)(0)
    if xrange(0,0)-xoffset*24 ge 24 then xoffset=xoffset+1d
    for i=0,number(0)-1 do xrang(*,i)=xrange(*,i)-xoffset(i)*24
  endif
  if keyword_set(ytime) then begin
;    yoffset=((long(yrange(0,*))/24))(0)+0d
    yoffset=((long(yrange(0,*))/24)-time_offset)(0)
    if yrange(0,0)-yoffset*24 ge 24 then yoffset=yoffset+1d
    for i=0,number(1)-1 do yrang(*,i)=yrange(*,i)-yoffset(i)*24
  endif
  xoffset=xoffset*24d
  yoffset=yoffset*24d

  yrang=transpose(reverse(transpose(yrang)))
  xrang=transpose(reverse(transpose(xrang)))
  labelflag=0
  if keyword_set(label) then begin
    if total(label) gt 0 then labelflag=1
    if n_elements(label) ne 1 and  n_elements(label) ne number(1) then  $
      message,'LABEL has to be 0, 1 or an array with (no. of horiz. plots) elem.'
    if n_elements(label) eq 1 then label=label+intarr(number(1))
  endif

  if keyword_set(fraction) eq 0 then fraction=fltarr(number(1))+1 $
  else if n_elements(fraction) ne number(1) then fraction=fltarr(number(1))+1

  ok=0
  if n_elements(label) eq 0 then begin
    ok=1
  endif else if total(label) gt 0 then ok=1
;  if ok eq 1 then begin
    if keyword_set(zrange) then begin
      dummy=size(zrange)
      wrong=0
      if n_elements(label) eq 0 then begin
        if dummy(0) eq 1 and dummy(1) eq 2 then label=1 $
        else if dummy(0) eq 2 and dummy(2) eq number(1) then $
          label=intarr(number(1))+1  $
        else wrong=1
      endif else begin
        if dummy(0) eq 1 and dummy(1) eq 2 then begin
          zr=zrange
          for i=0,n_elements(label)-2 do zr=[[zr],[zrange]]
          zrange=zr
        endif else if dummy(2) ne number(1) then wrong=1
      endelse
      if total(label) gt 0 then labelflag=1
      if wrong eq 1 then message,'ZRANGE has to be an 2-el vector or an ' + $
        'array of 2-el. vectors with (no. of rows) elem.'
      if keyword_set(ztitle) then  $
        if n_elements(label) ne n_elements(where(label ne 0)) then begin
        dztitle=replicate(ztitle,n_elements(label))+dztitle
      endif
    endif else zrange=fltarr(2,n_elements(label)>1)
;  endif else zrange=fltarr(2,n_elements(label)>1)
  zrang=transpose(reverse(transpose(zrange)))
  if n_elements(label) ne 0 then label=reverse(label)

  if keyword_set(xticks) then begin
    dummy=size(xticks)
    if dummy(0) ne 0 then begin
      if dummy(1) ne number(0) then $
        message,'XTICKS has to be 1 number or a vector with (no. of cols) elem.'
    endif else xticks=replicate(xticks,number(0))
  endif else  xticks=replicate(0,number(0))
  xticks=reverse(xticks)
  
  if keyword_set(yticks) then begin
    dummy=size(yticks)
    if dummy(0) ne 0 then begin
      if dummy(1) ne number(1) then $
        message,'YTICKS has to be 1 number or a vector with (no. of rows) elem.'
    endif else yticks=replicate(yticks,number(1))
  endif else yticks=replicate(0,number(1))
  yticks=reverse(yticks)

  if keyword_set(zticks) then begin
    dummy=size(zticks)
    if dummy(0) ne 0 then begin
      if dummy(1) ne n_elements(label) then $
        message,'ZTICKS has to be 1 number or a vector with (no. of rows) elem.'
    endif else zticks=replicate(zticks,n_elements(label))
  endif  else zticks=replicate(0,number(1))
  zticks=reverse(zticks)

  if keyword_set(dxtickname) then begin
    dummy=size(dxtickname)
    if dummy(0) ne 2 and  $
      (dummy(1) ne number(0) and dummy(3) ne max(xticks)) then $
      message,'DXTICKNAME has to be a string array with ' + $
      '(no. of horizontal plots, max(xticks)) elem.'
  endif
  if keyword_set(dytickname) then begin
    dummy=size(dytickname)
    if dummy(0) ne 2 and  $
      (dummy(1) ne number(1) and dummy(3) ne max(yticks)) then $
      message,'DYTICKNAME has to be a string array with ' + $
      '(no. of vertical plots, max(yticks)) elem.'
  endif
  if keyword_set(xtickname) then begin
    dummy=size(xtickname)
    if dummy(dummy(0)) ne xticks(0) then $
      message,'XTICKNAME has to be a string vector with xticks elem.'
    if max(xticks) ne min(xticks) then $
      message,'for XTICKNAME all plots must have xticks ticklabels' + $
      ' (use dxtickname instead)'
    dxtickname=replicate(xtickname,number(0))
  endif else if n_elements(dxtickname) eq 0 then $
    dxtickname=replicate('',number(0))
  if keyword_set(ytickname) then begin
    dummy=size(ytickname)
    if dummy(dummy(0)) ne yticks(0) then $
      message,'YTICKNAME has to be a string vector with yticks elem.'
    if max(yticks) ne min(yticks) then $
      message,'for YTICKNAME all plots must have yticks ticklabels' + $
      ' (use dytickname instead)'
    dytickname=replicate(ytickname,number(1))
  endif else if n_elements(dytickname) eq 0 then $
    dytickname=replicate('',number(1))
;dxtickname=reverse(dxtickname)
  dytickname=reverse(dytickname)
  
  if keyword_set(map) then begin
    map=1
    if not keyword_set(limit) then limit=0
    if n_elements(limit) ne 4 and limit(0) ne 0 then  $
      message,'LIMIT has to be a 4-element vector or zero'
    if not keyword_set(view_pos) then view_pos=[45.,0.,0.] 
    if n_elements(view_pos) ne 3 then $
      message,'VIEW_POS has to be a 3-element vector'
  endif else begin
    map=0
    limit=0
    view_pos=0
  endelse

  def_format='(e15.2)'
  if not keyword_set(xformat) then xformat=def_format
  if n_elements(xformat) eq 1 then xformat=replicate(xformat(0),number(0))
  if n_elements(xformat) ne number(0) then message,'XFORMAT has to be a ' + $
    'number or a vector with (no. of cols) elements'
  empty=where(xformat eq '') & if empty(0) ne -1 then xformat(empty)=def_format
  if not keyword_set(yformat) then yformat=def_format
  if n_elements(yformat) eq 1 then yformat=replicate(yformat(0),number(1))
  if n_elements(yformat) ne number(1) then message,'YFORMAT has to be a ' + $
    'number or a vector with (no. of rows) elements'
  empty=where(yformat eq '') & if empty(0) ne -1 then yformat(empty)=def_format
  yformat=reverse(yformat)
  if not keyword_set(zformat) then zformat=def_format
  if n_elements(zformat) eq 1 then zformat=replicate(zformat(0),number(1))
  if n_elements(zformat) ne number(1) then message,'ZFORMAT has to be a ' + $
    'number or a vector with (no. of rows) elements'
  empty=where(zformat eq '') & if empty(0) ne -1 then zformat(empty)=def_format
  zformat=reverse(zformat)

; defining structure for output
  maskout={plt,position:fltarr(4),lab_position:fltarr(4),lab_linespace:0.,$
           lab_dir:0b,lab_sys:'data', $
           n_pos:fltarr(4),$
           head_pos:fltarr(4),$
           xrange:fltarr(2),$
           yrange:fltarr(2),$
           zrange:fltarr(2),$
           arange_x:0,$
           arange_y:0,$
           arange_z:0,$
           vis:0,$
           charsize:0.,$
           xoffset:0d,$
           yoffset:0d,$
           xtickv:fltarr(31),$
           ytickv:fltarr(31),$
           xtickname:strarr(31),$
           ytickname:strarr(31),$
           xtime:0,$
           ytime:0,$
           xtitle:'',$
           ytitle:'',$
           xformat:'',$
           yformat:'',$
           zformat:'',$
           xticks:0,$
           yticks:0,$
           xticks_orig:0,$
           yticks_orig:0,$
           xt_flag:0,$
           yt_flag:0,$
           log_min:0.,$
           xtype:0,$
           ytype:0,$
           ztype:0, $
           number:bytarr(2),$
           total:bytarr(2),$
           map:0, $
           spherical:0, $
           limit:fltarr(4), $
           view_pos:fltarr(3), $
           screen_pos_norm:screen_pos_norm, $
           screen_pos_dev:screen_pos_dev, $
           back:0}
  lbl=replicate(maskout,n_elements(label)>1)
  maskout=replicate(maskout,number(0),number(1))
  mask={plot:maskout,label:lbl,charsize:0.}
  
; opens window if no window is open
  if !d.name eq 'X' or !d.name eq 'WIN' then $
    if !d.window eq -1 then window,retain=2,xsize=500,ysize=400
  if not posset then if !p.multi(0) eq 0 then $
    if !d.name ne 'PS' then erase

; defining mask areas
;
;    (0,1)     x0                              x1       (1,1)
;         -----------------------------------------------
;         |    |            title              |        |
;         |    |                               |        |
;         -----------------------------------------------  y2
;         |    |         headtitles            |        |
;         -----------------------------------------------  y12
;         |    |                               |    |   |
;         |    |                               |    | l |
;         |    |                               |    |   |
;         |    |                               |    | a |
;         |    |                               |    |   |
;         |    |          plot area            |    | b |
;         |    |                               |    |   |
;         |    |                               |    | e |
;         |    |                               |    |   |
;         |    |                               |    | l |
;         -----------------------------------------------  y1
;         |    |                               |        |
;         |------------------------------------|        |  y0
;         |  |            subtitle             |        |
;         -----------------------------------------------
;    (0,0)                                               (1,0)
;
;label area ends at x=0.99
;
  pos(2)=pos(2)>(pos(0)+0.02)
  pos(3)=pos(3)>(pos(1)+0.02)
  dx=pos(2)-pos(0) & dy=pos(3)-pos(1)
  
  corr=0
  c_old=!p.charsize
  repeat begin
    chh=!p.charsize*!d.y_ch_size
    chw=!p.charsize*!d.x_ch_size
    chn=convert_coord([chw,chh],/device,/to_normal)
    chwn=chn(0)/(dx)
    chhn=chn(1)/(dy)
    
    if keyword_set(small_label) eq 0 then small_label=0
    if keyword_set(fix_margins) then begin
      if n_elements(fix_margins) eq 2 then begin
        x0=fix_margins(0)
        x1=fix_margins(1)
      endif else begin
        x0=.15
        x1=0.75
        if small_label(0) eq 1 then begin
          x0=0.1
          x1=0.85
        endif
      endelse
                                ;   y0=.05 & y1=.15 & y12=.9 & y2=.9
;    if head_titflag then begin
;      y12=.85
;    endif
    endif else begin
      x0=16*chwn
      x1=1-20*chwn
      if small_label(0) eq 1 then begin
        x0=12*chwn
        x1=1-12*chwn
      endif
      if keyword_set(nolabel) then x1=1-8*chwn
    endelse
    if n_elements(title) eq 0 then title=''
    if title ne '' then begin
      y12=1-1.5*chhn & y2=1-1.5*chhn  
    endif else begin
      y12=1. & y2=1.
    endelse
    y0=1.*chhn
    if n_elements(subtitle) ne 0 then if subtitle ne '' then y0=2.*chhn
    y1=y0+2*chhn 
    
    if n_elements(big_xlabel) eq 1 then if big_xlabel gt 0 then begin
      y1=(big_xlabel+4)*chhn
    endif
    if head_titflag then begin
      nhl=0
      for ih=0,n_elements(headtitle)-1 do $
        nhl=count_substring(headtitle(ih),'!C')>nhl
      y12=y2-0.8*(1.+nhl)*chhn
    endif
                                ;reducing charsize if plot area is too
                                ;small
    if y12-y1 lt 0.08 then begin
      !p.charsize=0.99*!p.charsize
      corr=1
      ok=0
    endif else ok=1
    if !p.charsize lt 0.05 then begin
      message,/cont,'plot area too small.'
    endif
  endrep until ok
  if corr eq 1 then message,/cont, $
    'charsize reduced ('+string(c_old)+' -> '+string(!p.charsize)+')'
  
  if map eq 1 then y1=y0        ;making plot area bigger for map plots
                                ;(no axis labeling required)
  
                                ;seperation between plots 
  sx=convert_coord([sep,0.],/normal,/to_device) & sx=sx(0)
  sy=convert_coord([0.,sep],/normal,/to_device) & sy=sy(1)
  seper_x=convert_coord([min([sx,sy]),0],/device,/to_normal) & seper_x=seper_x(0)
  seper_y=convert_coord([0,min([sx,sy])],/device,/to_normal) & seper_y=seper_y(1)

                                ;adjust margins so that x and y axis
                                ;are equal in device units
  diffx=0 & diffy=0
  if keyword_set(isotropic) then begin
    fraction=float(fraction)/total(fraction)
    area_plot=[pos(0)+dx*x0,pos(1)+dy*y1,pos(0)+dx*x1,pos(1)+dy*y12]
    dxrange=(area_plot(2)-area_plot(0))/number(0)-2*seper_x
    ytot=(area_plot(3)-area_plot(1))
    dyrange=fltarr(number(1))
    idy=(yrange(1,*)-yrange(0,*))
    dummy=max(abs(idy)) & mxdy=idy(!c)
    toosmall=where(abs(idy) lt 0.1*abs(mxdy))
    if toosmall(0) ne -1 then begin
      for i=0,n_elements(toosmall)-1 do begin
        yrange(*,toosmall(i))=[yrange(0,toosmall(i))-.05*mxdy, $
                               yrange(1,toosmall(i))+.05*mxdy]
      endfor
      yrang=transpose(reverse(transpose(yrange)))
    endif
    for i=0,number(1)-1 do $
      dyrange(i)=ytot/(total(fraction))*fraction(number(1)-1-i)-2*seper_y
    if abs(xrange(0)-xrange(1)) gt 1e-5 then for iy=0,number(1)-1 do begin
      if abs(yrange(0,iy)-yrange(1,iy)) gt 1e-5 then begin
        dev=convert_coord([[0,0],[dxrange,dyrange(iy)]],/normal,/to_device)
        dxdev=dev(3)-dev(0) & dydev=dev(4)-dev(1)
        dxdat=abs(xrange(1)-xrange(0)) & dydat=abs(yrange(1,iy)-yrange(0,iy))
        dyrange(iy)=dydat/dxdat*dxdev/!d.y_size
;        dyrange(iy)=dyrange(iy)*dydat/dxdat*dxdev/dydev
      endif
    endfor
    
;   dyrange=dyrange>(0.1*max(dyrange))
;   dyrange=dyrange/total(dyrange)
    
    ytot_new=total(dyrange)+2*seper_y*number(1)
    old_dx=(x1-x0) & old_dy=(y12-y1)
    new_dx=old_dx & new_dy=old_dy
    if ytot_new gt ytot then new_dx=old_dx/ytot_new*ytot $
    else new_dy=old_dy/ytot*ytot_new
    diffx=(old_dx-new_dx)/2. & diffy=(old_dy-new_dy)/2.
    x0=x0+diffx & x1=x1-diffx
    y1=y1+diffy & y12=y12-diffy
    y0=y0+diffy & y2=y2-diffy
;    pos(0)=pos(0)+diffx & pos(2)=pos(2)-diffx & dx=pos(2)-pos(0)
;    pos(1)=pos(1)+diffy & pos(3)=pos(3)-diffy & dy=pos(3)-pos(1)
    fraction=dyrange/ytot_new
  endif


  area_title=[pos(0)+dx*x0,pos(1)+dy*y2,pos(0)+dx*x1,pos(3)-diffy]
  area_headtitle=[pos(0)+dx*x0,pos(1)+dy*y12,pos(0)+dx*x1,pos(1)+dy*y2]
  area_plot=[pos(0)+dx*x0,pos(1)+dy*y1,pos(0)+dx*x1,pos(1)+dy*y12]
  
  area_label=[pos(0)+dx*(x1+(1-x1)/1.4),pos(1)+dy*y1,$
              pos(2)-(pos(2)-pos(0))*0.01,pos(1)+dy*y12]
; area_subtitle=[pos(0)+dx*x0     ,pos(1),pos(0)+dx*x1,pos(1)+dy*y0]
 area_subtitle=[pos(0)+0.60*dx*x0,pos(1)+diffy,pos(0)+dx*x1,pos(1)+dy*y0]
 
 
  pos=convert_coord([0.,!p.charsize*!d.y_ch_size], $
                    /device,/to_normal)
  
; output of title
  if keyword_set(title) then begin
    x=area_title(0)
    y=area_title(3)-pos(1)*1.3
    tchsz=1.3*!p.charsize
    twd=get_textwidth(title,tchsz,normal=nwd)
    if nwd gt (x1-x0) then tchsz=tchsz*(x1-x0)/nwd
    xyouts,x,y,title,alignment=0,/normal,charsize=tchsz ;,color=255
  endif

; output of subtitle
  if keyword_set(subtitle) then begin
    x=area_subtitle(0)
    y=area_subtitle(3)-pos(1)*0.9
    xyouts,x,y,subtitle,alignment=0,/normal,charsize=0.75*!p.charsize ;,color=255
  endif
; output of headtitle
  if head_titflag then begin
    y=area_headtitle(3)-pos(1)*0.8
    for i=0,number(0)-1 do begin
      dat=(area_headtitle(2)-area_headtitle(0))/number(0)
      x=area_headtitle(0)+i*dat
      tchsz=0.8*!p.charsize
      twd=get_textwidth(headtitle(i),tchsz,normal=nwd)
      if nwd gt dat then tchsz=tchsz*(dat)/nwd
      xyouts,x,y,headtitle(i),alignment=0,/normal,charsize=tchsz ;,color=255
    endfor
  endif

; creating plot area
  dxrange=(area_plot(2)-area_plot(0))/number(0)
  
;dyrange=(area_plot(3)-area_plot(1))/number(1)
  ytot=(area_plot(3)-area_plot(1))
  dyrange=fltarr(2,number(1))
  for i=0,number(1)-1 do begin
    if i gt 0 then lowy=dyrange(1,i-1) else lowy=0
    hiy=lowy+ytot/(total(fraction))*fraction(number(1)-1-i)
    dyrange(*,i)=[lowy,hiy]
  endfor

  if map eq 0 then begin
    plot,[0,1],/nodata,/noerase,xst=5,yst=5, $
      pos=[area_plot(0)+seper_x,area_plot(1)+seper_y, $
           area_plot(2)-seper_x,area_plot(3)-seper_y], $
      /normal,charsize=!p.charsize*.8,color=!p.color ;,color=255

                                ;output of axis titles
    nxl=count_substring(xtitle,'!C')
    xyouts,(area_plot(0)+area_plot(2))/2.,(area_subtitle(3))> $
      (mask.plot(0).screen_pos_norm(1)+chhn*dy*(nxl+0.05)), $
      xtitle,alignment=0.5,/normal,charsize=!p.charsize*.8
    
    if keyword_set(short_label) then yrg=[area_plot([1,3])] else yrg=-1
    ytitle=add_line_space(ytitle,!p.charsize*.8,yrange=yrg)
    xyouts,2.*chn(0)+screen_pos_norm(0),(area_plot(1)+area_plot(3))/2., $
      ytitle,alignment=0.5,/normal,orientation=90,charsize=!p.charsize*.8  
  endif
  
  if n_elements(zbar_position) gt 0 then begin
    area_label=[area_label(0)>zbar_position(0,*), $
                area_label(1)>zbar_position(1,*), $
                area_label(2)<zbar_position(2,*), $
                area_label(3)<zbar_position(3,*)]
  endif
        
  if labelflag eq 1 then  $
    if n_elements(where(label ne 0)) eq n_elements(label) then begin
    plot,[0,1],/nodata,/noerase,xst=5,yst=5, $
      pos=[area_label(0),area_label(1)+seper_y, $
           area_label(2),area_label(3)-seper_y], $
      /normal,charsize=!p.charsize*.8 ;,color=255
    axis,xticks=1,xtickname=[' ',' '],ticklen=0,xax=0
    if keyword_set(short_label) then yrg=[area_plot([1,3])] else yrg=-1
    ztitle=add_line_space(ztitle,!p.charsize*.8,yrange=yrg)
    if small_label(0) eq 1 then spc='    ' else spc='      '
;  axis,yticks=1,ytickname=[spc,' '],ticklen=0,ytitle=ztitle,yax=0, $
;    charsize=!p.charsize*.8
    axis,yticks=1,ytickname=[spc,' '],ticklen=0,yax=0
    xyouts,/normal,2.*chwn+area_plot(2),(area_label(1)+area_label(3))/2., $
      alignment=0.5,orientation=90,charsize=!p.charsize*.8,ztitle
    axis,xticks=1,xtickname=[' ',' '],ticklen=0,color=0,xax=0
    axis,yticks=1,ytickname=['      ',' '],ticklen=0,color=0,yax=0
  endif
  
;creating label
  if labelflag then begin
    if n_elements(label) eq number(1) then dylrange=dyrange $
    else dylrange=[0,(area_plot(3)-area_plot(1))]
    ztstr=strarr(n_elements(label),32)
    if not keyword_set(zticks) then zticknr=intarr(n_elements(label)) else  $
      zticknr=zticks
    for j=n_elements(label)-1,0,-1 do begin
      if label(j) eq 1 then begin
        if n_elements(zbar_position) gt 0 then $
          label_position=[zbar_position(0,j),zbar_position(1,j)>y1, $
                          zbar_position(2,j),zbar_position(3,j)<y2] $
        else label_position=[area_label(0),area_plot(1)+dylrange(0,j)+seper_y,$
                             area_label(2),area_plot(1)+dylrange(1,j)-seper_y]
        mask.label(j).vis=1
        mask.label(j).position=label_position
        nor_pos=mask.plot(j).position      
        if mask.label(j).position(2)-mask.label(j).position(0) le 0 then begin
          message,/cont,'label area too small'
          mask.label(j).position(2)=1.5*mask.label(j).position(0)
        endif
        if mask.label(j).position(3)-mask.label(j).position(1) le 0 then begin
          message,/cont,'label area too small'
          mask.label(j).position(3)=1.5*mask.label(j).position(1)
        endif
        for k=0,1 do begin      ;convert ccord to device units
          dummy=convert_coord(mask.label(j).position(k*2:k*2+1), $
                              /normal,/to_device)
          mask.label(j).position(k*2:k*2+1)=long(dummy(0:1))
        endfor
        mask.label(j).position(2)=mask.label(j).position(2)> $
          (mask.label(j).position(0)+1)
        mask.label(j).position(3)=mask.label(j).position(3)> $
          (mask.label(j).position(1)+1)
        mask.label(j).n_pos= $
          (convert_coord(mask.label(j).position([[0,1],[2,3]]), $
                         /device,/to_normal))([0,1,3,4])
        mask.label(j).xrange=[0,1]
        if n_elements(label) gt 1 then mask.label(j).zrange=zrang(*,j) $
        else mask.label(j).zrange=zrang(*,0)
        mask.label(j).yrange=mask.label(j).zrange
        if not keyword_set(ztitle) then ztitle=' '
        mask.label(j).xticks=1
        mask.label(j).xrange=[0,0]
        mask.label(j).xtickname=[' ',' ']
        mask.label(j).xtype=0
;      mask.label(j).yticks=zticknr(j)
        mask.label(j).yticks_orig=zticknr(j)
        mask.label(j).ytype=ztype(j)
        mask.label(j).ztype=mask.label(j).ytype
        mask.label(j).log_min=logmin(j)
        mask.label(j).number=[0,j]
        mask.label(j).total=[1,number(1)]
        mask.label(j).yformat=zformat(j)
        mask.label(j).zformat=zformat(j)
        mask.label(j).charsize=!p.charsize
        if keyword_set(short_label) then yrg=[nor_pos([1,3])] else yrg=-1
        dztitle(j)=add_line_space(dztitle(j),!p.charsize*.8,$
                                  yrange=yrg)
        mask.label(j).ytitle=dztitle(j)
        dmy=msk_setrange(mask.label(j),/label,/no_yticks)
        mask.label(j)=dmy
      endif else mask.label(j).vis=0
    endfor
  endif

  for i=0,number(0)-1 do for j=number(1)-1,0,-1 do begin
    mask.plot(i,j).vis=1
    mask.plot(i,j).position= $
      [area_plot(0)+i*dxrange+seper_x,area_plot(1)+min(dyrange(*,j))+seper_y,$
       area_plot(0)+(i+1)*dxrange-seper_x,area_plot(1)+max(dyrange(*,j))-seper_y]
    nor_pos=mask.plot(i,j).position      
    for k=0,1 do begin          ;convert pos to device_units
      dummy=convert_coord(mask.plot(i,j).position(k*2:k*2+1),/normal,/to_device)
      mask.plot(i,j).position(k*2:k*2+1)=long(dummy(0:1))
    endfor
    mask.plot(i,j).n_pos= $
      (convert_coord(mask.plot(i,j).position([[0,1],[2,3]]), $
                     /device,/to_normal))([0,1,3,4])
    if n_elements(lb_pos) ne 0 then begin
      mask.plot(i,j).lab_position=lb_pos(i,j,*)
    endif
    if n_elements(lb_linespace) ne 0 then $
      mask.plot(i,j).lab_linespace=lb_linespace
    if mask.plot(i,j).position(2)-mask.plot(i,j).position(0) le 0 then begin
      message,/cont,'plot area too small in x'
      mask.plot(i,j).position(2)=mask.plot(i,j).position(0)+10
    endif
    if mask.plot(i,j).position(3)-mask.plot(i,j).position(1) le 0 then begin
      message,/cont,'plot area too small in y'
      mask.plot(i,j).position(3)=mask.plot(i,j).position(1)+10
    endif
    mask.plot(i,j).head_pos=[area_headtitle(0)+i*dxrange,area_headtitle(1),$
                             area_headtitle(0)+(i+1)*dxrange,area_headtitle(3)]
    mask.plot(i,j).xrange=xrang(*,i)
    mask.plot(i,j).yrange=yrang(*,j)
    mask.plot(i,j).xoffset=xoffset(i)
    mask.plot(i,j).yoffset=yoffset(j)
    if n_elements(label) gt 1 then begin
      mask.plot(i,j).zrange=zrang(*,j) 
    endif else mask.plot(i,j).zrange=zrang(*,0)
    if j eq 0 then dxtit=dxtitle(i) else dxtit=''
    if i eq 0 then dytit=dytitle(j) else dytit=''
    mask.plot(i,j).xtitle=dxtit
    if keyword_set(short_label) then yrg=[nor_pos([1,3])] else yrg=-1
    dytit=add_line_space(dytit,!p.charsize*.8,yrange=yrg)
    mask.plot(i,j).ytitle=dytit
    mask.plot(i,j).xtickname=dxtickname(i,*)
    mask.plot(i,j).ytickname=dytickname(j,*)
    mask.plot(i,j).xticks_orig=xticks(i)
    mask.plot(i,j).yticks_orig=yticks(j)
    mask.plot(i,j).xtype=xtype(i)
    mask.plot(i,j).ytype=ytype(j)
    mask.plot(i,j).ztype=ztype(j)
    mask.plot(i,j).log_min=logmin(j)
    mask.plot(i,j).number=[i,j]
    mask.plot(i,j).total=[number(0),number(1)]
    mask.plot(i,j).xtime=keyword_set(xtime)
    mask.plot(i,j).ytime=keyword_set(ytime)
    mask.plot(i,j).xformat=xformat(i)
    mask.plot(i,j).yformat=yformat(j)
    mask.plot(i,j).spherical=keyword_set(spherical)
    mask.plot(i,j).back=keyword_set(back)
    mask.plot(i,j).charsize=!p.charsize
    mask.plot(i,j).limit=limit
    mask.plot(i,j).view_pos=view_pos
    mask.plot(i,j).map=map
    dmy=msk_setrange(mask.plot(i,j),additional_xlabel=additional_xlabel, $
                     add_xl_timeflag=add_xl_timeflag,/no_yticks)
    mask.plot(i,j)=dmy
    if n_elements(lb_str) ne 0 then begin
      brd=0.025
      mijp=mask.plot(i,j).position
      dx=mijp(2)-mijp(0)
      dy=mijp(3)-mijp(1)
      case lb_str(j) of
        'bl': begin
          mask.plot(i,j).lab_dir=0b
          mask.plot(i,j).lab_position=[mijp(0)+dx*brd,mijp(1)+dy*brd, $
                                       mijp(0)+dx*0.4,mijp(1)+dy*0.4]
          mask.plot(i,j).lab_sys='device'
        end
        'br': begin
          mask.plot(i,j).lab_dir=0b
          mask.plot(i,j).lab_position=[mijp(2)-dx*0.4,mijp(1)+dy*brd, $
                                       mijp(2)-dx*brd,mijp(1)+dy*0.4]
          mask.plot(i,j).lab_sys='device'
        end
        'tl': begin
          mask.plot(i,j).lab_dir=1b
          mask.plot(i,j).lab_position=[mijp(0)+dx*brd,mijp(3)-dy*0.4, $
                                       mijp(0)+dx*0.4,mijp(3)-dy*brd]
          mask.plot(i,j).lab_sys='device'
        end
        'tr': begin
          mask.plot(i,j).lab_dir=1b
          mask.plot(i,j).lab_position=[mijp(2)-dx*0.4,mijp(3)-dy*0.4, $
                                       mijp(2)-dx*brd,mijp(3)-dy*brd]
          mask.plot(i,j).lab_sys='device'
        end
        else: begin
          mask.plot(i,j).lab_dir=1b
          mask.plot(i,j).lab_position=0
        end
      endcase 
    endif
  endfor

;change order
  maskret=mask
  for j=0,number(1)-1 do for i=0,number(0)-1 do $
    maskret.plot(i,number(1)-1-j)=mask.plot(i,j)
;    maskret.plot(i,j)=mask.plot(i,j)
  for j=0,n_elements(label)-1 do $
    maskret.label(n_elements(label)-1-j)=mask.label(j)
;  maskret.label(j)=mask.label(j)
  maskret.charsize=!p.charsize

; reset charsize
  !p.charsize=chszreset
  if not posset then begin
    !p.multi(0)=!p.multi(0)-1
    if !p.multi(0) lt 0 then !p.multi(0)=!p.multi(1)*!p.multi(2)-1
  endif
  
  pos=[min(maskret.plot.n_pos(0,*,*)), $
       min(maskret.plot.n_pos(1,*,*)), $
       max(maskret.plot.n_pos(2,*,*)), $
       max(maskret.plot.n_pos(3,*,*))]
  
  return,maskret
end


