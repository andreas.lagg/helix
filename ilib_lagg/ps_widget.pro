;+
; NAME:
;    ps_widget
;
;
; PURPOSE:
;    creates a widget to select postscript parameters
;
;
; CATEGORY:
;    none
;
;
; CALLING SEQUENCE:
;    retval=ps_widget(default=default,psname=psname,file=file,
;                     additional_flag=additional_flag,no_x=no_x)
;
; 
; INPUTS:
;    default - vector of default values [por/lan flag, a4/lett flag,
;              encaps flag, col/bw flag, bpp, quality flag, print flag]
;    file - name for postscript file
;    additional flag - structure array {text:'',value:0} for additional
;                      parameters
;
; OPTIONAL INPUTS:
;
;
;	
; KEYWORD PARAMETERS:
;    no_x - set PS-parameters without widget
;    no_file - device,filename=... is not executed
;
;
; OUTPUTS:
;    returns vector [blackwhite,cancel,quality,preview,eps_flag,
;    prt_flag,add_flag1,add_flag2, ...]
;
;
;
; OPTIONAL OUTPUTS:
;    psname - name for postscript file
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;    A. Lagg, 17-03.1997
;-

function size_offset,no_x=no_x,no_center=no_center
  common widget_ps,base,pl_button,a4_button,szx_field,szy_field,offx_field, $
    offy_field,enc_button,clr_button,qual_button,name_field,gs_button, $
    prt_button,add_button,ok_button,bpp_button
  common settings,portrait,sz_paper,non_encaps,color,bpp,quality,print,add, $
    size_margins,ps_nam,sz_xy
  common ratio,xyratio

  a4=[21,29.7]
  a3=[29.7,42]
  a2=a4*2.
  a1=a3*2.
  a0=a2*2.
  letter=[21.6,27.94]
  margin_tb=1.2
  margin_lr=0.9

  paper_size=[0.,0.]
  if n_elements(sz_xy) eq 2 then begin
    if min(sz_xy) gt 0 then begin
      if portrait eq 1 then $
        paper_size=[sz_xy(1)+2*margin_lr,sz_xy(0)+2*margin_tb] $
      else $
        paper_size=[sz_xy(0)+2*margin_lr,sz_xy(1)+2*margin_tb]
    endif
  endif else sz_xy=[0.,0.]

  case sz_paper of 
    0: tot_ps=a4
    1: tot_ps=a3
    2: tot_ps=a2
    3: tot_ps=a1
    4: tot_ps=a0
    5: tot_ps=letter
    else: tot_ps=a4
  end

  if min(paper_size) le 0 then paper_size=tot_ps
  
  if portrait eq 1 then begin   ;landscape
    size_x=paper_size(1)-2*margin_tb
    size_y=paper_size(0)-2*margin_lr
    if xyratio ne 0. then if size_x*xyratio lt size_y then $
      size_y=size_x*xyratio else size_x=size_y/xyratio
    sxy=[size_y,size_x-tot_ps(1)] 
    off_x=margin_lr+(tot_ps(0)-(sxy(0)+2*margin_lr))/2.
;    off_y=paper_size(1)-margin_tb+((sxy(1)+2*margin_tb))/2.
    if keyword_set(no_center) then $
      off_y=tot_ps(1)-margin_tb $;+((sxy(1)+2*margin_tb)) $
    else off_y=tot_ps(1)-margin_tb+((sxy(1)+2*margin_tb))/2.
  endif else begin              ;portrait
    size_x=paper_size(0)-2*margin_lr
    size_y=paper_size(1)-2*margin_tb
    if xyratio ne 0. then if size_x*xyratio lt size_y then $
      size_y=size_x*xyratio else size_x=size_y/xyratio
    sxy=[size_x,size_y]
    off_x=margin_lr+(tot_ps(0)-(sxy(0)+2*margin_lr))/2.
    if keyword_set(no_center) then $
      off_y=margin_tb +(tot_ps(1)-(sxy(1)+2*margin_tb)) $
    else off_y=margin_tb+(tot_ps(1)-(sxy(1)+2*margin_tb))/2.
  endelse

  if not keyword_set(no_x) then begin
    widget_control,szx_field,set_value=size_x
    widget_control,szy_field,set_value=size_y
    widget_control,offx_field,set_value=off_x
    widget_control,offy_field,set_value=off_y
  endif
  
  return,[size_x,size_y,off_x,off_y]
end

pro ps_widget_event,event,no_center=no_center
  common widget_ps,base,pl_button,a4_button,szx_field,szy_field,offx_field, $
    offy_field,enc_button,clr_button,qual_button,name_field,gs_button, $
    prt_button,add_button,ok_button,bpp_button
  common settings,portrait,sz_paper,non_encaps,color,bpp,quality,print,add, $
    size_margins,ps_nam,sz_xy
  common return,retval
  common ratio,xyratio

  widget_control,event.id,get_uvalue=uval

  case uval of 
    'PL': begin
      portrait=event.value
      size_margins=size_offset(no_center=no_center)
    end
    'A4': begin
      sz_paper=event.value
      size_margins=size_offset(no_center=no_center)
      sz_xy=[0.,0.]
      xyratio=0.
    end
    'SZX': begin
      size_margins(0)=event.value
      xyratio=0.
    end
    'SZY': begin
      size_margins(1)=event.value
      xyratio=0.
    end
    'OFFX': begin
      size_margins(2)=event.value
    end
    'OFFY': begin
      size_margins(3)=event.value
    end
    'NAME': begin
      ps_nam=(event.value)(0)
    end
    'ENC': begin
      non_encaps=event.value
      epos=strpos(ps_nam,'.',/reverse_search)
      if epos(0) ne -1 then begin
        ext=strmid(ps_nam,epos,strlen(ps_nam))
        if strlowcase(ext) eq '.ps' and non_encaps eq 1 then $
          ps_nam=strmid(ps_nam,0,epos)+'.eps'
        if strlowcase(ext) eq '.eps' and non_encaps eq 0 then $
          ps_nam=strmid(ps_nam,0,epos)+'.ps'        
        widget_control,name_field,set_value=ps_nam
      endif
    end
    'CLR': begin
      color=event.value
    end
    'QUAL': begin
      quality=event.value
    end
    'BPP': begin
      bpp=event.value
    end
    'GS': begin
      retval(3)=event.select
    end
    'PRT': begin
      print=event.select
    end
    'OK': begin
      if event.value eq 1 then retval(1)=1 else retval(1)=0
      widget_control,base,/destroy
    end
    else: begin                 ;additional_flag
      id=fix(uval)
      retval(6+id)=event.select
    end
  endcase
end

function ps_widget,default=defolt,psname=psname,file=file,size=sz, $
                   additional_flag=additional_flag,no_x=no_x,no_file=no_file, $
                   ratio=ratio,addflag_ret=addflag_ret, $
                   group_leader=group_leader,no_center=no_center
  common widget_ps,base,pl_button,a4_button,szx_field,szy_field,offx_field, $
    offy_field,enc_button,clr_button,qual_button,name_field,gs_button, $
    prt_button,add_button,ok_button,bpp_button
  common settings,portrait,sz_paper,non_encaps,color,bpp,quality,print,add, $
    size_margins,ps_nam,sz_xy
  common return,retval
  common ratio,xyratio

  if n_elements(ratio) ne 0 then xyratio=ratio else xyratio=0
  if n_elements(sz) eq 2 then begin
    if n_elements(size_margins) ne 0 then begin
      if min(sz eq sz_xy) eq 0 then dummy=temporary(size_margins)
    endif
    sz_xy=sz
    xyratio=0
  endif else sz_xy=[0.,0.]
  if keyword_set(additional_flag) then nel_add=n_elements(additional_flag) $
  else nel_add=0
  retval=intarr(6+nel_add)
  
  if n_elements(defolt) eq 0 then defolt=[0,0,0,0,8,1,0]
  if n_elements(file) le 0 then ps_nam='plot.ps' else ps_nam=file
  if n_elements(portrait) eq 0 then begin
    portrait=defolt(0) ne 0
    sz_paper=defolt(1)
    color=defolt(3) ne 0 & retval(0)=color eq 1
    bpp=fix(alog(float(defolt(4)<8)>0)/alog(2.))
    quality=defolt(5) & retval(2)=quality
    print=defolt(6) ne 0 & retval(5)=print
    non_encaps=defolt(2) ne 0 & retval(4)=non_encaps
  endif
  add=bytarr(nel_add>1)
  if nel_add gt 0 then retval(6:*)=additional_flag.value
  
  if non_encaps eq 1 then begin
    ps_pos=strpos(/reverse_search,ps_nam,'.ps')
    if ps_pos(0) ne -1 then ps_nam=strmid(ps_nam,0,ps_pos)+'.eps'
  endif
  
  old_dev=!d.name
  
  if n_elements(size_margins) eq 0 then $
    size_margins=size_offset(/no_x,no_center=no_center)
  if total(sz_xy) ge 1e-5 then size_margins(0:1)=sz_xy
  if not keyword_set(no_x) then begin ;create widget
    
    base=widget_base(title = "Save PostScript file ",/frame,/column, $
                     group_leader=group_leader, $
                     modal=n_elements(group_leader) eq 1)
    sub_base=widget_base(base,/row)
    lft=widget_base(sub_base,/col)
    rgt=widget_base(sub_base,/col)
    sub_base=widget_base(lft,/row)
    pl_button=cw_bgroup(sub_base,['Portrait','Landscape'],/exclusive,/row, $
                        set_value=portrait,/frame,uvalue='PL')
    sub_base=widget_base(rgt,/row)
    a4_button=cw_bgroup(sub_base,['A4','A3','A2','A1','A0','US'], $
                        /exclusive,/col, $
                        set_value=sz_paper,/frame,uvalue='A4')
    sub_base=widget_base(lft,/row)
    szx_field=cw_field(sub_base,title='Size:   ',/float,xsize=6,/all_events, $
                       value=size_margins(0),uvalue='SZX')
    szy_field=cw_field(sub_base,title='x ',/float,xsize=6,/all_events, $
                       value=size_margins(1),uvalue='SZY')
    sub_base=widget_base(lft,/row)
    offx_field=cw_field(sub_base,title='Offset: ',/float,xsize=6,/all_events, $
                        value=size_margins(2),uvalue='OFFX')
    offy_field=cw_field(sub_base,title='x ',/float,xsize=6,/all_events, $
                        value=size_margins(3),uvalue='OFFY')
    sub_base=widget_base(base,/row)
    enc_button=cw_bgroup(sub_base,['Non-Encaps.','Encaps.'],/exclusive,/row, $
                        set_value=non_encaps,/frame,uvalue='ENC')
    clr_button=cw_bgroup(sub_base,['Color','B/W'],/exclusive,/row, $
                        set_value=color,/frame,uvalue='CLR')
    sub_base=widget_base(base)
    bpp_button=cw_bgroup(sub_base,['1','2','4','8'],/exclusive,/row, $
                         set_value=bpp,uvalue='BPP', $
                         label_left='Bits per pixel: ')
;    sub_base=widget_base(base)
;    qual_button=cw_bgroup(sub_base,['low','medium','high'],/exclusive,/row, $
;                          set_value=quality,label_left='Quality: ', $
;                          uvalue='QUAL')
    sub_base=widget_base(base)
    name_field=cw_field(sub_base,title='Filename: ',/string,xsize=32, $
                        /all_events,value=ps_nam,uvalue='NAME')
    sub_base=widget_base(base,/row)
    gs_button=cw_bgroup(sub_base,['View with GhostScript'],/nonexclusive, $
                        /row,uvalue='GS')
    prt_button=cw_bgroup(sub_base,['Send to printer'],/nonexclusive,/row, $
                         uvalue='PRT',set_value=print)
    add_button=lonarr(nel_add>1)
    if nel_add gt 0 then begin
      for i=0,n_elements(additional_flag)-1 do $
        add_button(i)=cw_bgroup(base,additional_flag(i).text,/nonexclusive, $
                                set_value=additional_flag(i).value,/row, $
                                uvalue=strcompress(string(i),/remove_all))
    endif
    ok_button=cw_bgroup(base,['  Okay  ',' Cancel'],/row,uvalue='OK')

    widget_control,base,/realize
    if fix(!version.release) lt 5 then xmanager,'ps_widget',base,/modal $
    else xmanager,'ps_widget',base,group_leader=group_leader
  endif else begin              ;no_x

  endelse


                                ;setting postscript parameters
  if retval(1) ne 1 then begin
    set_plot,'ps'
    if non_encaps eq 1 then preview=1 else preview=0
    preview=0
    if fix(!version.release) lt 6 then begin
      device,color=color eq 0,landscape=portrait eq 1, $
        encapsulated=non_encaps eq 1,bits=2^((bpp>0)<3), $
        xsize=size_margins(0),ysize=size_margins(1),preview=preview, $
        xoffset=size_margins(2),yoffset=size_margins(3),/isolatin1
    endif else begin
      device,color=color eq 0,landscape=portrait eq 1, $
      encapsulated=non_encaps eq 1,bits=2^((bpp>0)<3), $
      xsize=size_margins(0),ysize=size_margins(1),preview=preview, $
      xoffset=size_margins(2),yoffset=size_margins(3),/isolatin1, $
        language_level=2
    endelse
    if not keyword_set(no_file) then begin
      if non_encaps eq 1 then begin
        ps_pos=strpos(/reverse_search,ps_nam,'.ps')
        if ps_pos(0) ne -1 then ps_nam=strmid(ps_nam,0,ps_pos)+'.eps'
      endif
      device,filename=ps_nam
    endif
    retval(4)=non_encaps
    retval(0)=color eq 1
    retval(2)=quality
    retval(5)=print
    psname=ps_nam
    sz=size_margins(0:1)
    sz_xy=sz
  endif
  
  
  if nel_add gt 0 then addflag_ret=retval(6:*) else addflag_ret=-1
  
  help,/device,output=dummy
  return,retval
end
