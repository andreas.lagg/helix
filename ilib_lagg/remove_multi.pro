;+
; NAME:
;    remove_multi
;
;
; PURPOSE:
;    removes multiple occurances of elements in an array
;
;
; CATEGORY:
;    reading EPD-Data
;
;
; CALLING SEQUENCE:
;    vec=remove_multi(start_arr,no_empty=no_empty)
;
; 
; INPUTS:
;    start_arr - array from which multiple numbers/strings should be removed
;
;
; OPTIONAL INPUTS:
;    no_empty - if set, emty strings are not returned
;
;	
; KEYWORD PARAMETERS:
;
;
;
; OUTPUTS:
;    vec - vector containing all different elements of start_array
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;    A. Lagg, 24/04/1996
;-

function remove_multi,start_array,no_empty=no_empty,index=index, $
                      rev_index=rev_index

  ret_vec=start_array(0)
  typ=size(ret_vec)
  typ=typ(1)
  index=0l
  rev_index=-1l
  if n_elements(start_array) gt 1 then begin
    for i=1l,n_elements(start_array)-1 do begin
      ok=where(ret_vec eq start_array(i))
      if ok(0) eq -1 then begin
        ret_vec=[ret_vec,start_array(i)]
        index=[index,i]
      endif else rev_index=[rev_index,i]
    endfor
    if typ eq 7 and n_elements(no_empty) gt 0 then begin
      emt=where(ret_vec ne '')
      if emt(0) ne -1 then ret_vec=ret_vec(emt)
    endif
  endif
  if n_elements(rev_index) gt 1 then rev_index=rev_index(1:*)
  return,ret_vec
end
