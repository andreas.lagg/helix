pro dlm_test,dlm,error
  
end

pro so_test,so,error
  
  error=0b
  
  
end


;test if the various shared objects work...
pro lib_test,all=all,cfitsio=cfitsio,helix=helix,ana=ana
  
  
  if keyword_set(all) then begin
    cfitsio=1 & helix=1 & ana=1
  endif
  
  dlm=''
  so=''
  
  if keyword_set(helix) then begin
    so=[so,'call_pikaia.so','compute_profile.so']
    dlm=[dlm,'fits']
  endif
  if keyword_set(ana) then begin
    dlm=[dlm,'f0']
  endif
  if keyword_set(cfitsio) then begin
  endif
  
  dlm_test,dlm,err_dlm
  so_test,so,err_so
  
  stop  
    
end
