                                ;cursor position in all plots
pro image_cursor,xdev,ydev,x0=x0,y0=y0,x1=x1,y1=y1,szc=szc,color=color,show=show
  common icoord,icoord
  common cursor,curs,oldcurs
  
  
  if n_elements(icoord) eq 0 then begin
    icrd={x0:x0,y0:y0,x1:x1,y1:y1}
  endif else icrd=icoord
  nc=n_elements(icrd.x0)
  if n_elements(show) eq 0 then rshow=bytarr(nc)+1b $
  else if n_elements(show) eq 1 then rshow=bytarr(nc)+show(0) $
  else rshow=show
  if (size(rshow))(0) ge 2 then $
    rshow=reverse(rshow,2) ;order on screen is reversed to array index order
  if n_elements(oldcurs) eq nc then for ic=0,nc-1 do begin
    tv,/device,true=3, $
      oldcurs(ic).colx,min(oldcurs(ic).xvec),oldcurs(ic).y
    tv,/device,true=3, $
      oldcurs(ic).coly,oldcurs(ic).x,min(oldcurs(ic).yvec)
  endfor
  
  rg=(where(icrd.x0 le xdev and icrd.x1 ge xdev and $
            icrd.y0 le ydev and icrd.y1 ge ydev ))(0)
  if rg lt 0 then return
  
  if n_elements(szc) eq 0 then szc=4
  new_curs=0
  if n_elements(curs) eq 0 then new_curs=1
  if n_elements(curs) ne nc then new_curs=1
  if new_curs then $
    curs=replicate({x:0,y:0,colx:intarr(szc*2+1,1,3), $
                    coly:intarr(1,szc*2+1,3), $
                    xvec:intarr(szc*2+1),yvec:intarr(szc*2+1)},nc)
  
  x=xdev-icrd.x0(rg)+icrd.x0
  y=ydev-icrd.y0(rg)+icrd.y0
  
  
  if max(x) lt !d.x_size and max(y) lt !d.y_size then $
    for ic=0,nc-1 do if rshow(ic) then begin
    curs(ic).x=x(ic)
    curs(ic).y=y(ic)
    sx=((fix(curs(ic).x)+indgen(2*szc+1)-szc)>0)<(!d.x_size-1)
    sy=((fix(curs(ic).y)+indgen(2*szc+1)-szc)>0)<(!d.y_size-1)
    
    curs(ic).colx=tvrd(min(sx),curs(ic).y,max(sx)-min(sx)+1,1,true=3)
    curs(ic).coly=tvrd(curs(ic).x,min(sy),1,max(sy)-min(sy)+1,true=3)
    
    curs(ic).xvec=sx
    curs(ic).yvec=sy
    
    
    if n_elements(color) eq 0 then col=2 else col=color
    if ic eq rg then col=1
    plots,/device,color=col, $
      curs(ic).xvec,curs(ic).y+intarr(szc*2+1)
    plots,/device,color=col, $
      curs(ic).x+intarr(szc*2+1),curs(ic).yvec
  endif
  oldcurs=curs
end
