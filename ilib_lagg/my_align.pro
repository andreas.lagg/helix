pro my_align,oimgtip,oimghin,trans=trans,flip=flip,mirror=mirror,box=box, $
             xarr=xarr_tr,yarr=yarr_tr,xyret=xyret,rotate=rotate, $
             amoeba=amoeba,powell=powell,guess=guess,norot=norot
  
  imgtip=oimgtip
  imghin=oimghin
  
  tsz=size(imgtip)
  tnx=tsz(1) & tny=tsz(2)
  hsz=size(imghin)
  hnx=hsz(1) & hny=hsz(2)
  
  window,0,xsize=max([tnx,hnx]),ysize=tny+hny
  loadct,0
  
  flip=keyword_set(flip)
  mirror=keyword_set(mirror)
  rotate=keyword_set(rotate)
  absval=0
  ok=0
  print,'Flipping the TIP image is necessary, at least for the data of May 2005. No check was done so far why this is so.'
  print,'Using the Abs-Value of the TIP magnetigram is helpful for alignment with SUMER continuum image.'
  print,'Press ''F'' to flip image, ''M'' to mirror image, ''R'' to rotate, ''A'' to use ABS(img),' + $
    ' ''C'' to continue'
  repeat begin                  ;flip tip image? abs of image
    erase
    print,'Flip=',flip,', Mirror=',mirror,', Rotate=',rotate,', Abs=',absval
    if flip eq 1 then imgtip=reverse(oimgtip,2) else imgtip=oimgtip
    if mirror eq 1 then imgtip=reverse(imgtip,1)
    if rotate eq 1 then imgtip=rotate(imgtip,1)     
    if absval then imgtip=abs(imgtip)
    tvscl,imghin
    tvscl,imgtip,0,hny,/device
    gk=strupcase(get_kbrd())
    if gk eq 'F' then flip=~flip      ;NOT operator
    if gk eq 'M' then mirror=~mirror  ;NOT operator
    if gk eq 'R' then rotate=~rotate  ;NOT operator
    if gk eq 'A' then absval=~absval  ;NOT operator
    if gk eq 'C' then ok=1
  endrep until ok
  tsz=size(imgtip)
  tnx=tsz(1) & tny=tsz(2)
  
                                ;select region from mdi image
  llx=0 & lly=0 & urx=hnx-1 & ury=hny-1
  if 1 eq 0 then begin          ;deactivate region selection. This is
                                ;only needed for full disk MDI. It is
                                ;better to preselect the region within
                                ;the reference image manually. This is
                                ;making the transformation easier
                                ;since no offsets are required then.
    ok=0
    repeat begin
      erase
      tvscl,imghin
      tvscl,imgtip,0,hny,/device
      userlct
      plots,color=1,[0,max([tnx,hnx])],[hny,hny]
      print,'Click on Lower Left boundary'
      cursor,llx,lly,/down,/device
      lly=lly<(hny-1)
      print,'Click on Upper Right boundary'
      cursor,urx,ury,/down,/device
      ury=ury<(hny-1)
      plots,color=1,[llx,llx,urx,urx,llx],[lly,ury,ury,lly,lly],/device
      print,'Box: x=[',llx,lly,'], y=[',urx,ury,']'
      print,'Happy with selection?'
      gk=get_kbrd()
      if strupcase(gk) eq 'Y' then ok=1
    endrep until ok
    imghin=imghin(llx:urx,lly:ury)
  endif
  wdelete,0
  loadct,0
  
  
;  trans_img=imghin
;  ref_img=imgtip
  trans_img=imgtip
  ref_img=imghin
  
                                ;remove NAN from tip image
  nofin=where(finite(trans_img) eq 0)
  if nofin(0) ne -1 then trans_img(nofin)=min(trans_img,/nan)
  
                                ;determine initial guess
  newguess=1
  if n_elements(guess) ne 0 then begin
    print,'Determine new initial guess or use previous data [N/p] ?'
    repeat tkey=strupcase(get_kbrd()) until tkey ne ''
    if tkey eq 'P' then begin
      newguess=0
      pin=guess.p
      qin=guess.q
    endif
  endif
  if newguess then begin
;  pp = setpts_roi(trans_img,ref_img,/restrict_fov)
    setpts,pp,trans_img,ref_img,plotonly=plotonly, $
      noscale=noscale,append=append,trima=trima,key=key
    tt = caltrans(pp)
    pin = tt[*,0]
    qin = tt[*,1]
  endif
  
  if keyword_set(powell) then amoeba=0 $
  else if keyword_set(amoeba) then powell=0 $
  else amoeba=1
  
  
  img_shift=aalign_images(trans_img,ref_img,pin,qin,pout,qout, $
                          amoeba=amoeba,powell=powell,norot=norot, $
                          missing=!values.f_nan,scale=0.01)
    
;  box=[llx,lly,urx,ury]
;  help,/st,trans
  
                                ;define x/y arr of transformed image
  szt=size(trans_img)
;  xarr=((intarr(ury-lly+1)+1.) ## indgen(urx-llx+1))
;  yarr=(indgen(ury-lly+1) ## (intarr(urx-llx+1)+1.))
  xarr=(intarr(szt(2))+1.) ## (indgen(szt(1)))
  yarr=(indgen(szt(2)) ## (intarr(szt(1))+1.))
  xarr_tr=poly_2d(xarr,pout,qout,2,tnx,tny,cubic=-0.5,missing=!values.f_nan)
  yarr_tr=poly_2d(yarr,pout,qout,2,tnx,tny,cubic=-0.5,missing=!values.f_nan)
  
  
                                ;create x and y vectors of TIP image
                                ;use shifted/rotated mdi coordinates
;   xx=[llx,urx]/float(hnx)*(xycut(2)-xycut(0))+xycut(0)
;   yy=[lly,ury]/float(hny)*(xycut(3)-xycut(1))+xycut(1)
;   xmdi=congrid(mdist.xstd(xx(0):xx(1),yy(0):yy(1)),urx-llx+1,ury-lly+1,/interp)
;   ymdi=congrid(mdist.ystd(xx(0):xx(1),yy(0):yy(1)),urx-llx+1,ury-lly+1,/interp)
  
;    xhin=(intarr(hsz(2))+1) ## indgen(hsz(1))
;    yhin=indgen(hsz(2)) ## (intarr(hsz(1))+1)
  
;    xhin_tr=poly_2d(xhin,pout,qout,2,tnx,tny,cubic=-0.5,missing=!values.f_nan)
;    yhin_tr=poly_2d(yhin,pout,qout,2,tnx,tny,cubic=-0.5,missing=!values.f_nan)
;    stop
; ;                                 ;convert to original scale
;    xtip=congrid(xhin_tr,sztip(1),sztip(2),/interp)
;    ytip=congrid(yhin_tr,sztip(1),sztip(2),/interp)
;   if flip then ytip=reverse(ytip,2)

                                ;store coordinates to sav-file
;   tipfile=(file_search(tip))(0)
;   tfroot=strmid(tipfile,strpos(tipfile,'/',/reverse_search)+1,11)
;   sav='./sav/align_mdi_'+tfroot+'.sav'
;   mdi_tref=mdist.header.tref
;   save,/xdr,/compress,file=sav,xtip,ytip,mdi_tref
;   print,'Wrote new TIP coordinates to '+sav
  
  
;   userlct,/full
;   slpos=where(byte(mdi) eq (byte('/'))(0),nsl)
;   if nsl gt 1 then mdiroot=strmid(mdi,slpos(nsl-2),strlen(mdi)) $
;   else mdiroot=mdi
; ;  xr=[min(tipst.xstd,/nan),max(tipst.xstd,/nan)]
; ;  yr=[min(tipst.ystd,/nan),max(tipst.ystd,/nan)]
;   xr=[min(xtip(*,0),/nan),max(xtip(*,0),/nan)] ;use 'lowest' x-row
;   yr=[min(ytip(0,*),/nan),max(ytip(0,*),/nan)] ;use 'leftest' y-col
  
;   image_cont_al,tipimg,zrange=[min(tipimg,/nan),max(tipimg,/nan)],xrange=xr,yrange=yr,xtitle='x [arcsec]',ytitle='y [arcsec]',title=tfroot+', aligned with '+mdiroot,cont=0
  
  
  
                                ;blink images to test alignment
  print,'Press key to quit'
  xs=max([(size(img_shift))(1),(size(ref_img))(1)])
  ys=max([(size(img_shift))(2),(size(ref_img))(2)])
  wdelete
  window,0,xsize=xs,ysize=ys    ;float(tny)/tnx*xs
  noval=where(finite(img_shift) eq 0) 
  if noval(0) ne -1 then img_shift(noval)=min(img_shift,/nan)
  repeat  begin
    tvscl,congrid(img_shift,512,float(tny)/tnx*xs,/interp)
    wait,.5
    tvscl,congrid(ref_img,512,float(tny)/tnx*xs,/interp)
    wait,.5
  endrep until get_kbrd(0) ne ''
  wdelete,0
  
                                ;store transormation info to a structure
  pq2rss,pout,qout,erot,exscl,eyscl,exshft,eyshft,enrss,nxt,ny
  xoff=exscl*exshft
  yoff=eyscl*eyshft
  trans={p:pout,q:qout,xshift:exshft,yshift:eyshft,xscale:exscl,yscale:eyscl, $
         rot:erot,tiprotate:rotate,tipflip:flip,tipmirror:mirror, $
         xoff:xoff,yoff:yoff,tipimg:oimgtip,hinimg:oimghin, $
         tipnam:'',hinnam:''}
  
  userlct,/full,coltab=0,/reverse
                                ;I do not know why but I have to add
                                ;the y-shift to x and vice versa! strange.
  xtrans=([0.,0,szt(1),szt(1)])*trans.xscale+trans.xoff
  ytrans=([0.,szt(2),szt(2),0])*trans.yscale+trans.yoff
  xxyy=[[xtrans],[ytrans]]
  cp=cos(-trans.rot*!dtor) & sp=sin(-trans.rot*!dtor)
  rmat=[[cp,sp],[-sp,cp]]
  for i=0,3 do xxyy(i,*)=rmat ## xxyy(i,*)
  xxyy(*,0)=(xxyy(*,0))+llx
  xxyy(*,1)=(xxyy(*,1))+lly
  image_cont_al,contour=0,/aspect,zrange=[-200,200],oimghin
  plots,[xxyy(*,0),xxyy(0,0)],[xxyy(*,1),xxyy(0,1)]
  
  
  xyret=xxyy
end
