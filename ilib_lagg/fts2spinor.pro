pro fts2spinor,fts,vacuum=vacuum, air=air,show=show,wlref=wlref,wlrange=wlrange
  
  if n_params() eq 0 then begin
    message,/cont,'Routine to convert FTS spectrum to spinor format.'
    print,'Usage: fts2spinor,''ftsfile''[,/air],[,/vacuum]'
    print,'FTS-Atlas available at: ftp://nsokp.nso.edu/pub/atlas/'
    reset
  endif
  
  if n_elements(air) eq 0 and n_elements(vacuum) eq 0 then air=1
  air=keyword_set(air)
  if keyword_set(vacuum) then air=0
  if air then print,'Using Wavelength in AIR' $
  else print,'Using Wavelength in VACUUM' 
  
  ffts=file_search(fts,count=cnt)
  
  if cnt eq 0 then begin
    message,/cont,'No FTS spectrum found: '+fts
    reset
  endif
  
  for i=0,cnt-1 do begin
    slpos=(strpos(/reverse_search,ffts(i),'/'))(0)+1
    path=strmid(ffts(i),0,slpos)
    file=strmid(ffts(i),slpos,strlen(ffts(i)))
    fsto=path+file+'_spinor.dat'
    
    fts_read,ffts(i),tel=tel,cor=cor,obs=obs,wlair=wlair,wlvac=wlvac
    
    if keyword_set(air) then wl=wlair else wl=wlvac
    
    dummy=min(cor,imin)
    if n_elements(wlref) eq 0 then wlref=wl(imin)
    print,'Reference WL=',wlref
    
    if n_elements(wlrange) eq 0 then wlrg=[min(wl),max(wl)] $
    else wlrg=wlref+[wlrange(0),wlrange(1)]
    print,'WL-range: ',wlrg
    
    srt=sort(wl)
    wl=wl(srt)
    cor=cor(srt)
    inrg=where(wl ge wlrg(0) and wl le wlrg(1))
    if inrg(0) eq -1 then message,'WL out of range!'
    
    
                                ;write stopro file
    k=n_elements(inrg)
    profile={wl:wl(inrg),i:cor(inrg), $
             q:fltarr(k),u:fltarr(k),v:fltarr(k),ic:1.}
    header='FTS-Spectrum from '+file+' WLRG='+ $
      add_comma(sep='-',n2s(minmaxp(wl(inrg))))
    write_stopro,file=fsto,profile=profile,wlref=wlref, $
      header=header,error=error,/verbose
    if error ne 0 then message,'Could not write '+fsto
    
    if keyword_set(show) then begin
      userlct
      plot,profile.wl,profile.i,title=header, $
        xtitle='Air WL [A]',ytitle='Corr. FTS Spectrum'
      plot,reverse(profile.wl),reverse(profile.i),title=header, $
        xtitle='Air WL [A]',ytitle='Corr. FTS Spectrum'
    endif
  endfor
  
end
