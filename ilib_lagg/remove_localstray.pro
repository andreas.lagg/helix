;remove localstraylight from a 4D data cube and write out a new cube
pro remove_localstray,fits4d,reread=reread
  common fits4d_data,data,hdr,oldfits4d
  
  if (size(fits4d))[0] eq 4 then fdat=ptr_new(fits4d,/no_copy) else begin
    reread=keyword_set(reread) or n_elements(oldfits4d) eq 0
    if reread eq 0 then reread=oldfits4d ne fits4d
    if reread then begin
      data=readfits_ssw(fits4d,hdr,/silent)
    endif
    oldfits4d=fits4d
    fdat=ptr_new(data,/no_copy)
  endelse
  szd=size(*fdat,/dim)
  
  szp=size(psf)
  if szp[0] ne 2 or szp[1] ne szp[2] or szp[1] mod 2 ne 1 then begin
    message,'psf must be a 2D-array of size (2n+1,2n+1) containing the normalized point spread function (total(psf)==1)'
    
  endif
  
  stop
end
