;return information on parameters of inversion like unit, scaling, colortable...
function get_parinfo,par,code,add=add,xy=xyin
  common xfwg,xfwg
@greeklett.pro
  
  pist={name:'',longname:'',shortname:'',unit:'',scaling:1.,colortable:'', $
        colortablerev:0,symm:0}
  
  if n_elements(xyin) ne 0 then xy=xyin
  pi=replicate(pist,n_elements(par))
  for i=0,n_elements(par)-1 do begin
    afact=1.
    pnam=par(i)
    if pnam eq 'Magnetic Field Strength [Gauss]' then pnam='BFIEL'
    if pnam eq 'Magnetic Field Inclination [radians]' then begin
      pnam='GAMMA' & afact=1/!dtor
    endif
    if pnam eq 'Disambiguated Azimuth of Trn. Field [radians]' then begin
      pnam='AZIMU' & afact=1/!dtor
    endif
    if pnam eq 'Estimated Filling Factor' then pnam='ALPHA'   
    word=strupcase(strsplit(pnam,/extract))
    word2=''
    if word(0) eq 'USER' or word(0) eq 'STOKES' then begin
      pnam=(strsplit(pnam,/extract))(0)
      if word(0) eq 'STOKES' and n_elements(word) ge 2 then $
        word2=(strsplit(par(i),/extract))(1)
      xpos=strpos(par(i),'x=',/reverse_search)
      ypos=strpos(par(i),'y=',/reverse_search)
      if xpos ne -1 and ypos ne -1 then begin
        xy=[fix(strmid(par(i),xpos+2,ypos-1-(xpos+2))), $
            fix(strmid(par(i),ypos+2,strlen(par(i))))]
      endif
    endif
    pi(i).name=pnam
    if pnam eq 'TMP' then pnam='TEMPE'
    if pnam eq 'FMAG' then pnam='BFIEL'
    if pnam eq 'CHI' then pnam='AZIMU'
    if pnam eq 'VTURB' then pnam='VMICI'
    if pnam eq 'VEL' then pnam='VELOS'
    case strupcase(pnam) of
      'VELOS': begin
        pi(i).longname='line-of-sight velocity'
        pi(i).shortname='velocity'
        pi(i).unit='km/s'
        pi(i).colortable='velo'
        if code eq 'spinor' then pi(i).scaling=1. else pi(i).scaling=1e-3
      end
      'VGRAD': begin
        pi(i).longname='line-of-sight velocity gradient'
        pi(i).shortname='velocity gradient'
        pi(i).unit='km/s'
        pi(i).colortable='velo'
        if code eq 'spinor' then pi(i).scaling=1. else pi(i).scaling=1e-3
      end
      'AZIMU': begin
        pi(i).longname='azimuth angle'
        pi(i).shortname='azimuth'
        pi(i).unit=f_grad
        pi(i).colortable='azi'
        pi(i).scaling=afact
      end
      'GAMMA': begin
        pi(i).longname='inclination angle'
        pi(i).shortname='inclination'
        pi(i).unit=f_grad
        pi(i).scaling=afact
      end
      'AZISOL': begin
        pi(i).longname='azimuth angle (solar reference frame)'
        pi(i).shortname='azimuth (solar)'
        pi(i).unit=f_grad
        pi(i).colortable='azi'
      end
      'INCSOL': begin
        pi(i).longname='inclination angle (solar reference frame)'
        pi(i).shortname='inclination (solar)'
        pi(i).unit=f_grad
      end
      'BFIEL': begin
        pi(i).longname='magnetic field strength'
        pi(i).shortname='B-field'
        pi(i).unit='G'
      end
      'DBFDZ': begin
        pi(i).longname='magnetic field strength gradient (z)'
        pi(i).shortname='dB/dz'
        pi(i).unit='G/km'
      end
      'DBDLT': begin
        pi(i).longname='magnetic field strength gradient ('+f_tau+')'
        pi(i).shortname='dB/d'+f_tau
        pi(i).unit='G/(log '+f_tau+')'
      end
      'BXSOL': begin
        pi(i).longname='BX (solar reference frame)'
        pi(i).shortname='BX (solar)'
        pi(i).unit='G'
      end
      'BYSOL': begin
        pi(i).longname='BY (solar reference frame)'
        pi(i).shortname='BY (solar)'
        pi(i).unit='G'
      end
      'BZSOL': begin
        pi(i).longname='BZ (solar reference frame)'
        pi(i).shortname='BZ (solar)'
        pi(i).unit='G'
      end
      'EZERO': begin
        pi(i).longname='line center to continuum opacity ratio'
        pi(i).shortname=f_eta+'!L0!N'
      end
      'ALPHA': begin
        pi(i).longname='filling factor'
        pi(i).shortname='FF'
      end
      'VDOPP': begin
        pi(i).longname='doppler broadening'
        pi(i).shortname='doppler width'
      end
      'GDAMP': begin
        pi(i).longname='damping parameter'
        pi(i).shortname='damping'
      end
      'SGRAD': begin
        pi(i).longname='source function gradient'
        pi(i).shortname='S1'
      end
      'SZERO': begin
        pi(i).longname='source function at '+f_tau+'=0'
        pi(i).shortname='S0'
      end
      'VDAMP': begin
        pi(i).longname='damping parameter (Hanle-mode)'
        pi(i).shortname='damping'
      end
      'DSLAB': begin
        pi(i).longname='slab thickness'
        pi(i).shortname='d!Lslab!N'
      end
      'SLHGT': begin
        pi(i).longname='height of slab'
        pi(i).shortname='H!Lslab!N'
      end
      'TEMPE': begin
        pi(i).longname='temperature'
        pi(i).shortname='temperature'
        pi(i).unit='K'
      end
      'TGRAD': begin
        pi(i).longname='temperature gradient'
        pi(i).shortname='TGRAD'
        pi(i).unit='K/log '+f_tau
      end
      'VMICI': begin
        pi(i).longname='micro turbulence'
        pi(i).shortname='mic.turb.'
        pi(i).unit='km/s'
      end
      'VMACI': begin
        pi(i).longname='macro turbulence'
        pi(i).shortname='mac.turb.'
        pi(i).unit='km/s'
      end
      'VLABS': begin
        pi(i).longname='absolute velocity shift'
        pi(i).shortname='abs.vel.'
        pi(i).unit='km/s'
      end
      'DENS': begin
        pi(i).longname='density'
        pi(i).shortname='density'
        pi(i).unit=''
      end
      'PRESS': begin
        pi(i).longname='gas pressure'
        pi(i).shortname='pressure'
        pi(i).unit=''
      end
      'PEL': begin
        pi(i).longname='electron pressure'
        pi(i).shortname='e!U-!Npress'
        pi(i).unit=''
      end
      'CHISQ': begin
        pi(i).longname=f_chi+'!U2!N'
        pi(i).shortname='chisqr'
      end
      'CCORR': begin
        pi(i).longname='Continuum Correction'
        pi(i).shortname='CCORR'
      end
      'KAPPA':
      'HEIGHT':
      'LTINC':
      'LTTOP':
      'LGTRF':
      'THETA':
      'KCWLR':
      'LOGGF': begin
        pi(i).longname='log(gf)'
        pi(i).shortname='log(gf)'
      end
      'STOKES': begin
        pi(i).colortable='stokes'
        pi(i).longname='Stokes '+word2
        pi(i).shortname=word2
        pi(i).symm=max(['Q','U','V'] eq word2)
      end
      'STOKES IC': begin
        pi(i).colortable='stokes'
        pi(i).longname='Stokes Ic'
        pi(i).shortname='Ic'
      end
      'STOKES I': begin
        pi(i).colortable='stokes'
        pi(i).longname='Stokes I'
        pi(i).shortname='I'
      end
      'STOKES Q': begin
        pi(i).colortable='stokes'
        pi(i).longname='Stokes Q'
        pi(i).shortname='Q'
        pi(i).symm=1
      end
      'STOKES U': begin
        pi(i).colortable='stokes'
        pi(i).longname='Stokes U'
        pi(i).shortname='U'
        pi(i).symm=1
      end
      'STOKES V': begin
        pi(i).colortable='stokes'
        pi(i).longname='Stokes V'
        pi(i).shortname='V'
        pi(i).symm=1
      end
      'FITNESS': begin
        pi(i).longname='Fitness'
        pi(i).shortname='fitness'
      end
      'CHISQ': begin
        pi(i).longname=f_chi+'!E2!N'
        pi(i).shortname=f_chi+'!E2!N'
      end
      'LINE_STRENGTH': begin
        pi(i).longname='Line Strength'
        pi(i).shortname='Line Strength'
      end
      'LINE_STRENGT': begin
        pi(i).longname='Line Strength'
        pi(i).shortname='Line Strength'
      end
      'BLEND_WL': begin
        pi(i).longname='blend WL'
        pi(i).shortname=f_lambda+'!LBlend!N'
      end
      'BLEND_WIDTH': begin
        pi(i).longname='blend width'
        pi(i).shortname=f_delta+f_lambda+'!LBlend!N'
      end
      'BLEND_A0': begin
        pi(i).longname='blend amplitude'
        pi(i).shortname='A!L0,Blend!N'
      end
      'VINST': begin
        pi(i).longname='instrumental broadening: voigt width'
        pi(i).shortname='VINST'
      end
      'AINST': begin
        pi(i).longname='instrumental broadening: voigt damping'
        pi(i).shortname='AINST'
      end
      'USER': begin
        if n_elements(xy) eq 2 then begin
          pi(i).shortname=xfwg.p(xy(0),xy(1)).user.name
          pi(i).longname=xfwg.p(xy(0),xy(1)).user.title
          pi(i).unit=xfwg.p(xy(0),xy(1)).user.unit
          pi(i).colortable=xfwg.p(xy(0),xy(1)).user.colortable
        endif else print,'Undefined USER parameter information.'
      end
      else: begin
        print,'no parinfo for '+par(i)
      end
    end
    if pi(i).unit ne '' then pi(i).unit='['+pi(i).unit+']'
  endfor
  
  return,pi
end
