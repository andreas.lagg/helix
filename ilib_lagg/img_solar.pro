;plot tip or other images in solar coordinates.
pro img_solar,tip=tip,ps=ps,mdi=mdi,sumer=sumer, $
              box=box,lonlat=lonlat,arcsec=arcsec, $
              solar=solar,soho=soho, $
              helium=helium,silicon=silicon, $
              magnetogram=magnetogram,continuum=continuum, $
              noerase=noerase,cwl=cwl, $
              ret_tipst=tipst,ret_mdist=mdist,ret_sumst=sumst, $
;              imgtip=tipimg,imgmdi=mdiimg,imgsum=sumimg
  imgtip=imgtip,imgmdi=imgmdi,imgsum=imgsum
  common continue,continue
  
  
;  if n_elements(tip) eq 0 then tip='/media/LACIE/VTT-May05/19May05/19may05.007-01cc'
;  if n_elements(mdi) eq 0 then mdi='/media/LACIE/VTT-May05/soi-ftp.stanford.edu/data/lagg\@mps.mpg.de/fd_M_01h.108513/0007.fits'
;  if n_elements(sumer) eq 0 then sumer='/media/LACIE/SUMER_May05/list_c4_1_050519_data_1548.2311.sav'
;  if n_elements(box) ne 4 then box=[ 420,-280.,560,-230]
; if n_elements(box) ne 4 then box=[ 400,-300.,580,-150]
; if n_elements(box) ne 4 then box=[-1000,-1000.,1000,1000]
  arcsec=keyword_set(arcsec)
  lonlat=keyword_set(lonlat)
  if arcsec eq 0 and lonlat eq 0 then arcsec=1
  
  
  ftip=n_elements(tip) ne 0
  fsum=n_elements(sumer) ne 0
  fmdi=n_elements(mdi) ne 0
  
  if total([ftip,fsum,fmdi]) eq 0 then $
    message,'Please specify tip, sumer or mdi data file.'
  
  szx=0 & szy=0
  if ftip then begin
    tipimg=get_tipimg(tip,helium=helium,silicon=silicon,cwl=cwl, $
                      magnetogram=magnetogram,continuum=continuum)
    tipst=c2solar_tipst(tip,solar=solar,soho=soho)
    szx=szx>(max(tipst.xstd)-min(tipst.xstd))
    szy=szy>(max(tipst.ystd)-min(tipst.ystd))
  endif
  if fmdi then begin
    mdist=c2solar_mdist(mdi,img=mdiimg,solar=solar,soho=soho)
    szx=szx>(max(mdist.xstd)-min(mdist.xstd))
    szy=szy>(max(mdist.ystd)-min(mdist.ystd))
  endif
  if fsum then begin
    sumst=c2solar_sumst(sumer,img=sumimg,solar=solar,soho=soho)
    szx=szx>(max(sumst.xstd)-min(sumst.xstd))
    szy=szy>(max(sumst.ystd)-min(sumst.ystd))
  endif
    
;  tipcut=cut_box(tipst,box=box,/arcsec)
;  mdicut=cut_box(mdist,box=box,/arcsec)
;  sumcut=cut_box(sumst,box=box,/arcsec)
  
  if ftip then psfile='tip_'+strmid(tip,strpos(tip,'/',/reverse_search)+1,11) $
  else if fsum then begin
    slpos=strpos(sumer,'/',/reverse_search)
    psfile='sumer_'+strmid(sumer,slpos+1,strlen(sumer)-slpos-5)
  endif else if fmdi then begin
    slpos=where(byte(mdi) eq (byte('/'))(0))
    slpos=slpos(n_elements(slpos)-2:n_elements(slpos)-1)
    psfile='mdi_'+strmid(mdi,slpos(0)+1,slpos(1)-slpos(0)-1)+'_'+ $
      strmid(mdi,slpos(1)+1,strlen(mdi)-slpos(1)-6)
  endif
  
                                ;determine aspect ratio from image size
  if n_elements(box) eq 4 then begin
    szx=box(2)-box(0)
    szy=box(3)-box(1)    
  endif
  xs=20.
  ys=float(szy)/szx*xs < 28.
  
  if n_elements(continue) eq 0 then continue=0
  if continue eq 0 then $
    psset,ps=ps,size=[xs,ys],file='./ps/'+psfile+'.ps',/no_x
  userlct,/full,coltab=0,glltab=0,/reverse
  
  soho=keyword_set(soho)
  solar=keyword_set(solar)
  if soho eq 0 and solar eq 0 then solar=1
  if solar then csys='Solar'
  if soho then csys='SOHO'
  
  if arcsec eq 1 then begin
    if ftip then begin
      rotimg=piximg(img=tipimg,x=tipst.xstd,y=tipst.ystd,range=range,/nan, $
                    box=box)
      
      xy=cut_box(tipst,box=box,/arcsec)
      imgtip=tipimg(xy(0):xy(2),xy(1):xy(3))
      imgtip=rotimg
      zrg=min_max(rotimg,percent=2.)
      
      image_cont_al,rotimg,xrange=range([0,2]),yrange=range([1,3]), $
        zrange=zrg,/aspect, $
        xtitle=csys+' x [arcsec]',ytitle=csys+' y [arcsec]',title=tip, $
        ztitle='Intensity',cont=0
      xyouts,0,0,/normal,tipst.date+',  '+add_comma(tipst.time,sep='-')
    endif
    
    if fsum then begin
      rotimg=piximg(img=sumimg,x=sumst.xstd,y=sumst.ystd,range=range,/nan, $
                    box=box)
      xy=cut_box(sumst,box=box,/arcsec)
      imgsum=sumimg(xy(0):xy(2),xy(1):xy(3))
      imgsum=rotimg
      zrg=min_max(rotimg,percent=2.)
      image_cont_al,rotimg,xrange=range([0,2]),yrange=range([1,3]), $
        zrange=zrg,/aspect, $
        xtitle=csys+' x [arcsec]',ytitle=csys+' y [arcsec]',title=sumer, $
        ztitle='Intensity',cont=0
      xyouts,0,0,/normal,sumst.date+',  '+add_comma(sumst.time,sep='-')
    endif
    
    if fmdi then begin
      rotimg=piximg(img=mdiimg,x=mdist.xstd,y=mdist.ystd,range=range,/nan, $
                    box=box)
      xy=cut_box(mdist,box=box,/arcsec)
      imgmdi=mdiimg(xy(0):xy(2),xy(1):xy(3))
      imgmdi=rotimg
      zrg=min_max(rotimg,percent=2.)
      image_cont_al,rotimg,xrange=range([0,2]),yrange=range([1,3]), $
        zrange=zrg,/aspect, $
        xtitle=csys+' x [arcsec]',ytitle=csys+' y [arcsec]',title=mdi, $
        ztitle='Intensity',cont=0
      xyouts,0,0,/normal,mdist.date+',  '+add_comma(mdist.time,sep='-')
    endif
  endif
  
  if lonlat eq 1 then begin
    
;    box=[27,-17,39,-14]
    if ftip then begin
      rotimg=piximg(img=tipimg,x=tipst.lon,y=tipst.lat,range=range,/nan, $
                    box=box)
      xy=cut_box(tipst,box=box,/lonlat)
      imgtip=tipimg(xy(0):xy(2),xy(1):xy(3))
      imgtip=rotimg
      zrg=min_max(rotimg,percent=2.)
      image_cont_al,rotimg,xrange=range([0,2]),yrange=range([1,3]), $
        zrange=zrg,/aspect, $
        xtitle='Longitude [�]',ytitle='Latitude [�]',title=tip, $
        ztitle='Intensity',cont=0
      xyouts,0,0,/normal,tipst.date+',  '+add_comma(tipst.time,sep='-')
    endif
    
    if fsum then begin
      rotimg=piximg(img=sumimg,x=sumst.lon,y=sumst.lat,range=range,/nan, $
                    box=box)
      xy=cut_box(sumst,box=box,/lonlat)
      imgsum=sumimg(xy(0):xy(2),xy(1):xy(3))
      imgsum=rotimg
      zrg=min_max(rotimg,percent=90.)
      image_cont_al,rotimg,xrange=range([0,2]),yrange=range([1,3]), $
        zrange=zrg,/aspect, $
        xtitle='Longitude [�]',ytitle='Latitude [�]',title=sumer, $
        ztitle='Intensity',cont=0
      xyouts,0,0,/normal,sumst.date+',  '+add_comma(sumst.time,sep='-')
    endif
    
    if fmdi then begin
      rotimg=piximg(img=mdiimg,x=mdist.lon,y=mdist.lat,range=range,/nan, $
                    box=box)
      xy=cut_box(mdist,box=box,/lonlat)
      imgmdi=mdiimg(xy(0):xy(2),xy(1):xy(3))
      imgmdi=rotimg
      zrg=min_max(rotimg,percent=2.)
      image_cont_al,rotimg,xrange=range([0,2]),yrange=range([1,3]), $
        zrange=zrg,/aspect, $
        xtitle='Longitude [�]',ytitle='Latitude [�]',title=mdi, $
        ztitle='Intensity',cont=0
      xyouts,0,0,/normal,mdist.date+',  '+add_comma(mdist.time,sep='-')
    endif
  endif
  
  if keyword_set(noerase) eq 0 then begin
    psset,/close
    continue=0
  endif else continue=1
end  

pro sumtip
  
  ps=1
  tipdir='/media/LACIE/VTT-May05/'
  sumdir='/media/LACIE/SUMER_May05/'
  mdidir='/media/LACIE/VTT-May05/soi-ftp.stanford.edu/data/lagg\@mps.mpg.de/'
  
;   img_solar,ps=ps,box=[ -400,-400.,400,400], $
;     tip=tipdir+'17May05/17may05.003-01cc', $
;     sumer=sumdir+'list_c4_1_050517_data_1548.2311.sav', $
;     mdi=mdidir+'fd_M_01h.108464/0022.fits'
  
;    img_solar,ps=ps,box=[ 400,-300.,580,-150], $
;      tip=tipdir+'19May05/19may05.007-01cc', $
;      sumer=sumdir+'list_c4_1_050519_data_1548.2311.sav', $
;      mdi=mdidir+'fd_M_01h.108513/0007.fits'
  
;    img_solar,ps=ps,box=[ 400,-300.,580,-150], $
;      tip=tipdir+'19May05/19may05.009-01cc', $
;      sumer=sumdir+'list_c4_1_050519_data_1548.2311.sav', $
;      mdi=mdidir+'fd_M_01h.108513/0007.fits'
  
;    img_solar,ps=ps,box=[ 400,-300.,580,-150], $
;      tip=tipdir+'19May05/19may05.014cc', $
;      sumer=sumdir+'list_c4_1_050519_data_1548.2311.sav', $
;      mdi=mdidir+'fd_M_01h.108513/0007.fits'
  
  img_solar,ps=ps,box=[ 400,-300.,580,-150], $
    tip=tipdir+'19May05/19may05.004cc', $
    sumer=sumdir+'list_c4_1_050519_data_1548.2311.sav', $
    mdi=mdidir+'fd_M_01h.108513/0007.fits'

  img_solar,ps=ps,box=[ 400,-300.,580,-150], $
    tip=tipdir+'19May05/19may05.002-01cc', $
    sumer=sumdir+'list_c4_1_050519_data_1548.2311.sav', $
    mdi=mdidir+'fd_M_01h.108513/0007.fits'
end

pro sumer,lonlat=lonlat,arcsec=arcsec,ps=ps
  
  files=file_search('/media/LACIE/SUMER_May05/*_c2*.sav')
  
  for i=0,n_elements(files)-1 do begin
    img_solar,ps=ps,lonlat=lonlat,arcsec=arcsec,sumer=files(i),/soho
  endfor
  
end


pro all,ps=ps
  
  tipdir='/media/LACIE/VTT-May05/'
  sumdir='/media/LACIE/SUMER-May05/'
  mdidir='/media/LACIE/MDI-May05/'
  
  tip=['20May05/20may05.002-01cc','20May05/20may05.004-01cc','20May05/20may05.008cc','19May05/19may05.007-01cc','19May05/19may05.009-01cc','19May05/19may05.002-01cc','18May05/18may05.010-01cc','18May05/18may05.007-01cc','18May05/18may05.003-01cc']
  sum=['list_c2_a_050520_data_1037.6622.sav','list_c2_a_050520_data_1037.6622.sav','list_c2_a_050520_data_1037.6622.sav','list_c2_a_050519_data_1037.6622.sav','list_c2_a_050519_data_1037.6622.sav','list_c2_a_050519_data_1037.6622.sav','list_c2_a_050518_data_1037.6622.sav','list_c2_a_050518_data_1037.6622.sav','list_c2_a_050518_data_1037.6622.sav']
  mdi=['fd_M_01h.108540/0048.fits','fd_M_01h.108540/0048.fits','fd_M_01h.108540/0048.fits','fd_M_01h.108514/0000.fits','fd_M_01h.108515/0000.fits','fd_M_01h.108513/0005.fits','fd_M_01h.108491/0000.fits','fd_M_01h.108490/0000.fits','fd_M_01h.108488/0000.fits']
  
  
  tip='19May05/19may05.002-01cc'
  mdi='fd_M_01h.108513/0005.fits'
  sum='res_C_IV_1548_050519.sav'

  for i=0,n_elements(tip)-1 do begin
;  for i=1,1 do begin
    if n_elements(boxtip) ne 0 then dummy=temporary(boxtip)
    if n_elements(boxsum) ne 0 then dummy=temporary(boxsum)
    
                                ;display images to get box-size
    img_solar,ps=0,box=boxtip,tip=tipdir+tip(i),/soho
    img_solar,ps=0,box=boxsum,sum=sumdir+sum(i),/soho
    
    box=[min([boxtip(0),boxsum(0)]),min([boxtip(1),boxsum(1)]), $
         max([boxtip(2),boxsum(2)]),max([boxtip(3),boxsum(3)])]
                                ;add 20%
    add=0.05
    box=[box(0)-add*(box(2)-box(0)),box(1)-add*(box(3)-box(1)), $
         box(2)+add*(box(2)-box(0)),box(3)+add*(box(3)-box(1))]
    
    img_solar,ps=ps,box=box,tip=tipdir+tip(i),/silicon,/mag,/noerase, $
      imgtip=magtip,/soho
    img_solar,ps=ps,box=box,/continuum,tip=tipdir+tip(i), $
;      sum=sumdir+sum(i), $
    mdi=mdidir+mdi(i),imgmdi=mdiimg,/soho
    
    nofin=where(finite(magtip) eq 0)
    if nofin(0) ne -1 then magtip(nofin)=min(magtip,/nan)
   align_tip_mdi,tip=magtip,mdi=mdiimg
  endfor

end


