;create a gaussian for a spectral PSF
pro psf_gauss,fwhm=fwhm,sigma=sigma
  
  wlrange=120d                   ;in \AA
  wlbin=6d-3
  
  nwl=long(wlrange/wlbin)/2*2
  print,'NWL: ',nwl
  print,'WL-Range: ',wlrange
  print,'WL-Bin: ',wlbin
  if nwl ge 2l^15 then message,'HeLIx does not like more WL points than 32767.'
  
  wlvec=((dindgen(nwl)-nwl/2d)/nwl)*wlrange
  
  if n_elements(fwhm) ne 0 then begin
    sigma=fwhm/(2*sqrt(2*alog(2))) 
    fname='./convolve/gauss_fwhm'+n2s(fwhm,format='(e10.3)')+'.dat'
  endif else begin
    fwhm=2*sqrt(2*alog(2))*sigma
    fname='./convolve/gauss_sigma'+n2s(sigma,format='(e10.3)')+'.dat'
  endelse
  gauss=exp(-(wlvec/sigma)^2/2)
  gauss=gauss/max(gauss)
  
  plot,wlvec,gauss,xrange=[-1,1]*2*fwhm
  oplot,color=1,fwhm*[-.5,.5],[0.5,0.5]
  
                                ;write out function
  openw,unit,/get_lun,fname
  printf,unit,';Gaussian, FWHM ='+n2s(fwhm,format='(e10.3)')
  printf,unit,';Gaussian, sigma='+n2s(sigma,format='(e10.3)')
  printf,unit,format='(a,a15,a16)',';','WL','Value'
  printf,unit,format='(2e16.6)',transpose([[wlvec],[gauss]])
  free_lun,unit
  print,'Wrote Gaussian to file: ',fname
end
