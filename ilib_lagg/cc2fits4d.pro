;example:
;cc2fits4d,dir='/data/slam/GREGOR-data/GRIS/2014/GREGOR-May14/03may14/',mask='03may14.012.fitscc',psf='/home/lagg/work/projects/Gregor/GRIS/PSF/gregor_psf_lor0.8_gauss0.25.fits'  
pro cc2fits4d,dir=dir,mask=mask,outdir=outdir,psffile=psffile,wl=wl,overwrite=overwrite, $
              format=format
  
  if n_elements(dir) eq 0 or n_elements(mask) eq 0 then begin
    print,'Usage example: '
    print,'   cc2fits4d,dir=''/data/slam/GREGOR-data/GREGOR-Sep14/gris/11sep14/'', $'
    print,'      mask=''11sep14.012*cc'''
    print,'Keywords:'
    print,'psffile=''filename.fits'' ;fits file containing 2d PSF (odd dimension)'
    print,'         ;if set, write out local straylight removed data set assuming'
    print,'           a PSF defined in the fits file'
    print,'Optional:'
    print,' outdir=''~/tmp'' - specify output directory'
    reset
  endif
;  dir='/data/slam/GREGOR-data/GREGOR-Sep14/gris/08sep14/'
;  mask='08sep14.005-??cc'
  
  
  dryrun=0
  
  if n_elements(outdir) eq 1 then odir=outdir else odir='/home/lagg/tmp/'
  
  out=strmid(mask,0,11)+'.4d.fits'
  nols_out=strmid(mask,0,11)+'.4d.noLS.fits'
  
  
;  out='08sep14.005.4d.fits'
;  nols_out='08sep14.005.4d.noLS.fits'
  common data,ccdata
  read_data,dir=dir,mask=mask,wl=wl,format=format
  nwl=ccdata.nwl
                                ;cc-files:
  if strmid(ccdata.fits[0],strlen(ccdata.fits[0])-2,2) eq 'cc' then begin
    data=float(transpose(ccdata.sp,[0,2,3,1])) 
  endif else begin
                                ;determine data order automatically
    ii=size(ccdata.sp,/dim)
    iord=[where(ii eq ccdata.nwl),where(ii eq 4), $
          where(ii eq ccdata.nx),where(ii eq ccdata.ny)]
    if min(iord eq [0,1,2,3]) eq 1 then data=ccdata.sp $
    else data=float(transpose(ccdata.sp,iord))
  endelse
  nx=ccdata.nx
  ny=ccdata.ny    
  naxes=[nwl,4,nx,ny]
  mkhdr,header,4,naxes,/extend
  sxaddpar,header,'NSR',1,'number of spectral regions'
  sxaddpar,header,'WLI1',1,'index of first spectral region'
  sxaddpar,header,'STOKES','IQUV','order of Stokes vector'
  sxaddpar,header,'XOFFSET',0,'x-offseet'
  sxaddpar,header,'YOFFSET',0,'y-offseet'
  
                                ;add info from cc-fileheader (use only
                                ;first file)
  mrd_head,ccdata.fits[0],hfits
  header=merge_fits_hdrs(header,hfits)
  
  print,'Writing 4D Fits file: '+odir+out
  ;check if output file exists
  fi=file_info(odir+out)
  if fi.exists eq 1 and keyword_set(overwrite) eq 0 then begin
     message,/cont,odir+out+' already exists. Overwrite (y/N)?'
     key=get_kbrd()
     if strupcase(key) ne 'Y' then return
  endif
  if dryrun eq 0 then write_fits_dlm,data,odir+out,header
  
                                ;create average WL vector
  wlvec=total(ccdata.wl,1)/nx
  mkhdr,headerwl,5,naxes(0),/image
  wldisp=wlvec[1]-wlvec[0]
  wloff=wlvec[0]
  sxaddpar,headerwl,'WLVEC','','wavelength vector'
  sxaddpar,headerwl,'WLDISP',wldisp,'wavelength dispersion'
  sxaddpar,headerwl,'WLOFF',wloff,'wavelength offset'
  print,'WLDISP',wldisp,' / wavelength dispersion'
  print,'WLOFF',wloff ,' / wavelength offset'
  if dryrun eq 0 then write_fits_dlm,wlvec,odir+out,headerwl,append=1
  
                                ;determine IC_HRSA  
  tp=reform(sqrt(total(data[*,1,*,*]^2+data[*,2,*,*]^2+data[*,3,*,*]^2,1))/nwl)
  wd=50<(min([nx,ny])/10)
  medtp=median(tp,wd)           ;boxcar to find most quiet region
  medall=medtp*0+!values.f_nan
  medall[wd:nx-wd-1,wd:ny-wd-1]=medtp[wd:nx-wd-1,wd:ny-wd-1]
  icmed=median(ccdata.icont,wd)  
  iqs=where(icmed ge 0.9*max(icmed))
  dummy=min(medall[iqs],/nan,imin)
  ixy=array_indices(medall,iqs[imin])
  icont_hsra=(median(ccdata.icont,wd))[ixy[0],ixy[1]]
                                ;write out continuum image
  mkhdr,headeric,4,naxes(2:3),/image
  sxaddpar,headeric,'ICONTIMG','','continuum image'
  sxaddpar,headeric,'IC_HSRA',icont_hsra,'average QS continuum level'
  print,'IC_HSRA',icont_hsra,' / average QS continuum level'
  if dryrun eq 0 then write_fits_dlm,ccdata.icont,odir+out,headeric,append=1
  print,'Wrote 4D cube to '+odir+out
  
  
                                ;write out 
  if n_elements(psffile) ne 0 then begin
  fi=file_info(psffile)
  if fi.exists eq 0 then begin
    print,'PSF file '+psffile+' not found.'
  endif else begin
    psf=readfits(psffile,phdr)
    
;    psf=mkpsf(10.,0.126,15650.e-10,0.1,/display,predef='GREGOR')
    npsf=(size(psf,/dim))[0]
    lsalpha=float(total(psf)-psf[npsf/2,npsf/2])
    print,'Local Straylight Factor = ',lsalpha
    
    
    lstray=straylight(data,psf)
    data_nols=data-lsalpha*lstray
    mkhdr,header,4,naxes,/extend
    sxaddpar,header,'FITS4D',out,'Local Straylight Removed'
    sxaddpar,header,'PSFFILE',psffile,'PSF file used for LS computation'
    sxaddpar,header,'LS_APLHA',lsalpha,'Local Straylight Factor'
    sxaddpar,header,'IC_HSRA',icont_hsra,'average QS continuum level'
    sxaddpar,header,'IC_HSRA_LS',icont_hsra*(1-lsalpha),'average QS continuum level after LS removal'
    print,'LS_APLHA',lsalpha,' / Local Straylight Factor'
    print,'IC_HSRA_LS',icont_hsra*(1-lsalpha),' / average QS continuum level after LS removal'
    if dryrun eq 0 then begin
      write_fits_dlm,data_nols,odir+nols_out,header
      write_fits_dlm,wlvec,odir+nols_out,headerwl,append=1
      write_fits_dlm,ccdata.icont*(1-lsalpha),odir+nols_out,headeric,append=1
    endif    
    print,'Wrote LOCAL STRAYLIGHT REMOVED DATA to '+odir+nols_out
  endelse
endif

end
