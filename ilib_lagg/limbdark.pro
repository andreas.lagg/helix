;compute limb darkening with simple polynomial approximation
;http://en.wikipedia.org/wiki/Limb_darkening
function limbdark,mu
  
    a1=0.93
    a2=-0.23
    a0=1-a1-a2
    
    I=1.*(a0*mu^0 + a1*mu^1+a2*mu^2)
    
    return,I
end
