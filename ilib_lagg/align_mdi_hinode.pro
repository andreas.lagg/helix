pro align_mdi_hinode,reread=reread
  common olddat,index_sp,data_sp,hinic,spf,mdi
;tipimage
  
  if n_elements(mdi) eq 0 or keyword_set(reread) then begin
    
;    mdifile='/data/slam/MDI/20080510/fd_M_96m_01d.5608.0006.fits'
    mdifile='/data/slam/MDI/20080507/fd_M_96m_01d.5605.0006.fits'
    data=readfits_ssw(mdifile,header)
    hst=fits_header2st(header)
    mdi={data:data,header:hst,file:mdifile}
  
                                ;get raw data images
    mask='/data/slam/Hinode/20080507/SP4D/H0900/CAL/SP3*.fits'
    spf = findfile(mask,count=n)
    if n eq 0 then message,'No files found in '+mask
;    spf=spf[0:95]
    read_sot,spf,index_sp,data_sp
    hinic=reform(data_sp(0,*,0,*)*0.)
;     for i=0,n_elements(spf)-1 do begin
;       read_ccx,spf(i)+'.ccx',icont=icont
;       hinic(*,i)=icont
;     endfor
  endif
  nv=where(mdi.data le -32767.)
  
  ;preselect mdi region
  nam='20080507_H0900'
  vvmdi=mdi.data(450:650,200:450)
  
                                ;average hinode i profile
  avg=total(total(data_sp,2),3)
  avgi=avg(*,0)
  dummy=min(avgi) & ibin=!c
  vvhin=transpose((reform(total(data_sp(ibin-10:ibin,*,3,*),1)))/11.)
  
  sav=nam+'_align.sav'
  fi=file_info(sav)
  if fi.exists then begin
    restore,sav
    guess={p:trans.p,q:trans.q}
  endif
  
  my_align,vvmdi,vvhin,trans=trans,flip=flip,mirror=mirror,box=box, $
    xarr=xarr_tr,yarr=yarr_tr,guess=guess
  trans.tipnam=mdi.file
  trans.hinnam=spf(0)+' - '+spf(n_elements(spf)-1)
  
  print,'Store transformation to sav-file...'
  fi=file_info(sav)
  if fi.exists then begin
    print,'Overwrite '+sav+' [y/N]?'
    repeat key=get_kbrd() until key ne ''
    if strupcase(key) ne 'Y' then read,'New filename: ',sav 
  endif 
                                ;test write access
  spawn,'touch '+sav+' ; test -w '+sav+' ; echo $?',res
  if res ne '0' then read,'Enter filename: ',sav
  save,trans,/xdr,/compress,file=sav
  print,'Image transformation stored in '+sav
  
  align_show,sav
end

