;convert output of spinor (inverted_atmos.fits) to an observation 4D
;fits file  to be used as input in format 'C'
pro spinor_prof2obs,filein
  
  fileout=strmid(filein,0,(strpos(filein,'.fits',/reverse_search))[0])+'_C.fits'
  
  data=readfits(filein,hdr)
  
                                ;rearrange IVQU->IQUV
  data=data[*,[0,2,3,1],*,*]
  sz=size(data,/dim)
  
                                ;create continuum image
  icont=fltarr(sz[2],sz[3])
  for ix=0,sz[2]-1 do for iy=0,sz[3]-1 do begin
    icont[ix,iy]=get_cont(data[*,0,ix,iy])
  endfor
  
                                ;create wl-vector
  wlref=sxpar(hdr,'WLREF')
  wlmin=sxpar(hdr,'WLMIN')
  wlmax=sxpar(hdr,'WLMAX')
  wlvec=dindgen(sz[0])/(sz[0]-1)*(wlmax-wlmin)+wlmin+wlref
  
                                ;write out data
  nbyt=bytesize(data)
  if nbyt ge 2e9 then begin
    message,/cont,'WARNING: Large FITS files must be written with the FTSWR dlm. Do you want to continue using the standard IDL writefits routine [N/y]?'
    key=get_kbrd()
    if strupcase(key) ne 'Y' then stop
  endif
  writefits,fileout,data,hdr
  writefits,fileout,wlvec,/append
  writefits,fileout,icont,/append
  
  
  
end
