;find sufi image for imay observation
pro imax_sufi,imax_id,sufi_wl=sufi_wl,level3=level3
  
  sufi_dir='/data/sunrise/ScienceFlight20090608/Sufi/'
  if keyword_set(level3) then sufi_dir=sufi_dir+'level3/' $
  else sufi_dir=sufi_dir+'level2/'
  
  imax_dir='/data/sunrise/ScienceFlight20090608/Imax/level2/'
  
  imax_cyc=fix(strmid(imax_id,0,3))
  imax_obs=fix(strmid(imax_id,4,3))
  print,'IMaX Cycle: ',imax_cyc
  print,'IMaX ObsNo: ',imax_obs
;  dtime=30.
;  print,'Search +-'+n2s(fix(dtime))+' sec around this IMaX observation'
  
  if n_elements(sufi_wl) eq 0 then sufi_wl='388'
  print,'Using SuFI Wavelength: ',sufi_wl
  case sufi_wl of
    '214': begin
      soff=4
      PlateScale = 0.019830 ; // Plattenskala fuer Sufi-214nm-Bilder
    end
    '300': begin
      soff=7
      PlateScale = 0.020690 ; // Plattenskala fuer Sufi-300nm-Bilder
    end
    '313': begin
      soff=10
      PlateScale = 0.020330 ; // Plattenskala fuer Sufi-313nm-Bilder
    end
    '388': begin
      soff=13
      PlateScale = 0.019877 ; // Plattenskala fuer Sufi-388nm-Bilder
    end
    '397': begin
      soff=16
      PlateScale = 0.019832 ; // Plattenskala fuer Sufi-397nm-Bilder
    end
    '525': begin
      soff=19
    end
    else: message,'Unknown SuFI Wavelength.'
  endcase
  
  PlateScaleIMaX = 0.054458   ; // Plattenskala fuer IMaX-525nm-Bilder
  PlateScaleCommon = 0.02069  ; // Common Plate scale
  
  imax_crop=[777.,259.,1490.,2230.] ; croping IMaX to approx. SUFI area
                                ;(ULx,ULy,LRx,LRy)
  icrp=imax_crop*PlateScaleCommon/PlateScaleIMaX
  icrp([3,1])=936-1-icrp([1,3])
  
  
  
  sufi_info='/data/sunrise/ScienceFlight20090608/Sufi/level3/SufiImaxAligned_I030/Shifts.txt'
  openr,unit,/get_lun,sufi_info
  i=0
  repeat begin
    a='' & readf,unit,a
    splt=strsplit(a)
    if i ge 20 and n_elements(splt) eq 22 then begin ;skip header
      line=strsplit(a,/extract)
      if fix(line(1)) eq imax_cyc and fix(line(0)) eq imax_obs then $
        sufi_txt=line
    endif
    i=i+1
  endrep until eof(unit)
  free_lun,unit
  if n_elements(sufi_txt) eq 0 then message,'Could not find IMaX Cycle / ObsNo.'
  sufi_off=float(sufi_txt(soff:soff+2))
  imax_off=float(sufi_txt(19:19+2))
  
  
                                ;read in time of IMaX observation
  openr,unit,/get_lun,'/data/sunrise/ScienceFlight20090608/Imax/level2/ImaxObservingDates.txt'
  repeat begin
    a='' & readf,unit,a
    if strmid(a,0,7) eq 'ObsTime' then begin
      if strmid(a,8,7) eq imax_id then imax_time=strmid(a,17,15)
    endif
  endrep until eof(unit)
  free_lun,unit
  if n_elements(imax_time) eq 0 then message,'Could not find IMaX Obs.'
  imax_date=strmid(imax_time,0,8)
  imax_tsec=(strmid(imax_time,9,2)*3600d + $
             strmid(imax_time,11,2)*60d + $
             strmid(imax_time,13,2))
  
                                ;file list of SuFI observations
  openr,unit,/get_lun,sufi_dir+'filenames.txt'
  sufi_files=''
  repeat begin
    a='' & readf,unit,a
    if strmid(a,3,3) eq sufi_wl then begin
      sufi_files=[sufi_files,a]      
      ; if sufi_date eq imax_date and  $
      ;   (sufi_tsec ge imax_tsec-dtime and $
      ;    sufi_tsec le imax_tsec+dtime) then sufi_files=[sufi_files,a]
    endif
  endrep until eof(unit)
  free_lun,unit
  if n_elements(sufi_files) eq 1 then message,'No SuFI files found.'
  sufi_files=sufi_files(1:*)
  
  sufi_time=strmid(sufi_files,7,15)
  sufi_date=strmid(sufi_time,0,8)
  sufi_tsec=(strmid(sufi_time,9,2)*3600d + $
             strmid(sufi_time,11,2)*60d + $
             strmid(sufi_time,13,2))
  isin=where(sufi_date eq imax_date)
  if isin(0) eq -1 then message,'No SuFI files for this date.'
  dummy=min(abs(sufi_tsec(isin)-imax_tsec(isin)),idx)
  sidx=isin(idx)
  
  print,'Closest SuFI image: '+sufi_files(sidx)
  sufi=file_search(sufi_dir+strmid(sufi_date(sidx),0,4)+'_'+strmid(sufi_date(sidx),4,2)+'_'+strmid(sufi_date(sidx),6,2)+'/*/'+sufi_wl+'/results/'+sufi_files(sidx),count=ns)
  if ns eq 0 then message,'Could not find SuFI File: '+sufi_files(sidx)
  print,'Found SuFI File: '
  print,sufi
  
                                ;search imax file
  imax=file_search(imax_dir+strmid(imax_date,0,4)+'y_'+strmid(imax_date,4,2)+'m_'+strmid(imax_date,6,2)+'d_new_fits4D/imax_'+imax_id+'.fits.gz',count=ni)
  if ni eq 0 then message,'Could not find IMaX file: ' +imax_id
  print,'Found IMaX File: '
  print,imax
  
  imaxdat=readfits(imax,ihead)
  isz=size(imaxdat)
  sufidat=readfits(sufi,shead)
  ssz=size(sufidat)
  
                                ;display IMaX File and overplot SUFI box
  psset,ps=ps,size=[20,14],/no_x,view=0,file='imax_sufi_'+imax_id+'.ps'
  
  userlct,/full,coltab=0,/reverse
  brd=40
  img=imaxdat(icrp(0)-brd:icrp(2)+brd, $
              icrp(1)-brd:icrp(3)+brd,4,0) ;continuum image
  img=imaxdat(icrp(0)-brd:icrp(2)+brd, $
              icrp(1)-brd:icrp(3)+brd,1,3) ;Stokes V image
  img=imaxdat(icrp(0)-brd:icrp(2)+brd, $
              icrp(1)-brd:icrp(3)+brd,2,0) ;I Core
  zrg=minmaxp(img,perc=99.9)
  xrg=icrp([0,2])+[-brd,brd]
  yrg=icrp([1,3])+[-brd,brd]
  image_cont_al,img,xrange=xrg,yrange=yrg,zrange=zrg, $
    /aspect,/cut,contour=0,title='IMaX '+imax_id+', ', $
    ztitle='IMaX I!LC!N',position=[0.08,0.08,0.5,0.95]
  
  userlct
  ioff=imax_off(0:1)*PlateScaleCommon/PlateScaleImax
  soff=sufi_off(0:1)*PlateScaleCommon/PlateScaleImax
  sxy=ssz(1:2)*PlateScale/PlateScaleImax
  off=ioff-soff
  bpos=[off(0),off(1),off(0)+sxy(0),off(1)+sxy(1)]+icrp([0,1,0,1])
  plots,color=1,thick=1,bpos([0,2,2,0,0]),bpos([1,1,3,3,1])
  print,'ioff=',ioff,'soff=',soff,'off=',off,'icrp=',icrp,'sxy=',sxy,'bpos=',bpos 
  
  userlct,/full,coltab=0,/reverse
  img=rotate(sufidat,7)
  zrg=minmaxp(img,perc=99.9)
  image_cont_al,img,xrange=[0.,ssz(1)],yrange=[0.,ssz(2)],zrange=zrg, $
    /aspect,/cut,contour=0,title='SuFI '+sufi_wl, $
    position=[0.55,0.08,0.95,0.95],/noerase
  
  psset,/close
  stop
end
