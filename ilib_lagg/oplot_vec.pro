pro oplot_vec,vx,vy,step=step,len=mul,color=col,ax=ax
;
if n_elements(col) eq 0 then col=!p.color
IF n_elements(ax) eq 0 THEN ax = 0


s = size(vx)

ax1 = ax * !dtor
sax = sin(ax1)
cax = cos(ax1)

vxneu = vx
vyneu = vy

IF ax NE 0 THEN BEGIN

   FOR i=0,s[1]-1 DO BEGIN

       FOR j=0,s[2]-1 DO BEGIN

           vxx = vx[i,j]
           vyy = vy[i,j]

           vxneu[i,j] = cax*vxx - sax*vyy 
           vyneu[i,j] = sax*vxx + cax*vyy 

       ENDFOR 

   ENDFOR

ENDIF

mag=sqrt(vxneu^2+vyneu^2)
s=size(mag)
lmax=max(mag)
vect=fltarr(s(1),s(2))
k=fix(step)
x0=0
x1=0
y0=0
y1=0
for i=1,step do begin
mark:   
    x0=fix(randomu(ss)*s(1))
    y0=fix(randomu(ss)*s(2))
  
    v1 = mul*vxneu(x0,y0)*4
    v2 = mul*vyneu(x0,y0)*4
 
    x1=x0+v1
    y1=y0+v2
 
    if x1 lt 0 or x1 gt s(1)-1 or y1 lt 0 or y1 gt s(2)-1 then goto,mark
    angle=22.5*!dtor
    a1=abs(atan(vxneu(x0,y0)/vyneu(x0,y0))) 
    st=mul*mag(x0,y0)*sin(angle)
    ct=mul*mag(x0,y0)*cos(angle)
    lte=sqrt(st^2+ct^2)*2.
      x2=x1-sin(angle+a1)*lte*vxneu(x0,y0)/abs(vxneu(x0,y0))
      x3=x1-sin(a1-angle)*lte*vxneu(x0,y0)/abs(vxneu(x0,y0))
    
      y2=y1-cos(a1+angle)*lte*vyneu(x0,y0)/abs(vyneu(x0,y0))
      y3=y1-cos(a1-angle)*lte*vyneu(x0,y0)/abs(vyneu(x0,y0))
      plots,[x0,x1,x2,x1,x3],[y0,y1,y2,y1,y3],color=col,thick=1
;     plots,[x0,x1,x2,x3,x1],[y0,y1,y2,y3,y1],thick=1
endfor
end
