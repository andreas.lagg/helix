;read in fts data file and return fts spectrum and wavelength
pro fts_read,fts,tel=tel,cor=cor,obs=obs,wlair=wlair,wlvac=wlvac
  
  
  print,'Reading '+fts+' ...',format='(a,$)'
  wn=dblarr(100000l)
  tel=dblarr(100000l)
  obs=dblarr(100000l)
  cor=dblarr(100000l)
  openr,unit,/get_lun,fts,error=error
  if error ne 0 then message,'Error in reading '+fts
  k=0
  wnd=0d & ind=0d & eard=0d & totd=0d
; For the region 13,500 to 20,000 cm-1, the four columns of each file
; contain, first the frequency, second the deduced telluric spectrum,
; third the observed photospheric spectrum before correction for
; telluric absorption, and fourth the photospheric spectrum corrected
; for telluric absorption.
  while not eof(unit) do begin
    readf,unit,wnd,ind,eard,totd
    wn(k)=wnd & tel(k)=ind & obs(k)=eard & cor(k)=totd
    k=k+1
  endwhile
  free_lun,unit
  wn=wn(0:k-1) & tel=tel(0:k-1) & obs=obs(0:k-1) & cor=cor(0:k-1)
    
  wlvac=1./wn*1e8               ;wavelength in Angstroem
  wlair=vac2air(wlvac)
 
  print,'Done.'
end
