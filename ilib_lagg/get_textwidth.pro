;+
; NAME:
;    get_text_width
;
;
; PURPOSE:
;    returns width of text (xyouts procedure)
;
;
; CATEGORY:
;    displaying routines
;
;
; CALLING SEQUENCE:
;    w=get_text_width(text,charsize)
;
; 
; INPUTS:
;    text - string containing text
;    charsize - charsize of output
;
; OPTIONAL INPUTS:
;
;
;	
; KEYWORD PARAMETERS:
;
;
;
; OUTPUTS:
;    w - width in device units
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;    A. Lagg, 12.9.1997
;-

function get_textwidth,text,charsize,normal=wd,device=wx
  
  xyouts,/normal,-10.,-10.,noclip=1,text,charsize=charsize,width=wd
  wx=(convert_coord([wd,0],/normal,/to_device))(0)
  return,wx
end

