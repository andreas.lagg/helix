;context-menu for right-click on xfits-plot window
pro ps_sensitive,ixy
  common xpswg,xpswg
  common xfwg,xfwg
  
  ; widget_control,xpswg.tabid.hist,bad_id=bad_id    
  ; if bad_id eq 0 then begin
    widget_control,xpswg.tabid.hist, $
      sensitive=xfwg.p(ixy(0),ixy(1)).plotmode eq 1
    widget_control,xpswg.tabid.scat, $
      sensitive=xfwg.p(ixy(0),ixy(1)).plotmode eq 2
;  endif
end

pro xpswg_imgset
  common xfwg,xfwg
  common xpswg,xpswg
  
  ii=xpswg.imgselect.val
  ixy=xpswg.ixy
  widget_control,xpswg.imgselect.id,set_combobox_select=ii
  widget_control,xpswg.imgsmooth.id, $
                 set_value=fix(xfwg.p(ixy(0),ixy(1)).img[ii].smooth)
  widget_control,xpswg.imgsmooth_apply.id, $
                 set_value=xfwg.p(ixy(0),ixy(1)).img[ii].smooth_apply
  for i=0,1 do $
    widget_control,xpswg.imgunsharp[i].id, $
                   set_value=xfwg.p(ixy(0),ixy(1)).img[ii].unsharp[i]
  widget_control,xpswg.imgunsharp_only.id, $
                 set_value=xfwg.p(ixy(0),ixy(1)).img[ii].unsharp_only
  widget_control,xpswg.imgunsharp_apply.id, $
                 set_value=xfwg.p(ixy(0),ixy(1)).img[ii].unsharp_apply
end

pro xpswg_event,event
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xpswg,xpswg
  
  widget_control,event.id,get_uvalue=uval
  ixy=xpswg.ixy
  ii=xpswg.imgselect.val
  ip=xfwg.p(ixy(0),ixy(1)).par.val
  iall=indgen(n_elements(xfwg.p.par.val))
  if ip lt fits.npar then begin
    ipsame=where(fits.par(xfwg.p.par.val) eq fits.par(ip)) 
    icsame=where(fits.comp(xfwg.p.par.val) eq fits.comp(ip) and $
                 fits.class(xfwg.p.par.val) eq fits.class(ip)) 
    ilsame=where(fits.logtau(xfwg.p.par.val) eq fits.logtau(ip) and $
                 fits.class(xfwg.p.par.val) eq fits.class(ip)) 
  endif else begin
    case xfwg.p(ixy(0),ixy(1)).par.str of
      'Stokes Par': ipsame=where(xfwg.p.par.str eq 'Stokes Par')
      'User': ipsame=where(xfwg.p.par.str eq 'User')
      else: 
    endcase
    icsame=ipsame
    ilsame=ipsame
  endelse
  if strmid(uval,0,6) eq 'condmm' then begin
    clayer=fix(strmid(uval,6,1))
    cmm=fix(strmid(uval,7,1))
    uval='condmm'
  endif
  if strmid(uval,0,7) eq 'condpar' then begin
    clayer=fix(strmid(uval,7,1))
    uval='condpar'
  endif
  
  case uval of 
    'done': widget_control,xpswg.base.id,/destroy
    'range0': xfwg.p(ixy(0),ixy(1)).range(0)=event.value
    'range1': xfwg.p(ixy(0),ixy(1)).range(1)=event.value
    'scatxrg0': xfwg.p(ixy(0),ixy(1)).scatter.xrange(0)=event.value
    'scatxrg1': xfwg.p(ixy(0),ixy(1)).scatter.xrange(1)=event.value
    'scatyrg0': xfwg.p(ixy(0),ixy(1)).scatter.yrange(0)=event.value
    'scatyrg1': xfwg.p(ixy(0),ixy(1)).scatter.yrange(1)=event.value
    'scatzrg0': xfwg.p(ixy(0),ixy(1)).scatter.zrange(0)=event.value
    'scatzrg1': xfwg.p(ixy(0),ixy(1)).scatter.zrange(1)=event.value
    'histxrg0': xfwg.p(ixy(0),ixy(1)).histo.xrange(0)=event.value
    'histxrg1': xfwg.p(ixy(0),ixy(1)).histo.xrange(1)=event.value
    'rgall': begin
      xfwg.p(ipsame).range=xfwg.p(ixy(0),ixy(1)).range
    end
    'coltab': xfwg.p(ixy(0),ixy(1)).coltab.val=event.index
    'colpar': begin
      xfwg.p(ipsame).coltab.val=xfwg.p(ixy(0),ixy(1)).coltab.val
      xfwg.p(ipsame).shorttitle=xfwg.p(ixy(0),ixy(1)).shorttitle
    end
    'colall': begin
      xfwg.p(*).coltab.val=xfwg.p(ixy(0),ixy(1)).coltab.val
      xfwg.p(*).shorttitle=xfwg.p(ixy(0),ixy(1)).shorttitle
    end
    'shorttitle': xfwg.p(ixy(0),ixy(1)).shorttitle=event.value
    'contour': begin
      xfwg.p(ixy(0),ixy(1)).contpar.index=event.index-1
      xfwg.p(ixy(0),ixy(1)).contpar.str=event.str
    end
    'scatpar': begin
      xfwg.p(ixy(0),ixy(1)).scatter.par=event.index
      xfwg.p(ixy(0),ixy(1)).scatter.str=event.str
    end
    'contourcol': xfwg.p(ixy(0),ixy(1)).contpar.col=event.value
    'contourthick': xfwg.p(ixy(0),ixy(1)).contpar.thick=event.value
    'contourval': xfwg.p(ixy(0),ixy(1)).contpar.val=event.value
    'contsmooth': xfwg.p(ixy(0),ixy(1)).contpar.smooth=event.value
    'contourall': begin
      case event.value of 
        'all': same=iall
        'comp': same=icsame
        'lgtau': same=ilsame
      endcase
      for is=0,n_elements(same)-1 do $
        xfwg.p(same[is]).contpar=xfwg.p(ixy(0),ixy(1)).contpar
    end
    'stokes': xfwg.p(ixy(0),ixy(1)).stokes=event.index
    'obsfit': begin
      xfwg.p(ixy(0),ixy(1)).obsfit=event.value
    end
    'userpar': xfits_setuserpar,ixy
    'ic':  xfwg.p(ixy(0),ixy(1)).ic.val=event.select
    'absval':  xfwg.p(ixy(0),ixy(1)).absval.val=event.select
    'maxval':  xfwg.p(ixy(0),ixy(1)).maxval.val=event.select
    'zlog':  xfwg.p(ixy(0),ixy(1)).zlog=event.select
    'scatcolor': begin
      xfwg.p(ixy(0),ixy(1)).scatter.color=event.select
      for i=0,1 do widget_control,xpswg.scatzrg(i).id, $
        sensitive=xfwg.p(ixy(0),ixy(1)).scatter.color eq 1
      widget_control,xpswg.scatbin.id, $
        sensitive=xfwg.p(ixy(0),ixy(1)).scatter.color eq 1
      widget_control,xpswg.scathnorm.id, $
        sensitive=xfwg.p(ixy(0),ixy(1)).scatter.color eq 1
    end
    'scathnorm': xfwg.p(ixy(0),ixy(1)).scatter.hnorm=event.select
    'scatxlog': xfwg.p(ixy(0),ixy(1)).scatter.xlog=event.select
    'scatylog': xfwg.p(ixy(0),ixy(1)).scatter.ylog=event.select
    'histnorm': xfwg.p(ixy(0),ixy(1)).histo.norm=event.select
    'scatbin': xfwg.p(ixy(0),ixy(1)).scatter.nbins=event.value
    'wlrg0': xfwg.p(ixy(0),ixy(1)).wlrg(0)=event.value
    'wlrg1': xfwg.p(ixy(0),ixy(1)).wlrg(1)=event.value
    'wlrgall': xfwg.p(ipsame).wlrg=xfwg.p(ixy(0),ixy(1)).wlrg
    'condpar': begin
      xfwg.p(ixy(0),ixy(1)).cond[clayer].par=event.index-1
      xfwg.p(ixy(0),ixy(1)).cond[clayer].str=event.str
    end
    'condspline': xfwg.p(ixy(0),ixy(1)).condspline=event.select
;    'condlayer': xfwg.p(ixy(0),ixy(1)).condlayer[event.value]=event.select
    'condmm': xfwg.p(ixy(0),ixy(1)).cond[clayer].mm[cmm]=event.value
    'condall': begin
      case event.value of 
        'all': same=iall
        'comp': same=icsame
        'lgtau': same=ilsame
      endcase
      for is=0,n_elements(same)-1 do $
        xfwg.p(same[is]).cond=xfwg.p(ixy(0),ixy(1)).cond
      xfwg.p(same).condspline=xfwg.p(ixy(0),ixy(1)).condspline
    end
    'azirotsens': xfwg.azi.rotsens=event.select
    'azioffset': xfwg.azi.offset=event.value
    'azivec': xfwg.p(ixy(0),ixy(1)).azipar.flag=event.select
    'azi180': xfwg.p(ixy(0),ixy(1)).azipar.ambi=event.select
    'aziscale': xfwg.p(ixy(0),ixy(1)).azipar.scale=event.value
    'azilen': xfwg.p(ixy(0),ixy(1)).azipar.length=event.value
    'azibin': xfwg.p(ixy(0),ixy(1)).azipar.bin=event.value
    'azicol': xfwg.p(ixy(0),ixy(1)).azipar.color=event.value
    'azithick': xfwg.p(ixy(0),ixy(1)).azipar.thick=event.value
    'azicomp': xfwg.p(ixy(0),ixy(1)).azipar.comp=fix(event.str)
    'aziall': begin
      case event.value of 
        'all': same=iall
        'comp': same=icsame
        'lgtau': same=ilsame
      endcase
      for is=0,n_elements(same)-1 do $
        xfwg.p(same[is]).azipar=xfwg.p(ixy(0),ixy(1)).azipar
    end
    'tab': begin
      xpswg.tab.val=event.tab
      if event.tab eq 5 then xpswg_imgset
    end
    'histbin': xfwg.p(ixy(0),ixy(1)).histo.nbins=event.value
    'plotmode': begin
      xfwg.p(ixy(0),ixy(1)).plotmode=event.value
      widget_control,xpswg.tab.id, $
                     set_tab_current=xfwg.p(ixy(0),ixy(1)).plotmode
      ps_sensitive,ixy
    end
    'imgunsharp0': begin
      xfwg.p(ixy(0),ixy(1)).img[ii].unsharp[0]=(event.value>0)<1
      if event.value ne xfwg.p(ixy(0),ixy(1)).img[ii].unsharp[0] then $
        widget_control,xpswg.imgunsharp[0].id, $
                       set_value=xfwg.p(ixy(0),ixy(1)).img[ii].unsharp[0]
    end
    'imgunsharp1': begin
      xfwg.p(ixy(0),ixy(1)).img[ii].unsharp[1]=(event.value>0)< $
                                          (min([fits.nx,fits.ny])-1)/2
      if event.value ne xfwg.p(ixy(0),ixy(1)).img[ii].unsharp[1] then $
        widget_control,xpswg.imgunsharp[1].id, $
                       set_value=xfwg.p(ixy(0),ixy(1)).img[ii].unsharp[0]
    end
    'imgunsharp_only': xfwg.p(ixy(0),ixy(1)).img[ii].unsharp_only=event.select
    'imgselect': begin
      xpswg.imgselect.val=event.index
      xpswg_imgset
    end
    'imgsmooth': xfwg.p(ixy(0),ixy(1)).img[ii].smooth=event.value
    'imgsmooth_apply': $
      xfwg.p(ixy(0),ixy(1)).img[ii].smooth_apply[event.value]=event.select
    'imgunsharp_apply': $
      xfwg.p(ixy(0),ixy(1)).img[ii].unsharp_apply[event.value]=event.select
    'imgall': begin
      case event.value of 
        'all': same=iall
        'comp': same=icsame
        'lgtau': same=ilsame
      endcase
      for j=0,n_elements(same)-1 do $
        xfwg.p(same[j]).img[ii]=xfwg.p(ixy(0),ixy(1)).img[ii]
      ;; stop
      ;; xfwg.p(same).img[ii].unsharp= $
      ;;   xfwg.p(ixy(0),ixy(1)).img[ii].unsharp
      ;; xfwg.p(same).img[ii].smooth_apply= $
      ;;   xfwg.p(ixy(0),ixy(1)).img[ii].smooth_apply
      ;; xfwg.p(same).img[ii].unsharp_apply= $
      ;;   xfwg.p(ixy(0),ixy(1)).img[ii].unsharp_apply
      ;; xfwg.p(same).img[ii].smooth= $
      ;;   xfwg.p(ixy(0),ixy(1)).img[ii].smooth
    end
    else: begin
      print,uval
      help,/st,event
    end
  end
end
 
pro xfits_plotsetwg,ixy,init=init
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xpswg,xpswg
  common parwindow,pwst
  common xppwg,xppwg
  
  subst={id:0l,val:0.,str:''}
  tabid={rang:0,cont:0,cond:0,hist:0,scat:0,image:0}
  nclayer=n_elements(xfwg.p[0].cond)
  oxpswg={base:subst,done:subst,range:replicate(subst,2),rgall:subst,ixy:ixy, $
          contour:subst,contourval:subst,stokes:subst,obsfit:subst, $ $
          contsmooth:subst,contourcol:subst,contourthick:subst, $
          wlrg:replicate(subst,2),wlrgall:subst, $
          condspline:subst,condpar:replicate(subst,nclayer),maxval:subst, $
          condmm:replicate(subst,nclayer,2),condall:subst,absval:subst, $
          coltab:subst,colpar:subst,colall:subst,ic:subst,tab:subst, $
          plotmode:subst,tabid:tabid,histbin:subst,scatpar:subst, $
          histxrg:replicate(subst,2),histnorm:subst,scathnorm:subst, $
          scatxrg:replicate(subst,2),scatyrg:replicate(subst,2), $
          scatzrg:replicate(subst,2),scatcolor:subst,scatbin:subst, $
          scatxlog:subst,scatylog:subst,shorttitle:subst,zlog:subst, $
          imgselect:subst,imgsmooth:subst,imgunsharp:replicate(subst,2), $
          imgsmooth_apply:subst,imgunsharp_apply:subst, $
          imgall:subst,imgunsharp_only:subst, $
          azivec:subst,azilen:subst,aziscale:subst,azicol:subst, $
          azithick:subst,azicomp:subst,azibin:subst,azi180:subst, $
          azirotsens:subst,azioffset:subst}
  if n_elements(xpswg) eq 0 then xpswg=temporary(oxpswg) $
  else xpswg=update_struct(xpswg,oxpswg)
  
  ip=xfwg.p(ixy(0),ixy(1)).par.val
  if ip lt fits.npar then begin
    tlong=fits.long(ip)
    tshort=fits.short(ip)
    tpar=fits.par(ip)
    parmode='fits'
  endif else case xfwg.p(ixy(0),ixy(1)).par.str of
    'Stokes Par': begin
      parmode='stokes'
      tpar='Stokes '+(['I','Q','U','V','Ic'])(xfwg.p(ixy(0),ixy(1)).stokes)
      pi=get_parinfo(tpar,fits.code)
      tlong=pi.longname
      tshort=pi.shortname
    end
    'User': begin
      parmode='user'
      tpar='User'
      pi=get_parinfo(tpar,fits.code,xy=ixy)
      tlong=pi.longname
      tshort=pi.shortname
    end
    else: return
  endcase 
  tstr=tlong
  ; geomwg=xfwg.base.id
  ; if n_elements(pwst) ne 0 then begin
  ;   widget_control,pwst.base.id,bad_id=bid
  ;   if bid eq 0 then begin
  ;     geomwg=pwst.base.id
  ;   endif
  ; endif
;  info=widget_info(geomwg,/geometry)
  info=widget_info(xfwg.base.id,/geometry)
  xpswg.base.id=widget_base(title=tstr,group_leader=xpwg.base.id,col=1, $
                              xoffset=info.xoffset, $
                              yoffset=info.yoffset+info.ysize+50)
;                            xoffset=info.xoffset+info.xsize/3., $
;                            yoffset=info.yoffset+info.ysize/3.)
  sub1=widget_base(xpswg.base.id,row=1)
  lab=widget_label(sub1,/align_left,value='Properties for map x='+ $
                   n2s(ixy(0))+',y='+n2s(ixy(1))+'    ')
  xpswg.plotmode.id= $
    cw_bgroup(sub1,/exclusive,['map','histogram','scatterplot'], $
              set_value=xfwg.p(ixy(0),ixy(1)).plotmode, $
              uval='plotmode',row=1)
  if tpar eq 'User' then begin
    subu=widget_base(xpswg.base.id,col=1,/frame)
    dummy=widget_button(subu,value=' Define User Parameter ',uvalue='userpar')
    common xupwg,xupwg
    if n_elements(xupwg) ne 0 then begin
      widget_control,xupwg.base.id,bad_id=bad_id    
      if bad_id eq 0 then dummy=xfits_userpar(ixy[0],ixy[1],/init)
    endif
  endif
  xpswg.tab.id=widget_tab(xpswg.base.id,uvalue='tab',location=0)
  xpswg.tabid.rang=widget_base(xpswg.tab.id,title='Range & Layout', $
                               col=1,space=0)
  xpswg.tabid.hist=widget_base(xpswg.tab.id,title='Histogram',col=1,space=0)
  xpswg.tabid.scat=widget_base(xpswg.tab.id,title='Scatter',col=1,space=0)
  xpswg.tabid.cont=widget_base(xpswg.tab.id,title='Contour/Azi',col=1,space=0)
  xpswg.tabid.cond=widget_base(xpswg.tab.id,title='Conditions',col=1,space=0)
  xpswg.tabid.image=widget_base(xpswg.tab.id,title='Smooth / Enhance',col=1,space=0)
  ps_sensitive,ixy
  
  subz=widget_base(xpswg.tabid.rang,col=1,/frame)
  subct=widget_base(xpswg.tabid.rang,col=1,/frame)
  subs=widget_base(xpswg.tabid.rang,col=1,/frame, $
                   sensitive=parmode eq 'stokes' or tpar eq 'User')
  
  subc=widget_base(xpswg.tabid.cont,col=1,/frame)
  suba=widget_base(xpswg.tabid.cont,col=1,/frame)
  subk=widget_base(xpswg.tabid.cond,col=1,space=0)
  
  sub=widget_base(subz,row=1)
  lab=widget_label(sub,value='Z-Range:')
  for i=0,1 do $
    xpswg.range(i).id=cw_field(sub,xsize=8,/all_events,/floating,title='', $
                               uvalue='range'+n2s(i), $
                               value=xfwg.p(ixy(0),ixy(1)).range(i))
  xpswg.rgall.id=widget_button(sub,value='Set for all '+tpar, $
                               uvalue='rgall')
  xpswg.zlog.id=cw_bgroup(sub,/nonexclusive,'z-log10', $
                        set_value=xfwg.p(ixy(0),ixy(1)).zlog, $
                        uval='zlog',row=1)
  
  sub1=widget_base(subct,row=1)
  sub2=widget_base(subct,row=1)
  dummy=widget_label(sub1,value='Color Table:',/align_left)
  coltabs=['Standard','BW','Azimuth','Velocity']
  coltabs=[coltabs,coltabs+' rev']
  xpswg.coltab.id=widget_combobox(sub1,uvalue='coltab', $
                                  value=['Default',coltabs])
  widget_control,xpswg.coltab.id,set_combobox_select= $
    xfwg.p(ixy(0),ixy(1)).coltab.val
  xpswg.colpar.id=widget_button(sub1,value='Set for all '+tpar, $
                                uvalue='colpar')
  xpswg.colall.id=widget_button(sub1,value='Set for all Maps', $
                                uvalue='colall')
  lab=widget_label(sub2,value='Title:',/align_left)
  xpswg.shorttitle.id=cw_bgroup(sub2,/exclusive,['long','short'], $
                                set_value=xfwg.p(ixy(0),ixy(1)).shorttitle, $
                                uval='shorttitle',row=1)
  
  
  if parmode eq 'fits' then begin
    isame=where(fits.par eq tpar)
    lab=widget_label(subz,/align_left,value=tshort+': ('+ $
                     n2s(min(fits.data(*,*,ip)/fits.valid(*,*,ip),/nan))+','+ $
                     n2s(max(fits.data(*,*,ip)/fits.valid(*,*,ip),/nan))+')')
    lab=widget_label(subz,/align_left,value='all '+tpar+': ('+ $
                     n2s(min(fits.data(*,*,isame)/ $
                             fits.valid(*,*,isame),/nan))+','+ $
                     n2s(max(fits.data(*,*,isame)/ $
                             fits.valid(*,*,isame),/nan))+')')
  endif
  
  sub=widget_base(subc,col=1)
  sub1=widget_base(sub,row=1)
  sub2=widget_base(sub,row=1)
  sub3=widget_base(sub,row=1)
  lab=widget_label(sub1,value='Contour:')
  contpar=['None',fits.short]
  ic=where(xfwg.p.par.str eq 'Stokes Par')
  if ic(0) ne -1 then begin
    pmax=(size(xfwg.p))(1)
    icx=ic mod pmax & icy=ic/pmax
    contpar=[contpar,'Stokes '+(['I','Q','U','V','Ic'])(xfwg.p(ic).stokes)+' '+$
             'x='+n2s(icx)+',y='+n2s(icy)]
  endif
  ic=where(xfwg.p.par.str eq 'User')
  if ic(0) ne -1 then begin
    pmax=(size(xfwg.p))(1)
    icx=ic mod pmax & icy=ic/pmax
    contpar=[contpar,'User '+strmid(xfwg.p(ic).user.name,0,4)+ $
             '.. x='+n2s(icx)+',y='+n2s(icy)]
  endif
  xpswg.contour.id=widget_combobox(sub1,uvalue='contour',value=contpar)
  widget_control,xpswg.contour.id, $
    set_combobox_select=xfwg.p(ixy(0),ixy(1)).contpar.index+1
  lab=widget_label(sub1,value='Apply to')
  dummy=cw_bgroup(sub1,['all','same Comp','same LGTAU'],uvalue='contourall', $
                  button_uvalue=['all','comp','lgtau'],row=1,space=0)
  xpswg.contourval.id=cw_field(sub2,/all_events,xsize=20,/string, $
                               uvalue='contourval', $
                               title='Levels (comma-sep list):', $
                               value=xfwg.p(ixy(0),ixy(1)).contpar.val)
;  xpswg.contsmooth.id=cw_field(sub2,xsize=2,/all_events,title='smooth', $
;                               uvalue='contsmooth',/integer, $
;                               value=fix(xfwg.p(ixy(0),ixy(1)).contpar.smooth))
  xfwg.p.contpar.smooth=0
  xpswg.contourcol.id= $
    cw_field(sub3,xsize=3,/all_events,title='Color:', $
             uvalue='contourcol',/integer, $
             value=fix(xfwg.p(ixy(0),ixy(1)).contpar.col))
  xpswg.contourthick.id= $
    cw_field(sub3,xsize=3,/all_events,title='Thickness:', $
             uvalue='contourthick',/integer, $
             value=fix(xfwg.p(ixy(0),ixy(1)).contpar.thick))
  
  sub=widget_base(suba,col=1)
  sub1=widget_base(sub,row=1)
  sub2=widget_base(sub,row=1)
  sub3=widget_base(sub,row=1)
  sub0a=widget_base(sub,row=1)
  sub0b=widget_base(sub,row=1)
  lab=widget_label(sub0a,value='Azimuth angle is defined as 2*phi=atan(Q/U). If not, adjust these values:',/align_left)
  xpswg.azioffset.id= $
    cw_field(sub0b,xsize=3,/all_events,title='Offset [deg]:', $
             uvalue='azioffset',/floating, $
             value=xfwg.azi.offset)
  xpswg.azirotsens.id= $
    cw_bgroup(sub0b,/exclusive,['clockwise','counter-clockwise'], $
              set_value=xfwg.azi.rotsens,uval='azirotsens',row=1)
  xpswg.azivec.id=cw_bgroup(sub1,/nonexclusive, $
                            'show azimuth arrows', $
                            set_value=xfwg.p(ixy(0),ixy(1)).azipar.flag, $
                            uval='azivec',row=1)  
  xpswg.azi180.id=cw_bgroup(sub1,/nonexclusive, $
                            'restrict azimuth to +-90 deg', $
                            set_value=xfwg.p(ixy(0),ixy(1)).azipar.ambi, $
                            uval='azi180',row=1)  
  lab=widget_label(sub1,value='Scale:',/align_left)
  xpswg.aziscale.id=cw_bgroup(sub1,/exclusive,['unity','|B|','|B*cos(INC)|'], $
                                set_value=xfwg.p(ixy(0),ixy(1)).azipar.scale, $
                              uval='aziscale',row=1)
  xpswg.azilen.id= $
    cw_field(sub2,xsize=6,/all_events,title='Length:', $
             uvalue='azilen',/floating, $
             value=fix(xfwg.p(ixy(0),ixy(1)).azipar.length))
  xpswg.azicol.id= $
    cw_field(sub2,xsize=3,/all_events,title='Color:', $
             uvalue='azicol',/integer, $
             value=fix(xfwg.p(ixy(0),ixy(1)).azipar.color))
  xpswg.azithick.id= $
    cw_field(sub2,xsize=3,/all_events,title='Thickness:', $
             uvalue='azithick',/integer, $
             value=fix(xfwg.p(ixy(0),ixy(1)).azipar.thick))
  xpswg.azibin.id= $
    cw_field(sub2,xsize=3,/all_events,title='Binning:', $
             uvalue='azibin',/integer, $
             value=fix(xfwg.p(ixy(0),ixy(1)).azipar.bin))
  lab=widget_label(sub3,value='Apply to')
  dummy=cw_bgroup(sub3,['all','same Comp','same LGTAU'],uvalue='aziall', $
                  button_uvalue=['all','comp','lgtau'],row=1,space=0)
  lab=widget_label(sub3,value='ATM-component')
  allcomp=remove_multi(fits.comp)
  xpswg.azicomp.id= $
    widget_combobox(sub3,uvalue='azicomp', $
                    value=string(allcomp,format='(i2)'))
  xfwg.p(ixy(0),ixy(1)).azipar.comp= $
    xfwg.p(ixy(0),ixy(1)).azipar.comp>min(allcomp)
  widget_control,xpswg.azicomp.id, $
                 set_combobox_select= $
                 where(allcomp eq xfwg.p(ixy(0),ixy(1)).azipar.comp)
  
  
  
  
  sub1=widget_base(subk,row=2,frame=1)
  lab=widget_label(sub1,value='Plot conditions:     apply to',/align_left)
  xpswg.condall.id=cw_bgroup(sub1,uvalue='condall',row=1,space=0, $
                             button_uvalue=['all','comp','lgtau'],$
                             ['all','same Comp','same LGTAU'])
  ;; lab=widget_label(sub1,value='Apply to layer:')
  ;; xpswg.condlayer.id=cw_bgroup(sub1,/nonexclusive, $
  ;;                              ['map','contour','AZI-vec'], $
  ;;                              set_value=xfwg.p(ixy(0),ixy(1)).condlayer, $
  ;;                              uval='condlayer',row=1)  
  sub1=widget_base(subk,row=1,frame=1)
  lab=widget_label(sub1,value='Spline Select Tool:')
  xpswg.condspline.id=cw_bgroup(sub1,/nonexclusive, $
                                'apply only to pixels inside spline', $
                                set_value=xfwg.p(ixy(0),ixy(1)).condspline, $
                                uval='condspline',row=1)  
  for ic=0,n_elements(xfwg.p(ixy(0),ixy(1)).cond)-1 do begin
    sub1=widget_base(subk,row=1,frame=1)
    lab=widget_label(sub1,value=xfwg.p(ixy(0),ixy(1)).cond[ic].layer+':')
    for i=0,1 do begin
      xpswg.condmm[ic,i].id= $
        cw_field(sub1,xsize=10,/all_events,/floating,title='', $
                 uvalue='condmm'+n2s(ic)+n2s(i), $
                 value=xfwg.p(ixy(0),ixy(1)).cond[ic].mm(i))
      if i eq 0 then begin
        lab=widget_label(sub1,value='<=')
        xpswg.condpar[ic].id=widget_combobox(sub1,uvalue='condpar'+n2s(ic), $
                                             value=contpar)
        lab=widget_label(sub1,value='<=')
        if xfwg.p(ixy(0),ixy(1)).cond[ic].par+1 ge n_elements(contpar) then $
          xfwg.p(ixy(0),ixy(1)).cond[ic].par=0
        widget_control,xpswg.condpar[ic].id, $
                       set_combobox_select=xfwg.p(ixy(0),ixy(1)).cond[ic].par+1
        xfwg.p(ixy(0),ixy(1)).cond[ic].str= $
          contpar[xfwg.p(ixy(0),ixy(1)).cond[ic].par+1]
      endif
    endfor
  endfor

  sub=widget_base(xpswg.tabid.image,col=1,frame=1)
  sub1=widget_base(sub,row=1)
  lab=widget_label(sub1,value='Apply smoothing / enhancement to',/align_left)
  xpswg.imgall.id=cw_bgroup(sub1,uvalue='imgall',row=1,space=0, $
                            button_uvalue=['all','comp','lgtau'],$
                            ['all','same Comp','same LGTAU'])
  
  sub=widget_base(xpswg.tabid.image,col=1,frame=1)
  sub1=widget_base(sub,row=1)
  lab=widget_label(sub1,value='Set image parameters for:',/align_left)
  xpswg.imgselect.id= $
    widget_combobox(sub1,uvalue='imgselect', $
                    value=['Map','Histogram','Scatter','Contour/AZI-vector'])
  ii=xpswg.imgselect.val
  widget_control,xpswg.imgselect.id,set_combobox_select=ii
  sub1=widget_base(sub,row=1)
  xpswg.imgsmooth.id= $
    cw_field(sub1,xsize=3,/all_events,title='IDL-Smooth', $
             uvalue='imgsmooth',/integer, $
             value=fix(xfwg.p(ixy(0),ixy(1)).img[ii].smooth))
  xpswg.imgsmooth_apply.id= $
    cw_bgroup(sub1,/nonexclusive,['apply smoothing'], $
              uval='imgsmooth_apply',row=1)
  
;  sub=widget_base(xpswg.tabid.image,col=1,frame=1)
  sub1=widget_base(sub,row=1)
  upar=['Amount','Radius']
  lab=widget_label(sub1,value='IDL-Unsharp Mask:',/align_left)
  xpswg.imgunsharp_apply.id= $
    cw_bgroup(sub1,/nonexclusive,['apply unsharp mask'], $
              uval='imgunsharp_apply',row=1)
  sub1=widget_base(sub,row=1)
  for i=0,1 do $
    xpswg.imgunsharp[i].id= $
    cw_field(sub1,xsize=6,/all_events,title=upar[i], $
             uvalue='imgunsharp'+n2s(i),/float)
  xpswg.imgunsharp_only.id= $
    cw_bgroup(sub1,/nonexclusive,'show only mask',uval='imgunsharp_only',row=1)
  xpswg_imgset
  
  
  
  sub0=widget_base(subs,col=2)
  sub=widget_base(sub0,row=2)
  sub1=widget_base(sub,row=1)
  sub2=widget_base(sub0,col=1)
  lab=widget_label(sub1,value='Stokes Par:')
  xpswg.stokes.id=widget_combobox(sub1,uvalue='stokes', $
                                  value='Stokes '+['I','Q','U','V','Ic'])
  widget_control,xpswg.stokes.id, $
    set_combobox_select=xfwg.p(ixy(0),ixy(1)).stokes
  xpswg.obsfit.id=cw_bgroup(sub1,/exclusive,['obs','fit','fit:deconv'], $
                            set_value=xfwg.p(ixy(0),ixy(1)).obsfit, $
                            uval='obsfit',row=1)
;  dummy=cw_bgroup(sub2,/nonexclusive,['/Ic','ABS','MAX'],col=1)
  xpswg.ic.id=cw_bgroup(sub2,/nonexclusive,'/Ic', $
                        set_value=xfwg.p(ixy(0),ixy(1)).ic.val, $
                        uval='ic',row=1,xpad=0,ypad=0)
  xpswg.absval.id=cw_bgroup(sub2,/nonexclusive,'ABS', $
                        set_value=xfwg.p(ixy(0),ixy(1)).absval.val, $
                        uval='absval',row=1,xpad=0,ypad=0)
  xpswg.maxval.id=cw_bgroup(sub2,/nonexclusive,'MAX', $
                        set_value=xfwg.p(ixy(0),ixy(1)).maxval.val, $
                        uval='maxval',row=1,xpad=0,ypad=0)
  sub1=widget_base(sub,row=1)
  lab=widget_label(sub1,value='WL-Range:')
  for i=0,1 do $
    xpswg.wlrg(i).id=cw_field(sub1,xsize=10,/all_events,title='', $
                               uvalue='wlrg'+n2s(i), $
                               value=xfwg.p(ixy(0),ixy(1)).wlrg(i))
  xpswg.wlrgall.id=widget_button(sub1,value='Set WL for all Stokes Par', $
                               uvalue='wlrgall')
;hist tab  
  sub=widget_base(xpswg.tabid.hist,col=1,/frame)
  sub1=widget_base(sub,row=1)
  xpswg.histbin.id= $
    cw_field(sub1,xsize=4,/all_events,title='Number of bins', $
             uvalue='histbin',/integer, $
             value=fix(xfwg.p(ixy(0),ixy(1)).histo.nbins))
  xpswg.histnorm.id=cw_bgroup(sub1,/nonexclusive,'normalize (%)', $
                              set_value=xfwg.p(ixy(0),ixy(1)).histo.norm, $
                              uval='histnorm',row=1)  
  
  sub1=widget_base(sub,row=1)
  lab=widget_label(sub1,value='x-range:')
  for i=0,1 do $
    xpswg.histxrg(i).id=cw_field(sub1,xsize=10,/all_events,title='', $
                                 uvalue='histxrg'+n2s(i), $
                                 value=xfwg.p(ixy(0),ixy(1)).histo.xrange(i))
  
;scatter tab  
  sub=widget_base(xpswg.tabid.scat,row=2)
  sub0a=widget_base(sub,col=1,/frame)
  sub0b=widget_base(sub,col=1)
  sub1=widget_base(sub0a,row=1)
  lab=widget_label(sub1,/align_left,value='Scatter plot x-axis: '+ $
                   xfwg.p(ixy(0),ixy(1)).par.str+ $
                   '  x='+n2s(ixy(0))+',y='+n2s(ixy(1)))
  sub1=widget_base(sub0a,row=1)
  lab=widget_label(sub1,value='Scatter plot y-axis:',/align_left)
  xpswg.scatpar.id= $
    widget_combobox(sub1,uvalue='scatpar',value=contpar(1:*))
  widget_control,xpswg.scatpar.id, $
    set_combobox_select=xfwg.p(ixy(0),ixy(1)).scatter.par
  xfwg.p(ixy(0),ixy(1)).scatter.str= $
    (contpar(1:*))(xfwg.p(ixy(0),ixy(1)).scatter.par)
  
  sub0a=widget_base(sub,col=1,/frame)
  sub0b=widget_base(sub,col=1,/frame)
  sub1=widget_base(sub0a,row=1)
  lab=widget_label(sub1,value='x-range:')
  for i=0,1 do $
    xpswg.scatxrg(i).id=cw_field(sub1,xsize=10,/all_events,title='', $
                                 uvalue='scatxrg'+n2s(i), $
                                 value=xfwg.p(ixy(0),ixy(1)).scatter.xrange(i))
  sub1=widget_base(sub0a,row=1)
  lab=widget_label(sub1,value='y-range:')
  for i=0,1 do $
    xpswg.scatyrg(i).id=cw_field(sub1,xsize=10,/all_events,title='', $
                                 uvalue='scatyrg'+n2s(i), $
                                 value=xfwg.p(ixy(0),ixy(1)).scatter.yrange(i))
  sub1=widget_base(sub0a,row=1)
  lab=widget_label(sub1,value='z-range:')
  for i=0,1 do begin
    xpswg.scatzrg(i).id=cw_field(sub1,xsize=10,/all_events,title='', $
                                 uvalue='scatzrg'+n2s(i), $
                                 value=xfwg.p(ixy(0),ixy(1)).scatter.zrange(i))
    widget_control,xpswg.scatzrg(i).id, $
      sensitive=xfwg.p(ixy(0),ixy(1)).scatter.color eq 1
  endfor
  xpswg.scatcolor.id=cw_bgroup(sub0b,/nonexclusive,'2D histogram', $
                               set_value=xfwg.p(ixy(0),ixy(1)).scatter.color, $
                               uval='scatcolor',row=1)  
  xpswg.scatbin.id= $
    cw_field(sub0b,xsize=4,/all_events,title='# 2D bins', $
             uvalue='scatbin',/integer, $
             value=fix(xfwg.p(ixy(0),ixy(1)).scatter.nbins))
  widget_control,xpswg.scatbin.id, $
    sensitive=xfwg.p(ixy(0),ixy(1)).scatter.color eq 1
  xpswg.scathnorm.id=cw_bgroup(sub0b,/nonexclusive,'normalize (%)', $
                               set_value=xfwg.p(ixy(0),ixy(1)).scatter.hnorm, $
                               uval='scathnorm',row=1)  
  sub0c=widget_base(sub0b,col=2)
  xpswg.scatxlog.id=cw_bgroup(sub0c,/nonexclusive,'log10(x)', $
                              set_value=xfwg.p(ixy(0),ixy(1)).scatter.xlog, $
                              uval='scatxlog',row=1)  
  xpswg.scatylog.id=cw_bgroup(sub0c,/nonexclusive,'log10(y)', $
                              set_value=xfwg.p(ixy(0),ixy(1)).scatter.ylog, $
                              uval='scatylog',row=1)  
  
;  widget_control,xpswg.tab.id,set_tab_current=xfwg.p(ixy(0),ixy(1)).plotmode
  widget_control,xpswg.tab.id,set_tab_current=xpswg.tab.val
  
  xpswg.done.id=widget_button(xpswg.base.id,value=' Done ',uvalue='done')
  
  xpswg.ixy=ixy
  if keyword_set(init) eq 0 then begin
    widget_control,xpswg.base.id,/realize
    xmanager,'xpswg',xpswg.base.id,no_block=1
  endif
  
end

pro xfits_plotsetting,ixy,init=init
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xpswg,xpswg
  
  if n_elements(xpswg) ne 0 then begin
    widget_control,xpswg.base.id,bad_id=bad_id    
    if bad_id eq 0 then begin
      widget_control,xpswg.base.id,/destroy
    endif
  endif
  xfits_plotsetwg,ixy,init=init
 
end
