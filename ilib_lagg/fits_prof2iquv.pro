data=readfits('/home/lagg/data/PHI/inverted_profs_139000.fits',header)     
  dnew=data                    
  dnew[*,1,*,*]=data[*,3,*,*]               
  dnew[*,3,*,*]=data[*,1,*,*] 
  icont=reform(dnew[0,0,*,*])        
  
  
  sxaddpar,header,'NWL',992,'# of wavelength points'              
  writefits,'~/data/PHI/inverted_profs_139000_iquv.fits',dnew,hdr       
  
  
  sz=size(dnew)               
  naxes=sz[1:4]                      
  mkhdr,headeric,4,[naxes(2),naxes(3)],/image          
  sxaddpar,headeric,'XTENSION','IMAGE','IMAGE extension',before='SIMPLE'
  sxaddpar,headeric,'ICONTIMG','','continuum image'         
   writefits,'~/data/PHI/inverted_profs_139000_iquv.fits',icont,headeric,append=1
   
   wl=dindgen(992)/991*14-7+6173.341d                                         

   mkhdr,headerwl,5,naxes(0),/image                    
sxaddpar,headerwl,'XTENSION','IMAGE','IMAGE extension',before='SIMPLE'
sxaddpar,headerwl,'WLVEC','','wavelength vector'      
writefits,'~/data/PHI/inverted_profs_139000_iquv.fits',wl,headerwl,append=1


stop
end
