function calc_image_resolution,img,pixelsize=pixelsize, $
                               row=row,column=col,azi=azi,plot=plot, $
                               _extra=extra
  
  if n_elements(pixelsize) eq 0 then pixelsize=1.
  
                                ; compute power spectrum:
  powy=powerspectrum_al(img,azi=azi,col=col,row=row)
  npw=((size(powy,/dim))[0])
  powx=(((findgen(npw))/(npw-1)))/(pixelsize*2)
  
                                ;minimum plateau
                                ;shift plateau up by 20%
  minp=min(smooth(alog10(powy[1:*]*1.2),/edge_t,(npw/20)>1))
  
                                ;linear fit to central part of power
                                ;spectrum to compute crossing point
                                ;with minimum plateau:  
                                ;define range for linear fit
  if npw/2 ge 10 then i0=npw/20 else i0=0
  pm=mean(minmax(smooth(alog10(powy[i0:npw/2]),/edge_t,(npw/20<(npw/2-i0-1))>1)))
;  pm=pm*.8
  dummy=min(abs(alog10(powy[i0:npw/2])-pm),imin)
  ifit=((imin+i0+[-1,1]*((npw/10)>2))>1)<(npw-1)
                                ;do linfit
  lf=linfit(powx[ifit[0]:ifit[1]],alog10(powy[ifit[0]:ifit[1]]),chisqr=chisqr)
  yfit=10^(lf[0]+lf[1]*powx)
                                ;determine resolution (cutting point
                                ;linear fit /& minimum plateau
  linres=(minp-lf[0])/lf[1]
                                ;resolution: first point where power is
                                ;below plateau
  imin=(where(alog10(powy)-minp le 0))[0]-1
  if imin ge 1 and imin le npw-1 then minres=powx[imin] else minres=0.
  
                                ;resolution: if no noise then there is
                                ;no plateau. Determine point where
                                ;where power spectrum lies 10x below
                                ;linear fit
  diff=powy[ifit[0]:*]/yfit[ifit[0]:*]
  idiff=min(where(diff le 0.1))
  if idiff eq -1 then devres=powx[npw-1] else devres=powx[idiff+ifit[0]]
  
  reso={n:npw,powx:powx,powy:powy, $
        linres:1./linres,minres:1./minres,devres:1./devres, $
        pixelsize:pixelsize,plateau:minp,linfit:lf,chisqr:chisqr}
  
  if keyword_set(plot) then begin
    userlct
    thick=(!d.name eq 'PS')*2+1
    plot,reso.powx[1:*],reso.powy[1:*],ytype=1,xrange=[0,max(powx)], $
         xtitle='Spatial Frequency [1/arcsec], ' + $
         'Pixelsize = '+n2s(pixelsize,format='(f7.4)')+' arcsec', $
         ytitle='Power',/xst,_extra=extra,thick=thick
    oplot,color=1,linestyle=1,1./reso.linres+[0,0],10^!y.crange,thick=thick
    xyouts,color=1,/data,orientation=90,align=1.,1./reso.linres, $
           10^!y.crange[1],string(reso.linres,format='(f5.2)')+'''''  '
    oplot,color=3,linestyle=1,1./reso.minres+[0,0],10^!y.crange,thick=thick
    xyouts,color=3,/data,orientation=90,align=1.,1./reso.minres, $
           10^!y.crange[1],string(reso.minres,format='(f5.2)')+'''''  '
    oplot,color=4,linestyle=1,1./reso.devres+[0,0],10^!y.crange,thick=thick
    xyouts,color=4,/data,orientation=90,align=1.,1./reso.devres, $
           10^!y.crange[1],string(reso.devres,format='(f5.2)')+'''''  '
    yfit=10^(reso.linfit[0]+reso.linfit[1]*reso.powx[1:*])
    oplot,color=3,linestyle=2,reso.powx[1:*],yfit,thick=thick
    oplot,color=2,linestyle=2,!x.crange,10^reso.plateau+[0,0],thick=thick
  endif
  
  return,reso
end
