pro hinode_pointing,file,time1,time2,offset
  
;file=hinode pointing file
;time(2) = start and end time in format '22-Jun-08 08:00:00.000'
;offset - specify hinode offset (eg. caused by eclipse period)  
  
  if n_params() ne 2 and n_params() ne 3 and n_params() ne 4 then begin
    print,'Plot Hinode Pointing.'
    print,'Example:'
    print,'hinode_pointing,''~/tmp/re-point_200806240100.txt'',''25-Jun-08 07:00:00.000'',''25-Jun-08 12:00:00.000''[,offset]'
    print,'Offset = [x,y] vector with known offset (eg. eclips season offset)'
    reset
  endif
  if n_elements(time2) eq 0 then begin
    time2=time1
    addt=1.
  endif else addt=0.
  
  time=[time1,time2]
  hh=fix(strmid(time,10,2))
  mm=fix(strmid(time,13,2))
  ss=fix(strmid(time,16,2))
  date=strmid(time(0),0,10)
  dd=fix(strmid(time,00,2))+[0,addt]
  yy=fix(strmid(time,07,2))+2000
  mon=strmid(time,3,3)
  monstr=['Jan','Feb','Mar','Apr','May','Jun', $
          'Jul','Aug','Sep','Oct','Nov','Dec']
  
  mo=intarr(2)
  for i=0,1 do $
    mo[i]=where(monstr eq mon[i])+1
  
;  julian=julday(1,1,1970,hh,mm,ss)-julday(1,1,1970,24)
  julian=julday(mo,dd,yy,hh,mm,ss)-julday(1,1,1970,00)
  n=300
;  tvec=findgen(n)/(n-1)*(julian(1)-julian(0))
  dt=1440./10                    ;output every 10 minutes
  n=(julian(1)-julian(0))*dt
  tvec=findgen(n)/dt+julian[0]
  solxy=fltarr(2,n)
  tsol=dblarr(n)
  if n_elements(offset) eq 2 then off=offset else off=0
  for i=0l,n-1 do begin
    t=(tvec(i))
    tt=systime(0,t*86400,/utc)
    hms=strmid(tt,11,8)
    dat=strmid(tt,8,2)+'-'+strmid(tt,4,3)+'-'+strmid(tt,22,2)
    solxy(*,i)=solb_spacecraft_pointing(file,dat+' '+hms)
    print,dat+' '+hms,solxy(*,i)+off,format='(a,''   x='',f6.1,'',y='',f6.1)'
    tsol(i)=t+0.5
  endfor
  
  result=label_date(date_format='%h:%i')
  psfile=strcompress(date+'.ps',/remove_all)  
;  psset,/ps,file=psfile,size=[18,26]
  set_plot,'PS'
  device,file=psfile
  !p.multi=[0,1,2]
  if n_elements(offset) eq 2 then begin
    title='OFFSET: x='+string(offset(0),format='(f10.2)')+ $
      ', y='+string(offset(1),format='(f10.2)')
    solxy(0,*)=solxy(0,*)+offset(0)
    solxy(1,*)=solxy(1,*)+offset(1)
  endif else title='No Offset'
  title='Hinode Pointing - '+title
  xtitle='UT of '+date+'!C'+(reverse(str_sep(file,'/')))(0)
  plot,tsol,solxy(0,*),/xst,/yst,XTICKFORMAT='LABEL_DATE',title=title, $
    xtitle=xtitle,xticks=12,ytickformat='(f15.2)',ytitle='XPOS (arcsec)'
  plot,tsol,solxy(1,*),/xst,/yst,XTICKFORMAT='LABEL_DATE',title=title, $
    xtitle=xtitle,xticks=12,ytickformat='(f15.2)',ytitle='YPOS (arcsec)'
  
  atime=anytim2ints(time1)
  rb0p=get_rb0p(atime,/deg)
  print,'P0=',rb0p(2),', VTT-slit=',180.-rb0p(2)
  slit=180.-rb0p(2)
  xyouts,0.,.5,/normal,'VTT slit || SP slit: '+string(slit,format='(f15.2)')
  
;  psset,/close
  device,/close
  print,'Hinode pointing plotted to '+psfile
  
  val=where(solxy[0,*] ne -1 and solxy[1,*] ne -1)
  pfile=strcompress(date+'.txt',/remove_all)  
  openw,unit,/get_lun,pfile
  printf,unit,'Hinode pointing '+systime(0)
  printf,unit,'Time','Solar-X','Solar-Y',format='(a16,2a10)'
  if val[0] ne -1 then for i=0l,n_elements(val)-1 do begin
    t=(tvec[val[i]])
    tt=systime(0,t*86400,/utc)
    printf,unit,tt,solxy[0,val[i]],solxy[1,val[i]],format='(a16,2f10.2)'

  endfor
  free_lun,unit
  print,'Pointinfg written to: ',pfile
  stop
end
