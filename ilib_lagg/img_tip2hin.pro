;transforms TIP image to Hinode reference image usig the transformation
;stored in a sav file created with align_tip_hinode.pro.  The offset
;in x and y is (values p(0) and q(0)) is NOT used, unless the /shift
;keyword is applied. The offset should be applied to the image when
;printing the image. This way a truncation of the image is avoided.
function img_tip2hin,oimg,savtrans,shift=shift,verbose=verbose
  
  restore,savtrans,verbose=verbose
  img=oimg
  if trans.tipflip then img=reverse(img,2)
  if trans.tipmirror then img=reverse(img,1)
  if trans.tiprotate then img=rotate(img,1)
  
  sz=size(img)
  nx=fix(sz(1)*trans.xscale+1)
  ny=fix(sz(2)*trans.yscale+1)
  p=trans.p & q=trans.q
  if keyword_set(shift) eq 0 then begin
    if keyword_set(verbose) then begin
      print,'Image Shift not applied. Use the following shift for plotting:'
      print,'xshift=',trans.p(0)/trans.p(2)
      print,'yshift=',trans.q(0)/trans.q(2)
    endif
    p(0)=0 & q(0)=0
  endif
  imgret=poly_2d(img,p,q,2,nx,ny,cubic=-0.1,missing=!values.f_nan)
    
  return,imgret
end
