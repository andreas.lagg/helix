;+
; NAME:
;    msk_setrange
;
;
; PURPOSE:
;    sets range for plots defined in mask.pro
;
;
; CATEGORY:
;    creating mask-plots
;
;
; CALLING SEQUENCE:
;    maskvar=msk_setrange(maskvar,[xy]_percent=[xy]_percent)
;
; 
; INPUTS:
;    maskvar - structure (type = output from mask) containing info
;              about mask plot
;
;
; OPTIONAL INPUTS:
;
;
;	
; KEYWORD PARAMETERS:
;    [xyz]_percent - extend [xyz-range by .. percent (only if
;                   maskvar.arange_[xyz] is 1
;    label - if set, the maskvar corresponds to a label plot
;            (important to change also zrange-parameters)
;
; OUTPUTS:
;    maskvar - structure (type = output from mask) with adjusted 
;              ticknames/tickvalues
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;    A. Lagg, 03/06/1996
;-

;setting row / column on mask to new range / tick values
pro mask_newrange,muster,new,x=x,y=y,z=z

  if keyword_set(x) then begin
    new.xrange=muster.xrange
    new.arange_x=muster.arange_x
    new.xticks=muster.xticks
    new.xticks_orig=muster.xticks_orig
    new.xtickv=muster.xtickv
    new.xtickname=muster.xtickname
  endif

  if keyword_set(y) then begin
    new.yrange=muster.yrange
    new.arange_y=muster.arange_y
    new.yticks=muster.yticks
    new.yticks_orig=muster.yticks_orig
    new.ytickv=muster.ytickv
    new.ytickname=muster.ytickname
  endif

  if keyword_set(z) then begin
    new.zrange=muster.zrange
    new.arange_z=muster.arange_z
  endif
end

;gets ticknames and tickvalues 
pro get_tickname,tickname,n_ticks,tickv,offset,tick_str,t_format,sz,range, $
                 time=time
  
  doy=strarr(31) & year=strarr(31) & add_ts=strarr(32) & addstr=strarr(32)
  dcnt=0
                                ;calculate new tick values if range
                                ;exceeds 1 day
  if keyword_set(time) then begin

    
    if float(!version.release) ge 5.1 then begin
       tick_times=double(findgen((range(1)+96) /6.)*6.)+ $
         long(range(0))-48+offset-12
;        tick_times=double(findgen((range(1)+96) ))+ $
;          long(range(0))-48+offset-12
      day_times=remove_multi(long(tick_times/24l) * 24l) -offset+12
    endif else begin
      tick_times=double(findgen((range(1)+96)/6.)*6.)+ $
        long(range(0))-48+offset-24
      day_times=remove_multi(long(tick_times/24l) * 24l) -offset
    endelse
    
    idx_in=where(day_times ge range(0) and day_times le range(1))
    if idx_in(0) ne -1 then begin
      day_times=day_times(idx_in)
      nr_day=n_elements(day_times)
      if nr_day ge 1 and abs(range(0)-range(1)) gt 12 then begin
        case 1 of
          nr_day eq 1: new_ticks=indgen((nr_day+2)*6)*4
          nr_day eq 2: new_ticks=indgen((nr_day+2)*4)*6
          nr_day ge 3 and nr_day le 4: new_ticks=indgen((nr_day+2)*2)*12
          nr_day ge 5 and nr_day le 8: new_ticks=day_times
          nr_day ge 120 and nr_day le 365 : begin
            caldat,(day_times+offset)/24.,mm,dd,yy
            fom=where(fix(dd) eq 1)
            if fom(0) ne -1 then new_ticks=day_times(fom) $
            else new_ticks=day_times(indgen(8)/7.*nr_day)
          end
          nr_day gt 365 and nr_day le 730 : begin
            caldat,(day_times+offset)/24.,mm,dd,yy
            fom=where(fix(dd) eq 1 and (mm eq 1 or mm eq 7 or $
                                        mm eq 4 or mm eq 10))
            if fom(0) ne -1 then new_ticks=day_times(fom) $
            else new_ticks=day_times(indgen(8)/7.*nr_day)
          end
          nr_day gt 730 and nr_day le 1095 : begin
            caldat,(day_times+offset)/24.,mm,dd,yy
            fom=where(fix(dd) eq 1 and (mm eq 1 or mm eq 7))
            if fom(0) ne -1 then new_ticks=day_times(fom) $
            else new_ticks=day_times(indgen(8)/7.*nr_day)
          end
          nr_day gt  1095 : begin
            caldat,(day_times+offset)/24.,mm,dd,yy
            fom=where(fix(dd) eq 1 and (mm eq 1))
            if fom(0) ne -1 then new_ticks=day_times(fom) $
            else new_ticks=day_times(indgen(8)/7.*nr_day)
          end
          else: begin
            max_ticks=8
            idx=(indgen(max_ticks))* $
              ((nr_day/max_ticks)+((nr_day mod max_ticks) ne 0))
            idx=idx(where(idx lt nr_day))
            new_ticks=day_times(idx)
          end
        endcase
        idx_in=where(new_ticks ge range(0) and new_ticks le range(1))
        if idx_in(0) ne -1 then begin
          n_ticks=n_elements(idx_in)-1
          tickv(*)=0
          tickv(0:n_ticks)=new_ticks(idx_in)
        endif
      endif
    endif
  endif
  
  if keyword_set(time) then begin
    if offset eq 0 then os=julday(01,01,2000,0.)*24d $
    else os=offset    
  endif
  if range(1)-range(0) lt 0.25 then dig=8 else dig=5
  tkch=0
  timetick=0
  for k=1,30 do begin
;    if max(tickname(0:n_ticks)) gt ' ' then begin
    if max(tickname(0:n_ticks)) ne '' then begin
      if k-1 le n_ticks then tick_str(k)=tickname(k-1) $
      else tick_str(k)=''
    endif else if keyword_set(time) then begin      
      timetick=1
      dummy=tickv(k-1)+os & dummy=dummy(0)
      tick_str(k)=conv_time(dummy*3600d,old='sec',new='doy', $
                            /hm,/julian)
      dd=conv_time(dummy*3600d,old='sec',new='date_dot', $
                   /julian)
      date=strmid(tick_str(k),0,8)
      doy(k)=strmid(date,5,3)
      year(k)=strmid(date,0,4)
      add_ts(k)=strmid(dd,11,dig)
      addstr(k)=''
      if offset ne 0 and (year(k) lt '2020' and year(k) gt '1980') then begin
        if (doy(k) ne doy(k-1) or year(k) ne year(k-1))  then begin
          dcnt=dcnt+1
          if year(k) ne '****' then begin
;           if year(k) ne year(k-1) then addstr(k)=addstr(k)+year(k)+' '
;           addstr(k)=addstr(k)+'DOY '+doy(k)
            if year(k) eq year(k-1) or doy(k) ne '001' then $
              addstr(k)='!U'+strmid(year(k),2,2)+'!N'+doy(k) $
            else addstr(k)=strmid(year(k),0,4)
          endif else addstr(k)=addstr(k)+strtrim(string(dcnt),2)
        endif
      endif      
    endif else begin
      tkch=1
      if t_format ne '()' then $
        tick_str(k)=strcompress(string(tickv(k-1),format=t_format),/remove_all)
    endelse
  endfor
  if timetick and n_ticks ge 1 then begin
    ts=remove_multi(add_ts(1:n_ticks),/no_empty)
    if n_elements(ts) eq 1 and ts(0) eq '00:00' then $
      tick_str=addstr $
    else tick_str=add_ts+'!C'+addstr
  endif
  if tkch and n_ticks gt 0 then begin    
    ttm=tick_str(1:n_ticks+1)
    if n_elements(ttm) ne n_elements(remove_multi(ttm)) then $
      tick_str(1:n_ticks+1)=string(tickv(0:n_ticks))
  endif

                                ;remove characters after komma if redundant
  if n_ticks ge 1 then begin
    kommapos=strpos(tick_str(0:n_ticks+1),'.')
    epos=strpos(strlowcase(tick_str(0:n_ticks+1)),'e')-1
    ke=strarr(n_ticks+2)
    afterkomma=0
    remove=0
    repeat begin
      for i=1,n_ticks+1 do begin
;        if kommapos(i) eq -1 then kommapos(i)=0
        if kommapos(i) eq -1 then kommapos(i)=strlen(tick_str(i))
        if epos(i) lt 0 then epos(i)=strlen(tick_str(i))
        ke(i)=strmid(tick_str(i),kommapos(i)+afterkomma,epos(i)-afterkomma)
      endfor
      bk=byte(ke(1:*))
      allow=[byte('0123456789.-+eE '),0b]
      isnum=1
      for ib=0,n_elements(bk)-1 do if max(allow eq bk(ib)) eq 0 then isnum=0
      if isnum then if max(abs(float('0'+ke(1:*)))) eq 0 then begin        
        for i=1,n_ticks+1 do begin
          tick_str(i)=strmid(tick_str(i),0,kommapos(i)+afterkomma)+ $
            strmid(tick_str(i),epos(i)+1,strlen(tick_str(i)))
        endfor
        remove=1
      endif
      afterkomma=afterkomma+1
    endrep until min(kommapos+afterkomma-epos) gt 0 or remove eq 1
  endif
end

;set range to [1.,1.] if log scaling is enabled
function logrange,range,min=min

  if not keyword_set(min) then min=1e-10
  mipos=(where(range eq min(range)))(0)
  mapos=(where(range eq max(range)))(0)
  if range(mipos) le min and range(mapos) gt min then range(mipos)=min
  if range(mapos) le min and range(mipos) ge min then range(mapos)=min
  
  
  return,range
end

;expanding range by xp percent
function exp_range,old,percent,log

  if log eq 1 then begin
    dmy=alog10(old>1e-10)
    diff=dmy(1)-dmy(0)
    new=10^(dmy+[-diff,diff]/100.*percent)
  endif else begin
    diff=old(1)-old(0)
    new=old+[-diff,diff]/100.*percent
  endelse
  
  
  return,new
end

;prime factors for minor ticks
function prime_factor,numb,max_fact=max_fact
  
  cf=2 & cnt=1
  fact=1
  rn=numb
  ende=0
  repeat begin
    if rn mod cf eq 0 then begin
      fact=[fact,cf] 
      rn=long(rn)/cf
    endif else begin
      cnt=cnt+1
      cf=(primes(cnt))(cnt-1)<numb
    endelse
    prod=1
    for i=0,n_elements(fact)-1 do prod=prod*fact(i)
    if prod eq numb then ende=1
    if n_elements(max_fact) ne 0 then if max_fact gt 0 then $
      if cf gt max_fact then begin
      ende=1
      fact=-1
    endif
  endrep until ende
  return,fact
end

;returns # of minor ticks
function get_minor,min,max,time=time
  
  best=5
  if keyword_set(time) then begin ;time in hours
    secarr=[1,2,5,10,15,20,30,60]
    minarr=[1,2,5,10,15,20,30,60]
    hrsarr=[1,2,3,4,6,8,12,24]
    dayarr=[1,2,3,5,10,20,30,50,100]
    all=[secarr,minarr*60.,hrsarr*3600.,dayarr*86400.]
    rg=[max([min,max])*3600.-min([min,max])*3600.]
    ok=where((rg(0) mod all) eq 0)
    if ok(0) ne -1 then begin
      pref=rg/float(best)
      diff=abs(all(ok)-pref(0))
      idx=ok(where(diff eq min(diff)))
      val=(all(ok(idx)))(0)
      store=rg/val
    endif else store=1
  endif else begin
    rg=(max([min,max])-min([max,min]))
    t_range=[1,12]
    nelt=t_range(1)-t_range(0)+1
    ende=0
    curr=best
    add=0
    good=[1.,1/2.,1/4.,1/5.,1/10.,1/3.,1/8.,1/6.]
    val=fltarr(nelt,n_elements(good))
    ticks=intarr(nelt)
    i=0
    repeat begin
      if curr ge t_range(0) and curr le t_range(1) then begin
        if curr gt 1 then begin
          xp=alog10(abs(rg/float(curr)))
          no=rg/float(curr)*10.^(-(fix(xp) + (xp gt 0))) 
          val(i,*)=no mod good
        endif
        ticks(i)=curr
        i=i+1
      endif else begin
        ende=ende+0.5
      endelse
      if add gt 0 then add=-add else add=-add+1
      curr=best+add    
    endrep until ende gt 1
    n_idx=fltarr(nelt)
    for i=0,nelt-1 do begin
      mn=where(val(i,*) eq min(val))
      if mn(0) ne -1 then $
        n_idx(i)=total(n_elements(good)-mn)/float(n_elements(mn))
    endfor
    store=ticks((where(n_idx eq max(n_idx)))(0))
    
  endelse
  
  store=store(0)
  if store gt 7 then begin
    fct=prime_factor(store)
    new=store/fct(1)
    if new gt 12 and n_elements(fct) gt 2 then new=store/fct(2)
    store=new<12
  endif

  return,store
end

;draw minor tickmarks on log-type plots
pro make_minorticks,maskvar,sz,y=y,x=x,ticklen=ticklen
  
  if ticklen gt 1e-4 then begin
    case 1 of
      keyword_set(x): begin
        low=min(maskvar.xrange) & high=max(maskvar.xrange)*10
        pos=maskvar.position([1,3])
        tv=maskvar.xtickv(0:sz)
      end
      keyword_set(y): begin
        tv=maskvar.ytickv(0:sz)
        pos=maskvar.position([0,2])
        low=min(maskvar.yrange) & high=max(maskvar.yrange)*10
      end
      else: message,/traceback,'keyword x or y missing'
    endcase
    minticks=[0.]
    f=1. & fct=1.
    strt=10.^(fix(alog10(low))-1)
    repeat begin
      mval=strt*f*fct
      if f eq 10 then begin
        f=1 & fct=fct*10 
      endif
      f=f+1.
      if mval gt low and mval lt high then minticks=[minticks,mval]
    endrep until mval gt high
    
    if n_elements(minticks) gt 1 and n_elements(minticks) lt 60 then begin
      minticks=minticks(1:*)
      tf=0.5
      for i=0,1 do begin
        if keyword_set(x) then begin
          axis,/xaxis,0,pos(i),xtickv=minticks,xticklen=ticklen*tf*(i*2-1), $
            xticks=n_elements(minticks)+1,/device, $
            xtickname=strarr(n_elements(minticks))+' ',color=!p.color
        endif else begin
          axis,/yaxis,pos(i),0,ytickv=minticks,yticklen=ticklen*tf*(i*2-1), $
            yticks=n_elements(minticks)+1,/device, $
            ytickname=strarr(n_elements(minticks))+' ',color=!p.color
        endelse
      endfor
    endif
  endif
end

;sets range for mask-plot (possible to change range after creating
;mask, eg. when automatic ranging is enabled (xrange=[0,0]))
function msk_setrange,maskvar,x_percent=x_percent,y_percent=y_percent, $
                      label=label,z_percent=z_percent, $
                      additional_xlabel=additional_xlabel, $
                      add_xl_timeflag=add_xl_timeflag, $
                      no_xticks=no_xticks,no_yticks=no_yticks
  
  xt=fltarr(32)
  yt=fltarr(32)
  xtstr=strarr(32)
  ytstr=strarr(32)
  
  if max(maskvar.position([1,3])) gt !d.y_size or $
    max(maskvar.position([0,2])) gt !d.x_size then outor=1 else outor=0
  
  inf_x=where(finite(maskvar.xrange) eq 0)
  if inf_x(0) ne -1 then maskvar.xrange(inf_x)=0.
  inf_y=where(finite(maskvar.yrange) eq 0)
  if inf_y(0) ne -1 then maskvar.yrange(inf_y)=0.
  
  if maskvar.arange_x eq 1 then if keyword_set(x_percent) then $
      maskvar.xrange=exp_range(maskvar.xrange,x_percent,maskvar.xtype)
  if maskvar.arange_y eq 1 then if keyword_set(y_percent) then $
    maskvar.yrange=exp_range(maskvar.yrange,y_percent,maskvar.ytype)
  if maskvar.arange_z eq 1 then if keyword_set(z_percent) then $
    maskvar.zrange=exp_range(maskvar.zrange,z_percent,maskvar.ztype)
  
  if maskvar.xtype eq 1 then $
    maskvar.xrange=logrange(maskvar.xrange>maskvar.log_min, $
                            min=maskvar.xrange(0)>maskvar.log_min)
  if maskvar.ytype eq 1 then $
    maskvar.yrange=logrange(maskvar.yrange>maskvar.log_min, $
                            min=maskvar.yrange(0)>maskvar.log_min)

  
  pmn=1e-9 & pmx=1e-8
  
  if outor ne 1 then begin
    plot,[pmn,pmx],[pmn,pmx],/nodata,/noerase,xticks=maskvar.xticks_orig, $
      yticks=maskvar.yticks_orig,charsize=maskvar.charsize*.8,xst=5,yst=5, $
      xrange=maskvar.xrange,yrange=maskvar.yrange,xtick_get=xt_tmp,$
      ytick_get=yt_tmp,xtype=maskvar.xtype,ytype=maskvar.ytype, $
      pos=maskvar.position(*),/device
  endif else begin
    xt_tmp=0
    yt_tmp=0
  endelse
  
  
  yir=where(yt_tmp ge min(maskvar.yrange) and yt_tmp le max(maskvar.yrange))
  if yir(0) ne -1 then yt_tmp=yt_tmp(yir) else yt_tmp=['']
  xir=where(xt_tmp ge min(maskvar.xrange) and xt_tmp le max(maskvar.xrange))
  if xir(0) ne -1 then xt_tmp=xt_tmp(xir) else xt_tmp=['']
  
  if max(yt_tmp) gt max(maskvar.yrange) then  $
    yt_tmp(where(yt_tmp eq max(yt_tmp)))=max(maskvar.yrange)
  if max(xt_tmp) gt max(maskvar.xrange) then  $
    xt_tmp(where(xt_tmp eq max(xt_tmp)))=max(maskvar.xrange)
  sz0=size(xt_tmp) & xsz=sz0(1)
  sz0=size(yt_tmp) & ysz=sz0(1)
  xt(0:xsz)=[xsz,xt_tmp]
  yt(0:ysz)=[ysz,yt_tmp]

  if maskvar.arange_x eq 1 then maskvar.xtickname=strarr(31)
  maskvar.xtickv(0:xsz-1)=xt(1:xsz)
  maskvar.xticks=(xt(0)-1)
  xtn=maskvar.xticks & xtv=maskvar.xtickv
  get_tickname,maskvar.xtickname,xtn,xtv, $
    maskvar.xoffset,xtstr,maskvar.xformat,xsz,maskvar.xrange, $
    time=maskvar.xtime eq 1
  xtstr=strcompress(xtstr)
  maskvar.xticks=xtn & maskvar.xtickv=xtv

  
  if maskvar.arange_y eq 1 then maskvar.ytickname=strarr(31)
  maskvar.ytickv(0:ysz-1)=yt(1:ysz)
  maskvar.yticks=(yt(0)-1)
  ytn=maskvar.yticks & ytv=maskvar.ytickv  
  get_tickname,maskvar.ytickname,ytn,ytv, $
    maskvar.yoffset,ytstr,maskvar.yformat,ysz,maskvar.yrange, $
    time=maskvar.ytime eq 1
  ytstr=strcompress(ytstr)
  maskvar.yticks=ytn & maskvar.ytickv=ytv
  
  xtstr=replace_e(xtstr,maskvar.xticks)  
  ytstr=replace_e(ytstr,maskvar.yticks)

  if maskvar.xtickv(xsz-1) eq maskvar.xrange(1) then $
    if maskvar.number(0) lt maskvar.total(0)-1 then $
    xtstr(xsz)='!C'+xtstr(xsz)
;    if maskvar.ytickv(ysz-1) eq maskvar.yrange(1) then $
;      if maskvar.number(1) lt maskvar.total(1)-1 then $
;      ytstr(ysz)='!B'+ytstr(ysz)

  
  if maskvar.xrange(0) eq maskvar.xrange(1) then $
    maskvar.arange_x=1 else maskvar.arange_x=0
  if maskvar.yrange(0) eq maskvar.yrange(1) then $
    maskvar.arange_y=1 else maskvar.arange_y=0
  if maskvar.zrange(0) eq maskvar.zrange(1) then $
    maskvar.arange_z=1 else maskvar.arange_z=0

  xtitle=maskvar.xtitle
  ytitle=maskvar.ytitle
  if maskvar.number(0) ge 1 or maskvar.yt_flag eq 1 then begin
    for k=1,31 do ytstr(k)=' '
    ytitle=' '
  endif
  if maskvar.number(1) ge 1 or maskvar.xt_flag eq 1 then begin
    for k=1,31 do xtstr(k)=' '
    xtitle=' '
  endif
  
  maskvar.xtickname=xtstr(1:*)
  maskvar.ytickname=ytstr(1:*)
  
  if maskvar.arange_x eq 0 then $
    if max(maskvar.xtickname(0:xsz-1)) ne '' then maskvar.xt_flag=1
  if maskvar.arange_y eq 0 then $
    if max(maskvar.ytickname(0:ysz-1)) ne '' then maskvar.yt_flag=1

  if maskvar.yformat eq '()' then begin
    maskvar.ytickname=' '
  endif
  
  if maskvar.map eq 0 then begin
    if maskvar.arange_x eq 1 then begin
      maskvar.xticks=1 & maskvar.xtickname=[' ',' ']
      maskvar.xtickv(0:1)=[pmn,pmx]
      xtitle=' '
    endif
    if maskvar.arange_y eq 1 then begin
      maskvar.yticks=1 & maskvar.ytickname=[' ',' ']
      maskvar.ytickv(0:1)=[pmn,pmx]
      ytitle=' '
    endif
    xsz=maskvar.xticks
    ysz=maskvar.yticks

                                ;creating additional x-labels
                                ;(needs procedure find_xpos.pro, EPD
                                ;data processing)
    if maskvar.number(1) eq 0 then if maskvar.arange_x eq 0 then  $
      if n_elements(additional_xlabel) gt 1 then begin
      axl_x=additional_xlabel(*,0)-maskvar.xoffset
      idx=find_xpos(maskvar.xtickv(0:maskvar.xticks),axl_x)
      if n_elements(add_xl_timeflag) ne $
        n_elements(additional_xlabel(0,*)) then $
        add_xl_timeflag=bytarr(n_elements(additional_xlabel(0,*)))
      if maskvar.xticks+1 ne n_elements(idx) then  $
        message,/cont,'error in creating additional labels, msk_setrange.pro' $
      else begin
        szax=size(additional_xlabel)
        for i=0,maskvar.xticks do begin
          if idx(i) ne -1 then begin
            if i eq 0 or (idx(i) ne idx((i-1)>0)) then begin
              if szax(3) ne 7 then begin
;                 if maskvar.xformat ne '' then $
;                   str=string(additional_xlabel(idx(i),1:*), $
;                              format=maskvar.xformat) $
;                 else $
                if min(long(additional_xlabel(idx(i),1:*)) eq $
                       additional_xlabel(idx(i),1:*)) eq 1 then $
                  frmt='(i10)' else frmt='(f10.2)'
                str=string(additional_xlabel(idx(i),1:*),format=frmt)
                
              endif else str=additional_xlabel(idx(i),1:*)
              for j=0,n_elements(additional_xlabel(0,*))-1 do $
                if add_xl_timeflag(j) eq 1 then begin
                os=julday(01,01,2000,0)*24d
                dd=conv_time(/hm,/julian,old='sec',new='date_dot', $
                             (additional_xlabel(idx(i),j)+os)*3600d)
                str(j-1)=strmid(dd,11,5)
              endif
              str=strcompress(str,/remove_all)
              maskvar.xtickname(i)=maskvar.xtickname(i)+'!C'+ $
                string(str,format='('+string(n_elements(str))+'(a,:,''!C''))')
            endif
          endif
        endfor
      endelse
    endif
;    cc=intarr(xsz+1)
;    for k=0,xsz do cc(k)=n_elements(strsplit(/extraxt,/regex,maskvar.xtickname(k),'C'))-1
;    if max(cc) gt 0 then xtitle=string(reform(byte(strarr(max(cc))+'!C'), $
;                                              2*max(cc)))+xtitle

                                ;creating invisible plot to get ticksize
    xminor=1 & yminor=1
    if abs(maskvar.xrange(1)-maskvar.xrange(0)) eq 0 then begin
      xr=[pmn,pmx]
      xminor=0
      if abs(maskvar.yrange(1)-maskvar.yrange(0)) eq 0 then nvx=1 else nvx=0
    endif else begin
      xr=maskvar.xrange
      nvx=0
    endelse
    
    
    if outor ne 1 then $
      plot,xst=5,yst=5,[pmn,pmx],[pmn,pmx],/noerase,/nodata,/device, $
      position=maskvar.position(*),xrange=xr,yrange=maskvar.yrange, $
      xtype=maskvar.xtype,ytype=maskvar.ytype

    char_x=maskvar.charsize*!d.x_ch_size*1d
    char_y=maskvar.charsize*!d.y_ch_size*1d
    mx=max([char_x,char_y])
;   char_n=(convert_coord(maskvar.position(0)+[0d,mx], $
;                          maskvar.position(1)+[0d,mx],/device,/to_data))
;   char_n=abs([char_n(0,1)-char_n(0,0),char_n(1,1)-char_n(1,0)])
;   dx=(xr(1)-xr(0))
;   dy=(maskvar.yrange(1)-maskvar.yrange(0))
    char_n=[mx,mx]*.8
    mxr=maskvar.xrange
    if total(maskvar.xrange) eq 0 then mxr=xr
    if outor ne 1 then $
      dev=convert_coord(mxr,maskvar.yrange,/data,/to_device) $
    else dev=fltarr(2,2)
    dx=abs(dev(0,1)-dev(0,0))
    dy=abs(dev(1,1)-dev(1,0))
    if dx gt 0 then char_n(0)=char_n(0)/dx else char_n(0)=0
    if dy gt 0 then char_n(1)=char_n(1)/dy else char_n(1)=0
    if nvx eq 1 then begin
      char_n(0)=0
      xr=maskvar.xrange
      yminor=0
    endif
    char_n=char_n<0.2
    
    if char_n(1) eq 0 then begin
      ysz=1
      yminor=1
    endif
    
                                ;calculating # of minor ticks
    if maskvar.arange_x ne 1 then if xminor ne 0 then $
      xminor=get_minor(maskvar.xtickv(0),maskvar.xtickv(1), $
                       time=maskvar.xtime eq 1)
    if maskvar.arange_y ne 1 then if yminor ne 0 then $
      yminor=get_minor(maskvar.ytickv(0),maskvar.ytickv(1), $
                       time=maskvar.ytime eq 1)
    
    if maskvar.xtype eq 1 then xminor=0
    if maskvar.ytype eq 1 then yminor=0
    
    if keyword_set(no_yticks) then char_n(0)=1e-10
    if keyword_set(no_xticks) then char_n(1)=1e-10
    
;    if xminor eq 0 and $
;      min(strcompress(maskvar.xtickname(0:xsz),/remove_all) eq '') eq 0 then $
;      nxt=0 else nxt=xsz
;    if yminor eq 0 and $
;      min(strcompress(maskvar.ytickname(0:ysz),/remove_all) eq '') eq 0 then $
;      nyt=0 else nyt=ysz
    
    if maskvar.arange_x eq 0 then $
      if maskvar.xtype eq 1 and xminor eq 0 and xsz gt 0 $
;      and maskvar.xrange(0)-maskvar.xrange(1) ne 0 $
    then $
      make_minorticks,maskvar,xsz,/x,ticklen=char_n(1)
    
    
    if maskvar.arange_y eq 0 then $
      if maskvar.ytype eq 1 and yminor eq 0 and ysz gt 0 $
;      and maskvar.yrange(0)-maskvar.yrange(1) ne 0 $
    then $
      make_minorticks,maskvar,ysz,/y,ticklen=char_n(0)
    
                                ;creating plot frame (all titles)
    if outor ne 1 then begin
      xtitl=xtitle
      if strcompress(/remove_all,xtitl) ne '' then begin
        xwd=get_textwidth(xtitl,maskvar.charsize*.8)
        spwd=get_textwidth(' ',maskvar.charsize*.8)
        xtitl=xtitl+string(bytarr(((maskvar.position(2)- $
                                    maskvar.position(0)-xwd)/spwd-3)>1)+32b)
      endif
      
                                ;count line feeds in ytitle      
      nl=count_substring(ytitle,'!C')
;      if nl(0) ne -1 then $
;        ytitle=ytitle+add_comma(replicate('!C',n_elements(nl)),sep='')
      
                                ;if only one ytickv in log scaling is
                                ;available, add another one to display
                                ;minor tick marks
;       if maskvar.ytype eq 1 and ysz eq 0 then begin
;         ysz=1 & yminor=9
;         maskvar.ytickv(1)=maskvar.ytickv(0)*10
;         maskvar.ytickname(1)=' '
;       endif
;       if maskvar.xtype eq 1 and xsz eq 0 then begin
;         xsz=1 & xminor=9
;         maskvar.xtickv(1)=maskvar.xtickv(0)*10
;         maskvar.xtickname(1)=' '
;       endif
      
;       maskvar.ytickv=(maskvar.ytickv>min(maskvar.yrange))<max(maskvar.yrange)
;       maskvar.xtickv=(maskvar.xtickv>min(maskvar.xrange))<max(maskvar.xrange)

      plot,[pmn,pmx],[pmn,pmx],/nodata,/noerase,position=maskvar.position(*), $
        xtickname=maskvar.xtickname(0:xsz),xrange=xr, $
        xtype=maskvar.xtype,xtickv=maskvar.xtickv(0:xsz),xticks=xsz, $
        yrange=maskvar.yrange,ytype=maskvar.ytype,xst=1,yst=1, $
        ytickname=maskvar.ytickname(0:ysz),yticks=ysz, $
        ytickv=maskvar.ytickv(0:ysz),xtitle=xtitl, $
        ytitle=ytitle,/device,charsize=maskvar.charsize*.8, $
        xthick=1.5,ythick=1.5,xticklen=char_n(1),yticklen=char_n(0), $
        xminor=xminor,yminor=yminor,color=!p.color
    endif
    
  endif else begin
    p=maskvar.position(*)
    if outor ne 1 then $
      for k=0,1 do begin
      dummy=convert_coord(p(k*2:k*2+1),/to_normal,/device)
      p(k*2:k*2+1)=dummy(0:1)
    endfor    
    if maskvar.number(0) eq 0 then begin
      myt=maskvar.ytitle
      dummy=count_substring(myt,'!C',position=epc)
      if epc(0) eq -1 then lines=0 else lines=n_elements(epc)
      if outor ne 1 then begin
        ll=(convert_coord([0,(lines)*!d.y_ch_size*0.8*maskvar.charsize], $
                          /device,/to_normal))(1)
        xyouts,p(0)-(p(2)-p(0))/10.-ll,p(1)+(p(3)-p(1))/2.,alignment=0.5, $
          /normal,myt,orientation=90,charsize=0.8*maskvar.charsize
      endif
    endif
    p=[p(0)+(p(2)-p(0))/50.,p(1),p(2)-(p(2)-p(0))/50.,p(3)]
    if outor ne 1 then $
      p_dev=convert_coord([[p(0),p(1)],[p(2),p(3)]],/normal,/to_device) $
    else p_dev=fltarr(4)
    p_dev=reform(p_dev(0:1,*),4)
    if maskvar.spherical then begin
      dx=abs(p_dev(2)-p_dev(0))
      if maskvar.back eq 1 then dx2=dx/2. else dx2=dx
      dy=abs(p_dev(3)-p_dev(1))
      ma=[dx2,dy,dx]
      mp=where(ma eq min(ma)) & mp=mp(0)
      if maskvar.back eq 1 then fx=2 else fx=1
      min=ma(mp)/2.
      p_dev=[p_dev(0)+dx/2.-min*fx,p_dev(1)+dy/2.-min, $
             p_dev(0)+dx/2.+min*fx,p_dev(1)+dy/2.+min]
    endif
    maskvar.position(*)=p_dev
  endelse

  if keyword_set(label) then begin
    maskvar.arange_z=maskvar.arange_y
    maskvar.zrange=maskvar.yrange
  endif
  
  return,maskvar
end




