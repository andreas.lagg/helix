;compute bx,by,bz from ambiguity resolved b,inc,azi
function bincazi2xyz,bmat,lon=lon,lat=lat,b0angle=b0angle,pangle=pangle, $
                     reverse=reverse
  
  sz=size(bmat,/dim)
  sz0=(size(bmat))[0]
  if sz[0] ne 3 or sz0 gt 3 then $
    message,'Input must be an array with dimensions' + $
            ' [3], [3,n] or [3,x,y]'
  if n_elements(lon) eq 0 then $
    message,'please specify longitude: ' + $
            'lon=atan(xcen/sqrt(radius^2-xcen^2-ycen^2))'
  if n_elements(lat) eq 0 then $
    message,'please specify latitude: lat=asin(ycen/radius)'
  if n_elements(b0angle) eq 0 then $
    message,'please specify solar B0-angle: b0angle=...'
  if n_elements(pangle) eq 0 then $
    message,'please specify solar P-angle: pangle=...'
  
;***********************************************************************
;     Construct coordinate transformation matrices from Gary and       *
;     Hagyard (1990).                                                  *
;     Note that the matrix C is the transpose of A, which is also the  *
;     inverse of A, since A is orthogonal.                             *
;***********************************************************************
  
  mat=fltarr(3,3)
  
  p=pangle*!dtor
  b=b0angle*!dtor
  phi=lat*!dtor                 ;latitude = asin(ycen/radius)
  theta=lon*!dtor               ;central meridian angle
                                ;lon=atan(xcen/sqrt(radius^2-xcen^2-ycen^2))
  
  mat[0,0]=-sin(b)*sin(p)*sin(theta) + cos(p)*cos(theta)
  mat[0,1]=-sin(phi)*(sin(b)*sin(p)*cos(theta)+cos(p)*sin(theta))- $
           cos(phi)*cos(b)*sin(p)
  mat[1,0]= sin(b)*cos(p)*sin(theta) + sin(p)*cos(theta)
  mat[1,1]= sin(phi)*(sin(b)*cos(p)*cos(theta) - sin(p)*sin(theta)) + $
            cos(phi)*cos(b)*cos(p)
  
  mat[2,0]=-cos(b)*sin(theta)
  mat[2,1]=-cos(b)*sin(phi)*cos(theta) + sin(b)*cos(phi)
  mat[0,2]= cos(phi)*(sin(b)*sin(p)*cos(theta) + cos(p)*sin(theta))- $
            sin(phi)*cos(b)*sin(p)
  mat[1,2]=-cos(phi)*(sin(b)*cos(p)*cos(theta) - sin(p)*sin(theta))+ $
           sin(phi)*cos(b)*cos(p)
  mat[2,2]= cos(phi)*cos(b)*cos(theta) + sin(phi)*sin(b)
  
  if keyword_set(reverse) eq 0 then begin
    ci=cos(bmat[1,*,*,*]*!dtor)
    si=sin(bmat[1,*,*,*]*!dtor)
    ca=cos(bmat[2,*,*,*]*!dtor)
    sa=sin(bmat[2,*,*,*]*!dtor)
    bf=bmat[0,*,*,*]
    bx=bf*si*ca
    by=bf*si*sa
    bz=bf*ci
  endif else begin
    mat=transpose(mat)
    bx=bmat[0,*,*,*]
    by=bmat[1,*,*,*]
    bz=bmat[2,*,*,*]
  endelse
  
  
  bxn=mat[0,0]*Bx+mat[1,0]*By+mat[2,0]*Bz
  byn=mat[0,1]*Bx+mat[1,1]*By+mat[2,1]*Bz
  bzn=mat[0,2]*Bx+mat[1,2]*By+mat[2,2]*Bz
  
  if keyword_set(reverse) eq 0 then begin
    b1=bxn
    b2=byn
    b3=bzn
  endif else begin
    b1=sqrt(bxn^2+byn^2+bzn^2)
    b3=atan(byn,bxn)/!dtor
    b2=acos(bzn/b1)/!dtor
  endelse
  if sz0 eq 2 then bout=transpose([[[b1]],[[b2]],[[b3]]]) $
  else bout=transpose([[[transpose(b1)]],[[transpose(b2)]],[[transpose(b3)]]])
;  bout=transpose([[[b1]],[[b2]],[[b3]]])
  
  return,bout
end
