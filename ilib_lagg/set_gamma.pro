;set gamma value for color table
;identical to gamma in xloadct
pro set_gamma,gamma,show=show
  
  gamma=(gamma>0.1)<10
  
  tvlct,/get,r,g,b
  nc=256
  ncolors=256
  cbot=0
  vbot=0 & vtop=100
  
  s = (nc-1)/100.
  x0 = vbot * s
  x1 = vtop * s
  if x0 ne x1 then s = (nc-1.0)/(x1 - x0) else s = 1.0
  int = -s * x0
  if gamma eq 1.0 then s = round(findgen(nc) * s + int > 0.0) $
  else s = ((findgen(nc) * (s/nc) + (int/nc) > 0.0) ^ gamma) * nc
  too_high = where(s ge nc, n)
  if n gt 0 then s[too_high] = 0L
  s = s < 255L
  l = lindgen(ncolors) + cbot
  r = s[r[l]]
  g = s[g[l]]
  b = s[b[l]]
	tvlct, r,g,b
  
  if keyword_set(show) then tv,indgen(256) # (intarr(50)+1)
  
end
