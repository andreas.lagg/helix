;create spinor 4D fitscube from MURAM fits files
;example:
;muram2fits4d,'312000','312000.fits',dirin='/data/sunrise/2009/SUFI/mhd/MURaM/MySimulations/MURaM_RUN_1/20111110.30G.ngrey.576x100x576/'



;directory processing example:
;dirproc,'/data/sunrise/2009/SUFI/mhd/MURaM/MySimulations/MURaM_RUN_1/20111110.30G.ngrey.576x100x576/','/home/lagg/data/MuRAM/'


pro dirproc,dir,outdir
  
  ff=findfile(dir+'eosT.*.fits',count=cnt)
  if cnt eq 0 then message,'no eosT fits files in '+dir
  
  froot=(reverse(strsplit(dir,'/',/extract)))[0]
  spawn,'mkdir -p '+outdir+froot
  
  for i=cnt-1,0,-1 do begin ;start with last frame
    num=(strsplit(ff[i],'eosT.',/regex,/extract))[1]
    num=(strsplit(num,'.',/extract))[0]
    out=outdir+froot+'/'+num+'.fits'
    print,'Processing num='+num
    muram2fits4d,num,out
    print
  endfor
  
  stop
end

pro muram2fits4d,num,oname,dirin=dirin,dirout=dirout,mode=mode,mask=mask,ext=ext
;
  if n_elements(dirin) eq 0 then dirin=''
  if n_elements(dirout) eq 0 then dirout=''
  
  if n_elements(mask) eq 0 then mask='result_'
  if n_elements(ext) eq 0 then ext='.fits'
  
  if mode eq 'eos' then begin
    sep='.'
    pfile='eosP'
    tfile='eosT'
    bfiles=mask+'_'+['5','6','7']
    vfiles=mask+'_'+['1','2','3']
    dfile=mask+'_0'
  endif else begin
    sep='_'
    pfile='pres'
    tfile='temp'
    bfiles=['magx','magy','magz']
    vfiles=['velx','vely','velz']
    dfile='dens'
  endelse
  
  print,'reading '+dirin+tfile+sep+num+ext
  t=read_fits_dlm(dirin+tfile+sep+num+ext,0,hh)
  t=transpose(temporary(t),[1,0,2])

  tx=double(sxpar(hh,'T_X'))
  ty=double(sxpar(hh,'T_Y'))
  tz=double(sxpar(hh,'T_Z'))
  szf=size(t)
  dz=float(ty)/szf[1]
  dx=float(tx)/szf[2]
  dy=float(tz)/szf[3]

  npar=9
  mhdcube=fltarr(npar,szf[1],szf[2],szf[3])
  mhdcube[0,*,*,*]=t[*,*,*]
  
  
  print,'reading '+dirin+pfile+sep+num+ext
  p=read_fits_dlm(dirin+pfile+sep+num+ext,0,hh)
  p=transpose(temporary(p),[1,0,2])
  help,p,mhdcube
  
  mhdcube[2,*,*,*]=p[*,*,*]
;
  
;
; 'eosP.'
; 'eosT.'
; 'result_0.' ! density
; 'result_1.' ! vx
; 'result_2.' ! vy
; 'result_3.' ! vz
; 'result_4.' ! energy
; 'result_5.' ! bx
; 'result_6.' ! by
; 'result_7.' ! bz
;
  
  vars=['d','vx','vy','vz','bx','by','bz'] 
  nums=['0','1','2','3','5','6','7'] 
  files=[dfile,vfiles,bfiles]
  slot=[1,3,4,5,6,7,8]
  for i=0,npar-3 do begin
    sfile=dirin+files[i]+sep+num+ext
    print,'reading ',sfile
    dat=read_fits_dlm(sfile,0,header)
;Tino: Mail, 12.6.2015:
; Die Einheit ist cm/s, allerdings gibt es eine Schwierigkeit. Du musst result_1, result_2, result_3 durch result_0 dividieren bevor Du die Geschw. in cm/s erhältst und result_5, result_6, result_7 muss mit sqrt(4*pi) multipliziert werden. Jedenfalls meistens. Manchmal haben die MHD-Leute das auch schon gemacht. Ich habe mittlerweile so viele MHD-Cubes, dass ich vergessen habe, wie es bei dem unten genannten ist. Probiere es halt aus. Habe Dir Karten von B und v_z aus diesem Cube angehängt, die ich benutze. Die Karten sind allerding bei tau=1 und nicht bei konstanter geom. Höhe wie in den Cubes. Die Umrechnungs-Cubes sind aber auch in dem u.g. Verzeichnis.
    if i eq 0 then d0=dat
    if mode eq eos then begin
      normv=d0 & normb=sqrt(4.*!pi)
    endif else begin
      normv=1. & normb=1.
    endelse
    case i of 
      0: d0=dat
      1: dat=dat/normd
      2: dat=dat/normd
      3: dat=dat/normd
      5: dat=dat*normb
      6: dat=dat*normb
      7: dat=dat*normb
      else:
    endcase
    mhdcube[slot[i],*,*,*]=transpose(dat,[1,0,2])
    image_cont_al,/aspect,contour=0,title=vars[i],reform(mhdcube[slot[i],67,*,*]) & wait,1
  endfor
  
  add=''
  mhdfile=dirout+oname
  
  addvx='' 
  addvy=' (vertical direction)'
  addvz=''
  
  mkhdr,header,mhdcube
  sxaddpar,header,'COMMENT','MuRAM 4D cube for SPINOR'
  fxaddpar,header,'PAR1','TEMP','temperature [K]'
  fxaddpar,header,'PAR2','RHO','density [g/cm^3]'
  fxaddpar,header,'PAR3','PRESS','pressure  [dyn/cm^2]'
  fxaddpar,header,'PAR4','VX','x-velocity [cm/s]'+addvx
  fxaddpar,header,'PAR5','VY','y-velocity [cm/s]'+addvy
  fxaddpar,header,'PAR6','VZ','z-velocity [cm/s]'+addvz
  fxaddpar,header,'PAR7','BX','B-Field X [G]'+addvx
  fxaddpar,header,'PAR8','BY','B-Field Y [G]'+addvy
  fxaddpar,header,'PAR9','BZ','B-Field Z [G]'+addvz

  fxaddpar,header,'T_X',tx,'x total size [cm]'
  fxaddpar,header,'T_Y',ty,'total depth [cm]'
  fxaddpar,header,'T_Z',tz,'y total size [cm]'
  
  
  hdr=header
  for i=0,n_elements(header)-1 do hdr[i]=(strsplit(header[i],/extract))[0]
  header=header[0:where(hdr eq 'END')]
  
  write_fits_dlm,mhdcube,mhdfile,header,/create
  print,'Wrote 4D-FITS cube with MHD data: ',mhdfile
stop
end
