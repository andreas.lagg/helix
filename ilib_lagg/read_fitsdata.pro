;read in inversion data from fits file and create an IDL structure
;containing the data and header information
pro read_fitsdata,file
  common fits,fits
  common xdir,xdir
  common bfitidx,bfitidx
  
  fi=file_info(file)
  if fi.exists eq 0 then begin
    print,'Cannot find FITS file: '+file
    reset
  end
  if n_elements(xdir) eq 0 then xfits_dir
  
  !err=0
  data=readfits(file,hdr)
  if !err lt -1 then begin
    message,/cont,'Error '+n2s(!err)+' in Reading FITS File: '+file
    reset
  endif
;  hst=fits_header2st(hdr)
  
  sz=size(data)
  nx=sz(1) & ny=sz(2) & npar=sz(3)
  valid=finite(data)
  code=strtrim(sxpar(hdr,'CODE'),2)
  case strupcase(code) of 
    'HELIX+': code='helix'
    'SPINOR': code='spinor'
    else: begin
      message,/cont,'Unknown inversion code. Assume spinor'
      code='spinor'
    endelse
  end
  pixelrep=fix(sxpar(hdr,'PIXELREP'))>1
                                ;is last parameter
                                ;get parameter names
  opar=strtrim(sxpar(hdr,'PAR*',comment=ocmt),2)
  if n_elements(opar)*pixelrep ne npar then begin
    message,/cont,'Number of parameters is not compatible with PIXELREP.'
    print,"Parameters: ",opar
    print,"Number of parameters = ",npar
    print,"PIXELREP = ",pixelrep
    opar=strtrim(sxpar(hdr,'IMTYPE*',comment=ocmt),2)
  endif
  if opar(0) eq '0' then begin
    np=sxpar(hdr,'CHISQ')       ;is last parameter
    ibz=sxpar(hdr,'BZSOL',/silent)
    if ibz ne 0 then begin
      print,'Ambiguity Resolved Data: Reading BXSOL,BYSOL,BZSOL'
      np=ibz
      ncoff=ibz-sxpar(hdr,'AZIMU',/silent)
    endif
    if np(0) eq '0' then begin
      print,'File does not contain atmospheric data from SPINOR or Helix:'
      print,file
      reset
    endif
    code='spinor'
    iltt=(where(strpos(hdr,'LTTOP') eq 0))(0) 
    opar=strarr(np)
    ocmt=strarr(np)
    for i=0,np-1 do begin
      opar(i) =(strsplit(/extract,hdr(iltt+i)))(0)
      ocmt(i) =(strsplit(/extract,hdr(iltt+i),'/'))(1)
    endfor
  endif
  nop=n_elements(opar)
  
                                ;read in input structure (currently helix only)
                                ;write out other ascii files contained
                                ;in fits file
  iext=1
  iptext=-1
  repeat begin
    errmsg=''
    exthdr=headfits(file,errmsg=errmsg,ext=iext,/silent)
    if errmsg eq '' then begin
      ascii=readfits(file,exthdr,ext=iext,/silent)      
      if sxpar(exthdr,'TTYPE1') eq 'ASCII-Lines' then begin
        asciifile=sxpar(exthdr,'ASCIINAM')
        lines=strtrim(string(ascii))
        typ=sxpar(exthdr,'EXTNAME')
        case typ of
          'INPUTFILE': begin
            if code eq 'helix' then asciifile='input.ipt' $
            else if code eq 'spinor' then asciifile='inv.inp'
            iptext=iext
          end
          else:
        endcase
        ffile=xdir.tmp+asciifile
        if lmgr(/demo) then print,'DEMO-Mode - input file not written' $
        else begin
          print,'Writing file ',ffile
          fdir=strmid(ffile,0,strpos(ffile,'/',/reverse_search)+1)
          if fdir ne '' then file_mkdir,fdir
          openw,unit,/get_lun,ffile
          for i=0,n_elements(lines)-1 do printf,unit,lines(i)
          free_lun,unit
        endelse
      endif
      iext=iext+1
    endif
  endrep until errmsg ne ''
  
  obsfile='' & synthfile=''
  if iptext ne -1 then begin
    iptst=strtrim(string(readfits(file,hdript,hdr,exten_no=iptext)))
    if code eq 'helix' then begin    
      rdir=['ATOM ','WGT ','SAV ','ATM_ARCHIVE ']
      for i=0,n_elements(rdir)-1 do begin
        dir=where(strtrim(strpos(strupcase(iptst),rdir(i)),2) eq 0)
        if dir(0) ne -1 then iptst(dir)=rdir(i)+'  '+xdir.tmp
      endfor
      ipt=read_ipt(ascii=iptst)

                                ;set noise to zero: Noise was already added when writing the fits file, no need to add it again
      if ipt.noise gt 0 then begin
        print,"Noise level in input file: ",ipt.noise
        print,"Setting noise level to zero, since noise was already added when producing the atm-file."
        print,"NOISE=0.000"
        ipt.noise=0.0
      endif
                                ;check prefilter & convolution files: 
      if ipt.prefilter ne '' then $
        ipt.prefilter=xdir.tmp+ipt.prefilter
      if ipt.conv_func ne '' then $
        ipt.conv_func=xdir.tmp+ipt.conv_func
      obsfile=ipt.dir.profile+'/'+ipt.observation
    endif else ipt=iptst
  endif else ipt=''
  
;------------------
;classify parameter
;------------------
  
                                ;set identifier for logtau value
  oltau=fltarr(nop)
  ocomp=intarr(nop)             ;set counter for component
  if code eq 'spinor' then begin
    for i=0,nop-1 do begin
      if opar[i] eq 'BXSOL' or opar[i] eq 'BYSOL' or opar[i] eq 'BZSOL' then $
        ii=i-ncoff $
      else ii=i
      splt=strsplit(/extract,ocmt(ii)) ;component is 3rd number in comment
      if n_elements(splt) ge 4 then ocomp(i)=fix(splt(3)) 
    endfor
    for ic=1,max(ocomp) do begin
      iic=where(ocomp eq ic)
      ilgt=where(opar eq 'LGTRF' and ocomp eq ic)
      if ilgt(0) ne -1 then begin
        ntau=n_elements(ilgt)   
        for j=0,n_elements(iic)-1 do begin ;check for parameters with same ntau
          isame=where(opar(iic) eq opar(iic(j)))
          if n_elements(isame) eq ntau then for il=0,ntau-1 do begin
            lgtmap=data(*,*,ilgt(il))
            noz=where(lgtmap ne 0.)
            if noz(0) eq -1 then lgt=0. else lgt=mean(lgtmap(noz))
            oltau(iic(isame(il)))=lgt
          endfor
        endfor
      endif
    endfor
  endif else begin
                                ;component counter for Helix
                                ;parameters
    ip=0
    ocomp[*]=-1
    for ic=0,ipt.ncomp-1 do $
      for it=0,n_tags(ipt.atm(ic).fitflag)-1 do $
        if ipt.atm(ic).fitflag.(it) eq 1 then begin
      ocomp[ip]=ic+1
      ip=ip+1
    endif
    for i=0,nop-1 do if ocomp[i] eq -1 then begin
      ocomp(i)=total(opar(0:i) eq opar(i))
    endif
  endelse
  
  oclass=strarr(nop)            ;parameter classification
  oclass(*)='ATM'
  if code eq 'helix' then begin
    genpar=['CCORR','STRAYLIGHT','RADCORRSOLAR']
    for i=0,nop-1 do begin
      if strpos(opar(i),'LINE') eq 0 then oclass(i)='LINE' $
      else if strpos(opar(i),'BLEND') eq 0 then oclass(i)='BLEND' $
      else if max(opar(i) eq genpar) eq 1 then oclass(i)='GEN' $
      else if strpos(opar(i),'FITNESS') eq 0 then oclass(i)='FITNESS'
    endfor
  endif else if code eq 'spinor' then begin
    for i=0,nop-1 do begin
      if strpos(opar(i),'CHISQ') eq 0 then oclass(i)='CHISQ'
    endfor
  endif
  
  par=opar
  cmt=ocmt
  comp=ocomp
  ltau=oltau
  class=oclass
                                ;pixel repetition: every pixel may
                                ;contain n times the parameter set
  pixrep=intarr(npar) 
  if code eq 'helix' then begin
    if npar mod nop ne 0 then $
      message,'Problem with FITS file: number of parameters does not' + $
      ' match FITS dimension.'
    for i=1,npar/n_elements(par)-1 do begin
      par=[par,opar]
      cmt=[cmt,ocmt]
      comp=[comp,ocomp]
      ltau=[ltau,oltau]
      class=[class,oclass]
    endfor
                                ;set counter for pixel-repetition run
    for i=0,npar/n_elements(opar)-1 do pixrep(i*nop:(i+1)*nop-1)=i
  endif
  
                                ;define valid array: where fitness=0 or TEMPE=0
  if code eq 'helix' then ival=where(class eq 'FITNESS') $
  else begin
    ival=(where(par eq 'TEMPE'))(0)
    if ival(0) eq -1 then ival=(where(par eq 'CHISQ'))(0)
  endelse
  ii=0
  if ival(0) ne -1 then for i=0,npar-1 do begin
    valid(*,*,i)=valid(*,*,i) and data(*,*,ival(ii)) ne 0
    if code eq 'helix' then if i ge ival(ii) then ii=ii+1
  endfor

                                ;descriptive names
  longnam=par+', Comp: '+n2s(comp)
  short=par+' C'+n2s(comp)
  if code eq 'helix' then begin
    if min(pixrep) ne max(pixrep) then begin
      longnam=longnam+', PixRep='+n2s(pixrep)
      short=short+' PR'+n2s(pixrep)
    endif
  endif
  if code eq 'spinor' then begin 
    longnam=longnam+', LTAU='+n2s(ltau,format='(f15.2)')
    short=short+' LT'+n2s(ltau,format='(f15.2)')
  endif
  
  
  if code eq 'helix' then $    
    synthfile=get_helixfitsname(file,/synth)
  if code eq 'spinor' then begin
    obsfile=get_spinorfitsname(file,/obs)
    synthfile=get_spinorfitsname(file,/synth)
  endif
  
  spath=['',xdir.helix]
  for is=0,n_elements(synthfile)-1 do begin
    ii=0
    repeat begin
      fi=file_info(spath(ii)+synthfile(is))
      ii=ii+1
    endrep until (fi.exists ne 0 and fi.regular eq 1) or ii ge n_elements(spath)
    if fi.exists eq 0 then begin
      message,/cont,'Cannot find obs-file '+synthfile(is)
    endif else if fi.regular eq 1 then synthfile(is)=fi.name
  endfor
      
                                ;x/y offset
  if code eq 'helix' then begin
    xoff=fix(sxpar(hdr,'XOFFSET'))
    yoff=fix(sxpar(hdr,'YOFFSET'))
  endif else begin
    xoff=0 & yoff=0
  endelse
  
  pix2pix=[1.,1.]               ;conversion from pixel to pixel
                                ;(e.g. when x/y binning is set)
  pixoff=fltarr(2)
  pix2arcsec=[1.,1.]            ;conversion from pixel to arcsec
  arcsecoff=fltarr(2)

  for io=0,n_elements(obsfile)-1 do begin
                                ;treat TIP CC files
    spath=['',xdir.helix]
    ii=0
    repeat begin
      fi=file_info(spath(ii)+obsfile(io))
      ii=ii+1
    endrep until  fi.regular ne 0 or ii ge n_elements(spath)
    if fi.regular eq 0 then begin
      message,/cont,'Cannot find obs-file '+obsfile(io)
    endif else if strlen(obsfile(io)) ge 2 then $
      obsfile(io)=fi.name
    fi=file_info(obsfile(io))
    if fi.regular then begin
      obshdr=headfits(obsfile(io))
      xpix=float(sxpar(obshdr,'STEPSIZE',count=c))
      if c eq 0 then xpix=1.
      if strtrim(sxpar(obshdr,'TELESCOP',count=c)) eq 'GREGOR' then ypix=0.126 $
      else ypix=0.16
      if c eq 0 then ypix=1.
      pix2arcsec=[xpix,ypix]
      if strmid(obsfile(io),strlen(obsfile(io))-2,2) eq 'cc' then begin

                                ;check for x/y binning
        szv=size(valid)
        if max(valid) ne 0 then for i=0,1 do begin
          if szv(2-i) ge 2 then col=total(valid(*,*,0),2-i,/nan) ne 0 $
          else col=intarr(szv(i+1))+1
          poff=min(where(col eq 1))
          ccol=shift(col,poff)
          ii=1
          repeat begin
            ccol=ccol+shift(col,ii+poff)
            pend=0
            if ii ge n_elements(col) then pend=1 $
            else pend=max(ccol(ii:*)) ge 2
            ii=ii+1
          endrep until pend
          pix2pix(i)=ii-1
          if pix2pix(i) ge 2 then begin
            pixoff(i)=poff
            for j=1,pix2pix(i)-1 do begin
              
              if i eq 0 then begin
                data(where(col eq 1)+j,*,*)=data(where(col eq 1),*,*) 
                valid(where(col eq 1)+j,*,*)=valid(where(col eq 1),*,*) 
              endif else begin
                data(*,where(col eq 1)+j,*)=data(*,where(col eq 1),*)
                valid(*,where(col eq 1)+j,*)=valid(*,where(col eq 1),*)
              endelse
              
            endfor
          endif
        endfor
      endif
    endif
  endfor
  
                                ;special treatment of parameters,
                                ;e.g. spinor azimuth should be between
                                ;-180 and 180 degrees
  if code eq 'spinor' then begin
    iazi=where(par eq 'AZIMU')
    if iazi(0) ne -1 then for i=0,n_elements(iazi)-1 do begin
;      data(*,*,iazi(i))=((data(*,*,iazi(i))+360 +90) mod 180) -90.
      data(*,*,iazi(i))=((data(*,*,iazi(i))+720 +180) mod 360) -180.
    endfor
  endif
  
  
                                ;add best fitness map, mean map,
                                ;... if pixel repetition was eneabled
  if max(pixrep) ge 1 then begin
    addpar=['bestfit','meanval','meanbest50','stddev','stddevbest50']
    print,'Pixel repetition detected. Creating Statistical arrays:'
    print,add_comma(addpar)
    nadd=n_elements(addpar)
    npr=max(pixrep)+1
    np0=(npar/npr)
    npadd=np0*nadd
    datanew=fltarr(nx,ny,npar+npadd)
    datanew(*,*,0:npar-1)=data & data=temporary(datanew)
    validnew=fltarr(nx,ny,npar+npadd)
    validnew(*,*,0:npar-1)=valid & valid=temporary(validnew)
    parnew=strarr(npar+npadd)
    parnew(0:npar-1)=par & par=temporary(parnew)
    shortnew=strarr(npar+npadd)
    shortnew(0:npar-1)=short & short=temporary(shortnew)
    longnew=strarr(npar+npadd)
    longnew(0:npar-1)=longnam & longnam=temporary(longnew)
    pixrepnew=intarr(npar+npadd)-1
    pixrepnew(0:npar-1)=pixrep & pixrep=temporary(pixrepnew)
    compnew=intarr(npar+npadd)
    compnew(0:npar-1)=comp & comp=temporary(compnew)
    classnew=strarr(npar+npadd)
    classnew(0:npar-1)=class & class=temporary(classnew)
    ltaunew=fltarr(npar+npadd)
    ltaunew(0:npar-1)=ltau & ltau=temporary(ltaunew)
    
    ifit=where(class eq 'FITNESS')
    allfit=data(*,*,ifit)
    dummy=max(allfit,dimension=3,bfitidx)
    srt=lonarr(nx,ny,npr)
    for ix=0,nx-1 do for iy=0,ny-1 do $
      srt(ix,iy,*)=reverse(sort(allfit(ix,iy,*)))
    n50=(npr*50/100)>1
    srt50arr=srt(*,*,0:n50-1)
    ixarr50=lonarr(nx,ny,n50)
    iyarr50=lonarr(nx,ny,n50)
    for i=0,n50-1 do begin
      ixarr50(*,*,i)=indgen(nx) # (intarr(ny)+1)
      iyarr50(*,*,i)=(intarr(nx)+1) # indgen(ny)
    endfor
    for ia=0,nadd-1 do begin
      iadd=ia*np0+npar
      for ip=0,np0-1 do begin
        allpr=data(*,*,indgen(npr)*np0+ip)
        allval=valid(*,*,indgen(npr)*np0+ip)
        if par(ip) eq 'AZIMU' then period=[-90,90] $
        else if par(ip) eq 'GAMMA' then period=[0,180] $
        else period=0
        case addpar(ia) of 
          'bestfit': begin
            data(*,*,iadd+ip)=allpr(bfitidx)
            valid(*,*,iadd+ip)=allval(bfitidx)
            pixrep(iadd+ip)=-1
            addshort='BEST'
            addlong='best fit'
          end
          'meanval': begin
            data(*,*,iadd+ip)= $
              mean_periodic(allpr,valid=allval,dimension=3,period=period, $
                            sdev=sdevmean)
            valid(*,*,iadd+ip)=total(allval,3) ne 0
            pixrep(iadd+ip)=-2
            addshort='AVG'
            addlong='mean value'
          end
          'meanbest50': begin
            data(*,*,iadd+ip)= $
              mean_periodic(allpr(ixarr50,iyarr50,srt50arr), $
                            valid=allval(ixarr50,iyarr50,srt50arr), $
                            dimension=3,period=period) 
            if (size(allval(ixarr50,iyarr50,srt50arr)))(0) eq 3 then begin
              valid(*,*,iadd+ip)=total(allval(ixarr50,iyarr50,srt50arr),3) ne 0
            endif else valid(*,*,iadd+ip)=allval(ixarr50,iyarr50,srt50arr)
            pixrep(iadd+ip)=-3
            addshort='AVG50'
            addlong='mean value, best 50%'
          end
          'stddev': begin
            if par(ip) eq 'GAMMA' or par(ip) eq 'AZIMU' then begin
              dummy=mean_periodic(allpr,valid=allval,dimension=3, $
                                  period=period,sdev=sdev)
              data(*,*,iadd+ip)=sdev
            endif else begin
              for ix=0,nx-1 do for iy=0,ny-1 do begin
                data(ix,iy,iadd+ip)=sqrt((moment(allpr(ix,iy,*),/nan))(1))
              endfor
            endelse
            valid(*,*,iadd+ip)=total(allval,3) ne 0
            pixrep(iadd+ip)=-4
            addshort='STD'
            addlong='standard deviation'
          end
          'stddevbest50': begin
            if par(ip) eq 'GAMMA' or par(ip) eq 'AZIMU' then begin
              dummy=mean_periodic(allpr(ixarr50,iyarr50,srt50arr), $
                                  valid=allval(ixarr50,iyarr50,srt50arr), $
                                  dimension=3,period=period,sdev=sdev)
              data(*,*,iadd+ip)=sdev
            endif else begin
              for ix=0,nx-1 do for iy=0,ny-1 do begin
                data(ix,iy,iadd+ip)= $
                  sqrt((moment(allpr(ix,iy,srt50arr(ix,iy,*)),/nan))(1))
              endfor
            endelse
            if (size(allval(ixarr50,iyarr50,srt50arr)))(0) eq 3 then begin
              valid(*,*,iadd+ip)=total(allval(ixarr50,iyarr50,srt50arr),3) ne 0
            endif else valid(*,*,iadd+ip)=allval(ixarr50,iyarr50,srt50arr)
            pixrep(iadd+ip)=-5
            addshort='STD50'
            addlong='standard deviation, best 50%'
          end
        endcase
        par(iadd+ip)=par(ip)
        comp(iadd+ip)=comp(ip)
        ltau(iadd+ip)=ltau(ip)
        class(iadd+ip)=class(ip)
        par(iadd+ip)=par(ip)
        short(iadd+ip)= $
          (strsplit(/regex,/extract,short(ip),' PR'))(0)+' '+addshort
        longnam(iadd+ip)= $
          (strsplit(/regex,/extract,longnam(ip),', Pix'))(0)+' '+addlong
      endfor
    endfor    
    npar=npar+npadd
    print,'Statistical arrays created.'
  endif
  
                                ;add parameters bx,by,bz if ambiguity
                                ;resolved fits file is present
  if sxpar(hdr,'AZI_AMBI') eq 1 then begin
    ib=where(par eq 'BFIEL')
    ia=where(par eq 'AZIMU')
    ig=where(par eq 'GAMMA')
    if (n_elements(ib) ne n_elements(ia) or $
        n_elements(ib) ne n_elements(ig)) then begin
      message,/cont,'AZI_AMBI mode: Cannot add BX,BY,BZ.' + $
              ' Same number of height layers for B, INC, AZI required'
    endif else begin
      npadd=3*n_elements(ib)
      datanew=fltarr(nx,ny,npar+npadd)
      datanew(*,*,0:npar-1)=data & data=temporary(datanew)
      validnew=fltarr(nx,ny,npar+npadd)
      validnew(*,*,0:npar-1)=valid & valid=temporary(validnew)
      parnew=strarr(npar+npadd)
      parnew(0:npar-1)=par & par=temporary(parnew)
      shortnew=strarr(npar+npadd)
      shortnew(0:npar-1)=short & short=temporary(shortnew)
      longnew=strarr(npar+npadd)
      longnew(0:npar-1)=longnam & longnam=temporary(longnew)
      pixrepnew=intarr(npar+npadd)-1
      pixrepnew(0:npar-1)=pixrep & pixrep=temporary(pixrepnew)
      compnew=intarr(npar+npadd)
      compnew(0:npar-1)=comp & comp=temporary(compnew)
      classnew=strarr(npar+npadd)
      classnew(0:npar-1)=class & class=temporary(classnew)
      ltaunew=fltarr(npar+npadd)
      ltaunew(0:npar-1)=ltau & ltau=temporary(ltaunew)
      print,'AZI_AMBI mode detected. Adding solar reference parameters:'
      lon=sxpar(hdr,'LON')
      lat=sxpar(hdr,'LAT')
      b0angle=sxpar(hdr,'B0ANGLE')
      pangle=sxpar(hdr,'PANGLE')
      print,'B0ANGLE',b0angle,format='(a10,''='',f8.2)'
      print,'PANGLE',pangle,format='(a10,''='',f8.2)'
      print,'LON',lon,format='(a10,''='',f8.2)'
      print,'LAT',lat,format='(a10,''='',f8.2)'
      if (abs(pangle) lt 1e-5 or abs(b0angle) lt 1e-5 or $
          abs(lat) lt 1e-5 or abs(lon) lt 1e-5) then begin
        message,/cont,'WARNING: At least one of the above parameters is zero. Make sure to enter the correct values in the header of the FITS file '+file
        print,'Press key to continue ...'
        key=get_kbrd()
      endif
      for ii=0,n_elements(ib)-1 do begin
        idx=npar+[0,3,6]+ii
        ip=ib[ii]
        ltau[idx]=ltau[ip]
        comp[idx]=comp[ip]
        class[idx]=class[ip]
        pixrep[idx]=pixrep[ip]
        par[idx]=['BX','BY','BZ']+'SOL'
        class[idx]=class[ip]
        short[idx]=par[idx]+' C'+n2s(comp[ip])+ $
                   ' LT'+n2s(ltau[ip],format='(f15.2)')
        print,'Adding '+add_comma(short[idx])
        longnam[idx]=par[idx]+', Comp: '+n2s(comp[ip])+ $
                     ', LTAU='+n2s(ltau[ip],format='(f15.2)')
        for j=0,2 do begin
          data[*,*,idx[j]]= $
            reform((bincazi2xyz(transpose([[[data[*,*,ib[ii]]]], $
                                           [[data[*,*,ig[ii]]]], $
                                           [[data[*,*,ia[ii]]]]],[2,0,1]), $
                                lon=lon,lat=lat, $
                                b0angle=b0angle,pangle=pangle))[j,*,*])
          valid[*,*,idx[j]]=(valid[*,*,ib[ii]] and valid[*,*,ig[ii]] and $
                             valid[*,*,ia[ii]])
        endfor
      endfor
      npar=npar+npadd
    endelse
  endif
  
  show=bytarr(npar)+1b
  noshow=['LTTOP','LTINC','LGTRF']
  for i=0,n_elements(noshow)-1 do begin
    ins=where(strpos(par,noshow(i)) eq 0)
    if ins(0) ne -1 then show(ins)=0b
  endfor
  
  fits={file:file,code:code,data:data,valid:valid,header:hdr,ipt:ipt, $
        nx:nx,ny:ny,npar:npar,par:par,cmt:cmt,short:short,long:longnam, $
        show:show,pixrep:pixrep,comp:comp,logtau:ltau,class:class, $
        obsfile:obsfile,nobs:n_elements(obsfile), $
        synthfile:synthfile,nsynth:n_elements(synthfile), $
        xoff:xoff,yoff:yoff,pix2arcsec:pix2arcsec,arcsecoff:arcsecoff, $
                                ;add keywords from ambiguity removal routine
        azi_ambi:sxpar(hdr,'AZI_AMBI'), $
        date_obs:sxpar(hdr,'DATE_OBS'), $
        xcen:sxpar(hdr,'XCEN'),ycen:sxpar(hdr,'YCEN'), $
        lon:sxpar(hdr,'LON'),lat:sxpar(hdr,'LAT'), $
        radius:sxpar(hdr,'RADIUS'),b0angle:sxpar(hdr,'B0ANGLE'), $
        pangle:sxpar(hdr,'PANGLE')}
  print,'FITS data stored in common block: common fits,fits'

end
