;converts number to string, removes blanks
function n2s,number,format=format,add_zero=add_zero

  sz=size(number)
  if sz(sz(0)+1) eq 1 then nr=fix(number) else nr=number
  if n_elements(format) eq 0 then format=''
  
  packet=1000                   ;split in subpackets because of IDL-format
                                ;restriction
  szn0=size(number(0))
  if szn0(szn0(0)+1) ne 7 then retval=string(number*0.) else retval=number
  retval(*)=''
  
  nn=n_elements(number)
  for ip=0l,nn-1,packet do begin
    ipp=(ip+packet-1)<(nn-1)
    retval(ip:ipp)=strcompress(string(nr(ip:ipp),format=format),/remove_all)
  endfor
  
  if keyword_set(add_zero) then begin
    len=strlen(retval)
    nok=where(len lt add_zero)
    if nok(0) ne -1 then for i=0,n_elements(nok)-1 do $
      retval(nok(i))=string(bytarr(add_zero-len(nok(i)))+48b)+retval(nok(i))
  endif
  return,retval
end
