function tip2cdsdate,date,time
  
;print,'###',date,'###'  

  if strmid(date,0,3) eq '200' or strmid(date,0,3) eq '199' $
    or strmid(date,0,3) eq '201' then begin
    utc = {cds_ext_time,	$
           year:	fix(strmid(date,0,4)),	$
           month:	fix(strmid(date,5,2)),	$
           day:	fix(strmid(date,8,2)),	$
           hour:	fix(strmid(time,0,2)),	$
           minute: fix(strmid(time,3,2)),	$
           second:	fix(strmid(time,6,2)),	$
           millisecond: 0}
  endif else begin
    imon=where(['jan','feb','mar','apr','may','jun','jul', $
      'aug','sep','oct','nov','dec'] eq strlowcase(strmid(date,3,3)))+1
    utc = {cds_ext_time,	$
           year:	fix('20'+strmid(date,7,2)),	$
           month:	fix(imon(0)),	$
           day:	fix(strmid(date,0,2)),	$
           hour:	fix(strmid(time,0,2)),	$
           minute: fix(strmid(time,2,2)),	$
           second:	0,	$
           millisecond: 0}
  endelse
  
  return,utc
end
