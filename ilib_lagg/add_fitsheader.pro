;adds user value to fitsheader
function add_fitsheader,fitsfile,parameter
  common askonce,first
  
  if n_elements(first) eq 0 then first=1
  
  if first eq 0 then return,0d
  
  print,'Fitsfile: ',fitsfile
  print,'keyword not found in header: ',parameter
  
  print,'Adding keyword manually: '+parameter
  parval=0d
  read,'Value for '+parameter+': ',parval
  
  
  fi=file_info(fitsfile)
  if fi.write eq 0 then begin
    print,'No write permission: ',fitsfile
    return,parval
  endif
  
  yn=''
  read,'Should this parameter be added to the header of the FITS file [y/n]?',yn
  
  if strupcase(string(yn)) eq 'Y' then begin
    data=readfits(fitsfile,hdr)
    sxaddpar,hdr,parameter,double(parval),'added parameter'
    writefits,fitsfile,data,hdr
  endif else first=0
  
  return,double(parval)
end
