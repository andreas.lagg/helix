

;apply spectral & spatial PSF to 4D data cube

;example:
;apply_psf,'/home/lagg/data/MuRAM/20111110.30G.ngrey.576x100x576/inverted_profs.1.fits',view=0,/re


;apply_psf,'inverted_profs.1.fits',gridxy=16.,detectorpixel=0.20,psf='gregor_psf_lor0.8_gauss0.25_0.20arcsec.fits',spectral_fwhm=0.15,wlpixel=0.04

pro apply_psf,fits, reread=reread,view=view,gridxy=gridxy,regx=regx,regy=regy, $
              detectorpixel=detpix,psf=psf,outfile=outfile,wlpixel=wlpixel, $
              binning=binning,spectral_fwhm=spectral_fwhm   ;,spec_psf=spec_psf
  common pdata,cube_orig,oldfits,hdr,order
  
                                ;flags for degrading steps
  noise=0b
  
  help=0
  if n_elements(gridxy) eq 0 then help=1
  if n_elements(detpix) eq 0 then help=1
  if n_elements(psf) eq 0 then message,/cont,'No PSF-file given. Performing only spectral degradation.'
  
  if help then begin
    print,'Usage:'
    print,'apply_psf,''inverted_profs.1.fits'''
    print,'Keywords (mandatory):'
    print,'  psf=''gregor_psf_lor0.8_gauss0.25_mhd.fits'' ;fits file containing psf (must have same pixel size as detector)'
    print,'  gridxy=16. ; gridsize per pixel in km'
    print,'  detectorpixel=0.126 ; detector pixel size in arcsec'
    print,'  psf=''gregor_psf_lor0.8_gauss0.25_mhd.fits'' ;fits file containing psf (must have same pixel size as detector)'
    print,'Keywords (optional):'
    print,'  binning=0 ;switch off binning to detector pixel size (default: on)'
    print,'  wlpixel=0.010d ;pixel size in WL-direction (angstrom)'
    print,'  spectral_fwhm=0.1 ;spectral convolution with gaussian (fwhm in Angstrom)'
    print,'  outfile=''file.fits'' ;name of output file. default: ''[dir-of-fits-file/]degraded_profs.fits'''
    print,'  reread=1,view=1'
    print
    print,'Example: (in ~/data/MuRAM/rempel_qs_dynamo/):'
    print,'apply_psf,''inverted_profs.15650.fits'',gridxy=10.42,detectorpixel=0.126,psf=''gregor_psf_lor0.8_gauss0.25_mhd.fits'',spectral_fwhm=0.1'
    retall
  endif
  fi=file_info(fits)
  if fi.exists eq 0 then message,'FITS file not found: '+fits
  
  if n_elements(psf) ne 0 then begin
    pi=file_info(psf)
    if pi.exists eq 0 then message,'PSF file not found: '+psf
  endif
  
  
  fp=get_filepath(fits)
  
  if n_elements(outfile) eq 0 then outfile=fp.path+'degraded_profs.fits'
;  outfile=fp.path+'mhd.30G_binned.fits'
  
  
  
  if n_elements(binning) eq 0 then binning=1b else binning=keyword_set(binning)
  spatial=1b
  
  pixscale=gridxy/700.           ;pixel scale in arcsec of input Stokes data
  fct=round(detpix/pixscale/3)>1 ;reduction factor to increase speed
  pixscale=pixscale*fct
  pix2arcsec=pixscale
  print,'Reduced pixel scale by factor '+n2s(fct)+' to increase speed.'
  print,'Old pixel scale: ',gridxy/700.
  print,'New pixel scale: ',pixscale
  print,'Detector pixel scale: ',detpix
  
;  detpix=0.126                  ;detector pixel size in arcsec (spatial)
  
                                ;spatial PSF
;  psf='~/work/projects/Gregor/GRIS/PSF/gregor_psf_ideal_mhd.fits'
;  psf='~/work/projects/Gregor/GRIS/PSF/gregor_psf_gauss0.45_mhd.fits'
;  psf='~/work/projects/Gregor/GRIS/PSF/gregor_psf_lor1.0_mhd.fits'
;  psf='~/work/projects/Gregor/GRIS/PSF/gregor_psf_lor0.8_gauss0.25_mhd.fits'
                                ;spectral psf
;  spec_psf=
  
  
  reread=keyword_set(reread) or n_elements(oldfits) eq 0
  if reread eq 0 then reread=oldfits ne fits
  if reread then begin
    cube_orig=read_fits_dlm(fits,0,hdr)    
;    cube_orig=readfits(fits,hdr)
    if n_elements(regx) eq 2 then cube_orig=cube_orig[*,*,regx[0]:regx[1],*]
    if n_elements(regy) eq 2 then cube_orig=cube_orig[*,*,*,regy[0]:regy[1]]
  endif
                                ;reduce cubesize to increase speed
  sz=size(cube_orig,/dim)
  if fct ge 2 then begin
    cnew=cube_orig[*,*,0:sz[2]/fct-1,0:sz[3]/fct-1]
    for i1=0,sz[1]-1 do cnew[*,i1,*,*]= $
      congrid(reform(cube_orig[*,i1,*,*]),sz[0],sz[2]/fct,sz[3]/fct) ;,/interp)
    cube=temporary(cnew)
  endif else cube=temporary(cube_orig)
                                ;correct order: IQUV instead of IVQU
  cube=cube[*,[0,2,3,1],*,*]
  order='IQUV'
  
  oldfits=fits
  
  wlmin=sxpar(hdr,'WLMIN')
  wlmax=sxpar(hdr,'WLMAX')
  wlref=sxpar(hdr,'WLREF')
  nwl=sxpar(hdr,'NWL')
  wlvec=dindgen(nwl)/(nwl)*(wlmax-wlmin)+wlmin+wlref
  wlpix=wlvec[1]-wlvec[0]
  if n_elements(wlpixel) eq 0 then wlpixel=wlpix $
  else wlpixel=double(wlpixel)  ;detector pixel size in Angstrom (spectral)
  
  sz=size(cube,/dim)
  print,'Using 4D data cube: '+fits
  print,'Dimensions: ',sz
  
  ;make it faster: for tests only compute the first 4 WL points....
;  sz[0]=4 & sz[1]=1   & wlvec=wlvec[0:3] & nwl=4
  
  degcube=cube
;Degrading step #1: spectral convolution
  if n_elements(spectral_fwhm) ne 0 then begin
    fwhm_pix=spectral_fwhm/wlpix
    sigma=fwhm_pix/(2*sqrt(2*alog(2.)))
    nw=5
    kernelx=findgen(fix(nw*fwhm_pix/2)*2+1)-fix(nw*fwhm_pix/2)
    kernel=exp(-0.5*(kernelx/sigma)^2)
    kernel=kernel/total(kernel)
    print,'Starting spectral degrading with a gaussian: FWHM=',spectral_fwhm,'A, sigma=',sigma,'pix'
    for is=0,sz[1]-1 do for ix=0,sz[2]-1 do for iy=0,sz[3]-1 do begin
        degcube[*,is,ix,iy]=convol(degcube[*,is,ix,iy],kernel,/edge_truncate)
        iproc=((is*sz[2]+ix)*sz[3]+iy+1)*100./(sz[1]*sz[2]*sz[3])
        print,iproc,format='(f8.2,"%",$,%"\r")'
      endfor
    print
  endif
;Degrading step #2: wl-pixel binning to detector
  if binning then begin
    print,'WL-Binning to detector ...'
    newnwl=fix((wlmax-wlmin)/wlpixel)
    newwl=dindgen(newnwl)/newnwl*(wlmax-wlmin)+wlmin+wlref
    newsz=sz[2:3]/detpix*pixscale
    pix2arcsec=detpix
    cubedet=fltarr(newnwl,sz[1],sz[2],sz[3])
    for is=0,sz[1]-1 do begin
      for ix=0,sz[2]-1 do for iy=0,sz[3]-1 do begin
        cubedet[*,is,ix,iy]=interpol(degcube[*,is,ix,iy],wlvec,newwl,/lsq)
        iproc=((is*sz[2]+ix)*sz[3]+iy+1)*100./(sz[1]*sz[2]*sz[3])
        print,iproc,format='(f8.2,"%",$,%"\r")'
     endfor
    endfor
    print
    degcube=temporary(cubedet)
    wlvec=newwl
    nwl=newnwl
  endif
  sz=size(degcube,/dim)
  print,'Dimensions: ',sz
  
;Degrading step #3: spatial convolution
  
  if n_elements(psf) ne 0 then begin
;  psfdat=read_fits_dlm(psf,0,phdr)
  psfdat=readfits(psf,phdr)
  print,'Spatial PSF:'
  print,phdr[max(where(strpos(phdr,'COMMENT') eq 0))+1:*]
  asec_pix=sxpar(phdr,'ASEC_PIX')
  
                                ;interpolate PSF to match Stokes pixel
                                ;scale
  psz=size(psfdat,/dim)
  pszip=psz*asec_pix/pixscale
  psf_ip=congrid(psfdat,pszip[0],pszip[1],/interp)
  psf_ip=psf_ip/total(psf_ip)
                                ;do convolution for every wavelength
                                ;point and stokes parameter
  if spatial then begin
    print,'Starting spatial degrading...'
    for is=0,sz[1]-1 do begin
      for iw=0,sz[0]-1 do begin
        degcube[iw,is,*,*]=convol(reform(degcube[iw,is,*,*]),psf_ip,/edge_wrap)
        if keyword_set(view) then begin
          !p.multi=[0,2,1] & !p.position=0
          image_cont_al,/aspect,/cut,contour=0,reform(cube[iw,is,*,*]), $
                        title='Orig'
          image_cont_al,/aspect,/cut,contour=0,reform(degcube[iw,is,*,*]), $
                        title='degraded'
        endif
        iproc=(is*sz[0]+iw+1)*100./(sz[0]*sz[1])
        print,iproc,format='(f8.2,"%",$,%"\r")'
;        iprog=(is*sz[0]+[iw-1,iw])*80./(sz[0]*sz[1])
;        if fix(iprog[0]) ne fix(iprog[1]) then print,'.',format='(a,$)'
      endfor
    endfor
    print
  endif else degcube=cube
endif

;Degrading step #4: pixel binning to detector
  if binning then begin
    print,'Binning to detector ...'
    newsz=sz[2:3]/detpix*pixscale
    pix2arcsec=detpix
    cubedet=fltarr(sz[0],sz[1],newsz[0],newsz[1])
    print,'Stokes '
    for is=0,sz[1]-1 do begin
      print,(['I','Q','U','V'])(is),format='(a,$)'
      cubedet[*,is,*,*]= $
        congrid(reform(degcube[*,is,*,*]),sz[0],newsz[0],newsz[1])
    endfor
    print
    degcube=temporary(cubedet)
  endif
  sz=size(degcube,/dim)
  print,'Dimensions: ',sz
  

;Degrading step #4: adding noise
  ;; if noiseflag then begin
  ;;   print,'Adding noise not yet implemented'
  ;; endif
  
;determining parameters of the degraded data  
  ic=reform(degcube[0,0,*,*])
  if keyword_set(view) then begin
    userlct,coltab=0,/full,/reverse
    !p.position=0 & !p.multi=[0,2,1]
    image_cont_al,contour=0,/aspect,/cut,ic
    userlct,coltab=0
    !p.position=0
  endif
  reso=calc_image_resolution(ic,pixelsize=pix2arcsec,/azi, $
                             plot=keyword_set(view))
  mom=moment(ic/mean(ic))
  rms=sqrt(mom[1])
  
;writing out new file
  nhdr=hdr
  mkhdr,nhdr,degcube
  nhdr=[nhdr[0:9],hdr[10:*]]
  sxaddpar,nhdr,'FILEORIG',fits,'original filename'
  if n_elements(psf) ne 0 then $
    sxaddpar,nhdr,'FILEPSF',psf,'PSF-file used for convolution'
  sxaddpar,nhdr,'SPECFWHM',spectral_fwhm,'FWHM in Angstrom of Gaussian used for spectral convolution'
  sxaddpar,nhdr,'STOKES',order
  sxaddpar,nhdr,'NWL',nwl
  sxaddpar,nhdr,'PIXSIZE',pix2arcsec,'pixel size in arcsec'
  sxaddpar,nhdr,'RMSCONT',rms,'RMS-contrast in continuum'
  sxaddpar,nhdr,'MINRES',reso.minres,'first point where power is below plateau'
  sxaddpar,nhdr,'LINRES',reso.linres,'cutting point linear power fit / plateau'
  sxaddpar,nhdr,'DEVRES',reso.devres,'resol. where power lies 10x below linear fit'
  write_fits_dlm,degcube,outfile,nhdr
                                ;write wl-vector as extension
  writefits,outfile,wlvec,/append
  
  icont=reform(max(degcube[*,0,*,*],dim=1))
  writefits,outfile,icont,/append

  
  print,'Wrote degraded cube to '+outfile
  print,'New Header:',nhdr
  
  
  
  
end
