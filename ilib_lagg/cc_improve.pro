;open cc files and write new version after performing some modifications
;(e.g. de-trending)
pro cc_improve,files,detrend=detrend
  common wgst,wgst
  common dtwg,dtwg
  common dtverbose,dtverbose
  
  dtverbose=0
                                ;check if files are existing
  fi=file_info(files)
  in=where(fi.exists)
  if in(0) eq -1 then begin
    message,/cont,'Files '+add_comma(sep=',',files)+' not found.'
    return
  endif
  file=files(in)
  nf=n_elements(file)
  
  add=''
  if keyword_set(detrend) then begin
    add=''
    if dtwg.flagsin.val eq 1 then add=add+'sin'
    if dtwg.flaglin.val eq 1 then add=add+'lin'
    if dtwg.flagoff.val eq 1 then add=add+'off'
  endif
  
  if add eq '' then begin
    message,/cont,'No action to be done for '+add_comma(sep=',',files)+ $
      '. Returning.'
    return
  endif
  
  if n_elements(wgst) ne 0 then $
    widget_control,wgst.base.id,/hourglass,sensitive=0
  format=['(i2,$)','(i3,$)','(i4,$)','(i5,$)','(i6,$)']
  for i=0,nf-1 do begin
    print
    print,'Reading file '+file(i)
    lslpos=strpos(file(i),'/',/reverse_search)
    fp=strmid(file(i),0,lslpos+1)
    fn=strmid(file(i),lslpos+1,strlen(file(i)))
    istip=check_tipmask(fn,iscc=iscc,obscore=obscore)
    if istip and iscc then begin
      fout=obscore+add+strmid(fn,strlen(obscore),strlen(fn))
    endif else begin
      message,/cont,'this routine should be applied only to TIP cc files.'+ $
        'Returning.'
      return
    endelse
    fpout=fp+fout
    
                                ;use data inout routines from mcv xtalk.pro
    dum=rfits_im(file(i),1,dd,hdr,nrhdr)
    get_lun,unit
    openr,unit,file(i)
    if(dd.bitpix eq 8) then begin
      datos=assoc(unit,bytarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
    endif else if(dd.bitpix eq 16) then begin   
      datos=assoc(unit,intarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
    endif else if(dd.bitpix eq 32) then begin   
      datos=assoc(unit,lonarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
    endif
    npos=dd.naxis3/4
    
                                ;file output
    print,'Writing file '+fpout
    fi=file_info(fpout)
    if fi.exists then begin
      print,fpout+' already exists.'
      print,'[O]verwrite, [N]ew name or [C]ancel? '
      repeat key=strupcase(get_kbrd()) until max(['O','C','N'] eq key) eq 1 
      case key of
        'O': print,'Overwriting '+fpout
        'N': begin
          reads,'Enter new filename: ',fpout
          print,'Writing to ',fpout
        end
        else: reset
      endcase        
    endif
    
    openw,/get_lun,unit_out,fpout,error=error
    writeu,unit_out,byte(hdr)   ;same header as input
    if(dd.bitpix eq 8) then begin
      dat_out=assoc(unit_out,bytarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
    endif else if(dd.bitpix eq 16) then begin   
      dat_out=assoc(unit_out,intarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
    endif else if(dd.bitpix eq 32) then begin   
      dat_out=assoc(unit_out,lonarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
    endif
    print,n2s(npos),format="('(',a,')',x,$)"
    for j=0,npos-1 do begin
      if((j+1)/10*10 eq j+1) then print,j+1,format=format(fix(alog10(j+1)))
                                ;read in slit columns
      imi=rfits_im2(datos,dd,4*j+1)
      imq=rfits_im2(datos,dd,4*j+2)
      imu=rfits_im2(datos,dd,4*j+3)
      imv=rfits_im2(datos,dd,4*j+4)
      
                                ;create profiles for correction
      print,'Processing slit pos '+string([j+1,npos],format='(i4,''/'',i4)')+$
        ' of '+strmid(file(i),strpos(file(i),'/',/reverse_search), $
                      strlen(file(i)))
      for k=0,dd.naxis2-1 do begin
        profile={i:imi(*,k),q:imq(*,k),u:imu(*,k),v:imv(*,k)}
        if keyword_set(detrend) then profile=apply_detrend(profile)
        imi(*,k)=profile.i
        imq(*,k)=profile.q
        imu(*,k)=profile.u
        imv(*,k)=profile.v
      endfor
      
                                ;again from mcv xtalk.pro: Output
      if(!version.arch eq "alpha" or $
         strmid(!version.arch,0,3) eq "x86") then begin   
        if(dd.bitpix eq 8) then begin
          dat_out(4*j)=byte(imi)
          dat_out(4*j+1)=byte(imq)
          dat_out(4*j+2)=byte(imu)
          dat_out(4*j+3)=byte(imv)
        endif else if(dd.bitpix eq 16) then begin
          dum = fix(imi)
          byteorder,dum
          dat_out(4*j)=dum
          dum = fix(imq)
          byteorder,dum
          dat_out(4*j+1)=dum
          dum = fix(imu)
          byteorder,dum
          dat_out(4*j+2)=dum
          dum = fix(imv)
          byteorder,dum
          dat_out(4*j+3)=dum
        endif else if(dd.bitpix eq 32) then begin   
          dum = long(imi)
          byteorder,dum,/lswap
          dat_out(4*j)=dum
          dum = long(imq)
          byteorder,dum,/lswap
          dat_out(4*j+1)=dum
          dum = long(imu)
          byteorder,dum,/lswap
          dat_out(4*j+2)=dum
          dum = long(imv)
          byteorder,dum,/lswap
          dat_out(4*j+3)=dum
        endif
      endif else begin
        if(dd.bitpix eq 8) then begin
          dat_out(4*j)=byte(imi)
          dat_out(4*j+1)=byte(imq)
          dat_out(4*j+2)=byte(imu)
          dat_out(4*j+3)=byte(imv)
        endif else if(dd.bitpix eq 16) then begin   
          dat_out(4*j)=fix(imi)
          dat_out(4*j+1)=fix(imq)
          dat_out(4*j+2)=fix(imu)
          dat_out(4*j+3)=fix(imv)
        endif else if(dd.bitpix eq 32) then begin   
          dat_out(4*j)=long(imi)
          dat_out(4*j+1)=long(imq)
          dat_out(4*j+2)=long(imu)
          dat_out(4*j+3)=long(imv)
        endif
      endelse
    endfor
    free_lun,unit
    free_lun,unit_out
    print,' Done.'
    
                                ;checking if ccx file of original
                                ;observation exists
    fccx=file_info(file(i)+'x')
    if fccx.exists then begin
;       tmpfile=fpout
;       xy_fits,tmpfile,/new_cont,struct=xyfst
;       write_ccx,xyfst,/force
; stop      
      print,'Copying ccx to '+fpout+'x'
      spawn,'cp '+file(i)+'x '+fpout+'x'
    endif
                                ;create cm file
    read_cm,fpout,/force_redo
  endfor
  
  print,strjoin(replicate('*',80))
  print,'Restart line_id to see the detrended data set!'
  print,'Command:'
  print,'line_id,data='''+fp+obscore+add+''''
  print,strjoin(replicate('*',80))
  
  if n_elements(wgst) ne 0 then $
    widget_control,wgst.base.id,hourglass=0,/sensitive
end
