;3d data cube viewer

pro draw3d,ps=ps,_extra=_extra
  common wg3d,wg3d
  common data3d,cube,cubest
  
  psset,ps=ps,/nowin,_extra=_extra
  if !d.name ne 'PS' then wset,fix(wg3d.draw.val)
  
  img=reform(cube(wg3d.zslider.val,*,*))*cubest.scale
  sz=size(img)
  if abs(min(cubest.zrange)-max(cubest.zrange)) le 1e-5 then $
    zrg=minmaxp(img,/nan,perc=99.9) $
  else zrg=cubest.zrange
  
  case cubest.coltab of
    'velo': begin
      userlct,neutral=0,glltab=7,center=256.*(0.-zrg(0))/(zrg(1)-zrg(0)),/full
    end
    else: userlct,/full
  endcase
  title=cubest.name
  if cubest.ztitle ne '' then begin
    title=title+', '+cubest.ztitle+': '+ $
      n2s(cubest.zvalue(wg3d.zslider.val),format='(f15.2)')+' '+cubest.zunit
  endif
  
  
  if total(abs(cubest.xrange)) le 1e-8 then cubest.xrange=[0,sz[1]]
  if total(abs(cubest.yrange)) le 1e-8 then cubest.yrange=[0,sz[2]]
  
  image_cont_al,img,zrange=zrg,/aspect,/cut,contour=0,title=title, $
                ztitle=cubest.unit,position=pos,ytitle='y [arcsec]', $
                xrange=cubest.xrange,yrange=cubest.yrange,xtitle='x [arcsec]'
   if n_elements(cubest.contour) gt 1 then begin
    cimg=smooth(cubest.contour,5,/edge)
    c_level=cubest.c_level
    nlev=n_elements(c_level)
    if nlev eq 1 and c_level(0) eq -99 then begin
      dummy=temporary(c_level)
      nlev=5
    endif
    contour,position=pos,/noerase,cimg,c_thick=2,c_labels=intarr(nlev)+1, $
            nlevels=nlev,/xst,/yst,xrange=cubest.xrange,yrange=cubest.yrange, $
            level=c_level
  endif
  xyouts,0.1,1.,charsize=1.4*!p.charsize,/normal,'!C'+cubest.title
  
  psset,/close
end

pro movie3d,file=file
  common wg3d,wg3d
  common data3d,cube,cubest
  
  tmpdir='$HOME/tmp/movietmp'
  spawn,'mkdir -p '+tmpdir+' ; rm -rf  '+tmpdir+'/*'
  files=''+tmpdir+'/3d_'+n2s(indgen(cubest.nz),format='(i4.4)')
  for i=0,cubest.nz-1 do begin
    wg3d.zslider.val=i
    widget_control,wg3d.zslider.id,set_value=i
    draw3d
    draw3d,/ps,file=files(i)+'.eps',/encaps,/jpg,/no_x,view=0,png=0, $
      size=[18.,wg3d.drawsize(1)*18./wg3d.drawsize(0)]
  endfor
  
  if n_elements(file) eq 0 then movie='3dcube' else movie=file
;  moviecmd='mencoder mf://'+tmpdir+'/3d_*.jpg -mf fps=3:type=jpg -ovc ' + $
;           'lavc -lavcopts vcodec=wmv2:vbitrate=5000 -oac copy -o '+movie
  
  moviecmd='mencoder mf://'+tmpdir+'/3d_*.jpg -mf fps=3:type=jpg -ovc x264 -x264encopts bitrate=3000 -oac copy -o '+movie+'.avi'
  

  print,'Executing Movie command:'
  print,moviecmd
  spawn,moviecmd
  print,'Movie created: ',movie 
end


pro wg3d_event,event
  common wg3d,wg3d
  
  widget_control,event.id,get_uvalue=uval
  
  case uval of
    'slider': begin
      wg3d.zslider.val=event.value
      draw3d
    end
    'image': print,'image action'
    'control': begin
      case event.value of
        'exit': begin
          widget_control,wg3d.base.id,/destroy
          retall
        end
        'print': draw3d,/ps
        'movie': movie3d
      endcase
    end
    else: message,/cont,'Unknown event: ',uval
  endcase
  
end

pro widget3d
  common wg3d,wg3d
  common data3d,cube,cubest
 
  subst={id:0l,val:0.,str:''}
  wg3d={base:subst,control:subst,zslider:subst,draw:subst,drawsize:intarr(2)}
  
  screen=get_screen_size()
  xs=fix(screen(0)*0.4)
  ys=fix(xs)
  
  wg3d.base.id=widget_base(title='3D data cube viewer',col=1)
  
  sub=widget_base(wg3d.base.id,col=2)
  if cubest.nz-1 gt 0 then $
    wg3d.zslider.id=widget_slider(sub,/vertical,ysize=ys,uvalue='slider', $
                                  min=0,max=cubest.nz-1,/drag)
  wg3d.draw.id=widget_draw(sub,xs=xs,ys=ys,uvalue='draw')
  wg3d.drawsize=[xs,ys]
  
  wg3d.control.id=cw_bgroup(wg3d.base.id,['Exit','Make Movie','Print Frame'], $
                            uvalue='control',row=1, $
                            button_uvalue=['exit','movie','print'])
  
  widget_control,wg3d.base.id,/realize
  xmanager,'wg3d',wg3d.base.id,no_block=1
  widget_control,wg3d.draw.id,get_value=val
  wg3d.draw.val=val
  draw3d
end

pro view3d,cube_in,name=name,unit=unit,scale=scale,veltab=veltab, $
           zvalue=zvalue,ztitle=ztitle,zunit=zunit,contour=contour, $
           zrange=zrange,c_level=c_level,title=title, $
           xrange=xrange,yrange=yrange
  common wg3d,wg3d
  common data3d,cube,cubest
  
  help=0
  if n_elements(cube_in) eq 0 then help=1 $
  else if (size(cube_in))(0) ne 3 then help=1
  if help then begin
    print,'Usage:'
    print,'   view3d,cube[,name=''B-Field''][,unit=''Gauss''][,scale=1.]'
    print,'   cube must be an array of size [nz,nx,ny]'
    print,'   /veltab            ;use velocity color table'
    print,'   zvalue=findgen(nz) ;vector containing height values'
    print,'   ztitle=''Height=''   ;title for height information'
    print,'   zunit=''km''         ;unit for height'
    print,'   contour=[nx,ny]    ;2D array for contour line overplot'
    retall
  endif
  
  
  if n_elements(wg3d) ne 0 then begin
    widget_control,wg3d.base.id,iconify=0,bad_id=bad_id
    if bad_id eq 0 then widget_control,wg3d.base.id,/destroy
  endif
  
;  restore,/v,'./atm_sav/5jan_data.v03.comp1.3Dlgt.sav'
;  cube_in=cube.bfiel
  
  if n_elements(scale) eq 0 then scale=1.
  if n_elements(name) eq 0 then name=''
  if n_elements(unit) eq 0 then unit=''
  
  
  cube=temporary(cube_in)
  sz=size(cube)
  if sz(0) ne 3 then message,'variable must be 3D data cube.'
  if n_elements(contour) eq 0 then contour=0
  
  if n_elements(c_level) eq 0 then c_level=0
  if n_elements(title) eq 0 then title=''
  cubest={name:name,unit:unit,scale:scale,coltab:'', $
          nx:sz(2),ny:sz(3),nz:sz(1),zvalue:findgen(sz(1)), $
          ztitle:'',zunit:'',zrange:fltarr(2),contour:contour, $
          c_level:c_level(sort(c_level)),title:title, $
          xrange:fltarr(2),yrange:fltarr(2)}
  if keyword_set(veltab) then cubest.coltab='velo'
  if n_elements(zvalue) ne 0 then cubest.zvalue=zvalue
  if n_elements(ztitle) ne 0 then cubest.ztitle=ztitle
  if n_elements(zunit) ne 0 then cubest.zunit=zunit
  if n_elements(xrange) ne 0 then cubest.xrange=xrange
  if n_elements(yrange) ne 0 then cubest.yrange=yrange
  if n_elements(zrange) ne 0 then cubest.zrange=zrange
  
  
  widget3d
end

pro l12,reread=reread,sav=sav
  common datacube,cube
  common oldsav,oldsav
  
  !p.charsize=1.4
  if n_elements(oldsav) eq 0 then reread=1 else reread=oldsav ne sav
  if n_elements(cube) eq 0 or keyword_set(reread) then begin
    if n_elements(sav) eq 0 then sav='atm_sav/imax_225_109.5.4nodes.3Dlgt.sav'
    restore,/v,sav
;    cube=temporary(cubez)
  endif
  oldsav=sav
  title=strmid((strsplit(sav,'imax_',/extract,/reg))(1),0,7)
  
  inz=indgen(60)+10
  il=indgen(n_elements(cube(0).tau))
  taurg=[-4.,0.2]
  intau=where(cube(0).tau ge min(taurg) and cube(0).tau le max(taurg))
  intau=reverse(intau)
  dummy=min(abs(cube(0).tau),lt0)
  
  xrg=[620,700]
  yrg=[570,630]
  sz=size(cube.tau(0))
  xidx=indgen(sz(1))+cube(0).x
  yidx=indgen(sz(2))+cube(0).y
  inx=minmax(where(xidx ge min(xrg) and xidx le max(xrg)))
  iny=minmax(where(yidx ge min(yrg) and yidx le max(yrg)))
  ccut=cube(inx(0):inx(1),iny(0):iny(1))
  
  contour=-ccut.bfiel(lt0)*cos(!dtor*ccut.gamma(lt0))
 
  
  if 1 eq 1 then $
  view3d,-ccut.bfiel(intau)*cos(!dtor*ccut.gamma(intau)), $
    name='B * cos(INC)',unit='[G]',zrange=[-100.,1500], $
    contour=contour,c_level=[500.,1000.,1500],title=title,$
    zvalue=reform(ccut(0,0).tau(intau)),ztitle='log(tau)',zunit=''
  
  if 1 eq 0 then $
    view3d,ccut.velos(intau), $
    name='LOS-Velocity',unit='[km/s]',/veltab,scale=1e-5,zrange=[-3.,5.], $
    contour=contour,c_level=[500.,1000.,1500],title=title,$
    zvalue=reform(ccut(0,0).tau(intau)),ztitle='log(tau)',zunit=''
  stop
end  

pro s225
  common datacube,cube
   common wg3d,wg3d
   
   frm=indgen(10)+104
;frm=frm(where(frm ne 110))
  sav='atm_sav/imax_225_'+n2s(frm,format='(i3.3)')+'.5.4nodes.3Dlgt.sav'
  nsav=n_elements(sav)
  xrg=[620,700]
  yrg=[570,630]
  
  ltau=-3.
  
  tmpdir='$HOME/tmp/movietmp'
  spawn,'mkdir -p '+tmpdir+' ; rm -rf  '+tmpdir+'/*'
  movie=['cut_bflux.wmv','cut_velos.wmv']
  for im=0,1 do begin
  for i=0,nsav-1 do begin
    restore,/v,sav(i)
    title=strmid((strsplit(sav(i),'imax_',/extract,/reg))(1),0,7)
    file=tmpdir+'/cut_'+title
    sz=size(cube.tau(0))
    xidx=indgen(sz(1))+cube(0).x
    yidx=indgen(sz(2))+cube(0).y
    inx=minmax(where(xidx ge min(xrg) and xidx le max(xrg)))
    iny=minmax(where(yidx ge min(yrg) and yidx le max(yrg)))
    ccut=cube(inx(0):inx(1),iny(0):iny(1))
    dummy=min(abs(ccut(0).tau-ltau),il)
    dummy=min(abs(cube(0).tau),lt0)
    
    contour=total(-ccut.bfiel(lt0-3:lt0+3)* $
                  cos(!dtor*ccut.gamma(lt0-3:lt0+3)),1)/7
    if im eq 0 then begin
      data=reform(total(-ccut.bfiel(lt0-3:lt0+3)* $
                        cos(!dtor*ccut.gamma(lt0-3:lt0+3)),1)/7, $
                  1,inx(1)-inx(0)+1,iny(1)-iny(0)+1)
      view3d,data, $
        name='B * cos(INC)',unit='[G]',zrange=[-100.,1500], $
        contour=contour,c_level=[500.,1000.,1500],title=title,$
        zvalue=reform(ccut(0,0).tau(lt0)),ztitle='log(tau)',zunit=''
    endif else begin
      data=reform(total(ccut.velos(il-3:il+3),1)/7, $
                  n_elements(il),inx(1)-inx(0)+1,iny(1)-iny(0)+1)
      view3d,data, $
        name='LOS-Velocity',unit='[km/s]',/veltab,scale=1e-5,zrange=[-3.,5.], $
        contour=contour,c_level=[500.,1000.,1500],title=title,$
        zvalue=reform(ccut(0,0).tau(il)),ztitle='log(tau)',zunit=''
    endelse
      draw3d,/ps,file=file+'.eps',/encaps,/jpg,/no_x,view=0,png=0, $
        size=[18.,wg3d.drawsize(1)*18./wg3d.drawsize(0)]
  endfor
  moviecmd='mencoder mf://'+tmpdir+'/cut_*.jpg -mf fps=1:type=jpg -ovc ' + $
    'lavc -lavcopts vcodec=wmv2:vbitrate=5000 -oac copy -o '+movie(im)
 
  print,'Executing Movie command:'
  print,moviecmd
  spawn,moviecmd
  print,'Movie created: ',movie(im)
endfor
  
end

pro example
  common datacube,cube
  
  if n_elements(cube) eq 0 then begin
    restore,/v,'atm_sav/5jan_data.v03.comp1.3Dzkm.sav'
    cube=temporary(cubez)
  endif
  
  inz=indgen(60)+10
  view3d,cube.velos(inz),name='LOS-Velocity',unit='[km/s]',/veltab,scale=1e-5, $
    zvalue=reform(cube(0,0).zkm(inz)),ztitle='Height',zunit='km'

end


pro example_maptau
  common data,data,header
  common datacube,cube
  
  if n_elements(data) eq 0 then data=read_fits_dlm('/home/lagg/data/Hinode/30nov06/2D_merged/inverted_atmos_maptau.1.fits',0,header)
  
  cube=reform(data[*,0:899,*,7])            ;VLOS
  cube=transpose(cube,[2,0,1])
  sz=size(cube)
  lttop=sxpar(header,'LTTOP')
  ltinc=sxpar(header,'LTINC')
  ltvec=findgen(sz[1])*ltinc+lttop
  ltrg=[-3.2,+0.4]
  inlt=reverse(where(ltvec ge min(ltrg) and ltvec le max(ltrg)))
  cube=cube[inlt,*,*]
  
  
  view3d,cube,name='LOS-Velocity',unit='[km/s]',/veltab,scale=1e-5, $
         ztitle='log(tau)',zunit='',xrange=[0,sz[2]]*.08,yrange=[0,sz[3]]*.08,$
         zvalue=ltvec[inlt]
  movie3d,file='movie_vlos'
  
  
  cube=reform(data[*,0:899,*,1])            ;TEMPE
  cube=transpose(cube,[2,0,1])
  cube=cube[inlt,*,*]
  view3d,cube,name='Temperature',unit='[K]',scale=1., $
         ztitle='log(tau)',zunit='',xrange=[0,sz[2]]*.08,yrange=[0,sz[3]]*.08,$
         zvalue=ltvec[inlt]
  movie3d,file='movie_temp'
 
  cube=reform(data[*,0:899,*,8])            ;MAG
  cube=transpose(cube,[2,0,1])
  cube=cube[inlt,*,*]
  view3d,cube,name='B-Field',unit='[G]',scale=1., $
         ztitle='log(tau)',zunit='',xrange=[0,sz[2]]*.08,yrange=[0,sz[3]]*.08,$
         zvalue=ltvec[inlt],zrange=[0,3500]
  movie3d,file='movie_mag'
 
   cube=reform(data[*,0:899,*,9])            ;INC
  cube=transpose(cube,[2,0,1])
  cube=cube[inlt,*,*]
  view3d,cube,name='B-Inclination',unit='[deg]',scale=1., $
         ztitle='log(tau)',zunit='',xrange=[0,sz[2]]*.08,yrange=[0,sz[3]]*.08,$
         zvalue=ltvec[inlt],zrange=[0,180]
  movie3d,file='movie_inc'
  
  cut=reform(data[*,625,inlt,8]) 
  psset,/ps,file='cut_mag.eps',/encapsulated,size=[20,8],/no_x,jpg=0
  userlct,/full
  @greeklett.pro
  image_cont_al,cut,contour=0,xrange=[0,sz[2]]*.08, $
                yrange=[ltvec[inlt[0]],ltvec[inlt[n_elements(inlt)-1]]], $
                zrange=[0,3500],title='B-Field', $
                xtitle='x [arcsec]',ytitle='log('+f_tau+')'
  psset,/close
 
end
