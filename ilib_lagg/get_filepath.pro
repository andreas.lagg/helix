;function to retrieve filename and path 
function get_filepath,fname
  
  nf=n_elements(fname)
  f=replicate({path:'',name:'',ext:'',root:'',base:''},nf)
  for i=0,nf-1 do begin
    slpos=strpos(fname[i],'/',/reverse_search)
    f[i].path=strmid(fname[i],0,slpos(0)+1)
    f[i].name=strmid(fname[i],slpos(0)+1,strlen(fname[i]))
    dpos=strpos(fname[i],'.',/reverse_search)
    f[i].root=strmid(fname[i],0,dpos(0))
    f[i].ext=strmid(fname[i],dpos(0)+1,strlen(fname[i]))
    dpos=strpos(f[i].name,'.',/reverse_search)
    f[i].base=strmid(f[i].name,0,dpos(0))
  endfor
  return,f
  
end
