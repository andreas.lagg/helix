;The IAU standard for conversion from air to vacuum wavelengths is
;given in Morton (1991, ApJS, 77, 119). For vacuum wavelengths (VAC)
;in Angstroms, convert to air wavelength (AIR) via:
;see http://www.sdss.org/dr4/products/spectra/vacwavelength.html
function vac2air,wl,reverse=reverse
  
  
  a2v= 1. / (1.0 + 2.735182E-4 + 131.4182 / wl^2 + 2.76249E8 / wl^4) 
  
  if keyword_set(reverse) then retwl=wl / a2v $
  else retwl=wl * a2v
  
  return,retwl
end
