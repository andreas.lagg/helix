;scales data using histogram.
;returns max and minvalues where all data are within the perc given by perc.
function histo_scale,data,perc=perc
  
  if n_elements(perc) eq 0 then perc=10.
  
  nel=n_elements(data)
  mm=minmaxp(data)
  nbin=200<(nel)
  hh=histogram(float(data),nbins=nbin,/nan,loc=x)
;  x=findgen(nbin)/(nbin)*(mm(1)-mm(0))+mm(0)
  
  nh=n_elements(hh)
  minval=0
  maxval=nbin-1
  for i=0,nh-1 do begin
    bot=total(hh(i:nh-1))/float(nel)
    top=total(hh(0:i))/float(nel)
    if bot ge (1.-perc/100.) then minval=i
    if top ge perc/100. then maxval=i
  endfor
  
;  plot,x,hh & oplot,[0,0]+x[minval],!y.crange & oplot,[0,0]+x[maxval],!y.crange & stop
  
  return,[x(minval),x(maxval)]
end
