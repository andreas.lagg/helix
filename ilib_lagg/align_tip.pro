pro align_tip,tip=tip,mdi=mdi,sum=sum,cwl=cwl,full=full, $
              magnetogram=magnetogram,continuum=continuum
  
;   if n_elements(tip) eq 0 then tip='/media/LACIE/VTT-May05/19May05/19may05.002-0?cc'
;   if n_elements(tip) eq 0 then
;   tip='/media/LACIE/VTT-May05/18May05/18may05.016-0?cc'
    if n_elements(tip) eq 0 then tip='/scratch/irpolar/data_aug03/21aug03/21aug03.022cc' 
;   if n_elements(mdi) eq 0 then mdi='/media/LACIE/MDI-May05/fd_M_01h.108513/0005.fits'
;   if n_elements(mdi) eq 0 then mdi='/media/LACIE/MDI-May05/fd_M_01h.108494/0050.fits'
   if n_elements(mdi) eq 0 then mdi='/scratch/bloomfield/MDI-Aug03/fd_M_01h.93233/fd_M_01h.93233.0058.fits'
;  if n_elements(sum) eq 0 then sum='/media/LACIE/SUMER-May05/list_ct_050519_data_1557.5211.sav'
;   if n_elements(tip) eq 0 then tip='/media/LACIE/VTT-May05/19May05/19may05.007-0?cc'
;   if n_elements(mdi) eq 0 then mdi='/media/LACIE/MDI-May05/fd_M_01h.108514/0030.fits'
;   if n_elements(tip) eq 0 then tip='/media/LACIE/VTT-May05/19May05/19may05.009-0?cc'
;   if n_elements(mdi) eq 0 then mdi='/media/LACIE/MDI-May05/fd_M_01h.108515/0040.fits'
   
   if keyword_set(continuum) eq 0 then magnetogram=1
   
                                ;get TIP Image
   img_solar,ps=0,box=box,tip=tip,/soho,silicon=n_elements(cwl) eq 0, $
     magnetogram=magnetogram,continuum=continuum,cwl=cwl, $
     imgtip=tipimg,ret_tipst=tipst
   
   
                                ;add 200%
   if keyword_set(full) eq 0 then begin
     add=2
     sohobox=[box(0)-add*(box(2)-box(0)),box(1)-add*(box(3)-box(1)), $
              box(2)+add*(box(2)-box(0)),box(3)+add*(box(3)-box(1))]
   endif else sohobox=[-1000,-1000,1000,1000]
     
   
   if n_elements(mdi) ne 0 then begin
;     img_solar,ps=0,box=sohobox,mdi=mdi,/soho,imgmdi=sohoimg,ret_mdist=sohost
     sohost=c2solar_mdist(mdi,img=sohoimg,/soho)
     mod_mdi=1 & mod_sum=0
     modstr='mdi'
   endif
   if n_elements(sum) ne 0 then begin
;     img_solar,ps=0,box=sohobox,sum=sum,/soho,imgsum=sohoimg,ret_sumst=sohost
     sohost=c2solar_sumst(sum,img=sohoimg,/soho)
     mod_mdi=0 & mod_sum=1
     modstr='sum'
   endif
   sohobox(0)=sohobox(0)>min(sohost.xstd)
   sohobox(1)=sohobox(1)>min(sohost.ystd)
   sohobox(2)=sohobox(2)<max(sohost.xstd)
   sohobox(3)=sohobox(3)<max(sohost.ystd)
   xvec=(sohost.xstd(*,0))(*)
   yvec=(sohost.ystd(0,*))(*)
   inx=where(xvec ge sohobox(0) and xvec le sohobox(2))
   iny=where(yvec ge sohobox(1) and yvec le sohobox(3))
   if inx(0) eq -1 or iny(0) eq -1 then message,'no SOHO data in TIP FOV.'
   xs=[min(inx),max(inx)]
   ys=[min(iny),max(iny)]
   sohoimg=sohoimg(xs(0):xs(1),ys(0):ys(1))
   
   
   xycut=cut_box(sohost,box=sohobox,/arcsec)
;    if n_elements(mdi) ne 0 then $
;      img_solar,ps=0,box=sohobox,mdi=mdi,/soho,imgmdi=sohoimg,ret_mdist=sohost
;    if n_elements(sum) ne 0 then $
;      img_solar,ps=0,box=sohobox,sum=sum,/soho,imgsum=sohoimg,ret_sumst=sohost
   
   nv=where(sohoimg lt -10000)
   if nv(0) ne -1 then sohoimg(nv)=0
   
                                ;get tip image in TIP coordinates
   magtip=get_tipimg(tip,cwl=cwl,/silicon, $
                     magnetogram=magnetogram,continuum=continuum)
;   tipimg=reverse(magtip,2)
   tipimg=magtip
   sztip=size(tipimg)

   
                                 ;remove NAN from tip image
  nofin=where(finite(tipimg) eq 0)
  if nofin(0) ne -1 then tipimg(nofin)=min(tipimg,/nan)
                                 ;remove NAN from soho image
  nofin=where(finite(sohoimg) eq 0)
  if nofin(0) ne -1 then sohoimg(nofin)=min(sohoimg,/nan)
  
  
  
;scale image to correct solar x/y
                                ;aspect ratio with resolution 0.5 pix
                                ;per arcsec
   reso=.5
   tnx=fix((box(2)-box(0))/reso)
   tny=fix((box(3)-box(1))/reso)
   mnx=fix((sohobox(2)-sohobox(0))/reso)
   mny=fix((sohobox(3)-sohobox(1))/reso)
;    screen=get_screen_size()
;    mny=screen(1)*0.9-tny
;    res=((sohobox(3)-sohobox(1))/mny)
;    mnx=fix((sohobox(2)-sohobox(0))/res) 
   
   sohoimg_scl=congrid(sohoimg,mnx,mny,/interp)
   
   window,0,xsize=max([tnx,mnx]),ysize=tny+mny
   loadct,0
   
   flip=1
   mirror=0
   absval=mod_sum eq 1
   ok=0
   print,'Flipping the TIP image is necessary, at least for the data of May 2005. No check was done so far why this is so.'
   print,'Using the Abs-Value of the TIP magnetigram is helpful for alignment with SUMER continuum image.'
   print,'Press ''F'' to flip image, ''M'' to mirror image, ''A'' to use ABS(img),' + $
     ' ''C'' to continue'
   repeat begin                 ;flip tip image? abs of image
     erase
     print,'Flip=',flip,', Mirror=',mirror,', Abs=',absval
     if flip eq 1 then tipimg=reverse(magtip,2) else tipimg=magtip
     if mirror eq 1 then tipimg=reverse(tipimg,1)
     if absval then tipimg=abs(tipimg)
     tipimg_scl=congrid(tipimg,tnx,tny,/interp)
     tvscl,sohoimg_scl
     tvscl,tipimg_scl,0,mny,/device
     gk=strupcase(get_kbrd())
     if gk eq 'F' then flip=~flip          ;NOT operator
     if gk eq 'M' then mirror=~mirror          ;NOT operator
     if gk eq 'A' then absval=~absval          ;NOT operator
     if gk eq 'C' then ok=1
   endrep until ok
   
   
                                ;select region from soho image
   ok=0
   repeat begin
     erase
     tvscl,sohoimg_scl
     tvscl,tipimg_scl,0,mny,/device
     userlct
     plots,color=1,[0,max([tnx,mnx])],[mny,mny]
     print,'Click on Lower Left boundary'
     cursor,llx,lly,/down,/device
     lly=lly<(mny-1)
     print,'Click on Upper Right boundary'
     cursor,urx,ury,/down,/device
     ury=ury<(mny-1)
     plots,color=1,[llx,llx,urx,urx,llx],[lly,ury,ury,lly,lly],/device
     print,'Box: x=[',llx,lly,'], y=[',urx,ury,']'
     print,'Happy with selection?'
     gk=get_kbrd()
     if strupcase(gk) eq 'Y' then ok=1
   endrep until ok
   sohoimg_scl=sohoimg_scl(llx:urx,lly:ury)
   
   loadct,0
  
  trans_img=sohoimg_scl
  ref_img=tipimg_scl
  
                                ;remove NAN from tip image
  nofin=where(finite(trans_img) eq 0)
  if nofin(0) ne -1 then trans_img(nofin)=min(trans_img,/nan)

;  pp = setpts_roi(trans_img,ref_img)
  setpts,pp,trans_img,ref_img,plotonly=plotonly, $
    noscale=noscale,append=append,trima=trima,key=key
 
  tt = caltrans(pp)
  pin = tt[*,0]
  qin = tt[*,1]
  
  img_shift=aalign_images(trans_img,ref_img,pin,qin,pout,qout, $
                          trans=trans,missing=!values.f_nan,/amoeba)
  
                                ;create x and y vectors of TIP image
                                ;use shifted/rotated soho coordinates
  xx=[llx,urx]/float(mnx)*(xycut(2)-xycut(0))+xycut(0)
  yy=[lly,ury]/float(mny)*(xycut(3)-xycut(1))+xycut(1)
  xsoho=congrid(sohost.xstd(xx(0):xx(1),yy(0):yy(1)),urx-llx+1,ury-lly+1,/interp)
  ysoho=congrid(sohost.ystd(xx(0):xx(1),yy(0):yy(1)),urx-llx+1,ury-lly+1,/interp)
  xsoho_tr=poly_2d(xsoho,pout,qout,2,tnx,tny,cubic=-0.5,missing=!values.f_nan)
  ysoho_tr=poly_2d(ysoho,pout,qout,2,tnx,tny,cubic=-0.5,missing=!values.f_nan)
                                ;extrapolate missing NAN pixels
  xsoho_ep=expol(xsoho_tr)
  ysoho_ep=expol(ysoho_tr)
                                ;convert to original scale
  xtip=congrid(xsoho_ep,sztip(1),sztip(2),/interp)
  ytip=congrid(ysoho_ep,sztip(1),sztip(2),/interp)

  
  userlct,/full
  slpos=where(byte(tip) eq (byte('/'))(0),nsl)
  if nsl ge 1 then tiproot=strmid(tip,slpos(nsl-1)+1,strlen(tip)) $
  else tiproot=tip
  if mod_mdi eq 1 then begin
    slpos=where(byte(mdi) eq (byte('/'))(0),nsl)
    if nsl gt 1 then froot=strmid(mdi,slpos(nsl-2),strlen(mdi)) $
    else froot=mdi
  endif else if mod_sum eq 1 then begin
    slpos=where(byte(sum) eq (byte('/'))(0),nsl)
    if nsl gt 1 then froot=strmid(sum,slpos(nsl-1),strlen(sum)) $
    else froot=mdi
  endif
  
  
  
  image_cont_al,tipimg,xtip,ytip,zrange=[min(tipimg,/nan),max(tipimg,/nan)],xtitle='x [arcsec]',ytitle='y [arcsec]',title=tiproot+', aligned with '+froot,cont=0,/aspect
  
  
                                ;blink images to test alignment
  xs=800.
  window,0,xsize=xs,ysize=float(tny)/tnx*xs
  noval=where(finite(img_shift) eq 0) 
  if noval(0) ne -1 then img_shift(noval)=min(img_shift,/nan)
  print,'Write out new Coordinates? (Y/n)'
  repeat  begin
    tvscl,congrid(img_shift,512,float(tny)/tnx*xs,/interp)
    wait,.5
    tvscl,congrid(tipimg_scl,512,float(tny)/tnx*xs,/interp)
    wait,.5
    gk=strupcase(get_kbrd(0)) 
  endrep until gk ne ''
  
                                ;do flipping and mirroring
  if flip then begin
     xtip=reverse(xtip,2)
     ytip=reverse(ytip,2)
  endif
  if mirror then begin
     xtip=reverse(xtip,1)
     ytip=reverse(ytip,1)
  endif
                                
  
                                ;store coordinates to sav-file
  if gk ne 'N' then begin
    tipfile=(file_search(tip))(0)
    tfroot=strmid(tipfile,strpos(tipfile,'/',/reverse_search)+1,11)
    sav='./sav/align_'+modstr+'_'+tfroot+'.sav'
    if mod_mdi then soho_tref=sohost.header.tref $
    else if mod_sum then soho_tref=sohost.time
    fi=file_info(sav)
    if fi.exists eq 1 then begin
      print,sav+' already exists. Overwrite coordinate file? (Y/n)'
      repeat  gk=strupcase(get_kbrd()) until gk ne '' 
    endif
    if gk ne 'N' then begin
      save,/xdr,/compress,file=sav,xtip,ytip,soho_tref
      print,'Wrote new TIP coordinates to '+sav
      
  print
  print,'The new coordinates are stored in the variables xtip and ytip.'
  print,'These variables contain the x and y value of every pixel in the' + $
    ' TIP image. '
  print
  print,'To plot a TIP image you can use the following commands:'
  print
  print,';get TIP image:'
  print,'    tipimg=get_tipimg('''+tip+''',/silicon,/magnetogram)'
;  print,'    tipimg=reverse(tipimg,2)'
  print
  print,';restore TIP coordinates in MDI / SUMER system:'
  print,'    restore,/v,'''+sav+''''
  print
  print,';plot image:'
  print,'    image_cont_al,tipimg,xtip,ytip,xtitle=''x [arcsec]'',ytitle=''y [arcsec]'',zrange=[min(tipimg,/nan),max(tipimg,/nan)],title='''+tiproot+', aligned with '+froot+''',cont=0,/aspect'
  print
  print,';Or use tv_tipall.pro:'
  print,'    tv_tipall,'''+tip+''',coord_sav='''+sav+''''
  print
    endif
  endif
  
end
