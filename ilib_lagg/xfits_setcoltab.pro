pro xfits_setcoltab,pi,zrange,reverse=reverse
  
  if n_tags(pi) ge 2 then begin
    colt=pi.colortable
    rev=pi.colortablerev
  endif else begin
    colt=pi
    rev=keyword_set(reverse)
  endelse
  
  case strlowcase(colt) of
    'velo': userlct,neutral=0,glltab=7,/full,verbose=0, $
                    reverse=rev, $
                    center=256.*(0.-zrange(0)) / (zrange(1)-zrange(0))
    'azi': begin
      userlct,/nologo,glltab=8,/full,verbose=0,reverse=rev
    end
    'stokes': userlct,/nologo,coltab=0,/full,verbose=0, $
                      reverse=rev eq 0
    else: userlct,/full,verbose=0,reverse=rev
  end  
end
