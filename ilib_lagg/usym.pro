;+
; NAME: MKUSYM
;
; PURPOSE: Create user defined symbols using USERSYM
;
; CATEGORY: plotting (auxiliary)
;
; CALLING SEQUENCE:
;   mkusym,type,fill=fillbit,color=colori,thick=thickn
;
; INPUTS: type  STRING allowed values:
;         'CIRCLE','TRIANGLE','SQUARE','DIAMOND','MPAE'
;
; KEYWORD PARAMETERS:
;       fill=fillbit,color=colori,thick=thickn
;        as in USERSYM
; OUTPUTS: via definition of !P.psym=8
;
; MODIFICATION HISTORY:
; 	Written by: M.Fraenz,N.Krupp jun 93
;
;-
PRO usym,type,fill=fillbit,color=colori,thick=thickn,charsize=charsize
  IF NOT(keyword_set(charsize)) THEN charsize=!p.charsize
  IF NOT(keyword_set(fillbit)) THEN fillbit=0
IF NOT(keyword_set(colori)) THEN colori=!p.color
IF NOT(keyword_set(thickn)) THEN thickn=1.0
type=strupcase(type)
CASE type OF
 'VLINE': BEGIN
            xarr=[0,0]
            yarr=[-1,1]*charsize
           END
 'CIRCLE': BEGIN
            A=findgen(37)*(!DPI*2./36.)
            XARR=COS(A)*charsize
            YARR=SIN(A)*charsize
           END
 'TRIANGLE': BEGIN
            x=sqrt(3.0d)/2.
            xarr=[-x,x,0.0d,-x]*1.6*charsize
            yarr=[-0.5d,-0.5d,1.0d,-0.5d]*1.6*charsize
;            xarr=[-1,1,0.0d,-1]
;            yarr=[-1d,-1d,1.0d,-1d]
           END
 'SQUARE': BEGIN
            xarr=double([-1,1,1,-1,-1])*charsize
            yarr=double([-1,-1,1,1,-1])*charsize
           END
 'DIAMOND': BEGIN
            xarr=double([0.,1.,0.,-1.,0.])*1.2*charsize
            yarr=double([-1.,0.,1.,0.,-1.])*1.2*charsize
           END
  'DOT': BEGIN
            xarr=double([-1,1,1,-1,-1])*.3*charsize
            yarr=double([-1,-1,1,1,-1])*.3*charsize
          END
  'VBAR': BEGIN
            xarr=double([-1,1,1,-1,-1])*0.25*charsize
            yarr=double([-1,-1,1,1,-1])*charsize
           END
  'MPAE'  : BEGIN
             pxarr=double([0.0500,0.0500,0.7000,0.45,0.2000,0.2000,0.3500, $
                           0.440,0.2000,0.2000])
             pyarr=double([0.0304,0.7000,0.7000,0.1420,0.1420,0.3220,0.3220, $
                           0.5020,0.5020,0.0304])
             mxarr=double([-0.05,-0.05,-0.2,-0.325,-0.45,-0.7,-0.45,-0.25, $
                           -0.40,-0.3,-0.2,-0.2,-0.05])
             myarr=double([0.03,0.7,0.7,0.55,0.7,0.7,0.03,0.03, $
                          0.35,0.25,0.35,0.03,0.03])
             axarr=double([-0.05,-0.2,-0.2,-0.49,-0.55,-0.7,-0.45,-0.05, $
                          -0.2,-0.32,-0.4,-0.2,-0.2,-0.05])
             ayarr=double([-0.7,-0.7,-0.55,-0.55,-0.7,-0.7,-0.03,-0.03, $
                          -0.2,-0.2,-0.38,-0.38,-0.2,-0.03])
             exarr=double([0.4,0.48,0.2,0.2,0.52,0.6,0.2,0.2,0.64,0.7,0.05])
             eyarr=double([-0.03,-0.19,-0.19,-0.29,-0.29,-0.45,-0.45,-0.55,-0.55,-0.7,-0.7])
;
             xarr=[pxarr,mxarr,axarr,exarr]*charsize
             yarr=[pyarr,myarr,ayarr,eyarr]*charsize
           END
  ELSE : message,'sorry, symbol not yet defined:'+type
ENDCASE
USERSYM,XARR,YARR,fill=fillbit,color=colori,thick=thickn
return
end
