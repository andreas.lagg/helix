function read_atmos,file
  
  openr,unit,/get_lun,file,error=err
  if err ne 0 then message,'Culd not read Atmosphere: '+file
  
  i=0
  repeat begin
    a='' & readf,unit,a
    if i ge 1 then begin
      tmp=(strsplit(a,/extract))
      if i eq 1 then begin
        n=n_elements(tmp)
        dat=fltarr(n)  
      endif
      if n_elements(tmp) eq n then $
        dat=[[dat],[float(tmp)]]
    endif
    i=i+1
  endrep until eof(unit)
  free_lun,unit
  dat=dat[*,1:*]
  par=['LOGTAU','Z','TEMP','P_GAS','P_ELEC','ABS_COEFF','DENSITY', $
       'B','VTURB','VELOC','FMAG','GAMMA','AZIMU']  
  unit=['','CM','K','CGS','CGS','CM^2/GM','GM/CM^3','G', $
        'CM/S','CM/S','G','DEG','DEG']
  
  atm={dat:dat,unit:unit[0:n-1],par:par[0:n-1]}
  
  return,atm
end
