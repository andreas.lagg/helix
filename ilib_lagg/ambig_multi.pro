;perform multiple calls to ambig with different random seeds to get
;best result
pro ambig_multi
  
  nseed=20
  for iseed=1,nseed do begin
    istr=string(fix(iseed),format='(i3.3)')
                                ;first call ambig_prepare
    ambig_prepare,'/home/lagg/data/Hinode/30nov06/2D_double/inverted_atmos.fits',pixx=0.08,pixy=0.08,date_obs='2006-11-30T23:30:00',xcen=-190.,ycen=-170.,iseed=iseed,/write_par
    
                                ;store original magfield-data
    spawn,'rm -rf noflip ; mkdir -p noflip ; cp -p magfield_ltau*.fits noflip/'
    
    
                                ;run ambig for all logtau layers
    pf=file_search('par_ltau*',count=cnt)
    unit=strarr(cnt)
    for i=0,cnt-1 do begin
      spawn,'cp -p '+pf[i]+' par ; LD_LIBRARY_PATH=/opt/intel/composerxe/mkl/lib/intel64/:/opt/intel/composer_xe_2013.3.163/compiler/lib/intel64:$LD_LIBRARY_PATH ; /home/lagg/work/polarimeter/aziambig/ambig ',unit=un
      unit[i]=un
      wait,3
    endfor
    
                                ;since processes are spawned (using
                                ;the unit keyword) we have to check
                                ;when its finished:
    allend=bytarr(cnt)
    repeat begin
      for i=0,cnt-1 do if allend[i] eq 0 then begin
        a=''  & readf,unit[i],a
        print,unit[i],a
        if eof(unit[i]) eq 1 then begin
          free_lun,unit[i]
          allend[i]=1b
        endif
      endif
      wait,.1
    endrep until min(allend)
                                ;move results
    spawn,'rm -rf '+istr+' ; mkdir -p '+istr+' ; mv magfield_ltau*.fits '+istr
    
  endfor
  
  ambig_merge
  stop
  
end
