pro phi_jpeg_test,j2000=j2000
  
  fatmos='~/data/Hinode/30nov06/2D_double/inverted_atmos_ambig_maptau.1.fits'
  data=read_fits_dlm(fatmos,0,hdr)         
  szd=size(data,/dim)
  
  ipar=sxpar(hdr,'TMP')-1
;  ipar=sxpar(hdr,'FMAG')-1
  lttop=sxpar(hdr,'LTTOP')
  ltbot=sxpar(hdr,'LTBOT')
  ltinc=sxpar(hdr,'LTINC')
  tau=findgen(szd[2])*ltinc+lttop
;  stop
  
  
  dummy=min(abs(tau),i0)
  
  img=reform(data[*,*,i0,ipar])
;  img=img[300:501,300:501]
;  r=[60,100,60,100]
  r=[350,400,500,600]
  sz=size(img,/dim)
  szuc=sz[0]*sz[1]
  
  
  mm=minmax(img)
  img=((img-mm[0])/(mm[1]-mm[0]) * 253.+1)
  fgif='~/tmp/phi.gif'
  write_gif,fgif,byte(img)
  fg=file_info(fgif)
  
  j2000=keyword_set(j2000)
  
  psset,ps=1,size=[28,18],file='~/tmp/phi_jpeg_test.ps',/no_x,/landscape,pdf=1
  !p.multi=[0,1,1]
  !p.charsize=.8
  
  zrg=minmaxp(img,perc=99.9)
  if j2000 eq 0 then begin
     qualarr=100-findgen(41)*2
     qualarr=100.-[0,1,2,4,8,16,32,64]
  endif else begin
     qualarr=findgen(16)
  endelse
  for iqual=0,n_elements(qualarr)-1 do begin
    if iqual ne 0 then erase
    quality=qualarr[iqual]
    jpg='~/tmp/phi.jpg'
;    nimg=reform(img,1,sz[0]*sz[1])
    nimg=img
    if j2000 eq 0 then begin
      qualstr='Quality: '+string(fix(quality),format='(i3)')
      write_jpeg,jpg,nimg,quality=quality
      read_jpeg,jpg,nimgjpg,/gray
    endif else begin
      qualstr='Levels: '+n2s(fix(quality),format='(i3)')
      write_jpeg2000,jpg,nimg,n_levels=quality
      nimgjpg=read_jpeg2000(jpg)
    endelse
    fi=file_info(jpg)
    imgjpg=reform(nimgjpg,sz[0],sz[1])
    userlct,/full,coltab=0,/reverse
;    if quality eq 100 then size100=fi.size
;    fact=float(size100)/fi.size
    fact=float(szuc)/fi.size
    imgscl=(imgjpg-1)/253.*(mm[1]-mm[0])+mm[0]
    image_cont_al,/cut,/aspect,contour=0,imgscl,title=qualstr+ $
                  ' Comp-Fact: '+string(fact,format='(f5.1)'), $
                  ztitle='counts',pos=[.02,.55,.22,.95],/noerase
    plots,/data,r([0,0,1,1,0]),r([2,3,3,2,2])
    diff=(img-imgjpg)/253.*(mm[1]-mm[0])
    zrd=minmaxp(diff,perc=99.0)
    image_cont_al,/cut,/aspect,contour=0,diff,zrange=max(abs(zrd))*[-1,1], $
                  title='Difference to loss-less',ztitle='counts', $
                  pos=[.02,.05,.22,.5],/noerase
    image_cont_al,/cut,/aspect,contour=0,imgscl[r[0]:r[1],r[2]:r[3]], $
                  title=qualstr+ $
                  ' Comp-Fact: '+string(fact,format='(f5.1)'),ztitle='counts',$
                  pos=[.25,.05,.60,.95],/noerase
    image_cont_al,/cut,/aspect,contour=0,diff[r[0]:r[1],r[2]:r[3]], $
                  zrange=max(abs(zrd))*[-1,1],ztitle='counts', $
                  title='Difference to loss-less',$
                  pos=[.63,.05,98,.95],/noerase
    mmm=mean(mm)
    mom=moment(diff/mmm)
;    mom=mom/mmm
;    DEVICE, SET_FONT='Helvetica Fixed',/TT_FONT,SET_CHARACTER_SIZE=[70,90] 
    xyouts,0.98,0.53,charsize=1.2,/normal,orientation=90, $
           'mean difference = '+string(mom[0],format='(e10.2)')+ $
           '!Cstd.dev       = '+string(sqrt(mom[1]),format='(e10.2)')+ $
           '!Cskewness      = '+string(mom[2],format='(e10.2)')
    xyouts,0.00,0.53,/normal,charsize=1, $
           'Uncompressed (8 bit) = '+string(szuc/1.e3,format='(i12)')+' kB!C'+$
           ' GIF size = '+string(fg.size/1.e3,format='(i12)')+ $
           ' kB (x '+n2s(float(szuc)/fg.size,format='(f8.2)')+')!C'+$
           ' JPG size = '+string(fi.size/1.e3,format='(i12)')+ $
           ' kB (x '+n2s(float(szuc)/fi.size,format='(f8.2)')+')!C'+$
           '!3'
  endfor
  psset,/close
end
