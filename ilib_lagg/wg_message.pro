;displays widget with a message, add user defined buttons
;(similar to dialog_message)

pro xwgm_event,event
  common wgm_comm,button_uval,wgm,retval
  
  widget_control,event.id,get_uvalue=uval
  case uval of
    'control': begin
      retval=button_uval(event.value)
      widget_control,wgm.base.id,/destroy
    end
  endcase
end

function wg_message,message,button=button,cancel=cancel,ok=ok,yes=yes,no=no, $
               title=title,_extra=_extra
  common wgm_comm,button_uval,wgm,retval
  
  retval=''
  subst={id:0l,val:0.,str:''}
  wgm={base:subst,control:subst}
  if n_elements(title) eq 0 then title='Message'
  if n_elements(button) eq 0 then button=''
  if keyword_set(ok) then button=[button,' OK ']
  if keyword_set(cancel) then button=[button,'Cancel']
  if keyword_set(yes) then button=[button,' Yes ']
  if keyword_set(no) then button=[button,' No ']
  ii=where(button ne '')
  if ii(0) ne -1 then button=button(ii) else button='  OK  '
  
  wgm.base.id=widget_base(title=title,col=1,_extra=_extra)
  
  for i=0,n_elements(message)-1 do $
    text=widget_label(wgm.base.id,value=message(i))
  
  button_uval=strcompress(strlowcase(button),/remove_all)
  wgm.control.id=cw_bgroup(wgm.base.id,button, $
                            uvalue='control',row=1)
  
  widget_control,/realize,wgm.base.id
  xmanager,'xwgm',wgm.base.id
  return,retval
end
