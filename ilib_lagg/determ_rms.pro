;routine to determine rms
function determ_rms,prof,show=show,status=status
  
  status=0
  show=keyword_set(show)
  show=0
  
  if show then userlct,/nologo
  if show then plot,prof
  
                                ;check if enough WL points for noise determination are present
  if n_elements(prof) le 11 then begin
    status=1
    return,0.0
  endif
  
                                ;remove signal
  mprof=median(prof,11)
 if show then  oplot,color=1,mprof
  
                                ;take only region around 10 lowest
                                ;points
  np=n_elements(mprof)
  low=(sort(abs(mprof)))(0:10<(np-1))
  add=np/10.
  i0=(low-add)>0
  i1=(low+add)<(np-1)
  iuse=bytarr(np)
  for i=0,n_elements(low)-1 do iuse(i0(i):i1(i))=1b
  idx=where(iuse)
  
  if show then oplot,psym=4,color=1,iuse

  
  resprof=prof-mprof
  if show then  oplot,color=2,resprof
  
  result = MOMENT(resprof(idx),sdev=sdev)  
;PRINT, 'Mean: ', result[0] & PRINT, 'Variance: ', result[1] 
;PRINT, 'Skewness: ', result[2] & PRINT, 'Kurtosis: ', result[3]   
if show then print,'Standard Deviation (RMS Error): ',sdev

if show then oplot,!x.crange,color=3,sdev+[0,0]

return,sdev
end


pro test
  restore,'plottmp.sav'
  prf=profile
  print,determ_rms(prf(0).u,/show)
  
end
