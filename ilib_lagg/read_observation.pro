function read_observation,ipt,x=xp,y=yp, $
                          spinor_mode=spinor_mode,fits_mode=fits_mode, $
                          hinode_mode=hinode_mode,imax_mode=imax_mode, $
                          icont=icont,error=thiserror, $
                          fits4d_mode=fits4d_mode,dir=dirin,observation=obsin, $
                          original=original
  @common_localstray
  common profile,profile
  common old_ipt,old_ipt
  
  if n_elements(dirin) eq 0 then ipt_dir_profile=ipt.dir.profile $
  else ipt_dir_profile=dirin
  if n_elements(obsin) eq 0 then ipt_observation=ipt.observation $
  else ipt_observation=obsin
  
  
  spinor_mode=0             ;if extension .dat then assume spinor file
  dir_mode=0
  fits_mode=0
  fits4d_mode=0
  hinode_mode=0
  imax_mode=0
  synth_mode=0
  datpos=strpos(ipt_observation,'.dat')
  errobs={error:1b}
  
  
  if ipt_dir_profile ne '' then $
    if strmid(ipt_dir_profile,strlen(ipt_dir_profile)-1,1) ne '/' then $
    ipt_dir_profile=ipt_dir_profile+'/'
  if datpos ne -1 then begin
    if (ipt.verbose ge 1) then print,'Use single profile'
    spinor_mode=1
    ipt.x=[0,0] & ipt.y=[0,0]
    ipt.stepx=1 & ipt.stepy=1
    ipt.display_map=0
  ;; endif else if ipt.synth then begin ;synthetic profile
  ;;   if (ipt.verbose ge 1) then print,'Use synthetic spectrum'
  ;;   synth_mode=1
  ;;   ipt.x=[0,0] & ipt.y=[0,0]
  ;;   ipt.stepx=1 & ipt.stepy=1
  ;;   ipt.display_map=0
  endif else if strpos(ipt_observation,'.sav') ne -1 then begin
    if (ipt.verbose ge 1) then print,'Use spectrum from sav file'
    if n_elements(old_ipt) ne 0 then $
      reread=ipt_observation ne old_ipt_observation $
    else reread=1    
    if reread then begin
      rsav=ipt_dir_profile+ipt_observation
      if (ipt.verbose ge 1) then print,'restoring '+rsav
      dummy=findfile(rsav,count=cnt)
      if cnt eq 0 then begin
        message,/cont,rsav+' not found.'
;        reset
        thiserror=1b
        return,errobs
      endif
      rob=''
      restore,rsav
      if n_elements(CONTIN_SM) ne 0 then begin
        profile=read_solmhd(ic=contin_sm,stokes=stokes_sm, $
                            label_pol=l_pol,w0=w0,wl=wl,verbose=ipt.verbose) 
      endif else begin
        sObj = OBJ_NEW('IDL_Savefile',rsav)
        varnames=(strlowcase(sobj->Names()))
        OBJ_DESTROY,sObj
        pridx=(where(varnames eq 'profile'))
        if pridx(0) eq -1 then pridx=(where(varnames eq 'fitprof'))
        if pridx(0) eq -1 then pridx=(where(varnames eq 'observation'))>0
        profname=(varnames(pridx))(0)
        if profname ne 'profile' then dummy=execute('profile='+profname)
        tn=tag_names(profile)
        profok=max(tn eq 'I') and max(tn eq 'WL')
        if profok eq 0 then begin
          message,/cont,'Invalid profile in '+rsav
;          reset     
          thiserror=1b
          return,errobs
        endif
      endelse
      old_ipt=ipt
    endif
    if max(tag_names(profile) eq 'X') eq 1 then begin
      ipt.x=(ipt.x)<(n_elements(profile.x)-1)>0
      ipt.y=(ipt.y)<(n_elements(profile.y)-1)>0
    endif else begin
      ipt.x=0 & ipt.y=0
    endelse
  endif else begin              ;spectrum from directory      
    fi=file_info(ipt_dir_profile+ipt_observation)
    if fi.exists eq 0 then begin
      message,/cont,'Cannot find '+ipt_dir_profile+ipt_observation
;      reset
      thiserror=1b
      return,errobs      
    endif
    if fi.directory eq 1 then begin ;spectrum from directory
                                ;check if directory contains Hinode FITS files
      ffiles=file_search(ipt_dir_profile+ipt_observation+'/*SP4*.fits', $
                         count=fcnt)
      if fcnt eq 0 then $       ;try SP3 files
        ffiles=file_search(ipt_dir_profile+ipt_observation+'/*SP3*.fits', $
                           count=fcnt)
      if fcnt ne 0 then begin
        if (ipt.verbose ge 1) then print,'Use Hinode FITS files from directory'
        hinode_mode=1
      endif else begin
        if (ipt.verbose ge 1) then print,'Use spectrum from directory'
        dir_mode=1
      endelse
    endif else begin
      if (strpos(fi.name,'SP4') ge 0 or strpos(fi.name,'SP3') ge 0) $
        and strpos(fi.name,'.fits') ge 0 then begin
        if (ipt.verbose ge 1) then print,'Hinode FITS File mode'
        hinode_mode=1
      endif else begin
        mrd_head,fi.name,header
        naxis=sxpar(header,'NAXIS')
        if naxis eq 4 then begin
          fnam=strmid(fi.name,strpos(fi.name,'/',/reverse_search)+1, $
                      strlen(fi.name))
          if (strpos(strlowcase(fnam),'imax') ge 0 and $
              strpos(fnam,'4D') eq -1) then imax_mode=1 $
          else fits4d_mode=1
        endif else fits_mode=1
        if (ipt.verbose ge 1) then $
          print,'Use spectrum from FITS file: 4D / IMaX / reg ', $
                fits4d_mode,imax_mode,fits_mode
      endelse
    endelse
  endelse
  dols=fix(ipt.localstray_rad ge 1e-5)
  if (dols and (hinode_mode ne 1 and imax_mode ne 1 and $
                fits_mode ne 1 and fits4d_mode ne 1)) then begin
    message,/cont,'LOCALSTRAY correction only available for Hinode, ' + $
      'TIP (FITS), IMaX, FITS4D  dataformat'''
;    reset
    thiserror=1b
    return,errobs
  endif
;----------------------------------------------------------  
                                ;read observation
  get_profidx,x=xp,y=yp,stepx=ipt.stepx,stepy=ipt.stepy, $
    rgx=ipt.x,rgy=ipt.y, $
    vecx=xpvec,vecy=ypvec,average=ipt.average,scansize=ipt.scansize
  nxp=n_elements(xpvec) & nyp=n_elements(ypvec)
  if spinor_mode eq 1 then begin
    observation=read_spinor(ipt_dir_profile+ipt_observation,icont=icont, $
                            error=error)
    if ipt.ic_level ge 1e-5 then icont=ipt.ic_level
    observation.i=observation.i*icont
    thiserror=error
    if error eq 1 then begin
      print,'No profiles found. Returning.'
;      reset
      thiserror=1b
      return,errobs
    endif
  endif else if (dir_mode eq 1 or fits_mode eq 1 or fits4d_mode eq 1 or $
                 hinode_mode eq 1 or imax_mode eq 1) then begin
    if dir_mode eq 1 then begin
      profname=strarr(nxp,nyp)
      for ix=0,n_elements(xpvec)-1 do for iy=0,n_elements(ypvec)-1 do begin
        xstr=string(xpvec(ix),format=pfmt)
        ystr=string(ypvec(iy),format=pfmt)
        profname(ix,iy)=ipt_observation+ $
          "/x"+xstr+"/"+"x"+xstr+"y"+ystr+".profile.dat"
      endfor
    endif
    
    error=1
    case 1 of 
      dir_mode eq 1: begin
        for ix=0,nxp-1 do for iy=0,nyp-1 do begin
          observation=read_spinor(ipt_dir_profile+profname(ix,iy), $
                                  icont=icont,$
                                  error=thiserror)
          error=error and thiserror
          if thiserror eq 0 then begin
            observation.i=observation.i*icont
            if ix eq 0 and iy eq 0 then begin
              nwl=n_elements(observation.wl)
              profile={i:dblarr(nwl,nxp,nyp), $
                       q:dblarr(nwl,nxp,nyp), $
                       u:dblarr(nwl,nxp,nyp), $
                       v:dblarr(nwl,nxp,nyp), $
                       ic:dblarr(nxp,nyp), $
                       wl:observation.wl, $
                       x:indgen(nxp),y:indgen(nyp)}
            endif
            if ipt.ic_level ge 1e-5 then observation.ic=ipt.ic_level
            profile.ic(ix,iy)=observation.ic
            profile.i(*,ix,iy)=observation.i
            profile.q(*,ix,iy)=observation.q
            profile.u(*,ix,iy)=observation.u
            profile.v(*,ix,iy)=observation.v
          endif 
        endfor
        if error eq 1 then begin
          print,'No profiles found. Returning.'
;          reset
          thiserror=1b
          return,errobs
        endif
        xpv=0 & ypv=0
        if ipt.average eq 1 then begin
          xpv=indgen(n_elements(xpvec))
          ypv=indgen(n_elements(ypvec))
        endif
      end
      hinode_mode eq 1: begin   ;fits files
        txpvec=[min(xpvec),max(xpvec)]
        typvec=[min(ypvec),max(ypvec)]
        if ipt.average eq 0 then begin
          txpvec=xpvec(0) & typvec=ypvec(0)
        endif
        if ipt.verbose ge 2 then $
          print,'Reading Hinode FITS '+ipt_dir_profile+ipt_observation+', '+$
          'x'+add_comma(remove_multi(n2s(txpvec,format=pfmt)),sep='-') +$
          'y'+add_comma(remove_multi(n2s(typvec,format=pfmt)),sep='-')
        profile=read_hinode(ipt_dir_profile+ipt_observation, $
                            x=txpvec,y=typvec,error=error, $
                            scan=ipt.obs_par.hin_scannr, $
                            norm_cont=ipt.norm_cont, $
                            lsprof=lsprof)
        thiserror=error ne 0
        if error eq 0 then begin
          icont=profile.ic
          profile.i=profile.i*profile.ic              
          if ipt.ic_level ge 1e-5 then profile.ic=ipt.ic_level
        endif else icont=0.
      end
      imax_mode eq 1: begin     ;imax 4D files
        txpvec=[min(xpvec),max(xpvec)]
        typvec=[min(ypvec),max(ypvec)]
        if ipt.average eq 0 then begin
          txpvec=xpvec(0) & typvec=ypvec(0)
        endif
        if ipt.verbose ge 2 then $
          print,'Reading IMaX '+ipt_dir_profile+ipt_observation+',  '+ $
          'x'+add_comma(remove_multi(n2s(txpvec,format=pfmt)),sep='-') +$
          'y'+add_comma(remove_multi(n2s(typvec,format=pfmt)),sep='-')
        profile=read_imax(x=txpvec,y=typvec,error=error, $
                          ipt_dir_profile+ipt_observation, $
                          lsprof=lsprof)
        profile.i=profile.i*profile.ic              
        if ipt.ic_level ge 1e-5 then profile.ic=ipt.ic_level
        icont=profile.ic
        thiserror=error ne 0
      end
      fits4d_mode eq 1: begin   ;helix / spinor 4D Fits files
        txpvec=[min(xpvec),max(xpvec)]
        typvec=[min(ypvec),max(ypvec)]
        if ipt.average eq 0 then begin
          txpvec=xpvec(0) & typvec=ypvec(0)
        endif
        if ipt.verbose ge 2 then $
          print,'Reading 4D-Fits '+ipt_dir_profile+ipt_observation+',  '+ $
          'x'+add_comma(remove_multi(n2s(txpvec,format=pfmt)),sep='-') +$
          'y'+add_comma(remove_multi(n2s(typvec,format=pfmt)),sep='-')
        head=headfits(ipt_dir_profile+ipt_observation)
        code=strtrim(strlowcase(sxpar(head,'CODE')),2)
        if code eq 'spinor' or code eq 'helix' then begin
          read_fitsstokes,ipt_dir_profile+ipt_observation,data,/iquv, $
            error=error,x=xpvec,y=ypvec,/average
        endif else begin
          header4d=''
          read_fits4d,ipt_dir_profile+ipt_observation,data,error=error, $
            x=xpvec,y=ypvec,/average,header=header4d
        endelse
        if error eq 0 then begin
          profile= $
;            {i:data.i,q:data.q,u:data.u,v:data.v,ic:data.icont,wl:data.wl}
            {i:(*data.iquv)(*,0),q:(*data.iquv)(*,1), $
             u:(*data.iquv)(*,2),v:(*data.iquv)(*,3),ic:data.icont,wl:data.wl}
          lsprof=get_lsprof(ipt_dir_profile+ipt_observation,txpvec,typvec, $
                            profile.wl,n_elements(profile.wl), $
                            ipt=ipt,mode=5,do_ls=do_ls)
          profile.i=profile.i*profile.ic      
          if ipt.ic_level ge 1e-5 then begin
            profile.i=profile.i*profile.ic/ipt.ic_level
            profile.q=profile.q*profile.ic/ipt.ic_level
            profile.u=profile.u*profile.ic/ipt.ic_level
            profile.v=profile.v*profile.ic/ipt.ic_level
            profile.ic=ipt.ic_level
          endif
          icont=profile.ic          
        endif
        thiserror=error ne 0
      end
      fits_mode eq 1: begin     ;fits files
        txpvec=[min(xpvec),max(xpvec)]
        typvec=[min(ypvec),max(ypvec)]
        if ipt.average eq 0 then begin
          txpvec=xpvec(0) & typvec=ypvec(0)
        endif
        if ipt.verbose ge 2 then $
          print,'Reading FITS '+ipt_dir_profile+ipt_observation+',  '+ $
          'x'+add_comma(remove_multi(n2s(txpvec,format=pfmt)),sep='-') +$
          'y'+add_comma(remove_multi(n2s(typvec,format=pfmt)),sep='-')
        profile=read_fitsobs(x=txpvec,y=typvec,error=error, $
                             file=ipt_dir_profile+ipt_observation, $
                             ipt=ipt,silent=ipt.verbose le 1, $
                             norm_cont=ipt.norm_cont, $
                             lsprof=lsprof)
        profile.i=profile.i*profile.ic              
        if ipt.ic_level ge 1e-5 then profile.ic=ipt.ic_level
        icont=profile.ic
        thiserror=error ne 0
      end
    endcase
    if thiserror eq 0 then begin
      if total(ipt.wl_range) le 1e-5 then $
        ipt.wl_range=[min(profile.wl),max(profile.wl)]
      inwl=where(profile.wl le max(ipt.wl_range) and $
                 profile.wl ge min(ipt.wl_range))
      if inwl[0] eq -1 then ipt.wl_range=[min(profile.wl),max(profile.wl)]
      observation=get_profile(profile=profile, $
                              ix=xpv,iy=ypv,icont=icont, $
                              wl_range=ipt.wl_range)
    endif
  endif else begin
    if total(ipt.wl_range) le 1e-5 then $
      ipt.wl_range=[min(profile.wl),max(profile.wl)]
    observation=get_profile(profile=profile, $
                            ix=xpvec,iy=ypvec,icont=icont, $
                            wl_range=ipt.wl_range)      
    if max(observation.i) lt 5. then $
      observation.i=observation.i*icont
  endelse
  
  if thiserror ne 0 then return,errobs

  if keyword_set(original) then return,observation
  
                                ;use WL-calibration from input file
  if ipt.wl_off gt 1e-5 and synth_mode eq 0 then begin
    if ipt.verbose ge 1 then print,'Using WL-Calibration from input file'
    observation.wl= $
      findgen(n_elements(observation.wl))*ipt.wl_disp+ipt.wl_off
  endif else if ipt.verbose ge 1 then $
    print,'Using WL-Calibration from data file'
  
  
  
                                ;add artificial noise: first read in
                                ;prefilter info for obs-WL (needed for
                                ;noise increase caused by prefilter)
                                ;prefilter_wlerr set to 0.
  prefilter=read_prefilter(ipt.prefilter,0.0, $
                           observation.wl,ipt.verbose)
  observation=add_noise(observation,noise=ipt.noise,inoise=ipt.inoise, $
                        doprefilter=prefilter.doprefilter, $
                        prefilter=prefilter.val)
  
  if ipt.spike ge 1 then $
    observation=remove_spike(observation,ipt.spike,verbose=ipt.verbose)
  
  
  if max(tag_names(ipt) eq 'FILTER_MODE') eq 0 then filter_mode=0 $
  else filter_mode=ipt.filter_mode
  if max(tag_names(ipt) eq 'SMOOTH') eq 1 then smoothval=ipt.smooth $
  else smoothval=0
  if smoothval ne 0 or ipt.median ne 0 then $
    observation=remove_noise(observation,filter=smoothval, $
                             mode=filter_mode,median=ipt.median)
  
                                ;WL-binning
  if ipt.wl_bin gt 1 then begin
    nwl=n_elements(observation.wl)
    if ipt.wl_bin ge nwl then begin
      message,/cont,'Error in WL-binning, bin-size: '+n2s(ipt.wl_bin)
;      reset
      thiserror=1b
      return,errobs
    endif else begin
      iw=0
      for i=0,nwl-1,ipt.wl_bin do begin
        j=(i+ipt.wl_bin-1)<(nwl-1)
        observation.wl(iw)=total(observation.wl(i:j))/(j-i+1)
        observation.i(iw)=total(observation.i(i:j))/(j-i+1)
        observation.u(iw)=total(observation.u(i:j))/(j-i+1)
        observation.q(iw)=total(observation.q(i:j))/(j-i+1)
        observation.v(iw)=total(observation.v(i:j))/(j-i+1)
        iw=iw+1
      endfor
      if max(tag_names(observation) eq 'IC') eq 1 then $
        toic=observation.ic else toic=1.
      observation={ic:toic,wl:observation.wl(0:iw-1), $
                   i:observation.i(0:iw-1),q:observation.q(0:iw-1), $
                   u:observation.u(0:iw-1),v:observation.v(0:iw-1)}
    endelse
  endif
  return,observation
end
