;counts nr of occurances of substring in string
function count_substring,str,sub,position=position
  
  
  if strlen(str) gt 0 and strlen(sub) gt 0 then begin
    bstr=byte(str)
    bsub=byte(sub)
    
    pos=bytarr(strlen(bstr))+1b
    for i=0,(n_elements(bsub))-1 do begin
      letti=where(bstr eq bsub(i))
      vlp=bytarr(strlen(bstr))
      if letti(0) ge 0 then vlp((letti-i)>0)=1b
      pos=pos*vlp
    endfor
    position=where(pos eq 1)
    if position(0) eq -1 then nline=0 else nline=n_elements(position)
  endif else begin
    nline=0 & position=-1
  endelse
  
  return,nline
end
