;boxcar smoothing with weighted averaging
function smooth_wgt,arr,smval,wgt
  
  retarr=arr
  sz=size(arr,/dim)
  sm=fix(smval)
  for ix=0,sz[0]-1 do for iy=0,sz[1]-1 do begin
    xx=(ix+[-sm,sm]/2>0)<(sz[0]-1)
    yy=(iy+[-sm,sm]/2>0)<(sz[1]-1)
    avg=total(arr[xx[0]:xx[1],yy[0]:yy[1]]*wgt[xx[0]:xx[1],yy[0]:yy[1]],/nan)/$
        total(wgt[xx[0]:xx[1],yy[0]:yy[1]],/nan)
    retarr[ix,iy]=avg    
  endfor
  return,retarr
end
