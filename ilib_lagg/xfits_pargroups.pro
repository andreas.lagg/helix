;find groups
pro xfits_pargroups
   common xfwg,xfwg
   common fits,fits
   common xf_group,xf_group
   
   parstr=[fits.short]
   np=n_elements(parstr)
   
   st={name:'',index:intarr(np),n:0}
   xf_group=replicate(st,100)
   ig=0
   
                                ;comp,lgtau groups
   tags=['comp','logtau']
   tgn=tag_names(fits)
   for i=0,n_elements(tags)-1 do begin
     it=where(tgn eq strupcase(tags(i)))
     grp=remove_multi(fits.(it),/no_empty)
     if n_elements(grp) ge 1 then for i=0,n_elements(grp)-1 do begin
       idx=where(fits.(it) eq grp(i) and fits.pixrep eq 0)
       if grp(i) eq fix(grp(i)) then format='(i10)' else format='(f15.2)'
       xf_group(ig).name=tgn(it)+' '+n2s(grp(i),format=format)
       xf_group(ig).n=n_elements(idx)
       xf_group(ig).index(0:xf_group(ig).n-1)=idx
       ig=ig+1
     endfor
   endfor
   
                                ;par groups
   for i=0,np-1 do if max(xf_group.name eq fits.par(i)) eq 0 then begin
     idx=where(fits.par eq fits.par(i) and fits.pixrep eq 0)
     if n_elements(idx) ge 1 then begin
       xf_group(ig).name=fits.par(i)
       xf_group(ig).n=n_elements(idx)
       xf_group(ig).index(0:xf_group(ig).n-1)=idx
       ig=ig+1
     endif
   endif
   
   if ig ge 1 then xf_group=xf_group(0:ig-1)
end
