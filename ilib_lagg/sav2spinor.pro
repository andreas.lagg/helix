pro sav2spinor,sav
  
  if n_params() ne 1 then begin
    print,'convert sav- file created by Helix to spinor profile.'
    print,'Usage: sav2spinor,''helixfile.sav'''
    reset
  endif
  
  restore,/v,sav
  
  if n_elements(observation) ne 0 then begin
    profile=observation
    wlref=total(minmaxp(profile.wl))/2.
    add='Observation'
  endif else if n_elements(fitprof) ne 0 then begin
    profile=fitprof
    wlref=all_lines(0).wl
    add='Fit'
  endif else message,'unkonown helix sav file.'
  header=add+' from HeLIX sav: '+sav
  
  fsto=strmid(sav,0,strpos(sav,'.sav'))+'_stopro.dat'
  
;   profile.i=profile.i/profile.ic
;   profile.q=profile.q/profile.ic
;   profile.u=profile.u/profile.ic
;   profile.v=profile.v/profile.ic
  write_stopro,file=fsto,profile=profile,wlref=wlref, $
    header=header,error=error,/verbose
  
  plot_profiles,profile,title=header
end
