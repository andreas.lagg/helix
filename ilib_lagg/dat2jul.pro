function dat2jul,datstr,reverse=reverse
  
  if keyword_set(reverse) then begin
    caldat,double(datstr),mmonth,mday,myear,mhour,mminute,msec
    retval=string(myear,format='(i4.4)')+'.'+ $
      string(mmonth,format='(i2.2)')+'.'+ $
      string(mday,format='(i2.2)')+'_'+ $
      string(mhour,format='(i2.2)')+':'+ $
      string(mminute,format='(i2.2)')+':'+ $
      string(msec,format='(i2.2)')
  endif else begin
    myear=strmid(datstr,0,4)
    mmonth=strmid(datstr,5,2)
    mday=strmid(datstr,8,2)
    mhour=strmid(datstr,11,2)
    mmin=strmid(datstr,14,2)
    msec=strmid(datstr,17,2)
    retval=julday(fix(mmonth),fix(mday),fix(myear), $
                  fix(mhour),fix(mmin),fix(msec))
  endelse
  
  return,retval
end
