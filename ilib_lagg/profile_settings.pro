function iquv2byte,in,reverse=reverse
  
  if keyword_set(reverse) then begin
    out=''
    if in(0) eq 1 then out=out+'I'
    if in(1) eq 1 then out=out+'Q'
    if in(2) eq 1 then out=out+'U'
    if in(3) eq 1 then out=out+'V'
  endif else begin
    out=[strpos(in,'I') ne -1,strpos(in,'Q') ne -1, $
         strpos(in,'U') ne -1,strpos(in,'V') ne -1]
  endelse
  return,out  
end

pro wgp_event,event
  common wgp,wgp
  common prof_set,prof_set
  common wgst,wgst
  
  if n_elements(wgst) ne 0 then if !d.name eq 'X' then begin
    if max(tag_names(wgst) eq 'PLOT') eq 1 then $
      widget_control,wgst.plot.id,get_value=widx
  endif
  
  if n_elements(wgst) ne 0 then $
    if max(tag_names(wgst) eq 'WLRANGE') eq 1 then $
    wlrg=wgst.wlrange.val
  
  widget_control,event.id,get_uvalue=uval
  case uval of
    'show_obs': prof_set.show_obs=event.select
    'show_fit': prof_set.show_fit=event.select
    'show_cont': prof_set.show_cont=event.select
    'show_lines': prof_set.show_lines=event.select
    'show_wgt': prof_set.show_wgt=event.select
    'show_comps': begin
      prof_set.show_comps=event.select
      widget_control,wgp.compnr.id,sensitive=prof_set.show_comps eq 1
    end
    'compnr': prof_set.compnr(event.value)=event.select
    'fitpsym': prof_set.fitpsym=event.select
    'show_info': prof_set.show_info=event.select
    'show_title': prof_set.show_title=event.select
    'norm2ic': prof_set.norm2ic=event.select
    'wlunit': prof_set.wlunit=event.select
    'show_input': prof_set.show_input=event.select
    'title': prof_set.title=event.value
    'irange0': prof_set.range(0,0)=event.value
    'irange1': prof_set.range(0,1)=event.value
    'qrange0': prof_set.range(1,0)=event.value
    'qrange1': prof_set.range(1,1)=event.value
    'urange0': prof_set.range(2,0)=event.value
    'urange1': prof_set.range(2,1)=event.value
    'vrange0': prof_set.range(3,0)=event.value
    'vrange1': prof_set.range(3,1)=event.value
    'wlrange0': prof_set.wlrange(0)=event.value
    'wlrange1': prof_set.wlrange(1)=event.value
    'autorange': begin
      prof_set.autorange=event.select
      
      if event.select eq 1 then $
        if n_elements(wlrg) ne 0 then prof_set.wlrange=wlrg
      for j=0,1 do begin
        for i=0,3 do widget_control,wgp.yrange(i,j).id, $
          sensitive=prof_set.autorange eq 0
        widget_control,wgp.wlrange(j).id,set_value=prof_set.wlrange(j), $
          sensitive=prof_set.autorange eq 0
      endfor
    end
    'charsize': prof_set.charsize=event.value
    'thick': prof_set.thick=event.value
    'show': begin
      widget_control,wgp.show.id,get_value=gv
      prof_set.show=iquv2byte(/reverse,gv)
    end
    'control': begin
      sz=[18.,18./!d.x_size*!d.y_size]
      oldch=!p.charsize
      !p.charsize=prof_set.charsize
      if n_elements(wgst) ne 0 then widget_control,wgst.base.id,sensitive=0
      if prof_set.autorange eq 1 then yrange=0 else begin
        yrange=transpose(prof_set.range)
        ws=where(iquv2byte(prof_set.show) eq 1)
        if ws(0) ne -1 then yrange=yrange(*,ws)
      endelse
      if abs(prof_set.wlrange(1)-prof_set.wlrange(0)) ge 1e-5 then $
        twlrg=prof_set.wlrange
      case event.value of
        'done': widget_control,wgp.base.id,/destroy
        'plotx': begin
;          psset,size=sz,win_nr=prof_set.win_nr
          plot_profiles,wl_range=twlrg,position=0,range=yrange
        end
        'plotps': begin
          if n_elements(pikaia_result) ne 0 then $
            psdir=pikaia_result.input.dir.ps $
          else psdir='./ps/'
          psf=psdir+'profile.ps'
          psset,file=psf,/ps,group_leader=wgp.base.id;,size=sz
          plot_profiles,wl_range=twlrg,range=yrange
          psset,/close
        end
      endcase
      if n_elements(wgst) ne 0 then widget_control,wgst.base.id,sensitive=1
      !p.charsize=oldch
    end
    else: begin
      help,/st,event
    end
  endcase
  
                                ;execute statement to avoid stopping
                                ;execution when window was closed.
  if prof_set.win_nr ne -1 then dummy=execute('wset,'+n2s(prof_set.win_nr))
;  if n_elements(widx) ne 0 then wset,widx
end

pro pset_widget
  common wgst,wgst
  common pikaia,pikaia_result,oldsav,prpar
  common verbose,verbose
  common param,param
  common prof_set,prof_set
  common wgp,wgp
  
  subst={id:0l,val:0.,str:''}
  if n_elements(wgp) eq 0 then begin
    wgp={base:subst, $
         show_obs:subst,show_fit:subst,show_cont:subst,show_wgt:subst, $
         show:subst,show_info:subst,show_lines:subst,show_comps:subst, $
         title:subst,show_title:subst,show_input:subst,charsize:subst, $
         control:subst,compnr:subst,fitpsym:subst,thick:subst, $
         yrange:replicate(subst,4,2),autorange:subst,norm2ic:subst, $
         wlunit:subst,wlrange:replicate(subst,2)}
    prof_set.autorange=1
    if n_elements(wgst) ne 0 then $
      if max(tag_names(wgst) eq 'WLRANGE') eq 1 then $
      prof_set.wlrange=wgst.wlrange.val
  endif
  
  if n_elements(wgst) ne 0 then begin
    widget_control,wgst.base.id,bad_id=bad_id
    if bad_id eq 0 then gl=wgst.base.id else dummy=temporary(wgst)
  endif
  wgp.base.id=widget_base(title='Plot Profile Settings',/col, $
                          group_leader=gl,floating=n_elements(gl) ne 0)
  
  sub=widget_base(wgp.base.id,/row,/frame)
  lab=widget_label(sub,value='Show Profiles: ')
  wgp.show_obs.id=cw_bgroup(sub,uvalue='show_obs','Observation', $
                            set_value=prof_set.show_obs,/nonexclusive)
  wgp.show_fit.id=cw_bgroup(sub,uvalue='show_fit','Fit', $
                            set_value=prof_set.show_fit,/nonexclusive)
  wgp.show_comps.id=cw_bgroup(sub,uvalue='show_comps','Comps', $
                            set_value=prof_set.show_fit,/nonexclusive)
  wgp.show_wgt.id=cw_bgroup(sub,uvalue='show_wgt','Weighting', $
                            set_value=prof_set.show_wgt,/nonexclusive)
  wgp.show_cont.id=cw_bgroup(sub,uvalue='show_cont','Cont-Level', $
                             set_value=prof_set.show_cont,/nonexclusive)
  sub=widget_base(wgp.base.id,/row)
  sub1=widget_base(sub,/row,/frame)
  prof_set.compnr(*)=1b
  lab=widget_label(sub1,value='Show Comp Nr: ')
  wgp.compnr.id=cw_bgroup(sub1,uvalue='compnr',row=2, $
                          n2s(indgen(20)+1), $
                          set_value=prof_set.compnr,/nonexclusive)
  widget_control,wgp.compnr.id,sensitive=prof_set.show_comps eq 1
  sub1=widget_base(sub,/row,/frame)
  wgp.fitpsym.id=cw_bgroup(sub1,uvalue='fitpsym',row=1,'symbols for fit', $
                           set_value=prof_set.fitpsym,/nonexclusive)
  widget_control,wgp.fitpsym.id,sensitive=prof_set.show_fit eq 1
  
  sub=widget_base(wgp.base.id,/row,/frame)
  lab=widget_label(sub,value='Show Stokes Par: ')
  stokes=['I','Q','U','V']
  wgp.show.id=cw_bgroup(sub,uvalue='show',stokes,/row, $
                        set_value=iquv2byte(prof_set.show),/nonexclusive)
  lab=widget_label(sub,value='          ')
  wgp.norm2ic.id=cw_bgroup(sub,uvalue='norm2ic','norm. to Ic', $
                           set_value=prof_set.norm2ic,/nonexclusive)
  wgp.wlunit.id=cw_bgroup(sub,uvalue='wlunit','nm/Angstrom', $
                           set_value=prof_set.wlunit,/nonexclusive)
;  sub1=widget_base(sub,/row,/frame)
                            
  sub=widget_base(wgp.base.id,/row,/frame)
  wgp.show_info.id=cw_bgroup(sub,uvalue='show_info', $
                             'Show Atmosphere Info', $
                             set_value=prof_set.show_info,/nonexclusive)
  wgp.show_lines.id=cw_bgroup(sub,uvalue='show_lines','Show Lines', $
                            set_value=prof_set.show_lines,/nonexclusive)
  wgp.show_input.id=cw_bgroup(sub,uvalue='show_input','Print Input-File (PS)',$
                             set_value=prof_set.show_input,/nonexclusive)
  
  sub=widget_base(wgp.base.id,col=1,/frame)
  subr=widget_base(sub,row=1)
  wgp.autorange.id=cw_bgroup(subr,uvalue='autorange','AutoRange',$
                             set_value=prof_set.autorange,/nonexclusive)
  lab=widget_label(subr,value='WL-range:')
  for j=0,1 do begin
    wgp.wlrange(j).id=cw_field(subr,title='',/floating,xsize=12, $
                               /all_events,value=prof_set.wlrange(j), $
                               uvalue='wlrange'+n2s(j))
    widget_control,wgp.wlrange(j).id,sensitive=prof_set.autorange eq 0
  endfor
  subr=widget_base(sub,row=2)
  for i=0,3 do begin
    lab=widget_label(subr,value=stokes(i)+'-range:')
    for j=0,1 do begin
      wgp.yrange(i,j).id=cw_field(subr,title='',/floating,xsize=10, $
                                  /all_events,value=prof_set.range(i,j), $
                                  uvalue=strlowcase(stokes(i))+'range'+n2s(j))
      widget_control,wgp.yrange(i,j).id,sensitive=prof_set.autorange eq 0
    endfor
  endfor
  
  subr=widget_base(wgp.base.id,col=2,/frame)
  wgp.show_title.id=cw_bgroup(subr,uvalue='show_title','Show Title', $
                              set_value=prof_set.show_wgt,/nonexclusive)
  wgp.title.id=cw_field(subr,/all_events,xsize=32,/string, $
                        uvalue='title',title='Title:')
  wgp.thick.id=cw_field(subr,uvalue='thick',title='Line thickness:',/all_ev, $
                           /integer,xsize=3)
  widget_control,wgp.thick.id,set_value=prof_set.thick
  wgp.charsize.id=cw_field(subr,uvalue='charsize',title='Charsize:',/all_ev, $
                           /float,xsize=5)
  widget_control,wgp.charsize.id,set_value=prof_set.charsize
    
    
  sub=widget_base(wgp.base.id,/row,/frame)
  wgp.control.id=cw_bgroup(sub,['Done','Plot Last Profile (X)', $
                                        'Plot Last Profile (PS)'], $
                           uvalue='control',row=1, $
                           button_uvalue=['done','plotx','plotps'])
  widget_control,wgp.base.id,/realize
  xmanager,'wgp',wgp.base.id,no_block=1
end

pro profile_settings,default=default,init=init
  common wgp,wgp
  common prof_set,prof_set
  

  ps_st={show_obs:1b,show_fit:1b,show_cont:1b,show:'IQUV', $
         show_info:1b,title:'',show_title:1b,show_wgt:1b,show_comps:1b, $
         show_lines:1b,show_input:1b,charsize:1.,thick:1,win_nr:-1, $
         compnr:bytarr(20),fitpsym:0b,norm2ic:0,wlunit:0,autorange:1, $
         wlrange:dblarr(2),range:dblarr(4,2)}
  if n_elements(prof_set) eq 0 then prof_set=ps_st
  
                                ;just return default profile parameters
  if keyword_set(init) then begin
    prof_set=ps_st
    default=prof_set
    return
  endif
  
  
  if n_elements(wgp) ne 0 then begin
    widget_control,wgp.base.id,iconify=0,bad_id=bad_id
    if bad_id eq 0 then return; else dummy=temporary(wgp)
  endif
  
  pset_widget
  
  
  
end
