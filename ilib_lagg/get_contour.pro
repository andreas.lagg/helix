function get_contour,img,level=level,show=show
  
  contour,img,level=level,path_xy=pathxy,/path_data,path_info=pathinfo
  npi=n_elements(pathinfo)
  dims = SIZE(img, /DIMENSIONS) 
  
  if npi eq 0 then return,{map:lonarr(dims(0),dims(1)),nel:0,area:0}
  
  result={map:lonarr(dims(0),dims(1)),pathxy:pathxy,pathinfo:pathinfo, $
          area:lonarr(npi),nel:npi}
  
  if keyword_set(show) then userlct
  n0=0
  for i=0l,npi-1 do begin
    line = [LINDGEN(PathInfo(I).N), 0]
    oROI = OBJ_NEW('IDLanROI', $ 
                   (pathXY(*, pathInfo(I).OFFSET + line))[0, *], $ 
                   (pathXY(*, pathInfo(I).OFFSET + line))[1, *])
    if keyword_set(show) then DRAW_ROI, oROI, COLOR = i+1
    maskResult = oROI -> ComputeMask(DIMENSIONS = [dims[0], dims[1]]) 
    IMAGE_STATISTICS, img, MASK = maskResult, COUNT = maskArea
    maskresult=maskresult ne 0
                                ;check if new result is inside old result
    if total((result.map ne 0) * maskresult) ge 1 then begin
                                ;remove this patch
      inside=where((result.map ne 0) * maskresult eq 1)
                                ;remove inside pixels
      imap=result.map(inside(0))-1
      result.map(inside)=0
      result.area(imap)=result.area(imap)-n_elements(inside)
      if keyword_set(show) then $
        plots,inside mod dims(0),inside/dims(0),psym=7
    endif else begin
      result.map=result.map + maskresult*(i+1)
      result.area(i)=maskArea
    endelse
  endfor
  return,result
end

pro test
  
  nel=40
  img=dist(nel)
  img=img+randomn(seed,nel,nel)*nel*.99
  img=smooth(img,6,/edge)
  
  img=fltarr(20,20)
  img(5:6,10)=1
  
  level=nel*0.5
  dim=size(img,/dimension)
  
  userlct,/full
  image_cont_al,img,retpos=pos,/aspect,/cut,contour=0, $
    xrange=[0.,nel],yrange=[0.,nel]
  contour,img,/xst,/yst,level=level,/noerase, $
    xrange=[0.,nel],yrange=[0.,nel],c_thick=3
  wait,2
  print,'Now analyze image...'
  cst=get_contour(img,level=level,show=1)
  
       
  idx=0                         ;example: analyze Patch #5
  if idx ge cst.nel then begin
    print,'Image has only ',cst.nel,' patches.'
    return
  end
  print,'Patch #',idx
  print,'  Area: ',cst.area(idx)
  if cst.area(idx) ge 1 then begin
    inarea=where(cst.map eq idx+1) ;add 1 since first patch is 1 (and not 0)
    xval=inarea mod dim(0)
    yval=inarea  /  dim(0)
    print,' X-range: ',minmaxp(xval)
    print,' Y-range: ',minmaxp(yval)
    plots,xval,yval,color=7,psym=4
  endif
  
  stop
  
end

