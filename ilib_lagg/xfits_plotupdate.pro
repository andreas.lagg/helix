                               ;do actual plotting for xfits
function pudata,ip,ix,iy,xsel,ysel,valid,parname,scatter=scatter, $
                contour=contour,title=title,shorttitle=shorttitle, $
                six=six,siy=siy,condition=condition
  common fits,fits
  common xfwg,xfwg
  
  title=''
  shorttitle=''
  six=ix
  siy=iy
  if ip lt fits.npar then begin
    data=fits.data(*,*,ip)
    valid=fits.valid(*,*,ip)
                                ;select window
    data=data(xsel(0):xsel(1),ysel(0):ysel(1),*)
    valid=valid(xsel(0):xsel(1),ysel(0):ysel(1),*)
    parname=fits.par(ip)
    pitmp=get_parinfo(fits.par(ip),fits.code)
    add=strsplit(fits.long(ip),', ',/regex,/extract)
    if n_elements(add) ge 2 then add=', '+strjoin(add(1:*)+' ') else add=''
    longtit=pitmp(0).longname
    if strlen(longtit) eq 0 then title=fits.short(ip) else title=longtit+add
    shorttitle=fits.short(ip)
  endif else begin
    otherpar=1b
    if keyword_set(contour) or keyword_set(scatter) or $
      n_elements(condition) eq 1 then begin
      if keyword_set(contour) then parname=xfwg.p(ix,iy).contpar.str 
      if n_elements(condition) eq 1 then parname=xfwg.p(ix,iy).cond[condition].str
      if keyword_set(scatter) then parname=xfwg.p(ix,iy).scatter.str
        if strpos(parname,'x=') ne -1 then begin
          six=fix((strsplit(parname,/extract,/regex,'x='))(1))
          siy=fix((strsplit(parname,/extract,/regex,'y='))(1))
          otherpar=0b
        endif
    endif
    if otherpar eq 1 then begin
      case (strsplit(xfwg.p(ix,iy).par.str,/extract,/regex))[0] of
;        'Stokes Par': $
        'Stokes': $
          parname='Stokes '+(['I','Q','U','V','Ic'])(xfwg.p(ix,iy).stokes)
        'User': begin
          parname='User'
        end
        else: message,'Undefined Parameter: '+xfwg.p(ix,iy).par.str
      endcase
    endif
    case (strsplit(parname,/extract))(0) of 
      'Stokes': begin
        data=xfits_getstokespar(six,siy,xrange=xsel+fits.xoff, $
                                yrange=ysel+fits.yoff, $
                                valid=valid,title=title)
        szd=size(data,/dim)
        if (szd[0] lt (xsel[1]-xsel[0])+1 or $
            szd[1] lt (ysel[1]-ysel[0]+1)) then begin
          dnew=fltarr(xsel[1]-xsel[0]+1,ysel[1]-ysel[0]+1)+!values.f_nan
          dnew[0:szd[0]-1,0:szd[1]-1]=data
          data=dnew
        endif
        shorttitle=title
      end
      'User': begin
        data=xfits_userpar(six,siy,xrange=xsel+fits.xoff, $
                           yrange=ysel+fits.yoff, $
                           valid=valid,title=title)
        shorttitle=title
      end
      else: message,'Undefined parname '+parname
    endcase
  endelse
  return,data
end

function apply_image_filter,data,xy,mode=mode
  common xfwg,xfwg
  
  minsz=min(size(data,/dim))
                                ;smoothing
  if xfwg.p[xy[0],xy[1]].img[mode].smooth_apply eq 1 then begin
    data=smooth(data,xfwg.p[xy[0],xy[1]].img[mode].smooth<(minsz-1),/nan,/edge_truncate)
  endif
                                ;unsharp-masking
  if xfwg.p[xy[0],xy[1]].img[mode].unsharp_apply eq 1 then $
    if xfwg.p[xy[0],xy[1]].img[mode].unsharp[0] ge 1e-4 then begin
    usmax=(minsz-1)/3
    udata=unsharp_mask(data,threshold=0, $
                       amount=xfwg.p[xy[0],xy[1]].img[mode].unsharp[0], $
                       radius=xfwg.p[xy[0],xy[1]].img[mode].unsharp[1]>0<usmax)
    if xfwg.p[xy[0],xy[1]].img[mode].unsharp_only eq 1 then $
      data=data-udata/xfwg.p[xy[0],xy[1]].img[mode].unsharp[0] $
    else data=udata
  endif
  return,data
end
        
pro xfits_plotupdate,ps=ps,singlepage=singlepage
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xdir,xdir
  common xss,xss

  if keyword_set(ps) then begin
    if xpwg.size(1) gt xpwg.size(0) then wd=18. else wd=27.
    psz=[wd,wd*xpwg.size(1)/xpwg.size(0)]
    file=(reverse(strsplit(/extract,fits.file,'/')))(0)
    file=(strsplit(/extract,file,'.fits',/regex))(0)+'.ps'
    psset,/ps,size=psz,file=xdir.ps+file,/oldsettings
  endif
  
  if !d.name ne 'PS' then begin
    widget_control,xpwg.draw.id,get_value=windex
    wset,windex
  endif
  
                                ;set zoom window correctly
  if xpwg.zoomwin(2).val lt xpwg.zoomwin(0).val then $
    xpwg.zoomwin([0,2]).val=xpwg.zoomwin([2,0]).val
  if xpwg.zoomwin(3).val lt xpwg.zoomwin(1).val then $
    xpwg.zoomwin([1,3]).val=xpwg.zoomwin([3,1]).val
  for i=0,3 do widget_control,xpwg.zoomwin(i).id, $
    set_value=fix(xpwg.zoomwin(i).val)
  
  xpwg.xoff=xpwg.zoomwin(0).val;-fits.xoff
  xpwg.yoff=xpwg.zoomwin(1).val;-fits.yoff
  
  if keyword_set(singlepage) eq 0 then begin
    ixy=indgen(xfwg.nx.val*xfwg.ny.val)
    if xpwg.multipage.val eq 0 then begin
      !p.multi=[0,xfwg.nx.val,xfwg.ny.val] 
    endif else begin
      !p.multi=0
      if !d.name ne 'PS' then ixy=xpwg.pagepm.val
    end
  endif else begin
    !p.multi=0
    ixy=xpwg.pagepm.val
  endelse
  
  !p.position=0
  !p.charsize=xpwg.charsize.val
  !x.margin=[8,2] & !y.margin=[3,2]
;  !x.margin=[12,1] & !y.margin=[3,2]
;  !x.margin=[8,0] & !y.margin=[0,0]
  erase
  
  szd=size(fits.data(*,*,0))
  xsel=[0,szd(1)-1]
  ysel=[0,szd(2)-1]
  if total(fix(xpwg.zoomwin.val)) ne 0 then begin
    xsel=((xpwg.zoomwin([0,2]).val-fits.xoff)>0)<(szd(1)-1)
    ysel=((xpwg.zoomwin([1,3]).val-fits.yoff)>0)<(szd(2)-1)
  endif
  szf=size(fits.data)
  xsel(1)=(xsel(1)>(xsel(0)+1))<(szf(1)-1)
  ysel(1)=(ysel(1)>(ysel(0)+1))<(szf(2)-1)
  
  ifit=(where(fits.par eq 'FITNESS' or fits.par eq 'CHISQ'))
  if n_elements(ifit) eq 1 then xfwg.parmode.val=0
  if xfwg.parmode.val ne 0 then begin
    ifit=ifit(xfwg.parmode.val-1+ $
              min(where(strpos(fits.short(ifit),'BEST') ge 0)))
  endif else ifit=ifit(0)
;  print,'Mean '+fits.par(ifit)+': ',total(fits.data(xsel(0):xsel(1),ysel(0):ysel(1),ifit))/total(fits.valid(xsel(0):xsel(1),ysel(0):ysel(1),ifit))
  
  if !d.name eq 'PS' and xpwg.multipage.val eq 1 then begin
    xx=scaleaxis(xsel,/x) & dx=max(xx)-min(xx)
    yy=scaleaxis(ysel,/y) & dy=max(yy)-min(yy)
    arplot=float(dx)/(dy)
    aold=0.
    for nx=1,xpwg.plotsperpage.val do begin
      ny=fix(xpwg.plotsperpage.val)/nx + $
         ((fix(xpwg.plotsperpage.val) mod nx) ne 0)
      ddx=float(!d.x_size)/nx & ddy=ddx/arplot
      if ddy ge float(!d.y_size)/ny then begin
        ddy=float(!d.y_size)/ny & ddx=ddy*arplot
      endif
      area=xpwg.plotsperpage.val*ddx*ddy/(!d.x_size*!d.y_size)
      if area ge aold then begin
        nnx=nx & nny=ny & aold=area
      endif
    endfor
    !p.multi=[0,nnx,nny]
  endif
    
  for ii=0,n_elements(ixy)-1 do begin
    ix=fix(ixy(ii)) mod fix(xfwg.nx.val)
    iy=fix(ixy(ii))  /  fix(xfwg.nx.val)
    if ii ge 1 and xpwg.multipage.val eq 1 and !d.name eq 'PS' and $
      (!p.multi[0] eq 0 or (ii mod fix(xpwg.plotsperpage.val)) eq 0) then begin
      erase
      !p.multi[0]=0
    endif
    ip=xfwg.p(ix,iy).par.val
    xrange=[xsel(0),xsel(1)+1]+fits.xoff
    yrange=[ysel(0),ysel(1)+1]+fits.yoff    
    data=pudata(ip,ix,iy,xsel,ysel,valid,parname,title=title)
    pi=get_parinfo(parname,fits.code,xy=[ix,iy])
    data=data*pi.scaling
    if title ne '' then pi.longname=title
    
                                ;plot conditions
    ncond=n_elements(xfwg.p[0].cond)
    szd=size(data,/dim)
    if n_elements(szd) eq 1 then szd=[szd,1]
    condvalid=bytarr(ncond,szd[0],szd[1])+1b
    conddata=fltarr(ncond,szd[0],szd[1])+1b
    condtitle=strarr(ncond)
    condparname=strarr(ncond)
    condadd=strarr(ncond)
    for ic=0,ncond-1 do if xfwg.p(ix,iy).cond[ic].par ge 0 then begin
      if total(abs(xfwg.p(ix,iy).cond[ic].mm)) gt 0 then begin
        conddata[ic,*,*]= $
          pudata(xfwg.p(ix,iy).cond[ic].par,ix,iy,xsel,ysel, $
                 cval,cpn,condition=ic,shorttitle=cst)
        condparname[ic]=cpn
        condtitle[ic]=cst
        condvalid[ic,*,*]= $
          cval and (conddata[ic,*,*] ge min(xfwg.p(ix,iy).cond[ic].mm) and $
                    conddata[ic,*,*] le max(xfwg.p(ix,iy).cond[ic].mm))
        cpi=get_parinfo(condparname[ic],fits.code)
        conddata[ic,*,*]=conddata[ic,*,*]*cpi.scaling
        condadd[ic]=' ('+xfwg.p(ix,iy).cond[ic].layer+ $
          ': '+n2s(min(xfwg.p(ix,iy).cond[ic].mm),format='(f15.2)')+ $
          ' <= '+condtitle[ic]+' <= '+ $
                    n2s(max(xfwg.p(ix,iy).cond[ic].mm),format='(f15.2)')+')'
      endif
    endif
    kadd=strjoin(condadd,'!C')
    if xfwg.p(ix,iy).condspline eq 1 then if n_elements(xss) ge 0 then $
      if xss.n ge 1 then begin
      if xss.n ge 2 then $
        spval=total(xss.par[0:xss.n-1].flag ne 0,3) ne 0 $
      else $
        spval=xss.par[0].flag ne 0
      for ic=0,ncond-1 do condvalid[ic,*,*]= $
        condvalid[ic,*,*] and spval[xsel[0]:xsel[1],ysel[0]:ysel[1]]
      kadd=kadd+'  (inside spline)'
    endif
    
    cadd=''
    if xfwg.p(ix,iy).contpar.index ge 0 then begin
      cdata=pudata(xfwg.p(ix,iy).contpar.index,ix,iy,xsel,ysel, $
                   cval,cparname,/contour,shorttitle=ctitle)
      if (xfwg.p(ix,iy).contpar.smooth ge 2 and $
          n_elements(cdata) gt xfwg.p(ix,iy).contpar.smooth) then begin
        cdata=smooth(cdata,xfwg.p(ix,iy).contpar.smooth,/nan,/edge_truncate)
      endif
      if ctitle ne '' then cadd=', Cont: '+ctitle
      cdata=apply_image_filter(cdata,[ix,iy],mode=3)
    endif
    
    nv=where(valid eq 0 or condvalid[0,*,*] eq 0) ;0 is map condition
    if nv(0) ne -1 then data(nv)=!values.f_nan
                                ;interpolate NAN
    if xpwg.flagval(0) eq 1 then begin
      if min(finite(data)) eq 0 then data=interpol_nan(data)
    endif
    if xpwg.smooth.val ge 2 then data=smooth(data,xpwg.smooth.val,/nan,/edge)
    data=apply_image_filter(data,[ix,iy],mode=xfwg.p(ix,iy).plotmode)
    if xfwg.p(ix,iy).azipar.ambi eq 1 then begin
;      data=((data+540+90) mod 180) -90 
      data=(data+360 + 90) mod 180 -90
    endif
    
    zrange=minmaxp(data,/nan,perc=99.9)
    if pi.symm eq 1 then zrange=[-1,1]*max(abs(zrange))
    if min(xfwg.p(ix,iy).range eq 0) eq 0 then zrange=xfwg.p(ix,iy).range
    
    if xfwg.p(ix,iy).zlog eq 1 then begin
      data=alog10(data)
      zrange=alog10(zrange)
      zlogadd='!X10!Nlog '
    endif else zlogadd=''
    xfwg.p(ix,iy).range_current=zrange
      
    if xfwg.p(ix,iy).coltab.val ge 1 then begin
      userlct,/nologo,coltab=0,/full,/reverse,verbose=0
      pi.colortable=(['','stokes','azi','velo','','stokes','azi','velo']) $
                    (xfwg.p(ix,iy).coltab.val-1)
      pi.colortablerev=([0,0,0,0,1,1,1,1])(xfwg.p(ix,iy).coltab.val-1)
    endif
    xfits_setcoltab,pi,zrange
    if xfwg.p[ix,iy].shorttitle eq 0 then begin
      title=pi.longname+cadd    ;fits.long(ip)
    endif else begin
      if ip lt fits.npar then title=fits.short[ip] $
      else title=pi.longname
    endelse

    xytitle=(['Pixel','Arcsec'])(xpwg.arcsec.val eq 1)
    
    xrgscale=scaleaxis(xrange,/x)
    yrgscale=scaleaxis(yrange,/y)
    aspect=xfwg.p(ix,iy).plotmode eq 0
    
    xrs=xrgscale & yrs=yrgscale
    xt=xytitle & yt=xytitle+kadd
    if xpwg.flagval[4] then data=reverse(data,2)
    if xpwg.flagval[5] then data=reverse(data,1)
    if xpwg.flagval[6] then begin
      data=rotate(data,1)
      dummy=xrs & xrs=yrs & yrs=dummy
      dummy=xt & xt=yt & yt=dummy
    endif
    
    image_cont_al,data,zrange=zrange,title=title,contour=0, $
      aspect=aspect,cut=aspect,ztitle=zlogadd+pi.unit,retpos=pos,exact=1, $
      integerzoom=xpwg.flagval(3) eq 1, $
      xrange=xrs,yrange=yrs,xtitle=xt,ytitle=yt, $
      onlypos=xfwg.p(ix,iy).plotmode ne 0,label=1
    if (xpwg.multipage.val eq 0) then $
          dpos=pos $
    else dpos=[!d.x_size*0.15,!d.y_size*0.1, $
               !d.x_size*0.95,!d.y_size*0.9]
;dpos=[!d.x_size*0.15,pos(1),!d.x_size*0.97,pos(3)]    
                                ;store position of plot
    posframe=[ix*!d.x_size/xfwg.nx.val,iy*!d.y_size/xfwg.ny.val, $
              (ix+1)*!d.x_size/xfwg.nx.val,(iy+1)*!d.y_size/xfwg.ny.val]
    case xfwg.p(ix,iy).plotmode of
      0: begin                  ;map mode               
        
                                ;draw contour
        if xfwg.p(ix,iy).contpar.index ge 0 then begin
                                ;set invalid data to NAN
          nv=where(cval eq 0 or condvalid[1,*,*] eq 0)
          if nv(0) ne -1 then cdata(nv)=!values.f_nan
          clevels=strsplit(xfwg.p(ix,iy).contpar.val,',',/extract)
          nlevels=n_elements(clevels)
          if clevels(0) eq '' then begin
            dummy=temporary(nlevels)
            dummy=temporary(clevels)
          endif else clevels=float(clevels(sort(float(clevels))))
          szc=size(cdata)
          xvec=(findgen(szc(1))+.5)/(szc(1))* $
            (xrgscale(1)-xrgscale(0))+xrgscale(0)
          yvec=(findgen(szc(2))+.5)/(szc(2))* $
            (yrgscale(1)-yrgscale(0))+yrgscale(0)
          xarr=(intarr(szc(2))+1) ## xvec
          yarr=yvec ## (intarr(szc(1))+1)
          
          if xpwg.flagval[4] then cdata=reverse(cdata,2)
          if xpwg.flagval[5] then cdata=reverse(cdata,1)
          if xpwg.flagval[6] then cdata=rotate(cdata,1)
          userlct,/nologo,coltab=0,verbose=0
          contour,cdata,xarr,yarr,xst=5,yst=5,/noerase,position=pos, $
                  /device,c_labels=intarr(32)+1, $
                  c_thick=xfwg.p(ix,iy).contpar.thick, $
                  c_color=xfwg.p(ix,iy).contpar.col, $
                  c_charsize=0.7,nlevels=nlevels,levels=clevels, $
                  zrange=minmaxp(cdata,/nan,perc=99.9), $
                  xrange=xrs,yrange=yrs
        endif
        
                                ;draw azimuth vectors
        if xfwg.p(ix,iy).azipar.flag eq 1 then begin
          iazi=where(fits.par eq 'AZIMU' and $
                     fits.comp eq xfwg.p(ix,iy).azipar.comp)
          ibf=where(fits.par eq 'BFIEL' and $
                     fits.comp eq xfwg.p(ix,iy).azipar.comp)
          iinc=where(fits.par eq 'GAMMA' and $
                     fits.comp eq xfwg.p(ix,iy).azipar.comp)
          
          ;; if n_elements(iazi) ge 2 then begin
          ;;   print,'multiple azimuth maps for comp.', $
          ;;         xfwg.p(ix,iy).azipar.comp, ' detected. Using first one.'
          ;; endif
          iazi=iazi[0]
          ibf=ibf[0]
          iinc=iinc[0]
          if iazi ne -1 then begin
            adata=pudata(iazi,ix,iy,xsel,ysel,condition=2, $
                         aval,aparname,shorttitle=atitle)
            
            if (xfwg.p(ix,iy).contpar.smooth ge 2 and $
                n_elements(adata) gt xfwg.p(ix,iy).contpar.smooth) then begin
              adata=smooth(adata,xfwg.p(ix,iy).contpar.smooth,/nan, $
                           /edge_truncate)
            endif
            adata=apply_image_filter(adata,[ix,iy],mode=3)
            if xpwg.flagval[4] then adata=reverse(adata,2)
            if xpwg.flagval[5] then adata=reverse(adata,1)
            if xpwg.flagval[6] then adata=rotate(adata,1)
            bdata=pudata(ibf,ix,iy,xsel,ysel,condition=2, $
                         bval,bparname,shorttitle=btitle)
            if (xfwg.p(ix,iy).contpar.smooth ge 2 and $
                n_elements(bdata) gt xfwg.p(ix,iy).contpar.smooth) then begin
              bdata=smooth(bdata,xfwg.p(ix,iy).contpar.smooth,/nan, $
                           /edge_truncate)
            endif
            bdata=apply_image_filter(bdata,[ix,iy],mode=3)
            if xpwg.flagval[4] then bdata=reverse(bdata,2)
            if xpwg.flagval[5] then bdata=reverse(bdata,1)
            if xpwg.flagval[6] then bdata=rotate(bdata,1)
            idata=pudata(iinc,ix,iy,xsel,ysel,condition=2, $
                         ival,iparname,shorttitle=ititle)
            if (xfwg.p(ix,iy).contpar.smooth ge 2 and $
                n_elements(idata) gt xfwg.p(ix,iy).contpar.smooth) then begin
              idata=smooth(idata,xfwg.p(ix,iy).contpar.smooth,/nan, $
                           /edge_truncate)
            endif
            idata=apply_image_filter(idata,[ix,iy],mode=3)
            if xpwg.flagval[4] then idata=reverse(idata,2)
            if xpwg.flagval[5] then idata=reverse(idata,1)
            if xpwg.flagval[6] then idata=rotate(idata,1)
            
            case xfwg.p(ix,iy).azipar.scale of 
              1: lendata=bdata/(minmaxp(abs(bdata),perc=99))[1]
              2: lendata=bdata/(minmaxp(abs(bdata*cos(idata*!dtor)),perc=99))[1]
              else: lendata=adata*0+1.
            endcase
                                ;set invalid data to NAN
            nv=where(aval eq 0 or condvalid[2,*,*] eq 0) 
            if nv(0) ne -1 then lendata(nv)=!values.f_nan
            
            sza=size(adata,/dim)
            nelx=sza[0]/(xfwg.p(ix,iy).azipar.bin>1)
            nely=sza[1]/(xfwg.p(ix,iy).azipar.bin>1)
            adata=adata+xfwg.azi.offset
            if xfwg.azi.rotsens eq 1 then adata=-adata
            azigrid=avgbox(adata,nx=nelx,ny=nely,cycle=[-90.,90])
            xvec=(findgen(sza(0))+.5)/(sza(0))* $
                 (xrgscale(1)-xrgscale(0))+xrgscale(0)
            yvec=(findgen(sza(1))+.5)/(sza(1))* $
                 (yrgscale(1)-yrgscale(0))+yrgscale(0)
            xgrid=congrid(xvec,nelx,/interp)
            ygrid=congrid(yvec,nely,/interp)
            lengrid=congrid(lendata,nelx,nely)
            lengrid=lengrid*xfwg.p(ix,iy).azipar.length
            userlct,/nologo,coltab=0,verbose=0
            velovect2,/overplot,color=xfwg.p(ix,iy).azipar.color, $
                      length=lengrid,cos(azigrid*!dtor),sin(azigrid*!dtor), $
                      xgrid,ygrid,thick=xfwg.p(ix,iy).azipar.thick, $
                      arrowsize=0.,noclip=0
          endif ; else begin
;            print,'no azimuth map for comp.', $
;                  xfwg.p(ix,iy).azipar.comp, ' detected.'
;          endelse 
        endif
        
        if xpwg.click.val eq 4 then xss_splineplot
      end
      1: begin                  ;histo mode
        hdata=histogram(data,min=zrange(0),max=zrange(1),/nan, $
                        nbins=xfwg.p(ix,iy).histo.nbins>1,loc=xhisto)
        ztitle='number of pixels'
        if xfwg.p(ix,iy).histo.norm eq 1 then begin
          hdata=hdata/total(hdata,/nan)*100
          ztitle='normalized occurence [%]'
        endif          
        yrg=[0,max(hdata)*1.1]
        xrg=xfwg.p(ix,iy).histo.xrange
        if abs(xrg(1)-xrg(0)) le 1e-5 then xrg=zrange
        plot,xhisto,hdata,psym=10,yrange=yrg,xrange=xrg, $
          xtitle=pi.unit,title=title,/xst,/yst,$;,/noerase, $
          ytitle=ztitle,/device;,position=dpos
      end
      2: begin                  ;scatter plot
        sdata=pudata(xfwg.p(ix,iy).scatter.par,ix,iy,xsel,ysel, $
                     sval,sparname,title=stitle,/scatter,six=six,siy=siy)
        if xpwg.flagval[4] then sdata=reverse(sdata,2)
        if xpwg.flagval[5] then sdata=reverse(sdata,1)
        if xpwg.flagval[6] then sdata=rotate(sdata,1)
        
        sdata=apply_image_filter(sdata,[six,siy],mode=2)
        spi=get_parinfo(sparname,fits.code,xy=[ix,iy])
        if stitle ne '' then spi.longname=stitle
        sdata=sdata*spi.scaling
        nv=where(sval eq 0)    ;set invalid data to NAN
        if nv(0) ne -1 then sdata(nv)=!values.f_nan
        if xpwg.smooth.val ge 2 then $
          sdata=smooth(sdata,xpwg.smooth.val,/nan,/edge)
        xrg=xfwg.p(ix,iy).scatter.xrange
        if abs(xrg(1)-xrg(0)) le 1e-5 then xrg=zrange
        yrg=xfwg.p(ix,iy).scatter.yrange
        if abs(yrg(1)-yrg(0)) le 1e-5 then yrg=minmax(sdata,/nan)
        if xfwg.p(ix,iy).scatter.color eq 0 then begin
          plot,data,sdata,/xst,/yst,xrange=xrg,yrange=yrg,psym=3,$;,/noerase, $
               xtitle=pi.longname+' '+pi.unit, $
               ytitle=spi.longname+' '+spi.unit, $
               xtype=xfwg.p(ix,iy).scatter.xlog, $
               ytype=xfwg.p(ix,iy).scatter.ylog, $
            /device;,position=dpos
        endif else begin
          dpos(2)=dpos(2)+(dpos(2)-dpos(0))*(1/(1-0.15)-1)
          nx=xfwg.p(ix,iy).scatter.nbins
          if nx lt 2 then nx=fix(256)
          ny=fix(nx*(dpos(3)-dpos(1))/(dpos(2)-dpos(0)))>2
          xyarr=fltarr(nx,ny)
          szd=size(data,/dim)
          xlogadd='' & ylogadd=''
          if xfwg.p(ix,iy).scatter.xlog eq 1 then begin
            xrg=alog10(xrg)
            data=alog10(data)
            xlogadd=' (log10)'
          endif
          if xfwg.p(ix,iy).scatter.ylog eq 1 then begin
            yrg=alog10(yrg)
            sdata=alog10(sdata)
            ylogadd=' (log10)'
          endif
          xidx=( data-xrg(0))/(xrg(1)-xrg(0))*nx
          yidx=(sdata-yrg(0))/(yrg(1)-yrg(0))*ny
          inrg=where(xidx ge 0 and xidx lt nx and $
                     yidx ge 0 and yidx lt ny)
          if inrg(0) ne -1 then for il=0l,n_elements(inrg)-1 do $
            xyarr(xidx(inrg(il)),yidx(inrg(il)))= $
            xyarr(xidx(inrg(il)),yidx(inrg(il)))+1
          iszero=where(xyarr eq 0)
          if iszero(0) ne -1 then xyarr(iszero)=!values.f_nan
          ztitle='number'
          if xfwg.p(ix,iy).scatter.hnorm eq 1 then begin
            xyarr=xyarr/total(xyarr,/nan)*100
            ztitle='normalized occurence [%]'
          endif          
          zrg=xfwg.p(ix,iy).scatter.zrange
          if abs(zrg(1)-zrg(0)) le 1e-5 then zrg=minmax(xyarr,/nan)
          userlct,/full,verbose=0
          image_cont_al,contour=0,xyarr,xrange=xrg,yrange=yrg, $
            xtitle=pi.longname+' '+pi.unit+xlogadd, $
            ytitle=spi.longname+' '+spi.unit+ylogadd, $
            zrange=zrg,ztitle=ztitle,$;/aspect,/cut, $
                        /exact,label=1,/device ;,position=dpos ;,/noerase
        endelse
      end
      else:
    end
    
    xfwg.p(ix,iy).posdev=pos       
    xfwg.p(ix,iy).posframe=posframe
    xfwg.p(ix,iy).xrg=!x.crange
    xfwg.p(ix,iy).yrg=!y.crange
    xfwg.xy_aspect=(xfwg.p(0,0).posframe(2)-xfwg.p(0,0).posframe(0))/$
                   (xfwg.p(0,0).posdev(2)-xfwg.p(0,0).posdev(0))
    mom=moment(data,/nan)
    xfwg.p[ix,iy].info.mean=mom[0]
    xfwg.p[ix,iy].info.contrast=sqrt(mom[1])*100/abs(mom[0])
  endfor
                                ;draw arrows for easy navigation
  if !d.name ne 'PS' then begin
    len=10.
    xa=[len,!d.x_size/2.,!d.x_size-len,!d.x_size/2.]
    ya=[!d.y_size/2.,!d.y_size-len,!d.y_size/2.,len]
    xd=[-1,0,1,0]
    yd=[0,1,0,-1]
    totrgx=(xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).xrg[0]- $
            xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).xrg[1])^2
    totrgy=(xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).yrg[0]- $
            xfwg.p(0:xfwg.nx.val-1,0:xfwg.ny.val-1).yrg[1])^2
    ixy=where(totrgx ge 1e-5 and totrgy ge 1e-5)
    if ixy[0] ne -1 then begin
      xpx=(reform(xfwg.p[0:xfwg.nx.val-1,0:xfwg.ny.val-1].xrg,2, $
                 xfwg.nx.val*xfwg.ny.val))[*,ixy]
      xpy=(reform(xfwg.p[0:xfwg.nx.val-1,0:xfwg.ny.val-1].yrg,2, $
                 xfwg.nx.val*xfwg.ny.val))[*,ixy]
      for i=0,3 do begin
      case i of
        0: af=min(xpx) gt fits.xoff
        1: af=max(xpy) lt fits.yoff+fits.ny
        2: af=max(xpx) lt fits.xoff+fits.nx
        3: af=min(xpy) gt fits.yoff
      endcase
      if af then $
        arrow,xa(i),ya(i),xa(i)+len*xd(i),ya(i)+len*yd(i),thick=2,hsize=len, $
        color=3,/solid
    endfor
    endif
  endif
  if keyword_set(ps) then psset,/close
end
  
