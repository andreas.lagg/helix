; $Id: image_cont.pro,v 1.9 2003/02/03 18:13:17 scottm Exp $
;
; Copyright (c) 1988-2003, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.

pro image_cont2, aorig,xorig,yorig, $
                 WINDOW_SCALE = window_scale, ASPECT = aspect, $
                 INTERP = interp,contour=cont, $
                 xrange=xrg_orig,yrange=yrg_orig,zrange=zrg_orig, $
                 xtitle=xtitle,ytitle=ytitle,title=title,ztitle=ztitle, $
                 _extra=_extra,zlog=zlog,only_colorbar=only_colorbar, $
                 zticks=zticks,ztickv=ztickv,color_range=color_range, $
                 transparent=transparent,position=position,label=label, $
                 nvcolor=nvcolor
print,'Please use image_cont_al.pro. The image_cont2.pro is a SSW routine.'  
;+
; NAME:
;	IMAGE_CONT
;
; PURPOSE:
;	Overlay an image and a contour plot.
;
; CATEGORY:
;	General graphics.
;
; CALLING SEQUENCE:
;	IMAGE_CONT, A
;
; INPUTS:
;	A:	The two-dimensional array to display.
;
; KEYWORD PARAMETERS:
; WINDOW_SCALE:	Set this keyword to scale the window size to the image size.
;		Otherwise, the image size is scaled to the window size.
;		This keyword is ignored when outputting to devices with 
;		scalable pixels (e.g., PostScript).
;
;	ASPECT:	Set this keyword to retain the image's aspect ratio.
;		Square pixels are assumed.  If WINDOW_SCALE is set, the 
;		aspect ratio is automatically retained.
;
;	INTERP:	If this keyword is set, bilinear interpolation is used if 
;		the image is resized.
;
; Keywords added by A. Lagg:
; XRANGE: 2-el vector giving range for x-axis
; YRANGE: 2-el vector giving range for y-axis
; ZRANGE: 2-el vector giving range for z-axis
; CONTOUR: flag for drawing contour levels (default=yes)
; XTITLE: xtitle
; XTITLE: ytitle
; TITLE: title
;  
; OUTPUTS:
;	No explicit outputs.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	The currently selected display is affected.
;
; RESTRICTIONS:
;	None.
;
; PROCEDURE:
;	If the device has scalable pixels, then the image is written over
;	the plot window.
;
; MODIFICATION HISTORY:
;	DMS, May, 1988.
;-
  
  if n_elements(aorig) eq 0 then begin
    message,/cont,'No array specified.'
    return
  endif
  
  if n_elements(label) eq 0 then label=1
  
                                ;do scaling if zrange is present
  a=aorig
  nan=where(finite(aorig) eq 0)
  
  if n_elements(xrg_orig) eq 2 then xrange=xrg_orig
  if n_elements(yrg_orig) eq 2 then yrange=yrg_orig
  if n_elements(zrg_orig) eq 2 then zrange=zrg_orig
  if n_elements(zrange) ne 2 then zrange=[min(aorig,/nan),max(aorig,/nan)]
  if n_elements(color_range) ne 2 then color_range=[1,254]
  cr=color_range
  
  zrflag=0
  if n_elements(zrange) eq 2 then if max(zrange) ne min(zrange) then begin
    a=(((aorig-zrange(0))/float(zrange(1)-zrange(0)))<1)>0
;    a=bytscl(a)
    a=((a*(cr(1)-cr(0))+cr(0))>cr(0))<cr(1)
                                ;keep NAN values, set to BG-color
    nf=where(finite(aorig) eq 0)
    if nf(0) ne -1 then begin
      if n_elements(nvcolor) eq 0 then a(nf)=!p.background else a(nf)=nvcolor
    endif
    zrflag=1
  endif
  
  on_error,2                    ;Return to caller if an error occurs
  sz = size(a)              
  if sz[0] lt 2 then message, 'Parameter not 2D'

  
  if n_params() eq 2 then message,'You must also specify the y coordinate.'
  
  if n_params() eq 3 then begin
    xarr=xorig 
    yarr=yorig 
    xcont=xorig
    ycont=yorig
    ok=sz(1:2) eq (size(xarr))(1:2) and sz(1:2) eq (size(yarr))(1:2)
    if min(ok) eq 0 then message,'X and Y must have same dimension as image.'
    
    
    if n_elements(xrange) ne 2 then xrange=[min(xarr),max(xarr)]
    if n_elements(yrange) ne 2 then yrange=[min(yarr),max(yarr)]
    
    
                                ;get new image
    dx=min(abs(xarr(1:*,0)-xarr(0:sz(1)-2,0)))
    dy=min(abs(yarr(0,1:*)-yarr(0,0:sz(2)-2)))
    angle=atan(yarr(1)-yarr(0),xarr(1)-xarr(0))
    ddx=dx*cos(angle) + dy*sin(angle)
    ddy=-dx*sin(angle) + dy*cos(angle)
    gs=abs([ddx,ddy])
    nx=(abs((xrange(1)-xrange(0))/ddx)<(sz(1)*sqrt(2.)))<1024
    ny=(abs((yrange(1)-yrange(0))/ddy)<(sz(2)*sqrt(2.)))<1024
    arot=tri_surf_store(a,xarr,yarr,nx=nx,ny=ny,missing=!values.f_nan,/linear)
    
                                ;select image inside x/y range
    minx=min(xarr,/nan,max=maxx)
    miny=min(yarr,/nan,max=maxy)
    xn=findgen(nx)/(nx-1)*(maxx-minx)+minx
    yn=findgen(ny)/(ny-1)*(maxy-miny)+miny
    inx=where(xn ge min(xrange) and xn le max(xrange))
    iny=where(yn ge min(yrange) and yn le max(yrange))
    if inx(0) ne -1 and iny(0) ne -1 then begin
      arot=arot(min(inx):max(inx),min(iny):max(iny))
      xarr=(fltarr(n_elements(iny))+1) ## xn(inx)
      yarr=yn(iny) ## (fltarr(n_elements(inx))+1)
    endif else arot(*)=!values.f_nan
    a=arot
    sz=size(a)
  endif  else begin
    if n_elements(xrange) ne 2 then xrange=[0,sz(1)]
    if n_elements(yrange) ne 2 then yrange=[0,sz(2)]
    xarr=((fltarr(sz(2))+1) ## $
          (findgen(sz(1))/(sz(1)-1)*(xrange(1)-xrange(0))+xrange(0)))
    yarr=((findgen(sz(2))/(sz(2)-1)*(yrange(1)-yrange(0))+yrange(0)) ## $
          (fltarr(sz(1))+1))
    xcont=xarr
    ycont=yarr
  endelse
  
  contour,[[0,0],[1,1]],/nodata, xstyle=5, ystyle = 5,_extra=_extra, $
    position=position
  
  px = !x.window * !d.x_vsize   ;Get size of window in device units
  py = !y.window * !d.y_vsize
  
                                ;subtract 10% for zrange
  pxo=px
  if zrflag and label then begin
    px(1)=pxo(1)-0.15*(pxo(1)-pxo(0))
    pxo(1)=pxo(1)-0.1*(pxo(1)-pxo(0))
  endif
  
                                ;change x and y range to maintain
                                ;aspect ratio
  if keyword_set(aspect) then begin
    ddx=(px(1)-px(0))/(max(xrange)-min(xrange))
    ddy=(py(1)-py(0))/(max(yrange)-min(yrange))
    if ddy gt ddx then yrange=total(yrange)/2.+[-.5,.5]/ddx*(py(1)-py(0))
    if ddx gt ddy then xrange=total(xrange)/2.+[-.5,.5]/ddy*(px(1)-px(0))
  endif
  

  swx = px[1]-px[0]             ;Size in x in device units
  swy = py[1]-py[0]             ;Size in Y
  six = float(sz[1])            ;Image sizes
  siy = float(sz[2])
  mmx=minmaxp(xarr,/nan)
  if xrange(1) lt xrange(0) then mmx=reverse(mmx)
  offx=(mmx(0)-xrange(0))/(xrange(1)-xrange(0))*swx
  mmy=minmaxp(yarr,/nan)
  if yrange(1) lt yrange(0) then mmy=reverse(mmy)
  offy=(mmy(0)-yrange(0))/(yrange(1)-yrange(0))*swy
  maxx=(mmx(1)-xrange(0))/(xrange(1)-xrange(0))*swx
  maxy=(mmy(1)-yrange(0))/(yrange(1)-yrange(0))*swy
  pxi=[px(0)+offx,px(0)+maxx]
  pyi=[py(0)+offy,py(0)+maxy]
  swxi = (pxi[1]-pxi[0])>1      ;Size in x in device units
  swyi = (pyi[1]-pyi[0])>1      ;Size in Y
stop  
                                ;Image aspect ratio
;   if n_elements(xrange) eq 2 and n_elements(yrange) eq 2 then $
;     aspi=float(xrange(1)-xrange(0))/(yrange(1)-yrange(0)) $
;   else  aspi = six / siy
;   aspw = swx / swy              ;Window aspect ratio
;   f = aspi / aspw               ;Ratio of aspect ratios

                                ;use the right 10% for displaying the
                                ;zrange
  if zrflag and label then begin
    zswy = swy
    zswx = swx
;     if keyword_set(aspect) then begin	;Retain aspect ratio?
;                                 ;Adjust window size
;       if f ge 1.0 then zswy = swy / f else zswx = swx * f
;     endif

                                ;make array for zrange
    szx=2. & szy=512.
    zarr=(findgen(szy)/(szy-1.)*(cr(1)-cr(0))+cr(0)) ## (fltarr(szx)+1)
    swx1=(pxo(1)-px(1))
    if (!d.flags and 1) ne 0 then begin	;Scalable pixels?
      tv,zarr,px[1],py[0],xsize = swx1, ysize = zswy, /device
    endif else begin
      tv,poly_2d(zarr,$         ;Have to resample image
                  [[0,0],[szx/swx1,0]], [[0,szy/zswy],[0,0]],$
                  keyword_set(interp),swx1+1,zswy), $
         px[1],py[0]
    endelse
    plot,/noerase,zrange,/nodata,xrange=[0,1],/yst,/xst,xticks=1, $
      xtickname=strarr(3)+' ',yrange=zrange, $
      pos = [px[1],py[0], pxo[1],py[0]+zswy],/dev, $
      ytickname=strarr(32)+' '
    zax=1
    if n_elements(zticks) ne 0 then zax=zticks ge 1
    if zax then begin
      if n_elements(_extra) ne 0 then begin
        ex=_extra
        if max(tag_names(ex) eq 'YTICKNAME') eq 1 then ex.ytickname=''
      endif
      axis,yax=1,1.,/yst,yrange=zrange,ytitle=ztitle,yticks=zticks, $
        ytickv=ztickv,ytickname=ytn,_extra=ex
    endif
  endif
  
  if keyword_set(only_colorbar) then return
  
                                ;reset ISO flag
  if n_elements(_extra) ne 0 then begin
    tn=tag_names(_extra)
    iiso=where(strpos(tn,'ISO') ne -1)
    if iiso(0) ne -1 then _extra.(iiso(0))=0
    ind=where(strpos(tn,'NODATA') ne -1)
    if ind(0) ne -1 then nodata=_extra.(ind(0))   
  endif
  

  
                                ;keep NAN values, set to BG-color
;   fi=where(finite(a))
;   if fi(0) ne -1 then a(fi)=(a(fi)>1)<cr(1)
;   nf=where(finite(a) eq 0)
;   if nf(0) ne -1 then a(nf)=!p.background
  if keyword_set(nodata) eq 0 then begin
    if nan(0) eq -1 or keyword_set(transparent) eq 0 then begin
      if (!d.flags and 1) ne 0 then begin	;Scalable pixels?
        pxi=[pxi[1]-swxi,pxi[1]]
        tv,((a)>cr(0))<cr(1),pxi[0],pyi[0],xsize = swxi, ysize = swyi, /device

      endif else begin          ;Not scalable pixels	
        if keyword_set(window_scale) then begin ;Scale window to image?
          pxi=[pxi[1]-six,pxi[1]]
          tv,a,pxi[0],pyi[0]    ;Output image
        endif else begin        ;Scale window
           pxi=[pxi[1]-swxi,pxi[1]]
                                 ;Have to resample image
           ap=poly_2d((a),[[0,0],[six/swxi,0]], [[0,siy/swyi],[0,0]],$
                      keyword_set(interp),swxi,swyi)
           tv,((ap)>cr(0))<cr(1),pxi[0],pyi[0]
        endelse                 ;window_scale
      endelse                   ;scalable pixels
    endif else begin            ;overplot valid pixels only
      a=float(a) & a(nan)=!values.f_nan
      for ix=0,sz(1)-1 do for iy=0,sz(2)-1 do if finite(a(ix,iy)) then begin
                                ;make it faster + ps file smaller:
                                ;check if iy+1 ... is
                                ;also finite
        iyadd=0
        ende=0
        repeat begin
          iyadd=iyadd+1
          if iy+iyadd eq sz(2) then ende=1 $
          else ende=finite(a(ix,iy+iyadd)) eq 0
        endrep until ende
        iyadd=iyadd-1        
        if iyadd eq 0 then ap=a(ix,iy)+intarr(2,2) $
        else ap=a(ix,iy:iy+iyadd) ## (intarr(2)+1)
        szx=round(swxi/six)>2
        szy=round(swyi*(iyadd+1)/siy)>2        
        if (!d.flags and 1) ne 0 then begin	;Scalable pixels?
          tv,ap,pxi(0)+ix*swxi/six,pyi(0)+iy*swyi/siy, $
            xsize=szx,ysize=szy,/device
        endif else begin
          ap=poly_2d(ap,[[0,0],[1,0]], [[0,iyadd+1],[0,0]], $
                     keyword_set(interp),szx,szy)
          tv,ap,pxi(0)+ix*swxi/six,pyi(0)+iy*swyi/siy,/device
       endelse
         iy=iy+iyadd
       endif
    endelse
  endif
  
  mx = !d.n_colors-1            ;Brightest color
  colors = [mx,mx,mx,0,0,0]     ;color vectors
  if !d.name eq 'PS' then colors = mx - colors ;invert line colors for pstscrp
  
  if n_elements(cont) eq 0 then cont=1

  contour,aorig,xcont,ycont,/noerase,/xst,/yst,/zst,$ ;Do the contour
    pos = [px[0],py[0], px[0]+swx,py[0]+swy],/dev,$
    c_color =  colors, $
    xrange=xrange,yrange=yrange,nodata=(cont eq 0) or keyword_set(nodata), $
    xtitle=xtitle,ytitle=ytitle,title=title,_extra=_extra
  
  return
end
