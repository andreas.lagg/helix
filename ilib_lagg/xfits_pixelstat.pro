pro xfits_pixelstat,xyin,ixy
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common pixelstat,psst
  
  xy=xyin-[fits.xoff,fits.yoff]
  if min(xy) lt 0 or xy(0) ge fits.nx or xy(1) ge fits.ny then return
  if max(fits.pixrep) lt 1 then return
  if n_elements(psst) eq 0 then begin
    subst={id:0l,val:0.,str:''}
    psst={base:subst,text:subst,draw:subst,xysz:intarr(2)}
    new_wg=1
  endif else begin
    widget_control,psst.base.id,bad_id=bad_id
    new_wg=psst.base.id eq 0 or bad_id ne 0
  endelse
  
  pixstat='fill in'
  if new_wg then begin
    screen=get_screen_size()
    geom=widget_info(/geometry,xfwg.base.id)
    psst.base.id=widget_base(group_leader=xpwg.base.id,col=1, $
                             title='Pixel Statistics')
    psst.text.id=widget_text(psst.base.id,value=pixstat,ysize=2)
    psst.draw.id=widget_draw(psst.base.id, $
                             xsize=screen[0]*.3,ysize=screen[0]*.3*0.75)
    widget_control,psst.base.id,/realize
  endif  
  
  pold=!p
  widget_control,psst.draw.id,get_value=windex
  if !d.name ne 'PS' then wset,windex
  !p.multi=0
  !p.position=0
  !p.charsize=xpwg.charsize.val
  
                                ;
  ip=xfwg.p(ixy(0),ixy(1)).par.val
  iall=where(fits.par eq fits.par[ip])
  ipr=where(fits.par eq fits.par[ip] and strpos(fits.short,' PR') ne -1)
  ifit=where(fits.par eq 'FITNESS' and strpos(fits.short,' PR') ne -1)
  data=reform(fits.data[xy[0],xy[1],ipr])
  pi=get_parinfo(fits.par[ip],fits.code,xy=ixy)
  data=data*pi.scaling
  rg=xfwg.p[ixy[0],ixy[1]].range_current
  if abs(rg[0]) le 1e-4 and  abs(rg[1]) le 1e-4 then rg=minmaxp(data,perc=99.9)
  nhist=30
  hdata=histogram(data,min=rg(0),max=rg(1),/nan, $
                  nbins=nhist,loc=xhisto)
  plot,xhisto,hdata,psym=10,/yst,/xst,title='Pixel Satistics for x'+string(xyin[0],format='(i4.4)')+'x'+string(xyin[1],format='(i4.4)'), $
       xtitle=fits.par[ip],ytitle='number'
  
  mom=moment(data)
  momstr=string(mom,format='(e10.3)')
  momstr[1]=string(sqrt(mom[1]),format='(e10.3)')
  info='All values: mean = '+momstr[0]+' | sdev = '+momstr[1]+' | skew = '+momstr[2]+' | kurt = '+momstr[3]
  
  data=data[reverse(sort(fits.data[xy[0],xy[1],ifit]))]
  data50=data[0:n_elements(data)/2>1]
  mom=moment(data50)
  momstr=string(mom,format='(e10.3)')
  momstr[1]=string(sqrt(mom[1]),format='(e10.3)')
  info=[info,'Best 50%:   mean = '+momstr[0]+' | sdev = '+momstr[1]+' | skew = '+momstr[2]+' | kurt = '+momstr[3]]
  
  
  widget_control,psst.text.id,set_value=info
  
  if !d.name eq 'PS' then psset,/close
  !p=pold
  widget_control,xpwg.draw.id,get_value=windex
  wset,windex
end
