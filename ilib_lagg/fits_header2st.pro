function fits_header2st,header
                                ;special treatment of TIP header
  f8=strcompress(strupcase(strmid(header,0,8)),/remove_all)
  for i=0l,n_elements(f8)-1 do $ ;replace '-' with '_'
    f8(i)=strjoin(strsplit(f8(i),'-',/extract),'_')

  fval08=strtrim(strmid(header,8,max(strlen(header))),2)
  fval10=strtrim(strmid(header,10,21),2)
  fval68=strtrim(strmid(header,10,68),2)
  fval=fval10
  f8u=remove_multi(/no_empty,f8)
  nf8=n_elements(f8u)
  f8name=strarr(nf8)
  f8typ=intarr(nf8)
  f8nel=intarr(nf8)
  strng=''
  for i=0l,nf8-1 do begin        ;remove non alphanum characters
    uselet=bytarr(strlen(strlen(f8u(i))))
    for ip=0,strlen(f8u(i))-1 do begin
      good=bindgen(26)+65b
      if ip ge 1 then good=[good,bindgen(10)+48b,byte('_')]
      uselet(ip)=max(good eq (byte(strmid(f8u(i),ip,1)))(0))
    endfor
    uidx=where(uselet)    
    if uidx(0) ne -1 and f8u(i) ne 'END' then begin
      f8name(i)=string((byte(f8u(i)))(uidx))
      validx=where(f8 eq f8u(i))
      cmt=0
      if strmid(fval08(validx(0)),0,1) ne '=' then begin
        fval(validx)=fval08(validx)
        cmt=1
      endif
      f8nel=n_elements(validx)
      bytval=byte(fval(validx))
      
      if cmt eq 1 then typ=fval(validx) $
      else if max(['T','F'] eq fval(validx(0))) then typ=fval(validx) eq 'T' $
      else if max(bytval eq (byte(''''))(0)) then typ=fval68(validx) $
      else if max(bytval eq (byte('.'))(0)) then typ=double(fval(validx)) $
      else typ=long(fval(validx))
      if f8nel eq 1 then add='(0)' else add=''
      if cmt eq 0 then if size(typ,/type) eq 7 then begin
        typ=strmid(typ,1,strlen(typ(0))-2)
      endif
      dummy=execute('var_'+f8name(i)+'=typ'+add)
      f8i=f8name(i)
       if i gt 0 then begin
         cnt=1
         while max(f8name(0:i-1) eq f8name(i)) eq 1 do begin
           f8name(i)=f8i+n2s(cnt)
           cnt=cnt+1
         endwhile
       endif
      strng=[strng,f8name(i)+':'+'var_'+f8i]
    endif
  endfor
  
  
                                ;create header structure
  if n_elements(strng) gt 1 then $
    res=execute('hst={'+add_comma(sep=',',strng(1:*))+'}')
  
  return,hst
end

