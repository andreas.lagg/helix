function aperture_s,pnx,pny,rc
  a=dblarr(pnx,pny)
  x=(dindgen(pnx,pny) mod pnx) - pnx/2
  y=(dindgen(pnx,pny)/pnx) - pny/2
  r0=rc*(pny/2.)
  return,0.5+0.5*erf(5.0*(r0-sqrt(x*x+y*y)))
end

function otf,pupil
  ds=size(pupil,/DIMENSIONS)
  ewf=dcomplexarr(2*ds[0],2*ds[1])
  ewf[0:ds[0]-1,0:ds[1]-1]=pupil
  ewf=fft(ewf)
  return,fft(ewf*conj(ewf),/inverse)/total(pupil)
end

function psf,n,lambda,telescope_d,arcsecperpix
  rad2deg=180.0/!pi
  scale_angle2CCD=arcsecperpix/(rad2deg*3600.0)
  q_number=lambda/(scale_angle2CCD*telescope_d)
  lim_freq=double(n)/q_number
  rc=0.5*lim_freq
  nph=n/2
  pupil=aperture_s(nph,nph,rc/double(nph))

;  print,nph,lim_freq,lim_freq/double(nph)
;  tvscl,pupil
  
  s=otf(pupil)
  f=double(fft(s,/inverse))
  return,f/total(f)
end

;lambda in meters!
function grispsf,n,lambda,arcsecperpix,show=show
  fac=50.0                 ;resolution for pupil sampling (higher=better=slower)
  ext=2.                       ;zoom factor to fit pupil (should not change result)
  nph=fac*n/2
  
  telescope_d=1.5
  co=0.300                      ;central obscuration
  ws=0.02                       ;width of struds
  rad2deg=180.0/!pi
  scale_angle2CCD=arcsecperpix/(rad2deg*3600.0)
  q_number=lambda/(scale_angle2CCD*telescope_d)*ext
  lim_freq=double(2.0*nph)/q_number ; telescope_d in pupil space
  rc=lim_freq
  rco=(co/telescope_d)*rc
  sw=(ws/co)*rco
  
  pupil=aperture_s(nph,nph,rc/double(nph))
  pupilstop=aperture_s(nph,nph,rco/double(nph))
  pupil-=pupilstop

  print,sw

  for w=-2*sw,2*sw do begin
;    pupil[nph/2+w/4,nph/2:nph-1]=0.0
    pupil[nph/2+w/4,*]=0.0
    pupil[*,nph/2+w/4]=0.0
  end

;  print,nph,lim_freq,lim_freq/double(nph)
;  xpupil=dblarr(2*nph,2*nph)
;  xpupil[nph-nph/2:nph+nph/2-1,nph-nph/2:nph+nph/2-1]=pupil
  
  s=otf(pupil)
  f=shift(double(fft(s,/inverse)),nph,nph)
;
  tvscl,f,nph,0
;
  ll=fix(nph-nph/fac*ext)+1
  ur=ll+n*ext-1
;  sf=f[nph-nph/fac:nph+nph/fac-1,nph-nph/fac:nph+nph/fac-1]
  sf=f[ll:ur,ll:ur]
  sf=congrid(sf,n,n,minus_one=1,cubic=0);,/interp)
  
  if keyword_set(show) then begin
    !p.multi=[0,3,1] & userlct,/full
    image_cont_al,/aspect,/cut,contour=0,pupil,title='pupil'
    image_cont_al,/aspect,/cut,contour=0,sf, $
                  zrange=minmaxp(sf,perc=99),title='PSF'
    image_cont_al,/aspect,/cut,contour=0,alog10(sf), $
                  zrange=minmaxp(alog10(sf),perc=99),title='alog10(PSF)'
  endif
;  sf=shift(sf,nph/fac,nph/fac)

  print,max(sf,idx)
  nn=2*nph/fac
  print,idx mod nn,idx/nn
  
  return,sf/total(sf)
end

pro gris_spinor,mhd=mhd,arcsecperpix=arcsecperpix
  
  wlref=15650d-10
  
  if keyword_set(mhd) then begin
    add='_mhd'
    n=251                        ;should be odd.
    arcsecperpix=10.42/700
  endif else if n_elements(arcsecperpix) eq 1 then begin
    add='_'+n2s(arcsecperpix,format='(f10.2)')+'arcsec'
    n=31                        ;should be odd.
  endif else begin
    add=''
    n=31                        ;should be odd.
    arcsecperpix=0.126
  endelse
  psf=grispsf(n,wlref,arcsecperpix,/show)
  psf=psf/max(psf)            ;normalize
  
  core=0.25        ;radius for PSF core to compute ratio between photons within core and outside
  
  mkhdr,hdr,psf
  sxaddpar,hdr,'TELESCOP','GREGOR','PSF computed for GREGOR'
  sxaddpar,hdr,'ASEC_PIX',arcsecperpix,'arcseconds per pixel'
  sxaddpar,hdr,'WLREF',wlref*1d10,'Ref. WL in Angstrom'
  sxaddpar,hdr,'CORE_RAD',core,'radius of core (arcsec)'
  
  phdr=hdr
  lhdr=hdr
  ghdr=hdr
  glhdr=hdr
  
                                ;add lorentzian
  xyarr=[(indgen(n)+.5) ## (intarr(n)+1)]-n/2.
  rarr=sqrt(xyarr^2+(transpose(xyarr))^2)*arcsecperpix
 
  s=0.75
;  s=1.5
  loramp=0.05
  lor=loramp/!pi*s/(s^2+(rarr)^2)
;  lor=lor/max(lor)            ;normalize
  sum=lor+psf
  sum=sum/total(sum)            ;normalize power
  sxaddpar,lhdr,'PSFTYP','PSF + Lorentzian'
  sxaddpar,lhdr,'LOR_SVAL',s,'Lorentzian width'
  sxaddpar,lhdr,'LOR_AMPL',loramp,'Lorentzian amplitude'
  sxaddpar,glhdr,'PSFTYP','PSF + Lorentzian'
  sxaddpar,glhdr,'LOR_SVAL',s,'Lorentzian width'
  sxaddpar,glhdr,'LOR_AMPL',loramp,'Lorentzian amplitude'
  
                                ;smear ideal + lor with gaussian, FWHM
  fwhm=0.25                     ;in arcsec
  sig=fwhm/(2*sqrt(2*alog(2.)))
  gs=exp(-rarr^2/(2*sig^2))
;  gs=gs/total(gs)
  fgs=fft(shift(gs,n/2.+1,n/2.+1),1)
  fpsf=fft(psf,1)               ;smear ideal
  fcon=fpsf*fgs
  con=fft(fcon,-1)
  con=double(con/total(con))
  fsum=fft(sum,1)               ;smear ideal * gauss
  fcon=fsum*fgs
  gl=fft(fcon,-1)
  gl=double(gl/total(gl))
  sxaddpar,ghdr,'PSFTYP','PSF * Gaussian'
  sxaddpar,ghdr,'GS_FWHM',fwhm,'FWHM of Gaussian (arcsec)'
  sxaddpar,glhdr,'PSFTYP','PSF * Gaussian'
  sxaddpar,glhdr,'GS_FWHM',fwhm,'FWHM of Gaussian (arcsec)'
  
                                ;compute ratios core vs. outside
  inmask=rarr le core
  csum=total(psf[where(inmask eq 1)]) & wsum=total(psf[where(inmask eq 0)])
  sxaddpar,phdr,'CORE_POW',csum/(wsum+csum),'core energy'
  sxaddpar,phdr,'WING_POW',wsum/(wsum+csum),'wing energy'
  csum=total(sum[where(inmask eq 1)]) & wsum=total(sum[where(inmask eq 0)])
  sxaddpar,lhdr,'CORE_POW',csum/(wsum+csum),'core energy'
  sxaddpar,lhdr,'WING_POW',wsum/(wsum+csum),'wing energy'
  csum=total(con[where(inmask eq 1)]) & wsum=total(con[where(inmask eq 0)])
  sxaddpar,ghdr,'CORE_POW',csum/(wsum+csum),'core energy'
  sxaddpar,ghdr,'WING_POW',wsum/(wsum+csum),'wing energy'
  csum=total(gl[where(inmask eq 1)]) & wsum=total(gl[where(inmask eq 0)])
  sxaddpar,glhdr,'CORE_POW',csum/(wsum+csum),'core energy'
  sxaddpar,glhdr,'WING_POW',wsum/(wsum+csum),'wing energy'
  
  
  psset,/ps,file='gris_spinor.ps',size=[18,10],/pdf,/no_x,/nomargin
  psf=psf/total(psf)
  !p.multi=[0,4,1] & userlct,/full & !p.charsize=1.5
  image_cont_al,/aspect,/cut,contour=0,alog10(psf), $
                zrange=minmax(alog10(psf)),title='PSF'
  image_cont_al,/aspect,/cut,contour=0,alog10(sum), $
                zrange=minmax(alog10(sum)),title='PSF+Lorentzian'
  image_cont_al,/aspect,/cut,contour=0,alog10(con), $
                zrange=minmax(alog10(con)),title='PSF*Gauss'
  image_cont_al,/aspect,/cut,contour=0,alog10(gl), $
                zrange=minmax(alog10(gl)),title='(PSF+Lorentzian)*Gauss'
  
  for i=0,3 do begin
    case i of 
      0: h=phdr[11:13]
      1: h=lhdr[11:16]
      2: h=ghdr[11:15]
      3: h=glhdr[11:18]
    endcase
    xyouts,i*0.33,0.2,/normal,strjoin('  '+h,'!C'),charsize=0.5
  endfor
  
  erase
  !p.position=0
  plot,/xst,/yst,rarr[*,n/2.],psf[*,n/2.],xtitle='arcsec',ytype=1
;  plot,/xst,/yst,rarr[*,n/2.],lor[*,n/2.]
  plot,/xst,/yst,rarr[*,n/2.],sum[*,n/2.],xtitle='arcsec',ytype=1
  plot,/xst,/yst,rarr[*,n/2.],con[*,n/2.],xtitle='arcsec',ytype=1
  plot,/xst,/yst,rarr[*,n/2.],gl[*,n/2.],xtitle='arcsec',ytype=1
  psset,/close
  
                                ;write out psf as fits file for usage
                                ;with spinor2d
  file='gregor_psf_ideal'+add+'.fits'
  writefits,file,psf*1d,phdr,append=0
  print,phdr
  print,"Wrote diff. limited (ideal) PSF to "+file
  
  sum=sum/total(sum)
  file='gregor_psf_lor'+n2s(s,format='(f6.1)')+add+'.fits'
  writefits,file,sum*1d,lhdr,append=0
  print,lhdr
  print,"Wrote diff. limited + Lorentzian PSF to "+file
  
  file='gregor_psf_gauss'+n2s(fwhm,format='(f6.2)')+add+'.fits'
  writefits,file,con*1d,ghdr,append=0
  print,ghdr
  print,"Wrote diff. limited PSF * Gauss to "+file
  
  file='gregor_psf_lor'+n2s(s,format='(f6.1)')+ $
       '_gauss'+n2s(fwhm,format='(f6.2)')+add+'.fits'
  writefits,file,gl*1d,glhdr,append=0
  print,ghdr
  print,"Wrote (diff. limited PSF + Lorentzian) * Gauss to "+file
  
  
  
  
  stop
end


pro psftest,psf
  
  icmhd=read_fits_dlm('/home/lagg/data/MuRAM/rempel_qs_dynamo/icont.fits',0,hdr)
  sz=size(icmhd,/dim)
  icmhd=icmhd[0:sz[0]/4,0:sz[1]/4]
  xykm=16.
  pixscale=xykm/700.
  
  
  psfdat=read_fits_dlm(psf,0,phdr)
  print,'Spatial PSF:'
  print,phdr[max(where(strpos(phdr,'COMMENT') eq 0))+1:*]
  asec_pix=sxpar(phdr,'ASEC_PIX')
   
  !p.multi=[0,2,1] & !p.position=0
  userlct,/full,coltab=0,/reverse
  image_cont_al,/aspect,/cut,contour=0,icmhd,title='Orig'
  
                                ;interpolate PSF to match Stokes pixel
                                ;scale
  psz=size(psfdat,/dim)
  pszip=psz*asec_pix/pixscale
  psf_ip=congrid(psfdat,pszip[0],pszip[1],/interp)
  psf_ip=psf_ip/total(psf_ip)
  
  icdeg=convol(icmhd,psf_ip,/edge_wrap)
  sz=size(icdeg,/dim)
  icdeg=congrid(icdeg,sz[0]*pixscale/asec_pix,sz[1]*pixscale/asec_pix)
  
  
  image_cont_al,/aspect,/cut,contour=0,icdeg,title='degraded'
  
  
  
  stop
  !p.position=0 & !p.multi=0
  userlct
  resomhd=calc_image_resolution(icmhd,pixelsize=pixscale,/azi,/plot)
  mommhd=moment(icmhd)
  rmsmhd=sqrt(mommhd[1])
  print,'RMS-contrast MHD: ',rmsmhd
  
  resodeg=calc_image_resolution(icdeg,pixelsize=asec_pix,/azi,/plot)
  momdeg=moment(icdeg)
  rmsdeg=sqrt(momdeg[1])
  print,'RMS-contrast degraded: ',rmsdeg
  stop
h=histogram(icdeg,nbins=30,loc=x)
plot,x,h  
stop
end
