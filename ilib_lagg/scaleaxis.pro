function scaleaxis,vec,x=x,y=y,reverse=reverse
  common xpwg,xpwg
  common fits,fits
  
  scale=[1.,1.]                 ;x, y scaling
  off=[0.,0.]                   ;x, y offset
  case xpwg.arcsec.val of
    0: 
    1: begin
;      scale=fits.pix2arcsec
      scale=xpwg.pixelscale.val
      if total(abs(scale)) lt 1e-4 then scale=fits.pix2arcsec
      off=fits.arcsecoff
    end
  endcase
  
  if keyword_set(x) then i=0 else if keyword_set(y) then i=1
  if keyword_set(reverse) then begin
    retval=(vec-off(i))/scale(i)
  endif else begin
    retval=vec*scale(i)+off(i)
  endelse
  
  return,retval
end
