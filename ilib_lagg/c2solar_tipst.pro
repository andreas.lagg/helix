function c2solar_tipst,tipfile,solar=solar,soho=soho,error=error,quiet=quiet
;  common olddat,tipold,thead,reduce,data,tot,dtot
  common verbose,verbose
  
  if n_elements(verbose) eq 0 then verb=1 else verb=verbose
  if keyword_set(quiet) then verb=0

                                ;check if file is split fits-file
  tf=add_qm(tipfile)
  xy_fits,tf,structure=xyfst
  thead=xyfst(0).fullheader
  
  
  tn=tag_names(thead)
  if max(tn eq 'ERROR') eq 1 then if thead.error ne 0 then begin
    if verb ge 1 then $
      message,/cont,'No position information in header of '+tipfile
    error=1
    return,-1
  endif
  
  if max(tn eq 'CAMERA') eq 0 then begin
    message,/cont,'No TIP-file: '+tipfile
    error=1
    return,-1
  endif
  
  tip2=strpos(thead.camera,'1024') ne -1
  gris=strpos(thead.telescop,'GREGOR') ne -1
  pixy=([0.38,0.17])(tip2)
  if gris then pixy=0.126
  
  tn=tag_names(thead)
  if max(tn eq 'STEPSIZE') eq 0 then begin
    print,'No stepsize info in header of '+thead.filename
    print,'File is possibly no observation!!!'
    print,'Use Stepsize='+n2s(pixy)+' and continue...'
    stepsize=pixy
  endif else stepsize=thead.stepsize
  
  
                                ;calculate array of x/y positions in
                                ;TIP arcsec values
  nwl=xyfst(0).nbin
  nrx=xyfst(0).nxtot
  nry=xyfst(0).ny
                                ;assume position at slit center and
                                ;position in header is scan center
  xvec=(float(indgen(nrx) mod thead.steps)-thead.steps/2.)*stepsize
  
                                ;get scan direction. if
                                ;slitorie-scandir is close to 360 then
                                ;the scan-dir is E -> W.
  if abs(thead.stepangl(0)) lt 1e-5 and $
    abs(thead.slitorie(0)) lt 1e-5 then begin
    message,/cont,'Could not determine scan direction. ' + $
      'Missing information: SLITORIE and STEPANGL'
    scandir=1
  endif else scandir=round((thead.slitorie-thead.stepangl)(0)/360)
  if scandir eq 0 then xvec=reverse(xvec)
  
xvec=reverse(xvec)
;  yvec=(findgen(nry)-nry/2.)*pixy
  yvec=(-findgen(nry)+nry/2.)*pixy ;slit is upside-down
;  yvec=(nry-1-findgen(nry))*pixy ;slit is upside-down
                                ;scan direction is perp to slit
                                ;orientation and terrestrial N-S is
                                ;180�
  slitorie=total(thead.slitorie)/n_elements(thead.slitorie)
  sso=sin(-(180.-slitorie)*!dtor)
  cso=cos(-(180.-slitorie)*!dtor)
  
  xarr=transpose(yvec*0+1.) ## xvec
  yarr=transpose(yvec)      ## (xvec*0+1.)
  
  xrot= xarr*cso + yarr*sso
  yrot=-xarr*sso + yarr*cso 
                                ;add central position of slit
                                ;position in arcsec from observers viewpoint
  
                                ;get r0,b0,... using SSW
  if max(tag_names(thead) eq 'DATEOBS') eq 1 then dateobs=thead.dateobs $
  else dateobs=thead.date_obs
  date=strmid(dateobs,8,2)+'-'+ $
    (['jan','feb','mar','apr','may','jun','jul', $
      'aug','sep','oct','nov','dec'])(fix(strmid(dateobs,5,2))-1)+'-'+ $
    strmid(dateobs,2,2)
  utime=strmid(thead.ut,0,8)
  hhmmss=strcompress(utime,/remove_all)
  atime=anytim2ints(hhmmss+' '+date)
  out=get_rb0p(atime,/deg)
  nel=n_elements(out(0,*))
  r0=total(out(0,*))/nel
  b0=total(out(1,*))/nel
  p0=total(out(2,*))/nel
  if max(abs(r0-thead.r0radius)) gt .01 then begin
    if verb ge 1 then begin
      print,'R0 in TIP header differs from calculated R0: diff='+ $
        n2s(max(abs(r0-thead.r0radius)))+' arcsec'
      print,'Using calculated R0'
    endif
    thead.r0radius=r0
  endif
  if max(abs(p0-thead.p0angle)) gt .01 then begin
    if verb ge 1 then begin
      print,'P0 in TIP header differs from calculated P0: diff='+ $
        n2s(max(abs(p0-thead.p0angle)))+' degrees'
      print,'Using calculated P0'
    endif
    thead.p0angle=p0
  endif
  if max(abs(b0-thead.b0angle)) gt .01 then begin
    if verb ge 1 then begin
      print,'B0 in TIP header differs from calculated B0: diff='+ $
        n2s(max(abs(b0-thead.b0angle)))+' degrees'
      print,'Using calculated B0'
    endif
    thead.b0angle=b0
  endif
                                ;rotate sun about -P0 angle (angle
                                ;between easth N-S and solar N-S)
  p0=total(thead.p0angle)/n_elements(thead.p0angle)
  cp0=cos(p0*!dtor)
  sp0=sin(p0*!dtor)
  
                                ;tip header contains central position
                                ;of slit. Before 01 May 07 this was in
                                ;VTT coordinates, 01may07 and later
                                ;contains slit in solar coordinates.
  if (atime.day)(0) lt 10348l then begin
    if verb ge 1 then $
      print,date+': Using SLITPOSX/Y in VTT coordinates' 
    slitx_vtt=thead.slitposx(0)
    slity_vtt=thead.slitposy(0)
  endif else begin
    if verb ge 1 then $
      print,date+': Using SLITPOSX/Y in SOLAR coordinates' 
    slitx_vtt= thead.slitposx(0)*cp0 - thead.slitposy(0)*sp0
    slity_vtt= thead.slitposx(0)*sp0 + thead.slitposy(0)*cp0
  endelse
  xrot=xrot+slitx_vtt(0)
  yrot=yrot+slity_vtt(0)
  xsol= xrot*cp0 + yrot*sp0
  ysol=-xrot*sp0 + yrot*cp0

                                ;calculate central position of each
                                ;pixel in solar coordinates SSW-lib
  xyssw=transpose([[xsol(*)],[ysol(*)]])
  b0=total(thead.b0angle)/n_elements(thead.b0angle)
  r0=total(thead.r0radius)/n_elements(thead.r0radius)
  lonlat=xy2lonlat(xyssw,b0=b0*!dtor,radius=r0)
  
                                ;calculate xy in solar coords for
                                ;standard values of b0=0. and r0=950.
  soho=keyword_set(soho)
  solar=keyword_set(solar)
  if soho eq 0 and solar eq 0 then solar=1
  if solar then begin
    if verb ge 1 then $
      print,'Converting to true solar coords (B0=0, R=950'')'
    xystd=lonlat2xy(lonlat,b0=0.,radius=950.)
  endif
  if soho then begin
    if verb ge 1 then $
      print,'Converting to SOHO coordinates'
    eph=pb0r(date, /soho,/arcsec)
    r0=eph(2)
    b0=eph(1)
    p0=eph(0)
    xystd=lonlat2xy(lonlat,b0=b0*!dtor,radius=r0)
  endif
  
  xstd=reform(xystd(0,*),nrx,nry)
  ystd=reform(xystd(1,*),nrx,nry)
  lon=reform(lonlat(0,*),nrx,nry)
  lat=reform(lonlat(1,*),nrx,nry)
  time=strcompress([min(utime),max(utime)],/remove_all)
  date=strcompress(dateobs,/remove_all)

  st={nx:nrx,ny:nry,xstd:xstd,ystd:ystd,lon:lon,lat:lat,xobs:xrot,yobs:yrot, $
      xsol:xsol,ysol:ysol,header:thead,file:tipfile,time:time,date:date}

  error=0
  return,st
end
