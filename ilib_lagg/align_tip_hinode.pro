pro align_tip_hinode,reread=reread
  common olddatth,index_sp,data_sp,itip,utip,qtip,vtip,hinic,tipic,tipcc,spf
;tipimage
  
;  tipcc='/nfs/work/ishikawa/VTT_Hinode/VTT-Apr08/09May08/09may08.003-01cc'
  tipcc='/data/slam/VTT-data/VTT-Apr08/07May08/07may08.004detrend-0?cc'
  if keyword_set(reread) or n_elements(itip) eq 0 then begin
    read_ccx,tipcc+'x',icont=icont,wl_vec=wl_vec 
    wlbin=where(wl_vec ge 10827.008d -.2 and wl_vec le 10827.008d)
    xy_fits,tipcc,i=itip,u=utip,q=qtip,v=vtip,bin=wlbin,/reset,silent=0,/norm2ic ;,/save_memory


                                ;get raw data images
;    mask='/nfs/work/ishikawa/Hinode/2008/05/09/SP4D/H08_H09/*.fits'
    mask='/data/slam/Hinode/20080507/SP4D/H0900/CAL/SP3*.fits'
    spf = findfile(mask,count=n)
    if n eq 0 then message,'No files found in '+mask
    read_sot,spf,index_sp,data_sp
    hinic=reform(data_sp(0,*,0,*)*0.)
;     for i=0,n_elements(spf)-1 do begin
;       read_ccx,spf(i)+'.ccx',icont=icont
;       hinic(*,i)=icont
;     endfor
  endif
  
  vvtip=reform(total(vtip,1)/float((size(vtip))(1)))
  mm=minmaxp(vvtip,perc=99.)
  vvtip=0>(vvtip-mm(0))/(mm(1)-mm(0))<1
  
                                ;average hinode i profile
  avg=total(total(data_sp,2),3)
  avgi=avg(*,0)
  dummy=min(avgi) & ibin=!c
  vvhin=transpose((reform(total(data_sp(ibin-10:ibin,*,3,*),1)))/11.)
  mm=minmaxp(vvhin,perc=99.9)
  vvhin=0>(vvhin-mm(0))/(mm(1)-mm(0))<1
  
  my_align,vvtip,vvhin,trans=trans,flip=flip,mirror=mirror,box=box, $
    xarr=xarr_tr,yarr=yarr_tr,amoeba=1,powell=0
  trans.tipnam=tipcc
  trans.hinnam=spf(0)+' - '+spf(n_elements(spf)-1)
  
  print,'Store transformation to sav-file...'
  sav=strmid(tipcc,0,strlen(tipcc)-2)+'_align.sav'
  fi=file_info(sav)
  if fi.exists then begin
    print,'Overwrite '+sav+' [y/N]?'
    repeat key=get_kbrd() until key ne ''
    if strupcase(key) ne 'Y' then read,'New filename: ',sav 
  endif 
                                ;test write access
  spawn,'touch '+sav+' ; test -w '+sav+' ; echo $?',res
  if res ne '0' then read,'Enter filename: ',sav
  save,trans,/xdr,/compress,file=sav
  print,'Image transformation stored in '+sav
  
  align_show,sav
end

