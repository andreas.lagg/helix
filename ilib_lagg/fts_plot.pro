;try this example:
pro fts_example,ps=ps
  wl=[5249.1050d,5249.4110d,5249.4370d,5249.5680d,5249.5760d,5249.6820d,5250.0000d,5250.2080d,5250.6450d,5250.8120d,5250.9300d,5251.4690d,5251.6080d,5251.9670d,5252.1000d]
  lab=['Fe I','Ca I','         Cr II','Ti I','       Nd II','Fe I + ?','Co I','Fe I','Fe I','Nd II','Ti I','Ti I','Fe I','Fe I','Ti I']
  geff=[0.917,1.500,1.600,0.750,0,1.208,.786,3.,1.5,0,1.,0,0,0,0]
  fts_plot,'~/work/spinor/examples/fts/sp19000',ps=ps,wlrange=5250.208d +[-2,2],charsize=1.25,yrange=[0.,1.1],/xst,/yst,color=1,line_wl=-0.008d + wl,line_label=lab,thick=3
  
  dma=(wl-5250.2080d)*1e3
  dkms=3e8/1e3*dma/1e3/5250.2080d
  dzeema=(geff*2000./1e4*1.6021e-19*(5250.208d *1e-10)^2/(4*!pi*9.1e-31*3e8))/1e-10*1e3
;  print,transpose([[wl],[dzeema],[dma],[dkms]])
end

pro fts_example2,ps=ps
  wl=[5249.1050d,5249.4110d,5249.4370d,5249.5680d,5249.5760d,5249.6820d,5250.0000d,5250.2080d,5250.6450d,5250.8120d,5250.9300d,5251.4690d,5251.6080d,5251.9670d,5252.1000d]
  lab=['Fe I','Ca I','         Cr II','Ti I','       Nd II','Fe I + ?','Co I','Fe I','Fe I','Nd II','Ti I','Ti I','Fe I','Fe I','Ti I']
  geff=[0.917,1.500,1.600,0.750,0,1.208,.786,3.,1.5,0,1.,0,0,0,0]
  fts_plot,'~/work/spinor/examples/fts/sp19000',ps=ps,wlrange=5250.208d +[-.6,.6],charsize=1.25,yrange=[0.,1.1],/xst,/yst,color=1,line_wl=-0.008d + wl,line_label=lab,thick=3,/imax
end

pro fts_example3,ps=ps
  wl=[5249.1050d,5249.4110d,5249.4370d,5249.5680d,5249.5760d,5249.6820d,5250.0000d,5250.2080d,5250.6450d,5250.8120d,5250.9300d,5251.4690d,5251.6080d,5251.9670d,5252.1000d]
  lab=['Fe I','Ca I','         Cr II','Ti I','       Nd II','Fe I + ?','Co I','Fe I','Fe I','Nd II','Ti I','Ti I','Fe I','Fe I','Ti I']
  geff=[0.917,1.500,1.600,0.750,0,1.208,.786,3.,1.5,0,1.,0,0,0,0]
  fts_plot,'~/work/spinor/examples/fts/sp19000',ps=ps,wlrange=5250.208d +[-.4,.7],charsize=1.25,yrange=[0.,1.1],/xst,/yst,color=1,line_wl=-0.008d + wl,line_label=lab,thick=3,/imax,file='~/tmp/fts.eps'
  fts_plot,'~/work/spinor/examples/fts/sp19000',ps=ps,wlrange=5250.208d +[-.4,.7],charsize=1.25,yrange=[0.,1.1],/xst,/yst,color=2,line_wl=-0.008d + wl,line_label=lab,thick=3,/imax,conv=85.,file='~/tmp/fts_imax.eps'
end

pro fts_example4,ps=ps
  wl=[5249.1050d,5249.4110d,5249.4370d,5249.5680d,5249.5760d,5249.6820d,5250.0000d,5250.2080d,5250.6450d,5250.8120d,5250.9300d,5251.4690d,5251.6080d,5251.9670d,5252.1000d]
  lab=['Fe I','Ca I','         Cr II','Ti I','       Nd II','Fe I + ?','Co I','Fe I','Fe I','Nd II','Ti I','Ti I','Fe I','Fe I','Ti I']
  geff=[0.917,1.500,1.600,0.750,0,1.208,.786,3.,1.5,0,1.,0,0,0,0]
  fts_plot,'~/work/spinor/examples/fts/sp19000',ps=ps,wlrange=5250.208d +[-.4,.7],charsize=1.25,yrange=[0.,1.1],/xst,/yst,color=1,line_wl=-0.008d + wl,line_label=lab,thick=3,/imax,conv=85.,file='~/tmp/fts_imax_6kms.eps',velos=-6.
  fts_plot,'~/work/spinor/examples/fts/sp19000',ps=ps,wlrange=5250.208d +[-.4,.7],charsize=1.25,yrange=[0.,1.1],/xst,/yst,color=3,line_wl=-0.008d + wl,line_label=lab,thick=3,/imax,conv=85.,file='~/tmp/fts_imax_p6kms.eps',velos=6.
end

pro fts_example5,ps=ps
  wl=[5249.1050d,5249.4110d,5249.4370d,5249.5680d,5249.5760d,5249.6820d,5250.0000d,5250.2080d,5250.6450d,5250.8120d,5250.9300d,5251.4690d,5251.6080d,5251.9670d,5252.1000d]
  lab=['Fe I','Ca I','         Cr II','Ti I','       Nd II','Fe I + ?','Co I','Fe I','Fe I','Nd II','Ti I','Ti I','Fe I','Fe I','Ti I']
  geff=[0.917,1.500,1.600,0.750,0,1.208,.786,3.,1.5,0,1.,0,0,0,0]
  fts_plot,'~/work/spinor/examples/fts/sp19000',ps=ps,wlrange=5250.208d +[-.4,.7],charsize=1.25,yrange=[0.,1.1],/xst,/yst,color=1,line_wl=-0.008d + wl,line_label=lab,thick=3,/imax,conv=85.,file='~/tmp/fts_imax_6kms.eps'
end

pro fts_example6,ps=ps
  wl=[5249.1050d,5249.4110d,5249.4370d,5249.5680d,5249.5760d,5249.6820d,5250.0000d,5250.2080d,5250.6450d,5250.8120d,5250.9300d,5251.4690d,5251.6080d,5251.9670d,5252.1000d]
  lab=['Fe I','Ca I','         Cr II','Ti I','       Nd II','Fe I + ?','Co I','Fe I','Fe I','Nd II','Ti I','Ti I','Fe I','Fe I','Ti I']
  geff=[0.917,1.500,1.600,0.750,0,1.208,.786,3.,1.5,0,1.,0,0,0,0]
  fts_plot,'~/work/spinor/examples/fts/sp19000',ps=ps,wlrange=5250.208d +[-.4,.7],charsize=1.25,yrange=[0.,1.1],/xst,/yst,color=1,line_wl=-0.008d + wl,line_label=lab,thick=3,/imax,conv=85.,file='~/tmp/fts_imax_3kms.eps',velos=-3.
end

pro fts_example7,ps=ps
  wl=[5249.1050d,5249.4110d,5249.4370d,5249.5680d,5249.5760d,5249.6820d,5250.0000d,5250.2080d,5250.6450d,5250.8120d,5250.9300d,5251.4690d,5251.6080d,5251.9670d,5252.1000d]
  lab=['Fe I','Ca I','         Cr II','Ti I','       Nd II','Fe I + ?','Co I','Fe I','Fe I','Nd II','Ti I','Ti I','Fe I','Fe I','Ti I']
  geff=[0.917,1.500,1.600,0.750,0,1.208,.786,3.,1.5,0,1.,0,0,0,0]
  fts_plot,'~/work/spinor/examples/fts/sp19000',ps=ps,wlrange=5250.208d +[-.4,.7],charsize=1.25,yrange=[0.,1.1],/xst,/yst,color=1,line_wl=-0.008d + wl,line_label=lab,thick=3,/imax,conv=85.,file='~/tmp/fts_imax_p4kms.eps',velos=+4.
end



pro fts_plot,fts,ps=ps,wlrange=wlrange,_extra=extra,air=air,vacuum=vac, $
             line_wl=line_wl,line_label=line_label,imax=imax,convolve=conv, $
             file=file,velos=velos
  
  fts_read,fts,tel=tel,cor=cor,obs=obs,wlair=wlair,wlvac=wlvac
  
  if n_elements(file) eq 0 then file=fts+'.eps'
  psset,ps=keyword_set(ps),file=file,/encapsulated,size=[24,16],/no_x
  userlct & erase
  @greeklett
  if keyword_set(air) eq 0 and keyword_set(vac) eq 0 then air=1
  if keyword_set(air) then begin
    wl=wlair
    addwl='Air'
  endif else begin
    wl=wlvac
    addwl='Vacuum'
  endelse
  wltit=f_lambda+' ['+f_angstrom+', '+addwl+']'
  
  spec=cor                      ;use corrected fts spectrum
                                ; (telluric blends removed) 
  
                                ;convolve with gaussian of specified
                                ;width in mA
  if n_elements(conv) ne 0 then begin
    nw=n_elements(wl)
    sigma=conv/(2*sqrt(2*alog(2.)))
    gs=exp(-(wl-wl(nw/2))^2/(2*(sigma/1e3)^2))
;    plot,(wl-wl(nw/2))*1e3,gs,xrange=[-1.,1.]*conv*.5,/xst & stop
    gs=[gs(n_elements(gs)/2:*),(gs(0:n_elements(gs)/2-1))]
    gs=gs/total(gs)
    spec=fft(fft(spec,1)*fft(gs,1),-1)
  endif
  
  ecol_flag=0b
  if n_elements(extra) ne 0 then begin
    tn=tag_names(extra)
    icol=where(tn eq 'COLOR')
    if icol(0) ne -1 then begin
      color=extra.color
      ecol_flag=1b
      extra.color=!p.color
    endif
  endif
  
  if n_elements(wlrange) ne 2 then wlrange=minmax(wl)
  
  if n_elements(velos) ne 0 then begin
    wloff=velos*1e3*mean(wlrange)/2.99792e8
    wl=wl+wloff
    line_wl=line_wl+wloff
  endif
  
  
  plot,wl,spec,xrange=wlrange,_extra=extra,/nodata, $
    xtitle=wltit,ytitle='Corr. FTS'
  if ecol_flag then extra.color=color
  oplot,!x.crange,[1.,1.],linestyle=1
  oplot,wl,spec,_extra=extra
  
  
  if keyword_set(imax) then begin
    wlimax=5250.208d +[-80, -40, +40, +80, +227]/1e3
    for i=0,n_elements(wlimax)-1 do begin
      dummy=min(abs(wl-wlimax(i)),imin)
      plots,color=3,psym=2,wlimax(i),spec(imin),symsize=2
      plots,color=!p.color,linestyle=1,wlimax(i)+[0,0],!y.crange
    endfor
  endif
  
  
                                ;draw vertical dotted lines at specified WL
  nl=n_elements(line_wl)

  if ecol_flag then extra.color=!p.color
  for i=0,nl-1 do begin
    dummy=min(abs(wl-line_wl(i)),imin)
    ypos=spec(imin)-0.02
    ypos=ypos+[0.,-.05]
    if line_wl(i) ge !x.crange(0) and line_wl(i) le !x.crange(1) then begin
      plots,/data,[0,0]+line_wl(i),ypos,_extra=extra
      if n_elements(line_label) eq nl then begin
        xyouts,orientation=270,/normal,-1,-1,/clip,'O',_extra=extra,width=w
        wdat=(convert_coord([[0d,0],[w,0d]],/normal,/to_data))([0,3])
        w=wdat(1)-wdat(0)
        xyouts,orientation=270,/data,line_wl(i)-w/2.,ypos(1)-0.01, $
          line_label(i),_extra=extra
                                ;calc FWHM of line
        icore=spec(imin)
        ip=min(where(spec(imin:*) ge 1-(1-icore)/2.))
        im=max(where(spec(0:imin) ge 1-(1-icore)/2.))
        fwhm=wl(im)-wl(ip+imin)
        if i eq 0 then print,'Line','WL [A]','FWHM [mA]',format='(3a20)'
        print,line_label(i),line_wl(i),fwhm*1e3,format='(a20,2f20.5)'

      endif
    endif
    
  endfor
  
  psset,/close
end
