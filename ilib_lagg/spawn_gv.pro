pro spawn_gv,file,_extra=_extra
  
  case strlowcase(!version.os_family) of
    'unix': begin
      spawn,'gv '+file(0)+' &'
    end
    'windows': begin
      message,/cont,'GV  spawn not enabled for Windows.'
    end
    else: message,'Unsupported OS: '+!version.os
  endcase
end
