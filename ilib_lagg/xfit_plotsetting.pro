;context-menu for right-click on xfits-plot window
pro xfits_plotsetwg,ixy
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xpswg,xpswg
  
  subst={id:0l,val:0.,str:''}
  xpswg={base:subst,range:replicate(subst,2)}
  
  ip=xfwg.p(ixy(0),ixy(1)).par.val
  tstr=fits.par(ip)+' Comp: '+n2s(fits.comp(ip)
  xpswg.base.id=widget_base(title=tstr,xpwg.draw.id,/context_menu)
  
  
end

pro xfits_plotsetting,ixy
  common xfwg,xfwg
  common fits,fits
  common xpwg,xpwg
  common xpswg,xpswg
  
  if n_elements(xpswg) ne 0 then begin
    widget_control,xpswg.base.id,bad_id=bad_id
    if bad_id eq 0 then begin
      if keyword_set(close) then widget_control,xpswg.base.id,/destroy
    endif
  endif
  
  xfits_plotsetwg,ixy
 
end
