;get stokes parameter for plot position ix,iy
function xfits_getstokespar,ix,iy,valid=valid,xrange=xrange,yrange=yrange, $
                            title=title,allstokes=allstokes
  common xpwg,xpwg
  common xdir,xdir
  common fits,fits
  common xfwg,xfwg
  common xpswg,xpswg
  common xfgsp_store,store
  
;  widget_control,xfwg.base.id,hourglass=1
  
  title=''
  data=fltarr(xrange(1)-xrange(0)+1,yrange(1)-yrange(0)+1)
  valid=data*0
  if n_elements(xpswg) eq 0 then begin
    xfits_plotsetting,[ix,iy],/init
  endif
  
   ; print,'Get Stokes Parameter for Parameter in Plot ',ix,iy  
   ; print,'Stokes: ',xfwg.p(ix,iy).stokes
   ; print,'Obs/Fit: ',xfwg.p(ix,iy).obsfit
   ; print,'WL-range: ',xfwg.p(ix,iy).wlrg
  
  filechange=1
  if n_elements(store) eq 0 then reread=1 $
  else begin
    iwl=(where((store.wlrg(0) eq xfwg.p(ix,iy).wlrg(0) and $
                store.wlrg(1) eq xfwg.p(ix,iy).wlrg(1) and $
                store.fitstype eq xfwg.p(ix,iy).obsfit and $
                store.absval eq xfwg.p(ix,iy).absval.val and $
                store.maxval eq xfwg.p(ix,iy).maxval.val and $
                store.parmode eq xfwg.parmode.val $
;               and store.ic eq xfwg.p(ix,iy).ic.val  $
               ) eq 1))(0)
    filechange=store(0).file ne fits.obsfile(0)
    reread=filechange or iwl eq -1
    ; print,'xfgs',reread,filechange,iwl
    ; print,store.wlrg(0) eq xfwg.p(ix,iy).wlrg(0)
    ; print,store.wlrg(1) eq xfwg.p(ix,iy).wlrg(1)
    ; print,store.fitstype eq xfwg.p(ix,iy).obsfit
  endelse
  if reread then begin
    spar=(['I','Q','U','V','Ic'])
    title=strarr(5)
    for i=0,4 do begin
      fitstype=xfwg.p(ix,iy).obsfit
      if fits.code eq 'spinor' then begin
        ofile=fits.file
;        ofile=fits.obsfile
        if fitstype ge 1 then ofile=fits.synthfile
      endif else begin
        ofile=fits.obsfile
        if fitstype ge 1 then ofile=fits.synthfile
      endelse
      print,'Obs-file: ',ofile
      if xfwg.parmode.val eq 0 then pixelrep=0 $
      else pixelrep=xfwg.parmode.val+max(fits.pixrep)
      img=get_obsimage(ofile,par=spar(i),wlrange=xfwg.p(ix,iy).wlrg, $
                       error=error,maximg=maximg,title=tit, $
                       fitstype=fitstype,icont=icont,pixelrep=pixelrep, $
                       absval=xfwg.p(ix,iy).absval.val)
      if xfwg.p(ix,iy).maxval.val eq  1 then img=maximg
      if min((size(icont))(1:2) eq (size(img))(1:2)) eq 1 then begin
;        if xfwg.p(ix,iy).ic.val ne 1 then img=img*icont
        if xfwg.p(ix,iy).ic.val eq 1 then img=img/icont
      endif
      if error ne 0 then begin
        message,/cont,'Error in retrieving observed/fitted image from '+ofile
        title=''
        return,data
      endif      
      xfwg.p(ix,iy).obsfit=fitstype
      title(i)=tit
      sz=size(img)
      if i eq 0 then img4=fltarr(5,sz(1),sz(2))
      img4(i,*,*)=temporary(img)
    endfor
;  endif 
;  if reread then begin
    newst=n_elements(store) eq 0 or filechange
    if newst eq 0 then newst=max(size(store(0).img) ne size(img4)) eq 1
    nstoremax=10
    if newst then begin
      sst={img:img4*0,file:'',wlrg:dblarr(2)-1d,title:strarr(5),fitstype:0, $
           ic:0,parmode:0.,absval:0,maxval:0}
      store=replicate(sst,nstoremax)
      iwl=0
    endif
    if iwl eq -1 then iwl=max(where(store.file ne ''))+1
    if iwl ge nstoremax then begin
      store(0:nstoremax-2)=store(1:nstoremax-1)
      iwl=iwl-1
    endif
    store(iwl).img=img4
    store(iwl).title=title
    store(iwl).wlrg=xfwg.p(ix,iy).wlrg
    store(iwl).file=fits.obsfile(0)
    store(iwl).fitstype=xfwg.p(ix,iy).obsfit
    store(iwl).ic=xfwg.p(ix,iy).ic.val
    store(iwl).absval=xfwg.p(ix,iy).absval.val
    store(iwl).maxval=xfwg.p(ix,iy).maxval.val
    store(iwl).parmode=xfwg.parmode.val
  endif
  
  
  img=reform(store(iwl).img(xfwg.p(ix,iy).stokes,*,*))
  szi=size(img)
  ;; xrg=xrange-fits.xoff
  ;; yrg=yrange-fits.yoff
  xrg=xrange;-fits.xoff
  yrg=yrange;-fits.yoff
  data=img(xrg(0)<(szi(1)-1):xrg(1)<(szi(1)-1), $
           yrg(0)<(szi(2)-1):yrg(1)<(szi(2)-1))
  title=store(iwl).title(xfwg.p(ix,iy).stokes)
  icimg=reform(store(iwl).img(4,xrg(0)<(szi(1)-1):xrg(1)<(szi(1)-1), $
                              yrg(0)<(szi(2)-1):yrg(1)<(szi(2)-1)))
  if xfwg.p(ix,iy).ic.val eq 1 then begin 
    data=data/icimg
    title=title+' (/Ic)'
  endif
  if keyword_set(allstokes) then begin
    data=reform(store(iwl).img(*, $
                                 xrg(0)<(szi(1)-1):xrg(1)<(szi(1)-1), $
                                 yrg(0)<(szi(2)-1):yrg(1)<(szi(2)-1)))
    title=store(iwl).title
    if xfwg.p(ix,iy).ic.val eq 1 then for i=0,3 do $
      data[i,*,*]=data[i,*,*]/icimg
  endif
  valid(*)=1
  
;  widget_control,xfwg.base.id,hourglass=0
  return,data
  
end
