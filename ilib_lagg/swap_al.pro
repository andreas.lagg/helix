;swap two variables
pro swap_al,var1,var2
  
  dummy=var1
  var1=var2
  var2=temporary(dummy)
  
end
