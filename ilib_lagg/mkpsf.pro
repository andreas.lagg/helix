function aperture_s,pnx,pny,rc
  a=dblarr(pnx,pny)
  x=(dindgen(pnx,pny) mod pnx) - pnx/2
  y=(dindgen(pnx,pny)/pnx) - pny/2
  r0=rc*(pny/2.)
  return,0.5+0.5*erf(5.0*(r0-sqrt(x*x+y*y)))
end

function otf,pupil,phi
  ds=size(pupil,/DIMENSIONS)
  ewf=dcomplexarr(2*ds[0],2*ds[1])
  ewf[0:ds[0]-1,0:ds[1]-1]=pupil*dcomplex(cos(phi),sin(phi))
  ewf=fft(ewf)
  return,fft(ewf*conj(ewf),/inverse)/total(pupil)
end

function clearpupil,n,lambda,telescope_d,arcsecperpix,display=display
  fac=2.0
  nph=fac*n/2
  
  rad2deg=180.0/!pi
  scale_angle2CCD=arcsecperpix/(rad2deg*3600.0)
  q_number=lambda/(scale_angle2CCD*telescope_d)
  lim_freq=double(2.0*nph)/q_number ; telescope_d in pupil space
  rc=lim_freq
  
  pupil=aperture_s(nph,nph,rc/double(nph))

  if(keyword_set(display)) then tvscl,rebin(pupil,512,512)
  
  return,pupil
end

function hinodepupil,n,lambda,arcsecperpix,display=display
  fac=2.0
  nph=fac*n/2
  
  telescope_d=0.5
  rad2deg=180.0/!pi
  scale_angle2CCD=arcsecperpix/(rad2deg*3600.0)
  q_number=lambda/(scale_angle2CCD*telescope_d)
  lim_freq=double(2.0*nph)/q_number ; telescope_d in pupil space
  rc=lim_freq
  rco=(0.172/telescope_d)*rc
  sw=(0.04/0.172)*rco
  
  pupil=aperture_s(nph,nph,rc/double(nph))
  pupilstop=aperture_s(nph,nph,rco/double(nph))
  pupil-=pupilstop

  print,'strut=',sw,' obscuration=',rco,rc

  for w=-2*sw,2*sw do begin
    pupil[nph/2+w/4,nph/2:nph-1]=0.0
    for x=0,nph/2+sw do pupil[fix((-0.5*w/4+nph/2.-x)>0),fix((0.8660254*w/4+nph/2.-0.57735027*x)>0)]=0.0
    for x=0,nph/2+sw do pupil[fix(( 0.5*w/4+nph/2.+x)<nph-1),fix((0.8660254*w/4+nph/2.-0.57735027*x)>0)]=0.0
  end

  if(keyword_set(display)) then tvscl,pupil
  
  return,pupil
end

function bighinodepupil,n,lambda,arcsecperpix,display=display
  fac=2.0
  nph=fac*n/2
  
  telescope_d=1.0
  rad2deg=180.0/!pi
  scale_angle2CCD=arcsecperpix/(rad2deg*3600.0)
  q_number=lambda/(scale_angle2CCD*telescope_d)
  lim_freq=double(2.0*nph)/q_number ; telescope_d in pupil space
  rc=lim_freq
  rco=2.0*(0.172/telescope_d)*rc
  sw=(0.04/2.0/0.172)*rco
  
  pupil=aperture_s(nph,nph,rc/double(nph))
  pupilstop=aperture_s(nph,nph,rco/double(nph))
  pupil-=pupilstop

  print,'strut=',sw,' obscuration=',rco,rc

  for w=-2*sw,2*sw do begin
    pupil[nph/2+w/4,nph/2:nph-1]=0.0
    for x=0,nph/2+sw do pupil[fix((-0.5*w/4+nph/2.-x)>0),fix((0.8660254*w/4+nph/2.-0.57735027*x)>0)]=0.0
    for x=0,nph/2+sw do pupil[fix(( 0.5*w/4+nph/2.+x)<nph-1),fix((0.8660254*w/4+nph/2.-0.57735027*x)>0)]=0.0
  end

  if(keyword_set(display)) then tvscl,rebin(pupil,512,512)
  
  return,pupil
end

function sunrisepupil,n,lambda,arcsecperpix,display=display
  fac=2.0
  nph=fac*n/2
  
  telescope_d=1.0
  rad2deg=180.0/!pi
  scale_angle2CCD=arcsecperpix/(rad2deg*3600.0)
  q_number=lambda/(scale_angle2CCD*telescope_d)

  lim_freq=double(2.0*nph)/q_number ; telescope_d in pupil space
  rc=lim_freq
  rco=(0.330/telescope_d)*rc
  sw=(0.015/0.330)*rco
  
  pupil=aperture_s(nph,nph,rc/double(nph))
  pupilstop=aperture_s(nph,nph,rco/double(nph))
  pupil-=pupilstop

  print,sw

  for w=-2*sw,2*sw do begin
    pupil[nph/2+w/4,0:nph-1]=0.0
    pupil[0:nph-1,nph/2+w/4]=0.0
  end

  if(keyword_set(display)) then tvscl,rebin(pupil,512,512)
  
  return,pupil
end

function gregorpupil,n,lambda,arcsecperpix,display=display
  fac=2.0
  nph=fac*n/2
  
  telescope_d=1.44
  rad2deg=180.0/!pi
  scale_angle2CCD=arcsecperpix/(rad2deg*3600.0)
  q_number=lambda/(scale_angle2CCD*telescope_d)

  lim_freq=double(2.0*nph)/q_number ; telescope_d in pupil space
  rc=lim_freq
  rco=(0.420/telescope_d)*rc
  sw=(0.020/0.420)*rco
  
  pupil=aperture_s(nph,nph,rc/double(nph))
  pupilstop=aperture_s(nph,nph,rco/double(nph))
  pupil-=pupilstop

  print,sw

  for w=-2*sw,2*sw do begin
    pupil[nph/2+w/4,0:nph-1]=0.0
    pupil[0:nph-1,nph/2+w/4]=0.0
  end

  if(keyword_set(display)) then tvscl,rebin(pupil,512,512)
  
  return,pupil
end

function psf,n,lambda,telescope_d,arcsecperpix,defoc,display=display,predef=predef
  fac=5.0             ; scale factor
  nph=fac*n/2
  
;  q_number=lambda*rad2deg*3600.0/(arcsecperpix*telescope_d)
  rad2deg=180.0/!pi
  scale_angle2CCD=arcsecperpix/(rad2deg*3600.0)
  q_number=lambda/(scale_angle2CCD*telescope_d)
  lim_freq=double(2.0*nph)/q_number ; telescope_d in pupil space
  rc=lim_freq
  
;  pupil=bighinodepupil(nph,lambda,arcsecperpix,display=display)      ; 1.0m Hinode (Solar C?)
  case strupcase(predef) of
    'SUNRISE': pupil=sunrisepupil(nph,lambda,arcsecperpix,display=display)
    'GREGOR': pupil=gregorpupil(nph,lambda,arcsecperpix,display=display)
    'HINODE': pupil=hinodepupil(nph,lambda,arcsecperpix,display=display) 
    else: pupil=clearpupil(nph,lambda,telescope_d,arcsecperpix,display=display)
  endcase
  
  print,nph
  defocus=zernike(nph/2,rc/double(nph/2),4)
  defocus=zernike(nph/2,rc/double(nph/2),80)
  defocus/=max(pupil*defocus,/nan)
  print,'max=',max(pupil*defocus,/nan),'  min=',min(pupil*defocus,/nan)

  s=otf(pupil,defoc*(2.0*!pi)*defocus)
  if(keyword_set(display)) then tvscl,abs(s)
  f=shift(double(fft(s,/inverse)),nph,nph)

  sf=f[nph-nph/fac:nph+nph/fac-1,nph-nph/fac:nph+nph/fac-1]
;  tvscl,real_part(sf),nph,0
  if(keyword_set(display)) then tvscl,sf
  sf=shift(sf,nph/fac,nph/fac)

  print,max(sf,idx)
  nn=2*nph/fac
  print,idx mod nn,idx/nn
  
  return,sf/total(sf)
end

function mkpsf,np,pixsz,lambda,defocus,display=display,predef=predef
;
; physical parameters
;
;  defocus=0.0          ; amount of defocus (waves)
;  pixsz=0.16           ; pixel size of calculated PSF
;  lambda=6302.49D-10   ; wavelength
;
; numerical parameters
;
  bh=6                 ; bin 2*bh+1 PSF pixels per image pixel (~half the binning factor)
;  np=90                ; pixel radius of the PSF to collect (in final pixel size)
; sz=4096              ; extent of the full resolution PSF: this must exceed 2*np*(2*bh+1)
  sz=2048   ; extent of the full resolution PSF: this must exceed 2*np*(2*bh+1)
  sz=2*np*(2*bh+1)
  sz=((sz/256)+1)*256
;  sz=5120              ; extent of the full resolution PSF: this must exceed 2*np*(2*bh+1)
;
  bf=2*bh+1            ; binning factor
  arcsecpp=pixsz/bf    ; unbinned PSF pixel size
  
  if n_elements(predef) eq 0 then predef='none'
  case strupcase(predef) of
    'SUNRISE': telescope_d=1.0
    'GREGOR': telescope_d=1.44
    'HINODE': telescope_d=0.5
    else: telescope_d=1.0
  endcase
  p=(shift(psf(sz,lambda,telescope_d,arcsecpp,defocus,display=display,predef=predef),sz/2,sz/2))[1:sz-1,1:sz-1]
  p=p/total(p,/nan)
;
; rebin the PSF
;
  np=long(np)
  pp=dindgen(2*np+1,2*np+1)*0.0
  xo=sz/2-1
  yo=sz/2-1
;
  if 1 eq 1 then begin
    for i=-np,np do for j=-np,np do begin
      for di=-bh,bh do for dj=-bh,bh do pp[i+np,j+np]+=p[bf*i+xo+di,bf*j+yo+dj]
    end
  end else begin
    for i=-np,np do for j=-np,np do begin
      for di=-bh/2,bh/2 do for dj=-bh,bh do pp[i+np,j+np]+=p[bf*i+xo+di,bf*j+yo+dj]
    end
  end
;
; some stats
;
  print,'size:',2*np+1,' x',2*np+1,' pixel size=',bf*arcsecpp
  print,'contained fraction:',total(pp)/total(p)
  pp=pp/total(pp)
  if(keyword_set(display)) then begin
    !p.position=0
    !p.multi=[0,2,1] & userlct,/full
    plot,roundmean(p),/ylog
    oplot,[(2*bh+1)*np,(2*bh+1)*np],10^[!y.crange[0],!y.crange[1]],linestyle=2
    image_cont_al,/cut,/aspect,contour=0,pp
  end
  
  mkhdr,hdr,pp
  sxaddpar,hdr,'TELESCOP',predef,'telescope'
  sxaddpar,hdr,'DIAMETER',telescope_d,'telescope diameter'
  sxaddpar,hdr,'ASECPIX',pixsz,'pixel size in arcseconds'
  sxaddpar,hdr,'WAVELEN',lambda*1d10,'wavelength in Angstrom'
  sxaddpar,hdr,'DEFOCUS',defocus,'additional defocus'
  
  if predef eq 'none' then tel=n2s(telescope_d,format='(f5.1)')+'m' $
  else tel=strlowcase(predef)
  fitsfile='psf_'+tel+'_'+n2s(pixsz,format='(f10.4)')+'.fits'
  writefits,fitsfile,pp,hdr
  print,'Wrote PSF to fits-file: ',fitsfile

  return,pp  

end
