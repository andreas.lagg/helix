pro grisshow
  
  dir='/data/slam/GREGOR-data/GREGOR-Jun16/gris/'
  
  
  files1=file_search(dir+'*/level1/??jun??.0??ccx',count=cnt)
  files1=strmid(files1,0,strlen(files1[0])-3)
  files2=file_search(dir+'*/level1/??jun??.0??-01ccx',count=cnt)
  files2=strmid(files2,0,strlen(files2[0])-6)
  files=[files1,files2]
;  files='/data/slam/GREGOR-data/GREGOR-Jun16/gris/09jun16/level1/09jun16.006'
  files=files(reverse(sort(files)))
  
  st={wl:0d,dwl:0.5d,stk:'IQUV',norm2ic:0}
  desc=replicate(st,5)
  desc[0].wl=10824.000 & desc[0].dwl=0.30d & desc[0].stk='I'
  desc[1].wl=10827.089 & desc[1].dwl=0.30d & desc[1].stk='IQUV'
  desc[2].wl=10827.700 & desc[2].dwl=0.25d & desc[2].stk='V'
  desc[3].wl=10830.330 & desc[3].dwl=0.30d & desc[3].stk='IQUV'
  desc[4].wl=10830.330 & desc[4].dwl=0.30d & desc[4].stk='I' & desc[4].norm2ic=1
  for i=0,n_elements(files)-1 do begin
    print,files[i]
    fp=get_filepath(files[i])
    common data,data
    read_data,dir=fp.path,mask=fp.name+'*cc'
    
                                ;display continuum image
    !p.charsize=1.2
    psset,ps=1,/pdf,file='~/tmp/'+fp.name+'.ps',size=[20,12],/no_x, $
          paper='nb',printfile=0,view=0
    userlct,coltab=0,/reverse,/full
    wl=data.wl[0,*]
    first=1
    for id=0,n_elements(desc)-1 do if desc[id].wl ge 10800 then begin
      for is=0,strlen(desc[id].stk)-1 do begin
        if first eq 0 then erase
        first=0
        if is eq 3 then wlrg=desc[id].wl+[0,1.]*desc[id].dwl $
        else wlrg=desc[id].wl+[-0.5,0.5]*desc[id].dwl
        inwl=where(wl ge wlrg[0] and wl le wlrg[1])
        if inwl[0] ne -1 then begin
          iwl=[min(inwl),max(inwl)]
          wlstr=add_comma(n2s(wlrg,format='(f9.3)'),sep='-')
          
          ststr=strmid(desc[id].stk,is,1)
          istk=where(['I','Q','U','V'] eq ststr)
           
          img=reform(total(data.sp[iwl[0]:iwl[1],*,istk,*],1)/ $
                     (iwl[1]-iwl[0]+1))
          img=transpose(img)
          if desc[id].norm2ic eq 0 then img=img/data.imgcont $
          else begin
            img=img/data.icont
            ststr=ststr+'/I!LC!N'
          endelse
          utime=strmid(data.header.utime,0,5)
          if max(utime ne '') eq 1 then utime=utime(where(utime ne '')) $
          else utime='-'
          title=fp.name+ $
                ' ('+add_comma(utime,sep='-')+')'+ $
                '  -  Stokes '+ststr+', '+wlstr
          rg=minmaxp(img,perc=99.)
          if istk ne 0 then rg=[-1,1]*max(abs(rg))
          
          image_cont_al,/cut,/aspect,contour=0,img,range=rg, $
                        xrange=[0,data.nx-1]*.135,yrange=[0,data.ny-1]*.135, $
                        xtitle='scan direction [arcsec]', $
                        ytitle='slit direction [arcsec]'
          xyouts,/normal,alignment=0.5,0.5,1,'!C'+title
         
        endif
      endfor        
    endif
    
    psset,/close
  endfor
  
end
