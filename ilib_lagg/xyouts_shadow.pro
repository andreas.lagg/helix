pro xyouts_shadow,x,y,string,_extra=_extra, $
                  shadow_color=shadow_color,shadow_thick=shadow_thick
  
  if n_elements(_extra) eq 0 then message,'Please use xyouts syntax.'
  if n_elements(shadow_thick) eq 0 then shadow_thick=1
  if n_elements(shadow_color) eq 0 then shadow_color=15
  
  ex=_extra
  data=0 & normal=0 & device=0
  if max(tag_names(ex) eq 'NORMAL') eq 1 then begin
    normal=1
    ex.normal=0
  endif else if max(tag_names(ex) eq 'DEVICE') eq 1 then begin
    device=1 
    ex.device=0
  endif else begin
    data=1
    ex.data=0
  end
  devst={device:0}
  if max(tag_names(ex) eq 'DEVICE') eq 0 then ex=create_struct(ex,devst)
  ex.device=1
  
  
  xydev=convert_coord(/to_device,data=data,normal=normal,device=device,x,y)
  oldcolor=!p.color
  colst={color:0}
  if max(tag_names(ex) eq 'COLOR') eq 1 then oldcolor=ex.color $
  else ex=create_struct(ex,colst)
  ex.color=shadow_color
  
  if !d.name eq 'PS' then fact=10. else fact=1.
  for i=1.,shadow_thick*fact,.5 do $
    xyouts,xydev(0)+i,xydev(1)-i,string,_extra=ex
  ex.color=oldcolor
  xyouts,xydev(0),xydev(1),string,_extra=ex
  
  
end
