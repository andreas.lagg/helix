;quicklook for Hinode FGIV data
pro fgiv_quick,file,ps=ps
  
  img=readfits_ssw(file)
  
  fileroot=strmid(file,strpos(/reverse_search,file,'/')+1,strlen(file))
  psset,ps=ps,size=[18,26],file='~/tmp/'+fileroot+'.ps'
  userlct,/full,coltab=0,/reverse
  
  img=readfits(file,header)
  
  sz=size(img)
  if sz(0) eq 3 then ny=sz(3) else ny=1
  !p.multi=[0,1,ny]
  xs=sz(1) & ys=sz(2)
  for i=0,ny-1 do begin
    zr=minmaxp(img(*,*,i),perc=99)
    if i ge 1 then zr=max(abs(zr))*[-1,1]
    image_cont_al,/aspect,contour=0,reform(img(*,*,i)),zrange=zr,title=fileroot,$
      xrange=[0,xs*.16],yrange=[0,ys*.16]
  endfor
  
  psset,/close
end
