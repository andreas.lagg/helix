function lineratio,wl_lines=wl_lines,gf=gf,dwl=dwl,smooth=smval
  common fits,fits
  common lrdat,lr,old_wl,old_dwl,old_smval
  
  redo=1
  if n_elements(lr) ne 0 then $
    redo=max(old_wl ne wl_lines) or old_dwl ne dwl or old_smval ne smval
  
  if redo then begin
    read_fitsstokes,fits.file,fdat,/iquv,/full,fitstype=0
    sz=size(fdat.v,/dim)
    vmax=fltarr(2,sz[1],sz[2])+!values.f_nan
    if n_elements(smval) ne 0 then $
      vv=smooth(fdat.v,[smval,1,1],/edge_t) $
    else vv=fdat.v
    for il=0,1 do begin
                                ;assume same WL for all pixels
      inwl=where(fdat.wl[*,0,0] ge wl_lines[il]-dwl/2. and $
                 fdat.wl[*,0,0] le wl_lines[il]+dwl/2.)
      if inwl[0] ne -1 then $
        vmax[il,*,*]=max(abs(vv[inwl,*,*]),dimension=1)
    endfor
    
    lr=reform(vmax[0,*,*]*gf[1]/(vmax[1,*,*]*gf[0]))
    old_wl=wl_lines
    old_dwl=dwl
    old_smval=smval
  endif
  
  return,lr
end
