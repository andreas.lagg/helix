function get_histocont,icimg

;take a continuum image, make a histogram of all pixels with a brightness of at least 50% of the max value and assume that the average QS continuum is the maximum of this histogram

maxic=max(smooth(icimg,5))
iin=where(icimg ge 0.5*maxic)

hist=histogram(icimg[iin],nbin=100,loc=x)

dummy=max(hist,imax)
icont=x[imax]
return,icont


end
