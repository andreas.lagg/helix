program fitstest
  integer(4) :: unit,status(10),bsz,naxes(3),bitpix,xyz(3)
  integer(8) :: offp

  naxes(1)=2048
  naxes(2)=1024
  naxes(3)=1280
  satus=0
  bitpix=8

  write(*,*) "Size: ",naxes,int(naxes(1),8)*naxes(2)*naxes(3)/int(2,8)**31

  unit=99
  call ftgiou(unit,status(1))
  call ftinit(unit,'/home/lagg/tmp/fitstest.fits',bsz,status(2)) !  open the fits file
  call ftiimg(unit,bitpix,3,naxes,status(3))

  offp=int(2,8)**31+1
  write(*,*) 'Offset: ',offp

!  call ftppri(unit,1,offp,1,10,status(4))

  xyz=naxes-2
  xyz=2
  write(*,*) "Write to position ",xyz
  call ftpssi(unit,1,3,naxes,xyz,xyz,1,status(5))

  call ftclos(unit,status(6))
  call ftfiou(unit,status(7))

  write(*,*) 'Status: ',status

end program
