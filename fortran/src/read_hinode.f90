subroutine read_hinode(file,xv,yv,profile,lsprof,do_ls,ok)
  use all_type
  use ipt_decl
  use tools
  implicit none
#ifdef MPI
  include 'mpif.h'
#endif
  type (proftyp) :: profile,lsprof
  character(LEN=maxstr) :: file,datnam,hinodenam(4096),filenam,hfile,fnew,&
       hdnam,fccx
  character(LEN=72) :: comment
  character(LEN=5) :: idstr
  integer(2) :: scan,nhinode,hinodexpos(4096),hinodescannr(4096),xerr,&
       hinode_fixedslit
  integer(2) :: slitindx,xv,yv,i,ui,nscan,nwl
  integer(4) :: isdir,unit,xunit,err,blocksize,dummy,myid,ierr
  integer(4) :: status
  INTEGER(4) :: naxes(3),naxis2 !,nfound,bitpix, &
!         fpixels(3), lpixels(3), inc(3),xnaxes(2),xstat,unitccx
  logical :: ok,doshow,error,do_ls,sp4_mode
  real(4) :: icont
  real(4) :: icimg(:,:)
  allocatable icimg
  real(8) :: wref,delw,wl_vec(maxwl)
  common /hincomm/ hfile,nhinode,hinodenam,hinodexpos,hinodescannr,datnam,&
       hinode_fixedslit
#ifndef GNU
  INTERFACE
     INTEGER FUNCTION SYSTEM (COMMANDA)
       CHARACTER(LEN=*) COMMANDA
     END FUNCTION SYSTEM
  END INTERFACE
#endif

  datnam=file

  scan=ipt%obs_par%hin_scannr
  nscan=0
  doshow=.false.
  if (hfile.ne.file) then
     doshow=.true.
     if (ipt%verbose.ge.1) write(*,*) 'Hinode Mode.'
     !look for Hinode data files:
     !if DATNAM contains directory then the header of the files are searched 
     !for the variable SLITINDX, the first match with OBXXX is used as the 
     !data file.
     !first check if file is single file or if it is directory:
     isdir=system('test -d '//trim(DATNAM))  

     if (isdir.eq.0) then
        if (ipt%verbose.ge.1) &
             write(*,*) 'Reading Hinode data from directory'//trim(datnam)
        sp4_mode=(system('ls -1 '//file(1:len_trim(file))//&
             '/*SP4*.fits '//trim(redirect)).eq.0)
        if (sp4_mode) then 
           datnam=file(1:len_trim(file))//'/*SP4*.fits'
        else
           datnam=file(1:len_trim(file))//'/*SP3*.fits' 
        end if
     endif
     myid=0
     ierr=0
#ifdef MPI
     call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
#endif
     write(unit=idstr,fmt='(i5.5)') myid
     hdnam='hindirlist.tmp'//idstr
     dummy=system('ls -1 '//datnam(1:len_trim(datnam))//' > '//trim(hdnam))
     err=0
     OPEN(UNIT=xunit ,FILE=trim(hdnam))
     do while (err.eq.0)
        read(XUNIT, fmt='(a)',iostat=err) filenam
        if (err.eq.0) then
           !get fits header variable containing obs number
           status=0
           call ftgiou(unit,status)
           call ftopen(unit,trim(filenam),0,blocksize,status)
           call ftgkyj(unit,'SLITINDX',slitindx,comment,status)
           call ftgkyj(unit,'NAXIS2',naxis2,comment,status)
           call ftclos(unit, status)
           call ftfiou(unit, status)
           if (status.eq.0) then
              nhinode=nhinode+1
              hinodenam(nhinode)=trim(filenam)
              hinodexpos(nhinode)=slitindx
              if (nhinode.gt.1) then
                 if (hinodexpos(nhinode).le.hinodexpos(nhinode-1)) &
                      nscan=nscan+1
              end if
              hinodescannr(nhinode)=nscan
           end if
        end if
     end do
     CLOSE(xunit,STATUS='DELETE')
     if (nhinode.ge.1) then
        if (ipt%verbose.ge.1) &
             write(*,'(a,i5,a,a,i5,'' to '',i5,a,i5,'' to '',i5)') &
             ' Found',nhinode,' Hinode FITS files', &
             ' slit pos', &
             minval(hinodexpos(1:nhinode)),&
             maxval(hinodexpos(1:nhinode)),&
             ' and scan# ',&
             minval(hinodescannr(1:nhinode)),&
             maxval(hinodescannr(1:nhinode))
        !check if Hinode data are fixed slit
        !scan: in that case use the scannr as
        !x-position
        hinode_fixedslit=0
        if (nhinode.ge.2) then
          if (minval(hinodexpos(1:nhinode)).eq.&
               maxval(hinodexpos(1:nhinode))) then
            if (ipt%verbose.ge.1) &
                 write(*,*) 'Hinode SP data is ''Fixed Slit Scan''.'//&
                 ' Using SCANNR for XPOS'
            hinode_fixedslit=1
          end if
        end if
     else
        if (ipt%verbose.ge.1) then
           write(*,*) &
             'Could not find any Hinode FITS file in '//trim(DATNAM)
         end if
        call stop
     end if
     hfile=file
  end if

  ui=0
  do i=1,nhinode
    if (hinode_fixedslit.eq.0) then 
      if ((hinodescannr(i).eq.scan).and.(hinodexpos(i).eq.xv)) ui=i
    else
      if (hinodescannr(i).eq.xv) ui=i
    end if
  end do
  if (ui.eq.0) then
     write(*,*) 'Could not find scan=',scan,' and xpos=',xv,' in '//trim(file)
     write(*,'(a,4(i5,a),i5)') 'Available positions: x=', &
          minval(hinodexpos(1:nhinode)),'-',&
          maxval(hinodexpos(1:nhinode)),', y=0-',naxis2-1,&
          ' and scan# ',&
          minval(hinodescannr(1:nhinode)),'-',&
          maxval(hinodescannr(1:nhinode))
     ok=.false.
     return
  end if
  
  fnew=hinodenam(ui)
  if (ipt%verbose.ge.2) write(*,*) 'Reading '//trim(fnew)//', y=',yv
  

  call readfits_hinode(fnew,yv,profile,naxes,error,ipt%verbose)
  if (error) then 
     ok=.false.
     return
  end if
  nwl=int(naxes(1),kind=2)
  
  fccx=trim(fnew)//'.ccx'
  allocate(icimg(1,naxes(2)))
  call read_ccx(fccx,profile%nwl,1,naxes(2), &
       delw,wref,wl_vec,icimg, &
       xerr,((i.eq.1).and.(ipt%verbose.ge.1)))
  if (xerr.ne.0) then
     if ((ipt%verbose.ge.2).and.(doshow)) then
        write(*,*) 'WARNING: No Hinode CCX file found: '//trim(fnew)//'.ccx'
        write(*,*) '--> Using WL-Calibration defined in ipt-file'
        write(*,*) '--> Using simple continuum calculation'
     end if
     call get_simple_cont(profile%i,nwl,icont)     
  else
     icont=icimg(1,yv+1)
  end if

!  profile%ic=icont
  profile%ic=1.
  profile%i(1:nwl)=profile%i(1:nwl)/icont
  profile%q(1:nwl)=profile%q(1:nwl)/icont
  profile%u(1:nwl)=profile%u(1:nwl)/icont
  profile%v(1:nwl)=profile%v(1:nwl)/icont
  do i=0,nwl-1 
     profile%wl(i+1)=i*DELW+WREF
  end do

  if (do_ls) then
     call get_lsprof(datnam,xv,yv,profile%wl,nwl,2_2,lsprof)
  end if

  deallocate(icimg)
  ok=.true.
end subroutine read_hinode
