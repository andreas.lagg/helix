module fits_comm
  use all_type
  implicit none

  character(LEN=maxstr) :: fst_atmfile,fst_proffile,asciistore(100)
  integer(4) :: atm_unit,prof_unit
  integer(2) :: verbose,asidx
  integer(4) :: nx,ny,nxp,nyp,nz,nzp,nwl,nstokes,npar,npixrep_atm,npixrep_prof
  integer(4) :: xoff,yoff,xoffp,yoffp,kb_count
  integer(4) :: naxesp(4),naxesa(3)
end module fits_comm

module fitsout
  use all_type
  implicit none
  public

contains

  !check if fits file can be extended.
  !the new file should only differ in the (x,y) size.
  !The fit parameters must be the same.
  !pixrep can be different
  !  function fitsout_append(iptin)
  subroutine fitsout_append(iptin,append)
    use fits_comm
    use tools
    implicit none
    type (ipttyp) :: iptin
    logical :: samepar,anyf
    character(LEN=68) :: oldpar(maxfitpar)
    integer(4) :: nfound,status,nparold,bsz,i,naxes(3),bitpix=-32
    integer(4) :: fpixels(3), lpixels(3), inc(3), append
    integer(4) :: nhdu,hdutype
    integer(4) :: xoffold,yoffold,xmm(2),ymm(2),xmmold(2),ymmold(2)
    character(LEN=72) :: comment
    real(4),allocatable :: data(:,:,:),dataold(:,:,:)

    append=0
    status=0
    atm_unit=25
    call ftgiou(atm_unit,status)
    call ftopen(atm_unit,trim(fst_atmfile),1,bsz,status) !  open the fits file
    if (status.ne.0) then
       write(*,*) 'Could not open FITS file: ',trim(fst_atmfile)
       call stop
    end if


    !check # of fitparameters
    call ftgkyj(atm_unit,'NFITPAR',nparold,comment,status)
    if (nparold.ne.npar) then
       write(*,*) 'Cannot extend FITS file: '//&
            'Different number of free parameters.'
       return
    end if

    !check name of fitparameters
    samepar=.true.
    call ftgkns(atm_unit,'PAR',1,nparold,oldpar(1:nparold),nfound,status)
    do i=1,nparold 
       samepar=(samepar.and.(oldpar(i)(1:idlen).eq.iptin%fitpar(i)(1:idlen)))
    end do
    if (.not.samepar) then
       write(*,*) 'Cannot extend atmosphere FITS file: '//&
            'Free parameters changed.'
       return
    end if

    !size & offset of old data
    call ftgknj(atm_unit,'NAXIS',1,3,naxes,nfound,status)
    naxesa=naxes(1:3)
    call ftgkyj(atm_unit,'XOFFSET',xoffold,comment,status)
    call ftgkyj(atm_unit,'YOFFSET',yoffold,comment,status)
    xmm=(/ xoffold , xoffold+naxes(1) /)
    ymm=(/ yoffold , yoffold+naxes(2) /)
    xmmold=xmm
    ymmold=ymm

    !delete all extensions (they are recreated anyway)
    nhdu=2
    status=0
    do while (status.eq.0)
       status=0
       call ftmahd(atm_unit,2,hdutype,status)
       if (status.eq.0) call ftdhdu(atm_unit,hdutype,status)
       nhdu=nhdu+1
    end do

    !same size? -> do nothing
    if ((xoff.ge.xoffold).and.(yoff.ge.yoffold).and.&
         (xoff+nx.le.naxes(1)+xoffold).and.(ny+yoff.le.naxes(2)+yoffold).and.&
         (nz.le.naxes(3))) then
       call ftclos(atm_unit,status)
       call ftfiou(atm_unit,status)
       xoff=xoffold
       yoff=yoffold
       nx=naxes(1)
       ny=naxes(2)
       nz=naxes(3)
       write(*,'(a,3i5)') 'Updating atmosphere FITS file. Size: ',naxes
       append=1
    else
       !determine new size
       if (xoff.lt.xmm(1)) xmm(1)=xoff
       if (yoff.lt.ymm(1)) ymm(1)=yoff
       if (xoff+nx.gt.xmm(2)) xmm(2)=xoff+nx
       if (yoff+ny.gt.ymm(2)) ymm(2)=yoff+ny
       if (naxes(3).gt.nz) nz=naxes(3)
       nx=xmm(2)-xmm(1)
       ny=ymm(2)-ymm(1)
       xoff=xmm(1)
       yoff=ymm(1)
       xmmold=xmmold-xoff+1
       ymmold=ymmold-yoff+1

       allocate(data(nx,ny,nz))
       allocate(dataold(naxes(1),naxes(2),naxes(3)))
       data=0.
       dataold=0.
       !get old data
       anyf=.false.
       inc(:)=1
       fpixels(:)=(/1,1,1/)
       lpixels(:)=naxes
       status=0
       call ftgsve(atm_unit,1,3,naxes,fpixels,lpixels,inc,0,&
            dataold,anyf,status)
       data(xmmold(1):xmmold(2)-1,ymmold(1):ymmold(2)-1,1:naxes(3))=dataold
       deallocate(dataold)
       !delete old file
       call ftdelt(atm_unit,status)
       call ftfiou(atm_unit,status)

       !open file with new size
       status=0
       bsz=1
       naxes(1)=nx
       naxes(2)=ny
       naxes(3)=nz
       naxesa=naxes(1:3)
       lpixels(:)=naxes
       write(*,'(a,3i5)') 'Extending atmopshere FITS file size to: ',naxes
       call ftgiou(atm_unit,status)
       call ftinit(atm_unit,trim(fst_atmfile),bsz,status) !  open the fits file
       call ftiimg(atm_unit,bitpix,3,naxes,status) !! new file
       call ftpsse(atm_unit,1,3,naxes,fpixels,lpixels,data,status)
       call ftclos(atm_unit,status)
       call ftfiou(atm_unit,status)

       deallocate(data)
       append=2
    end if
  end subroutine fitsout_append

  !same for profile fits file
  subroutine fitsout_pappend(append)
    use fits_comm
    use tools
    implicit none
    integer(4) :: nfound,status,bsz,naxes(4),bitpix=-32,bitpix64=-64
    integer(4) :: fpixels(4), lpixels(4), inc(4), append
    integer(4) :: hdutype
    integer(4) :: xoffold,yoffold,xmm(2),ymm(2),xmmold(2),ymmold(2),nwlold
    logical :: anyf
    character(LEN=72) :: comment
    real(4),allocatable :: data(:,:,:,:),dataold(:,:,:,:)
    real(4),allocatable :: ic(:,:),icold(:,:)
    real(8),allocatable :: wl(:,:,:),wlold(:,:,:)

    append=0
    status=0
    prof_unit=26
    call ftgiou(prof_unit,status)
    call ftopen(prof_unit,trim(fst_proffile),1,bsz,status) !  open the fits file

    !check #of WL points
    !size & offset of old data
    call ftgkyj(prof_unit,'NWL',nwlold,comment,status)

    if (nwl.ne.nwlold) then
       write(*,*) 'Cannot extend profile FITS file: '//&
            '# of WL-points changed.'
       return
    end if
    call ftgknj(prof_unit,'NAXIS',1,4,naxes,nfound,status)
    naxesp=naxes(1:4)
    call ftgkyj(prof_unit,'XOFFSET',xoffold,comment,status)
    call ftgkyj(prof_unit,'YOFFSET',yoffold,comment,status)
    xmm=(/ xoffold , xoffold+naxes(3) /)
    ymm=(/ yoffold , yoffold+naxes(4) /)
    xmmold=xmm
    ymmold=ymm

    !same size? -> do nothing
    if ((xoffp.ge.xoffold).and.(yoffp.ge.yoffold).and.&
         (xoffp+nxp.le.naxes(3)+xoffold).and.&
         (nyp+yoffp.le.naxes(4)+yoffold).and.&
         ((nwl*npixrep_prof).eq.naxes(1))) then
       call ftclos(prof_unit,status)
       call ftfiou(prof_unit,status)
       xoffp=xoffold
       yoffp=yoffold
       nzp=naxes(1)
       npixrep_prof=nzp/nwl
       nstokes=naxes(2)
       nxp=naxes(3)
       nyp=naxes(4)
       write(*,'(a,4i5)') 'Updating profile FITS file. Size: ',naxes(1:4)
       append=1
    else
       !determine new size
       if (xoffp.lt.xmm(1)) xmm(1)=xoffp
       if (yoffp.lt.ymm(1)) ymm(1)=yoffp
       if (xoffp+nxp.gt.xmm(2)) xmm(2)=xoffp+nxp
       if (yoffp+nyp.gt.ymm(2)) ymm(2)=yoffp+nyp
       nxp=xmm(2)-xmm(1)
       nyp=ymm(2)-ymm(1)
       nzp=naxes(1)
       if (naxes(1).lt.(nwl*npixrep_prof)) nzp=nwl*npixrep_prof
       npixrep_prof=nzp/nwl
       xoffp=xmm(1)
       yoffp=ymm(1)
       xmmold=xmmold-xoffp+1
       ymmold=ymmold-yoffp+1
       allocate(data(nzp,4,nxp,nyp))
       allocate(dataold(naxes(1),naxes(2),naxes(3),naxes(4)))
       allocate(ic(nxp,nyp))
       allocate(icold(naxes(3),naxes(4)))
       allocate(wl(nwl,nxp,nyp))
       allocate(wlold(nwl,naxes(3),naxes(4)))
       data=0.
       dataold=0.
       ic=0
       icold=0.
       wl=0.
       wlold=0
       !get old data
       anyf=.false.
       inc(:)=1
       !data
       fpixels(1:4)=(/1,1,1,1/)
       lpixels(:)=naxes
       status=0
       call ftgsve(prof_unit,1,4,naxes,fpixels,lpixels,inc,0,&
            dataold,anyf,status)
       data(1:naxes(1),1:4,xmmold(1):xmmold(2)-1,ymmold(1):ymmold(2)-1)=dataold
       deallocate(dataold)
       !icont
       call ftmahd(prof_unit,2,hdutype,status)
       call ftgsve(prof_unit,1,2,naxes(3:4),fpixels(3:4),lpixels(3:4), &
            inc(3:4),0,icold,anyf,status)
       ic(xmmold(1):xmmold(2)-1,ymmold(1):ymmold(2)-1)=icold
       deallocate(icold)
       !WL
       call ftmahd(prof_unit,3,hdutype,status)
       call ftgsvd(prof_unit,1,3,(/ nwl,naxes(3),naxes(4) /), &
            (/ fpixels(1),fpixels(3),fpixels(4) /),&
            (/ nwl,lpixels(3),lpixels(4) /),&
            (/ inc(1),inc(3),inc(4) /),0,wlold,anyf,status)
       wl(1:nwl,xmmold(1):xmmold(2)-1,ymmold(1):ymmold(2)-1)=wlold
       deallocate(wlold)

       !delete old file
       call ftdelt(prof_unit,status)
       call ftfiou(prof_unit,status)

       !open file with new size
       status=0
       bsz=1
       naxes(1)=nzp
       naxes(2)=4
       naxes(3)=nxp
       naxes(4)=nyp
       naxesp=naxes(1:4)
       fpixels(:)=1
       lpixels(:)=naxes
       write(*,'(a,4i5)') 'Extending profile FITS file size to: ',naxes(1:4)
       call ftgiou(prof_unit,status)
       call ftinit(prof_unit,trim(fst_proffile),bsz,status) !  open the fits file
       call ftiimg(prof_unit,bitpix,4,naxes,status) !! new file
       call ftpsse(prof_unit,1,4,naxes,fpixels,lpixels,data,status)
       deallocate(data)


!       call ftmahd(prof_unit,2,hdutype,status)
       call ftiimg(prof_unit,bitpix,2,naxes(3:4),status) !! new file
       call ftpsse(prof_unit,1,2,naxes(3:4),fpixels(3:4),lpixels(3:4),ic,status)
       deallocate(ic)

!       call ftmahd(prof_unit,3,hdutype,status)
       call ftiimg(prof_unit,bitpix64,3,(/ nwl,naxes(3),naxes(4) /),status)
       call ftpssd(prof_unit,1,3,(/ nwl,naxes(3),naxes(4) /), &
            (/ fpixels(1),fpixels(3),fpixels(4) /),&
            (/ nwl,lpixels(3),lpixels(4) /),wl,status)
       deallocate(wl)

       call ftclos(prof_unit,status)
       call ftfiou(prof_unit,status)
       append=2
    end if
  end subroutine fitsout_pappend

  !write out file as ascii table extension
  subroutine fitsout_asciifile(file,path,cmt)
    use all_type
    use fits_comm
    character(LEN=maxstr) :: file,path,fpath
    integer(4) :: iu
    integer(4) :: len,i,err,nl,status=0
    integer,dimension(1) :: tbcol
    character(len=16),dimension(1) :: tform,ttype,tunit,textension
    character(LEN=maxstr) :: cmt
    character(LEN=maxstr),dimension(maxcwl) :: line
    logical :: dowrite
    
    if (asidx.eq.1) then 
       dowrite=.true.
    else
       dowrite=.true.
       do i=1,asidx
          dowrite=(dowrite.and.(trim(asciistore(i)).ne.trim(file)))
       end do
    end if

    if (dowrite) then
       fpath=trim(path)//trim(file)
       iu=100
       open(iu,file=trim(fpath),iostat=err,action='READ',form='FORMATTED')
       i=1
       do while (err.eq.0)
          read(iu, fmt='(a)',iostat=err) line(i)
          i=i+1
       end do
       close(iu)
       nl=i-2

       !write out input file as ascii table extension
       len=maxval(len_trim(line(1:nl)))
       len=(len/80 +1)*80
       tbcol(1)=1
       write(tform(1),'("a",i4.4)') len
       ttype(1)='ASCII-Lines'
       textension(1)=trim(cmt)
       tunit(1)=' '
       !create fits extension
       status=0
       call ftitab(atm_unit,len,int(nl),1, &
            ttype,tbcol,tform,tunit,textension,status)
       call ftpkys(atm_unit,'ASCIINAM',trim(file),'filename',status)  
       call ftpkys(atm_unit,'ASCIIPTH',trim(path),'original path',status)  
       do i=1,nl
          call ftpcls(atm_unit,1,i,1,1,line(i),status)
       end do
       if (status.ne.0) then
          write(*,*) 'Problems in writing ASCII extension: '//trim(file)
       else
          asciistore(asidx)=trim(file)
          asidx=(asidx+1)
          if (asidx.gt.100) asidx=1
       end if
    end if
  end subroutine fitsout_asciifile

  !open fits for atmospheres
  subroutine fitsout_open(atm_file,prof_file,xin,yin,iptin)
    use fits_comm
    use tools
#ifdef INTEL
    use iflport
#endif
    implicit none
    character(LEN=maxstr) :: atm_file,prof_file
    integer(4) :: status,bsz=1,naxis,naxes(5),bitpix=-32
    integer(4) :: i,j,append
    integer(4) :: xin(2),yin(2)!,nparin,npixrep_atmin
    type (ipttyp) :: iptin
    character(LEN=maxstr) :: hostname,username,path,desc,ftmp
    character(maxllen) :: gitrevision
    integer(4) :: fstatus(13),ierr
#ifdef PGI
    integer :: hostnm
#endif
!    INCLUDE 'version.txt'
    call read_version(gitrevision)

    fst_atmfile=trim(atm_file)
    fst_proffile=trim(prof_file)

    nx=xin(2)-xin(1)+1
    ny=yin(2)-yin(1)+1
    nxp=xin(2)-xin(1)+1
    nyp=yin(2)-yin(1)+1
    xoff=xin(1)
    yoff=yin(1)
    xoffp=xin(1)
    yoffp=yin(1)
    !    npar=nparin
    !    npixrep_atm=npixrep_atmin
    npar=iptin%nfitpar
    if (iptin%pikaia_stat_cnt.ge.1) then
       npixrep_atm=iptin%pikaia_stat_cnt
    else
       npixrep_atm=iptin%pixelrep
    end if
    if (npixrep_atm.eq.0) npixrep_atm=1
    npixrep_prof=1
    verbose=iptin%verbose
    naxis=3
    nz=(npar+1)*npixrep_atm !parameters + fitness
    naxes(1)=nx
    naxes(2)=ny
    naxes(3)=nz
    naxesa=naxes(1:3)
    nwl=-1
    nzp=-1

    append=0
    if (verbose.ge.2) &
         write(*,*) 'Open FITS file for map output (atmosphere): '//trim(fst_atmfile)
    status=0
    atm_unit=25
    call ftgiou(atm_unit,status)
    call ftinit(atm_unit,fst_atmfile,bsz,status) !  open the fits file
    if (status.eq.105) then
       call fitsout_aclose(0)
       write(*,*) 'Atmosphere FITS File already exists: '//trim(fst_atmfile)
       call fitsout_append(iptin,append)
       if (append.eq.0) then 
          !          call delay_delete_file(fst_atmfile,6.)
          write(*,*) 'Please remove FITS file first.'
          call stop
       end if
       status=0
       call ftgiou(atm_unit,status)
       call ftopen(atm_unit,fst_atmfile,1,bsz,status) !  open the fits file
    end if
    if(status.ne.0) then
       write(*,*) 'could not open fits-file: '//trim(fst_atmfile)
       stop
    end if
    if (append.eq.0) then
       call ftiimg(atm_unit,bitpix,naxis,naxes,status) !! new file
    end if

    if (append.ne.1) then !rewrite header if fits dimension changed
       call ftpdat(atm_unit,status) !write date

       call ftpkys(atm_unit,'CODE','HELIX+','inversion code',status)
       call ftpkys(atm_unit,'GITREV',trim(gitrevision),'GIT reversion string',status)
!       call ftpkys(atm_unit,'RELDATE',svn_datstr,'SVN release date',status)
!       call ftpkys(atm_unit,'RELTIME',svn_timestr,'SVN release time',status)
!       call ftpkys(atm_unit,'REVISION',svn_revstr,'SVN reversion number',status)

       !write out user and host
#ifdef PGI
       call hostnm(hostname,ierr)
#else
       ierr=hostnm(hostname)
#endif
       call ftpkys(atm_unit,'HOSTNAME',trim(hostname),'hostname',status)
       call getlog(username)
       call ftpkys(atm_unit,'USERNAME',trim(username),'username',status)

       !write out header here
       call ftpkyj(atm_unit,'XOFFSET',int(xoff),'X-offset',status)
       call ftpkyj(atm_unit,'YOFFSET',int(yoff),'Y-offset',status)
       call ftpkyj(atm_unit,'PIXELREP',int(npixrep_atm),'pixel repetitions',status)

       !write out input file as ascii comment
       call ftpkys(atm_unit,'IPTFILE',trim(iptin%file),&
            'name of input file',status)
#ifndef PGI
       ierr=stat(trim(iptin%file),fstatus)
       call ftpkyj(atm_unit,'IPTSIZE',fstatus(8),'input file size',status)
       call ftpkys(atm_unit,'IPTDATE',trim(ctime(fstatus(10))),&
            'input file mod. date',status)
#endif
       call ftpkyj(atm_unit,'N_INPUT',int(iptin%n_ascii),&
            'number of lines in input file',status)
       call ftpcom(atm_unit,'FREE PARAMETERS',status)
       call ftpkyj(atm_unit,'NFITPAR',int(npar),'# of free parameters',status)
       do i=1,iptin%nfitpar
          call ftpkns(atm_unit,'PAR',i,1,iptin%fitpar(i),' ',status)
       end do
       call ftpkns(atm_unit,'PAR',iptin%nfitpar+1,1,'FITNESS',' ',status)

    end if

    !write out all files required for the inversion
    asidx=1
    desc='INPUTFILE'
    path=''
    call fitsout_asciifile(iptin%file,path,desc)
    !atomic data files
    desc='ATOM'
    do i=1,maxatm
       do j=1,maxlines
          if (len_trim(iptin%atom_file(i,j)).ge.2) then
             call fitsout_asciifile(iptin%atom_file(i,j), &
                  iptin%dir%atom,desc)
          end if
       end do
    end do
    !weighting files
    desc='WEIGHT'
    do i=1,maxmi
       if (len_trim(iptin%wgt_file(i)).ge.2) then
          call fitsout_asciifile(iptin%wgt_file(i),iptin%dir%wgt,desc)
       end if
    end do
    !convolution and prefilter
    desc='PREFILTER'
    path=''
    if (len_trim(iptin%prefilter).ge.2) then
       call fitsout_asciifile(iptin%prefilter,path,desc)
    end if
    desc='CONVOLUTION'
    if (len_trim(iptin%conv_func).ge.2) then
       call fitsout_asciifile(iptin%conv_func,path,desc)
    end if
    call ftflus(atm_unit,status)
  end subroutine fitsout_open

  subroutine fitsout_popen(stokes_nwl)
    use fits_comm
    integer(4) :: status,bsz=1,naxis,naxes(4),bitpix=-32,hdutype
    integer(4) :: append
    integer(2) :: stokes_nwl

    nstokes=4
    nwl=stokes_nwl
    if (nzp.lt.(nwl*npixrep_prof)) nzp=nwl*npixrep_prof
    npixrep_prof=nzp/nwl
    naxes(1)=nzp !number of WL points
    naxes(2)=nstokes
    naxes(3)=nxp
    naxes(4)=nyp
    naxesp=naxes(1:4)
    naxis=4

    append=0
    if (verbose.ge.2) &
         write(*,*) 'Open profile FITS file for map output (profile): '//&
         trim(fst_proffile)
    status=0
    prof_unit=26
    call ftgiou(prof_unit,status)
    call ftinit(prof_unit,fst_proffile,bsz,status) !  open the fits file
    if (status.eq.105) then
       call fitsout_pclose(0)
       write(*,*) 'Profile FITS File already exists: '//trim(fst_proffile)
       call fitsout_pappend(append)
       if (append.eq.0) then 
          !          call delay_delete_file(fst_atmfile,6.)
          write(*,*) 'Please remove profile FITS file first.'
          call stop
       end if
       status=0
       call ftgiou(prof_unit,status)
       call ftopen(prof_unit,fst_proffile,1,bsz,status) !  open the fits file
    end if
    if(status.ne.0) then
       write(*,*) 'could not open fits-file: '//trim(fst_proffile)
       stop
    end if
    if (append.eq.0) then
       call ftiimg(prof_unit,bitpix,naxis,naxes,status) !! new file
       !create first fits extension to hold IC image
       bitpix=-32
       call ftiimg(prof_unit,bitpix,2,(/ naxes(3),naxes(4) /),status)
       !create second fits extension for Wavelength array
       bitpix=-64
       call ftiimg(prof_unit,bitpix,3,(/ nwl,naxes(3),naxes(4) /),status)
    end if
    if (append.ne.1) then !rewrite header if fits dimension changed
       !primary data array
       call ftmahd(prof_unit,1,hdutype,status)
       call ftpdat(prof_unit,status) !write date
       call ftpkys(prof_unit,'CODE','HELIX+','inversion code',status)
       call ftpkys(prof_unit,'ATMFITS',trim(fst_atmfile),'atmospheric data file',status)
       call ftpkys(prof_unit,'STOKES','IQUV','order of stokes parameters',status)      
       call ftpkyj(prof_unit,'XOFFSET',int(xoffp),'X-offset',status)
       call ftpkyj(prof_unit,'YOFFSET',int(yoffp),'Y-offset',status)
       call ftpkyj(prof_unit,'NWL',int(nwl),'# of wavelength points',status)
       call ftpkyj(prof_unit,'PIXELREP',int(npixrep_prof),'max. pixel repetitions',status)
       !continuum image
       call ftmahd(prof_unit,2,hdutype,status)
       call ftpkyu(prof_unit,'ICONTIMG','continuum image',status)
      !wavelength array
       call ftmahd(prof_unit,3,hdutype,status)
       call ftpkyu(prof_unit,'WLVEC','wavelength vector',status)

    end if

    call ftflus(prof_unit,status)
    
  end subroutine fitsout_popen

!##################### open fits for profiles -------------


  subroutine fitsout_aclose(msg)
    use fits_comm
    implicit none
    integer(4) :: status=0,msg

    call ftclos(atm_unit,status)
    call ftfiou(atm_unit,status)
    if (msg.ne.0) then
       write(*,*) 'FITS-file (atmospheres) written: '//trim(fst_atmfile)
    end if
  end subroutine fitsout_aclose

  subroutine fitsout_pclose(msg)
    use fits_comm
    implicit none
    integer(4) :: status=0,msg

    call ftclos(prof_unit,status)
    call ftfiou(prof_unit,status)
    if (msg.ne.0) then
       write(*,*) 'FITS-file (profiles) written: '//trim(fst_proffile)
    end if
  end subroutine fitsout_pclose

  subroutine fitsout_close(msg)
    implicit none
    integer(4) :: msg

    call fitsout_aclose(msg)
    call fitsout_pclose(msg)
  end subroutine fitsout_close

  subroutine fitsout_write(xp,yp,ipr,par,stokes_nwl,&
       stokes_ic,stokes_wl,stokes_i,stokes_q,stokes_u,stokes_v,&
       fitness,keepbest)
    use fits_comm
    implicit none
    integer(4) :: xp,yp,ipr,fpos4(4,4),lpos4(4,4),fpos(4),lpos(4),ii,iprp
    integer(2) :: keepbest,stokes_nwl
    real(4) :: par(maxfitpar),fitness,prm,oldfitness,fittmp
    real(4) :: stokes_ic
    real(4),dimension(maxwl) :: stokes_i,stokes_q,stokes_u,stokes_v
    real(8),dimension(maxwl) :: stokes_wl
    integer(4) :: status(12),iz,hdutype
    integer(8) :: offs!,offp,offp0,offp1,offp2,offp3
    logical :: anyf,dowrite

    if (nwl.eq.-1) then
       call fitsout_popen(stokes_nwl)
    end if

    status=0
    !move to first hdu
    call ftmahd(atm_unit,1,hdutype,status(1))
    !read old fitness value
    offs=1+((npar+1-1+(ipr-1)*(npar+1))*ny+(yp-yoff))*nx+(xp-xoff)
    !    offs=1+((ipr*(npar+1)-1)*ny+(yp-yoff))*nx+(xp-xoff)
    if (ipr.le.1) then
       kb_count=0
    end if
    call ftgpve(atm_unit,1,offs,1,0,oldfitness,anyf,status(2))
    if ((keepbest.eq.0).or.(fitness.gt.oldfitness)) then
       if ((oldfitness.gt.0).and.(keepbest.eq.1)) then
          if (verbose.ge.3) &
               write(*,'(''x'',i4.4,''y'',i4.4,'', REP='',i4.4,a,f8.3,a,f8.3)')&
               xp,yp,ipr,': KEEPBEST: old=',oldfitness,', new=',fitness
          kb_count=kb_count+1
       end if
       do iz=1,npar+1
          offs=1+int(((iz-1+(ipr-1)*(npar+1))*ny+(yp-yoff)),8)*nx+(xp-xoff)
          !! write fit parameters to file
          if (iz.eq.npar+1) then
             prm=fitness
          else
             prm=par(iz)
          end if
          call ftppre(atm_unit,1,offs,1,prm,status(3))
       end do
       !check if prof file was already opened with correct NWL
       call ftmahd(prof_unit,1,hdutype,status(4))

       !pxrep mode: write out only fittest profile
       iprp=ipr
       dowrite=.true.
       if (iprp.ge.npixrep_prof) then
          iprp=npixrep_prof
          oldfitness=fitness
          do ii=1,ipr-1
             offs=1+int(((npar+(ii-1)*(npar+1))*ny+(yp-yoff)),8)*nx+(xp-xoff)
             call ftgpve(atm_unit,1,offs,1,0,fittmp,anyf,status(2))
             if (fittmp.gt.oldfitness) then
                oldfitness=fittmp
             end if             
          end do
          dowrite=((ipr.eq.1).or.(fitness.gt.oldfitness))
       end if

       if (dowrite) then
          do ii=1,4
             fpos4(:,ii)=[(iprp-1)*nwl+1,ii,xp-xoffp+1,yp-yoffp+1]
             lpos4(:,ii)=[(iprp-1)*nwl+stokes_nwl,ii,xp-xoffp+1,yp-yoffp+1]
          end do
          call ftpsse(prof_unit,1,4,naxesp,fpos4(:,1),lpos4(:,1),stokes_i(1:stokes_nwl),status(5))
          call ftpsse(prof_unit,1,4,naxesp,fpos4(:,2),lpos4(:,2),stokes_q(1:stokes_nwl),status(6))
          call ftpsse(prof_unit,1,4,naxesp,fpos4(:,3),lpos4(:,3),stokes_u(1:stokes_nwl),status(7))
          call ftpsse(prof_unit,1,4,naxesp,fpos4(:,4),lpos4(:,4),stokes_v(1:stokes_nwl),status(8))


          !continuum image
          call ftmahd(prof_unit,2,hdutype,status(9))
          fpos(1:2)=[xp-xoffp+1,yp-yoffp+1]
          call ftpsse(prof_unit,1,2,[naxesp(3),naxesp(4)],fpos(1:2),fpos(1:2),stokes_ic,status(10))
          !wavelength vector
          call ftmahd(prof_unit,3,hdutype,status(11))
          fpos(1:3)=[1,xp-xoffp+1,yp-yoffp+1]
          lpos(1:3)=[int(stokes_nwl),xp-xoffp+1,yp-yoffp+1]
          call ftpssd(prof_unit,1,3,[nwl,naxesp(3),naxesp(4)],fpos(1:3),lpos(1:3),stokes_wl(1:stokes_nwl),status(12))
       end if
    end if
    if (maxval(status).ne.0) then
       write(*,*) 'Problem writing FITS file, status: ',status
       write(*,*) 'Variables: ',xp,yp,ipr,stokes_nwl
       write(*,*) 'FITSCOMM: ', nx,ny,nxp,nyp,nz,nzp,nwl,nstokes, npar,npixrep_atm,npixrep_prof, xoff,yoff,xoffp,yoffp
!       write(*,*) 'offs,offp: ',offs,offp0,offp1,offp2,offp3
!       if (maxval([offs,offp0,offp1,offp2,offp3]).ge.2**31) then 
!          write(*,*) 'Note: CFITSIO only supports file sizes up to 2**31 bytes. For larger files CFITSIO must be recompiled with the flag -D_FILE_OFFSET_BITS=64 and -D_LARGEFILE_SOURCE'
!       end if
    end if
    call ftflus(atm_unit,status)
    call ftflus(prof_unit,status)

    if ((keepbest.ge.1).and.(ipr.eq.npixrep_atm).and.(kb_count.ge.1)) then
        write(*,'(''   x'',i4.4,''y'',i4.4,a,i3,''/'',i3,a)')&
             xp,yp,': KEEPBEST replaced ',kb_count,npixrep_atm,' genes of PIKAIA population.'
    end if

  end subroutine fitsout_write

#ifdef MPI
  subroutine fitsout_MPI_recv(from)
    !    use nxtval
    use all_type
    implicit none
    include 'mpif.h'
    integer(4) :: status(MPI_STATUS_SIZE),dstat(MPI_STATUS_SIZE),ierr,from
    integer(4) :: xp,yp,ipr,npar,intnwl,intnpar
    integer(2) :: keepbest,stokes_nwl
    real(4) :: par(maxfitpar),fitness
    real(4) :: stokes_ic
    real(4),dimension(maxwl) :: stokes_i,stokes_q,stokes_u,stokes_v
    real(8),dimension(maxwl) :: stokes_wl

    ierr=0
    call MPI_RECV(npar,1,MPI_INTEGER4,from,mpitag1,MPI_COMM_WORLD,status,ierr)
    intnpar=int(npar)
    call MPI_RECV(xp,1,MPI_INTEGER4,from,mpitag2,MPI_COMM_WORLD,dstat,ierr)
    call MPI_RECV(yp,1,MPI_INTEGER4,from,mpitag3,MPI_COMM_WORLD,dstat,ierr)
    call MPI_RECV(ipr,1,MPI_INTEGER4,from,mpitag4,MPI_COMM_WORLD,dstat,ierr)
    call MPI_RECV(par,intnpar,MPI_REAL,from,mpitag5,MPI_COMM_WORLD,dstat,ierr)
    call MPI_RECV(fitness,1,MPI_REAL,from,mpitag6,MPI_COMM_WORLD,dstat,ierr)
    call MPI_RECV(keepbest,1,MPI_INTEGER2,from,mpitag7,MPI_COMM_WORLD,dstat,ierr)
    call MPI_RECV(stokes_ic,1,MPI_REAL,from,mpitag8,MPI_COMM_WORLD,dstat,ierr)
    call MPI_RECV(stokes_nwl,1,MPI_INTEGER2,from,mpitag9, &
         MPI_COMM_WORLD,dstat,ierr)
    intnwl=int(stokes_nwl)
    call MPI_RECV(stokes_i(1),intnwl,MPI_REAL4,from,mpitag10,MPI_COMM_WORLD,&
         dstat,ierr)
    call MPI_RECV(stokes_q(1),intnwl,MPI_REAL4,from,mpitag11,MPI_COMM_WORLD,&
         dstat,ierr)
    call MPI_RECV(stokes_u,intnwl,MPI_REAL,from,mpitag12,MPI_COMM_WORLD,&
         dstat,ierr)
    call MPI_RECV(stokes_v,intnwl,MPI_REAL,from,mpitag13,MPI_COMM_WORLD,&
         dstat,ierr)
    call MPI_RECV(stokes_wl,intnwl,MPI_REAL8,from,mpitag14,MPI_COMM_WORLD,&
         dstat,ierr)


    call fitsout_write(xp,yp,ipr,par,stokes_nwl,&
         stokes_ic,stokes_wl,stokes_i,stokes_q,stokes_u,stokes_v,fitness,&
         keepbest)
    !    write(*,*) 'FITSOUT-MPI-RECV ',xp,yp,ipr,npar
    !    write(*,*) 'FITSOUT-MPI-RECV: par= ',par(1:4)
  end subroutine fitsout_MPI_recv

  subroutine fitsout_MPI_write(counter_comm,xp,yp,ipr,par,npar,stokes_nwl,&
       stokes_ic,stokes_wl,stokes_i,stokes_q,stokes_u,stokes_v,&
       fitness,keepbest)
    use all_type
    implicit none
    include 'mpif.h'
    integer(4) :: status(MPI_STATUS_SIZE),ierr
    integer(4) :: xp,yp,ipr,npar,counter_comm,intnwl
    integer(2) :: keepbest,stokes_nwl
    real(4) :: par(maxfitpar),fitness
    real(4) :: stokes_ic
    real(4),dimension(maxwl) :: stokes_i,stokes_q,stokes_u,stokes_v
    real(8),dimension(maxwl) :: stokes_wl

!        write(*,*) 'FITSOUT-MPI-WRITE: sending ',xp,yp,ipr,npar

    ierr=0
    !send the command that the master is listening to the parameter sending
    !203=FITS
    call MPI_SEND(MPI_BOTTOM,0,MPI_INTEGER,0,203,counter_comm,ierr)
    !send the parameters
    call MPI_SEND(npar,1,MPI_INTEGER4,0,mpitag1,MPI_COMM_WORLD,status,ierr)
    call MPI_SEND(xp,1,MPI_INTEGER4,0,mpitag2,MPI_COMM_WORLD,status,ierr)
    call MPI_SEND(yp,1,MPI_INTEGER4,0,mpitag3,MPI_COMM_WORLD,status,ierr)
    call MPI_SEND(ipr,1,MPI_INTEGER4,0,mpitag4,MPI_COMM_WORLD,status,ierr)
    call MPI_SEND(par,int(npar),MPI_REAL,0,mpitag5,MPI_COMM_WORLD,status,ierr)
    call MPI_SEND(fitness,1,MPI_REAL,0,mpitag6,MPI_COMM_WORLD,status,ierr)
    call MPI_SEND(keepbest,1,MPI_INTEGER2,0,mpitag7,MPI_COMM_WORLD,status,ierr)
    call MPI_SEND(stokes_ic,1,MPI_REAL,0,mpitag8,MPI_COMM_WORLD,status,ierr)
    call MPI_SEND(stokes_nwl,1,MPI_INTEGER2,0,mpitag9,MPI_COMM_WORLD,status,ierr)
    intnwl=int(stokes_nwl)
    call MPI_SEND(stokes_i,intnwl,MPI_REAL,0,mpitag10,MPI_COMM_WORLD,status,ierr)
    call MPI_SEND(stokes_q,intnwl,MPI_REAL,0,mpitag11,MPI_COMM_WORLD,&
         status,ierr)
    call MPI_SEND(stokes_u,intnwl,MPI_REAL,0,mpitag12,MPI_COMM_WORLD,&
         status,ierr)
    call MPI_SEND(stokes_v,intnwl,MPI_REAL,0,mpitag13,MPI_COMM_WORLD,&
         status,ierr)
    call MPI_SEND(stokes_wl,intnwl,MPI_REAL8,0,mpitag14,MPI_COMM_WORLD,&
         status,ierr)
  end subroutine fitsout_MPI_write
#endif
end module fitsout
