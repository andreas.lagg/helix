function get_line(parnam,fit,par,scl)
  use all_type
  use ipt_decl
  implicit none
!  type (ipttyp) :: ipt
  integer(2) :: ip
  character(maxstr) :: get_line,fmtstr
  type(iptyp) :: parnam
  integer(2) :: fit
  logical :: val
  real(4) :: par
  type (mmtyp) :: scl

!  common /iptcom/ ipt

  val=.false.
  do ip=1,ipt%nparset
     val=(val.or.((trim(ipt%parset(ip))).eq.(trim(parnam%prg))))
  end do

  get_line=" "
  if (val) then
     fmtstr='(i2,f14.3,2x,''('',f10.2,'' to '',f10.2,'')'')'
     write(unit=get_line,fmt=fmtstr) fit,par,scl
  else
     get_line=" "
  end if

end function get_line

subroutine nice_ticks(nticks,yrg,tickstrt,tickspc)
  implicit none
  integer(4) :: nticks
  real(4) :: yrg(2),dyrg,tickspc,tickstrt

  dyrg=yrg(2)-yrg(1)
  tickspc=dyrg/nticks
  tickspc=10**real(floor(log10(tickspc)))
  do while ((dyrg/tickspc).gt.nticks)
     tickspc=2*tickspc
  end do
  tickstrt=real(floor(yrg(1)/tickspc)+1)*tickspc
end subroutine nice_ticks

subroutine atm_line(natm,atm,scl,retline,npar,nline,lin,nblend,blend,gen)
  use tools
  use all_type
  use ipt_decl
  implicit none
!  type (ipttyp) :: ipt
  integer(2) :: natm,npar,nline,nblend
  real(4) :: val
  type (scltyp) :: scl(natm)
  type (atmtyp) :: atm(natm)
  type (linetyp) :: lin(nline)
  type (blendtyp) :: blend(nblend)
  type (gentyp) :: gen
  character(maxstr) :: rl(natm,npartot),cmpstr(natm),nam, &
       valstr,linstr,get_line
  character(maxlonstr) :: retline(50),cline
  character(idlen) :: pnmarr(npartot),linid
  character(2) :: nstr
  integer(2) :: ia,ip,icnt,len,il,ib
  type(parnametyp) :: pnm
!  external get_line
  
!  common /iptcom/ ipt

  data pnmarr/'B','AZI','INC','VLOS','WIDTH','DAMP','DOPP','A0','SZERO', &
       'SGRAD','ETAZERO','GDAMP','VMICI','DENSP','TEMPE','DSLAB','HEIGHT','FF'/
  
  call def_parname(pnm)

  do ia=1,natm
     rl(ia, 1)=get_line(pnm%b,atm(ia)%fit%b,atm(ia)%par%b, &
          scl(ia)%b)
     rl(ia, 2)=get_line(pnm%azi,atm(ia)%fit%azi,atm(ia)%par%azi, &
          scl(ia)%azi)
     rl(ia, 3)=get_line(pnm%inc,atm(ia)%fit%inc,atm(ia)%par%inc, &
          scl(ia)%inc)
     rl(ia, 4)=get_line(pnm%vlos,atm(ia)%fit%vlos,atm(ia)%par%vlos, &
          scl(ia)%vlos)
     rl(ia, 5)=get_line(pnm%width,atm(ia)%fit%width,atm(ia)%par%width, &
          scl(ia)%width)
     rl(ia, 6)=get_line(pnm%damp,atm(ia)%fit%damp,atm(ia)%par%damp, &
          scl(ia)%damp)
     rl(ia, 7)=get_line(pnm%dopp,atm(ia)%fit%dopp,atm(ia)%par%dopp, &
          scl(ia)%dopp)
     rl(ia, 8)=get_line(pnm%a0,atm(ia)%fit%a0,atm(ia)%par%a0, &
          scl(ia)%a0)
     rl(ia, 9)=get_line(pnm%szero,atm(ia)%fit%szero,atm(ia)%par%szero, &
          scl(ia)%szero)
     rl(ia,10)=get_line(pnm%sgrad,atm(ia)%fit%sgrad,atm(ia)%par%sgrad, &
          scl(ia)%sgrad)
     rl(ia,11)=get_line(pnm%etazero,atm(ia)%fit%etazero, &
          atm(ia)%par%etazero,scl(ia)%etazero)
     rl(ia,12)=get_line(pnm%gdamp,atm(ia)%fit%gdamp,atm(ia)%par%gdamp, &
          scl(ia)%gdamp)
     rl(ia,13)=get_line(pnm%vmici,atm(ia)%fit%vmici,atm(ia)%par%vmici, &
          scl(ia)%vmici)
     rl(ia,14)=get_line(pnm%densp,atm(ia)%fit%densp,atm(ia)%par%densp, &
          scl(ia)%densp)
     rl(ia,15)=get_line(pnm%tempe,atm(ia)%fit%tempe,atm(ia)%par%tempe, &
          scl(ia)%tempe)
     rl(ia,16)=get_line(pnm%dslab,atm(ia)%fit%dslab,atm(ia)%par%dslab, &
          scl(ia)%dslab)
     rl(ia,17)=get_line(pnm%height,atm(ia)%fit%height,atm(ia)%par%height, &
          scl(ia)%height)
     rl(ia,18)=get_line(pnm%ff,atm(ia)%fit%ff,atm(ia)%par%ff, &
          scl(ia)%ff)

     write(unit=nstr,fmt='(i2)') ia
     cmpstr(ia)='Component'//nstr
  end do

  write(unit=nstr,fmt='(i2.2)') natm
  retline=" "

  len=8+natm*(1+44)
  retline(1)=repeat('=',len)
  write(unit=retline(2),fmt='(a8,'//nstr//'(''|'',a44))') 'Par',cmpstr
  retline(3)=repeat('-',len)

  icnt=3
  do ip=1,npartot
     if (len_trim(rl(1,ip)).gt.1) then 
        icnt=icnt+1
        write(unit=retline(icnt),fmt='(a8,'//nstr//'(''|'',a44))') &
             pnmarr(ip),rl(:,ip)
     end if
  end do
  retline(icnt+1)=repeat('-',len)
  icnt=icnt+1

  if (maxval(blend(1:nblend)%par%a0).ne.0) then
     do ib=1,nblend
        icnt=icnt+1
        write(unit=retline(icnt),fmt='(a13,f12.5,3(''|'',a13,f10.4))') &
             'BLEND_WL    ',blend(ib)%par%wl, &
             'BLEND_WIDTH ',blend(ib)%par%width, &
             'BLEND_DAMP  ',blend(ib)%par%damp, &
             'BLEND_A0    ',blend(ib)%par%a0
     end do
     retline(icnt+1)=repeat('-',len)
     icnt=icnt+1     
  end if
  if ((gen%fit%ccorr.ne.0).or.(gen%fit%radcorrsolar.ne.0).or.&
       (gen%fit%straylight.ne.0))  then
     write(unit=retline(icnt),&
          fmt='(a7,f12.5,'' | '',a12,f12.5,'' | '',a12,f12.5)') &
          'CCORR_SOLAR ',gen%par%ccorr,&
          'STRAYLIGHT ',gen%par%straylight,&
          'RADCORRSOLAR ',gen%par%radcorrsolar
     icnt=icnt+1          
  end if
  
  !line parameters (straypol ampl.)
  icnt=icnt+1
  cline=' '
  if (ipt%straypol_corr.ge.1) then
     do il=1,nline
        nam=' '
        write(unit=linid,fmt='(12a)') char(lin(il)%id)
        select case (ipt%modeval)
        case (0_2) !GAUSS
           val=lin(il)%par%straypol_amp
           nam='STRAYPOL_AMP'
        case (1_2) !VOIGT
           val=lin(il)%par%straypol_eta0
           nam='STRAYPOL_ETA0'
        case (3_2) !VOIGT
           val=lin(il)%par%straypol_eta0
           nam='STRAYPOL_ETA0'
        case (4_2) !VOIGT
           val=lin(il)%par%straypol_eta0
           nam='STRAYPOL_ETA0'
        case (5_2) !VOIGT
           write(*,*) 'No straylight correction for Hanle-slab model'
        case default
           write(*,*) 'unknown mode for straylight-correction. ipt%modeval=',ipt%modeval
        end select
        if (il.eq.1) cline(1:len_trim(nam))=nam(1:len_trim(nam))
        write(unit=valstr,fmt='(f8.5)') val
        linstr=linid(1:11)
        cline=cline(1:len_trim(cline))//' ('//linstr(1:11)//': '//valstr(1:len_trim(valstr))//')'
     end do
  end if
  retline(icnt)=cline(1:len_trim(cline))

  npar=icnt

end subroutine atm_line

subroutine plot_profile(plot_title,obs,nprof,iquv_flag,atm, &
     scl,natm,wgt,nwgt,fitness,lin,nline,nblend,blend,gen)
  use all_type
  use ipt_decl
#ifdef X11
  use dislin
#endif
  implicit none
!  type (ipttyp) :: ipt
  logical :: wopen
  logical :: heonly
  logical :: first
  integer(4) :: winx,winy,pos(2),siz(2),dist,dy,ypos,xpos,myheight,small
  integer(4) :: iquv_flag(4),ntot
  integer(4) :: ip,i,symm,io,nprof,il,hgt,nwgt,iw,idwin,iadd
  integer(2) :: good_wl(2),natm,atm_flag,npar,nline,nblend
  character(5) :: ytit(4)
  character(10) :: clr(maxplotprof)
  character(50) :: fitstr
  character(maxstr) :: plot_title,cbuf
  character(maxlonstr) :: textline(50)
  real(4) :: ydat(maxwl),yrg(2),dyrg,ytickspc,ytickstrt,wgtdat(maxwl), &
       maxwgt,fitness,xtickspc,xtickstrt,xrg(2)
  type (proftyp) :: obs(nprof),wgt(nwgt)
  type (atmtyp) :: atm(maxatm)
  type (linetyp) :: lin(maxlin)
  type (scltyp) :: scl(maxatm)
  type (blendtyp) :: blend(maxblend)
  type (gentyp) :: gen
!  type (atmtyp) :: atm(natm)
!  type (linetyp) :: lin(nline)
!  type (scltyp) :: scl(natm)
!  type (blendtyp) :: blend(nblend)

!  common /iptcom/ ipt
  common /win/ wopen

  data clr/'FORE','RED','GREEN','BLUE','MAGENTA','CYAN','ORANGE','YELLOW',&
       'GREEN','BLUE','RED','GREEN','BLUE','MAGENTA','CYAN','ORANGE'/

  ytit(1)='I'
  ytit(2)='Q/Ic'
  ytit(3)='U/Ic'
  ytit(4)='V/Ic'
  ntot=sum(iquv_flag)
  atm_flag=0
  if (natm.gt.0) atm_flag=1

#ifdef X11
  winx=600
  winy=int(winx/21.*29.7)

  pos=(/350,2870/)
  siz=(/1500,2770/)
  dist=40
  myheight=30
  small=myheight*0.75
  if (blend(1)%par%a0.ne.0) then
     pos(2)=pos(2)-(nblend+1)*dist
     siz(2)=siz(2)-(nblend+1)*dist
  end if
  if ((gen%fit%ccorr.ne.0).or.(gen%fit%straylight.ne.0).or.(gen%fit%radcorrsolar.ne.0)) then
     pos(2)=pos(2)-dist
     siz(2)=siz(2)-dist
  end if

  if (wopen) then
     CALL ERASE()
     CALL WINID(idwin)
  else
     CALL METAFL('XWIN') 
     CALL SCRMOD('REVERSE')
     CALL X11MOD('STORE')
     CALL SETPAG('DA4P')
     CALL WINSIZ(winx,winy)
     !      CALL SETXID(1,'WINDOW')
     CALL DISINI()
     wopen=.true.
  end if

  CALL HEIGHT(myheight)
  CALL ERRMOD('ALL','OFF')
  CALL ERRMOD('WARNINGS','ON')
  if (ipt%verbose.ge.3) CALL ERRMOD('CHECK','ON')
!  if (ipt%verbose.ge.2) CALL ERRMOD('PROTOCOL','ON')

  CALL WINMOD('FULL')
  CALL PAGERA() !border around page
!  CALL X11FNT('ARIAL')
  CALL SIMPLX() !font

  CALL NAME(' ','X')
  dy=int(real(siz(2) - dist*(ntot+1+atm_flag))/(ntot+atm_flag))
  CALL AXSLEN(siz(1),dy)
  CALL LABDIG(-1,'X')
  CALL TICKS(5,'Y')
  CALL TICKS(10,'X')
  CALL LABELS('NONE','X')
  CALL TITJUS('LEFT')
  CALL TITLIN(plot_title,3)
!  CALL TITLIN('SIN(X), COS(X)',3)
  write(unit=fitstr,fmt='(f7.2)') fitness
  CALL TITLIN('Fitness: '//fitstr,4)
  CALL NOCHEK

  !define wl-range for automatic scaling
  heonly=.true.
  do il=1,ipt%nline
     if ((ipt%line(il)%id(1).ne.72).or.(ipt%line(il)%id(2).ne.101)) &
          heonly=.false.
  end do

!determine wl-range
  xrg=(/ 1e10,0e0 /)
  do io=1,nprof
     xrg(1)=minval( (/ xrg(1),real(minval(obs(io)%wl(1:obs(io)%nwl))) /) )
     xrg(2)=maxval( (/ xrg(2),real(maxval(obs(io)%wl(1:obs(io)%nwl))) /) )
  end do

!set x-axis labelling  
  if ((xrg(2)-xrg(1)).ge.4) then
     xtickstrt=real(int(xrg(1)/2.+1)*2)
     xtickspc=1.
  else if ((xrg(2)-xrg(1)).ge.2) then
     xtickstrt=real(int(xrg(1))+1)
     xtickspc=.5
  else
     xtickstrt=real(int(xrg(1)*10)+1)/10
     xtickspc=.2
  end if


  i=1
  symm=1
  do ip=1,4
     if (iquv_flag(ip).eq.1) then

        first=.true.
        do io=1,nprof
           if (heonly) then
              good_wl=(/minloc(abs(obs(io)%wl(1:obs(io)%nwl)-10828.)), &
                   minloc(abs(obs(io)%wl(1:obs(io)%nwl)-10835.))/)
           else
              good_wl=(/1,int(obs(io)%nwl)/)
           end if

           symm=1
           select case(ip)
           case (1)
              ydat=obs(io)%i
              symm=0
           case (2)
              ydat=obs(io)%q
           case (3)
              ydat=obs(io)%u
           case (4)
              ydat=obs(io)%v
           end select

           yrg=(/minval(ydat(good_wl(1):good_wl(2))), &
                maxval(ydat(good_wl(1):good_wl(2)))/)

           !do scaling according to weighting
           if (nwgt.gt.0) then
              do iw=1,obs(io)%nwl
                 if ((maxval(wgt%i(iw)).gt.1e-5).or.&
                      (maxval(wgt%q(iw)).gt.1e-5).or.&
                      (maxval(wgt%u(iw)).gt.1e-5).or.&
                      (maxval(wgt%v(iw)).gt.1e-5)) then
                    if (first) then
                       yrg=(/ydat(iw),ydat(iw)/)
                       first=.false.
                    else
                       yrg=(/minval((/yrg(1),ydat(iw)/)),&
                            maxval((/yrg(2),ydat(iw)/))/)
                    end if
                 end if
              end do
           end if
           if (ip.eq.1) then
              yrg(2)=maxval((/yrg(2),obs(io)%ic/))
           end if

           if (symm.eq.1) yrg=(/-maxval(abs(yrg)),maxval(abs(yrg))/)
           if (sum(abs(yrg)).lt.1e-8) then 
              yrg=(/-1e-8,1e-8/)
           end if
           dyrg=yrg(2)-yrg(1)
           yrg=(/yrg(1)-0.1*dyrg,yrg(2)+0.1*dyrg/)
           dyrg=yrg(2)-yrg(1)
           if (io.eq.1) then
              CALL AXSPOS(pos(1),pos(2)-(ntot+atm_flag-i)*(dist+dy))
              CALL NAME(ytit(ip),'Y')
              CALL LABELS('EXP','Y')
              if (i.eq.ntot) then
                 CALL NAME('WL [A]','X')
                 CALL LABELS('FLOAT','X')
              end if

              call nice_ticks(6,yrg,ytickstrt,ytickspc)

              CALL SETGRF('NAME','NAME','TICKS','NONE')
              CALL FRAME(1)
!              CALL GRAF(real(ipt%wl_range(1)),real(ipt%wl_range(2)),&
              CALL GRAF(xrg(1),xrg(2), &
                   xtickstrt,xtickspc, &
                   yrg(1),yrg(2),ytickstrt,ytickspc)

              if (i.eq.1) CALL TITLE()
           end if
           CALL COLOR('FORE')
           CALL DASH()
           if ((ip.eq.1).and.(io.eq.1)) then
              CALL CURVE(real(xrg),(/1,1/)*obs(io)%ic,2)
           else
              CALL XAXGIT()
           end if
           CALL SOLID()

           CALL COLOR(clr(io))
           CALL CURVE(real(obs(io)%wl),ydat,int(obs(io)%nwl))

        end do
        
        !legend
        CALL HEIGHT(small)
        CALL COLOR('FORE')
        iadd=0
        if (ip.eq.1) iadd=1
        CALL LEGINI(cbuf,nprof+iadd,7)
        CALL LEGPOS(pos(1)+50,pos(2)-(ntot+atm_flag-i+1)*(dist+dy)+dy/5)
        if (ip.eq.1) CALL LEGLIN(cbuf,'CONT',1)
        do io=1,nprof
           CALL LEGLIN(cbuf,obs(io)%name,io+iadd)
        end do
        CALL LEGTIT('')
        CALL LEGEND(cbuf,3)
        CALL HEIGHT(myheight)


        i=i+1
     end if
     CALL ENDGRF

        !plot weighting function
     if (nwgt.gt.0) then
        CALL COLOR('GREEN')
        CALL SETGRF('NONE','NONE','NONE','NAME')
        CALL NAME('Weight','Y')
        CALL LABELS('FLOAT','Y')
        CALL FRAME(0)
        CALL DASH()
        CALL LABDIG(2,'Y')
        do iw=1,nwgt
           select case (ip)
             case (1)
                wgtdat=wgt(iw)%i
             case (2)
                wgtdat=wgt(iw)%q
             case (3)
                wgtdat=wgt(iw)%u
             case (4)
                wgtdat=wgt(iw)%v
           end select
           maxwgt=maxval(wgtdat)
           if (maxwgt.lt.1e-5) maxwgt=1e-5
           yrg=(/0.,maxwgt*1.05/)
           call nice_ticks(6,yrg,ytickstrt,ytickspc)
           CALL GRAF(real(xrg(1)),real(xrg(2)),&
                xtickstrt,xtickspc, &
                yrg(1),yrg(2),ytickstrt,ytickspc)
           CALL CURVE(real(wgt(iw)%wl),wgtdat,int(wgt(iw)%nwl))
        end do
        CALL LABDIG(1,'Y')
        CALL ENDGRF
     end if
     CALL COLOR('FORE')
     CALL SOLID()
  end do

  !text output of atmospheric parameters
  if (atm_flag.eq.1) then
     call atm_line(natm,atm,scl,textline,npar,nline,lin,nblend,blend,gen)

     xpos=dist
     ypos=pos(2)-atm_flag*(dist+dy)

     CALL FIXSPC(0.8)
     CALL CHAWTH(0.8)
     hgt=int(0.75*real(2100-2*dist)/len_trim(textline(1))/0.8/0.8)
     if (hgt.gt.23) hgt=23
     CALL HEIGHT(hgt)
     do ip=1,npar 
        ypos=pos(2)-atm_flag*(dist+dy)+160+(ip-1)*int(hgt*1.5)
        call messag(trim(textline(ip)),xpos,ypos)
        if (ipt%verbose.ge.2) write(*,*) trim(textline(ip))
     end do
     CALL SENDBF()
  end if
#else
  if ((atm_flag.eq.1).and.(ipt%verbose.ge.2)) then
     call atm_line(natm,atm,scl,textline,npar,nline,lin,nblend,blend,gen)
     do ip=1,npar 
        write(*,*) trim(textline(ip))
     end do
  end if
#endif
end subroutine plot_profile
