!routine to read in convolution function
subroutine read_conv(wlrg,file,cnwlin,cmode,wljitter,prefile,wlin,&
     nwl,conv,verbose)
  use all_type
  use interpol
  use random
  character(LEN=maxstr) :: file,prefile,line,word(2),iomsg
  real(8),dimension(maxwl) :: wlin,spc
  real(4),dimension(4*maxwl+15) :: wsave
  real(4),dimension(maxwl) :: jitter,convfft,preval,premax
  real(8),dimension(2) :: wlrg
  real(8),dimension(maxcwl) :: wl,wlcenter,dlambda
  real(4),dimension(maxcwl) :: val
  real(4) :: wljitter,cmax,normval
  integer(4) :: err,cindex,sidx,j,nw,loc(1),idx
  integer(2) iw,verbose,nwl,ncwl,i,cnwl,cnwlin,dosum,cmode
  integer(4) :: iu
  type(convtyp) :: conv
  type(prefiltertyp) :: prefilter
  logical :: eqspc,info,obsmatch
  real(8) :: avgspc,diff,maxdiff,wlloc,wlcen

  cnwl=cnwlin
  !do nothing if file is emty
  conv%nwl=0
  conv%doconv=0
  conv%nwl_compare=-1
  conv%mode=cmode

  if (trim(file(1:1)).eq.' ') return

  if (verbose.ge.1) write(*,*) 'Read convolution file '//trim(file)

  iu=99
  open(unit=iu,file=trim(file),iostat=err,action='READ',form='FORMATTED',iomsg=iomsg)

  if (err.ne.0) then
     write(*,*) 'Could not open convolution file: ',trim(file)
     write(*,*) 'IOSTAT: ',err
     write(*,*) 'IOMSG: ',trim(iomsg)
     write(*,*) '***************************'
     write(*,*) 'ERROR: no convolution done!'
     write(*,*) '***************************'
     conv%doconv=0
     return
  end if

  iw=1
  do while (err.eq.0)
     read(iu, fmt='(a)',iostat=err) line
     cindex=index(trim(line),';')-1
     if ((cindex.lt.0).and.(len(trim(line)).gt.0)) then

        j=1
        sidx=1
        word=''
        !split input line into words separated by spaces
        do while ((sidx.gt.0).and.(j.le.2).and.len(trim(line)).gt.0)
           sidx=index(line,' ')
           if (sidx.gt.1) then
              word(j)=line(1:sidx-1)
              line=trim(line(sidx+1:))
              j=j+1
           else
              if (sidx.eq.1) line=trim(line(sidx+1:))
           end if
        end do
        nw=j-1
        sidx=index(trim(line),' ')

        if (iw.gt.maxcwl) then 
           write(*,*) 'Max. # of WL points in convolve-file exceeded: ', &
                iw,maxcwl
           write(*,*) 'Change MAXCWL in all_type.f90 and maxpar.pro'
           write(*,*) '***************************'
           write(*,*) 'ERROR: no convolution done!'
           write(*,*) '***************************'
           conv%doconv=0
           return
        else
           if (nw.ge.2) then
              read(word(1),*) wl(iw)
              read(word(2),*) val(iw)
              iw=iw+1
           end if
        end if
     end if
  end do
  ncwl=iw-1
  close(iu)

  !check for FP maxima: sum up over one FSR
  cmax=0.95*maxval(val(1:ncwl))
  normval=0.
  dosum=0
  do iw=2,ncwl 
     if ((val(iw).ge.cmax).and.(val(iw-1).lt.cmax)) dosum=dosum+1
     if (dosum.eq.1) then 
        normval=normval+val(iw)
     end if
  end do
  if (dosum.eq.1) normval=sum(val(1:ncwl))
  !do normalization over 1 FSR  
  val=val/normval

  conv%doconv=1

  !check if convolution WL vector is
  !exactly the same as the data vector:
  obsmatch=.false.
  if (nwl.eq.cnwl) then 
     if (maxval(abs(wlin(1:nwl)-wl(1:cnwl))).le.1e-3) then
        if (verbose.ge.1) &
             write(*,*) 'Convolution WL-vector matches observation.'
        conv%nwl=cnwl
        conv%wl(1:cnwl)=wlin(1:nwl)
        conv%val(1:cnwl)=val(1:nwl)
        obsmatch=.true.
     end if
  end if
 if (.not.obsmatch) then
    


  !The WL-range of the convolution should cover
  !the whole profile
  if (sum(abs(wlrg)).le.1e-5) &
       wlrg=(/ minval(wlin(1:nwl)), maxval(wlin(1:nwl)) /)

  !The WL-bins for the convolution must be
  ! - equally spaced
  ! - small enough to sample the
  !   convolution function.

  !if the keyword CONV_NWL is specified
  !then the binning is defined using
  !this keyword:
  if (cnwl.gt.1) then
     if (cnwl.ge.nwl) then 
        if (verbose.ge.1) write(*,*) 'Using CONV_NWL for WL-binning'
     else
        if (verbose.ge.1) write(*,*) 'Observation has more bins than CONV_NWL, using observation bin #'
        cnwl=nwl
     end if
     do i=1,cnwl  
        conv%wl(i)=real((i-1),kind=8)/(cnwl-1)*(wlrg(2)-wlrg(1))+wlrg(1) 
     end do
  else
     if (verbose.ge.1) &
          write(*,*) 'Using WL-binning from observation for the convolution with CONV_FUNC'
     cnwl=nwl
     conv%wl(1:cnwl)=wlin
  end if

  !test equal spacing
  if (cnwl.ge.3) then 
     spc(1:cnwl-2)=conv%wl(2:cnwl-1)-conv%wl(1:cnwl-2)
     avgspc=sum(spc)/(cnwl-1)
     eqspc=(maxval(abs(spc(1:cnwl-2)-avgspc)).lt.1e-3) !1 mAngstrom tolerance
  end if

  if ((.not.eqspc).and.(cnwl.le.0)) then 
     write(*,*) 'WL-bins of observation not equally spaced. Equal '// &
          'spacing is required for the convolution with an instrument function.'
     write(*,*) 'Specify the CONV_NWL keyword or use equally spaced'// &
          ' observations.'
     write(*,*) '***************************'
     write(*,*) 'ERROR: no convolution done!'
     write(*,*) '***************************'
     conv%doconv=0
     return
  end if


! ;shift filter function to put max
! ;value exactly at the central bin
! ;number of conv.wl
!since the convolution function can have many maxima (e.g. a Fabry Perot), look for the maximum +-0.1 Angstrom atround the center only. If there is no maximum in this range, an error is thrown.
  conv%val(1:cnwl)=val(1:cnwl)
  conv%nwl=cnwl
  if (mod(cnwl,2).eq.1) then
     wlcen=conv%wl(cnwl/2+1)
  else 
     wlcen=(conv%wl(cnwl/2)+conv%wl(cnwl/2+1))/2
  end if
   dlambda=1.0
   loc=maxloc(val(1:ncwl) * merge(1.0,0.0,(abs(wl(1:ncwl)-wlcen).le.dlambda)))
   wlloc=wl(loc(1))
   if ((loc(1).eq.1).or.(loc(1).eq.ncwl)) then
     write(*,*) 'Cannot find maximum in convolution function. '
     write(*,*) 'The maximum value should be in the center of the function.'
     write(*,*) '***************************'
     write(*,*) 'ERROR: no convolution done!'
     write(*,*) '***************************'
     stop
   end if
   if ((val(loc(1)).lt.val(loc(1)-1)).or.(val(loc(1)).lt.val(loc(1)+1))) then
      write(*,*) 'Cannot find maximum in convolution function +-1 Angstroem around'
     write(*,*) 'the center. Update your convolution function to have a maximum in the center.'
     write(*,*) '***************************'
     write(*,*) 'ERROR: no convolution done!'
     write(*,*) '***************************'
     stop
   end if

  if (abs(wlcen-wlloc).ge.0.1) then 
     if (verbose.ge.1) then
     write(*,*) '***************************'
     write(*,*) 'Convolution WARNING'
     write(*,*) '***************************'
        write(*,*) 'Maximum of filter function is not close to the central bin'
        write(*,*) 'of the convolution function. The convolution function was'
        write(*,*) 'shifted accordingly. This can cause a problem in the '
        write(*,*) 'wavelength range required for the convolution.'
      end if
  end if


  !interpolating convolution function to
  !observed WL pixels
!manual interpolation
  ! do i=1,cnwl 
  !    conv%val(i)=real(interpolate(conv%wl(i)-conv%wl(cnwl/2),&
  !         wl(1:ncwl)-wl(loc(1)),real(val(1:ncwl),kind=8),1_2,ncwl),kind=4)
  !write(*,*) 'HIERinter1',i-1,conv%val(i),sum(conv%val(1:i)),wl(loc(1)),conv%wl(i)-conv%wl(cnwl/2)
  !  end do
  !manual interpoaltion routine (to be
  !consistent with the fortran version) 
  do i=1,cnwl
     idx=-1
     do j=1,ncwl-1
        if ((wl(j)-wlloc.le.conv%wl(i)-wlcen).and.&
             (wl(j+1)-wlloc.ge.conv%wl(i)-wlcen)) idx=j
     end do
    if (idx.eq.-1) then 
        write(*,*) 'Convolution function does not cover the necessary '// &
             'WL-range ',conv%wl(1),conv%wl(cnwl)
        write(*,*) '***************************'
        write(*,*) 'ERROR: no convolution done!'
        write(*,*) '***************************'
        write(*,*) 'Convolution function: WL=',wlcen, &
             '+[',minval((conv%wl(1:cnwl)-wlcen)),',', &
             maxval((conv%wl(1:cnwl)-wlcen)),']'
        write(*,*) 'Data                : WL=',wlloc, &
        '+[',minval((wl(1:ncwl)-wlloc)),',', &
              maxval((wl(1:ncwl)-wlloc)),']'
        write(*,*) 'Extend the WL-range of the convolution fucntion.'
        conv%doconv=0
        stop
     end if
     conv%val(i)=(val(idx+1)-val(idx))/(wl(idx+1)-wl(idx))* &
          (conv%wl(i)-wlcen-(wl(idx)-wlloc))+val(idx)
  end do

  !correct normalization after interpolation:
  conv%val(1:cnwl)=conv%val(1:cnwl)/(wl(2)-wl(1))*(conv%wl(2)-conv%wl(1))


  !check if there is significant
  !transmission close to
  !WL-boundaries. This is not good since
  !the convolution might produce large
  !values outside a prefilter!  
  !  convmax=maxval(conv%val(1:cnwl))
  !  if ((maxval(conv%val(1:(cnwl)*.05)/convmax).ge.3e-2).or. &
  !       (maxval(conv%val((cnwl)*.95:cnwl)/convmax).ge.3e-2)) then 
  !     write (*,* ) 'Convolution function has significant transmission'// &
  !     ' (>3%) close to boundaries. The convolution might produce large'// &
  !     ' values outside a prefilter! Check your convolution function'// &
  !     ' and/or adapt your WL_RANGE'      
  !    endif


end if !obsmatch

  !for the FFT convolution a kernel
  !function should be used with the
  !maximum value of the filter function
  !located at the index 0. Therefore the
  !kernel function is centered at 0
  !  conv%val(1:cnwl)= (/ conv%val((cnwl-1)/2+1:cnwl), conv%val(1:(cnwl-1)/2) /)
  !  conv%val(1:cnwl)= (/ conv%val((cnwl-1)/2:cnwl), conv%val(1:(cnwl-1)/2-1) /)
  conv%val(1:cnwl)= (/ conv%val((cnwl)/2:cnwl), conv%val(1:(cnwl)/2-1) /)
!  write(*,*) 'HIERrc',conv%val(1:5)
!  write(*,*) 'HIERrc',conv%wl(1:5)

  !  rval(1:cnwl)=conv%val(1:cnwl)
  !  CALL RFFTI( int(cnwl), WSAVE )      
  !  CALL RFFTF( int(cnwl), rval(1:cnwl), WSAVE )
  !  conv%fft(1:cnwl)=rval(1:cnwl)

  !do reverse fft
  !  cval(1:cnwl)=cmplx(conv%val(1:cnwl),conv%val(1:cnwl)*0.)
  !  rval(1:cnwl)=conv%val(1:cnwl)
  !  call realft(rval(1:cnwl),cnwl,-1)
  !  call twofft(rval(1:cnwl),rval(1:cnwl),cval(1:cnwl),cval(1:cnwl),cnwl)
  !retrieve theindices of the original
  !WL-vector for which the comparison
  !with the observed spectrum is desired

  !add WL jitter to simulate deficiencies of a Fabry-Perot type
  !instrument (SO/PHI: simulate effect of uncertainty in voltage
  !applied to the etalons)
  jitter(:)=0
  if (wljitter.ge.1e-10) then
     CALL random_seed()
     do i=1,nwl
        jitter(i)=gennor(0.,wljitter)
     end do
  end if


  info=.false.
  maxdiff=00.
  do i=1,nwl
     diff=minval(abs(conv%wl(1:cnwl)-(wlin(i)+jitter(i))))
     if (diff.ge.maxdiff) maxdiff=diff
     loc=minloc(abs(conv%wl(1:cnwl)-(wlin(i)+jitter(i))))
     if ((diff.gt.5e-3).and.(wljitter.lt.1e-10)) then
        write(*,*) 'WARNING: large diff. between binning of obs. and'// &
             ' CONV_FUNC        Obs-bin #',i-1,', diff. ',diff
        info=.true.
     end if
     conv%iwl_compare(i)=loc(1)
  end do
  conv%nwl_compare=nwl

  if (info) then
     write(*,*) 'WL of data: ',wlin(1),' - ',wlin(nwl)
     write(*,*) 'Average data WL-spacing: ',avgspc,' #bins: ',nwl
     write(*,*) 'WL-range convolution: ',wlrg,' #bins: ',cnwl
     write(*,*) 'To find the optmum binning use the IDL tool find_opt_binning:'
     write(*,'(a,f10.3,a1,f10.3,a)') &
          'IDL> find_opt_binning,wlin=[Wl-vector of data], '//&
          'wlrg=[',wlrg(1),',',wlrg(2),']'
     write(*,*) 'Use IDL version of HeLIX+ (''CODE IDL'') to automatically call this routine.'
  end if
  if ((verbose.ge.1).or.(info)) &
       write(*,*) 'Max. diff between obs and conv WL in mA: ',maxdiff*1e3

  !after the convolution a normalization
  !has to be done to bring the spectrum
  !to the correct values. The correct
  !normalization requires the knowledge
  !of the prefilter curve.
  line=prefile !to prevent compiler warning message

  select case (conv%mode)
  case(0)
     call read_prefilter(prefile,0.,conv%wl,conv%nwl,prefilter,0)
     if (prefilter%doprefilter.eq.1) then
        CALL RFFTI( int(conv%nwl), WSAVE )
        convfft=conv%val
        CALL RFFTF( int(conv%nwl), convfft(1:conv%nwl), WSAVE )
        preval=prefilter%val
        call convolve(int(conv%nwl),preval(1:conv%nwl),convfft(1:conv%nwl),wsave)
        premax=0.
        !only take the region where prefilter is above 10% transmission
        where (prefilter%val(1:conv%nwl).ge.1e-1) premax=1
        conv%normval=maxval(preval(1:conv%nwl)*premax(1:conv%nwl))
     else
        !normalize convolution function such
        !that value of profile remains
        !unchanged
        conv%val(1:cnwl)=conv%val(1:cnwl)/sum(conv%val(1:cnwl))*cnwl
        conv%normval=1.
     end if
     conv%val=conv%val/conv%normval
     if (verbose.ge.1) write(*,*) 'Using convolution method FFT'
  case(1)
     conv%normval=1.
     conv%val=conv%val/conv%normval
     if (verbose.ge.1) write(*,*) 'Using convolution method MUL'
  end select

  !issue warning message if WL range of
  !filter function is small
  if (verbose.ge.1) then
     if (((maxval(wlrg)-minval(wlrg))/ &
          (maxval((conv%wl(conv%iwl_compare(1:conv%nwl_compare))))- &
          minval((conv%wl(conv%iwl_compare(1:conv%nwl_compare)))))).lt.2) then
        write(*,*) 'WARNING: WL_RANGE for convolution function should be '//&
             'at least 2x larger than WL-range of data.'
     end if
  end if

!  write(*,*) trim(file)
!  write(*,*) conv%normval
!  write(*,*) conv%wl(1:4)
!  write(*,*) conv%wl(cnwl-3:cnwl)
!  write(*,*) conv%val(1:4)
!  write(*,*) conv%val(cnwl-3:cnwl)
!call stop
end subroutine read_conv
