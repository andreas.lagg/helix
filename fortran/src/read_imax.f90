module imax_common
  use all_type
  save

  character(LEN=maxstr) :: imaxfile,imaxheader(:)
  allocatable imaxheader
  real(4) :: imaxdata(:,:,:,:)
  allocatable imaxdata
  integer(2) :: imaxnwl,imaxicidx
  real(8) :: imaxwl(maxwl)
  integer(4) :: imaxnaxes(4),imaxfpixels(4),imaxlpixels(4)
  real(4) :: imaxic(:,:),imaximgcont
  allocatable imaxic
end module imax_common

subroutine read_imax(file,xv,yv,profile,lsprof,do_ls,ok)
  use all_type
  use ipt_decl
  use tools
  use imax_common
  implicit none
#ifdef MPI
  include 'mpif.h'
#endif
  type (proftyp) :: profile,lsprof
  character(LEN=maxstr) :: file
  integer(2) :: xv,yv,ix
  INTEGER(4) :: nfound,bitpix!, fpixels(4), lpixels(4), inc(3),xnaxes(2),xstat,unitccx
  integer (4) :: status,unit,blocksize,inc(4)
  logical :: ok,do_ls,error,anynull
  real(4) :: imaxicsort(:)
  integer(4) :: isort(:),itot,ithresh,i
  allocatable imaxicsort
  allocatable isort
  INTERFACE
     INTEGER FUNCTION SYSTEM (COMMANDA)
       CHARACTER(LEN=*) COMMANDA
     END FUNCTION SYSTEM
  END INTERFACE
  
  status=0
  error=.false.
  
  !check if file is already read in
  if (imaxfile.ne.file) then
     if (ipt%verbose.ge.1) write(*,*) 'Reading IMaX file ',trim(file)

     !  Get an unused Logical Unit Number to use to open the FITS file.
     call ftgiou(unit,status)
     !  Open the FITS file previously created by WRITEIMAGE
     call ftopen(unit,file,0,blocksize,status)
     if (status.ne.0) then
        write(*,*) 'Could not find IMaX-file: '//trim(file)
        error=.true.
        call stop
     end if
     !determine bitpix (should be 32, which is 4-byte integer)
     call ftgidt(unit,bitpix,status)
     !  Determine the size of the image.
     call ftgknj(unit,'NAXIS',1,4,imaxnaxes,nfound,status)
     inc=(/1,1,1,1/)
     imaxfpixels=(/1,1,1,1/)
     imaxlpixels=imaxnaxes
     
     allocate(imaxdata(imaxlpixels(1),imaxlpixels(2), &
          imaxlpixels(3),imaxlpixels(4)))
     call ftgsve(unit,1,4, imaxnaxes, imaxfpixels, imaxlpixels, inc, 0, &
          imaxdata, anynull, status)
     call ftclos(unit, status)
     call ftfiou(unit, status)

     imaxnwl=imaxnaxes(3)
     select case (imaxnwl)
     case(5) !V5-6 data
        imaxwl(1:imaxnwl)=(/-80., -40., +40., +80., +227./)/1d3 + 5.250208d3
        imaxicidx=5
     case(8) !V8-4 data
        imaxwl(1:imaxnwl)=(/-120.,-80., -40., +40., +80.,+120., +227./)/1d3 + 5.250208d3
        imaxicidx=8
     case(12) !L12-2 data
        do i=1,imaxnwl
           imaxwl(i)=(i-1)*35d0 /1d3 -0.1925d0 + 5.250208d3
        end do
        imaxicidx=1
     case default
        write(*,*) 'IMaX data must contain 5, 8 or 12 WL positions.'
        error=.true.
        call stop
     end select

     allocate(imaxic(imaxnaxes(1),imaxnaxes(2)))
     imaxic=imaxdata(1:imaxnaxes(1),1:imaxnaxes(2),imaxicidx,1)

     !Image continuum: take average over 10% brightest
     !profiles as image continuum
     itot=imaxnaxes(1)*imaxnaxes(2)
     allocate(imaxicsort(itot))
     allocate(isort(itot))
     do ix=1,imaxnaxes(1)
        imaxicsort((ix-1)*imaxnaxes(2)+1:ix*imaxnaxes(2))=imaxic(ix,:)
     end do
     call qsort(imaxicsort,isort,itot)
     ithresh=0.9*itot
     imaximgcont=sum(imaxicsort(isort(ithresh:itot)))/(itot-ithresh+1)

     imaxfile=file
  end if
  
  if ((xv.lt.0).or.(xv.ge.imaxnaxes(1)).or.&
       (yv.lt.0).or.(yv.ge.imaxnaxes(2))) then
     write(*,*) 'Could not find XPOS=',xv,' and YPOS=',yv,&
          ' in IMaX data file ',file
     call stop
  end if



  profile%nwl=imaxnwl
  if (ipt%norm_cont.eq.0) then
     profile%ic=imaxic(xv+1,yv+1)
  else
     profile%ic=imaximgcont
  end if
  profile%i(1:imaxnwl)=imaxdata(xv+1,yv+1,1:imaxnwl,1)/profile%ic
  if (imaxnwl.eq.5) then !V5-6 data
     profile%q(1:imaxnwl)=imaxdata(xv+1,yv+1,1:imaxnwl,2)/profile%ic
     profile%u(1:imaxnwl)=imaxdata(xv+1,yv+1,1:imaxnwl,3)/profile%ic
     profile%v(1:imaxnwl)=imaxdata(xv+1,yv+1,1:imaxnwl,4)/profile%ic
  else !L12-2 data
     profile%v(1:imaxnwl)=imaxdata(xv+1,yv+1,1:imaxnwl,2)/profile%ic
     write(*,*) 'WARNING: IMaX L12-2 Stokes V divided by 0.55!!'
     profile%v(1:imaxnwl)=profile%v(1:imaxnwl)/0.55
  end if
  profile%wl(1:imaxnwl)=imaxwl(1:imaxnwl)

  if (do_ls) then
     call get_lsprof(file,xv,yv,profile%wl,imaxnwl,3_2,lsprof)
  end if

  ok=.true.
end subroutine read_imax
