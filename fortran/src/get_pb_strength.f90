subroutine get_pb_strength(line,B,strength,pb_method)
  use all_type
  use interpol
  implicit none
  type (linetyp) :: line
  integer(2) :: i,pb_method
  real(4) :: B,x1,x2,x3,x4
  real(8),dimension(max_pb_zl) :: strength
!  real(4) :: interpolate
!  external :: interpolate

  select case (pb_method)
  case (1_2)
     do i=1,line%pb%nr_zl
        strength(i)=interpolate(real(B,kind=8), &
             real(line%pb%b(1:line%pb%nr_b),kind=8), &
             line%pb%strength(i,1:line%pb%nr_b),2_2,line%pb%nr_b)
     end do
  case (0_2)
     x1=(b*1e-3)
     x2=(b*1e-3)**2
     x3=(b*1e-3)**3
     x4=(b*1e-3)**4
     do i=1,line%quan%n_sig
        strength(i)= &
             line%quan%WER(i)*line%f + &
             (line%pb%dr(i,1)*x1 + line%pb%dr(i,2)*x2 + &
             line%pb%dr(i,3)*x3 + line%pb%dr(i,4)*x4)
        strength(i+line%quan%n_sig+line%quan%n_pi)= &
             line%quan%WEB(i)*line%f + &
             (line%pb%db(i,1)*x1 + line%pb%db(i,2)*x2 + &
             line%pb%db(i,3)*x3 + line%pb%db(i,4)*x4)
     end do
     do i=1,line%quan%n_pi
        strength(i+line%quan%n_sig)= &
             line%quan%WEP(i)*line%f + &
             (line%pb%dp(i,1)*x1 + line%pb%dp(i,2)*x2 + &
             line%pb%dp(i,3)*x3 + line%pb%dp(i,4)*x4)
     end do
  end select

end subroutine get_pb_strength
