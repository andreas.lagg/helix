
!!    compile with:
!!    compute profile_
!!    equivalent to compute_profile.pro
!!    calculates profiule from given atmosphere atm defines in all_type
!!    .f90

!!    =======================================================      
SUBROUTINE simple_pro(argc, argv) !Called by IDL
  INTEGER(4) argc, argv(*)   !Argc and Argv are integers

  j = LOC(argc)             !Obtains the number of arguments (argc)
  !Because argc is passed by VALUE.

  !!    Call subroutine compute_profile, converting the IDL parameters
  !!    to standard FORTRAN, passed by reference arguments:

  CALL simple(%VAL(argv(1)))
END SUBROUTINE simple_pro
!!    ========================================================

!!    This subroutine is called by compute_profile_pro and has no
!!    IDL specific code. Here the computation of the line is done.
!!    
!!    ========================================================
SUBROUTINE simple(n)
  implicit none
  real(8) :: n

  write(*,*) 'inside fortran: ',n

END SUBROUTINE simple
