subroutine readsto(file,profile,ok)
  use all_type
  use ipt_decl
  implicit none
  type (proftyp) :: profile,ptmp(maxspec)
  real(4),dimension(4) :: stokes
  character(LEN=maxstr) :: file,line
  integer(2) :: punit
  integer(4) :: iostat,iw,ii,iq,iu,iv
  logical :: exist,ok
!  character(LEN=maxstr) :: head
  integer(2) :: nspec,i,nblend,is,ismax,nwlmax
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt



  punit=10
  ok=.false.
  inquire(file=trim(file),exist=exist)
  if (.not.exist) then
     write(*,*) 'File ',trim(file),' does not exist.'
  else
     open (unit=punit,file=file,iostat=iostat)
     if (iostat.eq.0) then
        ok=.true.
     else
        write(*,*) 'Problem in reading file: ',trim(file)
     end if
  end if
  if (ok) then
     if (ipt%verbose.ge.2) write(*,*) 'Reading profile ', &
          trim(file(index(trim(file),'/',back=.true.)+1:len_trim(file)))
     
     read (punit,*) nspec,line
     iw=index(line,'W')
     ii=index(line,'I')-iw
     iq=index(line,'Q')-iw
     iu=index(line,'U')-iw
     iv=index(line,'V')-iw
     if (ii.eq.0) ii=1
     if (iv.eq.0) iv=2
     if (iq.eq.0) iq=3
     if (iu.eq.0) iu=4
     is=1
     do while (is.le.nspec)
        read (punit,*) nblend,ptmp(is)%wlref,ptmp(is)%nwl,ptmp(is)%ic
        do i=1,ptmp(is)%nwl
!           read (punit,*) ptmp(is)%wl(i),ptmp(is)%i(i),ptmp(is)%v(i), &
!                ptmp(is)%q(i),ptmp(is)%u(i)
           read (punit,*) ptmp(is)%wl(i),stokes(1:4)
           ptmp(is)%i(i)=stokes(ii)
           ptmp(is)%v(i)=stokes(iv)
           ptmp(is)%q(i)=stokes(iq)
           ptmp(is)%u(i)=stokes(iu)
        end do
!        read (punit,*) head
        if ((is.eq.1).or.(ptmp(is)%nwl.ge.nwlmax)) then
           ismax=is
           nwlmax=ptmp(is)%nwl
        end if
        is=is+1
     end do
     profile=ptmp(ismax)
     if (nspec.gt.1) write(*,*) trim(file)//' (spectrum ',ismax,' only)'

  end if
  close (unit=punit)
  

end subroutine readsto
