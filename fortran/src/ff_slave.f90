subroutine ff_slave(n)
  ! ---------------------------------------------
  ! fitness function slave program
  !     ---------------------------------------------
  use piktools, only: userff
  implicit none

  include 'mpif.h'

  integer myid, ierr, status(MPI_STATUS_SIZE)
  integer master, msgtype, trial, flag
  integer(4) n

  real*4 :: result
  real*4 :: data(n)
  !      external userff

  ! ---------------------------------------------
  ! identify this slave task
  ! ---------------------------------------------
  call mpi_comm_rank( MPI_COMM_WORLD, myid, ierr )

  ! ---------------------------------------------
  ! listen for a new job
  ! ---------------------------------------------
  master = 0
25 msgtype = 1

  ! ---------------------------------------------
  ! receive data from master host
  ! ---------------------------------------------
  call mpi_recv( trial, 1, MPI_INTEGER, master,&
                 msgtype, MPI_COMM_WORLD, status, ierr )
  if (trial .EQ. -1) goto 99
  call mpi_recv( flag, 1, MPI_INTEGER, master,&
                 msgtype, MPI_COMM_WORLD, status, ierr )
  call mpi_recv( n, 1, MPI_INTEGER4, master,&
                 msgtype, MPI_COMM_WORLD, status, ierr )
  call mpi_recv( data(1:n), n, MPI_REAL, master,&
                 msgtype, MPI_COMM_WORLD, status, ierr )

  ! ---------------------------------------------
  ! perform calculations with data
  ! ---------------------------------------------
  result = userff( n, data, flag ) 

  ! ---------------------------------------------
  ! send result to master host
  ! ---------------------------------------------      
  msgtype = 2 
  call mpi_send( myid, 1, MPI_INTEGER, master,&
                 msgtype, MPI_COMM_WORLD, ierr )
  call mpi_send( trial, 1, MPI_INTEGER, master,&
                 msgtype, MPI_COMM_WORLD, ierr )
  call mpi_send( result, 1, MPI_REAL, &
         master, msgtype, MPI_COMM_WORLD, ierr )

  ! ---------------------------------------------
  ! go back for more work
  ! ---------------------------------------------
  goto 25

99 return
end subroutine ff_slave
