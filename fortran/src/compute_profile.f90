!!!    compile with:
!!!    compute profile_
!!!    equivalent to compute_profile.pro
!!!    calculates profiule from given atmosphere atm defines in all_type
!!!    .f90
  
!!!    =======================================================      
#ifdef IDL
  SUBROUTINE compute_profile_pro(argc, argv) !Called by IDL
#ifdef bit64
    INTEGER(8) argc, argv(*)   !Argc and Argv are integers
#else
    INTEGER(4) argc, argv(*)   !Argc and Argv are integers
#endif

    j = LOC(argc)             !Obtains the number of arguments (argc)
    !Because argc is passed by VALUE.

    !!    Call subroutine compute_profile, converting the IDL parameters
    !!    to standard FORTRAN, passed by reference arguments:

    CALL compute_profile(%VAL(argv(1)), %VAL(argv(2)), %VAL(argv(3)), &
         %VAL(argv(4)), %VAL(argv(5)), %VAL(argv(6)), %VAL(argv(7)), &
         %VAL(argv(8)), %VAL(argv(9)), %VAL(argv(10)), %VAL(argv(11)), &
         %VAL(argv(12)), %VAL(argv(13)), %VAL(argv(14)), %VAL(argv(15)), &
         %VAL(argv(16)), %VAL(argv(17)), %VAL(argv(18)), %VAL(argv(19)), &
         %VAL(argv(20)), %VAL(argv(21)), %VAL(argv(22)), %VAL(argv(23)), &
         %VAL(argv(24)), %VAL(argv(25)), %VAL(argv(26)), %VAL(argv(27)), &
         %VAL(argv(28)), %VAL(argv(29)), %VAL(argv(30)), %VAL(argv(31)), &
         %VAL(argv(32)), %VAL(argv(33)), %VAL(argv(34)), %VAL(argv(35)), &
         %VAL(argv(36)), %VAL(argv(37)), %VAL(argv(38)), %VAL(argv(39)), &
         %VAL(argv(40)), %VAL(argv(41)), %VAL(argv(42)))
  END SUBROUTINE compute_profile_pro
#endif

!!!    ========================================================
!!!    compute the gauss profile      
!!!    ========================================================
  subroutine gaussfunc(a0,shift,width2,wl,nwl,gprof,addnum)
    implicit none
    integer(2) :: nwl
    real(4) :: a0
    real(4) :: width2
    real(8) :: gprof(nwl),expo(nwl)
    real(8) :: shift
    real(8) :: wl(nwl)
    real(4) :: addnum

    expo=(-(wl-shift)**2./(width2))
    !  expo=(expo+40.)*(expo>(-40.))-40.
    gprof=a0 * exp(expo) / sqrt(3.1415927*width2) + addnum

  end subroutine gaussfunc

!!!    ========================================================
!!!    compute the voigt profile      
!!!    ========================================================
  subroutine voigtfunc(shift,damp,dopp,wl,nwl,hprof,fprof)
    implicit none
    integer(2) :: nwl
    !  real(4) :: a0
    real(4) :: dopp
    real(4) :: damp
    real(8) :: shift
    real(8) :: wl(nwl)
    !  real(4) :: addnum
    real(4), dimension(nwl) :: hprof,fprof

    real(8) :: vvec(nwl) !,damp8
    !  real(4) :: delta_vd
    !  real(4), dimension(nwl) :: H,F

!    if (damp.le.1e-5) write(*,*) 'DAMP IS LESS THAN 1E-5!!!'
!    if (dopp.le.1e-5) write(*,*) 'DOPP IS LESS THAN 1E-5!!!'

    vvec=(wl-shift)/dopp


    call fvoigt(nwl,damp,vvec,hprof,fprof)

!    write(*,*) 'HV0',hprof(1:20),sum(hprof(1:nwl))
!    write(*,*) 'FV0',fprof(1:20),sum(fprof(1:nwl))

!    call voigt(nwl,damp,vvec,hprof,fprof)
!    write(*,*) 'HV1',hprof(1:20),sum(hprof(1:nwl))
!    write(*,*) 'FV1',fprof(1:20),sum(fprof(1:nwl))

    !JuanMas Taylor expansion method. This is on average the fastest
    !method, especially when the damping is small
    !BUT: noisy! creates strange noise peaks in profiles...
    !Best seen in synthetic profiles with low QUV signal.
!    damp8=real(damp,kind=8)
!    call voigt_taylor(nwl,damp8,vvec,hprof,fprof)
!    write(*,*) 'HV2',hprof(1:20),sum(hprof(1:nwl))
!    write(*,*) 'FV2',fprof(1:20),sum(fprof(1:nwl))


!stop
    !voigt integral to be multiplied by a/!pi
    !(see e.g.) Jefferies ApJ 1989 343
    !  fprof=damp/3.1415927*fprof
    !  fprof=2.*fprof
    !  hprof=a0*hprof
    !  fprof=a0*fprof
  end subroutine voigtfunc


!!!    ========================================================

!!!    This subroutine is called by compute_profile_pro and has no
!!!    IDL specific code. Here the computation of the line is done.
!!!    
!!!    ========================================================
  SUBROUTINE compute_profile(atm,natm,line,nline, &
       wl,nwl,blend,nblend,gen, &
       lsi,lsq,lsu,lsv,dols,lspol, &
       convval,doconv,cmode, &
       prefilterval,doprefilter, &
       ret_wlidx,nret_wlidx, &
       obs_par,init,voigt,magopt,use_geff,use_pb,pb_method, &
       modeval,iprof_only,luse,iprof,qprof,uprof,vprof,normff,old_norm,&
       icomp,hanle_azi,norm_stokes_val,old_voigt)
    use all_type
    use hanle_module, only:hanle_prof
    implicit none
    integer(2) :: nwl,natm,nline,nblend,doconv,nret_wlidx,normff,&
         old_norm,icomp,cmode
    integer(2) :: init,voigt,magopt,use_geff,use_pb,pb_method,modeval,iprof_only
    integer(2) :: norm_stokes_val,doprefilter,dols,nwlnew,lspol,old_voigt
    real(4), dimension(nwl) :: iprof,qprof,uprof,vprof,hprof,fprof
    real(4), dimension(nwl) :: iproftmp,qproftmp,uproftmp,vproftmp
    real(4), dimension(nwl) :: hanle_i,hanle_q,hanle_u,hanle_v
    real(4), dimension(nwl) :: convval,prefilterval,cfp,convfft
    real(4), dimension(maxwl) :: lsi,lsq,lsu,lsv
    real(4), dimension(4*nwl+15) :: wsave
    integer(2),dimension(maxwl) :: ret_wlidx
    real(8), dimension(nwl) :: wl,wlnew
    real(8), dimension(nwl) :: gsub,qsub,usub
    real(4) :: hanle_azi,fctv
    integer(2) :: luse(nline,natm)
    type (atmtyp) :: atm(natm)
    type (blendtyp) :: blend(nblend)
    type (gentyp) :: gen
    type (linetyp) :: line(nline)
    type (obstyp) :: obs_par
    integer(2) :: il,ia,ii,ib,ia1,ia2,iw

    real(8) :: dopshift_wl,zeeman_shift
    real(8),dimension(maxsigma) :: zeeman_b,zeeman_r
    real(8),dimension(maxpi) :: zeeman_p
    real(8),dimension(max_pb_zl) :: web,wer,wep
    !  real(8), PARAMETER :: c_light=299792512.00000
    !  real(4), parameter :: dtor=0.017453292384744
    real(8), PARAMETER :: zfct=4.6686411E-13
    real(8), PARAMETER :: clight=2.99792512e8
    real(8), PARAMETER :: dpi=3.1415926536
    real(8), dimension(nwl) :: gprof
    real(4), dimension(nwl) :: i_center,i_plus,i_minus, &
         rho_p,rho_minus,rho_plus,eta_v,rho_v, &
         quprof,eta_q,eta_u,eta_i, &
         rho_q,rho_u,rho_qu, &
         R,F2DELTA,DELTA,I,Q,U,V
    real(4) :: b0,szero
    real(4) :: sininc2,cosinc,sin2azi,cos2azi,width2,FF(natm),straydamp,cfpsum
    real(4),parameter :: addnum=1e-4
    real(8),dimension(max_pb_zl) :: splitting,strength
    external norm_ff

    iprof=0.
    qprof=0.
    uprof=0.
    vprof=0.

    if (icomp.eq.-1) then
      ia1=1
      ia2=natm
    else
      ia1=icomp
      ia2=icomp
    end if

    if (old_voigt.eq.1) then
       fctv=0.5
    else
       fctv=1.0
    end if
    FF=atm(1:natm)%par%FF
    if (normff.eq.1) then
       call norm_ff(FF,atm(1:natm)%fit%FF,atm(1:natm)%linid,natm)
                                !normalization when more than one atom
                                !is used (e.g. fitting Si and He line
                                !simultaneously)
    end if
    FF=FF/sum(FF)
    do ia=ia1,ia2
      !normal component (no local straylight)
      if ((dols.eq.0).or.(ia.ne.natm)) then
        width2=2.*atm(ia)%par%width**2.

        eta_i=0 ; eta_q=0 ; eta_u=0 ; eta_v=0 
        rho_q=0 ; rho_u=0 ; rho_v=0 
        
        sininc2=sin(atm(ia)%par%inc*0.01745329)**2.
        cosinc=cos(atm(ia)%par%inc*0.01745329)
        sin2azi=sin(atm(ia)%par%azi*0.01745329*2.)
        cos2azi=cos(atm(ia)%par%azi*0.01745329*2.)
        do il=1,nline

          if (luse(il,ia).eq.1) then
            !compute profiles with hanle slab
            !model.  note that this routine
            !computes all 3 He lines at once, so
            !use_hanle_slab is only st tot 1 for
            !the first line
            if (modeval.eq.5) then
              if (line(il)%use_hanle_slab.eq.1) then
                 call hanle_prof(init,line(il),&
                     atm(ia)%par%b,atm(ia)%par%inc,atm(ia)%par%azi, &
                     atm(ia)%par%vlos,atm(ia)%par%dopp,atm(ia)%par%damp,&
                     atm(ia)%par%dslab,atm(ia)%par%height, &
                     obs_par,wl,nwl, &
                     hanle_i,hanle_q,hanle_u,hanle_v,gen%par%radcorrsolar)
                hanle_i=hanle_i*FF(ia)
                hanle_q=hanle_q*FF(ia)
                hanle_u=hanle_u*FF(ia)
                hanle_v=hanle_v*FF(ia)
                qprof = qprof + hanle_Q
                uprof = uprof + hanle_U
                vprof = vprof + hanle_V
                !same as below for zeeman
                if (old_norm.eq.1) then
                  iprof = iprof + hanle_I - FF(ia)
                else if (old_norm.eq.-1) then
                  iprof = iprof + hanle_I - FF(ia)
                else if (old_norm.eq.2) then
                  iprof = iprof + hanle_I
                else if (old_norm.eq.0) then
                  iprof = iprof + hanle_I
                endif
              endif
            else
              if (modeval.eq.1) then    !voigt-par mode
                !              dopp=atm(ia)%par%dopp
                !              damp=atm(ia)%par%damp
                !              etazero=atm(ia)%par%etazero
              else if (modeval.eq.2) then !voigt-phys mode (see Balasubramaniam,
                !ApJ 382, 699-705 1991, original:
                !Landi Degl'Innocenti, A&AS 25, 379-390, 1976)
                atm(ia)%par%dopp=line(il)%wl / clight * &
                     sqrt(2*1.3805e-23*atm(ia)%par%tempe/ &
                     (line(il)%mass*1.67000e-27)+atm(ia)%par%vmici**2)
                atm(ia)%par%damp=atm(ia)%par%gdamp * line(il)%wl**2 / &
                     (4*DPI*clight*atm(ia)%par%dopp)
                atm(ia)%par%etazero=atm(ia)%par%densp*line(il)%wl**2/ &
                     atm(ia)%par%dopp*1e-10* &
                     (1.-exp(-1.43883e8/(line(il)%wl*atm(ia)%par%tempe)))
              else if ((modeval.eq.3).or.(modeval.eq.4)) then !voigt-gdamp mode
                atm(ia)%par%damp=atm(ia)%par%gdamp * line(il)%wl**2 / &
                     (4*DPI*clight*atm(ia)%par%dopp)
              end if

              dopshift_wl = atm(ia)%par%vlos * line(il)%wl / clight
              !*DAVID
              IF (use_geff.eq.1) THEN 
                zeeman_shift = zfct * line(il)%wl**2. * &
                     line(il)%geff &
                     * atm(ia)%par%b
              else if ((use_pb.eq.1).and.&
                   (char(line(il)%id(1))//char(line(il)%id(2)).eq.'He')) then
                !use tabulated PB-effect
                call get_pb_splitting(line(il),atm(ia)%par%b,splitting,pb_method)
                call get_pb_strength(line(il),atm(ia)%par%b,strength,pb_method)
                strength=strength/line(il)%f
                zeeman_b(1:line(il)%quan%n_sig)= &
                     splitting(line(il)%quan%n_sig+line(il)%quan%n_pi+1: &
                     line(il)%quan%n_sig*2+line(il)%quan%n_pi)
                zeeman_r(1:line(il)%quan%n_sig)= &
                     splitting(1:line(il)%quan%n_sig)
                zeeman_p(1:line(il)%quan%n_pi)= &
                     splitting(line(il)%quan%n_sig+1: &
                     line(il)%quan%n_sig+line(il)%quan%n_pi)
                web(1:line(il)%quan%n_sig)= &
                     strength(line(il)%quan%n_sig+line(il)%quan%n_pi+1: &
                     line(il)%quan%n_sig*2+line(il)%quan%n_pi)
                wer(1:line(il)%quan%n_sig)= &
                     strength(1:line(il)%quan%n_sig)
                wep(1:line(il)%quan%n_pi)= &
                     strength(line(il)%quan%n_sig+1: &
                     line(il)%quan%n_sig+line(il)%quan%n_pi)
              else
                zeeman_B=atm(ia)%par%b*line(il)%wl**2. * &
                     4.6686411e-13*line(il)%quan%NUB
                zeeman_P=atm(ia)%par%b*line(il)%wl**2. * &
                     4.6686411e-13*line(il)%quan%NUP
                zeeman_R=atm(ia)%par%b*line(il)%wl**2. * &
                     4.6686411e-13*line(il)%quan%NUR
                web(1:line(il)%quan%n_sig)= &
                     line(il)%quan%WEB(1:line(il)%quan%n_sig)
                wer(1:line(il)%quan%n_sig)= &
                     line(il)%quan%WER(1:line(il)%quan%n_sig)
                wep(1:line(il)%quan%n_pi)= &
                     line(il)%quan%WEP(1:line(il)%quan%n_pi)
              end if
              !END DAVID
!write(*,*) 'HIER',web(1:line(il)%quan%n_sig),wer(1:line(il)%quan%n_sig),wep(1:line(il)%quan%n_pi),zeeman_b,zeeman_p,zeeman_r
              if (iprof_only.eq.0) then !flag to determine if only fit to
                !i-profile for straypol_run is to be done

                !!    calculate central component (PI)
                if (voigt.eq.1) then
                  IF (use_geff.eq.1) THEN 
                    call voigtfunc(line(il)%wl + dopshift_wl + &
                         line(il)%par%wlshift, &
                         atm(ia)%par%damp, &
                         atm(ia)%par%dopp, &
                         wl,nwl,hprof,fprof)
                    i_center= 1*fctv*(hprof)* &
                         atm(ia)%par%etazero * line(il)%par%strength
                    if (magopt.eq.1) then
                      rho_p=1*fctv*(fprof) * &
                           atm(ia)%par%etazero * line(il)%par%strength
                    else
                      rho_p=0.
                    end if
                  else
                    !*DAVID
                    !DEFINITIONS
                    i_center=0 ; rho_p=0 
                    do II=1,line(il)%quan%N_PI
                      call voigtfunc(line(il)%wl+dopshift_wl-zeeman_P(II) + &
                           line(il)%par%wlshift, &
                           atm(ia)%par%damp, &
                           atm(ia)%par%dopp, &
                           wl,nwl,hprof,fprof)

                      i_center= i_center+ &
                           fctv*hprof*atm(ia)%par%etazero*wep(II) * &
                           line(il)%par%strength
                      if (magopt.eq.1) then
                        rho_p=rho_p + fctv*fprof*atm(ia)%par%etazero * &
                             wep(II) * line(il)%par%strength
                      end if
                    end do
                  end if
                  !END DAVID
                else
                  call gaussfunc(atm(ia)%par%a0 * line(il)%par%strength, &
                       line(il)%wl + dopshift_wl + line(il)%par%wlshift, &
                       width2, &
                       wl,nwl,gprof,addnum)
                  i_center=1*fctv*(gprof-addnum)
                  rho_p=0.
                end if              !! voigt
                !!    calculate magnetic profiles only if b
                !!    is to be fitted
                if ((atm(ia)%fit%b.eq.1).or. &
                     (atm(ia)%par%b.ne.0)) then

                  !!    calculate shifted component (SIGMA)
                  if (voigt.eq.1) then
                    IF (use_geff.eq.1) THEN 
                      call voigtfunc(line(il)%wl+dopshift_wl-zeeman_shift + &
                           line(il)%par%wlshift, &
                           atm(ia)%par%damp, &
                           atm(ia)%par%dopp, &
                           wl,nwl,hprof,fprof)
                      i_minus     =fctv*(hprof)* atm(ia)%par%etazero * &
                           line(il)%par%strength
                      if (magopt.eq.1) then
                        rho_minus=fctv*(fprof)* atm(ia)%par%etazero * &
                             line(il)%par%strength
                      else
                        rho_minus=0.
                      end if
                      call voigtfunc(line(il)%wl+dopshift_wl+zeeman_shift + &
                           line(il)%par%wlshift, &
                           atm(ia)%par%damp, &
                           atm(ia)%par%dopp, &
                           wl,nwl,hprof,fprof)
                      i_plus     =fctv*(hprof)* atm(ia)%par%etazero * &
                           line(il)%par%strength
                      if (magopt.eq.1) then
                        rho_plus=fctv*(fprof)* atm(ia)%par%etazero * &
                             line(il)%par%strength
                      else
                        rho_plus=0.
                      end if
                    else
                      !*DAVID
                      i_minus=0.   ; i_plus=0.
                      rho_minus=0. ; rho_plus=0.
                      do II=1,line(il)%quan%N_SIG
                        call voigtfunc(line(il)%wl+dopshift_wl- &
                             zeeman_r(ii) + line(il)%par%wlshift,&
                             atm(ia)%par%damp, &
                             atm(ia)%par%dopp, &
                             wl,nwl,hprof,fprof)
                        i_minus=i_minus + fctv*hprof*atm(ia)%par%etazero* &
                             wer(II) * line(il)%par%strength
                        if (magopt.eq.1) then
                          rho_minus=rho_minus + fctv * &
                               fprof*atm(ia)%par%etazero* &
                               wer(II) * line(il)%par%strength
                        end if
                        call voigtfunc(line(il)%wl+dopshift_wl- &
                             zeeman_b(ii) + line(il)%par%wlshift,&
                             atm(ia)%par%damp, &
                             atm(ia)%par%dopp, &
                             wl,nwl,hprof,fprof)
                        i_plus=i_plus + &
                             fctv*hprof*atm(ia)%par%etazero* &
                             web(II) * line(il)%par%strength
                        if (magopt.eq.1) then
                          rho_plus=rho_plus + fctv*fprof * &
                               atm(ia)%par%etazero* &
                               web(II) * line(il)%par%strength
                        end if
                      end do
                      !END DAVID
                    end if
                  else
                    call gaussfunc(atm(ia)%par%a0 * line(il)%par%strength, &
                         line(il)%wl + dopshift_wl - zeeman_shift + &
                         line(il)%par%wlshift, &
                         width2, &
                         wl,nwl,gprof,addnum)
                    i_minus=fctv*(gprof-addnum)
                    rho_minus=0.

                    call gaussfunc(atm(ia)%par%a0 * line(il)%par%strength, &
                         line(il)%wl + dopshift_wl + zeeman_shift + &
                         line(il)%par%wlshift, &
                         width2, &
                         wl,nwl,gprof,addnum)
                    i_plus=fctv*(gprof-addnum)
                    rho_plus=0.
                  end if            !! voigt

                  !!    compute v-profile: Sum of to
                  !!    gaussians with opposite sign, shifted
                  !!    by Zeeman splitting value
                  !!    see ronan et al, 1987 SoPhys vol 113, p353
                  eta_v = eta_v+line(il)%f * 1./2.*(i_plus - i_minus) * cosinc
                  rho_v=  rho_v+line(il)%f * 1./2.*(rho_plus - rho_minus) * cosinc

                  !!    compute q-profile: +Q direction
                  !!    defines azimuth angle zero.
                  !!    see ronan et al, 1987 SoPhys vol 113, p353
                  quprof = line(il)%f * 1./2.*( i_center - &
                       1./2.*(i_plus + i_minus) )
                  rho_qu = line(il)%f * 1./2.*( rho_p - &
                       1./2.*(rho_plus + rho_minus) )
                  eta_q= eta_q+quprof * (sininc2) * cos2azi      
                  rho_q= rho_q+rho_qu * (sininc2) * cos2azi
                  !!    u-profile is q profile x tan(2*azi)
                  !!    see auer et al., 1977 SoPhys, vol 55,
                  !!    p47
                  eta_u= eta_u+quprof * (sininc2) * sin2azi
                  rho_u= rho_u+rho_qu * (sininc2) * sin2azi

                else                !!fit B
                  !!    sigma components are the same as central pi
                  !!    component because of zero land shift.
                  eta_q=0
                  eta_u=0
                  eta_v=0
                  i_plus=i_center
                  i_minus=i_center
                  rho_q=0
                  rho_u=0
                  rho_v=0
                  rho_plus=rho_p
                  rho_minus=rho_p
                end if              !!fit B

                eta_i=eta_i+line(il)%f * 1./2.*(i_center * (sininc2) &
                     + 1./2. * (i_minus + i_plus) * &
                     (1+(cosinc)**2.))

              else 
                !calculate only I profile with a simple fit of a voigt
                !or gaussian, no rad. transfer...
                !This is used only for the
                !straypol_run. The idea is to fit the
                !I profile with a single voigt or
                !gauss to determine the shape of the
                !profile to be used for the correction
                !in Q and U

                if ((line(il)%straypol_use.eq.1).and.&
                     (atm(ia)%straypol_use.eq.1)) then 
                   if (voigt.eq.1) then 
                    call voigtfunc(line(il)%wl + dopshift_wl, &
                         atm(ia)%par%damp, &
                         atm(ia)%par%dopp, &
                         wl,nwl,hprof,fprof)
                    gsub=hprof*atm(ia)%par%etazero*line(il)%par%strength
                  else
                    call gaussfunc(atm(ia)%par%a0 * line(il)%par%strength, &
                         line(il)%wl + dopshift_wl, &
                         2.*atm(ia)%par%width**2., &
                         wl,nwl,gprof,addnum)
                    gsub=(gprof-addnum)
                    !                 gsub=(gprof-addnum)*atm(ia)%par%etazero* &
                    !                      FF(ia)*line(il)%f
                  end if
                else
                  gsub=0
                end if
                eta_i=eta_i+line(il)%f*gsub
                DELTA=( (1 + eta_i)**2. * (1 + eta_i)**2.)
                F2DELTA=atm(ia)%par%sgrad * FF(ia)/DELTA
                if (modeval.eq.4) then
!                  B0=atm(ia)%par%szero
!                 B0=(-1+atm(ia)%par%szero- atm(ia)%par%sgrad)/sum(luse(:,ia))
!                  B0=(1+atm(ia)%par%szero)/sum(luse(:,ia))-atm(ia)%par%sgrad
                  SZERO=atm(ia)%par%szero
                else
                  SZERO=1. - atm(ia)%par%sgrad
                end if
                B0=SZERO
                I=B0*FF(ia) + F2DELTA*( (1+eta_i) * ((1+eta_i)**2.))
                !              I=1./(1+gsub/2.)/natm !same as for i in 'real' case
                Q=0
                U=0
                V=0
              end if
            end if
          end if !use line
        end do  !line

        if (modeval.ne.5) then
                R=(eta_q*rho_q + eta_u*rho_u +eta_v*rho_v)
                DELTA=( (1 + eta_i)**2. * &
                     ( (1 + eta_i)**2. - eta_q**2. - eta_u**2. - eta_v**2. &
                     + rho_q**2. + rho_u**2. + rho_v**2. ) - R**2.)
                F2DELTA=atm(ia)%par%sgrad * FF(ia)/DELTA

                !source function at tau=0: B0
                !B0 must match continuum level outside line
                !           B0 = atm(ia)%par%szero
!                atm(ia)%par%szero=line(1)%icont
                if (modeval.eq.4) then
!                  B0=atm(ia)%par%szero
!                  B0=(-1+atm(ia)%par%szero-atm(ia)%par%sgrad)/sum(luse(:,ia))
!                  B0=(1+atm(ia)%par%szero)/sum(luse(:,ia))-atm(ia)%par%sgrad
                  SZERO=atm(ia)%par%szero
                else
                  SZERO=1. - atm(ia)%par%sgrad
                end if
                B0 =SZERO


                I=B0*FF(ia) + F2DELTA*( (1+eta_i) * &
                     ((1+eta_i)**2. +rho_q**2.+rho_u**2.+rho_v**2.) )
                Q =-F2DELTA*( (1+eta_i)**2. * eta_q + &
                     (1+eta_i)*(eta_v*rho_u-eta_u*rho_v) + rho_q*R )
                U =-F2DELTA*( (1+eta_i)**2. * eta_u + &
                     (1+eta_i)*(eta_q*rho_v-eta_v*rho_q) + rho_u*R )
                V =-F2DELTA*( (1+eta_i)**2. * eta_v + &
                     (1+eta_i)*(eta_u*rho_q-eta_q*rho_u) + rho_v*R)
                !          V =-F2DELTA*( (1+eta_i)**2. * eta_v + (1+eta_i)*(eta_u*rho_q &
                !               -eta_q*rho_u)  + rho_v*R)

              qprof = qprof + Q
              uprof = uprof + U
              vprof = vprof + V
              !store only absorption signature in
              !I. This absorption signature is
              !subtracted from 1 after the loops
              !over lines & atmospheres
              if (old_norm.eq.1) then
                iprof = iprof + I - FF(ia)
              else if (old_norm.eq.-1) then
                iprof = iprof + I - FF(ia)
              else if (old_norm.eq.2) then
                iprof = iprof + I
              else if (old_norm.eq.0) then
                iprof = iprof + I
              endif
            end if
            do il=1,nline
              if (modeval.ne.5) then 
            !add straypolarization fit
            if ((line(il)%straypol_use.eq.1).and.&
                 (atm(ia)%straypol_use.eq.1)) then 
              if ((line(il)%straypol_par(ia)%width.gt.1e-5).or.&
                   (line(il)%straypol_par(ia)%damp.gt.1e-5).or.&
                   (line(il)%straypol_par(ia)%dopp.gt.1e-5)) then
                if (voigt.eq.1) then
                  if (modeval.eq.1) then
                    straydamp=line(il)%straypol_par(ia)%damp
                  else
                    if ((modeval.eq.2).or.(modeval.eq.3).or.&
                         (modeval.eq.4)) then
                      straydamp=&
                           line(il)%straypol_par(ia)%damp*line(il)%wl**2 / &
                           (4*DPI*clight* &
                           line(il)%straypol_par(ia)%dopp)
                    end if
                  end if
                  call voigtfunc(line(il)%wl* &
                       (1.+line(il)%straypol_par(ia)%vlos/clight), &
                       straydamp, &
                       line(il)%straypol_par(ia)%dopp, &
                       wl,nwl,hprof,fprof)
                  gsub=hprof * FF(ia)*line(il)%par%straypol_eta0
                else
                  call gaussfunc(line(il)%par%straypol_amp * FF(ia), &
                       line(il)%wl* &
                       (1.+line(il)%straypol_par(ia)%vlos/clight), &
                       2*line(il)%straypol_par(ia)%width**2, &
                       wl,nwl,gsub,addnum)
                  gsub=gsub-addnum
                end if
                !       gsub=gsub * line(il)%f
                if (line(il)%straypol_par(ia)%sign.eq.-1) gsub=-gsub
                !        if (abs(line(il)%straypol_par(ia)%slitori_solar-999).lt.1) then
                !correction along B (Hanle)
                qsub=+gsub*cos((atm(ia)%par%azi*2+hanle_azi)*0.01745329)
                usub=+gsub*sin((atm(ia)%par%azi*2+hanle_azi)*0.01745329)
                !        else !correction along limb ()
                !           qsub=+gsub*cos(2*obs_par%slit_orientation*0.01745329)
                !           usub=+gsub*sin(2*obs_par%slit_orientation*0.01745329)
                !        end if
                qprof=qprof+qsub
                uprof=uprof+usub
              end if
            end if
          end if
       end do !!line
      end iF
    end do !!atm
    !compute


!   if (doconv.ne.0) then !perform convolution
    wlnew=wl
    nwlnew=nwl
    if ((doconv.ne.0).or.(doprefilter.ne.0)) then
       !use FFT for convolution
       if (cmode.eq.0) then
          if (doprefilter.ne.0) then !perform prefilter
             iprof=iprof*prefilterval
             qprof=qprof*prefilterval
             uprof=uprof*prefilterval
             vprof=vprof*prefilterval
          end if
          if (doconv.ne.0) then !perform convolution
             CALL RFFTI( int(nwl), WSAVE )
             convfft=convval
             CALL RFFTF( int(nwl), convfft, WSAVE )
             call convolve(int(nwl),iprof,convfft,wsave)
             call convolve(int(nwl),qprof,convfft,wsave)
             call convolve(int(nwl),uprof,convfft,wsave)
             call convolve(int(nwl),vprof,convfft,wsave)
             if (nret_wlidx.gt.0) then
                iprof(1:nret_wlidx)=iprof(ret_wlidx(1:nret_wlidx))
                qprof(1:nret_wlidx)=qprof(ret_wlidx(1:nret_wlidx))
                uprof(1:nret_wlidx)=uprof(ret_wlidx(1:nret_wlidx))
                vprof(1:nret_wlidx)=vprof(ret_wlidx(1:nret_wlidx))
             end if
             wlnew(1:nret_wlidx)=wl(ret_wlidx(1:nret_wlidx))
             nwlnew=nret_wlidx
          end if
       else 
          !simulate measurement WITHOUT CONVOLUTION!
          !do a simple multiplication of filter curves
          if (doconv.eq.1) then
             wlnew(1:nret_wlidx)=wl(ret_wlidx(1:nret_wlidx))
             nwlnew=nret_wlidx
             do iw=1,nret_wlidx
                cfp(1:nwl)=cshift(convval(1:nwl),-(ret_wlidx(iw)-1))
                if (doprefilter.eq.1) cfp(1:nwl)=cfp(1:nwl)*prefilterval(1:nwl)
                !            cfpsum=sum(cfp(1:nwl))
                cfpsum=1. !PF curve is normalized to max (in read_prefilter),
                !FP curve is normalized to one FSR (in read_conv)
                iproftmp(iw)=sum(iprof(1:nwl)*cfp(1:nwl))/cfpsum
                qproftmp(iw)=sum(qprof(1:nwl)*cfp(1:nwl))/cfpsum
                uproftmp(iw)=sum(uprof(1:nwl)*cfp(1:nwl))/cfpsum
                vproftmp(iw)=sum(vprof(1:nwl)*cfp(1:nwl))/cfpsum
             end do
             iprof=0 ; iprof(1:nwl)=iproftmp(1:nwl)
             qprof=0 ; qprof(1:nwl)=qproftmp(1:nwl)
             uprof=0 ; uprof(1:nwl)=uproftmp(1:nwl)
             vprof=0 ; vprof(1:nwl)=vproftmp(1:nwl)
          else
             iprof=iprof*prefilterval
             qprof=qprof*prefilterval
             uprof=uprof*prefilterval
             vprof=vprof*prefilterval
          end if
       end if
    end if


    !    if (doprefilter.ne.0) then !perform prefilter
!     if (nret_wlidx.gt.0) then
!        iprof(1:nret_wlidx)=iprof(1:nret_wlidx)*prefilterval(ret_wlidx(1:nret_wlidx))
!        qprof(1:nret_wlidx)=qprof(1:nret_wlidx)*prefilterval(ret_wlidx(1:nret_wlidx))
!        uprof(1:nret_wlidx)=uprof(1:nret_wlidx)*prefilterval(ret_wlidx(1:nret_wlidx))
!        vprof(1:nret_wlidx)=vprof(1:nret_wlidx)*prefilterval(ret_wlidx(1:nret_wlidx))
!     else
!        iprof=iprof*prefilterval
!        qprof=qprof*prefilterval
!        uprof=uprof*prefilterval
!        vprof=vprof*prefilterval
!     end if
!   end if


!write(*,*) 'HIERCP1',vprof(3),ff,lsi(3)
!write(*,*) 'HIERLine',line(1)%icont
!write(*,*) 'HIERCPflags',doconv,cmode,doprefilter,nret_wlidx,init,voigt,magopt,use_geff,use_pb,pb_method,modeval,iprof_only,normff,old_norm,icomp,hanle_azi,norm_stokes_val,old_voigt

!write(*,*) 'HIER vprof1',vprof(1:3),lsv(1:3),icomp,natm,dols,old_norm,lspol
    if (dols.eq.1) then !add localstraylight component after convolution
      if ((icomp.eq.-1).or.(icomp.eq.natm)) then
        if (old_norm.eq.0) then
          iprof=iprof+ff(natm)*(lsi)
        else
          iprof=iprof+ff(natm)*(lsi-1.)
        endif
        if (lspol.ne.0) then 
          qprof=qprof+ff(natm)*(lsq)
          uprof=uprof+ff(natm)*(lsu)
          vprof=vprof+ff(natm)*(lsv)
        end if
      end if
    end if
!write(*,*) 'HIER vprof2',vprof(1:3)


                                !for computing individual components:
                                !bring the continuum level to unity
    if (normff.eq.0) then 
       iprof=iprof+1-sum(ff(ia1:ia2))
    end if
  

    !use old normalization (changes SGRAD
    !when changing number of lines!)
    select case (old_norm)
    case(1)
       iprof=(1.+iprof)*line(1)%icont
       iprof=(iprof-line(1)%icont)/nline+line(1)%icont
       qprof=qprof/nline
       uprof=uprof/nline
       vprof=vprof/nline     
    case(2)
       iprof=iprof*line(1)%icont/nline
       qprof=qprof/nline
       uprof=uprof/nline
       vprof=vprof/nline     
    case(-1)
       iprof=(1.+iprof)*line(1)%icont
    case default
      iprof=iprof*line(1)%icont
!      iprof=iprof*1.
    end select

    !profiles normalized to  I
    if (norm_stokes_val.eq.1) then
      qprof=qprof/(iprof/line(1)%icont)
      uprof=uprof/(iprof/line(1)%icont)
      vprof=vprof/(iprof/line(1)%icont)
    end if
    
    !straylight correction
    !(non polarized, non-dispersive)
    iprof=iprof*(1.-gen%par%straylight)+gen%par%straylight*line(1)%icont

    !fit continuum level + straylight reduction of Q,U,V
    iprof=iprof*gen%par%ccorr
    qprof=qprof*gen%par%ccorr!*(1.-gen%par%straylight)
    uprof=uprof*gen%par%ccorr!*(1.-gen%par%straylight)
    vprof=vprof*gen%par%ccorr!*(1.-gen%par%straylight)

    !calculate voigt profile for blend 
    !(only for final profile, not for individual components)
    if (icomp.eq.-1) then
       do ib=1,nblend
          if (blend(ib)%par%a0.ne.0) then
             call voigtfunc(blend(ib)%par%wl,blend(ib)%par%damp, &
                  blend(ib)%par%width,wlnew,nwlnew,&
                  hprof,fprof)
             iprof=iprof*(1-blend(ib)%par%a0*hprof)
             qprof=qprof*(1-blend(ib)%par%a0*hprof)
             uprof=uprof*(1-blend(ib)%par%a0*hprof)
             vprof=vprof*(1-blend(ib)%par%a0*hprof)
          end if
       end do
    end if

    
  END SUBROUTINE compute_profile
!!!    ========================================================
