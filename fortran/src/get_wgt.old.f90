subroutine get_idx(vec,n,rg,idx)
  implicit none
  integer(2) :: n
  real(8) :: vec(n),rg(2)
  logical :: idx(n)
  
  idx=.false.
  where ((vec(1:n).ge.rg(1)).and.(vec(1:n).le.rg(2))) idx=.true.

end subroutine get_idx

subroutine get_wgt(wgt,obs)
  use all_type
  use ipt_decl
  implicit none
  type (proftyp) :: wgt,obs
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  logical,dimension(obs%nwl) :: good,good_mag,vgood,vgood_mag,igood_red
  real(8),dimension(2) :: good_range,good_mag_range,vgood_range,vgood_mag_range,igood_red_range
  real(4),dimension(obs%nwl) :: wgti,wgtmag,scl_mag
  external get_idx


  wgt=obs
  wgt%i=0.
  wgt%q=0.
  wgt%u=0.
  wgt%v=0.

  good_range=[10828.5,10831.5]
  vgood_range=[10829.6,10831.5]
  igood_red_range=[10832.5,10835.0]

  good_mag_range=[10828.5,10833.5]
  vgood_mag_range=[10829.6_8,10833.5_8]
  
  call get_idx(obs%wl,obs%nwl,good_range,good)
  call get_idx(obs%wl,obs%nwl,vgood_range,vgood)
  call get_idx(obs%wl,obs%nwl,igood_red_range,igood_red)
  

  call get_idx(obs%wl,obs%nwl,good_mag_range,good_mag)
  call get_idx(obs%wl,obs%nwl,vgood_mag_range,vgood_mag)

  wgti=0.0
  wgtmag=0.0
  where (good.eq..true.) 
     wgti=0.2
     wgtmag=0.2
  end where
  where (vgood.eq..true.) 
     wgti=1.
     wgtmag=1.
  end where
  where (vgood_mag.eq..true.) wgtmag=1.
  where (igood_red.eq..true.) wgti=0.5*0

  !normalize
  if (sum(wgti).lt.1e-8) then
     write(*,*) 'Weighting problem (I) - set weighting to 1'
     wgti=1.
  end if
  if (sum(wgtmag).lt.1e-8) then
     write(*,*) 'Weighting problem (mag) - set weighting to 1'
     wgtmag=1.
  end if
  wgti=wgti/maxval(wgti)
  wgtmag=wgtmag/maxval(wgtmag)

  !scale weighting between I and QU according to sig strength
  scl_mag=(maxval(obs%i(1:obs%nwl))-minval(obs%i(1:obs%nwl)))/obs%ic/ &
       sqrt(maxval(obs%q(1:obs%nwl)**2+obs%u(1:obs%nwl)**2+ &
       obs%v(1:obs%nwl)**2))
  wgti=wgti/scl_mag

  wgt%i(1:obs%nwl)=wgti
  wgt%q(1:obs%nwl)=wgtmag
  wgt%u(1:obs%nwl)=wgtmag
  wgt%v(1:obs%nwl)=wgtmag

end subroutine get_wgt
