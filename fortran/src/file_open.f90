subroutine file_open(file,path,np,unit,error,verbose)
  use all_type
  implicit none
  integer(2) :: verbose,np,ip
  integer(4) :: error,unit
  character(LEN=maxstr) :: file
  character(LEN=maxstr),dimension(np) :: path

  error=1
  unit=101
  ip=0
  do while ((error.ne.0).and.(ip.lt.np))
     ip=ip+1
     open(unit,file=trim(path(ip))//"/"//trim(file),iostat=error, &
          action='READ',form='FORMATTED')
  end do
  
  if (verbose.ge.1) then
     if (error.eq.0) then
        write(*,*) 'Opening file '//trim(path(ip))//'/'//trim(file) 
     else
        write(*,*) 'File '//trim(file)//' not found.'
     end if
  end if
 

end subroutine file_open
