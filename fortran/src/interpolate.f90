module interpol

  contains
!************************************************************************
!*                                                                      *
!*                                                                      *
!************************************************************************
!*                                                                      *
!*   Function INTERPOLATE performs a Lagrangian interpolation of the    *
!*   dependent variable y for a given value of the independent          *
!*   variable x.  Reference:  James & James, Mathematics Dictionary,    *
!*   3rd edition, Van Nostrand.                                         *
!*                                                                      *
!*   Input                                                              *
!*   -----                                                              *
!*   x:  the value of the absicca for which the interpolation is        *
!*          desired.                                                    *
!*   xr:  the array of distinct, monotonically increasing abcissas.     *
!*   yr:  the array of corresponding ordinates.                         *
!*   degree:  the degree of the interpolation polynomial;               *
!*          1 = linear, 2 = quadratic, 3 = cubic, etc.                  *
!*   npts:  the number of elements in the xr and yr arrays.             *
!*                                                                      *
!************************************************************************
FUNCTION  INTERPOLATE (x, xr, yr, degree, npts)

  REAL*8 interpolate

  integer(2)  degree, npts, i, j, k, firstpt, lastpt
  real(8)  x, xr(npts)
  real(8)  yr(npts), y, sum, product


  !******   Locate the position of x in the array xr...

  i = 1
  do while  (((x - xr(i)) .gt. 0.0) .and. (i .lt. npts))
     i = i + 1
  end do
  !******   If x is equal to one of the points in xr, then no interpolation
  !******   is necessary...

  if  ((x - xr(i)) .eq. 0.0)  then
     y = yr(i)

     !******   Otherwise, use interpolation.  First, make sure that the inputs
     !******   make sense...

  else
     if  (degree .gt. (npts - 1))  then
        degree = npts - 1
     else if  (degree .eq. 0)  then
        stop 'degree of polynomial=0'
     end if

     !******   Now calculate the first and last points of the polynomial.
     !******   If x < xr(2) or x > xr(npts-1), then the job is simple...

     if  ((i .eq. 1) .or. (i .eq. 2))  then
        firstpt = 1
     else if  (i .eq. npts)  then
        firstpt = npts - degree

        !******   If xr(2) < x < xr(npts-1), and x is closer to 
        !******   xr(i-1) than xr(i), then prefer xr(i-2) over xr(i+1)...

     else if  (x .lt. ((xr(i-1) + xr(i)) / 2.0))  then
        firstpt = i - (degree + 2) / 2

        !******   On the other hand, if it's closer to xr(i), then...

     else
        firstpt = i - (degree + 1) / 2
     end if

     if  (firstpt .lt. 1)  then
        firstpt = 1
     end if
     lastpt = firstpt + degree
     if  (lastpt .gt. npts)  then
        lastpt = npts
     end if


     !******   Now let's interpolate...

     sum = 0.0
     do  k = firstpt, lastpt
        product = 1.0
        do  j = firstpt, lastpt
           if (k .ne. j) then
              product = product * (x - xr(j)) /  (xr(k) - xr(j))
           end if
        end do
        sum = sum + yr(k) * product
     end do
     y = sum
  end if

  INTERPOLATE = y

  return
end Function INTERPOLATE  ! interpolate
end module interpol
