!!Routine to read in input file
subroutine def_line(icomp,line,nline,verbose)
  use all_type
  use ipt_decl
  use tools
  implicit none
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  type (linetyp) :: line(maxlin)
  integer(2) :: ia,il,i,j,icomp,iac(2),nline,verbose
  integer(2) :: hedone,sidone,cadone,fedone
  character(len=idlen) :: lstr,ids(5)

  if (icomp.eq.0) then
     iac=(/1_2,ipt%ncomp/)
  else 
     iac=(/icomp,icomp/)
  end if

  hedone=0
  sidone=0
  cadone=0
  fedone=0

  nline=0
  line%straypol_use=0
  do ia=iac(1),iac(2)
     do il=1,maxlin
        do i=1,idlen 
           lstr(i:i)=char(ipt%atm(ia)%use_line(i,il))
        end do
        call upcase(lstr)
        select case (lstr(1:2))
        case ('HE')
           if (hedone.eq.0) then
              hedone=1
              nline=nline+3
              ids(1:3)=(/ 'HeI 10829.1', 'HeI 10830.2', 'HeI 10830.3'/)
              do j=1,3
                 do i=1,len(trim(ids(j)))
                    line(j+nline-3)%id(i)=ichar(ids(j)(i:i))
                 end do
              end do
              line(nline-2:nline)%wl=(/10829.0911_8,10830.2501_8,10830.3397_8/)
              line(nline-2:nline)%f=(/ 0.111, 0.333, 0.556/)
!              line(nline-2:nline)%geff=(/ 2.0, 1.75, 0.875/) !Tr3 = 1.25?
              line(nline-2:nline)%geff=(/ 2.0, 1.75, 1.25/) !Tr3 = 1.25?
              line(nline-2:nline)%jl=(/1,1,1/)
              line(nline-2:nline)%ju=(/0,1,2/)
              line(nline-2:nline)%sl=(/1,1,1/)
              line(nline-2:nline)%su=(/1,1,1/)
              line(nline-2:nline)%ll=(/0,0,0/)
              line(nline-2:nline)%lu=(/1,1,1/)

              !EXECUTE ROUTINE CUANTEN FOR MULTIPLE SPLITTING *DAVID
              call create_nc(line(nline-2:nline),3_2,verbose)


              line(nline-2:nline)%mass=4
                                   !fit flag for straypol
              line(nline-2:nline)%fit%straypol_amp=0 
              line(nline-2:nline)%par%straypol_amp=0 

              line(nline-2:nline)%fit%straypol_eta0=0 
              line(nline-2:nline)%par%straypol_eta0=0 

              line(nline-2:nline)%scale%straypol_amp%min=0 
              line(nline-2:nline)%scale%straypol_amp%max=+0.004
              line(nline-2:nline)%scale%straypol_eta0%min=0 
              line(nline-2:nline)%scale%straypol_eta0%max=+0.008
!              line(nline-2:nline)%scale%straypol_eta0%max=+0.100
              !parameter defining sign of correction
              line(nline-2)%straypol_par%sign=-1 
              line(nline-1)%straypol_par%sign=+1 
              line(nline-0)%straypol_par%sign=+1 
              line(nline-2:nline)%straypol_use=1
              do i=1,3 !read in Paschen-Back table
                 call def_pb(line(nline-i+1),ids(4-i))
              end do
         end if
        case ('SI')
           if (sidone.eq.0) then
              sidone=1
              nline=nline+1
              ids(1)='SiI 10827.1'
              do i=1,len(trim(ids(1)))
                 line(nline)%id(i)=ichar(ids(1)(i:i))
              end do
              line(nline)%wl=10827.088_8
!              line(nline)%f=10.**0.220
              line(nline)%f=1.
              line(nline)%geff=1.5
              line(nline)%mass=28
           end if
        case ('CA')
           if (cadone.eq.0) then
              cadone=1
              nline=nline+1
              ids(1)='CaI 10829.3'
              do i=1,len(trim(ids(1)))
                 line(nline)%id(i)=ichar(ids(1)(i:i))
              end do
              line(nline)%wl=10829.268_8
!              line(nline)%f=10.**(-0.944)
              line(nline)%f=1.
              line(nline)%geff=1.0
              line(nline)%mass=40
           end if
        case ('FE')
           if (fedone.eq.0) then
              fedone=1
              nline=nline+2
              ids(1:2)=(/ 'FeI 6301.5', 'FeI 6302.5' /)
              do j=1,2
                 do i=1,len(trim(ids(j)))
                    line(j+nline-2)%id(i)=ichar(ids(j)(i:i))
                 end do
              end do
              line(nline-1:nline)%wl=(/ 6301.5012_8, 6302.4936_8 /)
              line(nline-1:nline)%f=10**(/ -0.718,-1.235 /)
              line(nline-1:nline)%f=line(nline-1:nline)%f/ &
                   sum(line(nline-1:nline)%f)
              line(nline-1:nline)%geff=(/ 1.5,2.5 /)
              line(nline-1:nline)%mass=56
           end if
        case default
        end select
     end do
  end do

!  line(1:nline)%f= line(1:nline)%f/sum( line(1:nline)%f)

end subroutine def_line
