!this subroutine evaluates the atomic quanten numbers for
!the anomalous zeeman effect
!1-> LOW
!2-> UP
subroutine Cuanten(S1,S2,L1,L2,J1,J2,msig1,mpi,msig2,sig1,pi,sig2,g1,g2,geff)
  use all_type
  implicit none

  real(4) :: S1,S2,L1,L2,J1,J2
  real(8),dimension(maxsigma) :: msig1,msig2,sig1,sig2
  real(8),dimension(maxpi) :: mpi,pi
  real(8) :: g1,g2,geff
  real(4) :: m1(:),m2(:)
  integer(2) :: jj,n_pi,n_sig,ipi,isig1,isig2,i,j,im
  allocatable m1,m2

  allocate(m1(int(2*j1+1)))
  allocate(m2(int(2*j2+1)))

  msig1=0
  msig2=0
  sig1=0
  sig2=0
  mpi=0
  pi=0
  g1=0
  g2=0
  geff=0

  !magnetic quanten number Mlo Mup
  do i=0,int(2*j1)
     m1(i+1)=i-j1
  end do
  do i=0,int(2*j2)
     m2(i+1)=i-j2
  end do
  JJ = int(J2-J1)
  N_PI=2.*MINval((/j1,j2/))+1 !Number of pi components
  N_SIG=j1+j2 !Number of sigma components

  if (n_sig.eq.0) then
     write(*,*) 'Number of sigma components = 0.'//&
          'Please check your atomic data file.'
     stop
  end if

  !lande factors (with 'if' because j could be zero)
  if (j1.ne.0) then
     g1=real((3*j1*(j1+1)+s1*(s1+1)-l1*(l1+1)),kind=8)/(2.*j1*(j1+1))
  else
     g1=0
  end if
  if (j2.ne.0) then
     g2=real((3*j2*(j2+1)+s2*(s2+1)-l2*(l2+1)),kind=8)/(2.*j2*(j2+1))
  else
     g2=0
  end if
  geff=((g1+g2)/2.+(g1-g2)*(j1*(j1+1.)-j2*(j2+1.))/4.)

  !TYPE OF TRANSITIONS
  !BLUE COMPONENT => Mlo-Mup = +1
  !RED COMPONENT => Mlo-Mup = -1
  !CENTRAL COMPONENT => Mlo-Mup = 0

  !COUNTERS FOR THE COMPONENTS
  ipi=1
  isig1=1
  isig2=1

  do j=1,int(2*j1)+1
     do i=1,int(2*j2)+1
        IM=int(M2(i)-M1(j))
        select case (IM)
        case (0_2)          !M -> M  ;CENTRAL COMPONENT
           select case (JJ)
           case (-1_2)   !  j -> j-1
              pi(ipi)=j1**2-m1(j)**2
              mpi(ipi)=g1*m1(j)-g2*m2(i)
              ipi=ipi+1
           case (0_2)    !  j -> j
              pi(ipi)=m1(j)**2
              mpi(ipi)=g1*m1(j)-g2*m2(i)
              ipi=ipi+1
           case (1_2)    !  j -> j+1
              pi(ipi)=(j1+1.)**2-m1(j)**2
              mpi(ipi)=g1*m1(j)-g2*m2(i)
              ipi=ipi+1
           end select
        case (1_2)          !M -> M+1  ;BLUE COMPONENT
           select case (JJ)
           case (-1_2)   !  j -> j-1
              sig1(isig1)=(j1-m1(j))*(j1-m1(j)-1)/4.
              msig1(isig1)=g1*m1(j)-g2*m2(i)
              isig1=isig1+1
           case (0_2)    !  j -> j
              sig1(isig1)=(j1-m1(j))*(j1+m1(j)+1)/4.
              msig1(isig1)=g1*m1(j)-g2*m2(i)
              isig1=isig1+1
           case (1_2)    !  j -> j+1
              sig1(isig1)=(j1+m1(j)+1)*(j1+m1(j)+2)/4.
              msig1(isig1)=g1*m1(j)-g2*m2(i)
              isig1=isig1+1
           end select
        case (-1_2)           !M -> M-1   ;RED COMPONENT
           select case (JJ)
           case (-1_2)   !  j -> j-1
              sig2(isig2)=(j1+m1(j))*(j1+m1(j)-1)/4.
              msig2(isig2)=g1*m1(j)-g2*m2(i)
              isig2=isig2+1
           case (0_2)    !  j -> j
              sig2(isig2)=(j1+m1(j))*(j1-m1(j)+1)/4.
              msig2(isig2)=g1*m1(j)-g2*m2(i)
              isig2=isig2+1
           case (1_2)    !  j -> j+1
              sig2(isig2)=(j1-m1(j)+1)*(j1-m1(j)+2)/4.
              msig2(isig2)=g1*m1(j)-g2*m2(i)
              isig2=isig2+1
           end select
        case default
        end select
     end do
  end do

  !normalization OF EACH COMPONENT  
  pi=pi/sum(pi)      !*0.50
  sig1=sig1/sum(sig1)!*0.25
  sig2=sig2/sum(sig2)!*0.25
  !normalization OF ALL COMPONENTS  
!  pi=pi/(sum(pi)+sum(sig1)+sum(sig2))
!  sig1=sig1/(sum(pi)+sum(sig1)+sum(sig2))
!  sig2=sig2/(sum(pi)+sum(sig1)+sum(sig2))

  deallocate(m1)
  deallocate(m2)

end subroutine cuanten


