program vtest
  implicit none
  real(8) :: x(100)
  real(4) :: a,f(100),h(100)
  integer(2) :: i
  external voigt

  do i=1,100
     x(i)=(i-1.)/10
  end do

  call voigt(100_2,0.2,x,h,f)

  write(*,*) 'H',h
  write(*,*) 'F',f

end program vtest
