!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!                      M O D U L E   F T I M E                                
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
MODULE FTIME
!!
PUBLIC :: REALTIME, CPUTIME, SYSTIME
PRIVATE
!!
CONTAINS
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!! RETURN REAL TIME IN SECONDS SINCE CALLED FOR THE FIRST TIME              %
!! (based on standard FORTRAN-90 routine, T < 1 month)                      %
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   FUNCTION REALTIME()
   IMPLICIT NONE
   REAL(8)     :: REALTIME
!! ---
   REAL(8)           :: SE
   REAL(8),     SAVE :: S1
   INTEGER(4)       :: VAL(8), YE, MO, DA
   INTEGER(4)       :: DPM(12) = (/ 31,28,31,30,31,30,31,31,30,31,30,31 /)
   INTEGER(4), SAVE :: D1, IFIRST =0
!! ---
   CALL DATE_AND_TIME(VALUES=VAL)
   SE=REAL((VAL(5)*60+VAL(6))*60+VAL(7))+REAL(VAL(8))/1000.0_4
   YE=VAL(1)
   MO=VAL(2)
   DA=VAL(3)
   IF (IFIRST.EQ.0) THEN 
     S1=SE
     D1=DA
     IFIRST=1
   ELSE IF (DA.LT.D1) THEN
     IF ((DA.EQ.1).AND.(MO.EQ.3)) THEN                     !! 1st march
       IF (((MOD(YE,4).EQ.0).AND.(MOD(YE,100).NE.0)).OR. &
            (MOD(YE,400).EQ.0)) DA=DA+1                    !! 29  feb
     ENDIF
     DA=DA+DPM(MO)                                         !! new month
   ENDIF
   IF (DA.NE.D1) SE=SE+(DA-D1)*86400.0_8
   REALTIME=SE-S1
   END FUNCTION REALTIME

!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!! Return the cpu time in seconds since the program was startet               %
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   FUNCTION CPUTIME()
#ifdef winnt
   USE DFPORT, ONLY:ETIME
#endif
   IMPLICIT NONE
   REAL(8) :: CPUTIME
!! ---
#ifdef winnt
   REAL(4) :: TA(2)
   CPUTIME=REAL(ETIME(TA),kind=8)
#endif
!! ---
   REAL(4) :: ETIME, TA(2)
   CPUTIME=REAL(ETIME(TA),kind=8)
   END FUNCTION CPUTIME

!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!! %TIME SPENT FOR SYSTEM TASKS  (OR 0 IF NOT IMPLEMENTED)                   %
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   FUNCTION SYSTIME()
#ifdef winnt
   USE DFPORT, ONLY:ETIME
#endif
   IMPLICIT NONE
   REAL(8) :: SYSTIME
!! ---
#ifdef winnt
   REAL(4) :: TA(2),T
   T=ETIME(TA)
   SYSTIME=REAL(TA(2)/T,kind=8)
#endif
   REAL(4) :: ETIME,TA(2),T
   T=ETIME(TA)
   SYSTIME=REAL(TA(2)/T,kind=8)
   END FUNCTION SYSTIME

END MODULE FTIME
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%EOF

