subroutine get_profile(profvec,xyvec,np,froot,datamode,obs,error)
  use all_type
  use ipt_decl
  use localstray_com
  use localstraycomp_com
  use tools
  use piktools
  use pikvar
  implicit none
!  integer(4) :: punit,xunit,xstat
  type (proftyp) :: obs,tobs,bobs,tmpobs(1)
!  integer(2) :: localstray(2)
  integer(2) :: np,i,ncnt,iw,ia,il,il1,j
  character(LEN=maxstr) :: profvec(maxpavg),xyvec(maxpavg),file,froot,hfile
  integer(2) :: datamode
  logical :: ok,error
  integer(2) :: luse(act_nline,act_ncomp)
  logical :: tuse,times_ic
  integer(2) :: xv,yv,yindex,iwin(2),first
!  character(LEN=72) :: comment
  real(4) :: tic,tmpic(1)
!  logical :: anynull
  !  external str2int
  !  integer(2) :: str2int
!  integer(2) :: nwl
  type(convtyp) :: conv
  type(prefiltertyp) :: prefilter

  luse=0
  error=.true.
  times_ic=.false.
  ncnt=0
  do i=1,np
     file=ipt%dir%profile(1:len_trim(ipt%dir%profile))//trim(profvec(i))
     if (ipt%synth.eq.1) then !create synthetic spectrum
        if (ipt%verbose.ge.2) write(*,*) 'Creating synthetic spectrum'
        obs%nwl=ipt%wl_num
        if (obs%nwl.le.1) obs%nwl=256
        do iw=1,obs%nwl
           obs%wl(iw)=real(iw-1)/real(obs%nwl-1)* &
                (ipt%wl_range(2)-ipt%wl_range(1)) + ipt%wl_range(1)
        end do
        !calculate array use: determines if
        !line / atmosphere is used for calculations
        !flag if a atmosphere should be used for a line
        luse=0
        do ia=1,ipt%ncomp 
           do il=1,ipt%nline
              do il1=1,ipt%nline
                 tuse=(count((/(ipt%atm(ia)%use_line(:,il1).eq.&
                      ipt%line(il)%id)/)).eq.idlen)
                 if (tuse.or.(luse(il,ia).eq.1)) luse(il,ia)=1_2
                 !tuse=minval(abs(1*int(ipt%atm(ia)%use_line(:,il1).eq.&
                 !     ipt%line(il)%id)))
                 !luse(il,ia)=1*abs(int((luse(il,ia).eq.1).or.(tuse.eq.1)))
              end do
           end do
        end do

       !fill straypol variables for synthesis
        if (ipt%straypol_amp.ne.0) then
           do il=1,ipt%nline
              ipt%line(il)%straypol_par(1:ipt%ncomp)%width= &
                   ipt%atm(1:ipt%ncomp)%par%width
              ipt%line(il)%straypol_par(1:ipt%ncomp)%vlos= &
                   ipt%atm(1:ipt%ncomp)%par%vlos
              select case (ipt%modeval)
              case(1_2)
                 ipt%line(il)%straypol_par(1:ipt%ncomp)%damp= &
                      ipt%atm(1:ipt%ncomp)%par%damp
              case(2_2)
                 ipt%line(il)%straypol_par(1:ipt%ncomp)%damp= &
                      ipt%atm(1:ipt%ncomp)%par%gdamp
              case(3_2)
                 ipt%line(il)%straypol_par(1:ipt%ncomp)%damp= &
                      ipt%atm(1:ipt%ncomp)%par%gdamp
              case(4_2)
                 ipt%line(il)%straypol_par(1:ipt%ncomp)%damp= &
                      ipt%atm(1:ipt%ncomp)%par%gdamp
              case(5_2)
                 ipt%line(il)%straypol_par(1:ipt%ncomp)%damp= &
                      ipt%atm(1:ipt%ncomp)%par%gdamp
              case default
                 write (*,*) 'Undefined modeval: ',ipt%modeval
                 call stop
              end select
              ipt%line(il)%straypol_par(1:ipt%ncomp)%dopp= &
                   ipt%atm(1:ipt%ncomp)%par%dopp
              ipt%line%par%straypol_amp=ipt%straypol_amp
              ipt%line%par%straypol_eta0=ipt%straypol_amp
           end do
        end if

       call read_conv(ipt%wl_range,ipt%conv_func,ipt%conv_nwl,ipt%conv_mode, &
             ipt%conv_wljitter, &
             ipt%prefilter,obs%wl,obs%nwl,conv,ipt%verbose) 
        if (conv%doconv.ne.0) then
           compnwl=conv%nwl
           compwl(1:compnwl)=conv%wl(1:compnwl)
           cconvval(1:compnwl)=conv%val(1:compnwl)
           cret_wlidx=conv%iwl_compare
           cnret_wlidx=conv%nwl_compare
        else
           cconvval(1)=0
           cret_wlidx=conv%iwl_compare*0
           cnret_wlidx=-1
           compnwl=cnwl
           compwl(1:compnwl)=cwl(1:compnwl)
        end if
        !prefilter_wlerr set to 0.
        call read_prefilter(ipt%prefilter,0.0,&
             obs%wl,obs%nwl,prefilter,ipt%verbose)
        call fill_localstray(lsi,lsq,lsu,lsv,dols,obs%wl,obs%nwl)
        call compute_profile(ipt%atm,ipt%ncomp,ipt%line,ipt%nline, &
             obs%wl,obs%nwl,ipt%blend,ipt%nblend,ipt%gen, &
             lsi,lsq,lsu,lsv,dols,ipt%localstray_pol, &
             cconvval(1:compnwl),conv%doconv,conv%mode, &
             prefilter%val(1:obs%nwl),prefilter%doprefilter, &
             conv%iwl_compare,-1, &
             !             0.,0_2,0_2,-1_2, & !no convolution
             ipt%obs_par,1, &
             ipt%prof_shape,&
             ipt%magopt(1),ipt%use_geff(1),ipt%use_pb(1),ipt%pb_method(1), &
             ipt%modeval,0,luse, &
             !             iprof,qprof,uprof,vprof)
             obs%i,obs%q,obs%u,obs%v,1_2,ipt%old_norm,-1_2, &
             ipt%hanle_azi,ipt%norm_stokes_val,ipt%old_voigt)
        obs%ic=1.
        if (ipt%ic_level.ge.1e-5) obs%ic=ipt%ic_level
        ncnt=1
     else
        if ((datamode.eq.0).or.(datamode.eq.2)) then 
           call readsto(file,tobs,ok)
        else if (datamode.eq.3) then !hinode
           hfile=ipt%dir%profile(1:len_trim(ipt%dir%profile))//&
                ipt%observation(1:len_trim(ipt%observation))
           yindex=index(xyvec(i),'y')
           xv=str2int(xyvec(i)(2:yindex-1))
           yv=str2int(xyvec(i)(yindex+1:len(trim(xyvec(i)))))
           call read_hinode(hfile,xv,yv,tobs,lsprof,(i.eq.1),ok)
        else if (datamode.eq.4) then !imax
           hfile=ipt%dir%profile(1:len_trim(ipt%dir%profile))//&
                ipt%observation(1:len_trim(ipt%observation))
           yindex=index(xyvec(i),'y')
           xv=str2int(xyvec(i)(2:yindex-1))
           yv=str2int(xyvec(i)(yindex+1:len(trim(xyvec(i)))))
           call read_imax(hfile,xv,yv,tobs,lsprof,(i.eq.1),ok)
        else if (datamode.eq.5) then !4D Fits file
           hfile=ipt%dir%profile(1:len_trim(ipt%dir%profile))//&
                ipt%observation(1:len_trim(ipt%observation))
           yindex=index(xyvec(i),'y')
           xv=str2int(xyvec(i)(2:yindex-1))
           yv=str2int(xyvec(i)(yindex+1:len(trim(xyvec(i)))))
           call read_fits4d(hfile,xv,yv,tobs,lsprof,(i.eq.1),ok)
           times_ic=.true.
        else
           yindex=index(xyvec(i),'y')
           xv=str2int(xyvec(i)(2:yindex-1))
           yv=str2int(xyvec(i)(yindex+1:len(trim(xyvec(i)))))
           tmpobs(1)=tobs
           tmpic(1)=tic
           call read_tip(froot,xv,yv,yv,1,tmpobs(1),tmpic(1),lsprof,(i.eq.1),ok)
           tobs=tmpobs(1)
           tic=tmpic(1)
           times_ic=.true.
        end if
        if (ok) then 
           ncnt=ncnt+1
           if (ncnt.eq.1) then
              obs=tobs
              if (ipt%wl_off.gt.1e-5) then
                 if (ipt%verbose.ge.1) &
                      write(*,*) 'Using WL-Calibration from input file'
                 do j=1,obs%nwl
                    obs%wl(j)=(j-1)*ipt%wl_disp + ipt%wl_off
                 end do
              else
                 obs%wl(1:obs%nwl)=obs%wl(1:obs%nwl)+obs%wlref
                 if (ipt%verbose.ge.1) &
                      write(*,*) 'Using WL-Calibration from data file'
              end if
           else
              obs%ic=obs%ic+tobs%ic
              obs%i(1:obs%nwl)=obs%i(1:obs%nwl)+tobs%i(1:obs%nwl)
              obs%q(1:obs%nwl)=obs%q(1:obs%nwl)+tobs%q(1:obs%nwl)
              obs%u(1:obs%nwl)=obs%u(1:obs%nwl)+tobs%u(1:obs%nwl)
              obs%v(1:obs%nwl)=obs%v(1:obs%nwl)+tobs%v(1:obs%nwl)
           end if
        end if
     end if
  end do
  if (ncnt.gt.0) then
     error=.false.
     obs%ic=obs%ic/ncnt
     obs%i(1:obs%nwl)=obs%i(1:obs%nwl)/ncnt  !* obs%ic
     if (times_ic) obs%i(1:obs%nwl)=obs%i(1:obs%nwl) * obs%ic
     obs%q(1:obs%nwl)=obs%q(1:obs%nwl)/ncnt
     obs%u(1:obs%nwl)=obs%u(1:obs%nwl)/ncnt
     obs%v(1:obs%nwl)=obs%v(1:obs%nwl)/ncnt
     if (ipt%ic_level.ge.1e-5) then
        obs%i(1:obs%nwl)=obs%i(1:obs%nwl)*obs%ic/ipt%ic_level
        obs%q(1:obs%nwl)=obs%q(1:obs%nwl)*obs%ic/ipt%ic_level
        obs%u(1:obs%nwl)=obs%u(1:obs%nwl)*obs%ic/ipt%ic_level
        obs%v(1:obs%nwl)=obs%v(1:obs%nwl)*obs%ic/ipt%ic_level
        obs%ic=ipt%ic_level
     end if
  else
     error=.true.
     write(*,*) 'No profiles found: '//trim(xyvec(1))
  end if


  !add artificial noise: first read in prefilter info for obs-WL
  !(needed for noise increase caused by prefilter)
  call read_prefilter(ipt%prefilter,ipt%prefilter_wlerr,&
       obs%wl,obs%nwl,prefilter,ipt%verbose)
  call add_noise(obs,prefilter%doprefilter,prefilter%val)

  !remove spikes
  if (ipt%spike.ge.1) then
    first=1
     call remove_spike(obs%i,obs%nwl,ipt%spike,ipt%verbose,'I',first)
     call remove_spike(obs%q,obs%nwl,ipt%spike,ipt%verbose,'Q',first)
     call remove_spike(obs%u,obs%nwl,ipt%spike,ipt%verbose,'U',first)
     call remove_spike(obs%v,obs%nwl,ipt%spike,ipt%verbose,'V',first)
  end if

  !median profiles
  if (ipt%median.ge.2) then
     call median(obs%i,obs%nwl,ipt%median)
     call median(obs%q,obs%nwl,ipt%median)
     call median(obs%u,obs%nwl,ipt%median)
     call median(obs%v,obs%nwl,ipt%median)
  end if

  !smooth profiles
  if (ipt%smooth.gt.1) then
     select case (ipt%filter_mode)
     case(0_2)
        call smooth(obs%i,obs%nwl,ipt%smooth)
        call smooth(obs%q,obs%nwl,ipt%smooth)
        call smooth(obs%u,obs%nwl,ipt%smooth)
        call smooth(obs%v,obs%nwl,ipt%smooth)
     case default
        write(*,*) "Smoothing type ",ipt%filter_mode, &
             " not yet implemented."
        write(*,*) "Continue with unsmoothed data"
     end select
  end if

  !WL-binning
  if ((.not.error).and.(ipt%wl_bin.gt.1)) then
     if (ipt%wl_bin.ge.obs%nwl) then
        write(*,*) 'Error in WL-binning, bin-size: ',ipt%wl_bin
        error=.true.
     else
        iw=0
        bobs=obs
        bobs%wl=0.
        bobs%i=0.
        bobs%q=0.
        bobs%u=0.
        bobs%v=0.
        do i=1,obs%nwl,ipt%wl_bin
           iw=iw+1
           j=i+ipt%wl_bin-1
           if (j.gt.obs%nwl) j=obs%nwl
           bobs%wl(iw)=sum(obs%wl(i:j))/(j-i+1)
           bobs%i(iw)=sum(obs%i(i:j))/(j-i+1)
           bobs%q(iw)=sum(obs%q(i:j))/(j-i+1)
           bobs%u(iw)=sum(obs%u(i:j))/(j-i+1)
           bobs%v(iw)=sum(obs%v(i:j))/(j-i+1)
        end do
        bobs%nwl=iw
        obs=bobs
     end if
  end if

  if(.not.error) then 
     !set wl-range if not set in input file
     if (sum(ipt%wl_range).le.1e-5) then
        !     if (ipt%wl_range(1).lt.minval(obs%wl(1:obs%nwl))) &
        ipt%wl_range(1)=minval(obs%wl(1:obs%nwl))
        !     if (ipt%wl_range(2).gt.maxval(obs%wl(1:obs%nwl))) &
        ipt%wl_range(2)=maxval(obs%wl(1:obs%nwl))
     else !if wl_range is set, then restrict observation to this range
        iwin(1)=1
        iwin(2)=obs%nwl
        do i=1,obs%nwl 
           if (obs%wl(i).lt.ipt%wl_range(1)) iwin(1)=i+1
           if (obs%wl(i).le.ipt%wl_range(2)) iwin(2)=i
        end do
        if (iwin(2)-iwin(1).le.1) then
           error=.true.
           write(*,*) 'Invalid WL-range: ',ipt%wl_range
        else
           obs%nwl=iwin(2)-iwin(1)+1
           obs%wl(1:obs%nwl)=obs%wl(iwin(1):iwin(2))
           obs%i(1:obs%nwl)=obs%i(iwin(1):iwin(2))
           obs%q(1:obs%nwl)=obs%q(iwin(1):iwin(2))
           obs%u(1:obs%nwl)=obs%u(iwin(1):iwin(2))
           obs%v(1:obs%nwl)=obs%v(iwin(1):iwin(2))
           obs%wl(obs%nwl+1:)=0.
        end if
     end if

  end if
end subroutine get_profile
