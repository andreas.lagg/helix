#include <stdio.h>
typedef struct {
  unsigned char zero;
  float two;
  double three;
  short four[2];
  struct tag_2 {
    float s1;
    int i1;
  } sub;
} ASTRUCTURE;

int test1(float *f, int *n, ASTRUCTURE *ast)
{

  ast->zero++;
  ast->four[0]++;
  ast->four[0]++;
  ast->sub.i1++;
 *f *=*f;
 *n *=*n;

  return(1);
}

/*
Compile:
gcc  -I/opt/rsi/idl/external/ -c -o example_c.o example.c ; /usr/bin/ld -shared -o example_c.so example_c.o

IDL:

  sub={s1:0.,i1:0}
 s = {zero:0B,two:0.,three:0D,four: intarr(2),sub:sub}
 
 f=5. & n=5
  print, CALL_EXTERNAL('./fortran/example_c.so', 'test1', f,n,s, $
                       /unload,/auto_glue)
  help,/st,s
  print,s.four
  help,/st,s.sub
  print,f,n

*/
