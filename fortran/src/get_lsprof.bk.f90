!Mail of G. Scharmer, June 8, 2008
!2) Local stray light correction. I am GUESSING the following: 

!If this is to work, the local stray light representation must EXCLUDE
!pixels corresponding to the core of the psf and INCLUDE only a
!representation corresponding to the WING of the psf, else this becomes
!a very ill-conditioned inversion problem. By including only the WING
!of the psf, you mimic the effects of residual high-order aberrations
!neither compensated by the AO system nor by the MOMFBD
!processing. Such a psf has a diffraction limited core but with lower
!"weight" plus extended wings lowering the contrast (and giving
!straylight) but leaving the FWHM of the psf (the spatial resolution)
!intact. The trick here is to avoid deconvolution but to keep the
!straylight correction.
!
!To make local straylight correction work, I would therefore define an
!appropriate "straylight" psf that ONLY includes pixels outside the
!main core of the psf. To make that distinct, I would define the
!location of the first minimum in the (theroretical) diffraction
!pattern as the separation betwen the core and the wing. This
!corresponds to seeting all straylight psf pixels within a RADIUS of
!1.22 Lambda/D (diameter nearly 2.5 lambda/D) to zero. Some sort of
!appropriate form for the wing psf is then needed. In fact, AO theory
!may provide a good approximate form of this but I cannot check that at
!home -- all my AO literature is at work.
!
!So the model would then correspond to a psf that is partly a delta
!function (representing the entire core of the psf) plus a "wing" part,
!the straylight psf that "we" want to implement.
!
!Of course, all Stokes parameters must be included in this straylight
!correction in the same way. I see no justification for correcting
!Stokes I only.


subroutine ls_weight(x,y,widthx,widthy,wgt)
  real(4) :: x,y,widthx,widthy,wgt,D

  wgt=exp(-(x)**2./(widthx/2.)**2.) * exp(-(y)**2./(widthy/2.)**2.)

end subroutine ls_weight

subroutine get_lsprof(fcc,xpvec,ypvec,wlvec,nwl,mode,lsprof)
  use all_type
  use ipt_decl
  use tools
  use ftime
  implicit none
#ifdef MPI
  include 'mpif.h'
#endif
  type (proftyp) :: lsprof,dummy,tprof
  integer(2) :: scan,nhinode,hinodexpos(4096),hinodescannr(4096),xerr
  character(LEN=maxstr) :: fcc,fccx,datnam,hinodenam(4096),hfile,hdnam,hf
  integer(2) :: slitindx,spbshft,xpvec,ypvec,nwl,mode,dx,dy,xnew(2),ynew(2), &
       nls,ix,iy,inidx,data_error,ny,i,iiy
  real(4) :: totwgt,wgt,rad,tic
  logical :: error,ok,first
  integer(4) :: naxes(3)
  real(4) :: icimg(:,:)
  allocatable icimg
  real(8) :: wref,delw,wl_vec(maxwl),wlvec(maxwl)
  common /hincomm/ hfile,nhinode,hinodenam,hinodexpos,hinodescannr,datnam

  real(8) :: tcpu_start,tcpu_stop,treal_start,treal_stop,ttodo,dt



  treal_start=realtime()


  lsprof%wl=wlvec
  lsprof%nwl=nwl
  lsprof%i=0
  lsprof%q=0
  lsprof%u=0
  lsprof%v=0
  if (ipt%localstray_rad.ge.1e-5) then
     if (ipt%verbose.ge.2) write(*,*) 'Retrieving local straylight profile ...'

     xnew=ipt%stepx*ipt%localstray_rad*(/-1.,1./)*sqrt(2.)+xpvec
     ynew=ipt%stepy*ipt%localstray_rad*(/-1.,1./)*sqrt(2.)+ypvec
     totwgt=0.
     nls=0
     ny=ynew(2)-ynew(1)+1
     first=.true.
     do ix=xnew(1),xnew(2)
        do iy=ynew(1),ynew(2)
           iiy=iy-ynew(1)+1
           rad=sqrt(((real(ix,kind=4)-xpvec)/ipt%stepx)**2.+&
                ((real(iy,kind=4)-ypvec)/ipt%stepy)**2.)
           if ((rad.le.ipt.localstray_rad).and.&
                (rad.ge.ipt.localstray_core)) then
              call read_tip(fcc,ix,iy,tprof,tic,dummy,.false.,ok)
              data_error=1
              select case (mode)
              case(1) !TIP
                 if (ok) then
!                   tprof%i=tprof%i
!                   tprof%q=tprof%q
!                   tprof%u=tprof%u
!                   tprof%v=tprof%v
                    tprof%ic=tic
                    data_error=0
                 else
                    data_error=1
                 end if
              case(2) !Hinode
                 inidx=-1
                 do i=1,nhinode
                    if ((hinodescannr(i).eq.ipt%obs_par%hin_scannr).and.&
                         (hinodexpos(i).eq.ix)) inidx=i
                 end do
                 if (inidx.eq.-1) then
                    data_error=1
                 else
                    hf=hinodenam(inidx)
                    call readfits_hinode(hf,iy,tprof,naxes,error,0_2)
                    if (first) then
                       fccx=trim(hf)//'.ccx'
                       allocate(icimg(1,naxes(2)))
                       call read_ccx(fccx,nwl,1,naxes(2), &
                            delw,wref,wl_vec,icimg,xerr,.true.)
                       first=.false.
                    end if
                    tprof%ic=icimg(1,iy+1)
                    tprof%i=tprof%i/tprof%ic
                    tprof%q=tprof%q/tprof%ic
                    tprof%u=tprof%u/tprof%ic
                    tprof%v=tprof%v/tprof%ic
                    data_error=0
                 end if
              end select
              if (data_error.eq.0) then
                 call ls_weight(real(ix,kind=4)-xpvec,real(iy,kind=4)-ypvec,&
                      ipt%stepx*ipt%localstray_rad, &
                      ipt%stepy*ipt%localstray_rad,wgt)
                 totwgt=totwgt+wgt
                 lsprof%ic=lsprof%ic+tprof%ic*wgt
                 lsprof%i(1:nwl)=lsprof%i(1:nwl)+tprof%i(1:nwl)*wgt
                 lsprof%q(1:nwl)=lsprof%q(1:nwl)+tprof%q(1:nwl)*wgt
                 lsprof%u(1:nwl)=lsprof%u(1:nwl)+tprof%u(1:nwl)*wgt
                 lsprof%v(1:nwl)=lsprof%v(1:nwl)+tprof%v(1:nwl)*wgt
                 nls=nls+1
              end if
           end if
        end do
        if (mode.eq.2) deallocate(icimg)
     end do
     if (totwgt.gt.1e-5) then
        lsprof%ic=lsprof%ic/totwgt
        lsprof%i=lsprof%i/totwgt
        lsprof%q=lsprof%q/totwgt
        lsprof%u=lsprof%u/totwgt
        lsprof%v=lsprof%v/totwgt
        if (ipt%verbose.ge.2) &
             write(*,*) 'LOCALSTRAY profile computed from ',nls,' profiles.'
     else
        write(*,*) 'No profile found for LOCALSTRAY'
     end if
  end if

  write(*,'(a,6f8.4)') 'GETLSPROF: ',realtime()-treal_start,lsprof%i(1:5)
  !    write(*,*) 'LSPROF',lsprof%ic,lsprof%i(1:5)

end subroutine get_lsprof
