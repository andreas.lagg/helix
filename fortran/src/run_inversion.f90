
subroutine newscale(par,fit,mi,scl,im)
  use all_type
  implicit none
  integer(2) :: fit
  real(4) :: par
  type(misub) :: mi
  type(mmtyp) :: scl
  integer(2) :: im
  real(4) :: scrg

  fit=mi%fit(im-1)
  scrg=mi%perc_rg(im-1)/100.*(scl%max-scl%min)
  if ((par-scrg).gt.scl%min) scl%min=par-scrg
  if ((par+scrg).lt.scl%max) scl%max=par+scrg
end subroutine newscale

subroutine check_diffscl(atm1,atm2,scl1,scl2)
  use all_type
  use ipt_decl
  implicit none
  integer(2) :: atm1,atm2
  type(mmtyp) :: scl1,scl2
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt

  if ((atm1.eq.atm2).and.(atm1.ne.0).and.(atm2.ne.0)) then
     if ((scl1%min.ne.scl2%min).or.(scl1%max.ne.scl2%max)) then
        if (ipt%verbose.ge.1) then
             write(*,*) 'Scaling of coupled param. is different! '//&
             'Use scaling of 1st comp.'
             write(*,*) scl1%min,scl1%max,' -> ',scl2%min,scl2%max
          end if
        scl1=scl2
     end if
  end if
end subroutine check_diffscl

subroutine run_inversion(obs,atm_result,blend_result,gen_result,line_result, &
     fitprof,scl,method,ftns, &
     statdim,statfit,statpar,statifit,statqfit,statufit,statvfit)
  use all_type
  use ipt_decl
  use localstray_com
  use localstraycomp_com
  use piktools
  use pikvar
#ifdef X11
  use dislin
#endif
  implicit none
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  type (scltyp) :: scl(maxatm)
  type (proftyp) :: obs,wgt,weight,wgtarr(1),fitprof,plotprof(maxplotprof)
  type (atmtyp),dimension(maxatm) :: atm,atm_result
  type (blendtyp),dimension(maxblend) :: blend,blend_result
  type (gentyp) :: gen,gen_result
  type (linetyp),dimension(maxlin) :: lin,line_result
  type (prefiltertyp) :: prefilter
!  type(convtyp) :: conv
  integer(2) :: im1,im,ia,il,il1,nmi,nwl_compare,iwl_compare(maxwl),i0, &
       rnwl,rwl(maxwl),nfreepar
  integer(2) :: straypol_run,iprof_only,cmax,he1,he2,he3,method(maxmi)
  integer(2),dimension(act_ncomp,npartot) :: fit_atm
  character(idlen) :: astr(maxlin)
  real(4) :: ftns,tot_weight,wgt_sum
  real(4) :: statpar(pik_NMAX,pik_PMAX),statfit(pik_PMAX)
  integer(2) :: statdim(2)
  real(4) :: wg2(maxwl),quvn(maxwl),cvec(maxwl),sumquv
  integer(4) :: seed,imax(1),ip,ii
  real(4),dimension(maxwl) :: fpi,fpq,fpu,fpv
  real(4), dimension(maxwl,pik_PMAX) :: statifit,statqfit,statufit,statvfit
  integer(2) :: luse(act_nline,act_ncomp)
  logical :: tuse
  character(2) :: imstr
  logical :: wopen
  character(maxstr) :: plot_title
  integer(4) :: cnt,cntrate,cntmax
!  real(4) :: fitness
!  external fitness

  common /win/ wopen

  atm=ipt%atm
  atm_result=atm
  blend=ipt%blend
  blend_result=blend
  gen=ipt%gen
  gen_result=gen
  lin=ipt%line
  line_result=lin
  fitprof=obs

  !calculate array use: determines if
  !line / atmosphere is used for calculations
  !flag if a atmosphere should be used for a line
  luse=0
  do ia=1,ipt%ncomp 
     do il=1,ipt%nline
        do il1=1,ipt%nline
           if (sum(int(lin(il)%id)).gt.0) then 
           tuse=(count((/(atm(ia)%use_line(:,il1).eq.lin(il)%id)/)).eq.idlen)
           if (tuse.or.(luse(il,ia).eq.1)) luse(il,ia)=1_2
!           tuse=minval(abs(1*int(atm(ia)%use_line(:,il1).eq.lin(il)%id)))
!           luse(il,ia)=1*abs(int((luse(il,ia).eq.1).or.(tuse.eq.1)))
        end if
        end do
     end do
  end do

  !reset straypol parameters - start with 0 for fit
  do il=1,ipt%nline
     lin(il)%par%straypol_amp=0.
     lin(il)%par%straypol_eta0=0.
  end do


  nmi=ipt%nmi
  if (nmi.eq.0) nmi=1

  !loop for multi iterations
  do im1=1,nmi


     scl=ipt%scale !reread scaling (scaling is changed for straypol-run)
     if (ipt%verbose.ge.1) write(*,*) 'Multi Iteration #',im1

     !for straypol-correction: Do Gaussfit
     !to I only before starting iterations
     !start with -1: gauss fit to I only
     straypol_run=0
     iprof_only=0
     if (ipt%straypol_corr.gt.0) then !set straypol parameters
        if (im1.eq.1) then !first run is straypol run
           im=ipt%nmi
           iprof_only=1
           straypol_run=1
           !           do ia=1,ipt%ncomp !npartot parameters
           !              atm(ia)%fit=fittyp(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
           !           end do
           atm%fit%b=0   ; atm%par%b=0
           atm%fit%inc=0 ; atm%par%inc=0
           atm%fit%azi=0 ; atm%par%azi=0

           !           atm%fit%vlos=1

           do il=1,ipt%nline

              if (lin(il)%straypol_use.eq.1) then
                 !use same range & fit settings as for
                 !first multi-iteration run. This is
                 !defined in read_ipt
                 select case (ipt%modeval)
                 case (0_2) !GAUSS
                    lin(il)%fit%straypol_amp=1
                 case (1_2) !VOIGT
                    lin(il)%fit%straypol_eta0=2
                 case (2_2) !VOIGT_PHYS
                    write(*,*) 'voigt-physical mode not implemented for'//&
                         ' straylight-correction.'
                    stop
                 case (3_2) !VOIGT-GDAMP
                    lin(il)%fit%straypol_eta0=2
                 case (4_2) !VOIGT-SZERO
                    lin(il)%fit%straypol_eta0=2
                 case (5_2) !
                    write(*,*) 'No straylight correction for hanle-slab mode.'
                    stop
                 case default
                    write(*,*) 'unknown mode for straylight-correction:',ipt%modeval
                    stop              
                 end select
              end if
           end do

           !get max value of coupling
           call fit2arr(ipt%atm,ipt%ncomp,fit_atm)
           cmax=maxval(abs(fit_atm))
           !couple the amplitude of all
           !components and use the filling factor
           !to scale the gauss amplitude
           !but: according to Landi / Trujillo-Bueno: hanle signal does not 
           !couple via f, the relation is much more complex. Therefore: couple 
           !only h2 and he3 lines, since they are lying on top of each other, 
           !a separate fit would not make any sense there
           !But: Couple all components of one line to reduce no of free
           !parameters
           !           do il=1,ipt%nline
           !              select case (ipt%modeval)
           !              case (0_2) !GAUSS
           !                 lin(il)%fit(1:ipt%ncomp)%straypol_amp=-cmax-1
           !              case (1_2) !VOIGT
           !                 lin(il)%fit(1:ipt%ncomp)%straypol_eta0=-cmax-1
           !              end select
           !              cmax=cmax+1
           !           end do

           !couple 2 blended he-lines
           he3=0 ; he2=0 ; he1=0
           do il=1,ipt%nline
              write(unit=astr(il),fmt='(16a)') char(lin(il)%id)
              if (astr(il)(1:12).eq.'He1 10830.34') he3=il
              if (astr(il)(1:12).eq.'He1 10830.25') he2=il
              if (astr(il)(1:12).eq.'He1 10829.09') he1=il
           end do
           if ((he3.ne.0).and.(he2.ne.0)) then 
              !              lin(he3)%fit%straypol_amp=lin(he2)%fit%straypol_amp
              !              lin(he3)%fit%straypol_eta0=lin(he2)%fit%straypol_eta0
              select case (ipt%modeval)
              case (0_2) !GAUSS
                 lin(he1)%fit%straypol_amp=-cmax-1
                 lin(he2)%fit%straypol_amp=-cmax-2
                 lin(he3)%fit%straypol_amp=-cmax-2
              case (1_2) !VOIGT
                 lin(he1)%fit%straypol_eta0=-cmax-1
                 lin(he2)%fit%straypol_eta0=-cmax-2
                 lin(he3)%fit%straypol_eta0=-cmax-2
              case (3_2) !VOIGT-GDAMP
                 lin(he1)%fit%straypol_eta0=-cmax-1
                 lin(he2)%fit%straypol_eta0=-cmax-2
                 lin(he3)%fit%straypol_eta0=-cmax-2
              case (4_2) !VOIGT-SZERO
                 lin(he1)%fit%straypol_eta0=-cmax-1
                 lin(he2)%fit%straypol_eta0=-cmax-2
                 lin(he3)%fit%straypol_eta0=-cmax-2
              case (5_2) !hanle-slab
                 write(*,*) 'hanle-slab mode no straylight correction'
                 stop
              case default
                 write(*,*) 'unknown mode for straylight-correction:',ipt%modeval
                 stop              
              end select
              cmax=cmax+2
           end if
        else  !non straypol-runs
           if (im1.eq.2) atm=ipt%atm
           im=im1-1
        end if
     else
        im=im1
     end if !straypol parameters

     !define weighting
    call get_wgt(wgt,obs,im)
     weight=wgt
!     weight%i=weight%i*ipt%iquv_weight(1,im)
!     weight%q=weight%q*ipt%iquv_weight(2,im)
!     weight%u=weight%u*ipt%iquv_weight(3,im)
!     weight%v=weight%v*ipt%iquv_weight(4,im)

     if ((im.gt.1).and.(straypol_run.eq.0)) then
        !define new input atmosphere and set new
        !inversion parameters
        atm=atm_result   !set new initial values

        !set new fit flag & new scaling
        do ia=1,ipt%ncomp
           call newscale(atm(ia)%par%b,atm(ia)%fit%b, &
                ipt%mi(ia)%b,scl(ia)%b,im)
           call newscale(atm(ia)%par%azi,atm(ia)%fit%azi, &
                ipt%mi(ia)%azi,scl(ia)%azi,im)
           call newscale(atm(ia)%par%inc,atm(ia)%fit%inc, &
                ipt%mi(ia)%inc,scl(ia)%inc,im)
           call newscale(atm(ia)%par%vlos,atm(ia)%fit%vlos, &
                ipt%mi(ia)%vlos,scl(ia)%vlos,im)
           call newscale(atm(ia)%par%width,atm(ia)%fit%width, &
                ipt%mi(ia)%width,scl(ia)%width,im)
           call newscale(atm(ia)%par%damp,atm(ia)%fit%damp, &
                ipt%mi(ia)%damp,scl(ia)%damp,im)
           call newscale(atm(ia)%par%dopp,atm(ia)%fit%dopp, &
                ipt%mi(ia)%dopp,scl(ia)%dopp,im)
           call newscale(atm(ia)%par%a0,atm(ia)%fit%a0, &
                ipt%mi(ia)%a0,scl(ia)%a0,im)
           call newscale(atm(ia)%par%szero,atm(ia)%fit%szero, &
                ipt%mi(ia)%szero,scl(ia)%szero,im)
           call newscale(atm(ia)%par%sgrad,atm(ia)%fit%sgrad, &
                ipt%mi(ia)%sgrad,scl(ia)%sgrad,im)
           call newscale(atm(ia)%par%etazero,atm(ia)%fit%etazero, &
                ipt%mi(ia)%etazero,scl(ia)%etazero,im)
           call newscale(atm(ia)%par%gdamp,atm(ia)%fit%gdamp, &
                ipt%mi(ia)%gdamp,scl(ia)%gdamp,im)
           call newscale(atm(ia)%par%vmici,atm(ia)%fit%vmici, &
                ipt%mi(ia)%vmici,scl(ia)%vmici,im)
           call newscale(atm(ia)%par%densp,atm(ia)%fit%densp, &
                ipt%mi(ia)%densp,scl(ia)%densp,im)
           call newscale(atm(ia)%par%tempe,atm(ia)%fit%tempe, &
                ipt%mi(ia)%tempe,scl(ia)%tempe,im)
           call newscale(atm(ia)%par%dslab,atm(ia)%fit%dslab, &
                ipt%mi(ia)%dslab,scl(ia)%dslab,im)
           call newscale(atm(ia)%par%height,atm(ia)%fit%height, &
                ipt%mi(ia)%height,scl(ia)%height,im)
           call newscale(atm(ia)%par%ff,atm(ia)%fit%ff, &
                ipt%mi(ia)%ff,scl(ia)%ff,im)
        end do
     end if

                                !check if magnetic signal is below MIN_QUV
     if (ipt%min_quv.ge.1e-6) then
        wg2=(wgt%v**2+wgt%u**2+wgt%q**2)
        quvn=0.
        cvec=0
        where (wg2.gt.0) cvec=1
        quvn=sqrt((obs%q*cvec-sum(obs%q*cvec)/sum(cvec))**2 + &
             (obs%u*cvec-sum(obs%u*cvec)/sum(cvec))**2 + &
             (obs%v*cvec-sum(obs%v*cvec)/sum(cvec))**2)
        !take average of 4 highest values
        sumquv=0.
        do i0=1,4
           imax=maxloc(quvn)
           sumquv=sumquv+quvn(imax(1))
           quvn(imax)=0.
        end do
        sumquv=sumquv/4.
        if (ipt%verbose.ge.2) write(*,*) 'QUV-Strength: ',sumquv
        if (sumquv.lt.ipt%min_quv) then
           atm%fit%b=0   ; atm%par%b=0
           atm%fit%inc=0 ; atm%par%inc=0
           atm%fit%azi=0 ; atm%par%azi=0
           if (ipt%verbose.ge.1) write(*,'(a,e10.3,a)') 'QUV-Strength (', &
                sumquv,') is less than MIN_QUV. Fit is done with B=0.'
        end if
     end if


     if ((ipt%approx_dir(im).eq.1).or.(ipt%approx_azi(im).eq.1)) then 
        write(*,*) 'Angle Approximation not implemented in Fortran version.'
        stop
     end if

     !fill idx-variable with indices for pikaia
     !par-variable
     call get_pikaia_idx(atm,lin,blend,gen,atm,lin,blend,gen,ipt%verbose)


     !check if scaling for coupled
     !parameters is equal
     do ia=2,ipt%ncomp 
        call check_diffscl(atm(ia)%idx%b,atm(ia-1)%idx%b, &
             scl(ia)%b,scl(ia-1)%b)
        call check_diffscl(atm(ia)%idx%azi,atm(ia-1)%idx%azi, &
             scl(ia)%azi,scl(ia-1)%azi)
        call check_diffscl(atm(ia)%idx%inc,atm(ia-1)%idx%inc, &
             scl(ia)%inc,scl(ia-1)%inc)
        call check_diffscl(atm(ia)%idx%vlos,atm(ia-1)%idx%vlos, &
             scl(ia)%vlos,scl(ia-1)%vlos)
        call check_diffscl(atm(ia)%idx%width,atm(ia-1)%idx%width, &
             scl(ia)%width,scl(ia-1)%width)
        call check_diffscl(atm(ia)%idx%damp,atm(ia-1)%idx%damp, &
             scl(ia)%damp,scl(ia-1)%damp)
        call check_diffscl(atm(ia)%idx%dopp,atm(ia-1)%idx%dopp, &
             scl(ia)%dopp,scl(ia-1)%dopp)
        call check_diffscl(atm(ia)%idx%a0,atm(ia-1)%idx%a0, &
             scl(ia)%a0,scl(ia-1)%a0)
        call check_diffscl(atm(ia)%idx%szero,atm(ia-1)%idx%szero, &
             scl(ia)%szero,scl(ia-1)%szero)
        call check_diffscl(atm(ia)%idx%sgrad,atm(ia-1)%idx%sgrad, &
             scl(ia)%sgrad,scl(ia-1)%sgrad)
        call check_diffscl(atm(ia)%idx%etazero,atm(ia-1)%idx%etazero, &
             scl(ia)%etazero,scl(ia-1)%etazero)
        call check_diffscl(atm(ia)%idx%gdamp,atm(ia-1)%idx%gdamp, &
             scl(ia)%gdamp,scl(ia-1)%gdamp)
        call check_diffscl(atm(ia)%idx%vmici,atm(ia-1)%idx%vmici, &
             scl(ia)%vmici,scl(ia-1)%vmici)
        call check_diffscl(atm(ia)%idx%densp,atm(ia-1)%idx%densp, &
             scl(ia)%densp,scl(ia-1)%densp)
        call check_diffscl(atm(ia)%idx%tempe,atm(ia-1)%idx%tempe, &
             scl(ia)%tempe,scl(ia-1)%tempe)
        call check_diffscl(atm(ia)%idx%dslab,atm(ia-1)%idx%dslab, &
             scl(ia)%dslab,scl(ia-1)%dslab)
        call check_diffscl(atm(ia)%idx%height,atm(ia-1)%idx%height, &
             scl(ia)%height,scl(ia-1)%height)
        call check_diffscl(atm(ia)%idx%ff,atm(ia-1)%idx%ff, &
             scl(ia)%ff,scl(ia-1)%ff)
     end do

     !normalize weight
     !(not done)
     if (1.eq.0) then !do not normalize weight, use function
        ! as defined in wgt-file
        tot_weight=100.
        wgt_sum=sum(weight%i(1:weight%nwl)+weight%q(1:weight%nwl)+ &
             weight%u(1:weight%nwl)+weight%v(1:weight%nwl))
        if (wgt_sum.lt.1e-8) wgt_sum=1.
        weight%i=weight%i*tot_weight/wgt_sum
        weight%q=weight%q*tot_weight/wgt_sum
        weight%u=weight%u*tot_weight/wgt_sum
        weight%v=weight%v*tot_weight/wgt_sum
     end if


     !call pikaia
     ftns=0.
     call system_clock(cnt,cntrate,cntmax)
     seed=int(cnt)
     fitprof=obs
     nfreepar=0
     nwl_compare=-1

     !call call_pikaia with restricted WL range
     call fill_localstray(lsi,lsq,lsu,lsv,dols,obs%wl(1:obs%nwl),obs%nwl)
     call call_pikaia(lin,ipt%nline,atm,ipt%ncomp,obs%wl,obs%nwl, &
          blend,ipt%nblend,gen, &
          lsi,lsq,lsu,lsv,dols,ipt%localstray_pol, &
          ipt%wl_range,ipt%conv_func,ipt%conv_nwl,&
          ipt%conv_mode,ipt%conv_wljitter, &
          ipt%prefilter,ipt%prefilter_wlerr, &
          ipt%obs_par, &
          obs%i,obs%q,obs%u,obs%v, &
          weight%i,weight%q,weight%u,weight%v, &
          scl, &
          ipt%prof_shape,ipt%magopt(im),ipt%old_norm,&
          ipt%use_geff(im),ipt%use_pb(im), &
          ipt%pb_method(im),ipt%modeval,iprof_only, &
          ipt%verbose,method(im),ipt%ncalls(im),ipt%pikaia_pop, &
          luse, &
          atm_result,blend_result,gen_result,ftns,seed, &
          fitprof%i,fitprof%q,fitprof%u,fitprof%v,fitprof%wl,fitprof%nwl, &
          iwl_compare,nwl_compare,ipt%conv_output,&
          ipt%hanle_azi,ipt%norm_stokes_val,nfreepar, &
          ipt%chi2mode,ipt%old_voigt, &
          statdim,statfit,statpar,statifit,statqfit,statufit,statvfit)
!     if ((ipt%ncalls(im).eq.0).and.(ipt%synth.eq.1)) then
!        fitprof=obs
!     end if

     !straypol parameters to result atmosphere
     if (straypol_run.eq.1) then
        do il=1,ipt%nline
           lin(il)%straypol_par(1:ipt%ncomp)%vlos=atm_result%par%vlos
           lin(il)%straypol_par(1:ipt%ncomp)%width=atm_result%par%width
           lin(il)%straypol_par(1:ipt%ncomp)%dopp=atm_result%par%dopp
           select case (ipt%modeval)
           case (1_2)
              lin(il)%straypol_par(1:ipt%ncomp)%damp=atm_result%par%damp
           case (2_2)
              lin(il)%straypol_par(1:ipt%ncomp)%damp=atm_result%par%gdamp
           case (3_2)
              lin(il)%straypol_par(1:ipt%ncomp)%damp=atm_result%par%gdamp
           case (4_2)
              lin(il)%straypol_par(1:ipt%ncomp)%damp=atm_result%par%gdamp
           case (5_2)
              lin(il)%straypol_par(1:ipt%ncomp)%damp=atm_result%par%gdamp
           case default
           end select
        end do
     end if
     line_result=lin

     !get final weighting function (for fitness calculation)
     if (im1.eq.ipt%nmi) then
        call get_wgt(weight,obs,im)
        if (nwl_compare.ge.1) then 
           fpi(1:nwl_compare)=fitprof%i(iwl_compare(1:nwl_compare))
           fpq(1:nwl_compare)=fitprof%q(iwl_compare(1:nwl_compare))
           fpu(1:nwl_compare)=fitprof%u(iwl_compare(1:nwl_compare))
           fpv(1:nwl_compare)=fitprof%v(iwl_compare(1:nwl_compare))
        else
           fpi=fitprof%i
           fpq=fitprof%q
           fpu=fitprof%u
           fpv=fitprof%v
        end if
        ftns=fitness(obs%nwl,fpi,fpq,fpu,fpv, &
             obs%i,obs%q,obs%u,obs%v,weight%i,weight%q,weight%u,weight%v, &
             lin(1)%icont)
     end if


     ! do plotting if verbosity>=2 or last plot of multi-iter if verbosity>=1
        plotprof(1)=obs
        plotprof(1)%name='OBS'
!        plotprof(2)=fitprof
        plotprof(2)=fitprof
        plotprof(2)%name='FIT'
        ip=2
     if ((ipt%display_profile.ge.1).and.&
          ((ipt%verbose.ge.2).or.&
          ((ipt%verbose.ge.0).and.(im1.eq.ipt%nmi)))) then
        write(unit=imstr,fmt='(i2)') im1
        plot_title='Multi Iteration Step '//imstr    
        if ((ipt%display_comp.eq.1).and.(ipt%ncomp.ge.2)) then
           !calculate individual component plots
           
           !prefilter_wlerr set to 0.
           call read_prefilter(ipt%prefilter,0.0,fitprof%wl, &
                fitprof%nwl,prefilter,ipt%verbose)
           rnwl=cnret_wlidx
           rwl=cret_wlidx
!           compwl=obs%wl
!           compnwl=obs%nwl
           do ia=1,ipt%ncomp
              ip=ip+1
              write(unit=imstr,fmt='(i2.2)') ia
              plotprof(ip)%name='COMP '//imstr

              call compute_profile(atm_result(1:ipt%ncomp),ipt%ncomp, &
                   cline(1:cnline),cnline, &
                   compwl(1:compnwl),compnwl, &
                   blend_result(1:ipt%nblend),ipt%nblend,gen_result, &
                   lsi,lsq,lsu,lsv,dols,ipt%localstray_pol, &
                   cconvval(1:compnwl),doconv,ipt%conv_mode, &
                   prefilter%val(1:fitprof%nwl),prefilter%doprefilter, &
!                   cret_wlidx,cnret_wlidx,cobs_par, &
                   rwl,rnwl,cobs_par, &
                   0,cvoigt,cmagopt,cuse_geff,cuse_pb,cpb_method, &
                   cmodeval,ciprof_only,cuse(1:cnline,1:ipt%ncomp),&
                   plotprof(ip)%i,plotprof(ip)%q, &
                   plotprof(ip)%u,plotprof(ip)%v,0_2,cold_norm,ia, &
                   chanle_azi,cnorm_stokes_val,cold_voigt)
              plotprof(ip)%wl=obs%wl
              plotprof(ip)%nwl=obs%nwl
           end do
        end if
        if ((obs%nwl.le.4).and.(doconv.eq.0).and.(dols.eq.0)) then 
           ip=ip+1
           plotprof(ip)%nwl=256
           plotprof(ip)%name='FIT256'
           do ii=1,plotprof(ip)%nwl
              plotprof(ip)%wl(ii)=real((ii-1),kind=8)/(plotprof(ip)%nwl-1)* &
                   (ipt%wl_range(2)-ipt%wl_range(1))+ipt%wl_range(1)
           end do
           compnwl=plotprof(ip)%nwl
           !prefilter_wlerr set to 0.
           call read_prefilter(ipt%prefilter,0.0,&
                plotprof(ip)%wl,plotprof(ip)%nwl,prefilter,ipt%verbose)
!           call fill_localstray(lsi,lsq,lsu,lsv,dols, &
!                plotprof(ip)%wl(1:plotprof(ip)%nwl),plotprof(ip)%nwl)
           call compute_profile(atm_result(1:ipt%ncomp),ipt%ncomp, &
                cline(1:cnline),cnline, &
                plotprof(ip)%wl,plotprof(ip)%nwl, &
                blend_result(1:ipt%nblend),ipt%nblend,gen_result, &
                lsi,lsq,lsu,lsv,dols,ipt%localstray_pol, &
                cconvval(1:compnwl),0_2,ipt%conv_mode,  &
                prefilter%val(1:plotprof(ip)%nwl),prefilter%doprefilter, &
                cret_wlidx,cnret_wlidx,cobs_par, &
                0,cvoigt,cmagopt,cuse_geff,cuse_pb,cpb_method, &
                cmodeval,ciprof_only,cuse(1:cnline,ipt%ncomp),&
                plotprof(ip)%i,plotprof(ip)%q, &
                plotprof(ip)%u,plotprof(ip)%v,0_2,cold_norm,-1_2, &
                chanle_azi,cnorm_stokes_val,cold_voigt)
        end if
        wgtarr(1)=weight
        call plot_profile(plot_title,plotprof,ip,(/1,1,1,1/), &
             atm_result,scl,ipt%ncomp,wgtarr,1,ftns,lin,ipt%nline, &
             ipt%nblend,blend_result,gen_result)
     end if
  end do !multi iteration

  !write out buffered output
!  call flush()

end subroutine run_inversion
