      SUBROUTINE test(v)
      real*4 v
      integer :: i=0
      character(LEN=24) :: a,b,c
      
      DO i=1,8
        PRINT*,i,v
      END DO

      a='test'
      b='program'
      write(*,*) trim(a)//trim(b)

      END
      
        

       SUBROUTINE SUM_ARRAY(argc, argv)  !Called by IDL
       INTEGER*4 argc, argv(*)           !Argc and Argv are integers

       j = LOC(argc)         !Obtains the number of arguments (argc)
                      !Because argc is passed by VALUE.

       CALL SUM_ARRAY1(%VAL(argv(1)), %VAL(argv(2)), %VAL(argv(3)))
       RETURN
       END

      SUBROUTINE SUM_ARRAY1(array, n, sum)
       INTEGER*4 n
       REAL*4 array(n), sum

       sum=0.0
       DO i=1,n
       sum = sum + array(i)
       ENDDO
       RETURN
       END

program main
   character(LEN=48) :: c
   character(LEN=24) :: a,b
   character(LEN=400) :: file(10),path
      
      a='test'
      b='program'
      c=trim(a)//trim(b)
      file(1)='TEST'
      path='TESTPATH/'

      write(*,*) trim(path)//trim(file(1))
end program main
