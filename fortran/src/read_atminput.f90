module atminput_comm
  use all_type

  type(atminput_type),allocatable :: atminput(:)
end module atminput_comm

module atminput_module

  use all_type
  implicit none
  public

contains
  
  subroutine atminput_fill(xyvec)
    use atminput_comm
    use ipt_decl
    use tools
    integer(4) :: i,ia
    character(LEN=maxstr) :: xyvec
    integer(2) :: xv,yv,yindex
    !    integer(2) :: ncomp
    !    type(atmtyp) :: atm(ncomp)

    do ia=1,ipt%ncomp
       do i=1,atminput(ia)%npar
          yindex=index(xyvec,'y')
          xv=str2int(xyvec(2:yindex-1))+1
          yv=str2int(xyvec(yindex+1:len(trim(xyvec))))+1
          if ((xv.gt.atminput(ia)%nx).or.(yv.gt.atminput(ia)%ny)) then
             write(*,*) 'ATMINPUT_FILL: Cannot find x=',xv,'y=',yv,' in ',ipt%atm(ia)%atm_input
          else
             select case (trim(atminput(ia)%name(i)))
             case('BFIEL')
                ipt%atm(ia)%par%b=atminput(ia)%data(i,xv,yv)
             case('AZIMU')
                ipt%atm(ia)%par%azi=atminput(ia)%data(i,xv,yv)
             case('GAMMA')
                ipt%atm(ia)%par%inc=atminput(ia)%data(i,xv,yv)
             case('VELOS')
                ipt%atm(ia)%par%vlos=atminput(ia)%data(i,xv,yv)
             case('WIDTH')
                ipt%atm(ia)%par%width=atminput(ia)%data(i,xv,yv)
             case('AMPLI')
                ipt%atm(ia)%par%a0=atminput(ia)%data(i,xv,yv)
             case('VDAMP')
                ipt%atm(ia)%par%damp=atminput(ia)%data(i,xv,yv)
             case('VDOPP')
                ipt%atm(ia)%par%dopp=atminput(ia)%data(i,xv,yv)
             case('EZERO')
                ipt%atm(ia)%par%etazero=atminput(ia)%data(i,xv,yv)
             case('SZERO')
                ipt%atm(ia)%par%szero=atminput(ia)%data(i,xv,yv)
             case('SGRAD')
                ipt%atm(ia)%par%sgrad=atminput(ia)%data(i,xv,yv)
             case('GDAMP')
                ipt%atm(ia)%par%gdamp=atminput(ia)%data(i,xv,yv)
             case('VMICI')
                ipt%atm(ia)%par%vmici=atminput(ia)%data(i,xv,yv)
             case('DENSP')
                ipt%atm(ia)%par%densp=atminput(ia)%data(i,xv,yv)
             case('TEMPE')
                ipt%atm(ia)%par%tempe=atminput(ia)%data(i,xv,yv)
             case('DSLAB')
                ipt%atm(ia)%par%dslab=atminput(ia)%data(i,xv,yv)
             case('SLHGT')
                ipt%atm(ia)%par%height=atminput(ia)%data(i,xv,yv)
             case('ALPHA')
                ipt%atm(ia)%par%ff=atminput(ia)%data(i,xv,yv)
             case default
                if (ipt%verbose.ge.1) &
                write(*,*) 'Could not find parameter from ATMINPUT: ',atminput(ia)%name(i)
             end select
          end if
       end do
    end do

  end subroutine atminput_fill

     subroutine read_atminput()
       use ipt_decl
       use atminput_comm
       integer(2) :: ia,i,nparmax=0
       integer(4) :: unit,npar,nx,ny,bsz,status,naxes(3),fnaxes(3),nfound
       integer(4) :: fpixels(3),lpixels(3),inc(3),fstatus
       logical :: anyf
       real(4),allocatable :: data(:,:,:)
       character(LEN=72) :: comment
       character(LEN=maxstr) :: code

       allocate(atminput(ipt%ncomp))

       unit=35
       do ia=1,ipt%ncomp
          if (len_trim(ipt%atm(ia)%atm_input).ge.1) then
             if (ipt%verbose.ge.1) &
                  write(*,'(a,i2,'': '',a)') 'Reading ATM_INPUT fits file for COMP ',ia, &
                  trim(ipt%atm(ia)%atm_input)
             status=0
             call ftgiou(unit,status)
             call ftopen(unit,trim(ipt%atm(ia)%atm_input),1,bsz,status)
             if (status.ne.0) then
                write(*,*) 'Could not open FITS file: ',trim(ipt%atm(ia)%atm_input)
                call stop
             end if
             !size & offset of old data
             call ftgknj(unit,'NAXIS',1,3,fnaxes,nfound,status)
             naxes=fnaxes
             !check if file is atm_ file from HeLIx+ and transpose data accordingly
             call ftgkys(unit,'CODE',code,comment,fstatus)
             if (trim(code).eq.'HELIX+') then
                naxes(1)=fnaxes(3)
                naxes(2:3)=fnaxes(1:2)
             end if
             if (ipt%verbose.ge.2) write(*,*) 'npar=',naxes(1),' nx=',naxes(2),' ny=',naxes(3)
             if (naxes(1).gt.nparmax) nparmax=naxes(1)
             if (ia.eq.1) then
                nx=naxes(2)
                ny=naxes(3)
                npar=naxes(1)
             else
                if ((nx.ne.naxes(2)).or.(ny.ne.naxes(3))) then
                   write(*,*) 'ATM_INPUT error:'
                   write(*,*) 'COMP ',ia,': NX x NY dimension not matching first component'
                   call stop
                end if
             end if
             atminput(ia)%file=ipt%atm(ia)%atm_input
             atminput(ia)%npar=npar
             atminput(ia)%nx=nx
             atminput(ia)%ny=ny
             allocate(atminput(ia)%data(npar,nx,ny))
             allocate(data(fnaxes(1),fnaxes(2),fnaxes(3)))
             status=0
             call ftgkns(unit,'PAR',1,npar,atminput(ia)%name(1:npar),nfound,status)
             status=0
             fpixels(:)=(/1,1,1/)
             inc(:)=1
             lpixels=fnaxes
             call ftgsve(unit,1,3,fnaxes,fpixels,lpixels,inc,0,data,anyf,status)
             if (status.ne.0) then
                write(*,*) 'Could not read data from ATM_INPUT file.'
                call stop
             end if
             if (trim(code).eq.'HELIX+') then
                atminput(ia)%data=reshape(data,shape(atminput(ia)%data),order=[2,3,1])
             else
                atminput(ia)%data=data
             end if
             
             deallocate(data)
             call ftclos(unit,status)
          end if
       end do

     end subroutine read_atminput

   end module atminput_module
