module tools
  implicit none
  integer(4) :: debugcnt=0
  character(LEN=50) :: redirect,finddepth

contains

  subroutine set_syspar
#ifdef SUN
    redirect=' >/dev/null'
    finddepth=' '
#else
    redirect=' >/dev/null 2>/dev/null'
    finddepth=' -maxdepth 0'
#endif
  end subroutine set_syspar

  function check_nan(var)
#ifndef GNU
    USE, INTRINSIC :: IEEE_ARITHMETIC
#endif
    real(4) :: var(:)
    logical :: check_nan(size(var))

#ifdef IDL
    check_nan=isnan(var)
#elif GNU
    check_nan=isnan(var)
#else
    check_nan=ieee_is_nan(var)
#endif
  end function check_nan

  !!upcase function
  subroutine upcase(string) 
    implicit none
    character(len=*) :: string
    integer :: j
    do j = 1,len(string)
       if(string(j:j) >= "a" .and. string(j:j) <= "z") then
          string(j:j) = achar(iachar(string(j:j)) - 32)
       end if
    end do
    return
  end subroutine upcase
  !subroutine upcase (string)
  !  implicit none
  !  Character (len=*) ::  string
  !  string =                                                                  &
  !       transfer(merge(achar(iachar(transfer(string,"x",len(string)))-     &
  !       (ichar('a')-ichar('A')) ),                                         &
  !       transfer(string,"x",len(string)) ,                                 &
  !       transfer(string,"x",len(string)) >= "a" .and.                      &
  !       transfer(string,"x",len(string)) <= "z"), repeat("x", len(string)))
  !  return
  !end subroutine upcase

  function prg2ipt(paript)
    use all_type
    character(idlen) :: paript,prg2ipt,allipt(17),allprg(17)
    integer(2) :: i

    allipt=(/ &
         'BFIEL','AZIMU','GAMMA','VELOS', &
         'WIDTH','AMPLI','VDAMP','VDOPP','EZERO','SZERO', &
         'SGRAD','GDAMP','VMICI','DENSP','TEMPE','DSLAB','SLHGT' /)
    allprg=(/ &
         'B      ','AZI    ','INC    ','VLOS   ', &
         'WIDTH  ','A0     ','DAMP   ','DOPP   ','ETAZERO','SZERO  ', &
         'SGRAD  ','GDAMP  ','VMICI  ','DENSP  ','TEMPE  ','DSLAB  ','SLHGT  ' /)
    do i=1,17
       if (trim(allprg(i)).eq.trim(paript)) prg2ipt=allipt(i)
    end do

  end function prg2ipt

  !this subroutine writes out a file and closes it agin. Useful for
  !debugging since the output to STDOUT is buffered in the MPI version 
  subroutine debug(str)
#ifdef MPI
    include 'mpif.h'
#endif
    character(LEN=*) :: str
    character(len=4) :: scnt
    integer(4) :: myid, ierr, val(8)
#ifdef MPI
    call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
#else
    ierr=0
    myid=0
#endif

    debugcnt=debugcnt+1
    write(scnt,'(i4.4)') debugcnt
    CALL DATE_AND_TIME(VALUES=VAL)
    open(1,file='debug'//char(myid+48)//'_'//scnt//'.log',action='WRITE')
    write(1,*) val
    write(1,*) trim(str)
    close(1)
  end subroutine debug

  subroutine unix2c(utime, idate) 
    implicit none 
    integer utime, idate(6) 
    !utime  input  Unix system time, seconds since 1970.0 
    !idate  output Array: 1=year, 2=month, 3=date, 4=hour, 5=minute, 6=secs 
    !-Author  Clive Page, Leicester University, UK.   1995-MAY-2 
    integer mjday, nsecs 
    real day 
    !Note the MJD algorithm only works from years 1901 to 2099. 
    mjday    = int(utime/86400 + 40587) 
    idate(1) = 1858 + int( (mjday + 321.51) / 365.25) 
    day      = aint( mod(mjday + 262.25, 365.25) ) + 0.5 
    idate(2) = 1 + int(mod(day / 30.6 + 2.0, 12.0) ) 
    idate(3) = 1 + int(mod(day,30.6)) 
    nsecs    = mod(utime, 86400) 
    idate(6) = mod(nsecs, 60) 
    nsecs    = nsecs / 60 
    idate(5) = mod(nsecs, 60) 
    idate(4) = nsecs / 60 
  end subroutine unix2c

  subroutine debugr(x)
#ifdef MPI
    include 'mpif.h'
#endif
    real :: x
    character(len=4) :: scnt
    integer(4) :: myid, ierr, val(8)
#ifdef MPI
    call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
#else
    ierr=0
    myid=0
#endif

    debugcnt=debugcnt+1
    write(scnt,'(i4.4)') debugcnt
    CALL DATE_AND_TIME(VALUES=VAL)
    open(1,file='debug'//char(myid+48)//'_'//scnt//'.log',action='WRITE')
    write(1,*) val
    write(1,*) x
    close(1)
  end subroutine debugr

  function id2s(ids)
    use all_type
    implicit none
    character(LEN=idlen) :: id2s
    integer(1) :: ids(idlen),i

    do i=1,idlen
       id2s(i:i)=char(ids(i))
    end do
  end function id2s

  !convert a vector of byte characteres to a string
  function string(chr,len)
    use all_type
    implicit none
    integer(4) :: i,len
    integer(1) :: chr(len)
    character(LEN=maxstr) :: string

    do i=1,maxstr
       if (i.le.len) then
          string(i:i)=char(chr(i))
       else
          string(i:i)=char(32)
       end if
    end do
  end function string

  function str2real (string)
    implicit none
    Character (len=*) ::  string
    real(4) :: str2real
    integer(4) :: err

    read (string,*,iostat=err) str2real
    if (err.ne.0) then
       !     write(*,*) 'Error in converting '//trim(string)//' to real.'
       str2real=0.
    end if

    return
  end function str2real

  function str2double (string)
    implicit none
    Character (len=*) ::  string
    real(8) :: str2double
    integer(4) :: err

    read (string,*,iostat=err) str2double
    if (err.ne.0) then
       !     write(*,*) 'Error in converting '//trim(string)//' to double.'
       str2double=0.
    end if

    return
  end function str2double

  function str2int (string)
    implicit none
    Character (len=*) ::  string
    integer(4) :: err
    real(4) :: rl
    integer(2) :: str2int

    read (string,*,iostat=err) rl
    str2int=int(rl)
    if (err.ne.0) then
       !     write(*,*) 'Error in converting '//trim(string)//' to integer.'
       str2int=0
    end if

    return
  end function str2int

  subroutine mkdirhier(path,len,mode,ierr2)
    use all_type
    use ipt_decl
    implicit none
    !  type (ipttyp) :: ipt
    !  common /iptcom/ ipt
    character(maxstr) :: path,pth
    character(len=*) :: mode
    integer(4) :: len,ierr2,pos1,pos2!,ierror,ierr1,ierr3
    logical :: dir_e
#ifndef GNU
    INTERFACE
       INTEGER FUNCTION SYSTEM (COMMANDA)
         CHARACTER(LEN=*) COMMANDA
       END FUNCTION SYSTEM
    END INTERFACE
#endif

    ! interface
    !    integer function fortran_mkdir(dirname,mode) result(status)
    !      character(len=*), intent(in) :: dirname,mode
    !    end function fortran_mkdir
    ! end interface

    ! check for directory adding '/.'
    inquire( file=trim(path)//'/.', exist=dir_e )
    if ( dir_e ) then
       ierr2=0
       return
    end if

    !    open(10,file=trim(path)//'/'//'dummy.0',status='replace',iostat=ierr1)
    !    close(10)

    pth=path
    pos1=1
    do while (pos1.lt.len)
       pos2=index(path(pos1:len),'/')
       if (pos2.eq.0) pos2=len
       pth=path(1:pos1+pos2-1)
!       ierr2=fortran_mkdir(trim(pth),trim(mode))
       ierr2=system('mkdir -p '//trim(pth))
       pos1=pos1+pos2
    end do

    !check if file can be written to output directory
    !    open(10,file=trim(path)//'/'//'dummy.0',status='replace',iostat=ierr2)
    !    close(10)
    !    open(10,status='scratch',iostat=ierr3)
    !    close(10)
    !    if ((ierr2.ne.0).or.(ierr3.ne.0)) then
    !       write(*,*) 'Error in creating directory '//trim(path)
    !    else if (ierr1.ne.0) then
    !      if (ipt%verbose.ge.1) &
    !            write(*,*) 'Created directory '//trim(path)
    !    end if


  end subroutine mkdirhier

  function add_slash(dir)
    use all_type
    implicit none
    character(maxllen) :: dir,add_slash
    integer(2) :: ldir

    ldir=len_trim(dir)
    add_slash=dir(1:ldir)
    if (dir(ldir:ldir).ne.'/') then
       add_slash=add_slash(1:ldir)//'/'
    end if

  end function add_slash

  subroutine read_version(gitrevision)
    use all_type
    implicit none
    integer(4) :: err
    character(maxllen) :: gitrevision
    
    open(101,file='gitrevision',iostat=err, action='READ', form='FORMATTED', status='OLD')
    read(101, fmt='(a)',iostat=err) gitrevision
    close(101)
    if (err.ne.0) gitrevision='not available'
  end subroutine read_version
  
  !manual interpolation (linear, no extrapol)
  subroutine man_interpol(fin,xin,nin,fout,xout,nout)
    integer(2) :: nin,nout
    real(4) :: fin(nin),fout(nout)
    real(8) :: xin(nin),xout(nout)
    integer(2) :: i,j,idx

    fout=0
    do i=1,nout
       idx=-1
       do j=1,nin-1
          if ((xin(j).le.xout(i)).and.(xin(j+1).ge.xout(i))) idx=j
       end do
       if (idx.eq.-1) then 
          write(*,*) 'tools.f90, man_interpol: This routine does not allow for extrapolations.'
          return
       end if
       fout(i)=(fin(idx+1)-fin(idx))/(xin(idx+1)-xin(idx))* &
            (xout(i)-(xin(idx)))+fin(idx)
    end do
  end subroutine man_interpol

  !compute atan function in all 4 quadrants
  function atan4(y,x)
    !  real(4),dimension(:) :: x,y
    !  real(4),dimension(size(x)) :: atan4
    real(4) :: x,y
    real(4) :: atan4

    atan4=atan(y/x)
    !  where(x.lt.0) atan4=atan4+2*asin(1.)
    if (x.lt.0) atan4=atan4+2*asin(1.)
  end function atan4

  function minmax(val)
    real(4),dimension(:) :: val
    real(4),dimension(2) :: minmax
    minmax=(/ minval(val),maxval(val) /)
  end function minmax

  SUBROUTINE qsort(x,ind,n)

    ! Code converted using TO_F90 by Alan Miller
    ! Date: 2002-12-18  Time: 11:55:47

    IMPLICIT NONE
    !INTEGER, PARAMETER  :: dp = SELECTED_REAL_KIND(12, 60)

    REAL (4), INTENT(IN)  :: x(:)
    INTEGER, INTENT(OUT)   :: ind(:)
    INTEGER, INTENT(IN)    :: n

    !***************************************************************************

    !                                                         ROBERT RENKA
    !                                                 OAK RIDGE NATL. LAB.

    !   THIS SUBROUTINE USES AN ORDER N*LOG(N) QUICK SORT TO SORT A REAL (dp)
    ! ARRAY X INTO INCREASING ORDER.  THE ALGORITHM IS AS FOLLOWS.  IND IS
    ! INITIALIZED TO THE ORDERED SEQUENCE OF INDICES 1,...,N, AND ALL INTERCHANGES
    ! ARE APPLIED TO IND.  X IS DIVIDED INTO TWO PORTIONS BY PICKING A CENTRAL
    ! ELEMENT T.  THE FIRST AND LAST ELEMENTS ARE COMPARED WITH T, AND
    ! INTERCHANGES ARE APPLIED AS NECESSARY SO THAT THE THREE VALUES ARE IN
    ! ASCENDING ORDER.  INTERCHANGES ARE THEN APPLIED SO THAT ALL ELEMENTS
    ! GREATER THAN T ARE IN THE UPPER PORTION OF THE ARRAY AND ALL ELEMENTS
    ! LESS THAN T ARE IN THE LOWER PORTION.  THE UPPER AND LOWER INDICES OF ONE
    ! OF THE PORTIONS ARE SAVED IN LOCAL ARRAYS, AND THE PROCESS IS REPEATED
    ! ITERATIVELY ON THE OTHER PORTION.  WHEN A PORTION IS COMPLETELY SORTED,
    ! THE PROCESS BEGINS AGAIN BY RETRIEVING THE INDICES BOUNDING ANOTHER
    ! UNSORTED PORTION.

    ! INPUT PARAMETERS -   N - LENGTH OF THE ARRAY X.

    !                      X - VECTOR OF LENGTH N TO BE SORTED.

    !                    IND - VECTOR OF LENGTH >= N.

    ! N AND X ARE NOT ALTERED BY THIS ROUTINE.

    ! OUTPUT PARAMETER - IND - SEQUENCE OF INDICES 1,...,N PERMUTED IN THE SAME
    !                          FASHION AS X WOULD BE.  THUS, THE ORDERING ON
    !                          X IS DEFINED BY Y(I) = X(IND(I)).

    !*********************************************************************

    ! NOTE -- IU AND IL MUST BE DIMENSIONED >= LOG(N) WHERE LOG HAS BASE 2.

    !*********************************************************************

    INTEGER   :: iu(21), il(21)
    INTEGER   :: m, i, j, k, l, ij, it, itt, indx
    REAL      :: r
    REAL (4) :: t

    ! LOCAL PARAMETERS -

    ! IU,IL =  TEMPORARY STORAGE FOR THE UPPER AND LOWER
    !            INDICES OF PORTIONS OF THE ARRAY X
    ! M =      INDEX FOR IU AND IL
    ! I,J =    LOWER AND UPPER INDICES OF A PORTION OF X
    ! K,L =    INDICES IN THE RANGE I,...,J
    ! IJ =     RANDOMLY CHOSEN INDEX BETWEEN I AND J
    ! IT,ITT = TEMPORARY STORAGE FOR INTERCHANGES IN IND
    ! INDX =   TEMPORARY INDEX FOR X
    ! R =      PSEUDO RANDOM NUMBER FOR GENERATING IJ
    ! T =      CENTRAL ELEMENT OF X

    IF (n <= 0) RETURN

    ! INITIALIZE IND, M, I, J, AND R

    DO  i = 1, n
       ind(i) = i
    END DO
    m = 1
    i = 1
    j = n
    r = .375

    ! TOP OF LOOP

20  IF (i >= j) GO TO 70
    IF (r <= .5898437) THEN
       r = r + .0390625
    ELSE
       r = r - .21875
    END IF

    ! INITIALIZE K

30  k = i

    ! SELECT A CENTRAL ELEMENT OF X AND SAVE IT IN T

    ij = i + r*(j-i)
    it = ind(ij)
    t = x(it)

    ! IF THE FIRST ELEMENT OF THE ARRAY IS GREATER THAN T,
    !   INTERCHANGE IT WITH T

    indx = ind(i)
    IF (x(indx) > t) THEN
       ind(ij) = indx
       ind(i) = it
       it = indx
       t = x(it)
    END IF

    ! INITIALIZE L

    l = j

    ! IF THE LAST ELEMENT OF THE ARRAY IS LESS THAN T,
    !   INTERCHANGE IT WITH T

    indx = ind(j)
    IF (x(indx) >= t) GO TO 50
    ind(ij) = indx
    ind(j) = it
    it = indx
    t = x(it)

    ! IF THE FIRST ELEMENT OF THE ARRAY IS GREATER THAN T,
    !   INTERCHANGE IT WITH T

    indx = ind(i)
    IF (x(indx) <= t) GO TO 50
    ind(ij) = indx
    ind(i) = it
    it = indx
    t = x(it)
    GO TO 50

    ! INTERCHANGE ELEMENTS K AND L

40  itt = ind(l)
    ind(l) = ind(k)
    ind(k) = itt

    ! FIND AN ELEMENT IN THE UPPER PART OF THE ARRAY WHICH IS
    !   NOT LARGER THAN T

50  l = l - 1
    indx = ind(l)
    IF (x(indx) > t) GO TO 50

    ! FIND AN ELEMENT IN THE LOWER PART OF THE ARRAY WHCIH IS NOT SMALLER THAN T

60  k = k + 1
    indx = ind(k)
    IF (x(indx) < t) GO TO 60

    ! IF K <= L, INTERCHANGE ELEMENTS K AND L

    IF (k <= l) GO TO 40

    ! SAVE THE UPPER AND LOWER SUBSCRIPTS OF THE PORTION OF THE
    !   ARRAY YET TO BE SORTED

    IF (l-i > j-k) THEN
       il(m) = i
       iu(m) = l
       i = k
       m = m + 1
       GO TO 80
    END IF

    il(m) = k
    iu(m) = j
    j = l
    m = m + 1
    GO TO 80

    ! BEGIN AGAIN ON ANOTHER UNSORTED PORTION OF THE ARRAY

70  m = m - 1
    IF (m == 0) RETURN
    i = il(m)
    j = iu(m)

80  IF (j-i >= 11) GO TO 30
    IF (i == 1) GO TO 20
    i = i - 1

    ! SORT ELEMENTS I+1,...,J.  NOTE THAT 1 <= I < J AND J-I < 11.

90  i = i + 1
    IF (i == j) GO TO 70
    indx = ind(i+1)
    t = x(indx)
    it = indx
    indx = ind(i)
    IF (x(indx) <= t) GO TO 90
    k = i

100 ind(k+1) = ind(k)
    k = k - 1
    indx = ind(k)
    IF (t < x(indx)) GO TO 100

    ind(k+1) = it
    GO TO 90
  END SUBROUTINE qsort

  function ipp(i)
    integer(2) :: i,ipp
    i=i+1
    ipp=i
  end function ipp

  subroutine delay_delete_file(name,time)
    implicit none
    character(LEN=*) :: name
    integer :: nwarn,i
    real(4) :: tstep,time

    nwarn=3
    tstep=time/nwarn
    do i=1,nwarn
       write(*,'(a,i3,a)') 'DELETING '//trim(name)//' IN ',&
            int((nwarn-i+1)*tstep),'s'
       write(*,*) 'PRESS CTRL-C TO ABORT'
       call sleep(int(tstep))
    end do
    open(100,file=name)
    close(100,status="delete")
    write(*,*) 'File '//trim(name)//' deleted.'

  end subroutine delay_delete_file

  function reverse_string(cadena)
    use all_type
    implicit none
    character(LEN=maxstr) :: cadena,reverse_string
    integer :: k, n

    n = len_trim (cadena)
    reverse_string=cadena
    forall (k=1:n) reverse_string (k:k) = cadena (n-k+1:n-k+1)
    
  end function reverse_string
  
  subroutine timecheck(init)
    use ftime
    real(8) :: tcpu_start,tcpu_stop,treal_start,treal_stop
    integer :: init
    save

    if (init.eq.1) then
       write(*,*) 'Timecheck Init'
       tcpu_start=cputime()
       treal_start=realtime()
    else
       tcpu_stop=cputime()
       treal_stop=realtime()
       write(*,'(3(a,f8.3))') &
            'Timing: CPU =',tcpu_stop-tcpu_start,&
            ' Real=',treal_stop-treal_start,&
            ' Load=',100*(tcpu_stop-tcpu_start)/(treal_stop-treal_start)
    end if
  end subroutine timecheck

  subroutine def_parname(parname)
    use all_type
    implicit none
    type(parnametyp) :: parname

    parname%b%ipt='BFIEL'
    parname%b%prg='B'
    parname%azi%ipt='AZIMU'
    parname%azi%prg='AZI'
    parname%inc%ipt='GAMMA'
    parname%inc%prg='INC'
    parname%vlos%ipt='VELOS'
    parname%vlos%prg='VLOS'
    parname%width%ipt='WIDTH'
    parname%width%prg='WIDTH'
    parname%damp%ipt='VDAMP'
    parname%damp%prg='DAMP'
    parname%dopp%ipt='VDOPP'
    parname%dopp%prg='DOPP'
    parname%a0%ipt='AMPLI'
    parname%a0%prg='A0'
    parname%sgrad%ipt='SGRAD'
    parname%sgrad%prg='SGRAD'
    parname%szero%ipt='SZERO'
    parname%szero%prg='SZERO'
    parname%etazero%ipt='EZERO'
    parname%etazero%prg='ETAZERO'
    parname%gdamp%ipt='GDAMP'
    parname%gdamp%prg='GDAMP'
    parname%vmici%ipt='VMICI'
    parname%vmici%prg='VMICI'
    parname%densp%ipt='DENSP'
    parname%densp%prg='DENSP'
    parname%tempe%ipt='TEMPE'
    parname%tempe%prg='TEMPE'
    parname%dslab%ipt='DSLAB'
    parname%dslab%prg='DSLAB'
    parname%height%ipt='SLHGT'
    parname%height%prg='HEIGHT'
    parname%ff%ipt='ALPHA'
    parname%ff%prg='FF'

  end subroutine def_parname

  subroutine def_blendname(blendname)
    use all_type
    implicit none
    type(blendnametyp) :: blendname

    blendname%wl%ipt='BLEND_WL'
    blendname%wl%prg='WL'
    blendname%width%ipt='BLEND_WIDTH'
    blendname%width%prg='WIDTH'
    blendname%damp%ipt='BLEND_DAMP'
    blendname%damp%prg='DAMP'
    blendname%a0%ipt='BLEND_A0'
    blendname%a0%prg='A0'

  end subroutine def_blendname

  subroutine def_linename(linename)
    use all_type
    implicit none
    type(linenametyp) :: linename

    linename%wlshift%ipt='LINE_WLSHIFT'
    linename%wlshift%prg='WLSHIFT'
    linename%strength%ipt='LINE_STRENGTH'
    linename%strength%prg='STRENGTH'
    linename%straypol_amp%ipt='STRAYPOL_AMP'
    linename%straypol_amp%prg='STRAYPOL_AMP'
    linename%straypol_eta0%ipt='STRAYPOL_ETA0'
    linename%straypol_eta0%prg='STRAYPOL_ETA0'
  end subroutine def_linename

  subroutine def_genname(genname)
    use all_type
    implicit none
    type(gennametyp) :: genname

    genname%ccorr%ipt='CCORR'
    genname%straylight%ipt='STRAYLIGHT'
    genname%radcorrsolar%ipt='RADCORRSOLAR'
  end subroutine def_genname


  subroutine mysleep(tmsec)
    !===============================================================================
    implicit none
    character(len=100) :: arg ! input argument character string
    integer,dimension(8) :: t ! arguments for date_and_time
    integer :: s1,s2,ms1,ms2  ! start and end times [ms]
    real :: tmsec                ! desired sleep interval [ms]
    !===============================================================================
    ! Get start time:
    call date_and_time(values=t)
    ms1=(t(5)*3600+t(6)*60+t(7))*1000+t(8)

    do ! check time:
       call date_and_time(values=t)
       ms2=(t(5)*3600+t(6)*60+t(7))*1000+t(8)
       if(ms2-ms1>=tmsec)exit
    enddo
    !===============================================================================
  end subroutine mysleep
end module tools
