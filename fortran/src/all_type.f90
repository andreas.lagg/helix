!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!                            M O D U L E  A L L _T Y P E             
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
module all_type
  implicit none


  public
  integer(2), parameter :: maxlin=20 !!Max number of lines
  !maxlin must be equal to maxatm
  integer(2), parameter :: maxatm=20 !!Max number of atmospheres
  integer(2), parameter :: maxfitpar=256 !!Max number of atmospheres
  integer(2), parameter :: maxspec=8 !!Max number of spectra in profile.dat
  integer(2), parameter :: maxwl=4096 !!Max number of WL-points
  integer(2), parameter :: maxcwl=32767 !!Max number of WL-points for
                                       !!    convolve function
  integer(2), parameter :: idlen=12  !!length of ID string
  integer(2), parameter :: maxlines=16  !!max. number of lines per atomic-file
  integer(2), parameter :: maxblend=16  !!max. number of telluric blends
  integer(2), parameter :: maxmi=8   !!max. number of multi-iterations
  integer(2), parameter :: maxcmt=104 !!max. # of lines for comments
  integer(2), parameter :: maxipt=1024  !!Max # of lines in input file
  integer(2), parameter :: maxllen=160  !!Max len of input lines
  integer(2), parameter :: maxstr=160  !!Max len of strings
  integer(2), parameter :: maxlonstr=600  !!Max len of long strings
  integer(2), parameter :: npartot=18  !!total # of possible parameters 
  integer(2), parameter :: nblendpartot=4  !!total # of poss. pars for blends 
  integer(2), parameter :: ngenpartot=3  !!total # of poss. general parameters
  integer(2), parameter :: nlinpartot=4  !!total # of line parameters 
  integer(2), parameter :: maxplotprof=16  !!max number of plot profiles
  integer(2), parameter :: maxavg=4096 !!Max number of profiles to average 
  integer(4), parameter :: maxpavg=65536 !!Max number of profiles to average 
  !!                                    in two dimensions
  integer(2), parameter :: maxpi=17,maxsigma=16 !!max. quantum numbers J
  !!                               defines max. size for zeeman pattern arrays
  !                                =2*minval([maxjl,maxju])+1, max # of pi-comp
  !                                =maxjl+maxju , max # of sigma-comp.
  integer(2), parameter :: max_pb_zl=17 !max. number of zeeman levels (storage
  !                                     of Paschen-Back table from
  !                                     Socas-Navarro)
  integer(2), parameter :: max_pb_nr=20  ! max. number of different B-values
  !                                        for PB-table
  !THE NEXT TWO VALUES MUST BE ADJUSTED ALSO IN PIKAIA.F (NMAX, PMAX)
  integer(2), parameter :: pik_NMAX=96  ! max. number of PIKAIA parameters
  integer(2), parameter :: pik_PMAX=512 ! max. number of PIKAIA populations
  
!  integer(4) :: cntff1,cntff2,cntff3,cntff4
#ifdef MPI
  integer(4), parameter :: mpitag1=301,mpitag2=302,mpitag3=303,mpitag4=304, &
       mpitag5=305,mpitag6=306,mpitag7=307,mpitag8=308,mpitag9=309, &
       mpitag10=310,mpitag11=311,mpitag12=312,mpitag13=313,mpitag14=314
#endif

  !!dirtyp structure
  type dirtyp
     sequence
     character(LEN=maxllen) :: ps,profile,sav,atm,atm_suffix,wgt,atom
  end type dirtyp


  !!     parameter set for one component: values
  !!     must be npartot values!!!
  type partyp
     sequence
     real(4) :: b=0
     real(4) :: azi=0
     real(4) :: inc=0
     real(4) :: vlos=0
     real(4) :: width=0
     real(4) :: damp=0
     real(4) :: dopp=0
     real(4) :: a0=0
     real(4) :: szero=0
     real(4) :: sgrad=0
     real(4) :: etazero=0
     real(4) :: gdamp=0
     real(4) :: vmici=0
     real(4) :: densp=0
     real(4) :: tempe=0
     real(4) :: dslab=0
     real(4) :: height=0
     real(4) :: ff=0    
  end type partyp

  type iptyp
     sequence
     character(LEN=idlen) :: ipt,prg
  end type iptyp

  type gennametyp
     sequence
     type(iptyp) :: ccorr
     type(iptyp) :: straylight
     type(iptyp) :: radcorrsolar
  end type gennametyp

  type blendnametyp
     sequence
     type(iptyp) :: wl
     type(iptyp) :: width
     type(iptyp) :: damp
     type(iptyp) :: a0
  end type blendnametyp

  type linenametyp
     sequence
     type(iptyp) :: strength
     type(iptyp) :: wlshift
     type(iptyp) :: straypol_amp
     type(iptyp) :: straypol_eta0
  end type linenametyp

  type parnametyp
     sequence
     type(iptyp) :: b
     type(iptyp) :: azi
     type(iptyp) :: inc
     type(iptyp) :: vlos
     type(iptyp) :: width
     type(iptyp) :: damp
     type(iptyp) :: dopp
     type(iptyp) :: a0
     type(iptyp) :: szero
     type(iptyp) :: sgrad
     type(iptyp) :: etazero
     type(iptyp) :: gdamp
     type(iptyp) :: vmici
     type(iptyp) :: densp
     type(iptyp) :: tempe
     type(iptyp) :: dslab
     type(iptyp) :: height
     type(iptyp) :: ff
  end type parnametyp

  type namtyp
     sequence
     character(idlen) :: b
     character(idlen) :: azi
     character(idlen) :: inc
     character(idlen) :: vlos
     character(idlen) :: width
     character(idlen) :: damp
     character(idlen) :: dopp
     character(idlen) :: a0
     character(idlen) :: szero
     character(idlen) :: sgrad
     character(idlen) :: etazero
     character(idlen) :: gdamp
     character(idlen) :: vmici
     character(idlen) :: densp
     character(idlen) :: tempe
     character(idlen) :: dslab
     character(idlen) :: height
     character(idlen) :: ff
  end type namtyp

  !!     parameter set for one component: fit< flag
  type fittyp
     sequence
     integer(2) :: b=0
     integer(2) :: azi=0
     integer(2) :: inc=0
     integer(2) :: vlos=0
     integer(2) :: width=0
     integer(2) :: damp=0
     integer(2) :: dopp=0
     integer(2) :: a0=0
     integer(2) :: szero=0
     integer(2) :: sgrad=0
     integer(2) :: etazero=0
     integer(2) :: gdamp=0
     integer(2) :: vmici=0
     integer(2) :: densp=0
     integer(2) :: tempe=0
     integer(2) :: dslab=0
     integer(2) :: height=0
     integer(2) :: ff=0
  end type fittyp

  !!     min/max structure
  type mmtyp
     sequence
     real(4) :: min=0,max=0
  end type mmtyp

  !!     parameter set for scaling and default values
  type scltyp
     sequence
     type (mmtyp) :: b,azi,inc,vlos,width,damp,dopp,a0,szero,sgrad,etazero, &
          gdamp,vmici,densp,tempe,dslab,height,ff
  end type scltyp

  !!			structure holding all atmospheric parameters
  type atmtyp
     sequence
     type (fittyp) :: fit
     type (fittyp) :: fitflag
     type (fittyp) :: idx
     type (partyp) :: par
     type (partyp) :: ratio
     integer(4) :: npar
     integer(4) :: linid
     integer(2) :: straypol_use
     integer(2) :: dummy1
     integer(1) :: use_line(idlen,maxlin)=0
     character(LEN=maxstr) :: atm_input
  end type atmtyp

  type atminput_type
     sequence
     integer(2) :: npar=0,nx=0,ny=0,dummy
     character(LEN=maxstr) :: file
     character(LEN=idlen) :: name(npartot)
     real(4),allocatable :: data(:,:,:)
  end type atminput_type

  !!blends
  type blendscl
     sequence
     type (mmtyp) :: wl,width,damp,a0
  end type blendscl

  !!blends
  type blendfit
     sequence
     integer(2) :: wl,width,damp,a0
  end type blendfit

  !!blends
  type blendpar
     sequence
     real(8) :: wl
     real(4) :: width,damp,a0
     integer(4) :: dummy
  end type blendpar

  !! structure holding info for telluric blends
  type blendtyp
     sequence
     type (blendfit) :: fit
     type (blendfit) :: idx
     type (blendpar) :: par
     type (blendscl) :: scale
     integer(4) :: npar
     integer(4) :: dummy
  end type blendtyp

  type genscl
     sequence
     type (mmtyp) :: ccorr
     type (mmtyp) :: straylight
     type (mmtyp) :: radcorrsolar
  end type genscl

  !!gen pars
  type genfit
     sequence
     integer(2) :: ccorr
     integer(2) :: straylight
     integer(2) :: radcorrsolar
     integer(2) :: dummy
  end type genfit

  !!gen pars
  type genpar
     sequence
     real(4) :: ccorr
     real(4) :: straylight
     real(4) :: radcorrsolar
     real(4) :: dummy
  end type genpar

  !! structure holding info for general fit parameters
  type gentyp
     sequence
     type (genfit) :: fit
     type (genfit) :: idx
     type (genpar) :: par
     type (genscl) :: scale
     integer(4) :: npar
  end type gentyp

  !!			structure holding lines
  type qantyp
     sequence
     integer(2) :: n_pi,n_sig,dummy(2)
     real(8),dimension(maxsigma) :: nub,nur,web,wer
     real(8),dimension(maxpi) :: nup,wep
  end type qantyp
  type linfit
     sequence
     integer(2) :: straypol_amp,straypol_eta0,strength,wlshift
  end type linfit
  type linpar
     sequence
     real(4) :: straypol_amp,straypol_eta0,strength,wlshift
  end type linpar
  type linscl
     sequence
     type (mmtyp) :: straypol_amp,straypol_eta0,strength,wlshift
  end type linscl
  type straypolpar
     sequence
     real(4) :: vlos,width,damp,dopp
     integer(2) :: sign,dummy
  end type straypolpar
  type pbtyp
     sequence
     integer(2) :: nr_zl,nr_b,dummy(2)
     real(8),dimension(max_pb_zl,max_pb_nr) :: splitting
     real(8),dimension(max_pb_zl,max_pb_nr) :: strength
     real(4),dimension(maxsigma,4) :: cr,cb,dr,db
     real(4),dimension(maxpi,4) :: cp,dp
     real(4),dimension(max_pb_nr) :: b
  end type pbtyp
  type hanletyp
     sequence
     integer(4) :: is2,nterm,ntrans
     integer(4),dimension(30) :: ntm,nts,lsto2
     integer(4),dimension(3) :: adummy2
     real(8),dimension(30,0:20) :: energy
     integer(4),dimension(50) :: ntlsto,ntusto
     real(8),dimension(50) :: aesto,fsto,u1sto,u2sto,riosto,wlsto
  end type hanletyp
  type linetyp
     sequence
     real(8) :: wl
     real(4) :: geff
     real(4) :: f=0
     real(4) :: loggf=-20
     real(4) :: abund=0
     real(4) :: icont=1.
     real(4) :: ju,jl
     real(4) :: lu,ll
     real(4) :: su,sl
     real(4) :: gu,gl
     real(4) :: mass
     type (linfit) :: fit
     type (linfit) :: idx
     type (linpar) :: par
     type (linscl) :: scale
     type (straypolpar) :: straypol_par(maxatm)
!     integer(2) :: dummy1
     type (qantyp) :: quan
     integer(2) :: straypol_use
     integer(2) :: npar,ion,use_hanle_slab
!     integer(2) :: dummy2(2)
     integer(1) :: id(idlen)=0
     integer(2) :: dummy3(2)
     type (pbtyp) :: pb
     type (hanletyp) :: hanle
  end type linetyp

  type obstyp
     sequence
     real(4) :: slit_orientation,m1angle,posx,posy,solar_radius, &
          heliocentric_angle,posq2limb
     integer(2) :: hin_scannr,dummy
  end type obstyp

  !!multi iter-typ
  type misub
     sequence
     real(4) :: perc_rg(maxmi)
     integer(2) :: fit(maxmi)
  end type misub

  type mityp
     sequence
     type (misub)  :: b
     type (misub)  :: azi
     type (misub)  :: inc
     type (misub)  :: vlos
     type (misub)  :: width
     type (misub)  :: damp
     type (misub)  :: dopp
     type (misub)  :: a0
     type (misub)  :: szero
     type (misub)  :: sgrad
     type (misub)  :: etazero
     type (misub)  :: gdamp
     type (misub)  :: vmici
     type (misub)  :: densp
     type (misub)  :: tempe
     type (misub)  :: dslab
     type (misub)  :: height
     type (misub)  :: ff      
  end type mityp

  !!     structure containing input file
  type ipttyp
     sequence
     real(8) :: wl_range(2) !!WL-range to be used
     real(8) :: wl_disp,wl_off !dispersion and offset of WL-calibration
!     real(4) :: ccorr=1. !!factor for I (constant continuum correction)
!     real(4) :: align_dummy0
     real(4) :: hanle_azi !hanle azimuth offset (experimental)
     real(4) :: straypol_amp=0. !!amplitude for straypol-correction (only used
     !!for synthesis)
     real(4) :: approx_dev_inc(maxmi)=0 !!allowed deviation of inclination from Unno
     !!solution (in degrees)
     real(4) :: approx_dev_azi(maxmi)=0 !!allowed deviation of azimuth from Unno
     !!solution (in degrees)
     real(4) :: iquv_weight(4,maxmi)=1. !!4-element vector defining relative weighting
     !!of IQUV. Additionally the code does an
     !!automatic weighting according to the
     !!strength of the I signal compared to
     !!the QUV signals. Not used for Unno solution.
     real(4) :: min_quv=0. !!minimum magnetic signal
     real(4) :: noise=0. !!add artificial random noise to synth. profile 
     real(4) :: inoise=0. !!add artificial random noise to Stokes I profile 
     real(4) :: align_dummy1
     character(LEN=maxstr) :: wgt_file(maxmi)
     character(LEN=maxstr) :: atom_file(maxatm,maxlines) !atomic data filenames
     character(LEN=maxstr) :: profile_list
     character(LEN=maxstr) :: conv_func !filename of convolution function
                                       !to simulate measurement 
     character(LEN=maxstr) :: prefilter !filename of prefilter curve
                                       !to simulate measurement 
     character(maxstr) :: file
     character(LEN=maxllen) :: ascii(maxipt)
     character(LEN=maxllen) :: comment(maxcmt)
     character(maxstr) :: observation !!filename of IDL-save file containing the
     !!profiles
     character(idlen) :: parset(npartot) !!list of allowed parameters in different modes
     character(idlen) :: hanle_slab(npartot) !!parset for hanle narrow slab model
     character(idlen) :: voigt_phys(npartot) !!parset for voigt - physical pars  
     character(idlen) ::  voigt_par(npartot) !!parset for normal voigt
     character(idlen) ::  voigt_gdamp(npartot) !!parset for normal voigt
     character(idlen) ::  voigt_szero(npartot) !!parset for voigt with S0
     character(idlen) ::  gauss_par(npartot) !!parset for gauss
     character(idlen) ::  fitpar(maxfitpar) !!name of free parameters
     character(LEN=maxllen) :: code
     character(LEN=maxllen) :: pikaia_stat
     character(LEN=maxllen) :: norm_stokes !!normalization method: I or IC
     type (dirtyp) :: dir
     type (atmtyp) :: atm(maxatm) !!parameter values
     type (scltyp) :: scale(maxatm) !!scaling for fit parameters
     type (mityp) :: mi(maxatm) !!multi iteration variable
     type (blendtyp) :: blend(maxblend) !! variable containing blends
     type (obstyp) :: obs_par !!observation parameters
!     real(4) :: align_dummy2
     type (linetyp) :: line(maxlin) !!structure containing line parameters
     real(4) :: localstray_rad !radius for local straylight correction
     real(4) :: localstray_fwhm !fwhm for local straylight correction
     real(4) :: localstray_core !readius of core to be excluded
     integer(2) :: localstray_pol !!pol/unpol straylight
     integer(2) :: chi2mode !method to calc. chi2
     real(4) :: ic_level !overwrite IC level of data
     integer(2) :: n_ascii,n_cmt
     integer(2) :: display_map=0, & !!flag for display at end of run
          display_profile=1, & !!flag for display of profiles
          display_comp=0, & !!flag for display of atm. comp.
          old_norm=0, & !calculation in old normalization (SGRAD changed!)
          old_voigt=0, & !calculation in old voigt normalization
          save_fitprof=0, & !!if 1, the fitted profile is saved (IDL-sav)
          fitsout=0, & !!map results are written to FITS file
          ps=0, &  !!set output: X (=def.) or PS
          ncomp=0,& !!number of components
          nblend=0,& !!number of blends
          nline=0,& !!number of lines
          straypol_corr=0, & !!correction for straylight-polarization  
          stray=0, & !!flag: if set to 1 then the first component
          norm_stokes_val=0,& !value for stokes normalization (0=Ic, 1=I)
          norm_cont=0,&   !method for cont. normalization: local, slit, image  
          align_dummy(3)
      integer(4) ::  nfitpar=0 !number of free parameters
     !!is treated as straylight component, that
     !!means, no mag-field is fitted
     integer(2) :: approx_dir(maxmi)=0 !!if set to 1 then use Unno-solution for
     !!determination of inclination. In this case
     !!the inlcination is not fitted by pikaia.
     !!Additionally, the azimuthal angle is only
     !!calculated using Q and U observed profile.
     integer(2) :: approx_azi(maxmi)=0 !!only fit azimuth using unno solution,
     !!inclination is a free parameter
     integer(2) :: approx_dev(maxmi)=0 !!if set then the Unno solution is
     !! used as an estimate for
     !!the pikaia fit: the pikaia values for
     !!inclination and azimuth are only allowed to
     !!vary within a narrow range of the Unno
     !!solution
     integer(2) :: x(2),y(2)      !!two-elements vector containing xymin,xymax of
     !!the observation map to be analyzed
     integer(2) ::  stepx=4,stepy=4, & !!step size for going from xymin to xymax
          nosav=1, & !!if set to 1, no sav-file of the result is created
          average=1, & !!if set to one then average observation over
                                !!the xstep/ystep size
          scansize=0, & !!number of steps for one scan in a
                                !!repetitive observation
          prof_shape=0, & !!define profile shape of pi/sigma
                                !!components (gauss=0, voigt=1). default: Gauss
          synth=0, & !!synthetic profile mode
          smooth=0, & !smooth profiles
          median=0, & !!median filter for profiles
          spike=0 !!spike removal
     integer(2) :: align_dummy41(2)
     integer(2) :: magopt(maxmi)=1 !!include mag-optical effects (voigt only)
     integer(2) :: use_geff(maxmi)=1 !!use effective Lande factor (=1) or
                                     !! real Zeeman pattern (=0)
     integer(2) :: use_pb(maxmi)=0 !! if set, the zeeman-splitting and
     ! strength includes the Paschen-Back effect (from the table by
     ! Socas-Navarro)
     integer(2) :: pb_method(maxmi)=0 !! use polynomials (=poly=0) or table
     !! interpolations (=table=1) to calculate the PB-effect
     integer(2) :: method(maxmi)=0 !!minimization method
     integer(2) :: ncalls(maxmi) !!number of pikaia iterations
     integer(4) :: pixelrep,pikaia_stat_cnt
     integer(4) :: align_dummy5
     integer(2) :: pikaia_pop !!number of pikaia populations
     integer(2) :: filter_mode=0, & !!filtering method (0=IDL-smooth,1=FFT Low-Pass)
          verbose=1, & !!control verbosity (pikaia)
          nmi=0, & !!number of multi iterations
          wl_num=200, & !!number of WL-points for synthesis
          wl_bin=1, & !!WL binning
          conv_nwl=0, & !# of WL-points for convolution
          conv_mode=0, & !convolution method: 0=FFT, 1=MULTIPLICATION
          conv_output=0, & !flag for output of fittef profile
          modeval=0, & !!mode as index: 0=gauss, 1=voigt-par, 2=voigt-phys
          nparset, & !!# of parameters in parset
          piklm_bx,piklm_by, &!box size for combined pikaia/lmdiff mode
          keepbest=1,&  !flag to keep result only when fit is better
          align_dummy6(2)
     real(4) :: piklm_minfit !rerun with PIKAIA if fitness drops to
                             !piklm_minfit percent of previos value
     real(4) :: conv_wljitter !uncertainty in knowledge of wavelength
                              !for filter position
     real(4) :: prefilter_wlerr !error in knowledge of prefilter curve
                                !(e.g. caused by unknown
                                !temperature). Must be set to ZERO for
                                !real data!
     real(4) :: align_dummy7(1)
     type (gentyp) :: gen !! variable containing general parameters
!     real(4) :: align_dummy5
  end type ipttyp

  type iptgrptyp
     sequence
     character(LEN=48) :: name
     character(LEN=48) :: element(100)
     character(LEN=96) :: default(100)
     character(LEN=400) :: comment(100)
     integer(2) :: nmipar(100)
     integer(2) :: show(100)
  end type iptgrptyp

  type proftyp
     sequence
     integer(2) :: nwl,dummy
     real(4) :: ic
     real(8) :: wlref
     real(8),dimension(maxwl) :: wl!(maxwl)
     real(4),dimension(maxwl) :: i,q,u,v
     character(maxstr) :: name
  end type proftyp

  type convtyp
     sequence
     real(8),dimension(maxwl) :: wl
     real(4),dimension(maxwl) :: val
     real(4) :: normval
     integer(2),dimension(maxwl) :: iwl_compare
     integer(2) :: nwl_compare=-1,nwl=0,doconv=0,mode=0,dummy(2)
  end type convtyp

  type prefiltertyp
     sequence
     real(8),dimension(maxwl) :: wl
     real(4),dimension(maxwl) :: val
     integer(2) :: nwl=0,doprefilter=0,dummy(2)
  end type prefiltertyp

end module all_type

