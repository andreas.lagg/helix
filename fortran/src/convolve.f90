!Does the convolution using the rfft package.
!to convolve a function with a filter you first have to define wsave:
!  call rffti(n,wsave)
!then you have to calculate the fft of the filter function
!  call rfftf(n,filter,wsave)
!with wsave and the fft of the filter you can then enter this routine.

!important: for the rfft functions n MUST be an integer(4)

!input: 1-D data array and forward FFT of filter function, wsave from rffti
!output: data contains convoluted function
subroutine convolve(n,data,fftfilt,wsave)
  integer(4) :: n,i
  real(4),dimension(n) :: data,fftfilt
  real(4),dimension(4*n+15) :: wsave
  real(4) :: a,b,c,d,fac

!  double complex in, out
!  dimension in(N), out(N)
!  integer*8 plan
  
!  in=data 
!  call dfftw_plan_dft_1d(plan,N,in,out,-1,FFTW_MEASURE)
!  call dfftw_execute_dft(plan, in, out)
!  call dfftw_destroy_plan(plan)
!  data=real(out)

  call rfftf(n,data,wsave)


!write(*,*) 'HIERRFFTF:',N,data(1:N)
!write(*,*) 'HIERFFTW3:',N,real(out(1:N))
!call stop

!  Multiply the 2 transforms together. First multiply the zeroth term
!  for which all imaginary parts are zero.
  data(1)=data(1)*fftfilt(1)
!  Now do the remaining terms. Real and imaginary terms are stored in
!  adjacent elements of the arrays.
  DO I = 2, N - 1, 2
     A = data( I )
     B = data( I + 1 )
     C = fftfilt( I )
     D = fftfilt( I + 1 )

     data( I ) = A*C - B*D
     data( I + 1 ) = B*C + A*D         
  END DO

!  If there are an even number of elements, do the last term, for which
!  the imaginary parts are again zero.
  IF( MOD( N, 2 ) .EQ. 0 ) data( N ) = data( N ) * fftfilt( N )      

!  Now take the inverse FFT.
!  in=data 
!  call dfftw_plan_dft_1d(plan,N,in,out,+1,FFTW_MEASURE)
!  call dfftw_execute_dft(plan, in, out)
!  call dfftw_destroy_plan(plan)
!  data=real(out)
  CALL RFFTB( N, data, WSAVE )      

!  Divide the results by N*SQRT( N ) to take account of the different
!  normalisation of the FFTPACK results.
!  FAC = 1.0/( real( N,kind=4 ) * SQRT( real( N,kind=4 ) ) )

  !use normalization which agrees with IDL
  FAC = 1.0/ real( N,kind=4 )**2


  data=data*FAC
end subroutine convolve
