module hanle_module
  use all_type
  IMPLICIT REAL (A-H,O-Z)
  INTEGER Z,ZMIN,ZMAX

  real(4),dimension(405,405,4) :: xa0_mat,xa3_mat,xa4_mat
  real(4),dimension(405,405,4,0:1) :: xa1_mat,xa2_mat,xa5_mat,xa6_mat,xa7_mat, &
       xa8_mat,xa9_mat,xa10_mat
  real(4),dimension(405,405) :: xa11_mat,xa12_mat,xam1_mat,xam2_mat
  integer(4),dimension(405,405) :: qqp1_mat,qqp2_mat
  integer(4),dimension(405,405,4) :: x41_mat,x42_mat
  real(4),dimension(1000000) :: x0t10a_vec,x0t10b_vec

  integer(4) :: is2,nterm,ntrans,nunk,nemiss
  integer(4),dimension(30) :: lsto2
  real(4),dimension(30,0:20) :: energy
  real(4),dimension(50) :: aesto,fsto,u1sto,u2sto,riosto
  real(4),dimension(50) :: wlsto
  integer(4),dimension(50) :: ntlsto,ntusto
  real(4),dimension(405) :: rnutab
  integer(4),dimension(405) :: ntab,j2tab,jp2tab,ktab,irtab,qtab
  integer(4),dimension(-20:20) :: njlevl,njlevu
  real(4),dimension(-20:20,11) :: autl,autu
  real(4),dimension(-20:20,11,0:20) :: cl,cu
  real(4) :: thetad,chid,gammad
  real(8) :: trasf
  real(8),dimension(maxwl) :: onumvec
  real(4) :: fact(0:301)

!!!  integer(4) :: njoff,auoff(2),coff(3)

  real(4),parameter :: clight=2.997925d10,pigr=3.141592653590d0, &
       tras=pigr/180.e0,twopi=2.e0*pigr,const=0.08386d0

  integer(4) :: q,q2,qp,qp2,qu,qu2,ql,ql2
  integer(4) :: bigq,bigq2,bigqu,bigqu2,bigql,bigql2
  real(4) :: gei(0:2),a(405,405),am(405,405),a0(405,405),b(405)
  integer(4) :: indx(405)
  real(4) :: dr(-1:1,-1:1),di(-1:1,-1:1)
  real(4) :: dsur(0:6,-6:6,-6:6),dsui(0:6,-6:6,-6:6)
  real(4) :: dslr(0:6,-6:6,-6:6),dsli(0:6,-6:6,-6:6)
  real(4) :: tr(0:2,-2:2,0:3),ti(0:2,-2:2,0:3)
  real(4) :: e0(0:20)
  real(4) :: eps(0:3,maxwl),eta(0:3,maxwl)
  real(4) :: stokes(0:3,maxwl)
  real(4),dimension(0:12,0:12,0:12,-12:12) :: rhor,rhoi,rhomr,rhomi,rholr, &
       rholi,rhomlr,rhomli

contains
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine factrl
    integer(2) :: i
    fact(0)=1.
    do i=1,31 
      fact(i)=fact(i-1)*real(i,kind=4)
    end do
  end subroutine factrl
!!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!!
!!! subroutine VOIGT (nwl, A,V,dh,df)
!!! ----------------------
!!! This vectorizable Voigt function is based on the paper by
!!! Hui, Armstrong and Wray, JQSRT 19, 509 (1977). It has been
!!! checked agains the old Landolt & Boernstein standard voigt. Errors
!!! become significant (at the < 1 % level) around the "knee" between
!!! the Doppler core and the damping wings for a smaller than 1.e-3.
!!! Note that, as written here, the routine has no provision for
!!! the trivial case a=0. The normalization is such that the integral
!!! is sqrt(pi); i.e., a plain exp(-x**2) should be used at a=0.
!!! The new Landolt & Boernstein gives a useful expression for the
!!! small but finite a case.
!!! If J equals 1 then this function returns the magnetooptical profile.
!!! This version cannot handle the case of J=1 and a=0 !!!!!!!!!!!!!!!!!!
!!! V & A are dimensionless ( = wavelength/delta_wavelength = frequency/ 
!!! delta_frequency = velocity/delta velocity)
!!!
!!! Coded by: A. Nordlund/16-dec-82.
!!!-----------------------------------------------------------------------------
!!!same as voigt, but DP
  subroutine DVOIGT (nwl, A,V,dh,df)
    IMPLICIT NONE

    integer(2) :: nwl
    REAL(4)     :: A
    real(8), dimension(nwl) :: V
    real(4), dimension(nwl) :: dh,df
    !! local
    COMPLEX(8), dimension(nwl) :: Z
    COMPLEX(8), dimension(nwl)     :: VGT  
    !! ---
    REAL(8) :: A0 = 122.607931777104326
    REAL(8) :: A1 = 214.382388694706425
    REAL(8) :: A2 = 181.928533092181549
    REAL(8) :: A3 = 93.155580458138441
    REAL(8) :: A4 = 30.180142196210589
    REAL(8) :: A5 = 5.912626209773153
    REAL(8) :: A6 = 0.564189583562615
    REAL(8) :: B0 = 122.607931773875350
    REAL(8) :: B1 = 352.730625110963558
    REAL(8) :: B2 = 457.334478783897737
    REAL(8) :: B3 = 348.703917719495792
    REAL(8) :: B4 = 170.354001821091472
    REAL(8) :: B5 = 53.992906912940207
    REAL(8) :: B6 = 10.479857114260399
    !! ---
    IF (abs(A).GE.1e-5) THEN
      Z=CMPLX(A,(V),kind=8)
      VGT=(((((((A6*Z+A5)*Z+A4)*Z+A3)*Z+A2)*Z+A1)*Z+A0) &
           /(((((((Z+B6)*Z+B5)*Z+B4)*Z+B3)*Z+B2)*Z+B1)*Z+B0))

      dh=real(vgt,kind=4)
      df=-aimag(vgt)*0.5
    ELSE
      write(*,*) 'Cannot handle a=0 in subroutine dvoigt'
    END IF
  END subroutine DVOIGT
  !!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine campo(u1,u2,rio,wl,f,hs,barn,w)
    h=hs/958.e0 !solar radius
    sg=1.e0/(1.e0+h)
    sg2=sg*sg
    cg=sqrt(1.e0-sg2)
    cg2=cg*cg
    cg3=cg2*cg
    cfact=cg2*log((1.e0+sg)/cg)/sg
    ca0=1.e0-cg
    ca1=cg-.5d0-.5d0*cfact
    ca2=(cg+2.e0)*(cg-1.e0)/(3.e0*(cg+1.e0))
    x0=(ca0+ca1*u1+ca2*u2)/2.e0
    c0=cg*sg2
    c1=(8.e0*cg3-3.e0*cg2-8.e0*cg+2.e0+(4.e0-3.e0*cg2)*cfact)/8.e0
    c2=(cg-1.e0)*(9.e0*cg3+18.e0*cg2+7.e0*cg-4.e0)/(15.e0*(cg+1.e0))
    w=(c0+c1*u1+c2*u2)/(2.e0*(ca0+ca1*u1+ca2*u2))
    barn=(wl**5)*const*rio*x0*f
    return
  end subroutine campo
  SUBROUTINE COMPROD(cpA,cpB,cpC,cpD,cpE,cpF)
!!!---PERFORMS THE PRODUCT OF TWO COMPLEX NUMBERS:
!!!---(A+iB)*(C+iD)=(E+iF)
!!!---ON INPUT:   A,B,C,D
!!!---ON OUTPUT:  E,F
    cpE=cpA*cpC-cpB*cpD
    cpF=cpA*cpD+cpB*cpC
    RETURN
  END SUBROUTINE COMPROD
  SUBROUTINE COMPRODVEC(N,cpA,cpB,cpC,cpD,cpE,cpF)
!!!---PERFORMS THE PRODUCT OF TWO COMPLEX NUMBERS:
!!!---(A+iB)*(C+iD)=(E+iF)
!!!---ON INPUT:   A,B,C,D
!!!---ON OUTPUT:  E,F
    integer(2) :: N
    REAL(4), DIMENSION(N), INTENT(IN) :: cpC,cpD
    REAL(4), DIMENSION(N), INTENT(OUT) :: cpE,cpF
    cpE=cpA*cpC-cpB*cpD
    cpF=cpA*cpD+cpB*cpC
    RETURN
  END SUBROUTINE COMPRODVEC
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine ROTMATSU(KMAX,ALFA,BETA,RGAMMA,TDR,TDI)
!!!C-----CALCULATES THE ROTATION MATRICES OF ORDER 0, 1, 2,....,KMAX
!!!C-----THE RESULTS ARE GIVEN IN THE MATRICES DR(K,IQ,IQP) AND 
!!!C-----DI(K,IQ,IQP) REPRESENTING THE REAL AND IMAGINARY PART OF THE 
!!!C-----ROTATION MATRIX, RESPECTIVELY.
!!!C-----THE ANGLES ALFA, BETA, AND RGAMMA ARE GIVEN IN RADIANS.
!!!C-----THE MAXIMUM ALLOWED VALUE FOR KMAX IS 6
!!!      IMPLICIT REAL (A-H,O-Z)
    DIMENSION TDR(0:6,-6:6,-6:6), TDI(0:6,-6:6,-6:6)
    tdr=0 ; tdi=0
    do K=0,KMAX  !        ;2
      do IQ=-K,K !        ; 3
        do IQP=-K,K !     ; 4
          A1=ALFA*real(IQ,kind=4)
          A2=RGAMMA*real(IQP,kind=4)
          CA=COS(A1)
          SA=SIN(A1)
          CG=COS(A2)
          SG=SIN(A2)
          BO2=BETA/2.
          CB2=COS(BO2)
          SB2=SIN(BO2)
          IT1=K+IQ
          IT2=K-IQP
          ITMAX=IT1
          IF(IT2.LT.ITMAX) ITMAX=IT2
          ITMIN=0
          IT3=IQ-IQP
          IF(IT3.GT.0) ITMIN=IT3
          REDM=0.
          IF(BETA.ne.0.) then !  ;GO TO 6
            do IT=ITMIN,ITMAX ! ;5 
              S=1.
              IF(MOD(IT,2).NE.0)  S=-1.
              IE1=2*(K-IT)+IQ-IQP
              IE2=2*IT+IQP-IQ
              DEN=FACT(IT1-IT)*FACT(IT2-IT)*FACT(IT)*FACT(IT-IT3)
              REDM=REDM+S*(CB2**IE1)*(SB2**IE2)/DEN
            end do           !     ; 5    CONTINUE
            REDM=REDM*SQRT(FACT(K+IQ)*FACT(K-IQ)*FACT(K+IQP)*FACT(K-IQP))
          else !       ; 6    CONTINUE
            IF(IQ.EQ.IQP) REDM=1.
          end IF
          TDR(K,IQ,IQP)=REDM*(CA*CG-SA*SG)
          TDI(K,IQ,IQP)=-REDM*(CA*SG+SA*CG)
        end do       !             ; 4    CONTINUE
      end do         !             ; 3    CONTINUE
    end do       !                 ; 2    CONTINUE
  END subroutine ROTMATSU
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine TKP(K,IP,TR,TI)
!!!-----CALCULATES THE SYMBOL TKP------------------------
!!!     IMPLICIT REAL (A-H,O-Z)
    DIMENSION TR(0:3),TI(0:3)
    tr=0 ; ti=0

    IF (K.EQ.0.AND.IP.EQ.0) then 
      TR(0)=1.e0
      RETURN
    else IF (K.EQ.1.AND.IP.EQ.0) then 
      TR(3)=SQRT(1.5D0)
      RETURN
    else IF (K.EQ.2.AND.IP.EQ.0) then 
      TR(0)=SQRT(0.5D0)
      RETURN
    else IF (K.EQ.2.AND.IP.EQ.2) then 
      TA=SQRT(3.e0)/2.e0
      TR(1)=-TA
      TI(2)=-TA
      RETURN
    else IF (K.EQ.2.AND.IP.EQ.-2) then 
      TA=SQRT(3.e0)/2.e0
      TR(1)=-TA
      TI(2)=TA
    end IF
  END subroutine TKP
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine TKQ2(ALFA,BETA,RGAMMA,ALFAP,BETAP,RGAMMAP,TR,TI)
!!!C-----CALCULATES THE TENSOR TKQ FOR A DOUBLE ROTATION
!!!C-----ON INPUT: ALFA,BETA,RGAMMA=EULER ANGLES IN RADIANS OF FIRST ROT.
!!!C-----        : ALFAP,BETAP,RGAMMAP=SAME FOR SECOND ROTATION
!!!C-----ON OUTPUT: TR, TI= REAL AND IMAG PARTS OF TENSOR TKQ
!!!      IMPLICIT REAL (A-H,O-Z)
    DIMENSION TR(0:2,-2:2,0:3),TI(0:2,-2:2,0:3),TDR(0:6,-6:6,-6:6), &
         TDI(0:6,-6:6,-6:6),DRP(0:6,-6:6,-6:6),&
         DIP(0:6,-6:6,-6:6),TPR(0:3),TPI(0:3)
    tpr=0 ; tpi=0
    tdr=0 ; tdi=0
    drp=0 ; dip=0
    call ROTMATSU(2,ALFA,BETA,RGAMMA,TDR,TDI)
    call ROTMATSU(2,ALFAP,BETAP,RGAMMAP,DRP,DIP)
    do I=0,3 !          ; 20
      do K=0,2 !          ;1
        do IQ=-K,K !    ;2
          TR(K,IQ,I)=0.
          TI(K,IQ,I)=0.
          do IP=-K,K !    ;3
            call TKP(K,IP,TPR,TPI)
            do IR=-K,K !  ;4
              call COMPROD(TPR(I),TPI(I),TDR(K,IP,IR),TDI(K,IP,IR),E,F)
              call COMPROD(E,F,DRP(K,IR,IQ),DIP(K,IR,IQ),G,H)
              TR(K,IQ,I)=TR(K,IQ,I)+G
              TI(K,IQ,I)=TI(K,IQ,I)+H
            end do        !        ; 4    CONTINUE
          end do  !                ; 3    CONTINUE
        end do !                   ; 2    CONTINUE
      end do !                     ; 1    CONTINUE
    end do!                       ; 20   CONTINUE
  END subroutine TKQ2
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION imaxloc_d(arr)
    REAL(4), DIMENSION(:), INTENT(IN) :: arr
    INTEGER(4) :: imaxloc_d
    INTEGER(4), DIMENSION(1) :: imax
    imax=maxloc(arr(:))
    imaxloc_d=imax(1)
  END FUNCTION imaxloc_d
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION outerprod_d(a,b)
    REAL(4), DIMENSION(:), INTENT(IN) :: a,b
    REAL(4), DIMENSION(size(a),size(b)) :: outerprod_d
    outerprod_d = spread(a,dim=2,ncopies=size(b)) * &
         spread(b,dim=1,ncopies=size(a))
  END FUNCTION outerprod_d
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION outerprod_dn(n,a,b)
    integer(4), INTENT(IN) :: n
    REAL(4), DIMENSION(n), INTENT(IN) :: a,b
    REAL(4), DIMENSION(n,n) :: outerprod_dn
    outerprod_dn = spread(a,dim=2,ncopies=n) * &
         spread(b,dim=1,ncopies=n)
  END FUNCTION outerprod_dn
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	SUBROUTINE swap_rv(a,b)
	REAL(4), DIMENSION(:), INTENT(INOUT) :: a,b
	REAL(4), DIMENSION(SIZE(a)) :: dum
	dum=a
	a=b
	b=dum
	END SUBROUTINE swap_rv
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	SUBROUTINE ludcmp(la,ln,lindx,ld)
    IMPLICIT NONE
    REAL(4), DIMENSION(ln,ln), INTENT(INOUT) :: la
    INTEGER(4), DIMENSION(ln), INTENT(OUT) :: lindx
    REAL(4), INTENT(OUT) :: ld
    REAL(4), DIMENSION(ln) :: vv,dum
    REAL(4), PARAMETER :: TINY=1.0d-20
    INTEGER(4) :: j,ln,imax,j4,mloc(1)
    REAL(4), DIMENSION(ln,ln) :: la1
    ld=1.0
    vv=maxval(abs(la),dim=2)
    if (any(vv == 0.0)) then
      write(*,*) 'singular matrix in ludcmp'
      stop
    end if
    vv=1.0d0/vv
    do j=1,ln
!      imax=(j-1)+(imaxloc_d(vv(j:ln)*abs(la(j:ln,j))))
      mloc=maxloc(vv(j:ln)*abs(la(j:ln,j)))
      imax=(j-1)+mloc(1)
      if (j /= imax) then
        dum=la(imax,:) ; la(imax,:)=la(j,:) ; la(j,:)=dum
!        call swap_rv(la(imax,:),la(j,:))
        ld=-ld
        vv(imax)=vv(j)
      end if
      lindx(j)=imax
      if (la(j,j) == 0.0) la(j,j)=TINY
      if (j+1.le.ln) then 
        la(j+1:ln,j)=la(j+1:ln,j)/la(j,j)
        la(j+1:ln,j+1:ln)=&
             la(j+1:ln,j+1:ln)-outerprod_d(la(j+1:ln,j),la(j,j+1:ln))

!             la(j+1:ln,j+1:ln)-outerprod_dn(ln-j,la(j+1:ln,j),la(j,j+1:ln))
!             la(j+1:ln,j+1:ln)-spread(la(j+1:ln,j),dim=2,ncopies=ln-j)*&
!             spread(la(j,j+1:ln),dim=1,ncopies=ln-j)
!write(*,*) size(la(j+1:ln,j)),ln-j
    end if
    end do
	END SUBROUTINE ludcmp
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	SUBROUTINE lubksb(la,ln,lindx,lb)
    IMPLICIT NONE
    REAL(4), DIMENSION(ln,ln), INTENT(IN) :: la
    INTEGER(4), DIMENSION(ln), INTENT(IN) :: lindx
    REAL(4), DIMENSION(ln), INTENT(INOUT) :: lb
    INTEGER(4) :: i,ln,ii,ll
    REAL(4) :: summ
    ii=0
    do i=1,ln
      ll=lindx(i)
      summ=lb(ll)
      lb(ll)=lb(i)
      if (ii /= 0) then
        summ=summ-dot_product(la(i,ii:i-1),lb(ii:i-1))
      else if (summ /= 0.0) then
        ii=i
      end if
      lb(i)=summ
    end do
    do i=ln-1,1,-1   
      lb(i) = (lb(i)-dot_product(la(i,i+1:ln),lb(i+1:ln)))/la(i,i)
    end do
	END SUBROUTINE lubksb
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine ROTMATK(K,ALFA,BETA,RGAMMA,DR,DI)
!!! c-----an adaptation of rotmatsu
!!! C-----CALCULATES THE ROTATION MATRICES OF ORDER K
!!! C-----THE RESULTS ARE GIVEN IN THE MATRICES DR(IQ,IQP) AND 
!!! C-----DI(IQ,IQP) REPRESENTING THE REAL AND IMAGINARY PART OF THE 
!!! C-----ROTATION MATRIX, RESPECTIVELY.
!!! C-----THE ANGLES ALFA, BETA, AND RGAMMA ARE GIVEN IN RADIANS.
!!!    IMPLICIT REAL (A-H,O-Z)
    DIMENSION DR(-1:1,-1:1), DI(-1:1,-1:1)
    dr=0
    di=0
!!!loop optimization possible!  
    do IQ=-K,K 
      do IQP=-K,K 
        A1=ALFA*Real(IQ,kind=4)
        A2=RGAMMA*Real(IQP,kind=4)
        CA=COS(A1)
        SA=SIN(A1)
        CG=COS(A2)
        SG=SIN(A2)
        BO2=BETA/2.
        CB2=COS(BO2)
        SB2=SIN(BO2)
        IT1=K+IQ
        IT2=K-IQP
        ITMAX=IT1
        IF(IT2.LT.ITMAX) ITMAX=IT2
        ITMIN=0
        IT3=IQ-IQP
        IF(IT3.GT.0) ITMIN=IT3
        REDM=0.
        IF(BETA.ne.0.) then
          do IT=ITMIN,ITMAX 
            S=1.
            IF(MOD(IT,2).NE.0) S=-1.
            IE1=2*(K-IT)+IQ-IQP
            IE2=2*IT+IQP-IQ
            DEN=FACT(IT1-IT)*FACT(IT2-IT)*FACT(IT)*FACT(IT-IT3)
            REDM=REDM+S*(CB2**IE1)*(SB2**IE2)/DEN
          end do
          REDM=REDM*SQRT(FACT(K+IQ)*FACT(K-IQ)*FACT(K+IQP)*FACT(K-IQP))
        else
          IF(IQ.EQ.IQP) REDM=1.
        end IF
        DR(IQ,IQP)=REDM*(CA*CG-SA*SG)
        DI(IQ,IQP)=-REDM*(CA*SG+SA*CG)
      end do
    end do
  END subroutine ROTMATK
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION W3JS(J1,J2,J3,M1,M2,M3)
    IMPLICIT REAL (A-H,O-Z)
    INTEGER Z,ZMIN,ZMAX
    W3JS=0.0
    IF(M1+M2+M3.NE.0) GOTO 1000
    IA=J1+J2
    IF(J3.GT.IA) GOTO 1000
    IB=J1-J2
    IF(J3.LT.IABS(IB)) GOTO 1000
    JSUM=J3+IA
    IC=J1-M1
    ID=J2-M2
!!!     IF(MOD(JSUM,2).NE.0) GOTO 1000
!!!     IF(MOD(IC,2).NE.0) GOTO 1000
!!!     IF(MOD(ID,2).NE.0) GOTO 1000
    IF(IABS(M1).GT.J1) GOTO 1000
    IF(IABS(M2).GT.J2) GOTO 1000
    IF(IABS(M3).GT.J3) GOTO 1000
    IE=J3-J2+M1
    IF=J3-J1-M2
    ZMIN=MAX0(0,-IE,-IF)
    IG=IA-J3
    IH=J2+M2
    ZMAX=MIN0(IG,IH,IC)
    CC=0.0
    DO Z=ZMIN,ZMAX,2
      DENOM=FACT(Z/2)*FACT((IG-Z)/2)*FACT((IC-Z)/2)*&
           FACT((IH-Z)/2)*FACT((IE+Z)/2)*FACT((IF+Z)/2)
      IF(MOD(Z,4).NE.0) DENOM=-DENOM
      CC=CC+1.0/DENOM
    END DO
    CC1=FACT(IG/2)*FACT((J3+IB)/2)*FACT((J3-IB)/2)/FACT((JSUM+2)/2)
    CC2=FACT((J1+M1)/2)*FACT(IC/2)*FACT(IH/2)*&
         FACT(ID/2)*FACT((J3-M3)/2)*FACT((J3+M3)/2)
    CC=CC*SQRT(CC1*CC2)
    IF(MOD(IB-M3,4).NE.0) CC=-CC
    W3JS=CC
    IF(ABS(W3JS).LT.1.0E-8) W3JS=0.0
1000 RETURN
  END FUNCTION W3JS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION W6JS(J1,J2,J3,L1,L2,L3)
    IMPLICIT REAL (A-H,O-Z)
    INTEGER W,WMIN,WMAX
    INTEGER SUM1,SUM2,SUM3,SUM4
    W6JS=0.0
    IA=J1+J2
    IF(IA.LT.J3) GOTO 1000
    IB=J1-J2
    IF(IABS(IB).GT.J3) GOTO 1000
    IC=J1+L2
    IF(IC.LT.L3) GOTO 1000
    ID=J1-L2
    IF(IABS(ID).GT.L3) GOTO 1000
    IE=L1+J2
    IF(IE.LT.L3) GOTO 1000
    IF=L1-J2
    IF(IABS(IF).GT.L3) GOTO 1000
    IG=L1+L2
    IF(IG.LT.J3) GOTO 1000
    IH=L1-L2
    IF(IABS(IH).GT.J3) GOTO 1000
    SUM1=IA+J3
    SUM2=IC+L3
    SUM3=IE+L3
    SUM4=IG+J3
!!!     IF(MOD(SUM1,2).NE.0) GOTO 1000
!!!     IF(MOD(SUM2,2).NE.0) GOTO 1000
!!!     IF(MOD(SUM3,2).NE.0) GOTO 1000
!!!     IF(MOD(SUM4,2).NE.0) GOTO 1000
    WMIN=MAX0(SUM1,SUM2,SUM3,SUM4)
    II=IA+IG
    IJ=J2+J3+L2+L3
    IK=J3+J1+L3+L1
    WMAX=MIN0(II,IJ,IK)
    OMEGA=0.0
    DO W=WMIN,WMAX,2
      DENOM=FACT((W-SUM1)/2)*FACT((W-SUM2)/2)*FACT((W-SUM3)/2) &
           *FACT((W-SUM4)/2)*FACT((II-W)/2)*FACT((IJ-W)/2) &
           *FACT((IK-W)/2)
      IF(MOD(W,4).NE.0) DENOM=-DENOM
      OMEGA=OMEGA+FACT(W/2+1)/DENOM
    END DO
    THETA1=FACT((IA-J3)/2)*FACT((J3+IB)/2)*FACT((J3-IB)/2)/FACT(SUM1/2+1)
    THETA2=FACT((IC-L3)/2)*FACT((L3+ID)/2)*FACT((L3-ID)/2)/FACT(SUM2/2+1)
    THETA3=FACT((IE-L3)/2)*FACT((L3+IF)/2)*FACT((L3-IF)/2)/FACT(SUM3/2+1)
    THETA4=FACT((IG-J3)/2)*FACT((J3+IH)/2)*FACT((J3-IH)/2)/FACT(SUM4/2+1)
    THETA=THETA1*THETA2*THETA3*THETA4
    W6JS=OMEGA*SQRT(THETA)
    IF(ABS(W6JS).LT.1.0E-8) W6JS=0.0
1000 RETURN
  END FUNCTION W6JS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION W9JS(J1,J2,J3,J4,J5,J6,J7,J8,J9)
    IMPLICIT REAL (A-H,O-Z)
    KMIN=ABS(J1-J9)
    KMAX=J1+J9
    I=ABS(J4-J8)
    IF(I.GT.KMIN) KMIN=I
    I=J4+J8
    IF(I.LT.KMAX) KMAX=I
    I=ABS(J2-J6)
    IF(I.GT.KMIN) KMIN=I
    I=J2+J6
    IF(I.LT.KMAX) KMAX=I
    X=0.
    DO K=KMIN,KMAX,2
      S=1.
      IF(MOD(K,2).NE.0) S=-1.
      X1=W6JS(J1,J9,K,J8,J4,J7)
      X2=W6JS(J2,J6,K,J4,J8,J5)
      X3=W6JS(J1,J9,K,J6,J2,J3)
      X=X+S*X1*X2*X3*real(K+1,kind=4)
    END DO
    W9JS=X
    RETURN
  END FUNCTION W9JS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine gamma(l2,is2,j2,jp2,g)
    g=0.e0
    if(j2.eq.jp2) g=g+sqrt(real(j2*(j2+2)*(j2+1),kind=4)/4.e0)
    x1=1.e0
    if(mod(2+l2+is2+j2,4).ne.0) x1=-1.e0
    x2=sqrt(real((j2+1)*(jp2+1)*is2*(is2+2)*(is2+1),kind=4)/4.e0)
    x3=w6js(j2,jp2,2,is2,is2,l2)
    g=g+x1*x2*x3
    return
  end subroutine gamma
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine bigpar(l2,is2,k2,kp2,j2,jp2,js2,jt2,x)
    x=0.e0
    if(jp2.ne.jt2.and.j2.ne.js2) return
    if(jp2.eq.jt2) then
      if(.not.(iabs(j2-js2).gt.2.or.j2+js2.eq.0)) then
        call gamma(l2,is2,j2,js2,g)
        x1=g
        x2=w6js(k2,kp2,2,js2,j2,jp2)
        x=x1*x2
      end if
    end if
    if(j2.ne.js2) return
    if(iabs(jt2-jp2).gt.2.or.jt2+jp2.eq.0) return
    call gamma(l2,is2,jt2,jp2,g)
    x1=g
    x2=1.e0
    if(mod(k2-kp2,4).ne.0) x2=-1.e0
    x3=w6js(k2,kp2,2,jt2,jp2,j2)
    x=x+x1*x2*x3
    return
  end subroutine bigpar
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine tqli(d,e,n,np,z)
!!!c---------------------------------------------------------------------------
!!!c-----See "Numerical Recipes"
!!!c---------------------------------------------------------------------------
    dimension d(np),e(np),z(np,np)
    if(n.gt.1) then
      do i=2,n 
        e(i-1)=e(i)
      end do
      e(n)=0.e0
      do l=1,n
        iter=0
1       do m=l,n-1
          dd=abs(d(m))+abs(d(m+1))
          if(abs(e(m))+dd.eq.dd) go to 2
        end do
        m=n
2       if(m.ne.l) then
          if(iter.eq.30) Pause 'too many iterations'
          iter=iter+1
          g=(d(l+1)-d(l))/(2.e0*e(l))
          r=sqrt(g**2+1.e0)
          g=d(m)-d(l)+e(l)/(g+sign(r,g))
          s=1.e0
          c=1.e0
          p=0.e0
          do i=m-1,l,-1
            f=s*e(i)
            btq=c*e(i)
            if(abs(f).ge.abs(g)) then
              c=g/f
              r=sqrt(c**2+1.e0)
              e(i+1)=f*r
              s=1.e0/r
              c=c*s
            else
              s=f/g
              r=sqrt(s**2+1.e0)
              e(i+1)=g*r
              c=1.e0/r
              s=s*c
            endif
            g=d(i+1)-p
            r=(d(i)-g)*s+2.e0*c*btq
            p=s*r
            d(i+1)=g+p
            g=c*r-btq
            do k=1,n
              f=z(k,i+1)
              z(k,i+1)=s*z(k,i)+c*f
              z(k,i)=c*z(k,i)-s*f
            end do
          end do
          d(l)=d(l)-p
          e(l)=g
          e(m)=0.e0
          go to 1
        end if
      end do
    end if
    return
  end subroutine tqli
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine paschen(l2,is2,e0,bp,njlev,e,c)
    real(4) :: e0(0:20),e(-20:20,11),c(-20:20,11,0:20),d(11),ud(11),z(11,11)
    integer(4) :: njlev(-20:20)
    e=0 ; c=0 ; d=0 ; ud=0 ; z=0
    bb=bp*4.6686d-5
    j2min=abs(l2-is2)
    j2max=l2+is2
    do m2=-j2max,j2max,2 !!! ; 10
      j2a=abs(m2)
      if(j2a.lt.j2min) j2a=j2min
      j2b=j2max
      nj=1+(j2b-j2a)/2
      njlev(m2)=nj
      do j=1,nj !!!         ; 20
        j2vero=j2a+2*(j-1)
        d(j)=e0(j2vero) 
        x1=1.e0
        if(mod(2*j2vero+l2+is2+m2,4).ne.0) x1=-1.e0
        x2=real(j2vero+1,kind=4)*sqrt(real(is2*(is2+2)*(is2+1),kind=4)/4.e0)
        x3=w3js(j2vero,j2vero,2,-m2,m2,0)
        x4=w6js(j2vero,j2vero,2,is2,is2,l2)
        d(j)=d(j)+bb*(real(m2,kind=4)/2.e0+x1*x2*x3*x4)
      end do   !!!                   ; 20   continue
      if(nj.ne.1) then !!!   ;40
        do jp=2,nj !!!      ; 30
          jp2vero=j2a+2*(jp-1)
          j2vero=jp2vero-2
          x1=1.e0
          if(mod(j2vero+jp2vero+l2+is2+m2,4).ne.0) x1=-1.e0
          x2=sqrt(real((j2vero+1)*(jp2vero+1)*is2*(is2+2)*(is2+1),&
               kind=4)/4.e0)
          x3=w3js(j2vero,jp2vero,2,-m2,m2,0)
          x4=w6js(j2vero,jp2vero,2,is2,is2,l2)
          ud(jp-1)=bb*x1*x2*x3*x4
        end do                !!!    ; 30   continue     
      end if  !!!                     ; 40   continue 

      z(:,:)=0.e0 
      do i=1,nj 
        z(i,i)=1.e0
      end do
      call tqli(d,ud,nj,11,z)

      do jsmall=1,nj !!!  ; 70
        e(m2,jsmall)=d(jsmall)
        do jaux=1,nj !!!   ; 80
          j2=j2a+2*(jaux-1)
          c(m2,jsmall,j2)=z(jaux,jsmall)
        end do                !!!    ; 80   continue
      end do   !!!                   ; 70   continue
    end do  !!!                     ; 10   continue

  end subroutine paschen

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine hanle_init(line,obs_par)
    use all_type
!!!    implicit none
    implicit real (a-h,o-z)
    implicit integer(4) (i-n)
    type (linetyp) :: line
    type (obstyp) :: obs_par
    real(4) :: const
    type modtyp
      sequence
      integer(1) :: linid(idlen)
      real(4) :: emvec(3)
    end type modtyp
    type (modtyp) :: oldmod,tmod
    character(LEN=12) :: linid
    logical :: recalc

    write(*,*) 'HANLE_INIT start.'
    if (line%hanle%ntrans.gt.4) then
      write(*,*) 'Set ntr to larger value at begin of this module.'
      stop
    end if

!!!angles for emission vector should be
!!!already defined
    thetad=obs_par%heliocentric_angle
    chid=0.e0              
    gammad=obs_par%posq2limb

    tmod%linid=line%id
    tmod%emvec=real( (/ thetad, chid, gammad /),kind=4)
    recalc=.false.
    recalc=(recalc.or.(.not.all(tmod%linid.eq.oldmod%linid)).or.&
         any(tmod%emvec.ne.oldmod%emvec))


    if (recalc) then 

      oldmod=tmod
      write(*,'(a,3f8.2)') 'Emission vector = ',thetad,chid,gammad  
      write(*,*) 'Recalculating the matrices...'

      call factrl
      write(unit=linid,fmt='(12a)') char(line%id)
      if (linid.eq.'He1 10829.09') then
        nemiss=1 
      else 
        write(*,*) 'unknown line for Hanle computation.'
        stop
      end if

      xa0_mat=0
      xa1_mat=0
      xa2_mat=0
      xa3_mat=0
      xa4_mat=0
      xa5_mat=0
      xa6_mat=0
      xa7_mat=0
      xa8_mat=0
      xa9_mat=0
      xa10_mat=0
      xa11_mat=0
      xa12_mat=0
      x0t10a_vec=0
      x0t10b_vec=0



      is2=line%hanle%is2
      nterm=line%hanle%nterm
      ntrans=line%hanle%ntrans
      lsto2=line%hanle%lsto2
      energy=line%hanle%energy 
      aesto=line%hanle%aesto
      fsto=line%hanle%fsto
      ntlsto=line%hanle%ntlsto
      ntusto=line%hanle%ntusto
      u1sto=line%hanle%u1sto
      u2sto=line%hanle%u2sto
      riosto=line%hanle%riosto
      wlsto=line%hanle%wlsto
      m=0
      do i=1,nterm 
        jmin2=abs(is2-lsto2(i))
        jmax2=is2+lsto2(i)
        do j2=jmin2,jmax2,2
          do jp2=j2,jmax2,2 
            rnujjp=clight*(energy(i,j2)-energy(i,jp2))
            kmin=(jp2-j2)/2
            kmax=(jp2+j2)/2
            do k=kmin,kmax
              do q=-k,k
                if (.not.((j2.eq.jp2).and.(q.lt.0))) then !!! goto,l55
                  do ir=1,2
                    if (.not.((j2.eq.jp2).and.(q.eq.0).and.&
                         (ir.eq.2))) then !!! begin ; l60
                      m=m+1
                      ntab(m)=line%hanle%ntm(i)
                      j2tab(m)=j2
                      jp2tab(m)=jp2
                      ktab(m)=k
                      qtab(m)=q
                      irtab(m)=ir
                      rnutab(m)=rnujjp
                    end if !!!;l60
                  end do !!!;60
                end if             !!!;l55:
              end do               !!!55
            end do                 !!! 50
          end do                   !!! 40
        end do                     !!! 30 
      end do                       !!!20
      nunk=m  
!!!c----------------------------------------------------------------------
!!!c-----si introducono i parametri isti,idep
!!!c-----isti =1 se si vogliono considerare gli effeti di stimolazione
!!!c-----idep =1 se si vogliono introdurre le rates depolarizzanti (solo
!!!c-----        per il livello fondamentale e tutte uguali fra loro
!!!c-----        indipendentemente dal valore di K, con K.ne.0)
!!!c----------------------------------------------------------------------
      isti=1
      idep=0
      imag=1
      do nt=1,ntrans !!!do begin    ;2000
        ae=aesto(nt)
        nterml=ntlsto(nt)
        ntermu=ntusto(nt)
        lu2=lsto2(ntermu)
        ll2=lsto2(nterml)
!!!c----------------------------------------------------------------------
!!!c-----Relaxation rates dovute all'emissione spontanea
!!!c----------------------------------------------------------------------
        do i=1,nunk 
          if (ntab(i).eq.ntermu) then 
            a(i,i)=a(i,i) -ae
            xa0_mat(i,i,nt)=-ae
          end if
        end do
!!!c----------------------------------------------------------------------
!!!c-----Transfer rates dovute all'assorbimento
!!!c----------------------------------------------------------------------
        do i=1,nunk !!!do begin     ;200
          if (ntab(i).eq.ntermu) then 
            ju2=j2tab(i)
            jup2=jp2tab(i)
            ku=ktab(i)
            ku2=ku*2
            qu=qtab(i)
            qu2=qu*2
            iru=irtab(i)
            do j=1,nunk !!!210
              if(ntab(j).eq.nterml) then 
                jl2=j2tab(j)
                if (.not.((abs(ju2-jl2).gt.2).or.(ju2+jl2.eq.0))) then 
                  jlp2=jp2tab(j)
                  if (.not.((abs(jup2-jlp2).gt.2).or.&
                       (jup2+jlp2.eq.0))) then
                    ql=qtab(j)
                    ql2=ql*2
                    if (qu2.eq.ql2) then 
                      irl=irtab(j)
                      if(iru.eq.irl) then 
                        kl=ktab(j)
                        kl2=kl*2
                        do kr=0,2,2 !!!220
                          kr2=kr*2
                          if (.not.((kr.gt.ku+kl).or.&
                               (kr.lt.abs(ku-kl)))) then
                            x1=real(lu2+1,kind=4)
                            x2=sqrt(real(3*(ju2+1)*(jup2+1)*(jl2+1)*(jlp2+1)*(ku2+1)*(kl2+1)*(kr2+1),kind=4))
                            x3=1.e0
                            if (mod(kl2+ql2+jlp2-jl2,4).ne.0) &
                                 x3=-1.e0
                            x4=w9js(ju2,jl2,2,jup2,jlp2,2,ku2,kl2,kr2)
                            x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                            x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                            x7=w3js(ku2,kl2,kr2,-qu2,ql2,0)
!!!x0=x1*x2*x3*x4*x5*x6*x7*ae*gei(kr)
                            xa1_mat(i,j,nt,kr/2)=x1*x2*x3*x4*x5*x6*x7
!!!a(i,j)=a(i,j)+x0
                          end if
                        end do    !!!220
                      end if
                    end if
                  end if
                end if
              end if
            end do                !!!!!!!!!210
!!!!!!!!!----------------------------------------------------------------------
!!!!!!!!!-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
!!!c-----e per i rho^k_q(j,j') con j=j' e q < 0)
!!!c----------------------------------------------------------------------
            do j=1,nunk !!!230
              if (ntab(j).eq.nterml) then 
                if (.not.(j2tab(j).eq.jp2tab(j).and.&
                     (qtab(j).eq.0))) then 
                  jl2=jp2tab(j)
                  if (.not.((abs(ju2-jl2).gt.2).or.(ju2+jl2.eq.0))) then
                    jlp2=j2tab(j)
                    if (.not.((abs(jup2-jlp2).gt.2).or.&
                         (jup2+jlp2.eq.0))) then
                      ql=-qtab(j)
                      ql2=ql*2
                      if (qu2.eq.ql2) then
                        irl=irtab(j)
                        if (iru.eq.irl) then
                          kl=ktab(j)
                          kl2=kl*2
                          do kr=0,2,2 !!!begin ; 240
                            kr2=kr*2
                            if (.not.((kr.gt.ku+kl).or.&
                                 (kr.lt.abs(ku-kl)))) then
                              x1=real(lu2+1,kind=4)
                              x2=sqrt(real(3*(ju2+1)*(jup2+1)* &
                                   (jl2+1)*(jlp2+1)*(ku2+1)* &
                                   (kl2+1)*(kr2+1),kind=4))
                              x3=1.e0
                              if (mod(kl2+ql2+jlp2-jl2,4).ne.0) &
                                   x3=-1.e0
                              x4=w9js(ju2,jl2,2,jup2,jlp2,2,ku2,kl2,kr2)
                              x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                              x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                              x7=w3js(ku2,kl2,kr2,-qu2,ql2,0)
!!!x0=x1*x2*x3*x4*x5*x6*x7*ae*gei(kr)
                              sign=1.e0
                              if (mod(jl2-jlp2-ql2,4).ne.0) &
                                   sign=-1.e0
                              if (irl.eq.1) sign0=sign
                              if (irl.eq.2) sign0=-sign
!!!a(i,j)=a(i,j)+sign0*x0
                              xa2_mat(i,j,nt,kr/2)=&
                                   sign0*x1*x2*x3*x4*x5*x6*x7
                            end if
                          end do
                        end if
                      end if
                    end if
                  end if
                end if
              end if
            end do
          end if !!!ntab.eq.ntermu
        end do !!!200
!!!c----------------------------------------------------------------------
!!!c-----Transfer rates dovute all'emissione spontanea
!!!c----------------------------------------------------------------------
        do i=1,nunk !!!begin    ;300
          if (ntab(i).eq.nterml) then
            jl2=j2tab(i)
            jlp2=jp2tab(i)
            kl=ktab(i)
            kl2=kl*2
            ql=qtab(i)
            ql2=ql*2
            irl=irtab(i)
            do j=1,nunk !!!begin ; 310
              if (ntab(j).eq.ntermu) then
                ju2=j2tab(j)
                if (.not.((abs(ju2-jl2).gt.2).or.(ju2+jl2.eq.0))) then
                  jup2=jp2tab(j)
                  if (.not.((abs(jup2-jlp2).gt.2).or.&
                       (jup2+jlp2.eq.0))) then
                    iru=irtab(j)
                    if (iru.eq.irl) then
                      ku=ktab(j)
                      if(kl.eq.ku) then 
                        qu=qtab(j)
                        if(ql.eq.qu) then
                          x1=real(lu2+1,kind=4)
                          x2=sqrt(real((jl2+1)*(jlp2+1)*(ju2+1)*&
                               (jup2+1),kind=4))
                          x3=1.e0
                          if (mod(2+kl2+jlp2+jup2,4).ne.0) & 
                               x3=-1.e0
                          x4=w6js(jl2,jlp2,kl2,jup2,ju2,2)
                          x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                          x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                          x0=x1*x2*x3*x4*x5*x6*ae
!!!                                     a(i,j)=a(i,j)+x0
                          xa3_mat(i,j,nt)=x0
                        end if
                      end if
                    end if
                  end if
                end if
              end if
            end do
!!!c----------------------------------------------------------------------
!!!c-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
!!!c-----e per i rho^k_q(j,j') con j=j' e q < 0)
!!!c----------------------------------------------------------------------
            do j=1,nunk !!! begin ; 330
              if (ntab(j).eq.ntermu) then
                if (.not.((j2tab(j).eq.jp2tab(j)).and.&
                     (qtab(j).eq.0))) then 
                  ju2=jp2tab(j)
                  if (.not.((abs(ju2-jl2).gt.2).or.(ju2+jl2.eq.0))) then
                    jup2=j2tab(j)
                    if (.not.((abs(jup2-jlp2).gt.2).or.&
                         (jup2+jlp2.eq.0))) then
                      iru=irtab(j)
                      if(iru.eq.irl) then
                        ku=ktab(j)
                        if(kl.eq.ku) then
                          qu=-qtab(j)
                          qu2=qu*2
                          if(ql.eq.qu) then
                            x1=real(lu2+1,kind=4)
                            x2=sqrt(real((jl2+1)*(jlp2+1)*&
                                 (ju2+1)*(jup2+1),kind=4))
                            x3=1.e0
                            if (mod(2+kl2+jlp2+jup2,4).ne.0) &
                                 x3=-1.e0
                            x4=w6js(jl2,jlp2,kl2,jup2,ju2,2)
                            x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                            x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                            x0=x1*x2*x3*x4*x5*x6*ae
                            sign=1.e0
                            if (mod(ju2-jup2-qu2,4).ne.0) sign=-1.e0
                            if (iru.eq.1) sign0=sign
                            if (iru.eq.2) sign0=-sign
!!!                                        a(i,j)=a(i,j)+sign0*x0
                            xa4_mat(i,j,nt)=sign0*x0
                          end if
                        end if
                      end if
                    end if
                  end if
                end if
              end if
            end do
          end if
        end do !!!300
!!!c----------------------------------------------------------------------
!!!;c-----Relaxation rates dovute all'assorbimento
!!!c----------------------------------------------------------------------

        do i=1,nunk !!! begin     ; 400
          if (ntab(i).eq.nterml) then 
            j2=j2tab(i)
            jp2=jp2tab(i)
            k=ktab(i)
            k2=k*2
            q=qtab(i)
            q2=q*2
            ir=irtab(i)
            do j=1,nunk !!!begin ; 410
              if(ntab(j).eq.nterml) then
                js2=j2tab(j)
                jt2=jp2tab(j)
                if (.not.((js2.ne.j2).and.(jt2.ne.jp2))) then
                  kp=ktab(j)
                  kp2=kp*2
                  qp=qtab(j)
                  if (q.eq.qp) then
                    qp2=qp*2
                    irp=irtab(j)
                    if (ir.eq.irp) then
                      do kr=0,2,2 !!! begin ; 420
                        kr2=kr*2
                        if(kr.le.ll2) then
                          if (.not.((kr.gt.k+kp).or.&
                               (kr.lt.abs(k-kp)))) then
                            x1=real(lu2+1,kind=4)
                            x2=sqrt(real(3*(k2+1)*(kp2+1)*&
                                 (kr2+1),kind=4))
                            x3=1.e0
                            if (mod(2+lu2-is2+j2+qp2,4).ne.0) &
                                 x3=-1.e0
                            x4=w6js(ll2,ll2,kr2,2,2,lu2)
                            x5=w3js(k2,kp2,kr2,q2,-qp2,0)
                            x6=x1*x2*x3*x4*x5
                            y0=0.e0
!!!c--------------------------------------------------------------------
!!!c-----questa parte si calcola solo se j e` uguale a js
!!!c--------------------------------------------------------------------
                            if (j2.eq.js2) then 
                              if (.not.((kr2.gt.jp2+jt2).or.&
                                   (kr2.lt.abs(jp2-jt2)))) then
                                y1=sqrt(real((jp2+1)*(jt2+1),&
                                     kind=4))
                                y2=w6js(ll2,ll2,kr2,jt2,jp2,is2)
                                y3=w6js(k2,kp2,kr2,jt2,jp2,j2)
                                y0=y1*y2*y3
                              end if
                            end if
!!!c---------------------------------------------------------------------
!!!c-----questa parte si calcola solo se jp e` uguale a jt
!!!c---------------------------------------------------------------------
                            if (jp2.eq.jt2) then
                              if(.not.((kr2.gt.j2+js2).or.&
                                   (kr2.lt.abs(j2-js2)))) then
                                y1=sqrt(real((j2+1)*(js2+1),&
                                     kind=4))
                                y2=1.e0
                                if(mod(js2-jp2+k2+kp2+kr2,4).ne.0)y2=-1.e0
                                y3=w6js(ll2,ll2,kr2,js2,j2,is2)
                                y4=w6js(k2,kp2,kr2,js2,j2,jp2)
                                y0=y0+y1*y2*y3*y4
                              end if
                            end if
!!!                        x0=.5d0*x6*y0*ae*gei(kr)
!!!                        a(i,j)=a(i,j)-x0
                            xa5_mat(i,j,nt,kr/2)=-.5d0*x6*y0
                          end if
                        end if
                      end do
                    end if
                  end if
                end if
              end if
            end do !!!410
!!!c----------------------------------------------------------------------
!!!c-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
!!!c-----e per i rho^k_q(j,j') con j=j' e q < 0)
!!!c----------------------------------------------------------------------
            do j=1,nunk !!!begin ;460 
              if (ntab(j).eq.nterml) then
                if(.not.((j2tab(j).eq.jp2tab(j)).and.(qtab(j).eq.0))) then
                  js2=jp2tab(j)
                  jt2=j2tab(j)
                  if (.not.((js2.ne.j2).and.(jt2.ne.jp2))) then
                    kp=ktab(j)
                    kp2=kp*2
                    qp=-qtab(j)
                    if(q.eq.qp) then
                      qp2=qp*2
                      irp=irtab(j)
                      if(ir.eq.irp) then
                        do kr=0,2,2 !!! begin ;470 
                          kr2=kr*2
                          if (kr.le.ll2) then
                            if(.not.((kr.gt.k+kp).or.&
                                 (kr.lt.abs(k-kp)))) then
                              x1=real(lu2+1,kind=4)
                              x2=sqrt(real(3*(k2+1)*(kp2+1)*&
                                   (kr2+1),kind=4))
                              x3=1.e0
                              if(mod(2+lu2-is2+j2+qp2,4).ne.0) &
                                   x3=-1.e0
                              x4=w6js(ll2,ll2,kr2,2,2,lu2)
                              x5=w3js(k2,kp2,kr2,q2,-qp2,0)
                              x6=x1*x2*x3*x4*x5
                              y0=0.e0
!!!c--------------------------------------------------------------------
!!!c-----questa parte si calcola solo se j e` uguale a js
!!!c--------------------------------------------------------------------
                              if(j2.eq.js2) then
                                if (.not.((kr2.gt.jp2+jt2).or.&
                                     (kr2.lt.abs(jp2-jt2)))) then
                                  y1=sqrt(real((jp2+1)*(jt2+1),&
                                       kind=4))
                                  y2=w6js(ll2,ll2,kr2,jt2,jp2,is2)
                                  y3=w6js(k2,kp2,kr2,jt2,jp2,j2)
                                  y0=y1*y2*y3
                                end if
                              end if
!!!c---------------------------------------------------------------------
!!!c-----questa parte si calcola solo se jp e` uguale a jt
!!!c---------------------------------------------------------------------
                              if (jp2.eq.jt2) then
                                if (.not.((kr2.gt.j2+js2).or.&
                                     (kr2.lt.abs(j2-js2)))) then
                                  y1=sqrt(real((j2+1)*(js2+1),&
                                       kind=4))
                                  y2=1.e0
                                  if(mod(js2-jp2+k2+kp2+kr2,4).ne.0) y2=-1.e0
                                  y3=w6js(ll2,ll2,kr2,js2,j2,is2)
                                  y4=w6js(k2,kp2,kr2,js2,j2,jp2)
                                  y0=y0+y1*y2*y3*y4
                                end if
                              end if
!!!                          x0=.5d0*x6*y0*ae*gei(kr)
                              sign=1.e0
                              if(mod(js2-jt2-qp2,4).ne.0) sign=-1.e0
                              if(irp.eq.1) sign0=sign
                              if(irp.eq.2) sign0=-sign
!!!                          a(i,j)=a(i,j)-sign0*x0
                              xa6_mat(i,j,nt,kr/2)=&
                                   -sign0*.5d0*x6*y0
                            end if
                          end if
                        end do !!!;470
                      end if
                    end if
                  end if
                end if
              end if
            end do      !!!          ;460
          end if
        end do    !!!                 ;400
!!!c----------------------------------------------------------------------
!!!c-----Se non si vogliono prendere in considerazione gli effetti di
!!!c-----stimolazione si e` finito
!!!c----------------------------------------------------------------------
!!!c      if(isti.ne.1) go to 1300
!!!c----------------------------------------------------------------------
!!!c-----Altrimenti si continua con le transfer rates e le relaxation 
!!!c-----rates dovute all'emissione stimolata
!!!c----------------------------------------------------------------------
!!!c----------------------------------------------------------------------
!!!c-----Transfer rates dovute all'emissione stimolata
!!!c----------------------------------------------------------------------
        do i=1,nunk !!!   ;1100
          if(ntab(i).eq.nterml) then
            jl2=j2tab(i)
            jlp2=jp2tab(i)
            kl=ktab(i)
            kl2=kl*2
            ql=qtab(i)
            ql2=ql*2
            irl=irtab(i)
            do j=1,nunk !!!begin ; 1110
              if(ntab(j).eq.ntermu) then
                ju2=j2tab(j)
                if(.not.((abs(jl2-ju2).gt.2).or.(jl2+ju2.eq.0))) then
                  jup2=jp2tab(j)
                  if(.not.((abs(jlp2-jup2).gt.2).or.&
                       (jlp2+jup2.eq.0))) then
                    qu=qtab(j)
                    qu2=qu*2
                    if(ql2.eq.qu2) then
                      iru=irtab(j)
                      if(irl.eq.iru) then
                        ku=ktab(j)
                        ku2=ku*2
                        do kr=0,2,2 !!! begin ; 1120
                          kr2=kr*2
                          if(.not.((kr.gt.kl+ku).or.&
                               (kr.lt.abs(kl-ku)))) then
                            x1=real(lu2+1,kind=4)
                            x2=sqrt(real(3*(jl2+1)*(jlp2+1)*&
                                 (ju2+1)*(jup2+1)*(kl2+1)*(ku2+1)*&
                                 (kr2+1),kind=4))
                            x3=1.e0
                            if(mod(kr2+ku2+qu2+jup2-ju2,4).ne.0) &
                                 x3=-1.e0
                            x4=w9js(jl2,ju2,2,jlp2,jup2,2,kl2,ku2,kr2)
                            x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                            x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                            x7=w3js(kl2,ku2,kr2,-ql2,qu2,0)
!!!                          x0=x1*x2*x3*x4*x5*x6*x7*ae*gei(kr)
!!!                          a(i,j)=a(i,j)+x0
                            xa7_mat(i,j,nt,kr/2)=&
                                 x1*x2*x3*x4*x5*x6*x7
                          end if
                        end do  !!!  ;1120
                      end if
                    end if
                  end if
                end if
              end if
            end do     !!!           ;1110
!!!c----------------------------------------------------------------------
!!!c-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
!!!c-----e per i rho^k_q(j,j') con j=j' e q < 0)
!!!c----------------------------------------------------------------------
            do j=1,nunk !!! begin ; 1130
              if(ntab(j).eq.ntermu) then
                if(.not.((j2tab(j).eq.jp2tab(j)).and.(qtab(j).eq.0))) then
                  ju2=jp2tab(j)
                  if(.not.((abs(jl2-ju2).gt.2).or.(jl2+ju2.eq.0))) then
                    jup2=j2tab(j)
                    if(.not.((abs(jlp2-jup2).gt.2).or.&
                         (jlp2+jup2.eq.0))) then
                      qu=-qtab(j)
                      qu2=qu*2
                      if(ql2.eq.qu2) then
                        iru=irtab(j)
                        if(irl.eq.iru) then
                          ku=ktab(j)
                          ku2=ku*2
                          do kr=0,2,2 !!! begin ; 1140
                            kr2=kr*2
                            if(.not.((kr.gt.kl+ku).or.&
                                 (kr.lt.abs(kl-ku)))) then
                              x1=real(lu2+1,kind=4)
                              x2=sqrt(real(3*(jl2+1)*(jlp2+1)*&
                                   (ju2+1)*(jup2+1)*(kl2+1)*&
                                   (ku2+1)*(kr2+1),kind=4))
                              x3=1.e0
                              if(mod(kr2+ku2+qu2+jup2-ju2,4).ne.0)&
                                   x3=-1.e0
                              x4=w9js(jl2,ju2,2,jlp2,jup2,2,kl2,ku2,kr2)
                              x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                              x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                              x7=w3js(kl2,ku2,kr2,-ql2,qu2,0)
!!!                          x0=x1*x2*x3*x4*x5*x6*x7*ae*gei(kr)
                              sign=1.e0
                              if(mod(ju2-jup2-qu2,4).ne.0) &
                                   sign=-1.e0
                              if(iru.eq.1) sign0=sign
                              if(iru.eq.2) sign0=-sign
!!!                          a(i,j)=a(i,j)+sign0*x0
                              xa8_mat(i,j,nt,kr/2)=&
                                   sign0*x1*x2*x3*x4*x5*x6*x7
                            end if
                          end do  !!!;1140
                        end if
                      end if
                    end if
                  end if
                end if
              end if
            end do      !!!          ;1130
          end if
        end do !!!                   ;1100
!!!c----------------------------------------------------------------------
!!!c-----Relaxation rates dovute all'emissione stimolata
!!!c----------------------------------------------------------------------
        do i=1,nunk !!!begin     ;1200
          if(ntab(i).eq.ntermu) then
            j2=j2tab(i)
            jp2=jp2tab(i)
            k=ktab(i)
            k2=k*2
            q=qtab(i)
            q2=q*2
            ir=irtab(i)
            do j=1,nunk !!! begin ;1210
              if(ntab(j).eq.ntermu) then
                js2=j2tab(j)
                jt2=jp2tab(j)
                if(.not.((js2.ne.j2).and.(jt2.ne.jp2))) then
                  kp=ktab(j)
                  kp2=kp*2
                  qp=qtab(j)
                  if(q.eq.qp) then 
                    qp2=qp*2
                    irp=irtab(j)
                    if(ir.eq.irp) then
                      do kr=0,2,2 !!! begin ;1220
                        kr2=kr*2
                        if(kr.le.lu2) then
                          if(.not.((kr.gt.k+kp).or.&
                               (kr.lt.abs(k-kp)))) then
                            x1=real(lu2+1,kind=4)
                            x2=sqrt(real(3*(k2+1)*(kp2+1)*&
                                 (kr2+1),kind=4))
                            x3=1.e0
                            if(mod(2+ll2-is2+j2+kr2+qp2,4).ne.0) &
                                 x3=-1.e0
                            x4=w6js(lu2,lu2,kr2,2,2,ll2)
                            x5=w3js(k2,kp2,kr2,q2,-qp2,0)
                            x6=x1*x2*x3*x4*x5
                            y0=0.e0
!!! c--------------------------------------------------------------------
!!! c-----questa parte si calcola solo se j e` uguale a js
!!! c--------------------------------------------------------------------
                            if(j2.eq.js2) then
                              if(.not.((kr2.gt.jp2+jt2).or.&
                                   (kr2.lt.abs(jp2-jt2)))) then
                                y1=sqrt(real((jp2+1)*(jt2+1),&
                                     kind=4))
                                y2=w6js(lu2,lu2,kr2,jt2,jp2,is2)
                                y3=w6js(k2,kp2,kr2,jt2,jp2,j2)
                                y0=y1*y2*y3
                              end if
                            end if !!! 1230 continue
!!! c---------------------------------------------------------------------
!!! c-----questa parte si calcola solo se jp e` uguale a jt
!!! c---------------------------------------------------------------------
                            if(jp2.eq.jt2) then !!! ; go to 1240
                              if(.not.((kr2.gt.j2+js2).or.&
                                   (kr2.lt.abs(j2-js2)))) then
                                y1=sqrt(real((j2+1)*(js2+1),&
                                     kind=4))
                                y2=1.e0
                                if(mod(js2-jp2+k2+kp2+kr2,4).ne.0) y2=-1.e0
                                y3=w6js(lu2,lu2,kr2,js2,j2,is2)
                                y4=w6js(k2,kp2,kr2,js2,j2,jp2)
                                y0=y0+y1*y2*y3*y4
                              end if
                            end if !!! 1240 continue
!!!                        x0=.5d0*x6*y0*ae*gei(kr)
!!!                        a(i,j)=a(i,j)-x0
                            xa9_mat(i,j,nt,kr/2)=-.5d0*x6*y0
!!!                                ;1220 continue
                          end if
                        end if
                      end do  !!!    ;1220
                    end if
                  end if
                end if
              end if
            end do      !!!          ;1210
!!! c----------------------------------------------------------------------
!!! c-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
!!! c-----e per i rho^k_q(j,j') con j=j' e q < 0)
!!! c----------------------------------------------------------------------
            do j=1,nunk !!! begin ; 1260
              if(ntab(j).eq.ntermu) then
                if(.not.((j2tab(j).eq.jp2tab(j)).and.(qtab(j).eq.0))) then
                  js2=jp2tab(j)
                  jt2=j2tab(j)
                  if(.not.((js2.ne.j2).and.(jt2.ne.jp2))) then
                    kp=ktab(j)
                    kp2=kp*2
                    qp=-qtab(j)
                    if(q.eq.qp) then
                      qp2=qp*2
                      irp=irtab(j)
                      if(ir.eq.irp) then
                        do kr=0,2,2 !!! 1270
                          kr2=kr*2
                          if(kr.le.lu2) then
                            if(.not.((kr.gt.k+kp).or.&
                                 (kr.lt.abs(k-kp)))) then
                              x1=real(lu2+1,kind=4)
                              x2=sqrt(real(3*(k2+1)*(kp2+1)*&
                                   (kr2+1),kind=4))
                              x3=1.e0
                              if(mod(2+ll2-is2+j2+kr2+qp2,4).ne.0) x3=-1.e0
                              x4=w6js(lu2,lu2,kr2,2,2,ll2)
                              x5=w3js(k2,kp2,kr2,q2,-qp2,0)
                              x6=x1*x2*x3*x4*x5
                              y0=0.e0
!!! c--------------------------------------------------------------------
!!! c-----questa parte si calcola solo se j e` uguale a js
!!! c--------------------------------------------------------------------
                              if(j2.eq.js2) then
                                if(.not.((kr2.gt.jp2+jt2).or.&
                                     (kr2.lt.abs(jp2-jt2)))) then
                                  y1=sqrt(real((jp2+1)*&
                                       (jt2+1),kind=4))
                                  y2=w6js(lu2,lu2,kr2,jt2,jp2,is2)
                                  y3=w6js(k2,kp2,kr2,jt2,jp2,j2)
                                  y0=y1*y2*y3
                                end if
                              end if !!! 1280    continue
!!! c---------------------------------------------------------------------
!!! c-----questa parte si calcola solo se jp e` uguale a jt
!!! c---------------------------------------------------------------------
                              if(jp2.eq.jt2) then
                                if(.not.((kr2.gt.j2+js2).or.&
                                     (kr2.lt.abs(j2-js2)))) then
                                  y1=sqrt(real((j2+1)*(js2+1),&
                                       kind=4))
                                  y2=1.e0
                                  if(mod(js2-jp2+k2+kp2+kr2,4).ne.0) y2=-1.e0
                                  y3=w6js(lu2,lu2,kr2,js2,j2,is2)
                                  y4=w6js(k2,kp2,kr2,js2,j2,jp2)
                                  y0=y0+y1*y2*y3*y4
                                end if
                              end if !!! 1290    continue
!!!                          x0=.5d0*x6*y0*ae*gei(kr)
                              sign=1.e0
                              if(mod(js2-jt2-qp2,4).ne.0) sign=-1.e0
                              if(irp.eq.1) sign0=sign
                              if(irp.eq.2) sign0=-sign
!!!                          a(i,j)=a(i,j)-sign0*x0
                              xa10_mat(i,j,nt,kr/2)=&
                                   -sign0*.5d0*x6*y0
                            end if
                          end if
                        end do   !!! ; 1270    continue
                      end if
                    end if
                  end if
                end if
              end if
            end do      !!!          ; 1260    continue
          end if
        end do      !!!          ;1200 
      end do !!!2000

!!! c----------------------------------------------------------------------
!!! c-----Si aggiunge il termine immaginario dovuto alla differenza di
!!! c-----energia fra i due livelli della coerenza
!!! c----------------------------------------------------------------------   
      do i=1,nunk !!! begin       ;500
        if(j2tab(i).ne.jp2tab(i)) then
          if(irtab(i).eq.1) then
!!!                a(i,i+1)=a(i,i+1)+twopi*rnutab(i)
            xa11_mat(i,i+1)=xa11_mat(i,i+1)+twopi*rnutab(i)
          end if
          if(irtab(i).eq.2) then
!!!                a(i,i-1)=a(i,i-1)-twopi*rnutab(i)
            xa11_mat(i,i-1)=xa11_mat(i,i-1)-twopi*rnutab(i)
          end if
        end if
      end do   !!!                   ;500

!!! c-----------------------------------------------------------------------
!!! c-----Se idep=1, si aggiunge il termine dovuto alle collisioni 
!!! c-----depolarizzanti (solo per il livello fondamentale)
!!! c-----------------------------------------------------------------------
      if(idep.eq.1) then !!!   ;540
        do i=1,nunk 
          if(ntab(i).eq.1) then
            if(ktab(i).ne.0) then
!!!             a(i,i)=a(i,i)-delta
              xa12_mat(i,i)=-delta
            end if
          end if
        end do
      end if !!!                      ; 540  continue

!!! c==============================================================
!!! c     Do some calculations for am outside B-loop
!!! c     For inversion this needs to be only done once.
!!! c     LAter, for am calculation only matrix mult. is required 
!!! c==============================================================
      xam1_mat=0
      xam2_mat=0
      x41_mat=0
      x42_mat=0
      qqp1_mat=0
      qqp2_mat=0
      do i=1,nunk !!! begin       ; 620
        nterm=ntab(i)
        l2=lsto2(nterm)
        j2=j2tab(i)
        jp2=jp2tab(i)
        k=ktab(i)
        k2=k*2
        q=qtab(i)
        q2=q*2
        ir=irtab(i)
        do j=1,nunk !!! begin     ; 630
          if(ntab(j).eq.nterm) then
            js2=j2tab(j)
            jt2=jp2tab(j)
            kp=ktab(j)
            if(.not.((abs(k-kp).gt.1).or.(k+kp.eq.0))) then !!! ; go to 630
              kp2=kp*2
              qp=qtab(j)
              if( abs(q-qp).le.1) then !!! ;go to 630
                qp2=qp*2
                irp=irtab(j)
                x1=1.e0
                if(mod(j2+jp2-qp2,4).ne.0) x1=-1.e0
                x2=w3js(k2,kp2,2,-q2,qp2,q2-qp2)
                call bigpar(l2,is2,k2,kp2,j2,jp2,js2,jt2,x3)
                if((ir.eq.1).and.(irp.eq.1)) x41_mat(i,j,1)=1
                if((ir.eq.1).and.(irp.eq.2)) x41_mat(i,j,2)=1
                if((ir.eq.2).and.(irp.eq.1)) x41_mat(i,j,3)=1
                if((ir.eq.2).and.(irp.eq.2)) x41_mat(i,j,4)=1
                qqp1_mat(i,j)=qp-q
                x5=sqrt(real((k2+1)*(kp2+1),kind=4))
                xam1_mat(i,j)=x1*x2*x3*x5
              end if
            end if
!!!          end
!!!        endfor                  ; 630  continue
!!! c----------------------------------------------------------------------
!!! c-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
!!! c-----e per i rho^k_q(j,j') con j=j' e q < 0)
!!! c----------------------------------------------------------------------
!!!        for j=1,nunk do begin              ; 640
!!!          if(ntab(j-1).eq.nterm) then begin ;go to 640
            if(.not.((j2tab(j).eq.jp2tab(j)).and.(qtab(j).eq.0))) then
              js2=jp2tab(j)
              jt2=j2tab(j)
              kp=ktab(j)
              if(.not.((abs(k-kp).gt.1).or.(k+kp.eq.0))) then !!! ; go to 640
                kp2=kp*2
                qp=-qtab(j)
                if(abs(q-qp).le.1) then !!! ;go to 640
                  qp2=qp*2
                  irp=irtab(j)
                  x1=1.e0
                  if(mod(j2+jp2-qp2,4).ne.0) x1=-1.e0
                  x2=w3js(k2,kp2,2,-q2,qp2,q2-qp2)
                  call bigpar(l2,is2,k2,kp2,j2,jp2,js2,jt2,x3)
                  if((ir.eq.1).and.(irp.eq.1)) x42_mat(i,j,1)=1
                  if((ir.eq.1).and.(irp.eq.2)) x42_mat(i,j,2)=1
                  if((ir.eq.2).and.(irp.eq.1)) x42_mat(i,j,3)=1
                  if((ir.eq.2).and.(irp.eq.2)) x42_mat(i,j,4)=1
                  qqp2_mat(i,j)=qp-q
                  x5=sqrt(real((k2+1)*(kp2+1),kind=4))
                  sign=1.e0
                  if(modulo(js2-jt2-qp2,4).ne.0) sign=-1.e0
                  if(irp.eq.1) sign0=sign
                  if(irp.eq.2) sign0=-sign
                  xam2_mat(i,j)=x1*x2*x3*x5*sign0
                end if
              end if
            end if
          end if
        end do     !!!             ; 640  continue
      end do !!!                     ; 620  continue
!!!calculate matrix x0t10a_vec
      nl=ntlsto(nemiss)
      nu=ntusto(nemiss)
      ll2=lsto2(nl)
      lu2=lsto2(nu)
      jminl2=abs(ll2-is2)
      jmaxl2=ll2+is2
      jminu2=abs(lu2-is2)
      jmaxu2=lu2+is2
!!!dummy call to paschen to get njlevl
      e0=0
      do j2=jminl2,jmaxl2,2 
        e0(j2)=energy(nl,j2)
      end do
      call paschen(ll2,is2,e0,100.e0,njlevl,autl,cl)
      e0=0
      do j2=jminu2,jmaxu2,2
        e0(j2)=energy(nu,j2)
      end do
      call paschen(lu2,is2,e0,100.e0,njlevu,autu,cu)
      x0=real(lu2+1,kind=4)
      n0t10a=0
      do ml2=-jmaxl2,jmaxl2,2  !!!   ; 3070
        do mu2=-jmaxu2,jmaxu2,2 !!! begin ; 3080
          q2=ml2-mu2
          if(abs(q2).le.2) then !!!          ;go to 3080
            do mup2=-jmaxu2,jmaxu2,2 !!! begin ; 3090
              qp2=ml2-mup2
              if(abs(qp2).le.2) then !!! ;go to 3090
                bigq2=q2-qp2
                bigq=bigq2/2
                bigqu2=mup2-mu2
                bigqu=bigqu2/2
                jal2=abs(ml2)
                if(jal2.lt.jminl2) jal2=jminl2
                do jl2=jal2,jmaxl2,2 !!!    ; 3100
                  do jlp2=jal2,jmaxl2,2 !!! ; 3110
                    jau2=abs(mu2)
                    if(jau2.lt.jminu2) jau2=jminu2
                    do ju2=jau2,jmaxu2,2 !!! ; 3120
                      if(.not.((abs(ju2-jl2).gt.2).or.&
                           (ju2+jl2.eq.0))) then !!! ;3120
                        do jus2=jau2,jmaxu2,2 !!!begin ; 3130 
                          jaup2=abs(mup2)
                          if(jaup2.lt.jminu2) jaup2=jminu2
                          do jup2=jaup2,jmaxu2,2 !!! begin ; 3140
                            if(.not.((abs(jup2-jlp2).gt.2).or.&
                                 (jup2+jlp2.eq.0))) then !!! 3140
                              x1=1.e0
                              if(mod(2+jup2-mu2+qp2,4).ne.0) &
                                   x1=-1.e0
                              x2=sqrt(real((jl2+1)*(jlp2+1)*&
                                   (ju2+1)*(jup2+1),kind=4))
                              x3=w3js(ju2,jl2,2,-mu2,ml2,-q2)
                              x4=w3js(jup2,jlp2,2,-mup2,ml2,-qp2)
                              x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                              x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                              do jsmalll=1,njlevl(ml2) !!! 3150
                                do jsmallu=1,njlevu(mu2) 
                                  do k=0,2 !!!; 3170
                                    k2=k*2
                                    x8=w3js(2,2,k2,q2,-qp2,-bigq2)
                                    kumin=abs(jup2-jus2)/2
                                    kumax=(jup2+jus2)/2
                                    do ku=kumin,kumax !!! 3180
                                      ku2=ku*2
                                      x9=w3js(jup2,jus2,ku2,mup2,-mu2,-bigqu2)
                                      x10=sqrt(real(3*(k2+1)*(ku2+1),kind=4))
                                      n0t10a=n0t10a+1
                                      x0t10a_vec(n0t10a)=& 
                                           x0*x1*x2*x3*x4*x5*x6*x8*x9*x10
                                    end do !!! 3180 continue
                                  end do   !!! 3170 continue
                                end do     !!! 3160 continue
                              end do       !!! 3150 continue
                            end if
                          end do  !!! 3140 continue
                        end do    !!! 3130 continue
                      end if
                    end do        !!! 3120 continue
                  end do          !!! 3110 continue
                end do            !!! 3100 continue
              end if
            end do                !!! 3090 continue
          end if
        end do                    !!! 3080 continue
      end do                      !!! 3070 continue

      n0t10b=0
      x0=real(lu2+1,kind=4)
      do ml2=-jmaxl2,jmaxl2,2    !!! 8070
        do mu2=-jmaxu2,jmaxu2,2  !!! 8080
          q2=ml2-mu2
          if(abs(q2).le.2) then         !!!8080
            do mlp2=-jmaxl2,jmaxl2,2  !!! 8090
              qp2=mlp2-mu2
              if(abs(qp2).le.2) then !!!go to 8090
                bigq2=q2-qp2
                bigq=bigq2/2
                bigql2=ml2-mlp2
                bigql=bigql2/2
                jau2=abs(mu2)
                if(jau2.lt.jminu2) jau2=jminu2
                do ju2=jau2,jmaxu2,2    !!! 8100
                  do jup2=jau2,jmaxu2,2 !!! 8110 
                    jal2=abs(ml2)
                    if(jal2.lt.jminl2) jal2=jminl2
                    do jl2=jal2,jmaxl2,2 !!! 8120
                      if(.not.((abs(ju2-jl2).gt.2).or.&
                           (ju2+jl2.eq.0))) then
                        do jls2=jal2,jmaxl2,2  !!! 8130
                          jalp2=abs(mlp2)
                          if(jalp2.lt.jminl2) jalp2=jminl2
                          do jlp2=jalp2,jmaxl2,2 !!! 8140
                            if(.not.((abs(jup2-jlp2).gt.2).or.&
                                 (jup2+jlp2.eq.0))) then
                              x1=1.e0
                              if(mod(2+jls2-ml2+qp2,4).ne.0) &
                                   x1=-1.e0
                              x2=sqrt(real((jl2+1)*(jlp2+1)*&
                                   (ju2+1)*(jup2+1),kind=4))
                              x3=w3js(ju2,jl2,2,-mu2,ml2,-q2)
                              x4=w3js(jup2,jlp2,2,-mu2,mlp2,-qp2)
                              x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                              x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                              do jsmalll=1,njlevl(ml2) !!! 8150
                                do jsmallu=1,njlevu(mu2) !!! 8160
                                  onum0=autu(mu2,jsmallu)-&
                                       autl(ml2,jsmalll)
                                  do k=0,2 !!! 8170
                                    k2=k*2
                                    x8=w3js(2,2,k2,q2,-qp2,-bigq2)
                                    klmin=abs(jlp2-jls2)/2
                                    klmax=(jlp2+jls2)/2
                                    do kl=klmin,klmax!!! 8180
                                      kl2=kl*2
                                      x9=w3js(jls2,jlp2,kl2,ml2,-mlp2,-bigql2)
                                      x10=sqrt(real(3*(k2+1)*(kl2+1),kind=4))
                                      n0t10b=n0t10b+1
                                      x0t10b_vec(n0t10b)= &
                                           x0*x1*x2*x3*x4*x5*x6*x8*x9*x10
                                    end do !!! 8180 continue
                                  end do   !!! 8170 continue
                                end do     !!! 8160 continue
                              end do       !!! 8150 continue
                            end if
                          end do  !!! 8140 continue
                        end do    !!! 8130 continue
                      end if
                    end do        !!! 8120 continue
                  end do          !!! 8110 continue
                end do            !!! 8100 continue
              end if
            end do                !!! 8090 continue
          end if
        end do                    !!! 8080 continue
      end do                      !!! 8070 continue

      write(*,*) 'Matrix recalculation done.'
    end if !!!recalculation

!    write(*,*) 'HANLE_INIT done.'
  end subroutine hanle_init


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine hanle_calcprof(bfield,inc,azi,vlos,vdopp,dslab,height,obs_par, &
       wlin,nwlin,hanle_i,hanle_q,hanle_u,hanle_v)
    use all_type
    use ftime
!!!    implicit none
    implicit real (a-h,o-z)
    implicit integer(4) (i-n)
    integer(4),parameter :: maxonumcnt=100
    type (linetyp) :: line
    real(4) :: bfield,inc,azi,vlos,vdopp,dslab,height
    type (obstyp) :: obs_par
    integer(2) :: nwlin
    real(8), dimension(nwlin) :: wlin
    real(4), dimension(nwlin) :: hanle_i,hanle_q,hanle_u,hanle_v
    real(4), dimension(nwlin) :: etai,etaq,etau,etav
    real(4), dimension(nwlin) :: epsi,epsq,epsu,epsv
    real(4), dimension(nwlin) :: epsi0,epsq0,epsu0,epsv0
    real(4), dimension(nwlin) :: int,qu,uu,vu,tau
    real(4) :: ic

    logical :: onum_flag(-20:20,11,-20:20,11)
    integer(4) :: onum_id(-20:20,11,-20:20,11)
    real(4),dimension(maxonumcnt,maxwl) :: vo_store,ga_store
    real(8),dimension(maxwl)  :: argvec
    real(4),dimension(maxwl)  :: vo,ga,x13,x14
    real(8) :: dopshift_wl
    integer(4) :: onum_cnt

!    tcpu_start=cputime()

!    write(*,*) 'HANLE_CALCPROF start.'
    onum_cnt=0 
    onum_flag=.false.

    !calculate frequency vector
    trasf=-wlsto(nemiss)*wlsto(nemiss)
    onumvec(1:nwlin)=(wlin-10829.0911d0)/trasf
    dopshift_wl = vlos * 10829.0911d0/clight  
    hdbl=real(height,kind=4)

    !construct a from predefined matrices 
    a=0
    do nt=1,ntrans 
      ae=aesto(nt)
!!!c==============================================================
!!!c     per ogni transizione considerata nel modello atomico chiama 
!!!c     la subroutine campo (linea 1105) che calcola barn e w al posto 
!!!c     di D3model.for 
!!!c     da qui fino alla linea 560 resta uguale a D3hanleprof
!!!c==============================================================
      call campo(u1sto(nt),u2sto(nt),riosto(nt),wlsto(nt),fsto(nt), &
           hdbl,barn,w)
      gei(0)=barn
      gei(2)=barn*w/sqrt(2.e0)
      a=a+xa0_mat(:,:,nt)+xa3_mat(:,:,nt)+xa4_mat(:,:,nt)
      do kr=0,2,2 
        a=a+(xa1_mat(:,:,nt,kr/2)*gei(kr)*ae+ &
             xa2_mat(:,:,nt,kr/2)*gei(kr)*ae+ &
             xa5_mat(:,:,nt,kr/2)*gei(kr)*ae+ &
             xa6_mat(:,:,nt,kr/2)*gei(kr)*ae+ &
             xa7_mat(:,:,nt,kr/2)*gei(kr)*ae+ &
             xa8_mat(:,:,nt,kr/2)*gei(kr)*ae+ &
             xa9_mat(:,:,nt,kr/2)*gei(kr)*ae+ &
             xa10_mat(:,:,nt,kr/2)*gei(kr)*ae)
      end do
    end do !ntrans
    a=a+xa11_mat+xa12_mat

    chb=real(azi,kind=4)*pigr/180.e0
    thb=real(inc,kind=4)*pigr/180.e0
    bgauss=real(bfield,kind=4)
    call rotmatk(1,0.e0,-thb,-chb,dr,di)

    flarmor=bgauss*1.3996d6
    bcoeff=twopi*flarmor   

!!!c-----------------------------------------------------------------------
!!!c-----si azzera la matrice am(i,j)
!!!c-----------------------------------------------------------------------
    am=0
!!!c-----------------------------------------------------------------------
!!!c-----si costruisce la matrice am(i,j)
!!!c-----------------------------------------------------------------------
    !apply predefined matrices
    do i=1,405
      am(:,i)=bcoeff*(xam1_mat(:,i)* &
           (x41_mat(:,i,1)*di(0,qqp1_mat(:,i)) + &
           x41_mat(:,i,2)*dr(0,qqp1_mat(:,i)) + &
           x41_mat(:,i,3)*(-dr(0,qqp1_mat(:,i))) + &
           x41_mat(:,i,4)*di(0,qqp1_mat(:,i))))
      am(:,i)=am(:,i)+bcoeff*(xam2_mat(:,i)* &
           (x42_mat(:,i,1)*di(0,qqp2_mat(:,i)) + &
           x42_mat(:,i,2)*dr(0,qqp2_mat(:,i)) + &
           x42_mat(:,i,3)*(-dr(0,qqp2_mat(:,i))) + &
           x42_mat(:,i,4)*di(0,qqp2_mat(:,i))))
    end do
!!!c----------------------------------------------------------------------- 
!!!c-----Scrive in un file separato le matrici dei coefficienti
!!!c-----------------------------------------------------------------------
!!!c      open(unit=7,file='coeff.dat',status='unknown')
!!!c      do 800 i=1,nunk
!!!c      do 810 j=1,nunk
!!!c      if(imag.ne.1) write(7,7778) i,j,a(i,j)
!!!c 7778 format(2(1x,i3),3x,e15.7)
!!!c      if(imag.eq.1) write(7,7779) i,j,a(i,j),am(i,j)
!!!c 7779 format(2(1x,i3),2(3x,e15.7))
!!!c 810  continue
!!!c 800  continue
!!!c      close(unit=7)
!!!c-----------------------------------------------------------------------
!!!c-----si aggiunge la matrice magnetica alla matrice dei coefficienti
!!!c-----e si azzera il vettore b
!!!c-----------------------------------------------------------------------
    b=0
    a0=a+am
!!!600  continue
!!!c-----------------------------------------------------------------------
!!!c-----si sostituisce la prima riga della matrice con l'equazione della
!!!c-----traccia (moltiplicata per 10**7)
!!!c-----------------------------------------------------------------------
    where(ktab.eq.0) a0(1,:)=1.d7*sqrt(real(j2tab+1,kind=4))

    b(1)=1.d7
!!!c-----------------------------------------------------------------------
!!!c-----Soluzione
!!!c-----------------------------------------------------------------------
!!!c-----------------------------------------------------------------------
!!!c-----Chiama le subroutines (tratte da `Numerical Recipies') per
!!!c-----invertire la matrice
!!!c-----------------------------------------------------------------------
!    write(*,*) 'CPU-time: ',cputime()-tcpu_start
    call ludcmp(a0,nunk,indx,d) 
!    write(*,*) 'CPU-time: ',cputime()-tcpu_start
    call lubksb(a0,nunk,indx,b)
!    write(*,*) 'CPU-time: ',cputime()-tcpu_start
!!!-----------------------------------------------------------------------
!!!-----Scrive la soluzione
!!!-----------------------------------------------------------------------

!!!      open(unit=7,file='tanti.res',status='unknown')
!!!      open(unit=112,file='112.res',status='unknown')
!!!      do 950 i=1,nunk
!!!      write(7,8888) i,ntab(i),j2tab(i),jp2tab(i),ktab(i),qtab(i),
!!!     *              irtab(i),b(i)
!!! 8888 format(7(1x,i5),1x,e15.7)
!!! 950  continue
!!!      close(unit=7)
!!!-----------------------------------------------------------------------
!!!-----Trova i profili dei parametri di Stokes a partire dalla soluzione
!!!-----L'emissione e` calcolata secondo l'Eq.(7.71e) del libro.
!!!-----Si noti che tale equazione e` valida nel riferimento del campo
!!!-----magnetico
!!!-----I profili sono inizialmente calcolati in numero d'onda
!!!-----nell'intervallo (omin,omax)
!!!-----------------------------------------------------------------------
    theta=thetad*tras
    chi=chid*tras
    tgamma=gammad*tras
    call tkq2(-tgamma,-theta,-chi,chb,thb,0.e0,tr,ti)
!!!===============================================================
!!!     a questo punto del programma D3hanleprof legge la lunghezza
!!!     d'onda in Angstrom, pero' in wlsto  e' in microns
!!!===============================================================
    wl=wlsto(nemiss)*1.d4
!!!------------------------------------------------------------------------
!!!-----trasforma la velocita` in numero d'onda e calcola la costante
!!!-----di damping ridotta
!!!------------------------------------------------------------------------
    dnum=vdopp*1.d5/(wl*1.d-8*clight)
    adamp=aesto(nemiss)/(dnum*4.e0*pigr*clight)
!!!-------------------------------------------------------------------------
!!!-----si calcolano autovalori e autovettori
!!!-------------------------------------------------------------------------
    nl=ntlsto(nemiss)
    nu=ntusto(nemiss)
    ll2=lsto2(nl)
    lu2=lsto2(nu)
    jminl2=abs(ll2-is2)
    jmaxl2=ll2+is2
    jminu2=abs(lu2-is2)
    jmaxu2=lu2+is2

!!!---------------------------------------------------------------------------
!!!-----si scrive in una grossa matrice la parte della soluzione che
!!!-----interessa (la matrice densita` del termine superiore, nel caso
!!!-----dell'emissione)
!!!---------------------------------------------------------------------------
    e0=0
    do j2=jminu2,jmaxu2,2
      e0(j2)=energy(nu,j2)
    end do
    autu=0 ; cu=0
    call paschen(lu2,is2,e0,bgauss,njlevu,autu,cu)
    e0=0
    do j2=jminl2,jmaxl2,2 
      e0(j2)=energy(nl,j2)
    end do
    autl=0 ; cl=0
    call paschen(ll2,is2,e0,bgauss,njlevl,autl,cl)

    rhor=0
    rhom=0

    do i=1,nunk !         ; 4040
      if(ntab(i).eq.nu) then 
        if(irtab(i).eq.1) &
             rhor(j2tab(i),jp2tab(i),ktab(i),qtab(i))=b(i)
        if(irtab(i).eq.2) &
             rhoi(j2tab(i),jp2tab(i),ktab(i),qtab(i))=b(i)
        if(j2tab(i).eq.jp2tab(i)) then
          if(qtab(i).eq.0) then
            rhoi(j2tab(i),jp2tab(i),ktab(i),qtab(i))=0.e0
          else
            sign=1.e0
            if(mod(qtab(i),2).ne.0) sign=-1.e0
            if(irtab(i).eq.1) &
                 rhor(j2tab(i),jp2tab(i),ktab(i),-qtab(i))=sign*b(i)
            if(irtab(i).eq.2) &
                 rhoi(j2tab(i),jp2tab(i),ktab(i),-qtab(i))=-sign*b(i)
          end if
        else
          sign=1.e0
          if(mod(j2tab(i)-jp2tab(i)-2*qtab(i),4).ne.0) sign=-1.e0
          if(irtab(i).eq.1) &
               rhor(jp2tab(i),j2tab(i),ktab(i),-qtab(i))=sign*b(i)
          if(irtab(i).eq.2) &
               rhoi(jp2tab(i),j2tab(i),ktab(i),-qtab(i))=-sign*b(i)
        end if
      end if
    end do !                       ; 4040 continue

!!!---------------------------------------------------------------------------
!!!-----scrive i valori di rhor e rhoi (nel riferimento della verticale)
!!!---------------------------------------------------------------------------

!!!      open(unit=8,file='junk.dat')
!!!     do 7777 j2=jminu2,jmaxu2,2
!!!      do 7777 jp2=jminu2,jmaxu2,2
!!!     kmin=iabs(jp2-j2)/2
!!!      kmax=(jp2+j2)/2
!!!      do 7777 k=kmin,kmax
!!!      do 7777 q=-k,k
!!!      write(8,1778) j2,jp2,k,q,rhor(j2,jp2,k,q),rhoi(j2,jp2,k,q)
!!! 1778 format(4(1x,i3),2(1x,e15.7))
!!! 7777 continue
!!!      close(unit=8)
!!!---------------------------------------------------------------------------
!!!-----si trova la matrice densita` nel riferimento del campo magnetico
!!!---------------------------------------------------------------------------
    kmax=jmaxu2
    call rotmatsu(kmax,0.e0,-thb,-chb,dsur,dsui)
    do j2=jminu2,jmaxu2,2 !         ; 4100
      do jp2=jminu2,jmaxu2,2  !      ;4110 
        kkmin=abs(j2-jp2)/2
        kkmax=(j2+jp2)/2
        do k=kkmin,kkmax ! ; 4120
          do q=-k,k !     ; 4130
            rhomr(j2,jp2,k,q)=0.e0
            rhomi(j2,jp2,k,q)=0.e0
            do qp=-k,k ! ;4140 
              call comprod(rhor(j2,jp2,k,qp),rhoi(j2,jp2,k,qp), &
                   dsur(k,q,qp),dsui(k,q,qp),x1,x2)
              rhomr(j2,jp2,k,q)=rhomr(j2,jp2,k,q)+x1
              rhomi(j2,jp2,k,q)=rhomi(j2,jp2,k,q)+x2
            end do            !    ; 4140 continue
          end do              !    ; 4130 continue
        end do                !    ; 4120 continue
      end do                  !    ; 4110 continue
    end do                    !    ; 4100 continue
!!!=========================================================================
!!!     scrive i valori di rhomr e rhomi (nel riferimento mag)
!!!========================================================================
!!!      open(unit=89,file='kk.dat')
!!!      do 7779 j2=jminu2,jmaxu2,2
!!!      do 7779 jp2=jminu2,jmaxu2,2
!!!        kmin=iabs(jp2-j2)/2
!!!       kmax=(jp2+j2)/2
!!!      do 7779 k=kmin,kmax
!!!      do 7779 q=-k,k
!!!	 write(89,7778) j2,jp2,k,q,rhomr(j2,jp2,k,q),rhomi(j2,jp2,k,q)
!!! 7779 continue
!!!      close(unit=89)

!!!---------------------------------------------------------------------------
!!!-----azzera i profili
!!!---------------------------------------------------------------------------

    eps=0.e0
!!!---------------------------------------------------------------------------
!!!-----si fa la somma dell'equazione (7.71e) [(7.71b)]
!!!---------------------------------------------------------------------------
    nloop=0
    n0t10a=0
    x0=real(lu2+1,kind=4)

    do ml2=-jmaxl2,jmaxl2,2 !   ; 3070
      do mu2=-jmaxu2,jmaxu2,2 ! ; 3080
        q2=ml2-mu2
        if(abs(q2).le.2) then !               ;go to 3080
          do mup2=-jmaxu2,jmaxu2,2 !       ; 3090
            qp2=ml2-mup2
            if(abs(qp2).le.2) then !go to 3090
              bigq2=q2-qp2
              bigq=bigq2/2
              bigqu2=mup2-mu2
              bigqu=bigqu2/2
              jal2=(abs(ml2))
              if (jal2.lt.jminl2) jal2=jminl2
              do jl2=jal2,jmaxl2,2 !          ; 3100
                do jlp2=jal2,jmaxl2,2 !       ; 3110
                  jau2=(abs(mu2))
                  if (jau2.lt.jminu2) jau2=jminu2
                  do ju2=jau2,jmaxu2,2 ! ; 3120
                    if(.not.((abs(ju2-jl2).gt.2).or.(ju2+jl2.eq.0))) then !;3120
                      do jus2=jau2,jmaxu2,2 ! ; 3130 
                        jaup2=(abs(mup2))
                        if (jaup2.lt.jminu2) jaup2=jminu2
                        do jup2=jaup2,jmaxu2,2 ! ; 3140
                          if(.not.((abs(jup2-jlp2).gt.2).or.&
                               (jup2+jlp2.eq.0))) then ! go to 3140
                            do jsmalll=1,njlevl(ml2) ! ; 3150
                              do jsmallu=1,njlevu(mu2) ! ; 3160
                                if (.not.onum_flag(mu2,jsmallu,&
                                     ml2,jsmalll)) then
                                  onum_flag(mu2,jsmallu,ml2,jsmalll)=.true.
                                  onum_cnt=onum_cnt+1
                                  onum_id(mu2,jsmallu,ml2,jsmalll)=onum_cnt
                                  onum0=autu(mu2,jsmallu)-&
                                       autl(ml2,jsmalll) 
                                  argvec(1:nwlin)=(onum0-onumvec(1:nwlin))/&
                                       dnum-dopshift_wl
                                  call dvoigt(nwlin,adamp,argvec(1:nwlin),&
                                       vo(1:nwlin),ga(1:nwlin))
                                  ga=ga*2
                                  vo_store(onum_cnt,1:nwlin)=vo
                                  ga_store(onum_cnt,1:nwlin)=ga
                                  if (onum_cnt.ge.maxonumcnt) then 
                                    write(*,*) 'Increase variable maxonumcnt'
                                    stop
                                  end if
                                end if
                                vo(1:nwlin)=&
                                     vo_store(onum_id(mu2,jsmallu,ml2,jsmalll),&
                                     1:nwlin)
                                ga(1:nwlin)=&
                                     ga_store(onum_id(mu2,jsmallu,ml2,jsmalll),&
                                     1:nwlin)
                                x7=cl(ml2,jsmalll,jl2)*cl(ml2,jsmalll,jlp2)*&
                                     cu(mu2,jsmallu,ju2)*cu(mu2,jsmallu,jus2)
                                do k=0,2 ! ; 3170
                                  kumin=abs(jup2-jus2)/2
                                  kumax=(jup2+jus2)/2
                                  do ku=kumin,kumax ! ; 3180
                                    ku2=ku*2
                                    n0t10a=n0t10a+1
                                    do istok=0,3 ! ; 3200
                                      call comprod(tr(k,bigq,istok), &
                                           ti(k,bigq,istok), &
                                           rhomr(jup2,jus2,ku,bigqu), &
                                           rhomi(jup2,jus2,ku,bigqu),x11,x12)
                                      call comprodvec(nwlin,x11,x12,&
                                           vo(1:nwlin),ga(1:nwlin),&
                                           x13(1:nwlin),x14(1:nwlin))
                                      eps(istok,1:nwlin)=eps(istok,1:nwlin)+ &
                                           x0t10a_vec(n0t10a)*x7*x13(1:nwlin)
!!!                                        x0*x1*x2*x3*x4*x5*x6*x7*x8*x9*x10*x13
!!!                                   nloop=nloop+1
!!!                                 if(mod(nloop,1000000).eq.0) type *,nloop
                                    end do    ! ;3200 continue
                                  end do      ! ; 3180 continue
                                end do        ! ; 3170 continue
                              end do          ! ; 3160 continue
                            end do            ! ; 3150 continue
                          end if
                        end do   !  ; 3140 continue
                      end do     !  ; 3130 continue
                    end if
                  end do  !        ; 3120 continue
                end do    !        ; 3110 continue
              end do      !        ; 3100 continue
            end if
          end do           !       ; 3090 continue
        end if
      end do                !      ; 3080 continue
    end do                 !       ; 3070 continue
!!!==================================================================
!!!     qui inizia la parte per aggiungere la polarizazione del
!!!     termine inferiore
!!!==================================================================

    rholr=0
    rholi=0

    do i=1,nunk !                 ; 7040
      if(ntab(i).eq.nl) then !       ;go to 7040
        if(irtab(i).eq.1) rholr(j2tab(i),jp2tab(i),ktab(i),qtab(i))=b(i)
        if(irtab(i).eq.2) rholi(j2tab(i),jp2tab(i),ktab(i),qtab(i))=b(i)
        if(j2tab(i).eq.jp2tab(i)) then
          if(qtab(i).eq.0) then
            rholi(j2tab(i),jp2tab(i),ktab(i),qtab(i))=0.e0
          else
            sign=1.e0
            if(mod(qtab(i),2).ne.0) sign=-1.e0
            if(irtab(i).eq.1) &
                 rholr(j2tab(i),jp2tab(i),ktab(i),-qtab(i))=sign*b(i)
            if(irtab(i).eq.2) &
                 rholi(j2tab(i),jp2tab(i),ktab(i),-qtab(i))=-sign*b(i)
          end if
        else
          sign=1.e0
          if(mod(j2tab(i)-jp2tab(i)-2*qtab(i),4).ne.0) sign=-1.e0
          if(irtab(i).eq.1) &
               rholr(jp2tab(i),j2tab(i),ktab(i),-qtab(i))=sign*b(i)
          if(irtab(i).eq.2) &
               rholi(jp2tab(i),j2tab(i),ktab(i),-qtab(i))=-sign*b(i)
        end if
      end if
    end do !                       ; 7040 continue
    kmax=jmaxl2

    call rotmatsu(kmax,0.e0,-thb,-chb,dslr,dsli)
    do j2=jminl2,jmaxl2,2 !          ; 7100
      do jp2=jminl2,jmaxl2,2 !       ; 7110
        kkmin=abs(j2-jp2)/2
        kkmax=(j2+jp2)/2
        do k=kkmin,kkmax !  ; 7120
          do q=-k,k !       ; 7130
            rhomlr(j2,jp2,k,q)=0.e0
            rhomli(j2,jp2,k,q)=0.e0
            do qp=-k,k !  ; 7140
              call comprod(rholr(j2,jp2,k,qp),rholi(j2,jp2,k,qp), &
                   dslr(k,q,qp),dsli(k,q,qp),x1,x2)
              rhomlr(j2,jp2,k,q)=rhomlr(j2,jp2,k,q)+x1
              rhomli(j2,jp2,k,q)=rhomli(j2,jp2,k,q)+x2
            end do            !    ; 7140 continue
          end do              !    ; 7130 continue
        end do                !    ; 7120 continue
      end do                  !    ; 7110 continue
    end do                    !    ; 7100 continue
!!!      open(unit=8,file='junk.dat')
!!!	do 7777 j2=jminl2,jmaxl2,2
!!!      do 7777 jp2=jminl2,jmaxl2,2
!!!      kmin=iabs(jp2-j2)/2
!!!      kmax=(jp2+j2)/2
!!!      do 7777 k=kmin,kmax
!!!      do 7777 q=-k,k
!!!      write(8,1778) j2,jp2,k,q,rhomlr(j2,jp2,k,q),rhomli(j2,jp2,k,q)
!!! 1778 format(4(1x,i3),2(1x,e15.7))
!!! 7777 continue
!!!      close(unit=8)
    eta=0
    nloopa=0
    n0t10b=0
    x0=real(lu2+1,kind=4)
    do ml2=-jmaxl2,jmaxl2,2 !         ; 8070
      do mu2=-jmaxu2,jmaxu2,2 !       ; 8080
        q2=ml2-mu2
        if(abs(q2).le.2) then !                ;8080
          do mlp2=-jmaxl2,jmaxl2,2 !       ; 8090
            qp2=mlp2-mu2
            if(abs(qp2).le.2) then ! ;go to 8090
              bigq2=q2-qp2
              bigq=bigq2/2
              bigql2=ml2-mlp2
              bigql=bigql2/2
              jau2=abs(mu2)
              if (jau2.lt.jminu2) jau2=jminu2
              do ju2=jau2,jmaxu2,2 !          ; 8100
                do jup2=jau2,jmaxu2,2 !       ; 8110 
                  jal2=abs(ml2)
                  if (jal2.lt.jminl2) jal2=jminl2
                  do jl2=jal2,jmaxl2,2 ! ; 8120
                    if(.not.((abs(ju2-jl2).gt.2).or.(ju2+jl2.eq.0))) then !
                      do jls2=jal2,jmaxl2,2 ! ; 8130
                        jalp2=abs(mlp2)
                        if (jalp2.lt.jminl2) jalp2=jminl2
                        do jlp2=jalp2,jmaxl2,2 ! ; 8140
                          if(.not.((abs(jup2-jlp2).gt.2).or.&
                               (jup2+jlp2.eq.0))) then !
                            do jsmalll=1,njlevl(ml2) ! ; 8150
                              do jsmallu=1,njlevu(mu2) ! ; 8160
                                if (.not.onum_flag(mu2,jsmallu,&
                                     ml2,jsmalll)) then !
                                  onum_flag(mu2,jsmallu,ml2,jsmalll)=.true.
                                  onum_cnt=onum_cnt+1
                                  onum_id(mu2,jsmallu,ml2,jsmalll)=onum_cnt
                                  onum0=autu(mu2,jsmallu)-autl(ml2,jsmalll) 
                                  argvec(1:nwlin)= &
                                       (onum0-onumvec(1:nwlin))/dnum-dopshift_wl
                                  call dvoigt(nwlin,adamp,argvec(1:nwlin),&
                                       vo(1:nwlin),ga(1:nwlin))
                                  ga=ga*2
                                  vo_store(onum_cnt,1:nwlin)=vo(1:nwlin)
                                  ga_store(onum_cnt,1:nwlin)=ga(1:nwlin)
                                  if (onum_cnt.ge.maxonumcnt) then 
                                    write(*,*) 'Increase variable maxonumcnt'
                                    stop
                                  end if
                                end if
                                vo(1:nwlin)=&
                                     vo_store(onum_id(mu2,jsmallu,ml2,jsmalll),&
                                     1:nwlin)
                                ga(1:nwlin)=&
                                     ga_store(onum_id(mu2,jsmallu,ml2,jsmalll),&
                                     1:nwlin)
                                x7=  cl(ml2,jsmalll,jl2)*cl(ml2,jsmalll,jls2)*&
                                     cu(mu2,jsmallu,ju2)*cu(mu2,jsmallu,jup2)
                                do k=0,2 ! ; 8170
                                  k2=k*2
                                  klmin=abs(jlp2-jls2)/2
                                  klmax=(jlp2+jls2)/2
                                  do kl=klmin,klmax ! ; 8180
                                    kl2=kl*2
                                    n0t10b=n0t10b+1
                                    do istok=0,3 ! ;8200 
                                      call comprod(tr(k,bigq,istok), &
                                           ti(k,bigq,istok), &
                                           rhomlr(jls2,jlp2,kl,bigql), &
                                           rhomli(jls2,jlp2,kl,bigql),x11,x12)
                                      call comprodvec(nwlin,x11,x12,&
                                           vo(1:nwlin),ga(1:nwlin),&
                                           x13(1:nwlin),x14(1:nwlin))
                                      eta(istok,1:nwlin)=eta(istok,1:nwlin)+ &
                                           x0t10b_vec(n0t10b)*x7*x13(1:nwlin)
!!!                                   nloopa=nloopa+1l
!!!     if(mod(nloopa,1000000).eq.0) type *,nloopa
                                    end do !    ; 8200 continue
                                  end do  !    ; 8180 continue
                                end do    !    ; 8170 continue
                              end do      !    ; 8160 continue
                            end do        !    ; 8150 continue
                          end if
                        end do !    ; 8140 continue
                      end do   !    ; 8130 continue
                    end if
                  end do !         ; 8120 continue
                end do  !         ; 8110 continue
              end do    !         ; 8100 continue
            end if
          end do !                 ; 8090 continue
        end if
      end do !                     ; 8080 continue
    end do  !                     ; 8070 continue

    stokes=0
!!!==================================================================
!!!     qui calcola la variazione centro-bordo nel caso theta=/0.
!!!     nel centro del disco ritheta=riosto(nemiss), cioe' l'intensita'
!!!     del continuo dell'Allen.
!!!     (wlsto(nemiss)**5)*const e' lo stesso fattore che si utilizza
!!!     per rendere adimensionale barn
!!!      ritheta=riosto(nemiss)*(1-u1sto(nemiss)*(1.e0-dcos(theta))
!!!==================================================================
    ritheta=riosto(nemiss)*(1-u1sto(nemiss)*(1.e0-cos(theta))-&
         u2sto(nemiss)*(1.e0-cos(theta)*cos(theta)))
    ritheta2=ritheta*(wlsto(nemiss)**5)*const

    !compute stokes profiles from eta and eps
    !index 0,1,2,3 = I,Q,U,V
    etai=eta(0,1:nwlin)
    etaq=eta(1,1:nwlin)
    etau=eta(2,1:nwlin)
    etav=eta(3,1:nwlin)
    epsi=eps(0,1:nwlin)
    epsq=eps(1,1:nwlin)
    epsu=eps(2,1:nwlin)
    epsv=eps(3,1:nwlin)

    tau=dslab*etai

    epsi0=ritheta2
    epsq0=0.
    epsu0=0.
    epsv0=0.


    int=epsi0*exp(-tau)+(epsi/etai)*(1-exp(-tau))

    qu=epsq0*exp(-tau)+(epsi/etai)*(epsq/epsi)*(1-exp(-tau)) &
         -(epsi/etai)*(etaq/etai)*(1-exp(-tau))+(etaq/etai)*tau*exp(-tau) &
         *((epsi/etai)-epsi0)

    uu=epsu0*exp(-tau)+(epsi/etai)*(epsu/epsi)*(1-exp(-tau)) &
         -(epsi/etai)*(etau/etai)*(1-exp(-tau))+(etau/etai)*tau*exp(-tau) &
         *((epsi/etai)-epsi0)

    vu=epsv0*exp(-tau)+(epsi/etai)*(epsv/epsi)*(1-exp(-tau)) &
         -(epsi/etai)*(etav/etai)*(1-exp(-tau))+(etav/etai)*tau*exp(-tau) &
         *((epsi/etai)-epsi0)


    ! wavelength=trasf*onumvec(1:nwlin)+10829.0911d0
    ! ic=max(int)
    ic=ritheta2

    hanle_i=real(int/ic,kind=4)
    hanle_q=real(qu/ic,kind=4)
    hanle_u=real(uu/ic,kind=4)
    hanle_v=real(vu/ic,kind=4)

!    write(*,*) 'End Hanle. CPU-time: ',cputime()-tcpu_start
!    write(*,*) 'HANLE_CALCPROF done.'
  end subroutine hanle_calcprof

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!compute profile with full hanle treatment in narrow slab module.
!!!based on hanleprof.f by Laura Merenda
  subroutine hanle_prof(initin,line,b,inc,azi,vlos,vdopp,dslab,height, &
       obs_par,wl,nwl, &
       hanle_i,hanle_q,hanle_u,hanle_v)
    use all_type
    use ftime
    implicit none
    type (linetyp) :: line
    real(4) :: b,inc,azi,vlos,vdopp,dslab,height
    type (obstyp) :: obs_par
    integer(2) :: nwl,initin,init
    real(8), dimension(nwl) :: wl
    real(4), dimension(nwl) :: hanle_i,hanle_q,hanle_u,hanle_v
    real(4) :: tcpu_start,tcpu_stop

!    tcpu_start=cputime()

    init=initin
    if (nemiss.ne.1) init=1
    if (init.eq.1) then 
      call hanle_init(line,obs_par)
    end if

!
    call hanle_calcprof(b,inc,azi,vlos,vdopp,dslab,height,obs_par,wl,nwl, &
         hanle_i,hanle_q,hanle_u,hanle_v)

!    tcpu_stop=cputime()
!    write(*,*) 'Hanle comp done. CPU-time: ',tcpu_stop-tcpu_start
  end subroutine hanle_prof

end module hanle_module
