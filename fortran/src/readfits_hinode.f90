subroutine readfits_hinode(file,yv,profile,naxes,error,verbose)
  use all_type
  use tools
  implicit none
#ifdef MPI
  include 'mpif.h'
#endif
  type (proftyp) :: profile
  integer(4) :: status,unit,blocksize
  INTEGER(4) :: naxes(3),nfound,bitpix,fpixels(3), lpixels(3), inc(3)
  logical :: anynull,error
  integer(2) :: spbshft,nwl,ip,verbose,yv
  character(LEN=72) :: comment
  character(LEN=maxstr) :: file
  real(4) :: imgarr(maxwl)

  !  Get an unused Logical Unit Number to use to open the FITS file.
  status=0
  error=.false.
!  call ftfiou(-1,status)
  call ftgiou(unit,status)
  !  Open the FITS file previously created by WRITEIMAGE
  call ftopen(unit,file,0,blocksize,status)
  if (status.ne.0) then
     write(*,*) 'Could not find Hinode FITS-file: '//trim(file)
     call stop
!     error=.true.
!     return
  end if

  !determine bitpix (should be 32, which is 4-byte integer)
  call ftgidt(unit,bitpix,status)
  !  Determine the size of the image.
  call ftgknj(unit,'NAXIS',1,3,naxes,nfound,status)
  call ftgkyj(unit,'SPBSHFT',spbshft,comment,status)
  nwl=int(naxes(1),kind=2)
  profile%nwl=nwl
  fpixels(1)=1
  lpixels(1)=naxes(1)
  fpixels(2)=yv+1
  lpixels(2)=yv+1
  if ((yv+1.lt.1).or.(yv+1.gt.naxes(2))) then 
     if (verbose.ge.1) write(*,*) 'Could not find ypos=',yv,' in '//trim(file)
     error=.true.
  else
     inc=1
     do ip=1,4
        fpixels(3)=ip
        lpixels(3)=ip
        call ftgsve(unit, 1, 3, naxes, fpixels, lpixels, inc, 0, &
             imgarr, anynull, status)
        select case (ip)
        case(1)
           where (imgarr.lt.0) imgarr=imgarr+65536.
           profile%i(1:nwl)=imgarr(1:nwl)
           if (spbshft.eq.1) profile%i(1:nwl)=2*profile%i(1:nwl)
        case(2)
           profile%q(1:nwl)=imgarr(1:nwl)
           if (spbshft.eq.3) profile%q(1:nwl)=2*profile%q(1:nwl)
        case(3)
           profile%u(1:nwl)=imgarr(1:nwl)
           if (spbshft.eq.3) profile%u(1:nwl)=2*profile%u(1:nwl)
        case(4)
           profile%v(1:nwl)=imgarr(1:nwl)
           if (spbshft.eq.2) profile%v(1:nwl)=2*profile%v(1:nwl)
        end select
     end do

  end if
  call ftclos(unit, status)
  call ftfiou(unit, status)

end subroutine readfits_hinode
