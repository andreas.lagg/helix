!function to normalize filling factor:
!sum of FF for all components belonging to one line group (eg. He)
!should be one
subroutine norm_ff(ff,fitff,linid,ncomp)
  use all_type
  use ipt_decl
  implicit none
  !  type (ipttyp) :: ipt
  !  common /iptcom/ ipt
  integer(2) :: ncomp,nsl,i,j
  integer(4) :: minid,maxid,linid(ncomp)
  real(4) :: ff(ncomp),retff(ncomp),sumff,sumnofitff
  integer(2) :: fitff(ncomp)

  if (ncomp.eq.1) then
     retff=1.
  else
     minid=minval(linid(1:ncomp))
     maxid=maxval(linid(1:ncomp))
     retff=ff
     do i=minid,maxid
        nsl=0
        sumff=0.
        sumnofitff=0.
        do j=1,ncomp
           if (linid(j).eq.i) then
              nsl=nsl+1
              if (fitff(j).eq.0) then
                 sumnofitff=sumnofitff+abs(retff(j))
              else
                 sumff=sumff+abs(retff(j))
              end if
           endif
        end do
        if (sumff.eq.0) then
           sumff=1.
        end if
        if (nsl.gt.0) then
           if (abs(sumff).ge.1e-5) then
              where((linid.eq.i).and.(fitff.ne.0))
                 retff=retff/sumff*(1-sumnofitff)
              end where
           end if
        end if
     end do
  endif

  ff=retff
end subroutine norm_ff
