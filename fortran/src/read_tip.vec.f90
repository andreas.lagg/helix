subroutine read_tip(file,xv,yv0,yv1,ny,profile,icont,lsprof,do_ls,ok)
  use all_type
  use ipt_decl
  use tools
  implicit none
#ifdef MPI
  include 'mpif.h'
#endif
  type (proftyp),dimension(ny) :: profile,lsprof
  character(LEN=maxstr) :: file,fnew,fpre,fpost,fccx
  character(LEN=72) :: comment,nmode
  character(LEN=5) :: idstr
  character(LEN=3) :: fstr
  integer(2) :: xv,yv0,yv1,i,j,nwl,ip,xoff,xerr,ny,ii,iiy
  integer(4) :: isdir,unit,ccxunit,err,blocksize,dummy,myid,ierr
  integer(4) :: status,status_slt,status_avg,wstat,hdutype
  INTEGER(4) :: naxes(3),nfound,bitpix,zidx,fcnt, &
       fpixels(3), lpixels(3), inc(3),xnaxes(2),xstat,wnaxes,naxy
  real(4) :: rimgarray(maxwl)
  integer(4) :: imgarray(maxwl)
  logical :: ok,anynull,error,do_ls,first
  real(4) :: icont,imgarr(maxwl),icntslt,icntavg
  real(8) :: wref,delw
  real(4) :: icimg(:,:)
  allocatable icimg
  real(8) :: wl_disp,wl_off,wl_vec(maxwl)
  real(4),dimension(maxwl) :: it,qt,ut,vt,iprof,qprof,uprof,vprof, &
       prof,a_sort
  INTERFACE
     INTEGER FUNCTION SYSTEM (COMMANDA)
       CHARACTER(LEN=*) COMMANDA
     END FUNCTION SYSTEM
  END INTERFACE

  status=0
  error=.false.
  !  Get an unused Logical Unit Number to use to open the FITS file.
  call ftgiou(unit,status)
  !  Open the FITS file previously created by WRITEIMAGE
  fnew=file
  call ftopen(unit,fnew,0,blocksize,status)
  if (status.ne.0) then
     write(*,*) 'Could not find FITS-file: '//trim(file)
     write(*,*) 'If you want to use split TIP2-files, please '// &
          'specify the name of the first file (eg. ddmmmyy.002-01cc)'
     error=.true.
  end if
  !determine bitpix (should be 32, which is 4-byte integer)
  call ftgidt(unit,bitpix,status)
  !  Determine the size of the image.
  call ftgknj(unit,'NAXIS',1,3,naxes,nfound,status)
  naxy=naxes(2)
  allocate(icimg(naxes(3)/4,naxy))
  nwl=int(naxes(1),kind=2)
  profile%nwl=nwl
  fpixels(1)=1
  lpixels(1)=naxes(1)
  fpixels(2)=yv0+1
  if (fpixels(2).lt.1) fpixels(2)=1
  lpixels(2)=yv1+1
  if (lpixels(2).gt.naxy) lpixels(2)=naxy
  xoff=0
  inc=1
  if ((int(lpixels(2)).le.naxy).and.(fpixels(2).ge.0).and.(xv.ge.0)) then
     if ((int(xv*4+1,kind=4).gt.naxes(3))) then
        !test if FITS is split TIP2 file
        zidx=index(file,'-01')
        if (zidx.ne.0) then
           fcnt=2
           fpre=file(1:zidx-1_2)
           fpost=file(zidx+3:maxstr)
           do while ((.not.error).and.((xv*4+1).gt.naxes(3)))
              xv=xv-naxes(3)/4
              write(fstr,'(a1,i2.2)') '-',fcnt
              fnew=trim(fpre)//fstr//trim(fpost)
              call ftclos(unit, status) !close old unit
              call ftfiou(unit, status)
              call ftgiou(unit,status)  !open new unit
              call ftopen(unit,fnew,0,blocksize,status)
              if (status.eq.0) then
                 call ftgknj(unit,'NAXIS',1,3,naxes,nfound,status)
              else
                 error=.true.
              end if
              fcnt=fcnt+1
           end do
        else
           error=.true.
        end if
     end if
     first=.true.
     if (.not.error) then
        do ip=0,3
           fpixels(3)=(xv)*4+ip+1
           lpixels(3)=(xv)*4+ip+1
           if (bitpix.eq.32) then 
              call ftgsvj(unit,1,3, naxes, fpixels, lpixels, inc, 0, &
                   imgarray, anynull, status)
              prof=real(imgarray,kind=4)
           else if (bitpix.eq.-32) then
              call ftgsve(unit,1,3, naxes, fpixels, lpixels, inc, 0, &
                   rimgarray, anynull, status)
              prof=rimgarray
           else 
              write(*,*) 'Unknown FITS BITPIX. ',bitpix
              call stop
           end if
           !remove bad noisy pixels with median filter (from SSW)
           !                    call f_median(prof,prof,int(maxwl,kind=4),1,3,1,0.,a_sort)
           do ii=1,ny
              if ((yv0+ii.ge.1).and.(yv0+ii.le.naxy)) then
                 select case (ip) 
                 case(0)
                    if (first) then 
                       if ((ipt%verbose.ge.2).and.(do_ls)) then 
                          write(*,*) 'Pixel ',xv,', file '//trim(fnew)
                       end if
                       fccx=trim(fnew)//'x'
                       call read_ccx(fccx,profile%nwl,naxes(3)/4,naxes(2), &
                            wl_disp,wl_off,wl_vec,icimg, &
                            xerr,((i.eq.1).and.(ipt%verbose.ge.1).and.(do_ls)))
                       first=.false.
                    end if
                    if (xerr.ne.0) then
                       if ((first).and.(i.eq.1).and.&
                            (ipt%verbose.ge.1).and.(do_ls)) then
                          write(*,*) 'No Aux (ccx) file found. '// &
                               'Using simple continuum level calculation'
                       end if
                       call get_simple_cont(prof(ii),nwl,icont)
                    end if
                    icont=icimg(xv+1,yv0+ii)
                    if (icont.le.1e-5) icont=1e-5
                    profile(ii)%wl=wl_vec
                    profile(ii)%ic=icont
                    profile(ii)%i(1:nwl)=prof(1:nwl)/icont
                 case(1)
                    profile(ii)%q(1:nwl)=prof(1:nwl)/icont
                 case(2)
                    profile(ii)%u(1:nwl)=prof(1:nwl)/icont
                 case(3)
                    profile(ii)%v(1:nwl)=prof(1:nwl)/icont
                 end select
              end if
           end do
        end do
        ok=(status.eq.0)
        if (status.ne.0) then
           write(*,*) 'Could not read FITS file '//trim(fnew)
           write(*,*) 'Check binary format'
        end if
     else
        ok=.false.
     end if
  else
     ok=.false.
  end if
  !  The FITS file must always be closed before exiting the program. 
  !  Any unit numbers allocated with FTGIOU must be freed with FTFIOU.
  call ftclos(unit, status)
  call ftfiou(unit, status)
  deallocate(icimg)


  if (do_ls) then
     call get_lsprof(file,xv,yv0,profile(1)%wl,nwl,1_2,lsprof)
  end if

end subroutine read_tip
