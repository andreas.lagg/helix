!compilation of helix: when using dislin a conflict with the routine bzero
!may appear. This conflict can be solved when deleting the xstat.o library
!from the dislin archive:
!cd $DISLIN/lib, cp dislin-8.1.a dislin-8.1.a.old, ar -d dislin-8.1.a _xstat.o
!(H. Michels, Nov 11, 2003

program helix
  use all_type
  use ipt_decl
  use localstray_com
  use ftime
  use tools
  use pikvar
  use fitsout
  use atminput_module
  use atminput_comm
#ifdef X11
  use dislin
#endif
#ifdef MPI
  use nxtval
#endif
  implicit none
#ifdef MPI 
  include 'mpif.h'
#endif
#ifdef PIKMPI 
  include 'mpif.h'
#endif
  type (ipttyp) :: iptorig,iptbest
  character(LEN=maxstr) :: file,ifile,idir,plot_title,progfile
  character(50) :: pixstr
  character(LEN=2) :: mstr(3)
  !  type (ipttyp) :: ipt
  !  common /iptcom/ ipt
  type (atmtyp) :: atm(maxatm)
  type (blendtyp) :: blend(maxblend)
  type (gentyp) :: gen
  type (prefiltertyp) :: prefilter
  type(linetyp),dimension(maxlin) :: lin
  real(4), dimension(maxwl,pik_PMAX) :: statifit,statqfit,statufit,statvfit
  !  type (iptgrptyp) :: iptgrp(7)
  type (scltyp) :: scl(maxatm)
  character(LEN=maxstr) :: profname(:,:,:),atm_path,profvec(maxpavg),profstr, &
       xyvec(maxpavg),froot,prof_path,iptnoext
  character(LEN=maxlonstr) :: command,xypixrep
  character(20) :: xyinfo(:,:,:),xytmp,pfmt,xyold
  character(24) ::  ctime
  integer time
  allocatable profname,xyinfo
  integer(4) :: datpos,nx,ny,np,npnow,ip,i0,i1,ib,icnt,idx
  integer(4) :: ipr,iipr,pixelrep,intnwl,irep0,irep1
  integer(2) :: ix,iy,datamode
  integer(4) :: ix_4,ix1_4,iy1_4,nbx,nby,xmm(2),ymm(2),nbtot
  integer(4) :: ibb,ibx,iby,ii,ixx,iyy,xx,yy
  integer(4) :: nptot,nxny,isx,isy,ibold,nxold,nyold,isort(pik_PMAX)
  integer(4) :: profcnt(:)
  allocatable profcnt
  real(4) :: statpar(pik_NMAX,pik_PMAX),statfit(pik_PMAX)
  integer(2) :: statdim(2)
  real(4) :: fitness,fitness_old
  real(8) :: tcpu_start,tcpu_stop,treal_start,treal_stop,ttodo,ttodo_sec,dt
  real(8) :: tcpu_last,treal_last,treal_inv,treal_inv2tot,cload,ms
  !  integer(2) :: datpos,nx,ny,np,nmod,ix,iy,ip,ix1,iy1,xpos,ypos
  character(LEN=4) :: xstr,ystr
  character(LEN=5) :: pxrstr
  character(LEN=1) :: smchar
  type (proftyp) :: obs,plotprof(maxplotprof),wgt(maxmi),fit
  integer(4) :: nfound,fstatus,bsz,naxes(4),punit
  real(4) :: rdummy
  logical :: error,first,piklmfirst
  integer(4) :: npar,n1
  character(maxstr) :: buffer
  integer(4) :: xin,yin
  !  integer(4) :: str2int
  !  external str2int
  !  external str2real
  !  real(4) :: str2real
  logical ::  wopen,hlt,mifound,xin_use,yin_use,piktry
  logical ::  direxist,direxist3,direxist4,hinode_mode,imax_mode,fits4d_mode,atminput_mode
  !  integer(4),dimension(2) :: cnt,cntrate,cntmax
  integer(4) :: myid, numprocs, ierr, proc_done(1024),dest,tag,source
  integer(4) :: counter_comm,smaller_comm,lenfile
  integer(2) :: nvx,nvy,vecx(maxavg),vecy(maxavg),xold(maxavg),yold(maxavg)
  character(maxstr) :: lline
  character(maxllen) :: gitrevision
  character(maxstr) :: path(10)
  integer(1) :: fchar
  integer(2) :: npath,xyarr(:,:),method(maxmi)
  integer(4) :: errlist,il,iulist,progunit,progerr
  character(len=10) :: big_ben (3)
  integer(4) :: date_time(8),iargc
  real(4) :: par(maxfitpar)
  allocatable xyarr
#ifdef MPI
  integer(4) :: status(MPI_STATUS_SIZE),pnam_len
  CHARACTER(LEN=MPI_MAX_PROCESSOR_NAME) :: PROCNAME
  character(LEN=maxstr) :: mpitmp
#else
  character(maxstr) :: procname
#endif

#ifndef GNU
  INTERFACE
     INTEGER FUNCTION SYSTEM (COMMANDA)
       CHARACTER(LEN=*) COMMANDA
     END FUNCTION SYSTEM
  END INTERFACE
#endif
  common /win/ wopen

!  INCLUDE 'version.txt'
  call read_version(gitrevision)
  

  !  interface
  !     integer(4) function iargc ()
  !     end function iargc
  !  end interface

  tcpu_start=cputime()
  treal_start=realtime()
  tcpu_last=tcpu_start
  treal_last=treal_start
  call set_syspar()

  wopen=.false.   !flag for graphics window
  xin_use=.false. !flag for usage of x with -x keyword from command line
  yin_use=.false. !flag for usage of y with -y keyword from command line
  mstr=(/'PI','PO','LM'/)
  pfmt='(i4.4)'

  myid=0 ; numprocs=1
  proc_done=0
  !initialize MPI
#if defined(MPI) || defined(PIKMPI)
  call MPI_INIT( ierr )
  call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
  call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr ) 
  CALL MPI_get_processor_name(procname,pnam_len,ierr)
  !  if (ipt%verbose.ge.1) &
  write(*,fmt='(a,i3,a,i3,a,a)') &
       "Process ", myid, " of ", numprocs-1, ": ",trim(procname)
#else
  procname='localhost'
#endif


  if (myid.eq.0) then
     write(*,*) '*********************************************************'
     write(*,*) '* HeLIx+                 Andreas Lagg (lagg@mps.mpg.de) *'
     write(*,*) '* GIT-revision:                 ',trim(gitrevision),' *'
!     write(*,*) '* Release Date: ',svn_datstr,'         Time: ',svn_timestr,' *'
!     write(*,*) '* Revision: ',svn_revstr,'                                  *'
     write(*,*) '*********************************************************'
  else 
     !initialize random number generator
     !(prevents random mumbers to be the same for all nodes)
     do n1=1,10*myid
        call random_number(rdummy)
     end do
  end if

  !read input file only for node 0
#ifdef PIKMPI
  if (myid.eq.0) then
#endif
     npar=iargc()
     !npar=0
     if (npar.eq.0) file="xdisplay.ipt"
     hlt=.true.
     mifound=.false.
     do n1=1,npar !check for parameter '-i'
        call getarg(n1,buffer)
        if (trim(buffer).eq.'-i') then
           if (n1+1.le.npar) then
              call getarg(n1+1,buffer)
              file=trim(buffer)        
              hlt=.false.
           end if
           mifound=.true.
        end if
        !check for parameter -x and -y
        if ((trim(buffer).eq.'-x').and.(n1+1.le.npar)) then
           call getarg(n1+1,buffer)
           xin=str2int(buffer)
           xin_use=.true.
        endif
        if ((trim(buffer).eq.'-y').and.(n1+1.le.npar)) then
           call getarg(n1+1,buffer)
           yin=str2int(buffer)
           yin_use=.true.
        endif
     end do
     !  if (mifound.eq..false.) then !default filename
     !     hlt=.false.
     !     file='xdisplay.ipt'
     !  end if
     if (hlt) then
        write(*,*) 'Usage: '
        write(*,*) ' helix -i input-file.ipt -x xxx -y yyy'
        stop
     end if
     idir="./input/"
     ifile=trim(file)
     file=trim(idir)//trim(ifile(1:maxstr-len_trim(idir)))
     lenfile=len_trim(file)

     !give filename to other processes
     dest=0 ;tag=0 ; source=0
#ifdef MPI
     if (myid.eq.0) then
        do dest=1,numprocs-1 
           call MPI_Send(lenfile,1,MPI_Integer4,dest,2, &
                MPI_COMM_WORLD,ierr)
           call MPI_Send(file,lenfile,MPI_Character,dest,1, &
                MPI_COMM_WORLD,ierr)
           call MPI_Send(xin,1,MPI_Integer4,dest,3,MPI_COMM_WORLD,ierr)
           call MPI_Send(yin,1,MPI_Integer4,dest,4,MPI_COMM_WORLD,ierr)
           call MPI_Send(xin_use,1,MPI_Logical,dest,5,MPI_COMM_WORLD,ierr)
           call MPI_Send(yin_use,1,MPI_Logical,dest,6,MPI_COMM_WORLD,ierr)
        end do
     else
        call MPI_Recv(lenfile,1,MPI_Integer4,0,2, &
             MPI_COMM_WORLD,status,ierr)
        call MPI_Recv(file,lenfile,MPI_Character,0,1, &
             MPI_COMM_WORLD,status,ierr)
        call MPI_Recv(xin,1,MPI_Integer4,0,3,MPI_COMM_WORLD,status,ierr)
        call MPI_Recv(yin,1,MPI_Integer4,0,4,MPI_COMM_WORLD,status,ierr)
        call MPI_Recv(xin_use,1,MPI_Logical,0,5,MPI_COMM_WORLD,status,ierr)
        call MPI_Recv(yin_use,1,MPI_Logical,0,6,MPI_COMM_WORLD,status,ierr)
        file=file(1:lenfile)
     endif
#endif

     call read_ipt(file)

     !reading input atmosphere (ATM_INPUT keyword)
     call read_atminput()
     
     pixelrep=ipt%pixelrep
     if (pixelrep.lt.1) pixelrep=1

     !use x/y value from command line input
     if (xin_use) ipt%x=(/xin,xin/)
     if (yin_use) ipt%y=(/yin,yin/)

     !store variables from input files in separate variables
     !maybe not necessary??

     !default size for computational boxes (for pik_lm conversion method)
     nbx=1
     nby=1

     datamode=99 !mode of data retrieval undefined
     hinode_mode=.false.
     imax_mode=.false.
     fits4d_mode=.false.
     atminput_mode=(maxval(atminput%npar).ge.1)
!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !create list of profile names
     if ((ipt%synth.eq.1).and.(atminput_mode.eqv..false.)) then !use synthetic spectrum
        if (ipt%verbose.ge.1.) write(*,*) 'Use synthetic spectrum'
        allocate(profname(1,1,1))
        allocate(xyinfo(1,1,1))
        allocate(profcnt(1))
        profcnt=pixelrep
        nptot=1
        np=1
        i0=index(trim(ipt%file),'/',back=.true.)
        i1=index(trim(ipt%file),'.ipt',back=.true.)
        profname=ipt%file(i0+1:i1-1)
        xyinfo=ipt%file(i0+1:i1-1)
        xmm=ipt%x
        ymm=ipt%y
     else  !construct filenames for data files
        datpos=index(trim(ipt%observation),'.dat',back=.true.)
        !single dat file selected

        if (((datpos+3).eq.len_trim(ipt%observation)).and.(datpos.ge.1)) then
           if (ipt%verbose.ge.1.) write(*,*) 'use single profile'
           allocate(profname(1,1,1))
           allocate(xyinfo(1,1,1))
           allocate(profcnt(1))
           profcnt=pixelrep
           nptot=1
           np=1
           !       profname=ipt%dir%profile(1:len_trim(ipt%dir%profile))//&
           !            ipt%observation(1:len_trim(ipt%observation))
           profname="+"
           !        profname="/"//ipt%observation(1:len_trim(ipt%observation))
           xyinfo=ipt%observation(1:len_trim(ipt%observation))
           !        xyinfo=ipt%observation(1:8)
           !           ipt%x=(/1,1/)
           !           ipt%y=(/1,1/)
           datamode=0
           xmm=ipt%x
           ymm=ipt%y
        else
           if ((ipt%profile_list.ne.' ').and.((.not.xin_use).and.(.not.yin_use))) then !use xy data from external file list
              if (ipt%verbose.ge.1) &
                   write(*,*) 'read profile_list from '//trim(ipt%profile_list)
              path(1)='./'
              path(2)=trim(ipt%dir%profile)
              path(3)=ipt%dir%profile(1:len_trim(ipt%dir%profile))//"/"&
                   //ipt%observation(1:len_trim(ipt%observation))
              npath=1
              call file_open(ipt%profile_list,path(1:npath),npath,iulist, &
                   errlist,ipt%verbose)
              if (errlist.ne.0) then
                 write(*,*) 'Could not find PROFILE_LIST ',trim(ipt%profile_list)
                 stop
              end if
              il=0
              do while (errlist.eq.0)
                 read(iulist,fmt='(a)',iostat=errlist) lline
                 fchar=ichar(lline(1:1))
                 if ((fchar.ge.48).and.(fchar.le.57).and.(errlist.eq.0)) il=il+1
              end do
              nptot=il
              if (nptot.eq.0) then
                 write(*,*) 'Could not find valid pixels in '//&
                      trim(ipt%profile_list)
                 stop
              end if
              allocate(xyarr(nptot,2))
              il=0
              errlist=0
              rewind(iulist)
              do while (errlist.eq.0)
                 read(iulist,fmt='(a)',iostat=errlist) lline
                 fchar=ichar(lline(1:1))
                 if ((fchar.ge.48).and.(fchar.le.57).and.(errlist.eq.0)) then
                    il=il+1
                    xyarr(il,1:2)=(/int(str2real(lline(1:index(lline,' ')))), &
                         int(str2real(lline(index(lline,' ')+1:100)))/)
                 end if
              end do
              close(iulist)
              if (ipt%verbose.eq.1.and.myid.eq.0) & 
                   write(*,*) 'Profile list contains ',nptot,' pixels.'
           else  !use data files from a directory
              nx=1
              ny=1
              do ix=ipt%x(1),ipt%x(2)
                 call get_profidx(ix,ipt%y(1),ipt%stepx,ipt%stepy,vecx,vecy, &
                      ipt%average,ipt%scansize,ipt%x,ipt%y,nvx,nvy)
                 if (ix.gt.ipt%x(1)) then
                    if (vecx(1).ne.xold(nx)) nx=nx+1
                 end if
                 xold(nx)=vecx(1)
              end do
              do iy=ipt%y(1),ipt%y(2)
                 call get_profidx(ipt%x(1),iy,ipt%stepx,ipt%stepy,vecx,vecy, &
                      ipt%average,ipt%scansize,ipt%x,ipt%y,nvx,nvy)
                 if (iy.gt.ipt%y(1)) then
                    if (vecy(1).ne.yold(ny)) ny=ny+1
                 end if
                 yold(ny)=vecy(1)
              end do
              !          nptot=nx*ny
              !          allocate(xyarr(nptot,2))
              !          do ix=0,nx-1
              !            do iy=0,ny-1
              !              xyarr((ix*ny)+iy+1,1)=xold(ix+1)
              !              xyarr((ix*ny)+iy+1,2)=yold(iy+1)
              !            end do
              !          end do
              nxold=nx 
              nyold=ny
              !              nx=(nx/ipt%piklm_bx+1)*ipt%piklm_bx
              !              ny=(ny/ipt%piklm_by+1)*ipt%piklm_by
              nx=(nx/ipt%piklm_bx)*ipt%piklm_bx
              ny=(ny/ipt%piklm_by)*ipt%piklm_by

              !sort according to piklm box size
              nptot=nx*ny
              allocate(xyarr(nptot,2))
              xyarr(:,:)=-1
              do ix=0,nx/ipt%piklm_bx-1
                 do iy=0,ny/ipt%piklm_by-1
                    do ibx=0,ipt%piklm_bx-1
                       do iby=0,ipt%piklm_by-1
                          ii=(iby+ibx*ipt%piklm_by+&
                               iy*ipt%piklm_by*ipt%piklm_bx+ &
                               ix*ipt%piklm_bx*ny )+1
                          ixx=ix*ipt%piklm_bx+ibx
                          iyy=iy*ipt%piklm_by+iby
                          if ((ixx.le.nxold-1).and.(iyy.le.nyold-1)) then
                             xyarr(ii,1)=xold(ixx+1)
                             xyarr(ii,2)=yold(iyy+1)
                          end if
                       end do
                    end do
                 end do
              end do
           end if


           !calculate number of computation boxes according to piklm_bx/piklm_by
           xmm=(/minval(xyarr(:,1)),maxval(xyarr(:,1))/)
           ymm=(/minval(xyarr(:,2)),maxval(xyarr(:,2))/)
           isx=ipt%stepx
           if (isx.eq.0) isx=1
           isy=ipt%stepy
           if (isy.eq.0) isy=1
           !        nbx=((xmm(2)-xmm(1))/ipt%piklm_bx)/isx+1+1
           !        nby=((ymm(2)-ymm(1))/ipt%piklm_by)/isy+1+1
           nbx=(xmm(2)-xmm(1))/isx+1+1
           nby=(ymm(2)-ymm(1))/isy+1+1
           !        np=ipt%stepx*ipt%stepy
           np=isx*isy
           if (ipt%average.eq.0) np=1
           allocate(profname(nbx*nby,ipt%pixelrep,np))
           allocate(xyinfo(nbx*nby,ipt%pixelrep,np))
           allocate(profcnt(nbx*nby))
           profcnt=0
           xyinfo="-"
           profname="-"
           pfmt='(i4.4)'
           !check whether directory is 3-digit (old) or 4-digit
           direxist4=(system('find '//&
                ipt%dir%profile(1:len_trim(ipt%dir%profile))//&
                ipt%observation(1:len_trim(ipt%observation))//&
                '/x???? '//trim(finddepth)//trim(redirect)).eq.0)
           direxist3=(system('find '//&
                ipt%dir%profile(1:len_trim(ipt%dir%profile))//&
                ipt%observation(1:len_trim(ipt%observation))//&
                '/x??? '//trim(finddepth)//trim(redirect)).eq.0)
           if ((.not.direxist4).and.(direxist3)) pfmt='(i3.3)'
           ibb=0
           do ix_4=1,nptot
              ibb=ibb+1
              if ((xyarr(ix_4,1).ne.-1).and.(xyarr(ix_4,2).ne.-1)) then
                 call get_profidx(xyarr(ix_4,1),xyarr(ix_4,2),&
                      ipt%stepx,ipt%stepy, &
                      vecx,vecy,ipt%average,ipt%scansize,ipt%x,ipt%y,nvx,nvy)
                 !          ibx=(xyarr(ix_4,1)-xmm(1))/ipt%piklm_bx/isx+1
                 !          iby=(xyarr(ix_4,2)-ymm(1))/ipt%piklm_by/isy+1
                 if ((ibb.le.(nbx)*(nby)).and.&
                      (profcnt(ibb).le.pixelrep)) then 
                    profcnt(ibb)=profcnt(ibb)+pixelrep
                    do ix1_4=0,nvx-1
                       do iy1_4=0,nvy-1
                          ip=ix1_4*nvy+iy1_4
                          write(unit=xstr,fmt=trim(pfmt)) vecx(ix1_4+1)
                          write(unit=ystr,fmt=trim(pfmt)) vecy(iy1_4+1)
                          xytmp="x"//trim(xstr)//"y"//trim(ystr)
                          xyinfo(ibb,profcnt(ibb)/pixelrep,ip+1)= &
                               xytmp(1:len_trim(xytmp))
                          profname(ibb,profcnt(ibb)/pixelrep,ip+1)=&
                               "/x"//trim(xstr)//"/"//&
                               xytmp(1:len_trim(xytmp))//".profile.dat"
                       end do
                    end do
                 end if
              end if
           end do
        end if
     end if

     !check hinode data mode
     froot=ipt%dir%profile(1:len_trim(ipt%dir%profile))//&
          ipt%observation(1:len_trim(ipt%observation))
     if (myid.eq.0) then
        direxist=(system('test -d '//trim(froot)).eq.0)
        if (datamode.eq.99)  then
           if (direxist) then
              !check if directory contains Hinode files
              hinode_mode=(system('find '//&
                   ipt%dir%profile(1:len_trim(ipt%dir%profile))//&
                   ipt%observation(1:len_trim(ipt%observation))//&
                   '/*SP4*.fits '//trim(finddepth)//trim(redirect)).eq.0)
              if (.not.hinode_mode) then !try SP3 files
                 hinode_mode=(system('find '//&
                      ipt%dir%profile(1:len_trim(ipt%dir%profile))//&
                      ipt%observation(1:len_trim(ipt%observation))//&
                      '/*SP3*.fits '//trim(finddepth)//trim(redirect)).eq.0)
              end if
              !datafiles are contained in directories with x000 structure
              !if datamode was already defined (eg. use of single
              !profile), then do not set data mode
              datamode=2
           else
              !check if directory contains Hinode files
              i0=index(ipt%observation,'SP4')
              if (i0.lt.1) i0=index(ipt%observation,'SP3')
              i1=index(ipt%observation,'.fits')
              if ((i0.ge.1).and.(i1.ge.1)) then
                 hinode_mode=(system('find '//&
                      ipt%dir%profile(1:len_trim(ipt%dir%profile))//&
                      ipt%observation(1:len_trim(ipt%observation))//&
                      ' '//trim(finddepth)//trim(redirect)).eq.0)
                 if (.not.hinode_mode) then
                    write(*,*) 'No Hinode files found in '//&
                         ipt%dir%profile(1:len_trim(ipt%dir%profile))//&
                         ipt%observation(1:len_trim(ipt%observation))
                    call stop
                 end if
              end if
              i0=index(ipt%observation,'imax')
              i1=index(ipt%observation,'4D')
              if ((i0.ge.1).and.(i1.eq.0)) then
                 imax_mode=.true.
              endif
              !data is in FITS file
              fstatus=0
              bsz=0
              call ftgiou(punit,fstatus)
              call ftopen(punit,froot,0,bsz,fstatus) !  open the fits file
              if (fstatus.ne.0) &
                   write(*,*) 'Could not open FITS file: '//trim(froot)
              call ftgknj(punit,'NAXIS',1,4,naxes,nfound,fstatus)
              call ftclos(punit,fstatus)
              call ftfiou(punit,fstatus)
              if (nfound.eq.4) then
                 datamode=5
                 fits4d_mode=.true.
              else
                 datamode=1
              end if
           end if
           !overwrite data mode if hinode_mode was found
           if (hinode_mode) datamode=3
           if (imax_mode) datamode=4
        end if
     end if
#ifdef MPI
     if (myid.eq.0) then
        do dest=1,numprocs-1 
           call MPI_Send(datamode,1,MPI_Integer2,dest,7,MPI_COMM_WORLD,ierr)
           call MPI_Send(hinode_mode,1,MPI_Logical,dest,8,MPI_COMM_WORLD,ierr)
           call MPI_Send(imax_mode,1,MPI_Logical,dest,9,MPI_COMM_WORLD,ierr)
           call MPI_Send(fits4d_mode,1,MPI_Logical,dest,10,MPI_COMM_WORLD,ierr)
        end do
     else
        call MPI_Recv(datamode,1,MPI_Integer2,0,7,MPI_COMM_WORLD,status,ierr)
        call MPI_Recv(hinode_mode,1,MPI_Logical,0,8,MPI_COMM_WORLD,status,ierr)
        call MPI_Recv(imax_mode,1,MPI_Logical,0,9,MPI_COMM_WORLD,status,ierr)
        call MPI_Recv(fits4d_mode,1,MPI_Logical,0,10,MPI_COMM_WORLD,status,ierr)
     endif
#endif

     nbtot=nbx*nby*pixelrep
     nptot=nptot*pixelrep
     !open fits output
     if ((myid.eq.0).and.(ipt%fitsout.eq.1)) then
        if (len_trim(ipt%observation).ne.0) then
           atm_path=ipt%dir%atm(1:len_trim(ipt%dir%atm))//'atm_'//&
                ipt%observation(1:len_trim(ipt%observation))
           prof_path=ipt%dir%atm(1:len_trim(ipt%dir%atm))//'prof_'//&
                ipt%observation(1:len_trim(ipt%observation))
        else
           iptnoext=reverse_string(ipt%file)
           idx=scan(iptnoext,'.')
           iptnoext=iptnoext(idx+1:len_trim(iptnoext))
           idx=scan(iptnoext,'/')
           iptnoext=(iptnoext(1:idx-1))
           iptnoext=reverse_string(iptnoext)
           atm_path=ipt%dir%atm(1:len_trim(ipt%dir%atm))//'atm_'//&
                trim(iptnoext)
           prof_path=ipt%dir%atm(1:len_trim(ipt%dir%atm))//'prof_'//&
                trim(iptnoext)
        end if
        if (atm_path(len_trim(atm_path):len_trim(atm_path)).eq.'/') &
             atm_path=atm_path(1:len_trim(atm_path)-1)
        if (prof_path(len_trim(prof_path):len_trim(prof_path)).eq.'/') &
             prof_path=prof_path(1:len_trim(prof_path)-1)
        if (atm_path(len_trim(atm_path)-4:len_trim(atm_path)).eq.'.fits') &
             atm_path=atm_path(1:len_trim(atm_path)-5)
        if (prof_path(len_trim(prof_path)-4:len_trim(prof_path)).eq.'.fits') &
             prof_path=prof_path(1:len_trim(prof_path)-5)
        if (len_trim(ipt%dir%atm_suffix).ge.1) then 
           atm_path=atm_path(1:len_trim(atm_path))//'_'//&
                ipt%dir%atm_suffix(1:len_trim(ipt%dir%atm_suffix))
           prof_path=prof_path(1:len_trim(prof_path))//'_'//&
                ipt%dir%atm_suffix(1:len_trim(ipt%dir%atm_suffix))
        end if
        if (atm_path(len_trim(atm_path)-4:len_trim(atm_path)).ne.'.fits') &
             atm_path=atm_path(1:len_trim(atm_path))//'.fits'
        if (prof_path(len_trim(prof_path)-4:len_trim(prof_path)).ne.'.fits') &
             prof_path=prof_path(1:len_trim(prof_path))//'.fits'
        call fitsout_open(atm_path,prof_path,xmm,ymm,ipt)
     end if


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !MPI should start here  
     !initialize counter


#ifdef MPI
     call MPE_Counter_create(MPI_COMM_WORLD,smaller_comm,counter_comm,ierr )
     if (counter_comm.ne.MPI_COMM_NULL) then 
        !open progress file
        if ((myid.eq.1).and.(numprocs.ge.2)) then
           progunit=22
           call date_and_time(big_ben (1), big_ben (2), big_ben (3), date_time)
           progfile='progress_'//big_ben(2)//'.log'
           open(progunit,file=trim(progfile), &
                status='replace',iostat=progerr,action='WRITE')
           write(*,*) 'Writing progress to '//trim(progfile)
           write(progunit,*) repeat('-',78)
           write(progunit,*) 'HeLIx+ progress'
           write(progunit,*) 'Date: '//big_ben(1)//' Time: '//big_ben(2)
           write(progunit,*) 'Input file: ',trim(file)
           write(progunit,*) 'Output directory: ',&
                trim(ipt%dir%atm(1:len_trim(ipt%dir%atm))//'atm_'//&
                ipt%observation(1:len_trim(ipt%observation)))
           write(progunit,*) 'Total number of pixels to be inverted: ',nptot
           write(progunit,*) repeat('-',78)
        end if
#else
        counter_comm=0 ; smaller_comm=0 ; progerr=0 ; progunit=0 ; ierr=0 ; myid=0
        big_ben='' ; date_time=0 ; progfile=''
#endif     

        !store original ipt variable (for pik_lm the ipt-variable is canged
        !to pass the atmosphere calculated from pikaia to the lmdiff calls)
        iptorig=ipt

        !main do loop for the processing of the profiles
        first=.true.
        ib=0
        ibold=0

        do while (ib.lt.nptot)

#ifdef MPI
           call MPE_Counter_nxtval(counter_comm, ib, ierr )
           if (ib.eq.0)  call MPE_Counter_nxtval(counter_comm, ib, ierr )
#else
           ib=ib+1
#endif     
           !        ibx=mod((ib-1)/pixelrep,nbx)+1
           !        iby=((ib-1)/pixelrep)/nbx+1
           ibb=(ib-1)/pixelrep+1
           ipr=mod((ib-1),pixelrep)+1

           !special treatment of method for pik_lm minimization method (method=3)
           method=ipt%method
           fitness_old=0.
           !        do icnt=1,profcnt(ibb)
           icnt=1
           piktry=.false.
           !        do while (icnt.le.int(profcnt(ibb),kind=4))
           do while ((icnt.le.((profcnt(ibb)-1)/pixelrep+1)).and.&
                (ibb.le.nbx*nby))

              ip=int(icnt-1,kind=4)+int(sum(profcnt(1:ibb)),kind=4)
              ip=ip+ipr

              npnow=1
              nxny=ipt%stepx*ipt%stepy
              if (nxny.gt.maxpavg) then
                 write(*,*) 'Error: # of profiles to average exceeds max. value'
                 write(*,*) 'STEPX*STEPY = ',nxny
                 write(*,*) 'MAXPAVG     = ',maxpavg
                 call stop
              end if
              if (nxny.gt.np) then
                 nxny=np
              end if
              if (nxny.eq.0) nxny=1
              error=.true.
              if (datamode.eq.0) error=.false.
              do n1=1,nxny
                 if (trim(profname(ibb,icnt,n1)).ne."-") then
                    profvec(npnow)=ipt%observation(1:len_trim(ipt%observation))
                    if (trim(profname(ibb,icnt,n1)).ne."+") then
                       profstr=profvec(npnow)
                       profvec(npnow)=profstr(1:len_trim(profstr))//&
                            trim(profname(ibb,icnt,n1))
                       xyvec(npnow)=trim(xyinfo(ibb,icnt,n1))
                       error=.false.
                    end if
                    npnow=npnow+1
                 endif
              end do
              np=npnow-1

              !localstray only for hinode_mode and fits-mode
              if (ipt%localstray_rad.ge.1e-5) then
                 dols=1
              else
                 dols=0
              end if
              if ((dols.eq.1).and.((datamode.ne.3).and.&
                   (datamode.ne.4).and.(datamode.ne.1).and.&
                   (datamode.ne.5))) then 
                 write(*,*) 'LOCALSTRAY correction only available for'//&
                      ' Hinode, IMaX, FITS4D or TIP (FITS) dataformat'
                 call stop
              end if

              if (.not.error) then

                 !check if first pixel in piklmbox
                 if (pfmt.eq.'(i4.4)') then
                    ixx=str2int(xyvec(np)(2:5))
                    iyy=str2int(xyvec(np)(7:10))
                 else
                    ixx=str2int(xyvec(np)(2:4))
                    iyy=str2int(xyvec(np)(6:8))
                 end if
                 piklmfirst=((mod(ixx,ipt%piklm_bx).eq.0).and.&
                      (mod(iyy,ipt%piklm_by).eq.0))
                 if (ipt%method(1).eq.3) then
                    if (piklmfirst) then
                       method=0
                       if (ipt%verbose.ge.2) write(*,*) 'First pixel: PIKAIA'
                       ipt=iptorig
                       iptbest=ipt
                       piktry=.false.
                    else
                       if (piktry) then 
                          method=0
                          if (ipt%verbose.ge.2) write(*,*) &
                               'PIKAIA-retry (bad-fitness for LMDIFF)'
                       else
                          method=2
                          if (ipt%verbose.ge.2) write(*,*) &
                               'LMDIFF, initial conditions from '//xyold
                       end if
                       ipt=iptbest
                    end if
                 end if

                 !get initial condition / sythesis info from ATMINPUT keyword.
                 call atminput_fill(xyvec(np))
!                 write(*,*) ipt%atm(1)%par%b,atm(1)%par%b
!                 write(*,*) ipt%atm(1)%par%inc,atm(1)%par%inc
!                 write(*,*) ipt%atm(1)%par%azi,atm(1)%par%azi
!                 call stop
#ifdef MPI
                 call MPI_SEND(MPI_BOTTOM,0,MPI_INTeger,0,PROFTAG,counter_comm,ierr)
                 call MPI_SEND(nxny,1,MPI_Integer4,0,PROFTAG-1,counter_comm,ierr)
                 do ii=1,nxny
                    mpitmp=profvec(ii)
                    call MPI_SEND(mpitmp,int(maxstr),MPI_Character,0,PROFTAG+1,counter_comm,ierr)
                    mpitmp=xyvec(ii)
                    call MPI_SEND(mpitmp,int(maxstr),MPI_Character,0,PROFTAG+2,counter_comm,ierr)
                 end do
                 call MPI_SEND(int(np,2),1,MPI_Integer2,0,PROFTAG+3,counter_comm,ierr)
                 call MPI_SEND(froot,int(maxstr),MPI_Character,0,PROFTAG+4,counter_comm,ierr)
                 call MPI_SEND(datamode,1,MPI_Integer2,0,PROFTAG+5,counter_comm,ierr)
                 call MPI_RECV(obs%nwl,1,MPI_INTEGER2,0,PROFTAG+6,counter_comm,status,ierr)
                 call MPI_RECV(obs%ic,1,MPI_REAL,0,PROFTAG+7,counter_comm,status,ierr)
                 intnwl=int(obs%nwl)
                 call MPI_RECV(obs%i,intnwl,MPI_REAL,0,PROFTAG+8,counter_comm,status,ierr)
                 call MPI_RECV(obs%q,intnwl,MPI_REAL,0,PROFTAG+9,counter_comm,status,ierr)
                 call MPI_RECV(obs%u,intnwl,MPI_REAL,0,PROFTAG+10,counter_comm,status,ierr)
                 call MPI_RECV(obs%v,intnwl,MPI_REAL,0,PROFTAG+11,counter_comm,status,ierr)
                 call MPI_RECV(obs%wl,intnwl,MPI_REAL8,0,PROFTAG+12,counter_comm,status,ierr)
                 call MPI_RECV(error,1,MPI_LOGICAL,0,PROFTAG+13,counter_comm,status,ierr)
                 call MPI_RECV(lsprof%nwl,1,MPI_INTEGER2,0,PROFTAG+14,counter_comm,status,ierr)
                 intnwl=int(lsprof%nwl)
                 call MPI_RECV(lsprof%i,intnwl,MPI_REAL,0,PROFTAG+15,counter_comm,status,ierr)
                 call MPI_RECV(lsprof%q,intnwl,MPI_REAL,0,PROFTAG+16,counter_comm,status,ierr)
                 call MPI_RECV(lsprof%u,intnwl,MPI_REAL,0,PROFTAG+17,counter_comm,status,ierr)
                 call MPI_RECV(lsprof%v,intnwl,MPI_REAL,0,PROFTAG+18,counter_comm,status,ierr)
                 call MPI_RECV(lsprof%wl,intnwl,MPI_REAL8,0,PROFTAG+19,counter_comm,status,ierr)
#else
                 call get_profile(profvec,xyvec,int(np,2),froot,&
                      datamode,obs,error)
#endif
                 if (.not.error) then
                    ipt%line%icont=obs%ic
#ifdef X11
                    if (myid.eq.0) then
                       if (ipt%display_profile.ge.2) then
                          if ((ipt%verbose.ge.2).or.(nptot.eq.1)) then
                             plotprof(1)=obs
                             plotprof(1)%name='OBS'
                             plot_title=pixstr
                             call plot_profile(plot_title,plotprof, &
                                  1,(/1,1,1,1/), &
                                  ipt%atm,ipt%scale,ipt%ncomp,wgt(1),0,0., &
                                  ipt%line,ipt%nline,ipt%nblend,ipt%blend, &
                                  ipt%gen)
                          end if
                       end if
                    end if
#endif
                 !run inversion
                    treal_inv=realtime()
                    call run_inversion(obs,atm,blend,gen,lin,fit,scl,method, &
                         fitness,statdim,statfit,statpar,&
                         statifit,statqfit,statufit,statvfit)
                    treal_inv=realtime()-treal_inv
                    !store parameters as initial conditions for next run
                    if ((icnt.eq.1).or.(fitness.ge.fitness_old).or.piktry) then
                       iptbest=iptorig
                       iptbest%atm=atm
                       iptbest%blend=blend
                       iptbest%gen=gen
                       iptbest%line=lin
                       xyold=xyinfo(ibb,icnt,1)
                       fitness_old=fitness
                    end if
                    if (ipt%fitsout.eq.1) then
                       !store result in FITS file first convert the
                       !parameters to a par-vector, this can then be
                       !sent to the master and written to the fits file
                       cline(1:ipt%nline)=lin(1:ipt%nline)
                       catm(1:ipt%ncomp)=atm(1:ipt%ncomp)
                       cblend(1:ipt%nblend)=blend(1:ipt%nblend)
                       cgen=gen
                       !treat multi-iteration parameters correctly
                       !using fitflag instead of fit
                       catm(1:ipt%ncomp)%fit=ipt%atm(1:ipt%ncomp)%fitflag
                       call get_pikaia_idx(catm,cline,cblend,cgen,&
                            catm,cline,cblend,cgen,ipt%verbose)
                       call struct2pikaia(par(1:ipt%nfitpar),ipt%nfitpar,0_4,1_4,0_4)
                       if (pfmt.eq.'(i4.4)') then
                          xx=str2int(xyinfo(ibb,icnt,1)(2:5))
                          yy=str2int(xyinfo(ibb,icnt,1)(7:10))
                       else
                          xx=str2int(xyinfo(ibb,icnt,1)(2:4))
                          yy=str2int(xyinfo(ibb,icnt,1)(6:8))
                       end if
                       !check if old PIXELREP or POPLATION is used for statistics
                       if ((statdim(2).eq.0).or.&
                            (trim(ipt%pikaia_stat).eq.'NONE')) then
                          irep0=ipr
                          irep1=ipr
                          isort=1
                       else
                          irep0=1
                          irep1=ipt%pikaia_stat_cnt
                          call qsort(statfit,isort,int(pik_PMAX))
                          isort=isort(pik_PMAX:1:-1)
                       end if 
                       do iipr=irep0,irep1
                          if (ipt%pikaia_stat_cnt.ge.1) then
                             par=statpar(1:ipt%nfitpar,isort(iipr))
                             fitness=statfit(isort(iipr))
                             fit%i=statifit(:,isort(iipr))
                             fit%q=statqfit(:,isort(iipr))
                             fit%u=statufit(:,isort(iipr))
                             fit%v=statvfit(:,isort(iipr))
                          end if
#ifdef MPI
                          call fitsout_MPI_write(counter_comm,xx,yy,iipr,&
                               par,ipt%nfitpar, &
                               fit%nwl,fit%ic,fit%wl,&
                               fit%i,fit%q,&
                               fit%u,fit%v,&
                               fitness,ipt%keepbest)
#else
                          call fitsout_write(xx,yy,iipr,par,&
                               fit%nwl,fit%ic,fit%wl,&
                               fit%i,fit%q,&
                               fit%u,fit%v,&
                               fitness,ipt%keepbest)
#endif
                       end do
                       !st values back to best fit
                       par=statpar(1:ipt%nfitpar,isort(1))
                       fitness=statfit(isort(1))

                    else
                       !store result in individual ascii files
                       atm_path=ipt%dir%atm(1:len_trim(ipt%dir%atm))//'atm_'//&
                            ipt%observation(1:len_trim(ipt%observation))
                       if (atm_path(len_trim(atm_path):len_trim(atm_path)).eq.'/') &
                            atm_path=atm_path(1:len_trim(atm_path)-1)
                       if (len_trim(ipt%dir%atm_suffix).ge.1) then 
                          atm_path=atm_path(1:len_trim(atm_path))//'_'//&
                               ipt%dir%atm_suffix(1:len_trim(ipt%dir%atm_suffix))
                       end if
                       atm_path=atm_path(1:len_trim(atm_path))

                       if (ipr.ge.2) then
                          write(unit=pxrstr,fmt='(i5.5)') ipr-1
                          xypixrep=trim(xyinfo(ibb,icnt,1))//'.'//pxrstr
                       else
                          xypixrep=trim(xyinfo(ibb,icnt,1))
                       end if
                       call write_atm(atm,ipt%ncomp,lin,ipt%nline, &
                            blend,ipt%nblend,gen,fitness,fit, &
                            atm_path(1:len_trim(atm_path))//&
                            profname(ibb,icnt,1),xypixrep)
                       !copy input file to output directory
                       if (((myid.eq.0).or.(myid.eq.1)).and.(first)&
                            .and.(nptot.gt.1)) then
                          command='cp '//file(1:len_trim(file))//' '&
                               //atm_path(1:len_trim(atm_path))//'/input.ipt'
                          ierr=system(trim(command))
                          if (ierr.ne.0) then
                             write(*,*) 'Error in copying input file to atm_archive:'
                             write(*,*) trim(command)
                          else
                             write(*,*) 'Copied input file to ', &
                                  atm_path(1:len_trim(atm_path))//'/input.ipt'
                          end if
                       end if
                    end if
                 end if

                 if ((.not.piktry).and.&
                      (fitness.le.(ipt%piklm_minfit*fitness_old))) then
                    piktry=.true.
                    if (icnt.ge.2) icnt=icnt-1
                 else
                    piktry=.false.
                 end if

                 !time and load display
                 dt=(realtime()-treal_start)/real((ib),kind=8)
                 ttodo=dt*(real(nptot,kind=8)-&
                      real(ib,kind=8))
                 ttodo_sec=ttodo
                 !                 end if
                 if (realtime()-treal_last.gt.1e-5) then
                    cload=100_8*(cputime()-tcpu_last)/(realtime()-treal_last)
                 else
                    cload=100_8
                 end if
                 !compute fraction of time spent in inversion
                 treal_inv2tot=treal_inv/(realtime()-treal_last)
                 smchar='s'
                 if (ttodo.ge.600) then
                    ttodo=ttodo/60.0_8
                    smchar='m'
                    if (ttodo.ge.600) then
                       ttodo=ttodo/60.0_8
                       smchar='h'
                       if (ttodo.ge.96) then
                          ttodo=ttodo/24.0_8
                          smchar='d'
                       end if
                    end if
                 end if
                 if (.not.error) then
                 write(unit=pixstr,fmt='(a,a,i7,a,i7,a,f6.2,a)') &
                      trim(xyinfo(ibb,icnt,1)),':',ib,'/',nptot, &
                      ' ('//mstr(method(1)+1)//'=',fitness,','
                 write(*,fmt='(a,a,i3,a,a,i3,a,f5.1,a,a,i4,a,a14,a)') &
                      trim(pixstr), &
                      ' LD:',int(cload),'%,', &
                      ' I2T:',int(treal_inv2tot*100),'%),',ttodo,smchar, &
                      ' CPU',myid,' (',trim(procname(1:14)),')'
                 !Display warning if time spent outside inverisio is too long
                 if ((treal_inv2tot.le.0.70).and.(ipt%ncalls(1).ge.1)) then
                    write(*,fmt='(a,i3,a)') '%%% WARNING: only ',int(treal_inv2tot*100), &
                         '% of the time is spent in the ' // &
                         'inversion (I2T should be >90%).'
                    write(*,*) '             Check if your file system is slow.'
                 end if
                 !display estimated end time every 100th entry
                 if (mod(ib,100).eq.0) then
                    write(*,*) 'Estimated end time: ', ctime(time()+int(ttodo_sec))
                 end if
#ifdef MPI
                 if ((myid.eq.1).and.(numprocs.ge.2)) then
                    if ((ib/50).ne.(ibold/50)) then 
                       write(progunit,fmt='(a,a,i3,a,f5.1,a,a)') trim(pixstr), &
                            ' LD:',int(cload),'%,',ttodo,smchar,')'
                       ibold=ib
                    end if
                 end if
#endif
                 end if
                 tcpu_last=cputime()
                 treal_last=realtime()
                 first=.false.
              end if
              !           end do !pixel repetitions
              icnt=icnt+1
           end do !piklm-group
        end do !xy position

        ! END MPI
#ifdef MPI
        !send finish signal
        call mpi_barrier(smaller_comm,ierr)
        call MPE_Counter_free(smaller_comm,counter_comm, ierr )
     end if !counter is null counter (node 0)
     call MPI_FINALIZE(ierr)

     if ((myid.eq.1).and.(numprocs.ge.2)) then
        write(progunit,*) 'Done.'
        write(progunit,*) repeat('-',78)
        close(progunit)
     end if
#endif
     if ((myid.eq.0).and.(ipt%fitsout.eq.1)) then
        call fitsout_close(1)
     end if


     tcpu_stop=cputime()
     treal_stop=realtime()
     if ((treal_stop-treal_start).ge.600) then
        ms=60.0_8
        smchar='m'
     else
        smchar='s'
        ms=1.
     end if
     write(*,fmt='(i3,x,a20,a,f7.1,a,a,f7.1,a,a,f6.1,a)') &
          myid,trim(procname),' done. CPU ',(tcpu_stop-tcpu_start)/ms,smchar, &
          ', Real ',(treal_stop-treal_start)/ms,smchar, &
          ', Load ',100*(tcpu_stop-tcpu_start)/(treal_stop-treal_start),'%'
#ifdef PIKMPI
  elseif (myid.gt.0) then
     call ff_slave(atm(1)%npar+lin(1)%npar+blend(1)%npar+gen%npar)
  end if
#endif

#ifdef X11
  if ((wopen).and.(myid.eq.0)) then
     CALL DISFIN()
     wopen=.false.
  end if
#endif

#ifdef PIKMPI
  call mpi_finalize(ierr)
#endif

end program helix

SUBROUTINE STOP
  IMPLICIT NONE
#ifdef MPI
  include 'mpif.h'
#endif
  integer(4) :: ierr,myid
  ierr=0 ; myid=0
#ifdef MPI
  WRITE (*,*) 'STOP: CALLING MPI_ABORT...',myid
  CALL MPI_ABORT(MPI_COMM_WORLD,-1,IERR)
#endif
  STOP
END SUBROUTINE STOP

