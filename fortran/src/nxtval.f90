!adapted from MPE_Counter_create, _free, _nxtval
module nxtval
  integer(4),parameter :: server_rank=0
  integer(4),parameter :: REQUEST=200,GOAWAY=201,VALUE=202,FITS=203, &
       NXT_TAG=209, PROFTAG=99

#ifdef MPI
contains
  subroutine MPE_Counter_free(smaller_comm,counter_comm,ierr)
    implicit none
    !    use MPI
    include 'mpif.h'
    integer(4) :: smaller_comm,counter_comm
    integer(4) :: myid,numprocs,ierr,ierr0

    call MPI_Comm_size(counter_comm, numprocs,ierr)
    call MPI_Comm_rank(counter_comm, myid, ierr0)
    ierr=ierr+ierr0
    !    /* Make sure that all requests have been serviced */
    if (myid.eq.numprocs-1) then
       call MPI_Send(MPI_BOTTOM, 0, MPI_INTeger, server_rank, GOAWAY, &
            counter_comm,ierr0)
       ierr=ierr+ierr0
    end if
    call MPI_Comm_free( counter_comm, ierr0 )
    ierr=ierr+ierr0
    if (smaller_comm.ne.MPI_COMM_NULL) then
       call MPI_Comm_free( smaller_comm, ierr0 )
       ierr=ierr+ierr0
    end if
  end subroutine MPE_Counter_free

!checks for receive only every 10 ms, reduces CPU load on master CPU!
  subroutine mpi_recv_slow(BUF, COUNT, DATATYPE, SOURCE, TAG, &
       COMM, STATUS, IERROR)
    use tools
    implicit none
     include 'mpif.h'
    integer(4) :: buf,count,datatype,source,tag,comm,ierror
    integer(4) :: instatus(MPI_STATUS_SIZE),status(MPI_STATUS_SIZE)
    logical :: flag

    flag=.false.
    do while (.not.flag) 
!       call MPI_Iprobe(MPI_ANY_SOURCE,MPI_ANY_TAG, comm,flag,instatus,ierror) 
       call MPI_Iprobe(SOURCE,TAG, comm,flag,instatus,ierror) 
!       call sleepqq(10)
       call mysleep(10.)
    end do
    call MPI_recv(BUF,COUNT,DATATYPE,&
         instatus(MPI_SOURCE),instatus(MPI_TAG),COMM,STATUS,IERROR)
  end subroutine mpi_recv_slow

  subroutine MPE_Counter_create(old_comm,smaller_comm,counter_comm,ierr)
    use localstray_com
    use fitsout
    implicit none
    !    use MPI
    include 'mpif.h'
    integer(4) :: old_comm,smaller_comm,counter_comm,from
    integer(4) :: counter,message,done,myid,numprocs,color,ierr,ierr0,ii,nxny
    integer(4) :: status(MPI_STATUS_SIZE),intnwl,intmaxstr
    character(LEN=maxstr) :: profvec(maxpavg),xyvec(maxpavg),froot,mpitmp
    integer(2) :: np,datamode
    type (proftyp) :: obs
    logical :: error

    counter = 0
    done = 0
    ierr=0
    ierr0=0

    intmaxstr=int(maxstr)
    call MPI_Comm_size(old_comm, numprocs,ierr)
    call MPI_Comm_rank(old_comm, myid, ierr0)
    ierr=ierr+ierr0
    call MPI_Comm_dup( old_comm, counter_comm,ierr0 ) ! /* make one new comm */
    ierr=ierr+ierr0
    if (myid.eq.server_rank) then
       color = MPI_UNDEFINED
    else
       color = 0
    end if
    call MPI_Comm_split( old_comm, color, myid, smaller_comm, ierr0 )
    if (numprocs.eq.1) then
       write(*,*) 'only one processor.'
       return
    end if

    if (myid.eq.server_rank) then !       /* I am the server */
       do while (done.eq.0) 
          !receive data for FITS output, send it to master and write it
          !use own MPI_Recv routine: It checks only every 10 ms for a message
!          call MPI_Recv_slow(message, 1, MPI_integer,MPI_ANY_SOURCE, &
!               MPI_ANY_TAG, counter_comm, status, ierr0 )
          call MPI_Recv(message, 1, MPI_integer,MPI_ANY_SOURCE, &
               MPI_ANY_TAG, counter_comm, status, ierr0 )
          from=status(MPI_SOURCE)
          if (status(MPI_TAG).eq.FITS) then
             call fitsout_MPI_recv(from)
          else if (status(MPI_TAG).eq.PROFTAG) then
             call MPI_RECV(nxny,1,MPI_Integer4,from,PROFTAG-1,counter_comm, status, ierr0 )
             do ii=1,nxny
                call MPI_RECV(mpitmp,intmaxstr,MPI_Character,from,PROFTAG+1,counter_comm, status, ierr0 )
                profvec(ii)=mpitmp
                call MPI_RECV(mpitmp,intmaxstr,MPI_Character,from,PROFTAG+2,counter_comm, status, ierr0 )
                xyvec(ii)=mpitmp
             end do
             call MPI_RECV(np,1,MPI_Integer2,from,PROFTAG+3,counter_comm, status, ierr0 )
             call MPI_RECV(froot,intmaxstr,MPI_Character,from,PROFTAG+4,counter_comm, status, ierr0 )
             call MPI_RECV(datamode,1,MPI_Integer2,from,PROFTAG+5,counter_comm, status, ierr0 )
             call get_profile(profvec,xyvec,int(np,2),froot,&
                  datamode,obs,error)
             call MPI_SEND(obs%nwl,1,MPI_INTEGER2,from,PROFTAG+6,counter_comm,status,ierr)
             call MPI_SEND(obs%ic,1,MPI_REAL,from,PROFTAG+7,counter_comm,status,ierr)
             intnwl=int(obs%nwl)
             call MPI_SEND(obs%i,intnwl,MPI_REAL,from,PROFTAG+8,counter_comm,status,ierr)
             call MPI_SEND(obs%q,intnwl,MPI_REAL,from,PROFTAG+9,counter_comm,status,ierr)
             call MPI_SEND(obs%u,intnwl,MPI_REAL,from,PROFTAG+10,counter_comm,status,ierr)
             call MPI_SEND(obs%v,intnwl,MPI_REAL,from,PROFTAG+11,counter_comm,status,ierr)
             call MPI_SEND(obs%wl,intnwl,MPI_REAL8,from,PROFTAG+12,counter_comm,status,ierr)
             call MPI_SEND(error,1,MPI_LOGICAL,from,PROFTAG+13,counter_comm,status,ierr)
             call MPI_SEND(lsprof%nwl,1,MPI_INTEGER2,from,PROFTAG+14,counter_comm,status,ierr)
             intnwl=int(lsprof%nwl)
             call MPI_SEND(lsprof%i,intnwl,MPI_REAL,from,PROFTAG+15,counter_comm,status,ierr)
             call MPI_SEND(lsprof%q,intnwl,MPI_REAL,from,PROFTAG+16,counter_comm,status,ierr)
             call MPI_SEND(lsprof%u,intnwl,MPI_REAL,from,PROFTAG+17,counter_comm,status,ierr)
             call MPI_SEND(lsprof%v,intnwl,MPI_REAL,from,PROFTAG+18,counter_comm,status,ierr)
             call MPI_SEND(lsprof%wl,intnwl,MPI_REAL8,from,PROFTAG+19,counter_comm,status,ierr)
          else
          !original mpi_recv command
!          call MPI_Recv(message, 1, MPI_integer, MPI_ANY_SOURCE, &
!               MPI_ANY_TAG, counter_comm, status, ierr0 )       
          ierr=ierr+ierr0
          if (status(MPI_TAG).eq.REQUEST) then
             call  MPI_Send(counter, 1, MPI_INTeger4, status(MPI_SOURCE), &
                  VALUE, counter_comm, ierr0 )
             ierr=ierr+ierr0
             counter=counter+1
          else
             if (status(MPI_TAG).eq.GOAWAY) then
                done = 1
             else 
                write(*,fmt='(a,i5,a)') "bad tag ",status(MPI_TAG), &
                     " sent to MPE counter"
                call MPI_Abort(counter_comm, 1, ierr0 )
                ierr=ierr+ierr0
             end if
          end if
          end if
       end do
       call MPE_Counter_free( smaller_comm, counter_comm, ierr0 )
       ierr=ierr+ierr0
    end if
  end subroutine MPE_Counter_create

subroutine MPE_Counter_nxtval( counter_comm, val, ierr )
    implicit none
    !    use MPI
    include 'mpif.h'
    integer(4) :: counter_comm,val,ierr,ierr0
    integer(4) :: status(MPI_STATUS_SIZE)

    call MPI_Send(MPI_BOTTOM,0,MPI_INTeger,server_rank,REQUEST, &
         counter_comm,ierr0)
    ierr=ierr+ierr0
    call MPI_Recv(val,1,MPI_INTeger4,server_rank,VALUE, &
         counter_comm,status,ierr0)
    ierr=ierr+ierr0

end subroutine MPE_Counter_nxtval
  
#endif
end module nxtval
