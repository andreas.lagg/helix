program mtest
  implicit none
  integer(2) :: profcnt(:,:)
  allocatable profcnt
  integer(4) :: ip,icnt
  character(50) :: pixstr
  allocate(profcnt(1,1))

  profcnt=5

  icnt=1000
  do while (icnt.le.int(6000,kind=2))
     ip=int(icnt,kind=2)*int(10,kind=2)+sum(profcnt(:,:))-profcnt(1,1)
     write(unit=pixstr,fmt='(a,i6)') 'x=',int(ip,kind=4)
     write(*,'(a,i6)') pixstr,ip
     icnt=icnt+1
  end do
end program mtest
