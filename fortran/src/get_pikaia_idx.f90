  !get indices for atmospheric parameters
subroutine fit2arr(atm,natm,fit_atm)
  use all_type
  implicit none
  type (atmtyp) :: atm(maxatm)
  integer(2) :: ia,natm
  integer(2),dimension(natm,npartot) :: fit_atm

  do ia=1,natm
     fit_atm(ia,:)=(/ &
          atm(ia)%fit%b,         atm(ia)%fit%azi, &
          atm(ia)%fit%inc,       atm(ia)%fit%vlos,&
          atm(ia)%fit%width,     atm(ia)%fit%damp, &
          atm(ia)%fit%dopp,      atm(ia)%fit%a0 , &
          atm(ia)%fit%szero, &
          atm(ia)%fit%sgrad,     atm(ia)%fit%etazero,&
          atm(ia)%fit%gdamp,     atm(ia)%fit%vmici,&
          atm(ia)%fit%densp,     atm(ia)%fit%tempe,&
          atm(ia)%fit%dslab,     atm(ia)%fit%height,&
          atm(ia)%fit%ff /)
  end do
end subroutine fit2arr

!same for values
subroutine par2arr(atm,natm,par_atm)
  use all_type
  implicit none
  type (atmtyp) :: atm(maxatm)
  integer(2) :: ia,natm
  real(4),dimension(natm,npartot) :: par_atm

  do ia=1,natm
     par_atm(ia,:)=(/ &
          atm(ia)%par%b,         atm(ia)%par%azi, &
          atm(ia)%par%inc,       atm(ia)%par%vlos,&
          atm(ia)%par%width,     atm(ia)%par%damp, &
          atm(ia)%par%dopp,      atm(ia)%par%a0 , &
          atm(ia)%par%szero, &
          atm(ia)%par%sgrad,     atm(ia)%par%etazero,&
          atm(ia)%par%gdamp,     atm(ia)%par%vmici,&
          atm(ia)%par%densp,     atm(ia)%par%tempe,&
          atm(ia)%par%dslab,     atm(ia)%par%height,&
          atm(ia)%par%ff /)
  end do
end subroutine par2arr

!array of names
subroutine par2name(name)
  use all_type
  use tools
  implicit none
  character(LEN=idlen) :: name(npartot)
  type(parnametyp) :: pn

  call def_parname(pn)
  name=(/ &
       pn%b%ipt,         pn%azi%ipt, &
       pn%inc%ipt,       pn%vlos%ipt,&
       pn%width%ipt,     pn%damp%ipt, &
       pn%dopp%ipt,      pn%a0%ipt, &
       pn%szero%ipt, &
       pn%sgrad%ipt,     pn%etazero%ipt,&
       pn%gdamp%ipt,     pn%vmici%ipt,&
       pn%densp%ipt,     pn%tempe%ipt,&
       pn%dslab%ipt,     pn%height%ipt,&
       pn%ff%ipt /)
end subroutine par2name

!get indices for atmospheric parameters
subroutine blendfit2arr(blend,nblend,fit_blend)
  use all_type
  implicit none
  type (blendtyp) :: blend(maxblend)
  integer(2) :: ib,nblend
  integer(2),dimension(nblend,nblendpartot) :: fit_blend

  do ib=1,nblend
     fit_blend(ib,:)=(/ &
         blend(ib)%fit%wl,       blend(ib)%fit%width, &
         blend(ib)%fit%damp,     blend(ib)%fit%a0 /)
  end do
end subroutine blendfit2arr

  !get indices for general parameters
subroutine genfit2arr(gen,fit_gen)
  use all_type
  implicit none
  type (gentyp) :: gen
  integer(2),dimension(ngenpartot) :: fit_gen

  fit_gen(:)=(/ gen%fit%ccorr, gen%fit%straylight, gen%fit%radcorrsolar /)
end subroutine genfit2arr

                                !convert atm to parameter and back:
                                !coupled parameters are treated
                                !correctly, same for line parameters
subroutine get_pikaia_idx(atm,line,blend,gen, &
     atm_out,line_out,blend_out,gen_out,verbose)
  use all_type
  use ipt_decl
  implicit none
  type (atmtyp) :: atm(maxatm),atm_out(maxatm)
  type (blendtyp) :: blend(maxblend),blend_out(maxblend)
  type (gentyp) :: gen,gen_out
  type (linetyp) :: line(maxlin),line_out(maxlin)
  integer(2) :: ia,ip,il,set0,ib,verbose,iii
  integer(4) :: cidx,cidx0
  integer(2),dimension(act_ncomp,npartot) :: fit_atm,idx_atm,done_atm
  real(4),dimension(act_ncomp,npartot) :: par_atm,rat_atm
  character(LEN=idlen) :: name(npartot)
  integer(2),dimension(act_nblend,nblendpartot) :: fit_blend,idx_blend, &
       done_blend
  integer(2),dimension(ngenpartot) :: fit_gen,idx_gen,done_gen
  integer(2),dimension(act_nline,nlinpartot) :: fit_lin,idx_lin, &
       done_lin


  call fit2arr(atm,act_ncomp,fit_atm)
  call par2arr(atm,act_ncomp,par_atm)
  call blendfit2arr(blend,act_nblend,fit_blend)
  call genfit2arr(gen,fit_gen)

  !set at least one ff to 0
  if (1.eq.0) then
!     if (act_ncomp.eq.(sum((abs(1*int(fit_atm(1:act_ncomp,15).ne.0)))))) then
     if (act_ncomp.eq.count((/(fit_atm(1:act_ncomp,15).ne.0)/))) then
        set0=0
        do ia=1,act_ncomp
           if (fit_atm(ia,15).ne.0) set0=ia
        end do
        fit_atm(set0,15)=0
        write(*,*) 'only ncomp-1 filling factors are fit. '//&
             'Setting FF(comp=',set0,') to 0.'
     end if
  end if
  call par2name(name)
  idx_atm=0
  done_atm=0
  rat_atm=1
  cidx=0
  
  do ia=1,act_ncomp
     do ip=1,npartot
        if (fit_atm(ia,ip).ne.0) then
           if (fit_atm(ia,ip).gt.0) then !normal parameter to be fit
              cidx=cidx+1
              idx_atm(ia,ip)=cidx
              done_atm(ia,ip)=1
           else !treat coupled parameters
              if (done_atm(ia,ip).eq.0) then
                 cidx=cidx+1
                 idx_atm(ia,ip)=cidx
                 do iii=1,ia-1
                    if (fit_atm(iii,ip).eq.fit_atm(ia,ip)) then
                       idx_atm(ia,ip)=idx_atm(iii,ip)
                       if (iii.eq.1) cidx=cidx-1
                       if ((abs(par_atm(ia,ip)).gt.1e-4).or.&
                            par_atm(iii,ip).eq.par_atm(ia,ip)) then
                          if (par_atm(iii,ip).eq.par_atm(ia,ip)) then
                             rat_atm(ia,ip)=1.
                          else
                             rat_atm(ia,ip)=par_atm(iii,ip)/par_atm(ia,ip)
                          end if
                          if (verbose.ge.1) then 
                             write(*,*) 'Par ',trim(name(ip)),', Comp. ',ia, &
                                  ' coupled to Comp ',iii,'. Ratio: ', &
                                  rat_atm(ia,ip)
                          end if
                       else
                          write(*,*) 'Comp ',ia,': Cannot couple parameter ', &
                               name(ip),' with component ',iii, &
                               ' (ratio is infinity).'
                          stop
                       end if
                       done_atm(ia,ip)=1
                    end if
                 end do
              endif
           end if
        end if
     end do
  end do
  do ia=1,act_ncomp  !npartot variables
     atm(ia)%idx=fittyp(idx_atm(ia,1),idx_atm(ia,2),idx_atm(ia,3),&
          idx_atm(ia,4),idx_atm(ia,5),idx_atm(ia,6),idx_atm(ia,7),&
          idx_atm(ia,8),idx_atm(ia,9),idx_atm(ia,10),idx_atm(ia,11),&
          idx_atm(ia,12),idx_atm(ia,13),idx_atm(ia,14),idx_atm(ia,15), &
          idx_atm(ia,16),idx_atm(ia,17),idx_atm(ia,18))
     atm(ia)%ratio=partyp(rat_atm(ia,1),rat_atm(ia,2),rat_atm(ia,3),&
          rat_atm(ia,4),rat_atm(ia,5),rat_atm(ia,6),rat_atm(ia,7),&
          rat_atm(ia,8),rat_atm(ia,9),rat_atm(ia,10),rat_atm(ia,11),&
          rat_atm(ia,12),rat_atm(ia,13),rat_atm(ia,14),rat_atm(ia,15), &
          rat_atm(ia,16),rat_atm(ia,17),rat_atm(ia,18))
  end do
  atm%npar=cidx
  atm_out=atm

  !get indices for line parameters
  do il=1,act_nline
     fit_lin(il,:)=(/ &
          line(il)%fit%straypol_amp,line(il)%fit%straypol_eta0, &
          line(il)%fit%strength,line(il)%fit%wlshift/)
  end do

  idx_lin=0
  done_lin=0
  do il=1,act_nline
     do ip=1,nlinpartot
        if (fit_lin(il,ip).ne.0) then
           if (fit_lin(il,ip).gt.0) then !normal parameter to be fit
              cidx=cidx+1
              idx_lin(il,ip)=cidx
              done_lin(il,ip)=1
           else !treat coupled parameters
              if (done_lin(il,ip).eq.0) then
                 cidx=cidx+1
                 idx_lin(il,ip)=cidx
                 where(fit_lin.eq.fit_lin(il,ip))
                    idx_lin=idx_lin(il,ip)
                    done_lin=1
                 end where
              endif
           end if
        end if
     end do
  end do

  !nlinpartot variables
  do il=1,act_nline
     line(il)%idx= &
          linfit(idx_lin(il,1), & !straypol_amp
          idx_lin(il,2), & !straypol_eta0
          idx_lin(il,3), & !strength
          idx_lin(il,4) & !wlshift
          )          !straypol_eta0
     !             idx_lin(ia,il,3))          !gf
  end do
  line%npar=cidx-atm%npar
  line_out=line

  idx_blend=0
  done_blend=0
  cidx0=cidx
  do ib=1,act_nblend
     do ip=1,nblendpartot
        if (fit_blend(ib,ip).ne.0) then
           if (fit_blend(ib,ip).gt.0) then !normal parameter to be fit
              cidx=cidx+1
              idx_blend(ib,ip)=cidx
              done_blend(ib,ip)=1
           else !treat coupled parameters
              if (done_blend(ib,ip).eq.0) then
                 cidx=cidx+1
                 idx_blend(ib,ip)=cidx
                 where(fit_blend.eq.fit_blend(ib,ip))
                    idx_blend=idx_blend(ib,ip)
                    done_blend=1
                 end where
              endif
           end if
        end if
     end do
  end do

  do ib=1,act_nblend  !npartot varibbles
     blend(ib)%idx=blendfit(idx_blend(ib,1), idx_blend(ib,2), idx_blend(ib,3), idx_blend(ib,4))
  end do
  blend%npar=cidx-cidx0
  blend_out=blend

  idx_gen=0
  done_gen=0
  cidx0=cidx
  do ip=1,ngenpartot
     if (fit_gen(ip).ne.0) then
        if (fit_gen(ip).gt.0) then !normal parameter to be fit
           cidx=cidx+1
           idx_gen(ip)=cidx
           done_gen(ip)=1
        else !treat coupled parameters
           if (done_gen(ip).eq.0) then
              cidx=cidx+1
              idx_gen(ip)=cidx
              where(fit_gen.eq.fit_gen(ip))
                 idx_gen=idx_gen(ip)
                 done_gen=1
              end where
           endif
        end if
     end if
  end do

  gen%idx%ccorr=idx_gen(1)
  gen%idx%straylight=idx_gen(2)
  gen%idx%radcorrsolar=idx_gen(3)
  gen%npar=cidx-cidx0

  gen_out%idx=gen%idx
  gen_out%fit=gen%fit
  gen_out%par=gen%par
  gen_out%scale=gen%scale
  gen_out%npar=gen%npar
!  gen_out=gen

  
end subroutine get_pikaia_idx
