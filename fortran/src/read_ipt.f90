subroutine read_par(rword,nw,tline,nl,par,fit,scl,mi,fitflag)
  use all_type
  implicit none
  real(4) :: rword(50)
  real(4) :: par
  integer(2) :: fit,nw,nmi,i,nl,fitflag
  character(len=*) :: tline
  type (mmtyp) :: scl
  type (misub) :: mi
  intent (inout) rword,nw
  intent (in) tline,nl
  intent (out) par,fit,scl,mi

  if (nw.eq.2) then
     rword(1:4)=(/ rword(1),rword(1),rword(1),0. /)
  else
     if ((nw.lt.5).or.(mod(nw,2).ne.1)) then
        write(*,*) 'Wrong Parameter in line ',nl,': '
        write(*,*) trim(tline)
        write(*,*) 'Check number of entries for this line'
        write(*,*) 'Note: You must not use a parameter name as a first word in the comment block.'
        write(*,*) 'Format: '
        write(*,*) 'NAME    ini_val   min_value   max_val  fit/coupling'
        stop
     end if
  end if

  fitflag=0
  par=real(rword(1),kind=4)
  fit=int(rword(4))
  if (fit.ne.0) fitflag=1
  scl%min=rword(2)
  scl%max=rword(3)

  nmi=int((nw-4.)/2)
  do i=1,nmi
     mi%perc_rg(i)=rword(5+(i-1)*2)
     mi%fit(i)=int(rword(5+(i-1)*2+1))
     if (mi%fit(i).ne.0) fitflag=1
  end do
end subroutine read_par

subroutine checkmisspar(cnt,rg,pnm,error)
  use all_type
  use ipt_decl
  implicit none
#ifdef MPI
  include 'mpif.h'
#endif
  integer(2) :: cnt,error,ip
  type(mmtyp) :: rg
  type(iptyp) :: pnm
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
!  type (mmtyp) dummy
  logical :: val
  integer(4) :: myid,ierr

  myid=0
  ierr=0
#ifdef MPI
  call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
#endif

  val=.false.
  do ip=1,ipt%nparset
     val=(val.or.((trim(ipt%parset(ip))).eq.(trim(pnm%prg))))
  end do

  if ((val).and.(cnt.lt.ipt%ncomp)) then
     if (error.eq.0) then
       if (myid.eq.0) then
         write(*,*) 'Missing parameter for component ',cnt+1,', Suggestion:'
       end if
     end if
     if (myid.eq.0) then
       write(*,'(a5,3f16.2,i3)') pnm%ipt,(rg%min+rg%max)/2.,rg,1
     end if
     error=1
  end if

end subroutine checkmisspar

!!define input group
subroutine def_iptgroup(iptgroup)
  use all_type
  implicit none
  type (iptgrptyp) :: iptgroup(11)
  integer(2) :: i
  
  do i=1,10
     iptgroup(i)%show=1
  end do


  iptgroup(1)%name='COMMENTS'
  iptgroup(2)%name='DIRECTORIES'
  iptgroup(3)%name='CONTROL'
  iptgroup(4)%name='DATA SET'
  iptgroup(4)%name='POST PROCESSING'
  iptgroup(6)%name='ATMOSPHERES'
  iptgroup(7)%name='LINE PARAMETERS'
  iptgroup(8)%name='BLENDS'
  iptgroup(9)%name='GENERAL FIT PARAMETERS'
  iptgroup(10)%name='ANALYSIS METHOD'
  iptgroup(11)%name='PIKAIA PARAMATERS'

  iptgroup(2)%element(1)='PS'
  iptgroup(2)%element(2)='SAV'
  iptgroup(2)%element(3)='PROFILE_ARCHIVE'
  iptgroup(2)%element(4)='ATM_ARCHIVE'
  iptgroup(2)%element(5)='ATM_SUFFIX'
  iptgroup(2)%element(6)='WGT'
  iptgroup(2)%element(7)='ATOM'
  iptgroup(2)%default(1)='./ps/'
  iptgroup(2)%default(2)='./sav/'
  iptgroup(2)%default(3)='./profile_archive/'
  iptgroup(2)%default(4)='./atm_archive/'
  iptgroup(2)%default(5)='v01'
  iptgroup(2)%default(6)='./wgt/'
  iptgroup(2)%default(7)='./atom/'
  iptgroup(2)%comment(1)='directory for postscript output'
  iptgroup(2)%comment(2)='directory for storing results (sav-file)'
  iptgroup(2)%comment(3)='input directory for profiles / observations'
  iptgroup(2)%comment(4)='output directory for results (atmospheres)'
  iptgroup(2)%comment(5)='add a suffix to the atm-directory to identify this run'
  iptgroup(2)%comment(6)='directory for wgt-files'
  iptgroup(2)%comment(7)='directory for atomic data files'

  iptgroup(3)%element(1)='DISPLAY_MAP'
  iptgroup(3)%element(2)='DISPLAY_PROFILE'
  iptgroup(3)%element(3)='DISPLAY_COMP'
  iptgroup(3)%element(4)='VERBOSE'
  iptgroup(3)%element(5)='SAVE_FITPROF'
  iptgroup(3)%element(6)='FITSOUT'
  iptgroup(3)%element(7)='OUTPUT'
  iptgroup(3)%element(8)='OLD_NORM'
  iptgroup(3)%element(9)='OLD_VOIGT'
  iptgroup(3)%default(1:9)=(/'0','1','0','2','0','1','X','0','0'/)
  iptgroup(3)%comment(1)='if 1 then display maps of parameters after successful run (multiple pixels only!)'
  iptgroup(3)%comment(2)='if 1 then display fitted & observed profiles'
  iptgroup(3)%comment(3)='if 1 then display individual atm. components of fitted profiles'
  iptgroup(3)%comment(4)='verbosity of output: 0=quiet, 1=normal, 2=women'
  iptgroup(3)%comment(5)='if 1 then save fitted profile as sav-file (can be used as input profile)'
  iptgroup(3)%comment(6)='if 1 then write out map results in FITS format'
  iptgroup(3)%comment(7)='set to ''PS'' for postscript output'
  iptgroup(3)%comment(8)='use code with old normalization of the various atmospheric components'
  iptgroup(3)%comment(9)='if 1 use old normalization of Faraday/Voigt function'

  iptgroup(4)%element( 1)='OBSERVATION'
  iptgroup(4)%element( 2)='WL_RANGE'
  iptgroup(4)%element( 3)='WL_NUM'
  iptgroup(4)%element( 4)='WL_DISP'
  iptgroup(4)%element( 5)='WL_OFF'
  iptgroup(4)%element( 6)='WL_BIN'
  iptgroup(4)%element( 7)='XPOS'
  iptgroup(4)%element( 8)='YPOS'
  iptgroup(4)%element( 9)='PROFILE_LIST'
  iptgroup(4)%element(10)='STEPX'
  iptgroup(4)%element(11)='STEPY'
  iptgroup(4)%element(12)='AVERAGE'
  iptgroup(4)%element(13)='SCANSIZE'
  iptgroup(4)%element(14)='SYNTH'
  iptgroup(4)%element(15)='NOISE'
  iptgroup(4)%element(16)='INOISE'
  iptgroup(4)%element(17)='SMOOTH'
  iptgroup(4)%element(18)='MEDIAN'
  iptgroup(4)%element(19)='SPIKE'
  iptgroup(4)%element(20)='MIN_QUV'
  iptgroup(4)%element(21)='STRAYPOL_AMP'
!  iptgroup(4)%element(20)='CCORR'
  iptgroup(4)%element(22)='STRAYPOL_CORR'
  iptgroup(4)%element(23)='HANLE_AZI'
  iptgroup(4)%element(24)='SLIT_ORIENTATION'
  iptgroup(4)%element(25)='M1ANGLE'
  iptgroup(4)%element(26)='SOLAR_POS'
  iptgroup(4)%element(27)='SOLAR_RADIUS'
  iptgroup(4)%element(28)='HELIO_ANGLE'
  iptgroup(4)%element(29)='+Q2LIMB'
  iptgroup(4)%element(30)='HIN_SCANNR'
  iptgroup(4)%element(31)='NORM_STOKES'
  iptgroup(4)%element(32)='NORM_CONT'
  iptgroup(4)%element(33)='IC_LEVEL'
  iptgroup(4)%element(34)='LOCALSTRAY_RAD'
  iptgroup(4)%element(35)='LOCALSTRAY_FWHM'
  iptgroup(4)%element(36)='LOCALSTRAY_CORE'
  iptgroup(4)%element(37)='LOCALSTRAY_POL'
  iptgroup(4)%default( 1)='synth_default.profile.sav'
  iptgroup(4)%default( 2)='10825.0 10835.0'
  iptgroup(4)%default( 3)='256'
  iptgroup(4)%default( 4)='0'
  iptgroup(4)%default( 5)='0'
  iptgroup(4)%default( 6)='1'
  iptgroup(4)%default( 7)='000'
  iptgroup(4)%default( 8)='000'
  iptgroup(4)%default( 9)=''
  iptgroup(4)%default(10)='1'
  iptgroup(4)%default(11)='1'
  iptgroup(4)%default(12)='1'
  iptgroup(4)%default(13)='0'
  iptgroup(4)%default(14)='0'
  iptgroup(4)%default(15)='0.0'
  iptgroup(4)%default(16)='0.0'
  iptgroup(4)%default(17)='0 0'
  iptgroup(4)%default(18)='0'
  iptgroup(4)%default(19)='0'
  iptgroup(4)%default(20)='0.0'
  iptgroup(4)%default(21)='0.0'
!  iptgroup(4)%default(20)='1.00'
  iptgroup(4)%default(22)='0 B'
  iptgroup(4)%default(23)='0.'
  iptgroup(4)%default(24)='0.'
  iptgroup(4)%default(25)='0'
  iptgroup(4)%default(26)='0.0 0.0'
  iptgroup(4)%default(27)='950.'
  iptgroup(4)%default(28)='0.'
  iptgroup(4)%default(29)='0.'
  iptgroup(4)%default(30)='2'
  iptgroup(4)%default(31)='IC'
  iptgroup(4)%default(32)='LOCAL'
  iptgroup(4)%default(33)='0.'
  iptgroup(4)%default(34)='0.'
  iptgroup(4)%default(35)='0.'
  iptgroup(4)%default(36)='0.'
  iptgroup(4)%default(37)='1'
  iptgroup(4)%comment(1)='observation in one of two formats: 1.) tgz-file format ''obsname''.profiles.tgz, 2. spinor-profile'
  iptgroup(4)%comment(2)='WL-range to be used for analysis (may be only a part of the observation WL-range)'
  iptgroup(4)%comment(3)='# of WL-points (for synthesis only)'
  iptgroup(4)%comment(4)='WL-clibration: dispersion per WL-bin number'
  iptgroup(4)%comment(5)='WL-clibration: offset (used if != 0)'
  iptgroup(4)%comment(6)='wavelength binning'
  iptgroup(4)%comment(7)='two-elements vector containing xmin,xmax of the observation map to be analyzed'
  iptgroup(4)%comment(8)='two-elements vector containing ymin,ymax of the observation map to be analyzed'
  iptgroup(4)%comment(9)='filename of profile-list (2 columns: xxx yyy)'
  iptgroup(4)%comment(10)='step size for going from xmin to xmax'
  iptgroup(4)%comment(11)='step size for going from ymin to ymax'
  iptgroup(4)%comment(12)='if 1 then average observation over the stepx/stepy size'
  iptgroup(4)%comment(13)='stepsize of multiple scans within one observation'
  iptgroup(4)%comment(14)='if 1 then create synthetic profile'
  iptgroup(4)%comment(15)='noise level for adding artificial random noise (IQUV)'
  iptgroup(4)%comment(16)='noise level for adding random noise to Stokes I only'
  iptgroup(4)%comment(16)='smooth-value for profiles and smooth-method: (0=IDL-smooth function,1=FFT Low-Pass)'
  iptgroup(4)%comment(17)='median filter for observed profiles (0=off, >=2 median filter width)'
  iptgroup(4)%comment(18)='if >=1 then remove electronic spikes. Higher values remove broader spikes.'
  iptgroup(4)%comment(19)='min. mag. signal. If sqrt(Q^2+U^2+V^2)<MIN_QUV then the fit will be done with B=0.'
  iptgroup(4)%comment(20)='amplitude for stray-polarization (only used for synthesis)'
!  iptgroup(4)%comment(20)='factor for I (constant continuum correction)'
  iptgroup(4)%comment(21)='iteration steps and orientation of scattering-polarization correction.'//&
       'Orientation: ''B'' = along ''B''-field, X = a number defining an angle manually'
  iptgroup(4)%comment(22)='Hanle azimuth offset (experimental)'
  iptgroup(4)%comment(23)='slit-orientation (used for straypol-corr)'
  iptgroup(4)%comment(24)='angle of coelostat mirror M1'
  iptgroup(4)%comment(25)='pos. of observation (x and y in arcseconds)'
  iptgroup(4)%comment(26)='radius of sun in arcsec for time of obs.'
  iptgroup(4)%comment(27)='heliocentric angle of observation (for full Hanle)'
  iptgroup(4)%comment(28)='pos. Q to limb direction (for full Hanle)'
  iptgroup(4)%comment(29)='Hinode Scan# (repetetive scans)'
  iptgroup(4)%comment(30)='normalization of Stokes profiles: I or IC'
  iptgroup(4)%comment(32)='type of cont. normalization: local, slit or image'
  iptgroup(4)%comment(32)='set level for Ic. A positive number overwrites the Ic value contained in the '//&
       'observational data.'
  iptgroup(4)%comment(33)='add a local straylight component: take the average Stokes profile of the'//&
       ' surrounding profiles within the radius specified here and mix it via alpha to the other components'
  iptgroup(4)%comment(34)='FWHM of the Gaussian weighting used for the localstraylight profiles'
  iptgroup(4)%comment(35)='exclude core profiles within the radius specified here'
  iptgroup(4)%comment(36)='1=polarized / 0=unpolarized local straylight'

  iptgroup(5)%element(1)='CONV_FUNC'
  iptgroup(5)%element(2)='CONV_NWL'
  iptgroup(5)%element(2)='CONV_MODE'
  iptgroup(5)%element(4)='CONV_WLJITTER'
  iptgroup(5)%element(5)='CONV_OUTPUT'
  iptgroup(5)%element(6)='PREFILTER'
  iptgroup(5)%element(7)='PREFILTER_WLERR'
  iptgroup(5)%default(1)=' '
  iptgroup(5)%default(2)='0'
  iptgroup(5)%default(3)='FFT'
  iptgroup(5)%default(4)='0.0'
  iptgroup(5)%default(5)='0'
  iptgroup(5)%default(6)=' '
  iptgroup(5)%default(7)='0.0'
  iptgroup(5)%comment(1)='convolution function of instrument'
  iptgroup(5)%comment(2)='# of bins for convolution (used if number of WL_bins in data is small'
  iptgroup(5)%comment(3)='convolution method: FFT or MUL'
  iptgroup(5)%comment(4)='uncertainty in knowledge of exact wavelength for every filter position'//&
       '(eg. caused by error in etalon voltage)'//&
       'Make sure that the wavelength resolution is high enough '//&
       '(i.e. increase CONV_NWL). Must be set to ZERO for real data.'
  iptgroup(5)%comment(5)='flag to control the display/output of the fitted profile. If 1 then the profile is '//&
       'displayed with CONV_NWL WL pixels, if 0 the WL-pixels of the observation are used. NOTE: for local'//&
       ' straylight correction this flag is set to 0.'
  iptgroup(5)%comment(6)='prefilter curve'
  iptgroup(5)%comment(7)='error in knowledge of prefilter curve'//&
    ' (e.g. caused by unknown temperature). Must be set to ZERO for real data!'
  
  iptgroup(6)%element(1:23)=(/'NCOMP','BFIEL','AZIMU','GAMMA','VELOS', &
       'WIDTH','AMPLI','VDAMP','VDOPP','EZERO','SZERO', &
       'SGRAD','GDAMP','VMICI','DENSP','TEMPE','DSLAB','SLHGT', &
       'ALPHA','USE_A','USE_L','LINES','ATM_I'/)
  iptgroup(6)%element(20)='USE_ATOM'
  iptgroup(6)%element(21)='USE_LINE'
  iptgroup(6)%element(23)='ATM_INPUT'
  iptgroup(6)%comment( 1)='number of components'
  iptgroup(6)%comment( 2)='magnetic field value in Gauss'
  iptgroup(6)%comment( 3)='azimut of B-vector [deg]'
  iptgroup(6)%comment( 4)='inclination of B-vector [deg]'
  iptgroup(6)%comment( 5)='line-of-sight velocity in m/s'
  iptgroup(6)%comment( 6)='line width (Gauss only)'
  iptgroup(6)%comment( 7)='line ampl. (Gauss only)'
  iptgroup(6)%comment( 8)='damping constant (Voigt only)'
  iptgroup(6)%comment( 9)='doppler broadening (Voigt only)'
  iptgroup(6)%comment(10)='amplitude of components of propagation matrix (Voigt profile only!)'
  iptgroup(6)%comment(11)='source function at tau=0'
  iptgroup(6)%comment(12)='gradient of source function'
  iptgroup(6)%comment(13)='damping constant (Voigt profile phys. units only!)'
  iptgroup(6)%comment(14)='micro turbulence in m/s (Voigt profile phys. units only!)'
  iptgroup(6)%comment(15)='density parameter (Voigt profile phys. units only!)'
  iptgroup(6)%comment(16)='temperature (Voigt profile phys. units only!)'
  iptgroup(6)%comment(17)='optical thickness of He slab (hanle-slab model only!)'
  iptgroup(6)%comment(18)='He slab height in arcsec (hanle-slab model only!)'
  iptgroup(6)%comment(19)='Filling factor for this component (only with multiple components)'
  iptgroup(6)%comment(20)='atomic data file(s) to be used for this component'
  iptgroup(6)%comment(21)='Lines to be used for this component.'// &
  ' (Use photospheric atmospheres for photospheric lines only and simultaneously fit chromospheric lines)'
  iptgroup(6)%comment(22)='obsolete keyword: lines from USE_LINE parameter are used'
  iptgroup(6)%comment(23)='filename for input atmosphere: defines initial value'// &
       'for atmospheric parameters or delivers the input to synthesize a map.'
  iptgroup(6)%default(23)=' '
  
  iptgroup(7)%element(1)='LINE_ID      '
  iptgroup(7)%element(2)='LINE_STRENGTH'
  iptgroup(7)%element(3)='LINE_WLSHIFT '
  iptgroup(7)%comment( 1)='wavelength to identify the line'
  iptgroup(7)%comment( 2)='factor to adjust line strength'
  iptgroup(7)%comment( 3)='adjust central WL of line'     

  iptgroup(8)%element( 1)='NBLEND'
  iptgroup(8)%element( 2)='BLEND_WL'
  iptgroup(8)%element( 3)='BLEND_WIDTH'
  iptgroup(8)%element( 4)='BLEND_DAMP'
  iptgroup(8)%element( 5)='BLEND_A0'
  iptgroup(8)%default( 1)="1"
  iptgroup(8)%comment( 1)='number of telluric blends'
  iptgroup(8)%comment( 2)='WL and WL-range for blend'
  iptgroup(8)%comment( 3)='width of voigt-profile'
  iptgroup(8)%comment( 4)='damping of voigt-profile'
  iptgroup(8)%comment( 5)='amplitude of voigt-profile (blend is not used if BLEND_A0=0)'
  
  iptgroup(9)%element( 1)='CCORR     '
  iptgroup(9)%element( 2)='STRAYLIGHT'
  iptgroup(9)%element( 3)='RADCORRSOLAR'
  iptgroup(9)%default( 1)='1.'
  iptgroup(9)%default( 2)='0.'
  iptgroup(9)%default( 3)='1.'
  iptgroup(9)%comment( 1)='factor for continuum correction'
  iptgroup(9)%comment( 2)='spectral straylight correction (non-polarized, non-dispersive)'
  iptgroup(9)%comment( 3)='Correction for the intensity of the solar radiation field (for Hanle mode only)'
  

  iptgroup(10)%element(1)='APPROX_AZI'
  iptgroup(10)%element(2)='APPROX_DIR'
  iptgroup(10)%element(3)='APPROX_DEV_INC'
  iptgroup(10)%element(4)='APPROX_DEV_AZI'
  iptgroup(10)%element(5)='IQUV_WEIGHT'
  iptgroup(10)%element(6)='PROFILE'
  iptgroup(10)%element(7)='WGT_FILE'
  iptgroup(10)%element(9)='MAGOPT'
  iptgroup(10)%element(9)='USE_GEFF'
  iptgroup(10)%element(10)='USE_PB'
  iptgroup(10)%element(11)='PB_METHOD'
  iptgroup(10)%default(1)='0'
  iptgroup(10)%default(2)='0'
  iptgroup(10)%default(3)='20.'
  iptgroup(10)%default(4)='20.'
  iptgroup(10)%default(5)='1. 1. 1. 1.'
  iptgroup(10)%default(6)='voigt'
  iptgroup(10)%default(7)='1'
  iptgroup(10)%default(9)='0'
  iptgroup(10)%default(9)='0'
  iptgroup(10)%default(10)='0'
  iptgroup(10)%default(11)='poly'
  iptgroup(10)%comment( 1)='use approximation to calculate magnetic field '//&
  'azimuthal angle directly from Q and U profile (Auer, 1977)'
  iptgroup(10)%comment( 2)='use approximation for magnetic field direction. This keyword sets APPROX_AZI to 1.'//&
  ' This approximation for the inclination is not very accurate for low B and high inclinations'
  iptgroup(10)%comment( 3)='The approximation for the inclination angle of the magnetic field direction is used as an initial'//&
  ' value for the minimization. The value is allowed to vary +-X� around the approximation.'
  iptgroup(10)%comment( 4)='Same for azimuthal angle.'
  iptgroup(10)%comment( 5)='4-element vector defining relative weighting of IQUV (in that order).'//&
  'Additionally the code does an automatic weighting according to the strength of the I signal compared to the QUV signals.'
  iptgroup(10)%comment( 6)='file with WL-dependent weighting function for IQUV'
  iptgroup(10)%comment( 7)='functional form for pi- and sigma'//&
       ' components of spectral line. Available: gauss, voigt, voigt_phys, voigt_szero'
  iptgroup(10)%comment( 8)='include magneto-optical effects (dispersion coefficients, (Voigt profile only!))'
  iptgroup(10)%comment( 9)='use effective Lande factor (=1) or real Zeeman pattern (=0)'
  iptgroup(10)%comment(10)='if set, the zeeman-splitting and strength'//&
  ' includes the Paschen-Back effect (from the table by Socas-Navarro)'
  iptgroup(10)%comment(11)='use polynomials (=poly) or table interpolations (=table) to calculate the PB-effect'
  iptgroup(10)%nmipar((/1,2,3,4,5,8,9,10,11/))=1
  iptgroup(10)%nmipar(5)=4

  iptgroup(11)%element(1)='CODE'
  iptgroup(11)%element(2)='METHOD'
  iptgroup(11)%element(3)='NCALLS'
  iptgroup(11)%element(4)='CHI2METHOD'
  iptgroup(11)%element(5)='PIXELREP'
  iptgroup(11)%element(6)='KEEPBEST'
  iptgroup(11)%element(7)='PIKAIA_STAT'
  iptgroup(11)%element(8)='PIKAIA_POP'
  iptgroup(11)%default(1)='FORTRAN'
  iptgroup(11)%default(2)='PIKAIA'
  iptgroup(11)%default(3)='200'
  iptgroup(11)%default(4)='0'
  iptgroup(11)%default(5)='1'
  iptgroup(11)%default(6)='1'
  iptgroup(11)%default(7)=''
  iptgroup(11)%default(8)='0'
  iptgroup(11)%comment( 1)='PIKAIA code to use. Available: FORTRAN (=fast) or IDL (=platform independent).'
  iptgroup(11)%comment( 2)="minimization method: PIKAIA, POWELL (fast), LMDIFF (fast) or PIK_LM (combined, for maps only)"
  iptgroup(11)%comment( 3)='number of iterations in PIKAIA routine / max. number of calls for POWELL or LMDIF'
  iptgroup(11)%comment( 4)='method to calc. chi2 (JM = Borrero method)'
  iptgroup(11)%comment( 5)='number of repetitions for one pixel (for statistical analysis)'
  iptgroup(11)%comment( 6)='store result only if fitness is better than existing result (FITSOUT only)'
  iptgroup(11)%nmipar((/1,2/))=1
  iptgroup(11)%comment( 7)='switch on and control statistic module based on PIKAIA populations.'//&
       ' Requires FITSOUT=1 and PIXELREP=1. Example: POP75'
  iptgroup(11)%comment( 8)='number of PIKAIA populations (0=automatic)'

end subroutine def_iptgroup

!!Routine to read in input file
subroutine read_ipt(file)
  use all_type
  use ipt_decl
  use localstray_com
  use tools
  use pikvar
 implicit none
#ifdef MPI
  include 'mpif.h'
#endif
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  type (scltyp) :: pre
  type (linetyp) :: lin(maxlin)
  type (iptgrptyp) :: iptgrp(11)
  type(parnametyp) :: parname
  type (fittyp) :: cnt
  type (blendfit) :: blcnt
  character(LEN=*) :: file
  character(LEN=maxstr) :: word(50),uword(50),tline,mode(6),a,apar(npartot)
  integer(2) :: nparset(6)
  integer(2) :: iu,cindex,sindex,wgtcnt(maxmi),geffwarn,nl,ip,il,ic,eqat,it
  integer(2) :: i,j,nw,iword(50),ul_idx,ua_idx,linid,ii,ia,n,newlinegroup
  logical :: itmp,change_usepb
  real(4) :: rword(50),ffmin,ffmax,frc
  real(8) :: dword(50)
  integer(4) :: err,ierr,myid,nbl,lp_idx,idx,numprocs
 character(LEN=idlen) :: ids
! character(LEN=idlen) :: id2s
! character(LEN=maxstr) :: string
! real(4) :: str2real
!  external str2real
!  external id2s
!  external string
!  character(LEN=maxllen) :: add_slash
!  external add_slash
!  external norm_ff
  real(4) :: par
  integer(2) :: fit,fitflag,nline,parerr,damperr,verbose,newline!,npar
  integer(2) :: natmfit=0
  type (mmtyp) :: scl
  type (misub) :: mi
!  real(4),allocatable :: parvec(:)
  real(4) :: cpar(maxfitpar)
  type ltmp
     sequence
     real(8) :: id
     type (linpar) :: par
     type(linscl) :: scale
     type(linfit) :: fit
  end type ltmp
  type (ltmp) :: lptmp(maxlin)

  ul_idx=0
  ua_idx=0
  linid=0

  dols=0                        !default: do not do local straylight correction
  lspol=1

  iu=100
  open(iu,file=trim(file),iostat=err,action='READ',form='FORMATTED',status='OLD')

  if (err.ne.0) then
     write(*,*) 'Could not find input file ',trim(file),err
     stop  
  end if

  myid=0
  ierr=0
  numprocs=1
#ifdef MPI
  call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
  call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr ) 
#endif
  if (myid.eq.0) write(*,*) "Reading input file: ",trim(file)

  i=1
  do while (err.eq.0)
     read(iu, fmt='(a)',iostat=err) ipt%ascii(i)
     i=i+1
  end do
  ipt%n_ascii=i-2


  close(iu)

  call def_iptgroup(iptgrp)
  cnt=fittyp(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
  blcnt=blendfit(0,0,0,0)
  lp_idx=0

  ipt%hanle_slab(1)='B'
  ipt%hanle_slab(2)='AZI'
  ipt%hanle_slab(3)='INC'
  ipt%hanle_slab(4)='VLOS'
  ipt%hanle_slab(5)='DOPP'
  ipt%hanle_slab(6)='DAMP'
  ipt%hanle_slab(7)='DSLAB'
  ipt%hanle_slab(8)='HEIGHT'
  ipt%hanle_slab(9)='FF'

  ipt%voigt_phys(1)='B'
  ipt%voigt_phys(2)='AZI'
  ipt%voigt_phys(3)='INC'
  ipt%voigt_phys(4)='VLOS'
  ipt%voigt_phys(5)='GDAMP'
  ipt%voigt_phys(6)='VMICI'
  ipt%voigt_phys(7)='DENSP'
  ipt%voigt_phys(8)='TEMPE'
  ipt%voigt_phys(9)='SGRAD'
  ipt%voigt_phys(10)='FF'

  ipt%voigt_par(1)='B'
  ipt%voigt_par(2)='AZI'
  ipt%voigt_par(3)='INC'
  ipt%voigt_par(4)='VLOS'
  ipt%voigt_par(5)='DAMP'
  ipt%voigt_par(6)='DOPP'
  ipt%voigt_par(7)='SGRAD'
  ipt%voigt_par(8)='ETAZERO'
  ipt%voigt_par(9)='FF'

  ipt%voigt_gdamp(1)='B'
  ipt%voigt_gdamp(2)='AZI'
  ipt%voigt_gdamp(3)='INC'
  ipt%voigt_gdamp(4)='VLOS'
  ipt%voigt_gdamp(5)='GDAMP'
  ipt%voigt_gdamp(6)='DOPP'
  ipt%voigt_gdamp(7)='SGRAD'
  ipt%voigt_gdamp(8)='ETAZERO'
  ipt%voigt_gdamp(9)='FF'

  ipt%voigt_szero(1)='B'
  ipt%voigt_szero(2)='AZI'
  ipt%voigt_szero(3)='INC'
  ipt%voigt_szero(4)='VLOS'
  ipt%voigt_szero(5)='GDAMP'
  ipt%voigt_szero(6)='DOPP'
  ipt%voigt_szero(7)='SZERO'
  ipt%voigt_szero(8)='SGRAD'
  ipt%voigt_szero(9)='ETAZERO'
  ipt%voigt_szero(10)='FF'

  ipt%gauss_par(1)='B'
  ipt%gauss_par(2)='AZI'
  ipt%gauss_par(3)='INC'
  ipt%gauss_par(4)='VLOS'
  ipt%gauss_par(5)='WIDTH'
  ipt%gauss_par(6)='A0'
  ipt%gauss_par(7)='SGRAD'
  ipt%gauss_par(8)='FF'

  !set some default values
  ipt%ncalls(1)=10
  ipt%pixelrep=1
  ipt%keepbest=1
  ipt%dir%atm='./atm_archive/'
  ipt%dir%atm_suffix=''
  ipt%dir%ps='./ps/'
  ipt%dir%sav='./sav/' !not used for fortran version
  ipt%dir%wgt='./wgt/'
  ipt%wgt_file=' '
  ipt%atom_file='-'
  ipt%profile_list=' '
  ipt%atm%atm_input=' '
  ipt%conv_func=' '
  ipt%conv_nwl=0
  ipt%conv_mode=0
  ipt%conv_wljitter=0
  ipt%conv_output=0
  ipt%prefilter=' '
  ipt%approx_azi=0
  ipt%approx_dir=0


  ipt%display_comp=0
  ipt%old_norm=0
  ipt%old_voigt=0
  ipt%magopt=1
  ipt%use_geff=1
  ipt%use_pb=0
  ipt%pb_method=0
  ipt%iquv_weight=1
  ipt%average=1
  ipt%wl_range=0
  ipt%hanle_azi=0.
  ipt%norm_stokes='-'
  ipt%norm_cont=0
  ipt%ic_level=0.
  ipt%localstray_rad=0.
  ipt%localstray_fwhm=0.
  ipt%localstray_core=0.
  ipt%localstray_pol=1

  ipt%piklm_bx=1
  ipt%piklm_by=1
  ipt%piklm_minfit=0.

  ipt%wl_bin=1
  ipt%wl_off=0
  ipt%wl_disp=0

  ipt%noise=0.
  ipt%inoise=0.
  ipt%min_quv=0.

  ipt%nblend=0
  ipt%blend%par%a0=0

  ipt%chi2mode=0

  ipt%x=0
  ipt%y=0
  ipt%stepx=1
  ipt%stepy=1

  lptmp%par%strength=1.
  ipt%line%par%strength=1.

  ipt%gen%par%ccorr=1.
  ipt%gen%fit%ccorr=0.
  ipt%gen%scale%ccorr=mmtyp(0.8,1.2)
  ipt%gen%par%straylight=0.
  ipt%gen%fit%straylight=0.
  ipt%gen%scale%straylight=mmtyp(0.0,1.0)
  ipt%gen%par%radcorrsolar=1.
  ipt%gen%fit%radcorrsolar=0.
  ipt%gen%scale%radcorrsolar=mmtyp(0.1,10.)

  call def_range(pre)

  do i=1,ipt%n_ascii !loop over input file lines
     cindex=index(ipt%ascii(i),';')-1
     if (cindex.le.0) cindex=len(trim(ipt%ascii(i)))
     tline=trim(ipt%ascii(i)(1:cindex))
     j=1
     sindex=1
     word=''
     !split input line into words separated by spaces
     do while ((sindex.gt.0).and.(j.le.30).and.len(trim(tline)).gt.0)
        sindex=index(tline,' ')
        if (sindex.gt.1) then
           word(j)=tline(1:sindex-1)
           tline=trim(tline(sindex+1:))
           j=j+1
        else
           if (sindex.eq.1) tline=trim(tline(sindex+1:))
        end if
     end do
     nw=j-1
     if (word(1)(1:1).eq.';') nw=0
     do j=1,nw
        uword(j)=word(j)
        call upcase(uword(j))
     end do
     do j=2,nw
        rword(j-1)=str2real(word(j))
     end do
     do j=2,nw
        dword(j-1)=str2double(word(j))
     end do
     iword=int(rword)
     !query all keywords
     if (nw.gt.0) then
        select case (trim(uword(1)))
        case ('VERBOSE')
           ipt%verbose=iword(1)
           if ((numprocs.ge.3).and.(ipt%verbose.ge.1)) then
              if (myid.eq.0) write(*,*) 'MPI-mode: VERBOSE is set to 0'
              ipt%verbose=0
           end if
        case ('PS')
           ipt%dir%ps=word(2)
        case ('SAV')
           ipt%dir%sav=word(2)
        case ('PROFILE_ARCHIVE')
           ipt%dir%profile=word(2)
        case ('ATM_ARCHIVE')
           ipt%dir%atm=word(2)
        case ('ATM_SUFFIX')
           ipt%dir%atm_suffix=word(2)
        case ('WGT')
           ipt%dir%wgt=word(2)
        case ('ATOM')
           ipt%dir%atom=word(2)
        case ('DISPLAY_MAP')
           ipt%display_map=iword(1)
        case ('DISPLAY_PROFILE')
           ipt%display_profile=iword(1)
        case ('DISPLAY_COMP')
           ipt%display_comp=iword(1)
        case ('SAVE_FITPROF')
           ipt%save_fitprof=iword(1)
        case ('FITSOUT')
           ipt%fitsout=iword(1)
        case ('OLD_NORM')
           ipt%old_norm=iword(1)
        case ('OLD_VOIGT')
           ipt%old_voigt=iword(1)
        case ('OUTPUT')
           itmp=(uword(2).eq.'PS')
           ipt%ps=count((/itmp/))
        case ('OBSERVATION')
           ipt%observation=word(2)
        case ('XPOS')
           ipt%x(1:2)=iword(1)
           if (nw.ge.3) ipt%x(2)=iword(2)
        case ('YPOS')
           ipt%y(1:2)=iword(1)
           if (nw.ge.3) ipt%y(2)=iword(2)
        case ('PROFILE_LIST')
           if (nw.ge.2) ipt%profile_list=word(2)
        case ('NORM_STOKES')
           if (nw.ge.2) ipt%norm_stokes=uword(2)
        case ('NORM_CONT')
           select case (trim(uword(2)))
           case ('LOCAL')
              ipt%norm_cont=0
           case ('SLIT')
              ipt%norm_cont=1
           case ('IMAGE')
              ipt%norm_cont=2
           case default
              write(*,*) 'Unknown method for continuum normalization: '//&
                   trim(word(1))//&
                   '. Available methods: LOCAL, SLIT or IMAGE'
              call stop
           end select
        case ('IC_LEVEL')
           ipt%ic_level=rword(1)
        case ('STEPX')
           ipt%stepx=iword(1)
        case ('STEPY')
           ipt%stepy=iword(1)
        case ('AVERAGE')
           ipt%average=iword(1)
        case ('SCANSIZE')
           ipt%scansize=iword(1)
        case ('SYNTH')
           ipt%synth=iword(1)
        case ('NOISE')
           ipt%noise=rword(1)
        case ('INOISE')
           ipt%inoise=rword(1)
        case ('MIN_QUV')
           ipt%min_quv=rword(1)
        case ('CONV_FUNC')
           ipt%conv_func=word(2)
        case ('CONV_NWL')
           ipt%conv_nwl=iword(1)
           if (ipt%conv_nwl.ge.maxwl) ipt%conv_nwl=maxwl
        case ('CONV_MODE')
           select case (trim(uword(2)))
           case ('FFT')
              ipt%conv_mode=0
           case ('MUL')
              ipt%conv_mode=1
           case default
              write(*,*) 'Unknown method for CONV_MODE: '//&
                   trim(word(1))//&
                   '. Available methods: FFT, MUL'
              call stop
           end select
       case ('CONV_WLJITTER')
           ipt%conv_wljitter=rword(1)
        case ('CONV_OUTPUT')
           ipt%conv_output=iword(1)
        case ('PREFILTER')
           ipt%prefilter=word(2)
        case ('PREFILTER_WLERR')
           ipt%prefilter_wlerr=rword(1)
        case ('SMOOTH')
           ipt%smooth=iword(1)
           if (nw.ge.3) ipt%filter_mode=iword(2)
        case ('MEDIAN')
           ipt%median=iword(1)
        case ('SPIKE')
           ipt%spike=iword(1)
        case ('NCOMP')
           ipt%ncomp=iword(1)
           if (ipt%ncomp.gt.maxatm) then
              write(*,*) 'Maximum number of components is ',maxatm
              call stop
           end if
        case ('NBLEND')
           ipt%nblend=iword(1)
           if (ipt%nblend.gt.maxblend) then
              write(*,*) 'Maximum number of blends is ',maxblend
              call stop
           end if
        case ('BFIEL')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%b=cnt%b+1
           ipt%atm(cnt%b)%par%b=par
           ipt%atm(cnt%b)%fit%b=fit
           ipt%atm(cnt%b)%fitflag%b=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%b)%b=scl
           ipt%mi(cnt%b)%b=mi
        case ('AZIMU')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%azi=cnt%azi+1
           ipt%atm(cnt%azi)%par%azi=par
           ipt%atm(cnt%azi)%fit%azi=fit
           ipt%atm(cnt%azi)%fitflag%azi=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%azi)%azi=scl
           ipt%mi(cnt%azi)%azi=mi
        case ('GAMMA')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%inc=cnt%inc+1
           ipt%atm(cnt%inc)%par%inc=par
           ipt%atm(cnt%inc)%fit%inc=fit
           ipt%atm(cnt%inc)%fitflag%inc=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%inc)%inc=scl
           ipt%mi(cnt%inc)%inc=mi
        case ('VELOS')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%vlos=cnt%vlos+1
           ipt%atm(cnt%vlos)%par%vlos=par
           ipt%atm(cnt%vlos)%fit%vlos=fit
           ipt%atm(cnt%vlos)%fitflag%vlos=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%vlos)%vlos=scl
           ipt%mi(cnt%vlos)%vlos=mi
        case ('WIDTH')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%width=cnt%width+1
           ipt%atm(cnt%width)%par%width=par
           ipt%atm(cnt%width)%fit%width=fit
           ipt%atm(cnt%width)%fitflag%width=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%width)%width=scl
           ipt%mi(cnt%width)%width=mi
        case ('VDAMP')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%damp=cnt%damp+1
           ipt%atm(cnt%damp)%par%damp=par
           ipt%atm(cnt%damp)%fit%damp=fit
           ipt%atm(cnt%damp)%fitflag%damp=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%damp)%damp=scl
           ipt%mi(cnt%damp)%damp=mi
        case ('VDOPP')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%dopp=cnt%dopp+1
           ipt%atm(cnt%dopp)%par%dopp=par
           ipt%atm(cnt%dopp)%fit%dopp=fit
           ipt%atm(cnt%dopp)%fitflag%dopp=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%dopp)%dopp=scl
           ipt%mi(cnt%dopp)%dopp=mi
        case ('SGRAD')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%sgrad=cnt%sgrad+1
           ipt%atm(cnt%sgrad)%par%sgrad=par
           ipt%atm(cnt%sgrad)%fit%sgrad=fit
           ipt%atm(cnt%sgrad)%fitflag%sgrad=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%sgrad)%sgrad=scl
           ipt%mi(cnt%sgrad)%sgrad=mi
        case ('SZERO')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%szero=cnt%szero+1
           ipt%atm(cnt%szero)%par%szero=par
           ipt%atm(cnt%szero)%fit%szero=fit
           ipt%atm(cnt%szero)%fitflag%szero=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%szero)%szero=scl
           ipt%mi(cnt%szero)%szero=mi
        case ('EZERO')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%etazero=cnt%etazero+1
           ipt%atm(cnt%etazero)%par%etazero=par
           ipt%atm(cnt%etazero)%fit%etazero=fit
           ipt%atm(cnt%etazero)%fitflag%etazero=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%etazero)%etazero=scl
           ipt%mi(cnt%etazero)%etazero=mi
        case ('AMPLI')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%a0=cnt%a0+1
           ipt%atm(cnt%a0)%par%a0=par
           ipt%atm(cnt%a0)%fit%a0=fit
           ipt%atm(cnt%a0)%fitflag%a0=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%a0)%a0=scl
           ipt%mi(cnt%a0)%a0=mi
        case ('ALPHA')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%ff=cnt%ff+1
           ipt%atm(cnt%ff)%par%ff=par
           ipt%atm(cnt%ff)%fit%ff=fit
           ipt%atm(cnt%ff)%fitflag%ff=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%ff)%ff=scl
           ipt%mi(cnt%ff)%ff=mi
        case ('GDAMP')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%gdamp=cnt%gdamp+1
           ipt%atm(cnt%gdamp)%par%gdamp=par
           ipt%atm(cnt%gdamp)%fit%gdamp=fit
           ipt%atm(cnt%gdamp)%fitflag%gdamp=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%gdamp)%gdamp=scl
           ipt%mi(cnt%gdamp)%gdamp=mi
        case ('VMICI')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%vmici=cnt%vmici+1
           ipt%atm(cnt%vmici)%par%vmici=par
           ipt%atm(cnt%vmici)%fit%vmici=fit
           ipt%atm(cnt%vmici)%fitflag%vmici=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%vmici)%vmici=scl
           ipt%mi(cnt%vmici)%vmici=mi
        case ('DENSP')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%densp=cnt%densp+1
           ipt%atm(cnt%densp)%par%densp=par
           ipt%atm(cnt%densp)%fit%densp=fit
           ipt%atm(cnt%densp)%fitflag%densp=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%densp)%densp=scl
           ipt%mi(cnt%densp)%densp=mi
        case ('TEMPE')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%tempe=cnt%tempe+1
           ipt%atm(cnt%tempe)%par%tempe=par
           ipt%atm(cnt%tempe)%fit%tempe=fit
           ipt%atm(cnt%tempe)%fitflag%tempe=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%tempe)%tempe=scl
           ipt%mi(cnt%tempe)%tempe=mi
        case ('DSLAB')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%dslab=cnt%dslab+1
           ipt%atm(cnt%dslab)%par%dslab=par
           ipt%atm(cnt%dslab)%fit%dslab=fit
           ipt%atm(cnt%dslab)%fitflag%dslab=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%dslab)%dslab=scl
           ipt%mi(cnt%dslab)%dslab=mi
        case ('SLHGT')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           cnt%height=cnt%height+1
           ipt%atm(cnt%height)%par%height=par
           ipt%atm(cnt%height)%fit%height=fit
           ipt%atm(cnt%height)%fitflag%height=fitflag
           natmfit=natmfit+fitflag
           ipt%scale(cnt%height)%height=scl
           ipt%mi(cnt%height)%height=mi
        case('LINE_ID')
           lp_idx=lp_idx+1
           lptmp(lp_idx)%id=dword(1)
        case('LINE_STRENGTH')
          if (lp_idx.le.0) then
             write(*,*) 'Please define LINE_ID prior to LINE_STRENGTH'
             call stop
          end if
          lptmp(lp_idx)%par%strength=rword(1)
          lptmp(lp_idx)%scale%strength%min=rword(2)
          lptmp(lp_idx)%scale%strength%max=rword(3)
          lptmp(lp_idx)%fit%strength=iword(4)
        case('LINE_WLSHIFT')
          if (lp_idx.le.0) then
             write(*,*) 'Please define LINE_ID prior to LINE_WLSHIFT'
             call stop
          end if
          lptmp(lp_idx)%par%wlshift=rword(1)
          lptmp(lp_idx)%scale%wlshift%min=rword(2)
          lptmp(lp_idx)%scale%wlshift%max=rword(3)
          lptmp(lp_idx)%fit%wlshift=iword(4)
        case ('BLEND_WL')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           blcnt%wl=blcnt%wl+1
           ipt%blend(blcnt%wl)%par%wl=par
           ipt%blend(blcnt%wl)%fit%wl=fit
           ipt%blend(blcnt%wl)%scale%wl=scl
        case ('BLEND_A0')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           blcnt%a0=blcnt%a0+1
           ipt%blend(blcnt%a0)%par%a0=par
           ipt%blend(blcnt%a0)%fit%a0=fit
           ipt%blend(blcnt%a0)%scale%a0=scl
        case ('BLEND_WIDTH')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           blcnt%width=blcnt%width+1
           ipt%blend(blcnt%width)%par%width=par
           ipt%blend(blcnt%width)%fit%width=fit
           if ((scl%min.lt.pre%dopp%min).or.(scl%max.gt.pre%dopp%max)) then
              if (scl%min.lt.pre%dopp%min) scl%min=pre%dopp%min
              if (scl%max.gt.pre%dopp%max) scl%max=pre%dopp%max
              if (ipt%verbose.ge.1) write(*,*) 'Changed scaling: BLEND_WIDTH',&
                   scl%min,scl%max,', Blend ',blcnt%width
           end if
           ipt%blend(blcnt%width)%scale%width=scl
        case ('BLEND_DAMP')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           blcnt%damp=blcnt%damp+1
           ipt%blend(blcnt%damp)%par%damp=par
           ipt%blend(blcnt%damp)%fit%damp=fit
           if ((scl%min.lt.pre%damp%min).or.(scl%max.gt.pre%damp%max)) then
              if (scl%min.lt.pre%damp%min) scl%min=pre%damp%min
              if (scl%max.gt.pre%damp%max) scl%max=pre%damp%max
              if (ipt%verbose.ge.1) write(*,*) 'Changed scaling: BLEND_DAMP', &
                   scl%min,scl%max,', Blend ',blcnt%damp
           end if
           ipt%blend(blcnt%damp)%scale%damp=scl
        case ('CCORR')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           if ((fit.eq.1_2).and.((par.lt.scl%min).or.(par.gt.scl%max))) then
              if (par.lt.scl%min) par=scl%min
              if (par.gt.scl%max) par=scl%max
              if (ipt%verbose.ge.1) &
                   write(*,*) 'Changed initial value: CCORR',par
           end if
           ipt%gen%par%ccorr=par
           ipt%gen%fit%ccorr=fit
           ipt%gen%scale%ccorr=scl
        case ('STRAYLIGHT')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           if ((fit.eq.1_2).and.((par.lt.scl%min).or.(par.gt.scl%max))) then
              if (par.lt.scl%min) par=scl%min
              if (par.gt.scl%max) par=scl%max
              if (ipt%verbose.ge.1) &
                   write(*,*) 'Changed initial value: STRAYLIGHT',par
           end if
           ipt%gen%par%straylight=par
           ipt%gen%fit%straylight=fit
           ipt%gen%scale%straylight=scl
        case ('RADCORRSOLAR')
           call read_par(rword,nw,ipt%ascii(i),i,par,fit,scl,mi,fitflag)
           if ((fit.eq.1_2).and.((par.lt.scl%min).or.(par.gt.scl%max))) then
              if (par.lt.scl%min) par=scl%min
              if (par.gt.scl%max) par=scl%max
              if (ipt%verbose.ge.1) &
                   write(*,*) 'Changed initial value: RADCORRSOLAR',par
           end if
           ipt%gen%par%radcorrsolar=par
           ipt%gen%fit%radcorrsolar=fit
           ipt%gen%scale%radcorrsolar=scl
        case ('ATM_INPUT')
           if (nw.ge.2) ipt%atm(ua_idx+1)%atm_input=trim(word(2))
        case ('USE_ATOM')
           ua_idx=ua_idx+1
           do j=2,nw
              ipt%atom_file(ua_idx,j-1)=trim(word(j))
           end do
           !test if this atomic file was already
           !used. If yes, set linid to the index
           !of the component which already used
           !this atom, otherwise increase linid
           !by one
           !write(*,*) ua_idx,trim(word(2))
           ipt%atm(ua_idx)%linid=0
           do j=ua_idx-1,1,-1
              eqat=count((/ipt%atom_file(j,1:nw-1).eq.&
              ipt%atom_file(ua_idx,1:nw-1)/))
              if (eqat.eq.nw-1) ipt%atm(ua_idx)%linid=ipt%atm(j)%linid
           end do
           if (ipt%atm(ua_idx)%linid.eq.(0)) then
              ipt%atm(ua_idx)%linid=maxval(ipt%atm(1:ua_idx)%linid)+1
           end if
        case ('USE_LINE')
           ul_idx=ul_idx+1
           if (nw.gt.2) then
              write(*,*) 'Only one line group can act on an '//&
                   'atmospheric component. Define a linegroup in def_line.f90'
              stop
           end if
           do ii=1,len(trim(word(2)))
              ipt%atm(ul_idx)%use_line(ii,1)=ichar(word(2)(ii:ii))
           end do
           newlinegroup=1
           do ii=1,ul_idx-1
              if (trim(string(ipt%atm(ii)%use_line(1:len(trim(word(2))),1),2)).eq.trim(word(2))) then 
                 newlinegroup=0
              end if
           end do
           linid=linid+newlinegroup
           ipt%atm(ul_idx)%linid=linid
           write(*,*) 'Please use the new ''USE_ATOM'' keyword'//&
                ' instead of ''USE_LINE''!'
        case ('WL_RANGE')
           ipt%wl_range=real(rword(1:2),kind=8)
!           if (sum(ipt%wl_range**2).lt.1e-5) ipt%wl_range=(/0.,1e6/)
        case ('WL_DISP')
           ipt%wl_disp=real(rword(1),kind=8)
        case ('WL_OFF')
           ipt%wl_off=real(rword(1),kind=8)
        case ('WL_BIN')
           ipt%wl_bin=iword(1)
           if (ipt%wl_bin.lt.1) ipt%wl_bin=1
           if (ipt%wl_bin.ge.maxwl) ipt%wl_bin=maxwl
        case ('WL_NUM')
           ipt%wl_num=iword(1)
           if (ipt%wl_num.lt.2) ipt%wl_num=2
           if (ipt%wl_num.gt.maxwl) ipt%wl_num=maxwl
!        case ('CCORR')
!           ipt%ccorr=rword(1)
        case ('STRAYPOL_AMP')
           ipt%straypol_amp=rword(1)
        case ('HANLE_AZI')
           ipt%hanle_azi=rword(1)
        case ('STRAYPOL_CORR')
           ipt%straypol_corr=iword(1)
           if (nw.ge.3) then
              if (ipt%verbose.ge.2) &
                   write(*,*) 'Unused: orientation of scatter-pol dir'
           end if
        case ('HIN_SCANNR')
           ipt%obs_par%hin_scannr=iword(1)
        case ('SLIT_ORIENTATION')
           ipt%obs_par%slit_orientation=rword(1)
        case ('M1ANGLE')
           ipt%obs_par%m1angle=rword(1)
        case ('SOLAR_RADIUS')
           ipt%obs_par%solar_radius=rword(1)
        case ('+Q2LIMB')
           ipt%obs_par%posq2limb=rword(1)
        case ('HELIO_ANGLE')
           ipt%obs_par%heliocentric_angle=rword(1)
        case ('CODE')
           if ((ipt%verbose.ge.1).and.(trim(uword(2)).ne.'FORTRAN')) &
                write(*,*) 'CODE: only FORTRAN available'
           ipt%code='FORTRAN'
        case ('PIKAIA_STAT')
           ipt%pikaia_stat=uword(2)
        case ('METHOD')
           do j=2,nw
              select case (trim(uword(j)))
              case ('PIKAIA')
                 ipt%method(j-1:)=0
              case ('POWELL')
                 ipt%method(j-1:)=1
              case ('LMDIF')
                 ipt%method(j-1:)=2
              case ('LMDIFF')
                 ipt%method(j-1:)=2
              end select              
           end do
           if (word(2).eq.('PIK_LM')) then 
              ipt%method(1:)=3
              if (nw.ge.3) ipt%piklm_bx=iword(2)
              if (nw.ge.4) ipt%piklm_by=iword(3)
              if (nw.ge.5) ipt%piklm_minfit=rword(4)
           end if
        case ('NCALLS')
           ipt%ncalls(1:nw-1)=iword(1:nw-1)
           do j=1,nw-1  !number of multi iterations
              if (iword(j).gt.0) ipt%nmi=ipt%nmi+1
           end do
        case ('PIKAIA_POP')
           ipt%pikaia_pop=iword(1)
           if (ipt%pikaia_pop.gt.pik_PMAX) then
              write(*,*) 'PIKAIA_POP larger than maximum allowed value.'
              write(*,*) 'PIKAIA_POP=',ipt%pikaia_pop,', MAX=',pik_PMAX
              write(*,*) 'setting PIKAIA_POP=',pik_PMAX
              ipt%pikaia_pop=pik_PMAX
           endif
        case ('PIXELREP')
           ipt%pixelrep=iword(1)
           if (ipt%pixelrep.eq.0) ipt%pixelrep=1
        case ('KEEPBEST')
           ipt%keepbest=iword(1)
        case ('CHI2MODE')
          if (uword(2).eq.'JM') then 
            ipt%chi2mode=1
          end if
          if (uword(2).eq.'PLAIN') then 
            ipt%chi2mode=2
          end if
        case ('SOLAR_POS')
           ipt%obs_par%posx=rword(1)
           ipt%obs_par%posy=rword(2)
        case ('LOCALSTRAY_RAD')
           ipt%localstray_rad=rword(1)
        case ('LOCALSTRAY_FWHM')
           ipt%localstray_fwhm=rword(1)
        case ('LOCALSTRAY_CORE')
           ipt%localstray_core=rword(1)
        case ('LOCALSTRAY_POL')
           ipt%localstray_pol=iword(1)
        case ('APPROX_DIR')
           ipt%approx_dir(1:nw-1)=iword(1:nw-1)
        case ('UNNO')
           ipt%approx_dir=iword(1)
           write(*,*) 'UNNO is old keyword for old keyword for APPROX_DIR'
        case ('APPROX_AZI')
           ipt%approx_azi(1:nw-1)=iword(1:nw-1)
        case ('AZI_UNNO')
           ipt%approx_azi=iword(1) !old keyword for approx_azi
           write(*,*) 'UNNO is old keyword for old keyword for APPROX_AZI'
        case ('ESTIMATE_UNNO')
           write(*,*) 'keyword ESTIMATE_UNNO not supported anymore.' 
           write(*,*) 'Please use APPROX_AZI and APPROX_DIR'
           stop
        case ('APPROX_DEV_INC')
           ipt%approx_dev_inc(1:nw-1)=rword(1:nw-1)
        case ('APPROX_DEV_AZI')
           ipt%approx_dev_azi(1:nw-1)=rword(1:nw-1)
        case ('ESTIMATE_UNNO_RG_INC')
           ipt%approx_dev_inc=rword(1)
           write(*,*) 'ESTIMATE_UNNO_RG_INC is old keyword for' // &
                'old keyword for APPROX_DEV_INC'
        case ('ESTIMATE_UNNO_RG_AZI')
           ipt%approx_dev_azi=rword(1)
           write(*,*) 'ESTIMATE_UNNO_RG_AZI is old keyword for' // &
                'old keyword for APPROX_DEV_AZI'             
        case ('IQUV_WEIGHT')
           if (mod(nw-1,4).ne.0) then
              write(*,*) 'weight vector has to be vector with 4xn elements'
           end if
           do j=1,(nw-1)/4
              ipt%iquv_weight(:,j)=rword((j-1)*4+1:j*4)
           end do
           wgtcnt(1:(nw-1)/4)=1
        case ('WGT_FILE')
           ipt%wgt_file(1:nw-1)=word(2:nw)
           if (nw.eq.2) then
              ipt%wgt_file=word(2)
           end if
        case ('PROFILE')
           select case (trim(uword(2)))
           case ('GAUSS')
              ipt%prof_shape=0
           case ('VOIGT')
              ipt%prof_shape=1
           case default 
              ipt%prof_shape=1
              write(*,*) 'undefined profile-shape, use voigt.'
           end select
        case ('MAGOPT')
           ipt%magopt(1:nw-1)=iword(1:nw-1)
           if (nw.eq.1) ipt%magopt=iword(1)
        case ('USE_GEFF')
           ipt%use_geff(1:nw-1)=iword(1:nw-1)
           if (nw.eq.1) ipt%use_geff=iword(1)
        case ('USE_PB')
           ipt%use_pb(1:nw-1)=iword(1:nw-1)
           if (nw.eq.1) ipt%use_pb=iword(1)
        case ('PB_METHOD')
           do j=1,nw-1
              select case (trim(uword(1+j)))
              case ('POLY')
                 ipt%pb_method(j)=0
              case ('TABLE')
                 ipt%pb_method(j)=1
              case default
                 ipt%pb_method(j)=0
              end select
           end do
        case default  !all other words are treated as comment
           ipt%n_cmt=ipt%n_cmt+1
           ipt%comment(ipt%n_cmt)=trim(ipt%ascii(i))
        end select
     end if
  end do


  if (ipt%verbose.ge.2) write(*,*) 'Done reading input-file keywords.'

  if (ipt%synth.ne.0) ipt%keepbest=0

  select case(trim(ipt%pikaia_stat(1:3)))
  case('NON')
  case('')
     ipt%pikaia_stat='NONE'
  case('POP')
     if (ipt%verbose.ge.1) write(*,*) 'PIKAIA_STAT mode: ',trim(ipt%pikaia_stat)
  case default
     write(*,*) 'Undefined PIKAIA_STAT mode. Available modes are ''NONE'', ''POPxx'''
     write(*,*) 'Example: POP75 - take the fittest 75% of the PIKAIA population'
     write(*,*) 'Taking default value: ''NONE'''
     ipt%pikaia_stat='NONE'
  end select
  if ((trim(ipt%pikaia_stat).ne.'NONE').and.&
       ((ipt%fitsout.eq.0).or.(ipt%pixelrep.ge.2))) then 
     write(*,*) 'PIKAIA_STAT requires FITSOUT=1 and PIXELREP=1.'
     call stop
  end if
!  if ((trim(ipt%pikaia_stat).ne.'NONE').and.(ipt%keepbest.ne.0)) then
!     if (ipt%verbose.ge.1) write(*,*) 'PIKAIA_STAT mode is on. Set KEEPBEST=0.'
!     ipt%keepbest=0
!  end if

  nbl=maxval((/blcnt%wl,blcnt%a0,blcnt%damp,blcnt%width/))
  if (nbl.ne.ipt%nblend) then
     write(*,*) 'Wrong number of blends in your input file:'
     write(*,*) 'NBLEND=',ipt%nblend,', but # of blends = ',nbl
     call stop
  end if
  if (ipt%nblend.eq.0) then
     ipt%blend%par%a0=0
     ipt%nblend=1
  end if

   if (trim(ipt%norm_stokes).eq.'-') then
      ipt%norm_stokes='IC'
      if (ipt%verbose.ge.1) then
         write(*,*) 'No normalization for Stokes profiles specified. '
         write(*,*) '   Assuming ',trim(ipt%norm_stokes)
      end if
   end if
   if ((trim(ipt%norm_stokes).ne.'I').and.&
        (trim(ipt%norm_stokes).ne.'IC')) then
      write(*,*) 'Unknown normalization of Stokes profiles: ',&
           trim(ipt%norm_stokes)
      call stop
   end if
   if (ipt%verbose.ge.2) &
        write(*,*) 'Stokes normalization: ',trim(ipt%norm_stokes)
   if (trim(ipt%norm_stokes).eq.'I') ipt%norm_stokes_val=1
   if (trim(ipt%norm_stokes).eq.'IC') ipt%norm_stokes_val=0

  !check if path names end with trailing /
  ipt%dir%atm=add_slash(ipt%dir%atm)
  ipt%dir%profile=add_slash(ipt%dir%profile)
  ipt%dir%ps=add_slash(ipt%dir%ps)
  ipt%dir%wgt=add_slash(ipt%dir%wgt)

  !set flag for approx_dev
  ipt%approx_dev=count((/(ipt%approx_dev_inc.ne.0).or.&
       (ipt%approx_dev_azi.ne.0)/))

  !use new 'USE_ATOM_ keyword
  if (ul_idx.eq.0) then
     if ((ua_idx.eq.0).or.(ua_idx.ne.ipt%ncomp)) then 
        write(*,*) 'check USE_ATOM parameter: must be set for all components'
        stop
     end if
     
     !read in atomic data
     ipt%nline=0
     do ic=1,ipt%ncomp
        call read_atom(ipt%atom_file(ic,:),ipt%dir%atom, &
             nline,ipt%verbose,maxval(ipt%use_pb),lin)
        do il=1,nline
           ipt%atm(ic)%use_line(:,il)=lin(il)%id
        end do
        !remove multiple lines
        if (ic.eq.1) then
           ipt%line=lin
           nl=nline
        else
           nl=0
           do il=1,nline
              newline=1
              do ip=1,ipt%nline
                 if (count((/ipt%line(ip)%id.eq.lin(il)%id/)).eq.idlen) then
                    newline=0
                 end if
              end do
              if (newline.eq.1) then
                 if (ipt%nline+nl.gt.maxlin) then 
                    write(*,*) 'Exceeding max. number of lines. maxlin=',maxlin
                    stop
                 end if
                 nl=nl+1
                 ipt%line(ipt%nline+nl)=lin(il)
              end if
           end do
        end if
        ipt%nline=ipt%nline+nl
     end do
  else !old USE_LINE keyword
  !check if use_line parameter is set for every component and
  !replace shortcut by real line_id
     verbose=ipt%verbose
     do ia=1,ipt%ncomp
        if (ipt%atm(ia)%use_line(1,1).eq.0) then
           write(*,*) 'USE_LINE parameter is not set for component ',ia
           stop
        else
           call def_line(ia,lin,nline,verbose)
           do i=1,nline
              if (lin(i)%id(1).ne.0) then
                 ipt%atm(ia)%use_line(:,i)=lin(i)%id
                 if ((ipt%verbose.ge.2).and.(verbose.ne.0)) then
                    ids=id2s(ipt%atm(ia)%use_line(:,i))
                    write(*,*) 'Use Line ',i,ids,' for Component ',ia
                 end if
              end if
           end do
           verbose=0
        end if
     end do
     !define lines for all components
     call def_line(0_2,lin,nline,0_2)
     ipt%nline=nline
     ipt%line=lin
  end if
  
  !check if component is straypol
  !capcable
  do ic=1,ipt%ncomp
     ipt%atm(ic)%straypol_use=0
     do il=1,ipt%nline
        if ((ipt%atm(ic)%use_line(1,il).eq.72).and.&
             (ipt%atm(ic)%use_line(2,il).eq.101)) then
           ipt%atm(ic)%straypol_use=1
        end if
     end do
  end do
  


                                !fill the line-structure with the
                                !line-parameter values from the input
                                !file
  ipt%line%par%strength=1.
  do il=1,lp_idx 
     idx=0
     do it=1,ipt%nline
        if (abs(ipt%line(it)%wl-lptmp(il)%id).lt.1e-3) then
           idx=it
        end if
     end do
    if (idx.eq.0) then
       write(*,*) 'LINE_ID ',lptmp(il)%id,' not found in atomic data file.'
       call stop
    end if
    ipt%line(idx)%par%strength=lptmp(il)%par%strength
    ipt%line(idx)%fit%strength=lptmp(il)%fit%strength
    ipt%line(idx)%scale%strength=lptmp(il)%scale%strength
    ipt%line(idx)%par%wlshift=lptmp(il)%par%wlshift
    ipt%line(idx)%fit%wlshift=lptmp(il)%fit%wlshift
    ipt%line(idx)%scale%wlshift=lptmp(il)%scale%wlshift
  end do

  !check compatibility of settings with lines
  change_usepb=.true.
  do ic=1,ipt%ncomp
     do il=1,maxlin
        if (ipt%atm(ic)%use_line(1,il).gt.0) then
           ids=id2s(ipt%atm(ic)%use_line(:,il))
           if (trim(ids(1:2)).eq.'He') change_usepb=.false.
        end if
     end do
  end do
  if ((maxval(ipt%use_pb).gt.0).and.change_usepb) then
     write(*,*) 'USE_PB=1 only works for He 1083 nm.'
     ipt%use_pb=0
  end if

  mode=(/ &
       'gauss      ','voigt-par  ','voigt-phys ','voigt-gdamp', &
       'voigt-szero','hanle-slab ' /)
  nparset=(/ 8,9,10,9,10,9  /)
  !check used parameters to decide which mode to use:
  if ((cnt%dslab.ge.1).and.(cnt%height.ge.1)) then !hanle_slab mode
     ipt%modeval=5
     ipt%nparset=nparset(ipt%modeval+1)
     ipt%parset=ipt%hanle_slab
     if (ipt%verbose.ge.2) write(*,*) trim(mode(ipt%modeval+1))//' MODE'
     if (ipt%prof_shape.ne.1) then
        write(*,*) 'Profile shape not consistent with parameter set.'
        write(*,*) ' --> set profile shape to VOIGT'
        ipt%prof_shape=1
     end if
  elseif ((cnt%gdamp.ge.1).and.(cnt%vmici.ge.1).and.&
       (cnt%densp.ge.1).or.(cnt%tempe.ge.1)) then !voigt_phys mode
     ipt%modeval=2
     ipt%nparset=nparset(ipt%modeval+1)
     ipt%parset=ipt%voigt_phys
     if (ipt%verbose.ge.2) write(*,*) trim(mode(ipt%modeval+1))//' MODE'
     if (ipt%prof_shape.ne.1) then
        write(*,*) 'Profile shape not consistent with parameter set.'
        write(*,*) ' --> set profile shape to VOIGT'
        ipt%prof_shape=1
     end if
     !voigt mode
  elseif ((cnt%damp.ge.1).and.(cnt%dopp.ge.1).and.(cnt%etazero.ge.1)) then
     ipt%modeval=1
     ipt%nparset=nparset(ipt%modeval+1)
     ipt%parset=ipt%voigt_par
     if (ipt%verbose.ge.2) write(*,*) trim(mode(ipt%modeval+1))//' MODE'
     if (ipt%prof_shape.ne.1) then
        write(*,*) 'Profile shape not consistent with parameter set.'
        write(*,*) ' --> set profile shape to VOIGT'
        ipt%prof_shape=1
     end if
     !voigt-gdamp mode
  elseif ((cnt%gdamp.ge.1).and.(cnt%dopp.ge.1).and.(cnt%etazero.ge.1).and.(cnt%szero.ge.1)) then
     ipt%modeval=4
     ipt%nparset=nparset(ipt%modeval+1)
     ipt%parset=ipt%voigt_szero
     if (ipt%verbose.ge.2) write(*,*) trim(mode(ipt%modeval+1))//' MODE'
     if (ipt%prof_shape.ne.1) then
        write(*,*) 'Profile shape not consistent with parameter set.'
        write(*,*) ' --> set profile shape to VOIGT'
        ipt%prof_shape=1
     end if
  elseif ((cnt%gdamp.ge.1).and.(cnt%dopp.ge.1).and.(cnt%etazero.ge.1)) then
     ipt%modeval=3
     ipt%nparset=nparset(ipt%modeval+1)
     ipt%parset=ipt%voigt_gdamp
     if (ipt%verbose.ge.2) write(*,*) trim(mode(ipt%modeval+1))//' MODE'
     if (ipt%prof_shape.ne.1) then
        write(*,*) 'Profile shape not consistent with parameter set.'
        write(*,*) ' --> set profile shape to VOIGT'
        ipt%prof_shape=1
     end if
  elseif ((cnt%width.ge.1).and.(cnt%a0.ge.1)) then        !gauss mode
     ipt%modeval=0
     ipt%nparset=nparset(ipt%modeval+1)
     ipt%parset=ipt%gauss_par
     if (ipt%verbose.ge.2) write(*,*) trim(mode(ipt%modeval+1))//' MODE'
     if (ipt%prof_shape.ne.0) then
       write(*,*) 'Profile shape not consistent with parameter set.'
       write(*,*) ' --> set profile shape to GAUSS'
       ipt%prof_shape=0
     end if
     ipt%magopt=0
     ipt%use_geff=1
     ipt%use_pb=0
     ipt%pb_method=0
   else
     write(*,*) 'Incomplete atmospheric parameters.'
     write(*,*) 'Available parameter sets are:'
     write(*,*)
     apar=''
     do i=0,5 
       select case(i)
       case(0)
         apar=ipt%gauss_par
       case(1)
         apar=ipt%voigt_par
       case(2)
         apar=ipt%voigt_phys
       case(3)
         apar=ipt%voigt_gdamp
       case(4)
         apar=ipt%voigt_szero
       case(5)
         apar=ipt%hanle_slab
       case default
       end select
       write(*,*) 'MODE: ',trim(mode(i+1))
       write(*,*) 'Parameters: '
       a='' 
       do ip=1,nparset(i+1)
         a=trim(a)//' '//trim(prg2ipt(apar(ip)))
       end do
       write(*,*) trim(a)
     end do
     stop
   end if

  !check for missing parameters
  call def_range(pre)
  call def_parname(parname)
  parerr=0
  damperr=0
  call checkmisspar(cnt%b,pre%b,parname%b,parerr)
  call checkmisspar(cnt%azi,pre%azi,parname%azi,parerr)
  call checkmisspar(cnt%inc,pre%inc,parname%inc,parerr)
  call checkmisspar(cnt%vlos,pre%vlos,parname%vlos,parerr)
  call checkmisspar(cnt%width,pre%width,parname%width,parerr)
  call checkmisspar(cnt%damp,pre%damp,parname%damp,damperr)
  if (damperr.ne.0) then
    il=1
    do while ((ipt%line(il)%use_hanle_slab.eq.0).and.(il.lt.maxlin))
      il=il+1
    end do
    if (il.lt.maxlin) then
      if (myid.eq.0) then 
        write(*,*) '  Setting VDAMP for all components to default value '//&
           'for hanle_slab mode.'
        write(*,*) '  The default value is calculated from DOPP and corresponds'
        write(*,*) '  to thermal broadening effects only.'
    end if
      ipt%atm(1:ipt%ncomp)%fit%damp=0
      ipt%atm(1:ipt%ncomp)%par%damp=-1
    else
      parerr=1
    end if
  end if
  call checkmisspar(cnt%dopp,pre%dopp,parname%dopp,parerr)
  call checkmisspar(cnt%a0,pre%a0,parname%a0,parerr)
  call checkmisspar(cnt%sgrad,pre%sgrad,parname%sgrad,parerr)
  call checkmisspar(cnt%szero,pre%szero,parname%szero,parerr)
  call checkmisspar(cnt%etazero,pre%etazero,parname%etazero,parerr)
  call checkmisspar(cnt%gdamp,pre%gdamp,parname%gdamp,parerr)
  call checkmisspar(cnt%vmici,pre%vmici,parname%vmici,parerr)
  call checkmisspar(cnt%densp,pre%densp,parname%densp,parerr)
  call checkmisspar(cnt%tempe,pre%tempe,parname%tempe,parerr)
  call checkmisspar(cnt%dslab,pre%dslab,parname%dslab,parerr)
  call checkmisspar(cnt%height,pre%height,parname%height,parerr)
  call checkmisspar(cnt%ff+1_2,pre%ff,parname%ff,parerr)
  if (parerr.ne.0) stop


!  call check_scaling(cnt)

  !set weighting equal to first
  !weighting for multi iteration
  do i=2,ipt%nmi 
     if (wgtcnt(i).eq.0) then 
        ipt%iquv_weight(:,i)=ipt%iquv_weight(:,i-1)
     end if
  end do

  !add gauss fit to I only if
  !straylight-polarization correction is
  !chosen
  if (ipt%straypol_corr.gt.0) then
     ipt%nmi=ipt%nmi+1
     n=ipt%nmi
     ipt%ncalls(n)=ipt%straypol_corr
     ipt%method(n)=maxval(ipt%method(1:n))
     !use the same fitting rules as for the
     !normal components in the input file
     !(including coupling and fixing of
     !parameters)
     ipt%mi%b%fit(n)=ipt%atm%fit%b
     ipt%mi%azi%fit(n)=ipt%atm%fit%azi
     ipt%mi%inc%fit(n)=ipt%atm%fit%inc
     ipt%mi%damp%fit(n)=ipt%atm%fit%damp
     ipt%mi%dopp%fit(n)=ipt%atm%fit%dopp
     ipt%mi%sgrad%fit(n)=ipt%atm%fit%sgrad
     ipt%mi%szero%fit(n)=ipt%atm%fit%szero
     ipt%mi%etazero%fit(n)=ipt%atm%fit%etazero
     ipt%mi%gdamp%fit(n)=ipt%atm%fit%gdamp
     ipt%mi%vmici%fit(n)=ipt%atm%fit%vmici
     ipt%mi%densp%fit(n)=ipt%atm%fit%densp
     ipt%mi%tempe%fit(n)=ipt%atm%fit%tempe
     ipt%mi%dslab%fit(n)=ipt%atm%fit%dslab
     ipt%mi%height%fit(n)=ipt%atm%fit%height
     ipt%mi%ff%fit(n)=ipt%atm%fit%ff
     ipt%mi%vlos%fit(n)=ipt%atm%fit%vlos

     ipt%mi%b%perc_rg(n)=0.
     ipt%mi%azi%perc_rg(n)=0.
     ipt%mi%inc%perc_rg(n)=0.
     ipt%mi%damp%perc_rg(n)=0.
     ipt%mi%dopp%perc_rg(n)=0.
     ipt%mi%sgrad%perc_rg(n)=0.
     ipt%mi%szero%perc_rg(n)=0.
     ipt%mi%etazero%perc_rg(n)=0.
     ipt%mi%gdamp%perc_rg(n)=0.
     ipt%mi%vmici%perc_rg(n)=0.
     ipt%mi%densp%perc_rg(n)=0.
     ipt%mi%tempe%perc_rg(n)=0.
     ipt%mi%dslab%perc_rg(n)=0.
     ipt%mi%height%perc_rg(n)=0.
     ipt%mi%ff%perc_rg(n)=0.
     ipt%mi%vlos%perc_rg(n)=0.

     ipt%iquv_weight(:,n)=(/1,0,0,0/)
  end if

  !cannot have use_geff=1 and use_pb=1
  geffwarn=0
  do i=1,ipt%nmi
     if ((ipt%use_pb(i).eq.1).and.(ipt%use_geff(i).eq.1)) geffwarn=1
  end do
  if (geffwarn.eq.1) then
     write(*,*) 'Incompatible parameters: USE_GEFF=1 and USE_PB=1'
     stop
  end if

  if (ipt%modeval.eq.5) then !consistency checks for He slab model
     !check if one He line carries the flag 'use_hanle_slab'
     if (sum(ipt%line(1:ipt%nline)%use_hanle_slab).ne.1) then 
        write(*,*) 'Problem. Code should have set use_hanle_slab=1 for 1 line'
        call stop
     endif
  endif
  

                                !check if FF are within minmax values
  do ia=1,ipt%ncomp
     if ((ipt%atm(ia)%par%ff.lt.ipt%scale(ia)%ff%min).or.&
          (ipt%atm(ia)%par%ff.gt.ipt%scale(ia)%ff%max)) then
        ipt%atm(ia)%par%ff= &
        (ipt%scale(ia)%ff%min+ipt%scale(ia)%ff%max)/2.+ipt%scale(ia)%ff%min
        if (ipt%verbose.ge.1) then 
          write(*,*) 'FF for comp. ',ia, &
               ' is out of range. New value: ',ipt%atm(ia)%par%ff
       end if
    end if
 end do
    
  !add localstraylight component
  if (ipt%localstray_rad.ge.1e-5) then
    if (ipt%verbose.ge.2) write(*,*) 'Adding local straylight component.'
    dols=1
    lspol=ipt%localstray_pol
    ipt%ncomp=ipt%ncomp+1
    if (sum(ipt%atm(1:ipt%ncomp-1)%fit%ff).ne.0) ipt%atm(ipt%ncomp)%fit%ff=1
    ipt%scale(ipt%ncomp)%ff=pre%ff
                               !if FF of all other components is
                               !zero, then fit the first component
!    if (sum(ipt%atm(1:ipt%ncomp-1)%fit%ff).le.1) ipt%atm(1)%fit%ff=1 
    ipt%atm(ipt%ncomp)%par%ff=(1.-sum(ipt%atm(1:ipt%ncomp-1)%par%ff))
    if (ipt%atm(ipt%ncomp)%par%ff.le.0.01) ipt%atm(ipt%ncomp)%par%ff=0.01
    !reduce FFs for other components since a new
    !FF was added
!    ipt%atm(1:ipt%ncomp-1)%par%ff=ipt%atm(1:ipt%ncomp-1)%par%ff/ &
!         (sum(ipt%atm(1:ipt%ncomp-1)%fit%ff)+1)* &
!         sum(ipt%atm(1:ipt%ncomp-1)%fit%ff)
                                !make consistency checks for FF
    ffmin=sum(ipt%scale(1:ipt%ncomp-1)%ff%min)
    ffmax=sum(ipt%scale(1:ipt%ncomp-1)%ff%max)      
    ipt%scale(ipt%ncomp)%ff%min=1.-ffmax
    ipt%scale(ipt%ncomp)%ff%max=1.-ffmin      
!  ipt%atm(ipt%ncomp)%par%ff= &
!        (ipt%scale(ipt%ncomp)%ff%max-ipt%scale(ipt%ncomp)%ff%min)/2.
    ipt%atm(ipt%ncomp)%linid=ipt%atm(1)%linid
  end if

  !check scaling
  call check_scaling(cnt)


  !set sum of FF to one
  call norm_ff(ipt%atm(1:ipt%ncomp)%par%ff, &
       ipt%atm(1:ipt%ncomp)%fit%ff*0_2+1_2, &
       ipt%atm(1:ipt%ncomp)%linid,ipt%ncomp)

  if (ipt%ncomp.gt.1) then
     if (sum(ipt%atm(1:ipt%ncomp)%fit%ff).eq.1) then
        write(*,*) "PROBLEM:"
        write(*,*) "   Filling factor of only one component is free parameter!"
        write(*,*) "   Code has no freedom to adjust filling factors."
     end if
  end if

  !do not fit FF if only one component
  if (ipt%ncomp.eq.1) then
     ipt%atm%par%ff=1
     ipt%atm%fit%ff=0
     do i=1,ipt%nmi
        ipt%mi%ff%fit(i)=0
     end do
  end if
  
  !calculate heliocentric angle and +Q direction
  if (ipt%verbose.ge.2.and.ipt%modeval.eq.5) then
    write(*,*) 'Emission Vector: Read from input file'
    write(*,'(a,f7.2)')  '   Heliocentric Angle    = ',&
         ipt%obs_par%heliocentric_angle
    write(*,'(a,f7.2)')  '   +Q to limb direction  = ',ipt%obs_par%posq2limb
    write(*,'(a,3f7.2)') '   Hanle emission vector = ', &
         ipt%obs_par%heliocentric_angle,0., &
         mod(360.-ipt%obs_par%posq2limb+90.+360.,360.)
  endif

  if ((ipt%conv_output.eq.1).and.(dols.eq.1)) then 
    write(*,*) 'CONV_OUTPUT must be 0 when local straylight correction'//&
         ' is used. Continue with CONV_OUTPUT=0'
    ipt%conv_output=0
  end if
  
  act_ncomp=ipt%ncomp
  act_nblend=ipt%nblend
  act_nline=ipt%nline


  !fill in name of free parameters
  !fill in parameters required for fitting

  !treat multi-iteration parameters correctly using fitflag instead of fit
  catm=ipt%atm
  catm(1:ipt%ncomp)%fit=catm(1:ipt%ncomp)%fitflag
  call get_pikaia_idx(catm,ipt%line,ipt%blend,ipt%gen,&
          catm,cline,cblend,cgen,ipt%verbose)
  cnatm=ipt%ncomp
  cnline=ipt%nline
  cnblend=ipt%nblend
  ipt%nfitpar=natmfit+ipt%line(1)%npar+ipt%blend(1)%npar+ipt%gen%npar
  call struct2pikaia(cpar(1:ipt%nfitpar),ipt%nfitpar,0_4,1_4,1_4)
  ipt%fitpar=fitparname
  ipt%file=trim(file)

  if (ipt%pikaia_pop.eq.0) then
     ipt%pikaia_pop=(ipt%nfitpar*5)*2!/2
     if (ipt%pikaia_pop.le.36) ipt%pikaia_pop=36
!     if (ipt%pikaia_pop.ge.256) ipt%pikaia_pop=256
     if (ipt%pikaia_pop.ge.pik_PMAX) ipt%pikaia_pop=pik_PMAX
     if (ipt%verbose.ge.2) write(*,*) 'setting PIKAIA_POP=',ipt%pikaia_pop
  end if

  ipt%pikaia_stat_cnt=0
  if (trim(ipt%pikaia_stat).ne.'NONE') then
     frc=str2real(trim(ipt%pikaia_stat(4:14)))/100.
     ipt%pikaia_stat_cnt=int(ipt%pikaia_pop*frc)
     if (ipt%pikaia_stat_cnt.le.1) ipt%pikaia_stat_cnt=1
     if (ipt%verbose.ge.1) &
          write(*,*) 'PIKAIA_STAT=',trim(ipt%pikaia_stat),': use ', ipt%pikaia_stat_cnt,' of ',ipt%pikaia_pop,' populations'
  end if

  if (ipt%verbose.ge.1) write(*,*) 'Done reading input file ',trim(ipt%file)

  if (myid.eq.0) then 
     write(*,*) 'Comments: '
     do i=1,ipt%n_cmt
        if (trim(ipt%comment(i)).ne.'') write(*,*) trim(ipt%comment(i))
     end do
  end if


end subroutine read_ipt
