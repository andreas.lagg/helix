function maxcouple() result (retval)
  use all_type
  use ipt_decl
  implicit none
!  type (ipttyp) :: ipt
  integer(2) :: cidx(maxatm,npartot)
  integer(2) :: retval

  cidx=reshape((/ipt%atm%fit%b,ipt%atm%fit%azi,ipt%atm%fit%inc, &
       ipt%atm%fit%vlos,ipt%atm%fit%width,ipt%atm%fit%damp, &
       ipt%atm%fit%dopp,ipt%atm%fit%a0,ipt%atm%fit%szero,ipt%atm%fit%sgrad, &
       ipt%atm%fit%etazero,ipt%atm%fit%gdamp,ipt%atm%fit%vmici, &
       ipt%atm%fit%densp,ipt%atm%fit%tempe, &
       ipt%atm%fit%dslab,ipt%atm%fit%height, &
       ipt%atm%fit%ff/), &
       (/maxatm,npartot/))
  retval=maxval(abs(cidx))
end function maxcouple

function docheck(iptscl,cnt,rg,name,ncomp,fit,par) result (retval)
  use all_type
  implicit none
  character(LEN=*) :: name
  type (mmtyp) :: rg    
  integer(2) :: cs,cnt,ncomp,fit
  real(4) :: par
  type (mmtyp) :: iptscl,retval

  if (cnt.gt.ncomp) then
     write(*,*) 'Too many parameters: ',name
     write(*,*) 'You defined only NCOMP=',ncomp,' atmospheres.'
     stop
  end if

  retval=iptscl
  if (fit.ne.0) then

     cs=0
     if (iptscl%min.lt.rg%min) then
        cs=1
        retval%min=rg%min
     end if
     if (iptscl%max.gt.rg%max) then
        cs=1
        retval%max=rg%max
     end if
     if (iptscl%min.eq.iptscl%max) then 
        cs=1
        retval%min=rg%min
        retval%max=rg%max
     end if

     if (par.lt.retval%min) then
        par=retval%min
        cs=1
     end if
     if (par.gt.retval%max) then
        par=retval%max
        cs=1
     end if

     if (cs.eq.1) then
        write(*,*) 'Changed scaling: ',name,retval%min,retval%max,&
             ', Comp. ',ncomp
     end if
  end if
  return
end function docheck

subroutine def_range(pre)
  use all_type
  implicit none
  type (scltyp) :: pre

  pre%b      =mmtyp(0.,10000.)
  pre%azi    =mmtyp(-180.,180.)
  pre%inc    =mmtyp( 0.,180.)
  pre%vlos   =mmtyp(-5e5,5e5)
  pre%width  =mmtyp(0.001,5.)
  pre%damp   =mmtyp(1e-4,500.0)
  pre%dopp   =mmtyp(1e-4,50.0)
  pre%a0     =mmtyp(0.,10.)
  pre%szero  =mmtyp(-10.,10.)
  pre%sgrad  =mmtyp(-100.,100.)
  pre%etazero=mmtyp(0.,100000.)
  pre%gdamp  =mmtyp(1e-3,20.)
  pre%vmici  =mmtyp(0.,1e5)
  pre%densp  =mmtyp(0.0,100.)
  pre%tempe  =mmtyp(0.0,20000.)
  pre%dslab  =mmtyp(0.01,5.)
  pre%height =mmtyp(0.01,100.)
  pre%ff     =mmtyp(0.001,0.999)

end subroutine def_range

!check if scaling lies inside allowed range
subroutine check_scaling(cnt)
  use all_type
  use ipt_decl
  use tools
  implicit none
  external norm_ff
  interface
     function docheck(scl,cnt,rg,name,ncomp,fit,par) result (res)
       use all_type
       character(LEN=*) :: name
       integer(2) :: cnt,ncomp,fit
       real(4) :: par
       type (mmtyp) :: scl,res,rg
     end function docheck
     function maxcouple() result (res)
       integer(2) :: res
     end function maxcouple
  end interface
!  type (ipttyp) :: ipt
  type (fittyp) :: cnt
  type (scltyp) :: pre
  integer(2) :: ia,cmax

  call def_range(pre)
  do ia=1,ipt%ncomp
     ipt%scale(ia)%b = &
          docheck(ipt%scale(ia)%b,   cnt%b,   pre%b, 'BFIEL', &
          ipt%ncomp,ipt%atm(ia)%fit%b,ipt%atm(ia)%par%b)
     ipt%scale(ia)%azi = &
          docheck(ipt%scale(ia)%azi,cnt%azi,  pre%azi, 'AZIMU', &
          ipt%ncomp,ipt%atm(ia)%fit%azi,ipt%atm(ia)%par%azi)
     ipt%scale(ia)%inc = &
          docheck(ipt%scale(ia)%inc,cnt%inc, pre%inc, 'GAMMA', &
          ipt%ncomp,ipt%atm(ia)%fit%inc,ipt%atm(ia)%par%inc)
     ipt%scale(ia)%vlos = &
          docheck(ipt%scale(ia)%vlos,cnt%vlos,pre%vlos, 'VELOS', &
          ipt%ncomp,ipt%atm(ia)%fit%vlos,ipt%atm(ia)%par%vlos)
     ipt%scale(ia)%width = &
          docheck(ipt%scale(ia)%width,cnt%width,pre%width,'WIDTH', &
          ipt%ncomp,ipt%atm(ia)%fit%width,ipt%atm(ia)%par%width)
     ipt%scale(ia)%a0 = &
          docheck(ipt%scale(ia)%a0,cnt%a0,pre%a0,'AMPLI', &
          ipt%ncomp,ipt%atm(ia)%fit%a0,ipt%atm(ia)%par%a0)
     ipt%scale(ia)%sgrad = &
          docheck(ipt%scale(ia)%sgrad,cnt%sgrad,pre%sgrad,'SGRAD', &
          ipt%ncomp,ipt%atm(ia)%fit%sgrad,ipt%atm(ia)%par%sgrad)
     ipt%scale(ia)%szero = &
          docheck(ipt%scale(ia)%szero,cnt%szero,pre%szero,'SZERO', &
          ipt%ncomp,ipt%atm(ia)%fit%szero,ipt%atm(ia)%par%szero)
     ipt%scale(ia)%gdamp = &
          docheck(ipt%scale(ia)%gdamp,cnt%gdamp,pre%gdamp,'GDAMP', &
          ipt%ncomp,ipt%atm(ia)%fit%gdamp,ipt%atm(ia)%par%gdamp)
     ipt%scale(ia)%vmici = &
          docheck(ipt%scale(ia)%vmici,cnt%vmici,pre%vmici,'VMICI', &
          ipt%ncomp,ipt%atm(ia)%fit%vmici,ipt%atm(ia)%par%vmici)
     ipt%scale(ia)%densp = &
          docheck(ipt%scale(ia)%densp,cnt%densp,pre%densp,'DENSP', &
          ipt%ncomp,ipt%atm(ia)%fit%densp,ipt%atm(ia)%par%densp)
     ipt%scale(ia)%tempe = &
          docheck(ipt%scale(ia)%tempe,cnt%tempe,pre%tempe,'TEMPE', &
          ipt%ncomp,ipt%atm(ia)%fit%tempe,ipt%atm(ia)%par%tempe)
     ipt%scale(ia)%dslab = &
          docheck(ipt%scale(ia)%dslab,cnt%dslab,pre%dslab,'DSLAB', &
          ipt%ncomp,ipt%atm(ia)%fit%dslab,ipt%atm(ia)%par%dslab)
     ipt%scale(ia)%height = &
          docheck(ipt%scale(ia)%height,cnt%height,pre%height,'SLHGT', &
          ipt%ncomp,ipt%atm(ia)%fit%height,ipt%atm(ia)%par%height)
     ipt%scale(ia)%etazero = &
          docheck(ipt%scale(ia)%etazero,cnt%etazero,pre%etazero,'EZERO', &
          ipt%ncomp,ipt%atm(ia)%fit%etazero,ipt%atm(ia)%par%etazero)
     ipt%scale(ia)%ff = &
          docheck(ipt%scale(ia)%ff,cnt%ff,pre%ff,'ALPHA', &
          ipt%ncomp,ipt%atm(ia)%fit%ff,ipt%atm(ia)%par%ff)
     ipt%scale(ia)%damp = &
          docheck(ipt%scale(ia)%damp,cnt%damp,pre%damp,'VDAMP', &
          ipt%ncomp,ipt%atm(ia)%fit%damp,ipt%atm(ia)%par%damp)
     ipt%scale(ia)%dopp = &
          docheck(ipt%scale(ia)%dopp,cnt%dopp,pre%dopp,'VDOPP', &
          ipt%ncomp,ipt%atm(ia)%fit%dopp,ipt%atm(ia)%par%dopp)
  end do

  !set sum of FF to one
  call norm_ff(ipt%atm(1:ipt%ncomp)%par%ff, &
       ipt%atm(1:ipt%ncomp)%fit%ff*0_2+1_2, &
       ipt%atm(1:ipt%ncomp)%linid,ipt%ncomp)
  

                                !source function has to be the same
                                !for all components: set coupling to
                                !one and scaling to max val in input file
!  cmax=maxcouple(ipt)
!  if (ipt%verbose.ge.1) write(*,*) 'Couple SGRAD for all components'
!  do ia=1,ipt%ncomp
!     if (ipt%atm(ia)%fit%sgrad.ne.0) then
!        ipt%atm(ia)%fit%sgrad=-cmax-1
!        ipt%scale(ia)%sgrad%min=minval(ipt%scale(1:ipt%ncomp)%sgrad%min)
!        ipt%scale(ia)%sgrad%max=maxval(ipt%scale(1:ipt%ncomp)%sgrad%max)
!     end if
!  end do

!do not couple eta0
  if (1.eq.0) then
   
                                !eta_zero has to be the same
                                !for all components, relative ratio is
                                !treated by loggf: set coupling to
                                !one and scaling to max val in input file
  cmax=maxcouple()
  if (ipt%verbose.ge.1) write(*,*) 'Couple ETAZERO for all components'
  do ia=1,ipt%ncomp
     if (ipt%atm(ia)%fit%etazero.ne.0) then
        ipt%atm(ia)%fit%etazero=-cmax-1
        ipt%scale(ia)%etazero%min=minval(ipt%scale(1:ipt%ncomp)%etazero%min)
        ipt%scale(ia)%etazero%max=maxval(ipt%scale(1:ipt%ncomp)%etazero%max)
     end if
  end do
end if
   
end subroutine check_scaling
