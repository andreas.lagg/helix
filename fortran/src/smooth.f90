!function similar to IDL smooth subroutine
!f=subroutine to be smoothed, nf=# of elements of f, sval=smooth-value
subroutine smooth(f,nf,sval)
  use all_type
  real(4), dimension(maxwl) :: f,retf
  integer(2) :: nf,sval,i,i0,i1


  do i=1,nf
     i0=i-sval/2
     if (i0.lt.1) i0=1
     i1=i+sval/2
     if (i1.gt.nf) i1=nf

     retf(i)=sum(f(i0:i1))/(i1-i0+1)
  end do
  f(1:nf)=retf(1:nf)
  
end subroutine smooth
