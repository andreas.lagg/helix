program mpitest
#ifdef MPI
  include 'mpif.h'
  integer(4) :: myid, numprocs, ierr, i, irec
  integer(4) :: pnam_len,status(MPI_STATUS_SIZE)
  real(4) :: sqr,sqrec
  CHARACTER(LEN=MPI_MAX_PROCESSOR_NAME) :: PROCNAME

  call MPI_INIT( ierr )
  call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
  call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr ) 
  CALL MPI_get_processor_name(procname,pnam_len,ierr)

  if (myid.eq.0) then
     write(*,*) "Hello, I am the master: ",trim(procname)
     do i=1,numprocs-1
        sqr=sqrt(real(i,kind=4)*2)
        write(*,*) 'Master: sending integer (2*ID) ',i*2,' to slave ',i
        call MPI_Send(i*2,1,MPI_INTEGER,i,111,MPI_COMM_WORLD,ierr)
        write(*,*) 'Master: sending real sqrt(2*ID) ',sqr,' to slave ',i
        call MPI_Send(sqr,1,MPI_REAL,i,222,MPI_COMM_WORLD,ierr)
     end do
  else
     write(*,fmt='(a,i3,a,i3,a,a)') &
          "Hello, I am slave# ", myid, " of ",numprocs-1, " on ",trim(procname)
     call MPI_RECV(irec,1,MPI_INTEGER,0,111,MPI_COMM_WORLD,status,ierr)
     write(*,*) 'Slave ',myid,': receiving integer (2*ID) ',irec
     call MPI_RECV(sqrec,1,MPI_REAL,0,222,MPI_COMM_WORLD,status,ierr)
     write(*,*) 'Slave ',myid,': receiving real sqrt(2*ID) ',sqrec
  endif


   call MPI_FINALIZE(ierr)
#else
   write(*,*) "No MPI version of mpitest."
#endif
end program mpitest
