subroutine fill_localstray(lsi,lsq,lsu,lsv,flag,wlout,nwl)
  use all_type
  use localstray_com
  use tools
  integer(2) :: flag,nwl,n
  real(4),dimension(maxwl) :: lsi,lsq,lsu,lsv
  real(8),dimension(maxwl) :: wlout

  flag=dols
  if (dols.eq.0) return
  if (nwl.ne.lsprof%nwl) then
     n=lsprof%nwl
     call man_interpol(lsprof%i(1:n),lsprof%wl(1:n),n, &
          lsi(1:nwl),wlout(1:nwl),nwl)
     call man_interpol(lsprof%q(1:n),lsprof%wl(1:n),n, &
          lsq(1:nwl),wlout(1:nwl),nwl)
     call man_interpol(lsprof%u(1:n),lsprof%wl(1:n),n, &
          lsu(1:nwl),wlout(1:nwl),nwl)
     call man_interpol(lsprof%v(1:n),lsprof%wl(1:n),n, &
          lsv(1:nwl),wlout(1:nwl),nwl)
  else
     lsi=lsprof%i
     lsq=lsprof%q
     lsu=lsprof%u
     lsv=lsprof%v
  end if
end subroutine fill_localstray
