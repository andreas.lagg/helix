module piktools
  implicit none

  contains

!fitness function
function fitness(nwl,ifit,qfit,ufit,vfit,iobs,qobs,uobs,vobs,&
     iwgt,qwgt,uwgt,vwgt,icont)
  use all_type
  use quvstrength
  use tools
!use pikvar
  implicit none
  integer(2) :: nwl
  real(4), dimension(maxwl) :: iwgt,qwgt,uwgt,vwgt
  real(4), dimension(maxwl) :: iobs,qobs,uobs,vobs
  real(4), dimension(nwl)   :: ifit,qfit,ufit,vfit,diffvec
  real(4) :: fitness,diff,icont,chi2,noise,weights(4)

!!! Juanmas definition
!NUMW=31 wavelengths inverted
!SYN & OBS=> DOUBLE(NUMW,4)
!WEIGHTS => DOUBLE(4)
!NFREE=4*NUMW-NUMBER_OF_FREE_PARAMETERS (Currently 9)

!NOISE=1e-3
!WEIGHTS(:)=1D0/NOISE
!! This modifies weight in Stokes I
!WEIGHTS(1)=(0.8D0*ABS(1.01D0-MAXVAL(OBS(:,1)))+0.2D0)/NOISE
!CHI2=0D0
!DO I=1,4
!      ! Note weights enter as square
!      CHI2=CHI2+(1D0/NFREE)*WEIGHTS(I)**2D0*SUM((OBS(:,I)-SYN(:,I))**2D0)
!ENDDO

  !JuanMas definition
  if (chi2mode.eq.1) then
    noise=1e-3
    weights=1./noise
    weights(1)=(0.8D0*ABS(1.01D0-MAXVAL(iobs(1:nwl))/icont)+0.2D0)/NOISE
!    chi2=1./nfree * &
!         (weights(1)**2.*sum(iwgt(1:nwl)**2.*((ifit(1:nwl)-iobs(1:nwl))/icont) **2)+ &
!         weights(2)**2.*sum(qwgt(1:nwl)**2.*(qfit(1:nwl)-qobs(1:nwl)) **2)+ &
!         weights(3)**2.*sum(uwgt(1:nwl)**2.*(ufit(1:nwl)-uobs(1:nwl)) **2)+ &
!         weights(4)**2.*sum(vwgt(1:nwl)**2.*(vfit(1:nwl)-vobs(1:nwl)) **2))
    diffvec=1./nfree * &
         (weights(1)**2.*(iwgt(1:nwl)**2.*((ifit(1:nwl)-iobs(1:nwl))/icont) **2)+ &
         weights(2)**2.*(qwgt(1:nwl)**2.*(qfit(1:nwl)-qobs(1:nwl)) **2)+ &
         weights(3)**2.*(uwgt(1:nwl)**2.*(ufit(1:nwl)-uobs(1:nwl)) **2)+ &
         weights(4)**2.*(vwgt(1:nwl)**2.*(vfit(1:nwl)-vobs(1:nwl)) **2))
    !do not consider NAN values (most likely spikes or 0 prefilter transmission)
    where(check_nan(diffvec)) diffvec=0
    chi2=sum(diffvec(1:nwl))
    diff=chi2 / 10. !make fitness smaller
  else if (chi2mode.eq.2) then !PLAIN mode
    !multiply by 1e3 to prevent numerical errors for small differences
    diffvec(1:nwl)=&
         ((iwgt(1:nwl)*(1e3*(ifit(1:nwl)-iobs(1:nwl))/icont)**2.)+&
         (qwgt(1:nwl)* (1e3*(qfit(1:nwl)-qobs(1:nwl)))**2.) + &
         (uwgt(1:nwl)* (1e3*(ufit(1:nwl)-uobs(1:nwl)))**2.) + &
         (vwgt(1:nwl)* (1e3*(vfit(1:nwl)-vobs(1:nwl)))**2.))/1e3
    !do not consider NAN values (most likely spikes or 0 prefilter transmission)
    where(check_nan(diffvec)) diffvec=0
    diff=sum(diffvec(1:nwl))
    diff=diff/(nfree/100.)
  else
    !multiply by 1e3 to prevent numerical errors for small differences
    diffvec(1:nwl)=&
         ((iwgt(1:nwl)*(1e3*(ifit(1:nwl)-iobs(1:nwl))/icont)**2.)/istr+&
         (qwgt(1:nwl)* (1e3*(qfit(1:nwl)-qobs(1:nwl)))**2.)/qstr + &
         (uwgt(1:nwl)* (1e3*(ufit(1:nwl)-uobs(1:nwl)))**2.)/ustr + &
         (vwgt(1:nwl)* (1e3*(vfit(1:nwl)-vobs(1:nwl)))**2.)/vstr)/1e6
    !do not consider NAN values (most likely spikes or 0 prefilter transmission)
    where(check_nan(diffvec)) diffvec=0
    diff=sum(diffvec(1:nwl))
    diff=diff/(nfree/100.)

  end if

  if (diff.lt.1e-8) diff=1e-8
  fitness=1./(diff)

!  if (check_nan(fitness)) then
!    write(*,*) 'NAN detected'
!    write(*,*) 'diff=',diff
!    write(*,*) 'icont=',icont
!    write(*,*) 'IFIT=',ifit(1:nwl)
!    write(*,*) 'QFIT=',qfit(1:nwl)
!    write(*,*) 'UFIT=',ufit(1:nwl)
!    write(*,*) 'VFIT=',vfit(1:nwl)
!    write(*,*) 'ATM=',catm(1)%par
!    stop
!  end if
!write(*,*) fitness,diff,minval(diffvec(1:nwl)),maxval(diffvec(1:nwl))
!write(*,*) minloc(ifit(1:nwl)),minloc(iobs(1:nwl))
!write(*,*) 'ifit=',ifit(1:nwl)
!write(*,*) 'iobs=',iobs(1:nwl)
!  fitness=alog(1./(diff))
!  fitness=-diff
end function fitness

!function to be maximized
!!    ========================================================
function func(n,par)
  use all_type
  use pikvar
  use quvstrength
  use localstraycomp_com
  implicit none
  integer(4) :: n
  real(4) :: par(n)
  real(4), dimension(compnwl) :: ifit,qfit,ufit,vfit
  real(4) :: func
  integer(2) :: unwl
!  real(4) :: fitness
!  external fitness

  call struct2pikaia(par,n,1_4,0_4,0_4)
  call compute_profile(catm(1:cnatm),cnatm, &
       cline(1:cnline),cnline, &
       compwl(1:compnwl),compnwl,cblend(1:cnblend),cnblend,cgen, &
       lsi,lsq,lsu,lsv,cdols,clspol, &
       cconvval(1:compnwl),doconv,cmode, &
       cprefilterval(1:compnwl),doprefilter, &
       cret_wlidx,cnret_wlidx, &
       cobs_par, &
       0,cvoigt,cmagopt,cuse_geff,cuse_pb,cpb_method,cmodeval,ciprof_only, &
       cuse(1:cnline,1:cnatm),&
       ifit,qfit,ufit,vfit,1_2,cold_norm,-1_2,chanle_azi,cnorm_stokes_val,&
       cold_voigt)

  if ((doconv.eq.1).and.(cnret_wlidx.gt.0)) then
     unwl=cnret_wlidx
  else
     unwl=compnwl
  end if

  func=fitness(unwl,ifit,qfit,ufit,vfit, &
       ciobs,cqobs,cuobs,cvobs,ciwgt,cqwgt,cuwgt,cvwgt,cline(1)%icont)

!  write(*,*) 'func',minval(compwl(1:compnwl)),maxval(compwl(1:compnwl)),func
end function func
!!    ========================================================

!     call function for PIKMPI version
function userff(n,par,flag)
  integer(4) :: n
  real(4) :: par(n)
  integer flag
  real*4 :: userff

  userff=func(n,par)
end function userff

end module piktools
