subroutine add_noise(obs,doprefilter,prefilter)
  use all_type
  use ipt_decl
  use random
  implicit none
  integer(2) :: i,doprefilter
  type (proftyp) :: obs
  !  type (ipttyp) :: ipt
  real(4),dimension(obs%nwl) :: inoisei,noisei,noiseu,noiseq,noisev
  real(4),dimension(obs%nwl) :: prefilter,pf

  if ((ipt%noise.gt.1e-10).or.(ipt%inoise.gt.1e-10)) then

     if (ipt%verbose.ge.1) then
        write(*,*) 'Adding noise to IQUV-profile: level=',ipt%noise
        write(*,*) 'Adding noise to I-profile: level=',ipt%inoise
     end if

     CALL random_seed()

     !use random number generator creating a normal distribution
     !scale noise with light level (where
     !there is less light you have more
     !noise!)
     do i=1,obs%nwl 
        noisei(i)=gennor(0.,ipt%noise)/sqrt(obs%i(i)/obs%ic)
        noiseq(i)=gennor(0.,ipt%noise)/sqrt(obs%i(i)/obs%ic)
        noiseu(i)=gennor(0.,ipt%noise)/sqrt(obs%i(i)/obs%ic)
        noisev(i)=gennor(0.,ipt%noise)/sqrt(obs%i(i)/obs%ic)
        inoisei(i)=gennor(0.,ipt%inoise)
     end do

     !take into account reduced photon flux
     !with prefilter
     if (doprefilter.eq.1) then
        pf=prefilter
        where(prefilter.lt.1e-4) pf=1e-4
        noisei(1:obs%nwl)=noisei(1:obs%nwl)/pf(1:obs%nwl)
        noiseq(1:obs%nwl)=noiseq(1:obs%nwl)/pf(1:obs%nwl)
        noiseu(1:obs%nwl)=noiseu(1:obs%nwl)/pf(1:obs%nwl)
        noisev(1:obs%nwl)=noisev(1:obs%nwl)/pf(1:obs%nwl)
     end if

     !write(*,'(6f8.6)') noisei,noiseq,noiseu,noisev 

     obs%i(1:obs%nwl)=obs%i(1:obs%nwl)+noisei+inoisei
     obs%q(1:obs%nwl)=obs%q(1:obs%nwl)+noiseq
     obs%u(1:obs%nwl)=obs%u(1:obs%nwl)+noiseu
     obs%v(1:obs%nwl)=obs%v(1:obs%nwl)+noisev
  end if

end subroutine add_noise
