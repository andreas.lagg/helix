!module to declare ipt-type (replacement for common block
module ipt_decl
  use all_type
  implicit none
  save

  public
  type (ipttyp) :: ipt
  integer(2) :: act_ncomp,act_nline,act_nblend
  !actual number of lines and components
  !                                 (set in read_ipt.f90 to the values
  !                                  of ipt%ncomp, ipt%nline)

end module ipt_decl
