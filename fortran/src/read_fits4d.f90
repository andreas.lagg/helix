module fits4d_common
  use all_type
  save

  character(LEN=maxstr) :: fits4dfile,fits4dheader(:),fits4dorder
  allocatable fits4dheader
  real(4) :: fits4data(:,:,:,:)
  allocatable fits4data
  integer(2) :: fits4dnwl,fits4dicidx,fits4diquv(4)
  real(8) :: fits4dwlarr(:,:,:)
  allocatable fits4dwlarr
  real(8) :: fits4dwlvec(:)
  allocatable fits4dwlvec
  integer(4) :: fits4dnaxes(4),fits4dfpixels(4),fits4dlpixels(4)
  integer(4) :: fits4dxoff,fits4dyoff,fits4dnwlhdr,fits4dnx,fits4dny
  real(8) :: fits4dwlref,fits4dwlmin,fits4dwlmax
  real(4) :: fits4dic(:,:),fits4dimgcont
  logical :: fits4d_flagic,fits4d_flagwlvec,fits4d_flagwlarr
  allocatable fits4dic
end module fits4d_common

subroutine read_fits4d(file,xvin,yvin,profile,lsprof,do_ls,ok)
  use all_type
  use ipt_decl
  use tools
  use fits4d_common
  implicit none
#ifdef MPI
  include 'mpif.h'
#endif
  type (proftyp) :: profile,lsprof
  character(LEN=maxstr) :: file
  integer(2) :: xvin,yvin,xv,yv,ix,ii
  INTEGER(4) :: nfound,bitpix
  integer (4) :: fstatus,unit,blocksize,inc(4),hdutype,naxes3(3),extnaxes(4)
  logical :: ok,do_ls,error,anynull,anyf,wlflag
  real(4) :: fits4dicsort(:),ictmp(:,:,:),icont_hsra
  integer(4) :: isort(:),itot,ithresh,iext,myid,ierr
  character(LEN=72) :: comment
  allocatable fits4dicsort,ictmp
  allocatable isort
  INTERFACE
     INTEGER FUNCTION SYSTEM (COMMANDA)
       CHARACTER(LEN=*) COMMANDA
     END FUNCTION SYSTEM
  END INTERFACE

  fstatus=0
  error=.true.
  ok=.false.

  myid=0
  ierr=0
#ifdef MPI
  call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
#endif

  !check if file is already read in
  if (fits4dfile.ne.file) then
     if ((ipt%verbose.ge.1).and.(myid.eq.0)) write(*,*) 'Reading Fits 4D file ',trim(file)

     !  Get an unused Logical Unit Number to use to open the FITS file.r
     call ftgiou(unit,fstatus)
     !  Open the FITS file previously created by WRITEIMAGE
     call ftopen(unit,file,0,blocksize,fstatus)
     if (fstatus.ne.0) then
        if (myid.eq.0) write(*,*) 'Could not find Fits4d-file: '//trim(file)
        call stop
     end if
     !determine bitpix (should be 32, which is 4-byte integer)
     call ftgidt(unit,bitpix,fstatus)
     !  Determine the size of the image.
     call ftgknj(unit,'NAXIS',1,4,fits4dnaxes,nfound,fstatus)
     fits4dxoff=0
     fits4dyoff=0
     call ftgkyj(unit,'XOFFSET',fits4dxoff,comment,fstatus)
     call ftgkyj(unit,'YOFFSET',fits4dyoff,comment,fstatus)
     fits4dorder='-'
     fstatus=0
     call ftgkys(unit,'STOKES',fits4dorder,comment,fstatus)
     if (trim(fits4dorder).eq.'-') then
        fits4dorder='IQUV'
        write(*,*) "WARNING: Stokes order not specified in FITS file."
        write(*,*) "You can specify the order with the FITS-keyword:"
        write(*,*) "STOKES 'IQUV'"
        write(*,*) "ASSUMING Stokes order: ",trim(fits4dorder)
     end if
     fstatus=0

     if ((ipt%verbose.ge.1).and.(myid.eq.0)) &
          write(*,*) 'Stokes order in FITS file: ',trim(fits4dorder)
     fits4diquv(1)=index(trim(fits4dorder),'I')
     fits4diquv(2)=index(trim(fits4dorder),'Q')
     fits4diquv(3)=index(trim(fits4dorder),'U')
     fits4diquv(4)=index(trim(fits4dorder),'V')

     inc=(/1,1,1,1/)
     fits4dfpixels=(/1,1,1,1/)
     fits4dlpixels=fits4dnaxes

!     fits4dfpixels(3:4)=(/ipt%x(1)+1-fits4dxoff,ipt%y(1)+1-fits4dyoff/)
!     fits4dlpixels(3:4)=(/ipt%x(2)+1-fits4dxoff,ipt%y(2)+1-fits4dyoff/)
     fits4dnx=fits4dlpixels(3)-fits4dfpixels(3)+1
     fits4dny=fits4dlpixels(4)-fits4dfpixels(4)+1

     !check for WL info in main header
     wlflag=.false.
     fstatus=0
     fits4dwlref=0
     fits4dwlmin=0
     fits4dwlmax=0
     fits4dnwlhdr=0
     call ftgkyd(unit,'WLREF',fits4dwlref,comment,fstatus)
     call ftgkyd(unit,'WLMIN',fits4dwlmin,comment,fstatus)
     call ftgkyd(unit,'WLMAX',fits4dwlmax,comment,fstatus)
     call ftgkyj(unit,'NWL',fits4dnwlhdr,comment,fstatus)
     if ((fstatus.eq.0).and.(fits4dnwlhdr.ge.2).and.(fits4dwlmin.ne.fits4dwlmax)) wlflag=.true.

     if ((minval(fits4dfpixels).ge.1).and.&
          (fits4dlpixels(3).le.fits4dnaxes(3)).and.&
          (fits4dlpixels(4).le.fits4dnaxes(4))) then



        fstatus=0
        allocate(fits4data(fits4dlpixels(1),fits4dlpixels(2),fits4dnx,fits4dny))
        call ftgsve(unit,1,4, fits4dnaxes, fits4dfpixels, fits4dlpixels, inc, 0, &
             fits4data, anynull, fstatus)
        fits4dnwl=fits4dnaxes(1)
        if (fits4dnaxes(2).ne.4) then
           if ((ipt%verbose.ge.1).and.(myid.eq.0)) &
                write(*,*) 'FITS 4D does not contain I,Q,U,V.'
           call stop
        end if

        if (.not.wlflag) then 
           fstatus=0 !check for wavelength info in first FITS extension
           fits4dwlref=0
           fits4dwlmin=0
           fits4dwlmax=0
           fits4dnwlhdr=0
           call ftgkyd(unit,'WLREF',fits4dwlref,comment,fstatus)
           call ftgkyd(unit,'WLMIN',fits4dwlmin,comment,fstatus)
           call ftgkyd(unit,'WLMAX',fits4dwlmax,comment,fstatus)
           call ftgkyj(unit,'NWL',fits4dnwlhdr,comment,fstatus)
        end if
        fstatus=0
        fits4d_flagic=.false.
        fits4d_flagwlarr=.false.
        fits4d_flagwlvec=.false.
!        allocate(fits4dic(fits4dlpixels(3),fits4dlpixels(4)))
        allocate(fits4dic(fits4dnx,fits4dny))
        do iext=2,3
           !icont, wlve or WLARR in first or second extension
           fstatus=0
           call ftmahd(unit,iext,hdutype,fstatus)
           call ftgknj(unit,'NAXIS',1,4,extnaxes,nfound,fstatus)
           if (fstatus.eq.0) then
              select case(nfound)
              case(1) !extension is single WL-vector
                 allocate(fits4dwlvec(fits4dnaxes(1)))
                 call ftgsvd(unit,1,1,fits4dnaxes(1), &
                      fits4dfpixels(1),fits4dlpixels(1),inc(1),0,&
                      fits4dwlvec,anyf,fstatus)
                 if ((ipt%verbose.ge.1).and.(myid.eq.0)) &
                      write(*,*) 'Found WL-vector'
                 fits4d_flagwlvec=.true.
              case(2) !extension is continuum image
                 call ftgsve(unit,1,2,fits4dnaxes(3:4),&
                      fits4dfpixels(3:4),fits4dlpixels(3:4), &
                      inc(3:4),0,fits4dic,anyf,fstatus)
                 if ((ipt%verbose.ge.1).and.(myid.eq.0)) &
                      write(*,*) 'Found Cont-Image'
                 call ftgkye(unit,'IC_HSRA',icont_hsra,comment,fstatus)
                 if ((ipt%norm_cont.eq.2).and.(icont_hsra.ne.0)) &
                      fits4dic(:,:)=icont_hsra
                 fits4d_flagic=.true.
              case(3) !extension is 3D WL Arr or Cont Image for several spectra
                 call ftgknj(unit,'NAXIS',1,3,naxes3,nfound,fstatus)
                 if (naxes3(1).eq.fits4dnwl) then
                    allocate(fits4dwlarr(fits4dnaxes(1),fits4dlpixels(3),&
                         fits4dlpixels(4)))
                    call ftgsvd(unit,1,3,&
                         (/ fits4dnaxes(1),fits4dnaxes(3),fits4dnaxes(4) /), &
                         (/ fits4dfpixels(1),fits4dfpixels(3),fits4dfpixels(4) /),&
                         (/ fits4dlpixels(1),fits4dlpixels(3),fits4dlpixels(4) /),&
                         (/ inc(1),inc(3),inc(4) /),0,fits4dwlarr,anyf,fstatus)
                    if ((ipt%verbose.ge.1).and.(myid.eq.0)) &
                         write(*,*) 'Found WL-array'
                    fits4d_flagwlarr=.true.
                 else
                    allocate(ictmp(naxes3(1),fits4dlpixels(3),fits4dlpixels(4)))
                    call ftgsve(unit,1,3,&
                         (/ naxes3(1),fits4dnaxes(3),fits4dnaxes(4) /), &
                         (/ 1,fits4dfpixels(3),fits4dfpixels(4) /),&
                         (/ naxes3(1),fits4dlpixels(3),fits4dlpixels(4) /),&
                         (/ 1,inc(3),inc(4) /),0,ictmp,anyf,fstatus)
                    fits4dic(:,:)=ictmp(1,:,:)
                    call ftgkye(unit,'ICONT_HSRA',icont_hsra,comment,fstatus)
                    if ((ipt%norm_cont.eq.2).and.(icont_hsra.ne.0)) &
                         fits4dic(:,:)=icont_hsra
                    deallocate(ictmp)
                    if ((ipt%verbose.ge.1).and.(myid.eq.0)) &
                         write(*,*) 'Found Cont-Images, using spectral region 1'
                    fits4d_flagic=.true.
                 end if
              end select
           end if
        end do
        call ftclos(unit, fstatus)
        call ftfiou(unit, fstatus)

        if (.not.fits4d_flagic) then 
           fits4dic(:,:)=maxval(fits4data(:,1,:,:))
        end if

        !Image continuum: take average over 10% brightest
        !profiles as image continuum
        itot=fits4dnx*fits4dny

        allocate(fits4dicsort(itot))
        allocate(isort(itot))
        !     do ix=1,fits4dnaxes(3)
        do ix=1,fits4dnx
           fits4dicsort((ix-1)*fits4dny+1:ix*fits4dny)=&
                fits4dic(ix,:)
        end do
        call qsort(fits4dicsort,isort,itot)
        ithresh=0.9*itot
        if (ithresh.lt.1) ithresh=1
        fits4dimgcont=sum(fits4dicsort(isort(ithresh:itot)))/(itot-ithresh+1)
        fits4dfile=file
     end if
  end if

  xv=xvin-fits4dxoff
  yv=yvin-fits4dyoff
  if ((xv.lt.0).or.(xv.ge.fits4dnaxes(3)).or.&
       (yv.lt.0).or.(yv.ge.fits4dnaxes(4))) then
     write(*,*) 'Could not find XPOS=',xvin,' and YPOS=',yvin,&
          ' in Fits4D data file ',file
     write(*,*) 'File Size:'
     write(*,*) '  x=',fits4dxoff,' - ',fits4dnaxes(3)+fits4dxoff
     write(*,*) '  y=',fits4dyoff,' - ',fits4dnaxes(4)+fits4dyoff
     return
  end if
  xv=xv-(fits4dfpixels(3)-1)
  yv=yv-(fits4dfpixels(4)-1)



  profile%nwl=fits4dnwl
  if (ipt%norm_cont.eq.0) then
     profile%ic=fits4dic(xv+1,yv+1)
  else
     profile%ic=fits4dimgcont
  end if
  profile%i(1:fits4dnwl)=&
       fits4data(1:fits4dnwl,fits4diquv(1),xv+1,yv+1)/profile%ic
  profile%q(1:fits4dnwl)=&
       fits4data(1:fits4dnwl,fits4diquv(2),xv+1,yv+1)/profile%ic
  profile%u(1:fits4dnwl)=&
       fits4data(1:fits4dnwl,fits4diquv(3),xv+1,yv+1)/profile%ic
  profile%v(1:fits4dnwl)=&
       fits4data(1:fits4dnwl,fits4diquv(4),xv+1,yv+1)/profile%ic

  if (fits4d_flagwlvec) then
     profile%wl(1:fits4dnwl)=fits4dwlvec(1:fits4dnwl)
  else if (fits4d_flagwlarr) then
     profile%wl(1:fits4dnwl)=fits4dwlarr(1:fits4dnwl,xv+1,yv+1)
  else 
     do ii=1,fits4dnwl 
        profile%wl(ii)=fits4dwlmin +&
             (ii-1)*(fits4dwlmax-fits4dwlmin)/(fits4dnwlhdr-1)+fits4dwlref
     end do
     if ((abs(fits4dwlref).le.1e-5).and.(ipt%verbose.ge.1).and.(myid.eq.0)) &
          write(*,*) 'WARNING: No WL-calibration in 4D-fits file!'
  end if
  if (do_ls) then
     call get_lsprof(file,xv,yv,profile%wl,fits4dnwl,5_2,lsprof)
  end if
  error=.false.
  ok=.true.

end subroutine read_fits4d
