subroutine par_write(unit,name,par,fit)
  use all_type
  use ipt_decl
  implicit none
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  type(iptyp) :: name
  real(4) :: par(maxatm)
  integer(4) :: unit,ip
  integer(2) :: fit(maxatm)
  character(2) :: nstr
  logical :: val

  val=.false.
  do ip=1,ipt%nparset
     if ((trim(ipt%parset(ip))).eq.(trim(name%prg))) val=.true.
!     val=(val.or.((trim(ipt%parset(ip))).eq.(trim(name%prg))))
  end do

  if (val) then 
     write(unit=nstr,fmt='(i2.2)') ipt%ncomp
     write(unit,fmt='(a10,'//nstr//'(f16.5),'//nstr//'(i5))') &
          name%ipt,par(1:ipt%ncomp),fit(1:ipt%ncomp)
  end if

end subroutine par_write

subroutine linatmpar_write(unit,name,nlen,par,fit)
  use all_type
  use ipt_decl
  implicit none
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  integer(4) :: unit,nlen
  character(LEN=nlen) :: name
  real(4) :: par(maxatm)
  integer(2) :: fit(maxatm)
  character(2) :: nstr

  write(unit=nstr,fmt='(i2.2)') ipt%ncomp
  write(unit,fmt='(a16,'//nstr//'(f16.5),'//nstr//'(i5))') &
       name(1:nlen),par(1:ipt%ncomp),fit(1:ipt%ncomp)
end subroutine linatmpar_write

subroutine linpar_write(unit,name,nlen,par,fit)
  use all_type
  use ipt_decl
  implicit none
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  integer(4) :: unit,nlen
  character(LEN=nlen) :: name
  real(4) :: par
  integer(2) :: fit


  write(unit,fmt='(a15,f16.5,i5)') &
       name(1:nlen),par,fit
end subroutine linpar_write

subroutine write_atm(atm,natm,line,nline,blend,nblend,gen, &
     fitness,fitprof,resultname,xyinfo)
  use all_type
  use ipt_decl
  use tools
  implicit none
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  integer(2) :: natm,nprof,nline,nofit(maxatm),nblend
  type (atmtyp) :: atm(natm)
  type (blendtyp) :: blend(nblend)
  type (gentyp) :: gen
  type (linetyp) :: line(maxlin)
  type (proftyp) :: fitprof
  character(LEN=maxstr) :: resultname,file,file_atm,file_prof,path
  character(LEN=80) :: sep
  character(LEN=idlen) :: idstr
  character(LEN=*) :: xyinfo
  integer(4) :: ierr1,ierr2,i
  integer(1) :: id(idlen)
  real(4) :: fitness
  integer(4) :: date_time(8)
  character(len=10) :: big_ben (3)
  character(1) :: nstr
  type(parnametyp) :: parname
  type(blendnametyp) :: blendname
  type (gennametyp) :: genname
  real(8) :: wlref


  file=resultname(1:len_trim(resultname))
  path=file(1:index(trim(file),'/',back=.true.))
  file_atm =path(1:len_trim(path))//xyinfo(1:len_trim(xyinfo))//'_atm.dat'
  file_prof=path(1:len_trim(path))//xyinfo(1:len_trim(xyinfo))//'_profile.dat'

  call mkdirhier(path,len_trim(path),'755',ierr1)

  sep=repeat('-',78)
  call date_and_time(big_ben (1), big_ben (2), big_ben (3), date_time)
!!!!!!!!!!!!!!!!!!!! atmosphere
  open(10,file=trim(file_atm),status='replace',iostat=ierr2,action='WRITE')
  if (ierr2.ne.0) then
     write(*,*) 'Error writing atmosphere to file '//trim(file_atm)
     write(*,*) 'Please check directory structure.'
     write(*,*) 'Result NOT STORED!!!'
  else
     !header:
     write(10,99) trim(resultname(1:len_trim(resultname)))
     write(10,99) 'Date: '//trim(big_ben(1))//' Time: '//trim(big_ben(2))
     write(10,99) 'Input File: '//ipt%file(1:len_trim(ipt%file))
     write(10,99) 'Observation: '//ipt%observation(1:len_trim(ipt%observation))
     write(10,fmt='(a,f12.4)') 'Fitness: ',fitness
     select case(int(ipt%modeval))
        case (0)
           write(10,99) 'MODE: GAUSS'
        case (1)
           write(10,99) 'MODE: VOIGT'
        case (2)
           write(10,99) 'MODE: VOIGT_PHYS'
        case (3)
           write(10,99) 'MODE: VOIGT_GDAMP'
        case (4)
           write(10,99) 'MODE: VOIGT_SZERO'
        case (5)
           write(10,99) 'MODE: HANLE_SLAB'
        end select
        write(idstr,fmt='(i3)') ipt%nmi
     write(10,fmt='(a,'//trim(idstr)//'i6)') 'NCALLS: ',ipt%ncalls(1:ipt%nmi)
     write(10,fmt='(a,i2)') 'NCOMP: ',ipt%ncomp
     write(10,99) trim(sep)
     !parameters
     call def_parname(parname)
     call par_write(10,parname%b,atm%par%b,atm%fit%b)
     call par_write(10,parname%azi,atm%par%azi,atm%fit%azi)
     call par_write(10,parname%inc,atm%par%inc,atm%fit%inc)
     call par_write(10,parname%vlos,atm%par%vlos,atm%fit%vlos)
     call par_write(10,parname%width,atm%par%width,atm%fit%width)
     call par_write(10,parname%damp,atm%par%damp,atm%fit%damp)
     call par_write(10,parname%dopp,atm%par%dopp,atm%fit%dopp)
     call par_write(10,parname%a0,atm%par%a0,atm%fit%a0)
     call par_write(10,parname%szero,atm%par%szero,atm%fit%szero)
     call par_write(10,parname%sgrad,atm%par%sgrad,atm%fit%sgrad)
     call par_write(10,parname%etazero,atm%par%etazero,atm%fit%etazero)
     call par_write(10,parname%gdamp,atm%par%gdamp,atm%fit%gdamp)
     call par_write(10,parname%vmici,atm%par%vmici,atm%fit%vmici)
     call par_write(10,parname%densp,atm%par%densp,atm%fit%densp)
     call par_write(10,parname%tempe,atm%par%tempe,atm%fit%tempe)
     call par_write(10,parname%dslab,atm%par%dslab,atm%fit%dslab)
     call par_write(10,parname%height,atm%par%height,atm%fit%height)
     call par_write(10,parname%ff,atm%par%ff,atm%fit%ff)

     !blend parameters
     call def_blendname(blendname)
     if (maxval(blend%par%a0).gt.1e-10) then
        write(10,99) trim(sep)
        write(10,fmt='(a)') 'Blend Parameters:'
        write(10,fmt='(a,i3)') 'NBLEND:',nblend
        write(10,99) trim(sep)
        write(unit=nstr,fmt='(i1)') nblend
        write(10,fmt='(a12,'//nstr//'(f16.5),'//nstr//'(i5))') &
             blendname%wl%ipt,blend(1:nblend)%par%wl, &
             blend(1:nblend)%fit%wl
        write(10,fmt='(a12,'//nstr//'(f16.5),'//nstr//'(i5))') &
             blendname%width%ipt,blend(1:nblend)%par%width, &
             blend(1:nblend)%fit%width
        write(10,fmt='(a12,'//nstr//'(f16.5),'//nstr//'(i5))') &
             blendname%damp%ipt,blend(1:nblend)%par%damp, &
             blend(1:nblend)%fit%damp
        write(10,fmt='(a12,'//nstr//'(f16.5),'//nstr//'(i5))') &
             blendname%a0%ipt,blend(1:nblend)%par%a0, &
             blend(1:nblend)%fit%a0
     end if

     !blend parameters
     call def_genname(genname)
     if ((gen%fit%ccorr.ne.0).or.(gen%fit%straylight.ne.0)&
          .or.(gen%fit%radcorrsolar.ne.0)) then
        write(10,99) trim(sep)
        write(10,fmt='(a)') 'General Parameters:'
        if (gen%fit%ccorr.ne.0) then 
           write(10,99) trim(sep)
           write(10,fmt='(a14,(f16.5),(i5))') &
                genname%ccorr%ipt,gen%par%ccorr,gen%fit%ccorr
        end if
        if (gen%fit%straylight.ne.0) then 
           write(10,99) trim(sep)
           write(10,fmt='(a14,(f16.5),(i5))') &
                genname%straylight%ipt,gen%par%straylight,gen%fit%straylight
        end if
        if (gen%fit%radcorrsolar.ne.0) then 
           write(10,99) trim(sep)
           write(10,fmt='(a14,(f16.5),(i5))') &
                genname%radcorrsolar%ipt,gen%par%radcorrsolar,&
                gen%fit%radcorrsolar
        end if
     end if

     !line parameters
     write(10,99) trim(sep)
     write(10,fmt='(a)') 'Line Parameters:'
     write(10,fmt='(a,i3)') 'NLINE:',nline
     nofit=0
     do i=1,ipt%nline
        write(10,99) trim(sep)
        id=line(i)%id
        where (id.eq.0)
           id=32
        endwhere
        write(10,fmt='(20a)') char(id)
        call linatmpar_write(10,'VLOS',4,line(i)%straypol_par%vlos,nofit)
        call linatmpar_write(10,'WIDTH',5,line(i)%straypol_par%width,nofit)
        call linatmpar_write(10,'DAMP',4,line(i)%straypol_par%damp,nofit)
        call linatmpar_write(10,'DOPP',4,line(i)%straypol_par%dopp,nofit)
        call linpar_write(10,'STRAYPOL_AMP',12, &
             line(i)%par%straypol_amp,line(i)%fit%straypol_amp)
        call linpar_write(10,'STRAYPOL_ETA0',13, &
             line(i)%par%straypol_eta0,line(i)%fit%straypol_eta0)
        call linpar_write(10,'STRENGTH',8, &
             line(i)%par%strength,line(i)%fit%strength)
        call linpar_write(10,'WLSHIFT',7, &
             line(i)%par%wlshift,line(i)%fit%wlshift)
     end do

     if (ipt%verbose.ge.1) &
          write(*,*) 'Atmosphere written to '//trim(file_atm)
  close(10)
  end if

  if (ipt%save_fitprof.eq.1) then
!!!!!!!!!!!!!!!!!!!! profile in stopro format
     open(10,file=trim(file_prof),status='replace',iostat=ierr2,action='WRITE')
     if (ierr2.ne.0) then
        write(*,*) 'Error writing profile to file '//trim(file_prof)
        write(*,*) 'Please check directory structure.'
        write(*,*) 'Result NOT STORED!!!'
     else
        nprof=1
        write(10,fmt='(i3,2x,a1,''WIVQU'',a1,3x,a,1x,a,1x,a,1x,a,'':'',a)') &
             nprof,'''','''',trim(ipt%observation),trim(xyinfo), &
             trim(ipt%file),trim(big_ben(1)),trim(big_ben(2))
        idstr=" "
        if ((fitprof%wlref.eq.0).and.(ipt%nline.ge.1)) then
           wlref=line(1)%wl
        else
           wlref=fitprof%wlref
        end if
        write(10,fmt='(i5,f16.4,i6,ES16.5,''  |  '',4i5,3x,a1,a,a1)') &
             nprof,wlref,fitprof%nwl,fitprof%ic,1,fitprof%nwl,1, &
             fitprof%nwl,'''',trim(idstr),''''
        do i=1,fitprof%nwl
           write(10,fmt='(5ES15.6)') &
                fitprof%wl(i)-wlref,fitprof%i(i)/fitprof%ic,&
                fitprof%v(i),fitprof%q(i),fitprof%u(i)
        end do
        write(10,*)
        if (ipt%verbose.ge.1) &
             write(*,*) 'Fit-Profile written to '//trim(file_prof)
        close(10)
     end if
  end if
  
99 format (a)

end subroutine write_atm
