!!    compile with:
!!    lf95  -I/opt/rsi/idl/external/ -c -o example_f.o example.f
!!    /usr/bin/ld -shared -o example_f.so example_f.o
!!    
!!    IDL-call:
!!    x=findgen(10)
!!    sum=0.
!!    s=CALL_EXTERNAL('example_f.so', 'sum_array_',x,n_elements(x),sum)
!!    print,sum

SUBROUTINE SUM_ARRAY(argc, argv) !Called by IDL
  INTEGER(4) argc, argv(*)   !Argc and Argv are integers

  j = LOC(argc)             !Obtains the number of arguments (argc)
  !Because argc is passed by VALUE.

  !!    Call subroutine SUM_ARRAY1, converting the IDL parameters
  !!    to standard FORTRAN, passed by reference arguments:

  CALL SUM_ARRAY1(%VAL(argv(1)),%VAL(argv(2)),%VAL(argv(3)))
END SUBROUTINE SUM_ARRAY

!!    This subroutine is called by SUM_ARRAY and has no
!!    IDL specific code.
!!    
SUBROUTINE SUM_ARRAY1(low,med,hi)
!  use all_type
  type tlow
     integer(2) :: n
     integer(1) :: byt(10)
     real(4) :: r(5)
  end type tlow
  type (tlow) :: low
  type tmed
     integer(2) :: n
     type (tlow) :: low(2)
  end type tmed
  type (tmed) :: med
  type thi
     integer(2) :: n
     integer(1) :: byt(10)
     type (tmed) :: med(5)
  end type thi
  type (thi) :: hi
!  type (linetyp) :: line(3)

  write(*,*) 'you are inside fortran'

  !!     write(*,*) hi%med(4)%low(2)%byt,hi%med(4)%low(1)%byt
  !!     write(*,*) hi.byt

  !!     line%n=6
!  write(*,*) line(1)%id
!  write(*,*) line(2)%id
!  write(*,*) line(3)%id

  low%r=(/1,2,3,4,500/)

  med%low(1)%r=(/1,2,3,4,5/)*2
  med%low(2)%r=(/2,2,3,4,8/)*2
  med%low(1)%r=low%r*20

  hi%med(1)%low=low
  hi%med(3)%low=low
  hi%med(5)=med

END SUBROUTINE SUM_ARRAY1

