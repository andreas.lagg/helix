subroutine get_profidx(xp,yp,stepx,stepy,vecx,vecy,avg,scansize,rgx,rgy, &
     nvx,nvy)
  use all_type
  implicit none
  integer(2) :: xp,yp,stepx,stepy,avg,scansize,rgx(2),rgy(2),nvx,nvy
  integer(2) :: vecx(maxavg),vecy(maxavg)
  integer(2) :: nsc,ix,iy,pos,i,mxp

  vecx(1)=xp ; vecy(1)=yp
  nvx=1      ; nvy=1

  if (scansize.ne.0) then
     nsc=xp/scansize
  else
     nsc=0
  end if

  i=0
  do ix=1,stepx
     pos=(xp-nsc*scansize)/(stepx)*stepx+nsc*scansize+ix-1
     mxp=maxval((/maxval(rgx),minval(rgx)+stepx/))
!     if ((pos.ge.minval(rgx)).and.(pos.le.mxp)) then
        i=i+1
        vecx(i)=pos
!     end if
  end do
  nvx=i

  i=0
  do iy=1,stepy
     pos=(yp)/(stepy)*stepy+iy-1
     mxp=maxval((/maxval(rgy),minval(rgy)+stepy/))
!     if ((pos.ge.minval(rgy)).and.(pos.le.mxp)) then
        i=i+1
        vecy(i)=pos
!     end if
  end do
  nvy=i

  if (scansize.gt.0) then
     i=1
     do ix=2,nvx
        if ((vecx(1)/scansize).eq.(vecx(ix)/scansize)) i=i+1
     end do
     nvx=i
  end if


  if (avg.eq.0) then
     nvx=1
     nvy=1
  end if
! write(*,'(a,$)') 'X='
! do ix=1,nvx 
!    write(*,'(i2,''-'',$)') vecx(ix)
! end do
! write(*,'(a,$)') ',  Y='
! do iy=1,nvy
!    write(*,'(i2,''-'',$)') vecy(iy)
! end do
! write(*,*)
end subroutine get_profidx
