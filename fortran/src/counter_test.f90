!test program for shared counter:
!compilation:
!make nxtval.o ; /opt/mpich-1.2.5//bin/mpif90 -fpp2  -static -lowercase -openmp  -pthread -Vaxlib counter_test.f90 -L./  -L/opt/mpich-1.2.5//lib -lfmpich -lmpich -lnxt
program counter_test
  use NXTVAL
  implicit none
  include 'mpif.h'

  integer(4) :: numprocs,counter_comm,smaller_comm, ierr, myid, val
  integer(4) :: i,i2,maxit
  real(4) :: dummy

  maxit=20

  call MPI_Init(ierr)
  call MPI_Comm_rank(MPI_COMM_WORLD , myid, ierr )
  call MPI_Comm_size(MPI_COMM_WORLD, numprocs,ierr)
  call MPI_Comm_Dup(MPI_COMM_WORLD,counter_comm)

  call MPE_Counter_create(MPI_COMM_WORLD,smaller_comm,counter_comm,ierr ); 

  if (counter_comm.ne.MPI_COMM_NULL) then 
     val=0
     do while (val.le.maxit)
        call MPE_Counter_nxtval(counter_comm, val, ierr )
        if (val.le.maxit) then
           write(*,*) 'Process',myid," received value ", val
           !     do i2=1,10000 !computation
           !        dummy=sqrt(3.14)**real(mod(i2,5))
           !    end do
        end if
     end do
     call mpi_barrier(smaller_comm,ierr)
     call MPE_Counter_free(smaller_comm,counter_comm, ierr )
  end if

  call MPI_Finalize(ierr)
end program counter_test
