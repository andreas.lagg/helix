!!Routine to read in input file
subroutine read_atom(file,path,nline,verbose,use_pb,atom)
  use all_type
  use tools
  implicit none
  integer(2) :: nline,verbose,use_pb
  character(LEN=maxstr) :: file(maxlines),path
  character(LEN=2*maxstr) :: fpath
  type (linetyp) :: atom(maxlin)
  type (hanletyp) :: hanle

!  external str2real
!  real(4) :: str2real
!  external str2double
!  real(8) :: str2double
  !  external str2int
  !  integer(4) :: str2int
  
  integer(2) :: i,j,il,in,nt,nw,ib,ip,iu,ii
  integer(2) :: jmin2,jmax2,j2
  integer(4) err,cindex,sidx
  character(LEN=maxstr) :: line,word(20),uword,aup
  character(LEN=3*maxstr) :: ids
  character(LEN=9) :: wls
  character(LEN=idlen) :: linid
  character(LEN=3) :: slinid
  real(8) :: wl
  real(8),dimension(maxpi) :: zeeman_p,wep
  real(8),dimension(maxsigma) :: zeeman_b,zeeman_r,web,wer
  real(4) :: geff
  integer(1) :: bids(idlen)
  logical :: newline,hanlemode,first,first2

  nline=0
  first2=.true.
  do i=1,maxlines
     if (trim(file(i)).ne.'-') then
        fpath=trim(path)//trim(file(i))

        iu=100
        open(iu,file=trim(fpath),iostat=err,action='READ',form='FORMATTED')
        if (err.ne.0) then
           write(*,*) 'Could not find atomic data file ',trim(fpath),err
           stop 
        end if

        il=1
        ii=0
        hanlemode=.false.
        do while (err.eq.0)
           read(iu, fmt='(a)',iostat=err) line
           ii=ii+1
           if (err.eq.0) then
              cindex=index(trim(line),';')-1
              if ((cindex.lt.0).and.(len(trim(line)).gt.0)) then

                 j=1
                 sidx=1
                 word=''
                 !split input line into words separated by spaces
                 do while ((sidx.gt.0).and.(j.le.20).and.len(trim(line)).gt.0)
                    sidx=index(line,' ')
                    if (sidx.gt.1) then
                       word(j)=line(1:sidx-1)
                       line=trim(line(sidx+1:))
                       j=j+1
                    else
                       if (sidx.eq.1) line=trim(line(sidx+1:))
                    end if
                 end do
                 nw=j-1

                 uword=word(1)
                 call upcase(uword)
                 if (uword.eq.'HANLEPROF') hanlemode=.true.
                 if (hanlemode) then
                    read(iu,*) hanle%is2
                    read(iu,*) hanle%nterm
                    do in=1,hanle%nterm
                       read(iu,*) hanle%ntm(in),hanle%lsto2(in)
                       jmin2=abs(hanle%is2-hanle%lsto2(in))
                       jmax2=hanle%is2+hanle%lsto2(in)
                       do j2=jmin2,jmax2,2 
                          read(iu,*) hanle%energy(in,j2) !energy(30,0:20)
                       end do
                    end do
                    read(iu,*) hanle%ntrans
                    do nt=1,hanle%ntrans
                       read(iu,*) hanle%nts(nt),hanle%ntlsto(nt),&
                            hanle%ntusto(nt),&
                            hanle%aesto(nt)
                       read(iu,*) hanle%u1sto(nt),hanle%u2sto(nt),&
                            hanle%riosto(nt),hanle%wlsto(nt),hanle%fsto(nt)
                    end do
                    
                    !He 10830 line is transition #1 in Hanle part.
                    !Take only this one, saves computation time!
!                   BUT: in rare cases leads to singular matrix in hanle_module
!                    hanle%ntrans=1
!                    if (verbose.ge.1) then
!                       write(*,*) 'Atomic Data File: '//&
!                            'Using only transition #1 (=He 10830 Triplet)'
!                    end if

                    !since this is the end of the atomic data file
                    !the reading should be stopped here
                    err=1
        
                    !fill in the hanle info to the atoms 'He1'
                    first=.true.
                    do in=1,nline
                       write(unit=slinid,fmt='(3a)') char(atom(in)%id(1:3))
                       if (slinid.eq.'He1') then
                          atom(in)%hanle=hanle
                          if (first) atom(in)%use_hanle_slab=1
                          first=.false.
                       end if
                    end do
                 else
                    wl=str2double(word(1))
                    write(wls,fmt='(f9.2)') wl
                    ids=trim(word(2))//trim(word(3))//wls
                    do ib=1,idlen
                       bids(ib)=ichar(ids(ib:ib))
                    end do

                    !check if line was already read
                    newline=.true.
                    do ip=1,nline+il-1
                       if (count((/atom(ip)%id.eq.bids/)).eq.idlen) then
                          newline=.false.
                       endif
                    end do
                    if (newline) then
                       if (verbose.ge.2) then
                          write(*,*) 'Reading line '//ids(1:idlen)
                       end if
                       nline=nline+1

                       if (nline.gt.maxlin) then
                          write(*,*) 'Maximum allowed spectral lines: ',maxlin
                          call stop
                       end if


                       atom(nline)%id=bids
                       atom(nline)%wl=wl
                       atom(nline)%ion=str2int(word(3))
                       !treatment of log(gf): The loggf was
                       !interpreted as natural log until Dec. 14 2009,
                       !which is wrong. This was corrected on Dec. 14
                       !2009. For compatibility with the old results
                       !we specially treat the he1083.0.dat file
                       !here. There the log(gf) values are given as
                       !natural log.
                       atom(nline)%loggf=str2real(word(4))
                       if (trim(file(i)).eq.'he1083.0.dat') then
                          if ((first2).and.(verbose.ge.1)) then
                             write(*,*) 'WARNING:'
                             write(*,*) '  Using old loggf values for He 10830.'
                             write(*,*) '  New inversions should use the '//&
                                  'atomic data file ''he1083.0.new.dat''.'
                             first2=.false.
                          end if
                          atom(nline)%f=exp(atom(nline)%loggf)
                       else
                          atom(nline)%f=10**(atom(nline)%loggf)
                       end if
                       atom(nline)%abund=str2real(word(5))
                       atom(nline)%sl=str2real(word(7))
                       atom(nline)%ll=str2real(word(8))
                       atom(nline)%jl=str2real(word(9))
                       atom(nline)%su=str2real(word(10))
                       atom(nline)%lu=str2real(word(11))
                       atom(nline)%ju=str2real(word(12))
                       atom(nline)%straypol_use=0

                       !calculate lande factors
                       call create_nc(atom(nline),1_2,verbose)

                       !check difference between calculated geff and
                       !geff in file
                       geff=str2real(word(6))
                       if (geff.le.20) then
                          if (abs(geff-atom(nline)%geff).gt.1e-4) then
                             if (verbose.ge.1) then 
                                write(*,*) 'Atom '//trim(ids)//': use geff='//&
                                     trim(word(6))
                                atom(nline)%geff=geff
                             end if
                          end if
                       end if

                       !atom-specific settings
                       aup=word(2)
                       call upcase(aup)
                       select case (trim(aup))
                       case ('HE')
                          atom(nline)%mass=4.
                          !paschen back treatment
                          if (use_pb.eq.1) call def_pb(atom(nline),ids)

                          !parameters for correction of
                          !straypolarization
                          select case (int(atom(nline)%ju)*2)
                          case (0_2)
                             atom(nline)%straypol_par%sign=-1
                          case default
                             atom(nline)%straypol_par%sign=+1
                          end select

                          atom(nline)%fit%straypol_amp=0 !fit flag for straypol
                          atom(nline)%par%straypol_amp=0 !gauss ampl. for sp-corr 
                          atom(nline)%scale%straypol_amp%min=0 !-0.02
                          atom(nline)%scale%straypol_amp%max=+0.004

                          atom(nline)%fit%straypol_eta0=0 !fit flag for straypol
                          atom(nline)%par%straypol_eta0=0 !eta0 for sp-correction
                          atom(nline)%scale%straypol_eta0%min=0 !-0.02
                          !                       atom(nline)%scale%straypol_eta0%max=+0.008
                          atom(nline)%scale%straypol_eta0%max=+0.500
                          atom(nline)%straypol_use=1
                       case ('FE')
                          atom(nline)%mass=55.845
                       case ('NI')
                          atom(nline)%mass=58.693
                       case ('NA')
                          atom(nline)%mass=22.
                       case ('TI')
                          atom(nline)%mass=47.867
                       case ('SI')
                          atom(nline)%mass=28.
                       case ('CA')
                          atom(nline)%mass=40.
                       case ('MG')
                          atom(nline)%mass=24.
                       case ('CR')
                          atom(nline)%mass=51.996
                       case default
                          write(*,*) 'Unknown element: '//trim(word(2))
                          stop
                       end select
                    end if
                 end if !hanleprof
              end if
           end if
        end do
        close(iu)
     end if
  end do

  if (verbose.ge.3) then
     do i=1,nline
        write(*,*)
        write(unit=linid,fmt='(12a)') char(atom(i)%id)
        write(*,*) 'Atom: ',linid
        write(*,*) '    WL=',atom(i)%wl
        write(*,*) '    LOGGF=',atom(i)%loggf,', GF=',atom(i)%f
        write(*,*) '    n_pi=',atom(i)%quan%n_pi,'   n_sig=',atom(i)%quan%n_sig
        write(*,*) '    gl,gu,geff: ',atom(i)%gl,atom(i)%gu,atom(i)%geff
        zeeman_B=atom(i)%wl**2.*4.6686411e-13*atom(i)%quan%NUB*1e3
        zeeman_P=atom(i)%wl**2.*4.6686411e-13*atom(i)%quan%NUP*1e3
        zeeman_R=atom(i)%wl**2.*4.6686411e-13*atom(i)%quan%NUR*1e3
        wep=atom(i)%quan%WEP
        wer=atom(i)%quan%WER
        web=atom(i)%quan%WEB
        write(*,*) '    Transition type b:'
        do j=1,atom(i)%quan%n_sig
           write(*,'(2(a,f12.7))') '      Shift (mA): ',zeeman_b(j), &
                '  Strength: ',web(j)
        end do
        write(*,*) '    Transition type p:'
        do j=1,atom(i)%quan%n_pi
           write(*,'(2(a,f12.7))') '      Shift (mA): ',zeeman_p(j), &
                '  Strength: ',wep(j)
        end do
        write(*,*) '    Transition type r:'
        do j=1,atom(i)%quan%n_sig
           write(*,'(2(a,f12.7))') '      Shift (mA): ',zeeman_r(j), &
                '  Strength: ',wer(j)
        end do
     end do
  end if

end subroutine read_atom
