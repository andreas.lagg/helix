module quvstrength
  implicit none
  save

  public
  real(4) :: quv_strength,istr,qstr,ustr,vstr
  integer(2) :: nfree,chi2mode
end module quvstrength

module pikvar
  use all_type
  implicit none
  save

  public
  real(8), dimension(maxwl) :: cwl,compwl
  integer(2) :: cuse(maxlin,maxatm)
  real(4), dimension(maxwl) :: ciobs,cqobs,cuobs,cvobs
  real(4), dimension(maxcwl) :: cconvval
  real(4), dimension(maxwl) :: ciwgt,cqwgt,cuwgt,cvwgt,cprefilterval
  integer(2) :: cnatm,cnline,cnblend,cnwl,cvoigt,cmagopt,cold_norm,cuse_geff, &
       cuse_pb,cpb_method,compnwl,cnorm_stokes_val,cold_voigt
  real(4) :: chanle_azi
  integer(2),dimension(maxwl) :: cret_wlidx
  integer(2) :: cmodeval,ciprof_only,cnret_wlidx
  integer(2) :: doconv,doprefilter,cmode
  type (scltyp) :: cscale(maxatm)
  type (linetyp) :: cline(maxlin)
  type (atmtyp) :: catm(maxatm)
  type (blendtyp) :: cblend(maxblend)
  type (gentyp) :: cgen
  type (obstyp) :: cobs_par
  character(LEN=idlen),dimension(maxfitpar) :: fitparname

end module pikvar
