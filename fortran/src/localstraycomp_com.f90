!module to declare localstray (replacement for common block
module localstraycomp_com
  use all_type
  implicit none
  save
  
  public
  real(4),dimension(maxwl) :: lsi,lsq,lsu,lsv
  integer(2) :: cdols,clspol

end module localstraycomp_com
