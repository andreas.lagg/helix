!write_atm: Version which first writes everything into a string and
!then to disk (performance increase especially over NFS)
subroutine par_write(unstr,name,par,fit)
  use all_type
  use ipt_decl
  implicit none
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  type(iptyp) :: name
  real(4) :: par(maxatm)
  integer(4) :: ip
  integer(2) :: fit(maxatm)
  character(2) :: nstr
  logical :: val
  character(LEN=maxlonstr) :: unstr

  val=.false.
  do ip=1,ipt%nparset
     if ((trim(ipt%parset(ip))).eq.(trim(name%prg))) val=.true.
!     val=(val.or.((trim(ipt%parset(ip))).eq.(trim(name%prg))))
  end do

  if (val) then 
     write(unit=nstr,fmt='(i2.2)') ipt%ncomp
     write(unit=unstr,fmt='(a10,'//nstr//'(f16.5),'//nstr//'(i5))') &
          name%ipt,par(1:ipt%ncomp),fit(1:ipt%ncomp)
  else
     unstr=''
  end if

end subroutine par_write

subroutine linatmpar_write(unstr,name,nlen,par,fit)
  use all_type
  use ipt_decl
  implicit none
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  integer(4) :: nlen
  character(LEN=nlen) :: name
  real(4) :: par(maxatm)
  integer(2) :: fit(maxatm)
  character(2) :: nstr
  character(LEN=maxlonstr) :: unstr

  write(unit=nstr,fmt='(i2.2)') ipt%ncomp
  write(unit=unstr,fmt='(a16,'//nstr//'(f16.5),'//nstr//'(i5))') &
       name(1:nlen),par(1:ipt%ncomp),fit(1:ipt%ncomp)
end subroutine linatmpar_write

subroutine linpar_write(unstr,name,nlen,par,fit)
  use all_type
  use ipt_decl
  implicit none
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  integer(4) :: nlen
  character(LEN=nlen) :: name
  real(4) :: par
  integer(2) :: fit
  character(LEN=maxlonstr) :: unstr


  write(unit=unstr,fmt='(a15,f16.5,i5)') &
       name(1:nlen),par,fit
end subroutine linpar_write

subroutine write_atm(atm,natm,line,nline,blend,nblend,gen, &
     fitness,fitprof,resultname,xyinfo)
  use all_type
  use ipt_decl
  use tools
  implicit none
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  integer(2) :: natm,nprof,nline,nofit(maxatm),nblend
  type (atmtyp) :: atm(natm)
  type (blendtyp) :: blend(nblend)
  type (gentyp) :: gen
  type (linetyp) :: line(maxlin)
  type (proftyp) :: fitprof
  character(LEN=maxstr) :: resultname,file,file_atm,file_prof,path
  character(LEN=80) :: sep
  character(LEN=idlen) :: idstr
  character(LEN=*) :: xyinfo
  integer(4) :: ierr1,ierr2,i
  integer(1) :: id(idlen)
  real(4) :: fitness
  integer(4) :: date_time(8)
  character(len=10) :: big_ben (3)
  character(1) :: nstr
  type(parnametyp) :: parname
  type(blendnametyp) :: blendname
  type (gennametyp) :: genname
  real(8) :: wlref
  character (LEN=maxlonstr) :: output(1024)
  integer(2) :: io

  io=0
  sep=repeat('-',78)
  call date_and_time(big_ben (1), big_ben (2), big_ben (3), date_time)
!!!!!!!!!!!!!!!!!!!! atmosphere
     !header:
     write(unit=output(ipp(io)),fmt='(a)') trim(resultname(1:len_trim(resultname)))
     write(unit=output(ipp(io)),fmt='(a)') 'Date: '//trim(big_ben(1))//' Time: '//trim(big_ben(2))
     write(unit=output(ipp(io)),fmt='(a)') 'Input File: '//ipt%file(1:len_trim(ipt%file))
     write(unit=output(ipp(io)),fmt='(a)') 'Observation: '//ipt%observation(1:len_trim(ipt%observation))
     write(unit=output(ipp(io)),fmt='(a,f12.4)') 'Fitness: ',fitness
     select case(int(ipt%modeval))
        case (0)
           write(unit=output(ipp(io)),fmt='(a)') 'MODE: GAUSS'
        case (1)
           write(unit=output(ipp(io)),fmt='(a)') 'MODE: VOIGT'
        case (2)
           write(unit=output(ipp(io)),fmt='(a)') 'MODE: VOIGT_PHYS'
        case (3)
           write(unit=output(ipp(io)),fmt='(a)') 'MODE: VOIGT_GDAMP'
        case (4)
           write(unit=output(ipp(io)),fmt='(a)') 'MODE: VOIGT_SZERO'
        case (5)
           write(unit=output(ipp(io)),fmt='(a)') 'MODE: HANLE_SLAB'
        end select
        write(idstr,fmt='(i3)') ipt%nmi
     write(unit=output(ipp(io)),fmt='(a,'//trim(idstr)//'i6)') 'NCALLS: ',ipt%ncalls(1:ipt%nmi)
     write(unit=output(ipp(io)),fmt='(a,i2)') 'NCOMP: ',ipt%ncomp
     write(unit=output(ipp(io)),fmt='(a)') trim(sep)
     !parameters
     call def_parname(parname)
     call par_write(output(ipp(io)),parname%b,atm%par%b,atm%fit%b)
     call par_write(output(ipp(io)),parname%azi,atm%par%azi,atm%fit%azi)
     call par_write(output(ipp(io)),parname%inc,atm%par%inc,atm%fit%inc)
     call par_write(output(ipp(io)),parname%vlos,atm%par%vlos,atm%fit%vlos)
     call par_write(output(ipp(io)),parname%width,atm%par%width,atm%fit%width)
     call par_write(output(ipp(io)),parname%damp,atm%par%damp,atm%fit%damp)
     call par_write(output(ipp(io)),parname%dopp,atm%par%dopp,atm%fit%dopp)
     call par_write(output(ipp(io)),parname%a0,atm%par%a0,atm%fit%a0)
     call par_write(output(ipp(io)),parname%szero,atm%par%szero,atm%fit%szero)
     call par_write(output(ipp(io)),parname%sgrad,atm%par%sgrad,atm%fit%sgrad)
     call par_write(output(ipp(io)),parname%etazero,atm%par%etazero,atm%fit%etazero)
     call par_write(output(ipp(io)),parname%gdamp,atm%par%gdamp,atm%fit%gdamp)
     call par_write(output(ipp(io)),parname%vmici,atm%par%vmici,atm%fit%vmici)
     call par_write(output(ipp(io)),parname%densp,atm%par%densp,atm%fit%densp)
     call par_write(output(ipp(io)),parname%tempe,atm%par%tempe,atm%fit%tempe)
     call par_write(output(ipp(io)),parname%dslab,atm%par%dslab,atm%fit%dslab)
     call par_write(output(ipp(io)),parname%height,atm%par%height,atm%fit%height)
     call par_write(output(ipp(io)),parname%ff,atm%par%ff,atm%fit%ff)

     !blend parameters
     call def_blendname(blendname)
     if (maxval(blend%par%a0).gt.1e-10) then
        write(unit=output(ipp(io)),fmt='(a)') trim(sep)
        write(unit=output(ipp(io)),fmt='(a)') 'Blend Parameters:'
        write(unit=output(ipp(io)),fmt='(a,i3)') 'NBLEND:',nblend
        write(unit=output(ipp(io)),fmt='(a)') trim(sep)
        write(unit=nstr,fmt='(i1)') nblend
        write(unit=output(ipp(io)),fmt='(a12,'//nstr//'(f16.5),'//nstr//'(i5))') &
             blendname%wl%ipt,blend(1:nblend)%par%wl, &
             blend(1:nblend)%fit%wl
        write(unit=output(ipp(io)),fmt='(a12,'//nstr//'(f16.5),'//nstr//'(i5))') &
             blendname%width%ipt,blend(1:nblend)%par%width, &
             blend(1:nblend)%fit%width
        write(unit=output(ipp(io)),fmt='(a12,'//nstr//'(f16.5),'//nstr//'(i5))') &
             blendname%damp%ipt,blend(1:nblend)%par%damp, &
             blend(1:nblend)%fit%damp
        write(unit=output(ipp(io)),fmt='(a12,'//nstr//'(f16.5),'//nstr//'(i5))') &
             blendname%a0%ipt,blend(1:nblend)%par%a0, &
             blend(1:nblend)%fit%a0
     end if

     !blend parameters
     call def_genname(genname)
     if ((gen%fit%ccorr.ne.0).or.(gen%fit%straylight.ne.0)&
          .or.(gen%fit%radcorrsolar.ne.0)) then
        write(unit=output(ipp(io)),fmt='(a)') trim(sep)
        write(unit=output(ipp(io)),fmt='(a)') 'General Parameters:'
        if (gen%fit%ccorr.ne.0) then 
           write(unit=output(ipp(io)),fmt='(a)') trim(sep)
           write(unit=output(ipp(io)),fmt='(a14,(f16.5),(i5))') &
                genname%ccorr%ipt,gen%par%ccorr,gen%fit%ccorr
        end if
        if (gen%fit%straylight.ne.0) then 
           write(unit=output(ipp(io)),fmt='(a)') trim(sep)
           write(unit=output(ipp(io)),fmt='(a14,(f16.5),(i5))') &
                genname%straylight%ipt,gen%par%straylight,gen%fit%straylight
        end if
        if (gen%fit%radcorrsolar.ne.0) then 
           write(unit=output(ipp(io)),fmt='(a)') trim(sep)
           write(unit=output(ipp(io)),fmt='(a14,(f16.5),(i5))') &
                genname%radcorrsolar%ipt,gen%par%radcorrsolar,&
                gen%fit%radcorrsolar
        end if
     end if

     !line parameters
     write(unit=output(ipp(io)),fmt='(a)') trim(sep)
     write(unit=output(ipp(io)),fmt='(a)') 'Line Parameters:'
     write(unit=output(ipp(io)),fmt='(a,i3)') 'NLINE:',nline
     nofit=0
     do i=1,ipt%nline
        write(unit=output(ipp(io)),fmt='(a)') trim(sep)
        id=line(i)%id
        where (id.eq.0)
           id=32
        endwhere
        write(unit=output(ipp(io)),fmt='(20a)') char(id)
        call linatmpar_write(output(ipp(io)),'VLOS',4,line(i)%straypol_par%vlos,nofit)
        call linatmpar_write(output(ipp(io)),'WIDTH',5,line(i)%straypol_par%width,nofit)
        call linatmpar_write(output(ipp(io)),'DAMP',4,line(i)%straypol_par%damp,nofit)
        call linatmpar_write(output(ipp(io)),'DOPP',4,line(i)%straypol_par%dopp,nofit)
        call linpar_write(output(ipp(io)),'STRAYPOL_AMP',12, &
             line(i)%par%straypol_amp,line(i)%fit%straypol_amp)
        call linpar_write(output(ipp(io)),'STRAYPOL_ETA0',13, &
             line(i)%par%straypol_eta0,line(i)%fit%straypol_eta0)
        call linpar_write(output(ipp(io)),'STRENGTH',8, &
             line(i)%par%strength,line(i)%fit%strength)
        call linpar_write(output(ipp(io)),'WLSHIFT',7, &
             line(i)%par%wlshift,line(i)%fit%wlshift)
     end do


!do the writing
  file=resultname(1:len_trim(resultname))
  path=file(1:index(trim(file),'/',back=.true.))
  file_atm =path(1:len_trim(path))//xyinfo(1:len_trim(xyinfo))//'_atm.dat'
  file_prof=path(1:len_trim(path))//xyinfo(1:len_trim(xyinfo))//'_profile.dat'
  call mkdirhier(path,len_trim(path),'755',ierr1)
  open(10,file=trim(file_atm),status='replace',iostat=ierr2,action='WRITE')
  if (ierr2.ne.0) then
     write(*,*) 'Error writing atmosphere to file '//trim(file_atm)
     write(*,*) 'Please check directory structure.'
     write(*,*) 'Result NOT STORED!!!'
  else
     do i=1,io 
        if (len_trim(output(i)).ge.1) write(10,fmt='(a)') trim(output(i))
     end do
     close(10)
     if (ipt%verbose.ge.1) &
          write(*,*) 'Atmosphere written to '//trim(file_atm)
  end if

  if (ipt%save_fitprof.eq.1) then
!!!!!!!!!!!!!!!!!!!! profile in stopro format
     open(10,file=trim(file_prof),status='replace',iostat=ierr2,action='WRITE')
     if (ierr2.ne.0) then
        write(*,*) 'Error writing profile to file '//trim(file_prof)
        write(*,*) 'Please check directory structure.'
        write(*,*) 'Result NOT STORED!!!'
     else
        nprof=1
        write(10,fmt='(i3,2x,a1,''WIVQU'',a1,3x,a,1x,a,1x,a,1x,a,'':'',a)') &
             nprof,'''','''',trim(ipt%observation),trim(xyinfo), &
             trim(ipt%file),trim(big_ben(1)),trim(big_ben(2))
        idstr=" "
        if ((fitprof%wlref.eq.0).and.(ipt%nline.ge.1)) then
           wlref=line(1)%wl
        else
           wlref=fitprof%wlref
        end if
        write(10,fmt='(i5,f16.4,i6,ES16.5,''  |  '',4i5,3x,a1,a,a1)') &
             nprof,wlref,fitprof%nwl,fitprof%ic,1,fitprof%nwl,1, &
             fitprof%nwl,'''',trim(idstr),''''
        do i=1,fitprof%nwl
           write(10,fmt='(5ES15.6)') &
                fitprof%wl(i)-wlref,fitprof%i(i)/fitprof%ic,&
                fitprof%v(i),fitprof%q(i),fitprof%u(i)
        end do
        write(10,*)
        if (ipt%verbose.ge.1) &
             write(*,*) 'Fit-Profile written to '//trim(file_prof)
        close(10)
     end if
  end if
  
99 format (a)

end subroutine write_atm
