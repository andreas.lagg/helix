subroutine get_pb_splitting(line,B,splitting,pb_method)
  use all_type
  use interpol
  implicit none
  type (linetyp) :: line
  integer(2) :: i,pb_method
  real(4) :: B,x2,x3,x4,x5,bwl2
  real(8),dimension(max_pb_zl) :: splitting
!  real(8) :: interpolate
!  external :: interpolate

  select case (pb_method)
  case (1_2)
     do i=1,line%pb%nr_zl
        splitting(i)=interpolate(real(B,kind=8), &
             real(line%pb%b(1:line%pb%nr_b),kind=8), &
             real(line%pb%splitting(i,1:line%pb%nr_b),kind=8),2_2,line%pb%nr_b)
     end do
  case (0_2)
     x2=(b*1e-3)**2
     x3=(b*1e-3)**3
     x4=(b*1e-3)**4
     x5=(b*1e-3)**5
     bwl2=b*line%wl**2*4.6686411e-13
     do i=1,line%quan%n_sig 
        splitting(i)= &
             bwl2*line%quan%NUR(i) + &
             1e-3*(line%pb%cr(i,1)*x2 + line%pb%cr(i,2)*x3 + &
             line%pb%cr(i,3)*x4 + line%pb%cr(i,4)*x5)
        splitting(i+line%quan%n_sig+line%quan%n_pi)= &
             bwl2*line%quan%NUB(i) + &
             1e-3*(line%pb%cb(i,1)*x2 + line%pb%cb(i,2)*x3 + &
             line%pb%cb(i,3)*x4 + line%pb%cb(i,4)*x5)
     end do
     do i=1,line%quan%n_pi 
        splitting(i+line%quan%n_sig)= &
             bwl2*line%quan%NUP(i) + &
             1e-3*(line%pb%cp(i,1)*x2 + line%pb%cp(i,2)*x3 + &
             line%pb%cp(i,3)*x4 + line%pb%cp(i,4)*x5)
     end do
  end select


end subroutine get_pb_splitting
