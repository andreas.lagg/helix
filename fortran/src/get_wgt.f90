subroutine get_wgt(wgt,obs,im)
  use all_type
  use ipt_decl
  use tools
  implicit none
  type (proftyp) :: wgt,obs
!  type (ipttyp) :: ipt
!  common /iptcom/ ipt
  character(LEN=maxstr) :: file,line,word(10)
  character :: fc
  integer(4) :: i,err,j,sindex,nw
  integer(2) :: iu,im,i0,i1,im1,cnt
  logical :: iok,qok,uok,vok
  real(4) :: wgtval
  real(8) :: wlmin,wlmax
!  external str2real
!  external str2double
!  real(4) str2real
!  real(8) str2double
  real(8) mwgt(4)


  wgt=obs
  wgt%i=0.
  wgt%q=0.
  wgt%u=0.
  wgt%v=0.

  if (im.eq.0) then
     i0=1
     i1=ipt%nmi
  else
     i0=im
     i1=im
  end if

do im1=i0,i1

   iu=100
   file=ipt%dir%wgt(1:len_trim(ipt%dir%wgt))//trim(ipt%wgt_file(im1))
   cnt=0
   err=-1
   do while (err.ne.0)
      open(iu,file=trim(file),iostat=err,action='READ',form='FORMATTED')
      if (err.ne.0) then
         write(*,*) 'Could not find wgt-file '//trim(file)//&
              ', using default.wgt',err,cnt
         file=ipt%dir%wgt(1:len_trim(ipt%dir%wgt))//'default.wgt'
         if (cnt.ge.2) call stop
      end if
      cnt=cnt+1
   end do

   i=1
   do while (err.eq.0)
      read(iu, fmt='(a)',iostat=err) line
      !get fist character
      fc=line(1:1)

      if ((fc.eq.'I').or.(fc.eq.'Q').or.(fc.eq.'U').or.(fc.eq.'V')) then
         !split into words(space=delimiter)
         j=1
         sindex=1
         word=''
         !split input line into words separated by spaces
         do while ((sindex.gt.0).and.(j.le.10).and.len(trim(line)).gt.0)
            sindex=index(line,' ')
            if (sindex.gt.1) then
               word(j)=line(1:sindex-1)
               line=trim(line(sindex+1:))
               j=j+1
            else
               if (sindex.eq.1) line=trim(line(sindex+1:))
            end if
         end do
         nw=j-1
         wgtval=str2real(word(2))
         wlmin=str2double(word(3))
         wlmax=str2double(word(4))
!         nmi=ipt%nmi
!         if (ipt%straypol_corr.gt.0) then
!            nmi=nmi-1
!         end if
         mwgt=ipt%iquv_weight(:,im1)
         mwgt=mwgt/sum(mwgt)*4
         select case (fc)
         case ('I')
            where ((wgt%wl.ge.wlmin).and.(wgt%wl.le.wlmax)) &
            wgt%i=wgtval*mwgt(1)
            iok=.true.
         case ('Q')
            where ((wgt%wl.ge.wlmin).and.(wgt%wl.le.wlmax)) &
            wgt%q=wgtval*mwgt(2)
            qok=.true.
         case ('U')
            where ((wgt%wl.ge.wlmin).and.(wgt%wl.le.wlmax)) &
            wgt%u=wgtval*mwgt(3)
            uok=.true.
         case ('V')
            where ((wgt%wl.ge.wlmin).and.(wgt%wl.le.wlmax)) &
            wgt%v=wgtval*mwgt(4)
            vok=.true.
         case default
         end select
      end if
      i=i+1
   end do
   close(iu)
end do

if (.not.iok) write(*,*) 'WARNING: no weighting defined for ''I'''
if (.not.qok) write(*,*) 'WARNING: no weighting defined for ''Q'''
if (.not.uok) write(*,*) 'WARNING: no weighting defined for ''U'''
if (.not.vok) write(*,*) 'WARNING: no weighting defined for ''V'''

if (ipt%verbose.ge.2) then
   write(*,*) 'Weight function: ',trim(file)
end if

end subroutine get_wgt
