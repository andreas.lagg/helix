!THIS SUBROUTINE CALCULATE THE SHIFT AND STRENGTH OF THE
!ANOMALOUS ZEEMAN EFFECT IN LS COUPLING
!AND CREATE A COMMON BLOCK N_CUANTEN WITH ALL THE PARAMETERS

!THE STRUCTURE OF C_N IS THE FOLLOWING:
!IS A VECTOR STRUCTURE  CONTAINING THE DIFERENT LINES
!IN THE CASE OF HELIUM THE DIMENSION IS THREE
!INSIDE YOU WILL FIND THE SHIFTS AND THE STRENGTHS
!IN ORDER TO CREATE A STRUCTURE MORE EASILY, THE DIMENSION OF
!THE VARIABLES IS SET UP TO THE BIGGER ONE, AND THIS IS BECAUSE 
!YOU ALSO HAVE THE NUMBER OF PI AND SIGMA COMPONENTS
!WITH THESE TWO VARIABLES YOU KNOW ALLWAIS WHICH IS THE
!SPLITTING OF THE LINES, WHICH DOES NOT COINCIDE WITH THE REAL DIMENSION  
!THEY ARE N_PI, N_SIG (NUMBER OF COMPONENTS)
!NUP(N_PI),NUB(N_SIG),NUR(N_SIG) SHIFTS OF PRINCIPAL,BLUE AND RED COMPONENTS
!WEP(N_PI),WEB(N_SIG),WER(N_SIG) RESPECTIVE STRENGTHS
!IN GENERAL: CUANTEN(LINE).NUP ..... ETCETERA
subroutine create_nc(line,nline,verbose)
  use all_type
  implicit none
  integer(2) :: verbose,nline
  type (linetyp) :: line(nline)
  integer(2) :: i,n_pi,n_sig
  real(4),dimension(nline) :: sl,su,ll,lu,jl,ju
  real(8) :: glo,gup,gef
  real(8),dimension(maxsigma) :: nub,nur,web,wer
  real(8),dimension(maxpi) :: wep,nup
  character(idlen) :: astr

  sl=line%sl ; su=line%su
  ll=line%ll ; lu=line%lu
  jl=line%jl ; ju=line%ju

  
!ASSIGN THE VARIABLES TO THE STRUCTURE
  do I=1,nline
     N_PI=2.*MINVAL((/jl(I),ju(I)/))+1
     N_SIG=jl(I)+ju(I)
                                !CUANTEN COMPUTES THE CUANTEM NUMBERS
     call CUANTEN(SL(I),SU(I),LL(I),LU(I),JL(I),JU(I), &
          NUB,NUP,NUR,WEB,WEP,WER,GLO,GUP,GEF)
     LINE(I)%QUAN%N_PI=N_PI
     LINE(I)%QUAN%N_SIG=N_SIG
     LINE(I)%QUAN%NUB=NUB
     LINE(I)%QUAN%NUP=NUP
     LINE(I)%QUAN%NUR=NUR
     LINE(I)%QUAN%WEB=WEB
     LINE(I)%QUAN%WEP=WEP
     LINE(I)%QUAN%WER=WER
     line(i)%GL=GLO
     line(i)%GU=GUP
     line(i)%GEFF=GEF
     if (verbose.ge.1) then
         write(unit=astr,fmt='(12a)') char(line(i)%id)
         write(*,'(a14,3(a6,f7.4),2(a6,i2))') &
              astr,'geff=',gef,'gup=',gup,'glo=',glo, &
              'npi=',n_pi,'nsig=',n_sig
    endif
  end do

end subroutine create_nc
