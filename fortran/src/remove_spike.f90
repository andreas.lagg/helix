!removes single electronic spikes
subroutine remove_spike(obs,nwl,spike,verbose,par,first)
  use all_type
  real(4), dimension(maxwl) :: obs,diffsig,diff,med
  integer(2) :: nwl,spike,verbose,ns,i,first
  real(4) :: stdev
  character :: par

                                !calculate STDEV of difference between
                                !WL points of signal
  diffsig(1:nwl-1)=abs(obs(2:nwl)-obs(1:nwl-1))
  stdev=sqrt(sum((diffsig(1:nwl-1)-sum(diffsig)/(nwl-1.))**2)/(nwl-1.))
                                !calculate difference between original and
                                !median profile
  med=obs
  call median(med(1:nwl),nwl,2_2+spike)
  diff=0
  diff(1:nwl)=abs(obs(1:nwl)-med(1:nwl))
                                !identify spike if diff is larger than
                                !5*stdev
  if (maxval(diff).gt.5*stdev) then 
    where (diff.gt.5*stdev) obs=med
  
    if ((verbose.eq.1).and.(first.eq.1)) then
      write(*,*) 'Spike(s) removed.'
      first=0
    end if
    if (verbose.ge.2) then
      ns=0
      do i=1,nwl
        if (diff(i).gt.5*stdev) ns=ns+1
      end do
      write(*,*) ns,' spikes removed in Stokes ',par
    end if
  end if
end subroutine remove_spike
