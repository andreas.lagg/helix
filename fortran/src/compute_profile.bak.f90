!!    compile with:
!!    compute profile_
!!    equivalent to compute_profile.pro
!!    calculates profiule from given atmosphere atm defines in all_type
!!    .f90

!!    =======================================================      
#ifdef IDL
SUBROUTINE compute_profile_pro(argc, argv) !Called by IDL
#ifdef bit64
  INTEGER(8) argc, argv(*)   !Argc and Argv are integers
#else
  INTEGER(4) argc, argv(*)   !Argc and Argv are integers
#endif

  j = LOC(argc)             !Obtains the number of arguments (argc)
  !Because argc is passed by VALUE.

  !!    Call subroutine compute_profile, converting the IDL parameters
  !!    to standard FORTRAN, passed by reference arguments:

  CALL compute_profile(%VAL(argv(1)), %VAL(argv(2)), %VAL(argv(3)), &
       %VAL(argv(4)), %VAL(argv(5)), %VAL(argv(6)), %VAL(argv(7)), &
       %VAL(argv(8)), %VAL(argv(9)), %VAL(argv(10)), %VAL(argv(11)), &
       %VAL(argv(12)), %VAL(argv(13)), %VAL(argv(14)), %VAL(argv(15)), &
       %VAL(argv(16)), %VAL(argv(17)), %VAL(argv(18)), %VAL(argv(19)), &
       %VAL(argv(20)), %VAL(argv(21)), %VAL(argv(22)), %VAL(argv(23)), &
       %VAL(argv(24)), %VAL(argv(25)), %VAL(argv(26)), %VAL(argv(27)), &
       %VAL(argv(28)), %VAL(argv(29)), %VAL(argv(30)), %VAL(argv(31)), &
       %VAL(argv(32)) )
END SUBROUTINE compute_profile_pro
#endif

!!    ========================================================
!!    compute the gauss profile      
!!    ========================================================
subroutine gaussfunc(a0,shift,width2,wl,nwl,gprof,addnum)
  implicit none
  integer(2) :: nwl
  real(4) :: a0
  real(4) :: width2
  real(8) :: gprof(nwl),expo(nwl)
  real(8) :: shift
  real(8) :: wl(nwl)
  real(4) :: addnum

  expo=(-(wl-shift)**2./(width2))
  !  expo=(expo+40.)*(expo>(-40.))-40.
  gprof=a0 * exp(expo) / sqrt(3.1415927*width2) + addnum

end subroutine gaussfunc

!!    ========================================================
!!    compute the voigt profile      
!!    ========================================================
subroutine voigtfunc(shift,damp,dopp,wl,nwl,hprof,fprof)
  implicit none
  integer(2) :: nwl
  !  real(4) :: a0
  real(4) :: dopp
  real(4) :: damp
  real(8) :: shift
  real(8) :: wl(nwl)
  !  real(4) :: addnum
  real(4), dimension(nwl) :: hprof,fprof

  real(8) :: vvec(nwl)
  !  real(4) :: delta_vd
  !  real(4), dimension(nwl) :: H,F

  if (damp.le.1e-5) write(*,*) 'DAMP IS LESS THAN 1E-5!!!'
  if (dopp.le.1e-5) write(*,*) 'DOPP IS LESS THAN 1E-5!!!'

  vvec=(wl-shift)/dopp


!  call fvoigt(nwl,damp,vvec,hprof,fprof)
  call voigt(nwl,damp,vvec,hprof,fprof)

  !voigt integral to be multiplied by a/!pi
  !(see e.g.) Jefferies ApJ 1989 343
!  fprof=damp/3.1415927*fprof
!  fprof=2.*fprof
  !  hprof=a0*hprof
  !  fprof=a0*fprof
end subroutine voigtfunc


!!    ========================================================

!!    This subroutine is called by compute_profile_pro and has no
!!    IDL specific code. Here the computation of the line is done.
!!    
!!    ========================================================
SUBROUTINE compute_profile(atm,natm,line,nline, &
     wl,nwl,blend,nblend,gen, &
     convval,doconv, &
     ret_wlidx,nret_wlidx, &
     obs_par,init,voigt,magopt,use_geff,use_pb,pb_method, &
     modeval,iprof_only,luse,iprof,qprof,uprof,vprof,normff,old_norm,&
     icomp,hanle_azi,norm_stokes_val)
  use all_type
  implicit none
  integer(2) :: nwl,natm,nline,nblend,doconv,nret_wlidx,normff,old_norm,icomp
  integer(2) :: init,voigt,magopt,use_geff,use_pb,pb_method,modeval,iprof_only
  integer(2) :: norm_stokes_val
  real(4), dimension(nwl) :: iprof,qprof,uprof,vprof,hprof,fprof
  real(4), dimension(nwl) :: fiprof,fqprof,fuprof,fvprof
  real(4), dimension(nwl) :: convfft,convval
  real(4), dimension(4*nwl+15) :: wsave
  integer(2),dimension(maxwl) :: ret_wlidx
  real(8), dimension(nwl) :: wl
  real(8), dimension(nwl) :: gsub,qsub,usub
  real(4) :: hanle_azi
  integer(2) :: luse(nline,natm)
  type (atmtyp) :: atm(natm)
  type (blendtyp) :: blend(nblend)
  type (gentyp) :: gen
  type (linetyp) :: line(nline)
  type (obstyp) :: obs_par
  integer(2) :: il,ia,it,ii,ib,ia1,ia2
  character(2) :: linid

  real(8) :: dopshift_wl,zeeman_shift
  real(8),dimension(maxsigma) :: zeeman_b,zeeman_r
  real(8),dimension(maxpi) :: zeeman_p
  real(8),dimension(max_pb_zl) :: web,wer,wep
  !  real(8), PARAMETER :: c_light=299792512.00000
  !  real(4), parameter :: dtor=0.017453292384744
  real(8), dimension(nwl) :: gprof
  real(4), dimension(nwl) :: i_center,i_plus,i_minus, &
       rho_p,rho_minus,rho_plus,eta_v,rho_v, &
       quprof,eta_q,eta_u,eta_i, &
       rho_q,rho_u,rho_qu, &
       F,R,F2DELTA,DELTA,B0,I,Q,U,V
  real(4) :: sininc2,cosinc,sin2azi,cos2azi,width2,FF(natm),straydamp
  real(4),parameter :: addnum=1e-4
  real(8),dimension(max_pb_zl) :: splitting,strength
  external norm_ff


  iprof=0.
  qprof=0.
  uprof=0.
  vprof=0.

  if (icomp.eq.-1) then
     ia1=1
     ia2=natm
  else
     ia1=icomp
     ia2=icomp
  end if

  FF=atm(1:natm)%par%FF
  if (normff.eq.1) call norm_ff(FF,atm(1:natm)%fit%FF,atm(1:natm)%linid,natm)
  do ia=ia1,ia2
     width2=2.*atm(ia)%par%width**2.
     
     sininc2=sin(atm(ia)%par%inc*0.01745329)**2.
     cosinc=cos(atm(ia)%par%inc*0.01745329)
     sin2azi=sin(atm(ia)%par%azi*0.01745329*2.)
     cos2azi=cos(atm(ia)%par%azi*0.01745329*2.)
     do il=1,nline
        linid=char(line(il)%id(1))//char(line(il)%id(2))


        if (luse(il,ia).eq.1) then
           if (modeval.eq.1) then    !voigt-par mode
              !              dopp=atm(ia)%par%dopp
              !              damp=atm(ia)%par%damp
              !              etazero=atm(ia)%par%etazero
           else if (modeval.eq.2) then !voigt-phys mode (see Balasubramaniam,
              !ApJ 382, 699-705 1991, original:
              !Landi Degl'Innocenti, A&AS 25, 379-390, 1976)
              atm(ia)%par%dopp=line(il)%wl / 2.99792512e8 * &
                   sqrt(2*1.3805e-23*atm(ia)%par%tempe/ &
                   (line(il)%mass*1.67000e-27)+atm(ia)%par%vmici**2)
              atm(ia)%par%damp=atm(ia)%par%gdamp * line(il)%wl**2 / &
                   (4*3.1415927*2.99792512e8*atm(ia)%par%dopp)
              atm(ia)%par%etazero=atm(ia)%par%densp*line(il)%wl**2/ &
                   atm(ia)%par%dopp*1e-10* &
                   (1.-exp(-1.43883e8/(line(il)%wl*atm(ia)%par%tempe)))
           else if ((modeval.eq.3).or.(modeval.eq.4)) then !voigt-gdamp mode
              atm(ia)%par%damp=atm(ia)%par%gdamp * line(il)%wl**2 / &
                   (4*3.1415927*2.99792512e8*atm(ia)%par%dopp)
           end if

           dopshift_wl = atm(ia)%par%vlos * line(il)%wl / 2.99792512e8
           !*DAVID
           IF (use_geff.eq.1) THEN 
              zeeman_shift = 4.6686411E-13 * line(il)%wl**2. * &
                   line(il)%geff &
                   * atm(ia)%par%b
           else if ((use_pb.eq.1).and.(linid(1:2).eq.'He')) then
              !use tabulated PB-effect
              call get_pb_splitting(line(il),atm(ia)%par%b,splitting,pb_method)
              call get_pb_strength(line(il),atm(ia)%par%b,strength,pb_method)
              strength=strength/line(il)%f
              zeeman_b(1:line(il)%quan%n_sig)= &
                   splitting(line(il)%quan%n_sig+line(il)%quan%n_pi+1: &
                   line(il)%quan%n_sig*2+line(il)%quan%n_pi)
              zeeman_r(1:line(il)%quan%n_sig)= &
                   splitting(1:line(il)%quan%n_sig)
              zeeman_p(1:line(il)%quan%n_pi)= &
                   splitting(line(il)%quan%n_sig+1: &
                   line(il)%quan%n_sig+line(il)%quan%n_pi)
              web(1:line(il)%quan%n_sig)= &
                   strength(line(il)%quan%n_sig+line(il)%quan%n_pi+1: &
                   line(il)%quan%n_sig*2+line(il)%quan%n_pi)
              wer(1:line(il)%quan%n_sig)= &
                   strength(1:line(il)%quan%n_sig)
              wep(1:line(il)%quan%n_pi)= &
                   strength(line(il)%quan%n_sig+1: &
                   line(il)%quan%n_sig+line(il)%quan%n_pi)
           else
              zeeman_B=atm(ia)%par%b*line(il)%wl**2. * &
                   4.6686411e-13*line(il)%quan%NUB
              zeeman_P=atm(ia)%par%b*line(il)%wl**2. * &
                   4.6686411e-13*line(il)%quan%NUP
              zeeman_R=atm(ia)%par%b*line(il)%wl**2. * &
                   4.6686411e-13*line(il)%quan%NUR
              web(1:line(il)%quan%n_sig)= &
                   line(il)%quan%WEB(1:line(il)%quan%n_sig)
              wer(1:line(il)%quan%n_sig)= &
                   line(il)%quan%WER(1:line(il)%quan%n_sig)
              wep(1:line(il)%quan%n_pi)= &
                   line(il)%quan%WEP(1:line(il)%quan%n_pi)
           end if
           !END DAVID

           if (iprof_only.eq.0) then !flag to determine if only fit to
              !i-profile for straypol_run is to be done

              !!    calculate central component (PI)
              if (voigt.eq.1) then
                 IF (use_geff.eq.1) THEN 
                    call voigtfunc(line(il)%wl + dopshift_wl + &
                         line(il)%par%wlshift, &
                         atm(ia)%par%damp, &
                         atm(ia)%par%dopp, &
                         wl,nwl,hprof,fprof)
                    i_center= 1*0.5*(hprof)* &
                         atm(ia)%par%etazero * line(il)%par%strength
                    if (magopt.eq.1) then
                       rho_p=1*0.5*(fprof) * &
                            atm(ia)%par%etazero * line(il)%par%strength
                    else
                       rho_p=0.
                    end if
                 else
                    !*DAVID
                    !DEFINITIONS
                    i_center=0 ; rho_p=0 
                    do II=1,line(il)%quan%N_PI
                       call voigtfunc(line(il)%wl+dopshift_wl-zeeman_P(II) + &
                            line(il)%par%wlshift, &
                            atm(ia)%par%damp, &
                            atm(ia)%par%dopp, &
                            wl,nwl,hprof,fprof)

                       i_center= i_center+ &
                            0.5*hprof*atm(ia)%par%etazero*wep(II) * &
                            line(il)%par%strength
                       if (magopt.eq.1) then
                          rho_p=rho_p + 0.5*fprof*atm(ia)%par%etazero * &
                               wep(II) * line(il)%par%strength
                       end if
                    end do
                 end if
                 !END DAVID
              else
                 call gaussfunc(atm(ia)%par%a0 * line(il)%par%strength, &
                      line(il)%wl + dopshift_wl + line(il)%par%wlshift, &
                      width2, &
                      wl,nwl,gprof,addnum)
                 i_center=1*0.5*(gprof-addnum)
                 rho_p=0.
              end if              !! voigt
              !!    calculate magnetic profiles only if b
              !!    is to be fitted
              if ((atm(ia)%fit%b.eq.1).or. &
                   (atm(ia)%par%b.ne.0)) then

                 !!    calculate shifted component (SIGMA)
                 if (voigt.eq.1) then
                    IF (use_geff.eq.1) THEN 
                       call voigtfunc(line(il)%wl+dopshift_wl-zeeman_shift + &
                            line(il)%par%wlshift, &
                            atm(ia)%par%damp, &
                            atm(ia)%par%dopp, &
                            wl,nwl,hprof,fprof)
                       i_minus     =0.5*(hprof)* atm(ia)%par%etazero * &
                            line(il)%par%strength
                       if (magopt.eq.1) then
                          rho_minus=0.5*(fprof)* atm(ia)%par%etazero * &
                               line(il)%par%strength
                       else
                          rho_minus=0.
                       end if
                       call voigtfunc(line(il)%wl+dopshift_wl+zeeman_shift + &
                            line(il)%par%wlshift, &
                            atm(ia)%par%damp, &
                            atm(ia)%par%dopp, &
                            wl,nwl,hprof,fprof)
                       i_plus     =0.5*(hprof)* atm(ia)%par%etazero * &
                            line(il)%par%strength
                       if (magopt.eq.1) then
                          rho_plus=0.5*(fprof)* atm(ia)%par%etazero * &
                               line(il)%par%strength
                       else
                          rho_plus=0.
                       end if
                    else
                       !*DAVID
                       i_minus=0.   ; i_plus=0.
                       rho_minus=0. ; rho_plus=0.
                       do II=1,line(il)%quan%N_SIG
                          call voigtfunc(line(il)%wl+dopshift_wl- &
                               zeeman_r(ii) + line(il)%par%wlshift,&
                               atm(ia)%par%damp, &
                               atm(ia)%par%dopp, &
                               wl,nwl,hprof,fprof)
                          i_minus=i_minus + 0.5*hprof*atm(ia)%par%etazero* &
                               wer(II) * line(il)%par%strength
                          if (magopt.eq.1) then
                             rho_minus=rho_minus + 0.5 * &
                                  fprof*atm(ia)%par%etazero* &
                                  wer(II) * line(il)%par%strength
                          end if
                          call voigtfunc(line(il)%wl+dopshift_wl- &
                               zeeman_b(ii) + line(il)%par%wlshift,&
                               atm(ia)%par%damp, &
                               atm(ia)%par%dopp, &
                               wl,nwl,hprof,fprof)
                          i_plus=i_plus + &
                               0.5*hprof*atm(ia)%par%etazero* &
                               web(II) * line(il)%par%strength
                          if (magopt.eq.1) then
                             rho_plus=rho_plus + 0.5*fprof * &
                                  atm(ia)%par%etazero* &
                                  web(II) * line(il)%par%strength
                          end if
                       end do
                       !END DAVID
                    end if
                 else
                    call gaussfunc(atm(ia)%par%a0 * line(il)%par%strength, &
                         line(il)%wl + dopshift_wl - zeeman_shift + &
                         line(il)%par%wlshift, &
                         width2, &
                         wl,nwl,gprof,addnum)
                    i_minus=0.5*(gprof-addnum)
                    rho_minus=0.

                    call gaussfunc(atm(ia)%par%a0 * line(il)%par%strength, &
                         line(il)%wl + dopshift_wl + zeeman_shift + &
                         line(il)%par%wlshift, &
                         width2, &
                         wl,nwl,gprof,addnum)
                    i_plus=0.5*(gprof-addnum)
                    rho_plus=0.
                 end if            !! voigt

                 !!    compute v-profile: Sum of to
                 !!    gaussians with opposite sign, shifted
                 !!    by Zeeman splitting value
                 !!    see ronan et al, 1987 SoPhys vol 113, p353
                 eta_v = line(il)%f * 1./2.*(i_plus - i_minus) * cosinc
                 rho_v=  line(il)%f * 1./2.*(rho_plus - rho_minus) * cosinc

                 !!    compute q-profile: +Q direction
                 !!    defines azimuth angle zero.
                 !!    see ronan et al, 1987 SoPhys vol 113, p353
                 quprof = line(il)%f * 1./2.*( i_center - &
                      1./2.*(i_plus + i_minus) )
                 rho_qu = line(il)%f * 1./2.*( rho_p - &
                      1./2.*(rho_plus + rho_minus) )

                 eta_q= quprof * (sininc2) * cos2azi      
                 rho_q= rho_qu * (sininc2) * cos2azi
                 !!    u-profile is q profile x tan(2*azi)
                 !!    see auer et al., 1977 SoPhys, vol 55,
                 !!    p47
                 eta_u= quprof * (sininc2) * sin2azi
                 rho_u= rho_qu * (sininc2) * sin2azi

              else                !!fit B
                 !!    sigma components are the same as central pi
                 !!    component because of zero land shift.
                 eta_q=0
                 eta_u=0
                 eta_v=0
                 i_plus=i_center
                 i_minus=i_center
                 rho_q=0
                 rho_u=0
                 rho_v=0
                 rho_plus=rho_p
                 rho_minus=rho_p
              end if              !!fit B

              eta_i=line(il)%f * 1./2.*(i_center * (sininc2) &
                   + 1./2. * (i_minus + i_plus) * &
                   (1+(cosinc)**2.))

!              F=atm(ia)%par%ff
              R=(eta_q*rho_q + eta_u*rho_u +eta_v*rho_v)
              DELTA=( (1 + eta_i)**2. * &
                   ( (1 + eta_i)**2. - eta_q**2. - eta_u**2. - eta_v**2. &
                   + rho_q**2. + rho_u**2. + rho_v**2. ) - R**2.)
              F2DELTA=atm(ia)%par%sgrad * FF(ia)/DELTA

              !source function at tau=0: B0
              !B0 must match continuum level outside line
              !           B0 = atm(ia)%par%szero
              if (modeval.eq.4) then
                 B0=atm(ia)%par%szero
              else
                 B0 =1. - atm(ia)%par%sgrad
              end if

              I=B0*FF(ia) + F2DELTA*( (1+eta_i) * &
                   ((1+eta_i)**2. +rho_q**2.+rho_u**2.+rho_v**2.) )
              Q =-F2DELTA*( (1+eta_i)**2. * eta_q + &
                   (1+eta_i)*(eta_v*rho_u-eta_u*rho_v) + rho_q*R )
              U =-F2DELTA*( (1+eta_i)**2. * eta_u + &
                   (1+eta_i)*(eta_q*rho_v-eta_v*rho_q) + rho_u*R )
              V =-F2DELTA*( (1+eta_i)**2. * eta_v + &
                   (1+eta_i)*(eta_u*rho_q-eta_q*rho_u) + rho_v*R)
              !          V =-F2DELTA*( (1+eta_i)**2. * eta_v + (1+eta_i)*(eta_u*rho_q &
              !               -eta_q*rho_u)  + rho_v*R)

           else  !calculate only I profile with a simple fit of a voigt
              !or gaussian, no rad. transfer...
              !This is used only for the
              !straypol_run. The idea is to fit the
              !I profile with a single voigt or
              !gauss to determine the shape of the
              !profile to be used for the correction
              !in Q and U

              if (voigt.eq.1) then 
                 call voigtfunc(line(il)%wl + dopshift_wl, &
                      atm(ia)%par%damp, &
                      atm(ia)%par%dopp, &
                      wl,nwl,hprof,fprof)
                 gsub=hprof*atm(ia)%par%etazero*line(il)%par%strength
              else
                 call gaussfunc(atm(ia)%par%a0 * line(il)%par%strength, &
                      line(il)%wl + dopshift_wl, &
                      2.*atm(ia)%par%width**2., &
                      wl,nwl,gprof,addnum)
                 gsub=(gprof-addnum)
!                 gsub=(gprof-addnum)*atm(ia)%par%etazero* &
!                      FF(ia)*line(il)%f
              end if
              eta_i=line(il)%f*gsub
              DELTA=( (1 + eta_i)**2. * (1 + eta_i)**2.)
              F2DELTA=atm(ia)%par%sgrad * FF(ia)/DELTA
              if (modeval.eq.4) then
                 B0=atm(ia)%par%szero
              else
                 B0=1. - atm(ia)%par%sgrad
              end if
              I=B0*FF(ia) + F2DELTA*( (1+eta_i) * ((1+eta_i)**2.))
!              I=1./(1+gsub/2.)/natm !same as for i in 'real' case
              Q=0
              U=0
              V=0


           end if
!           qprof = qprof + Q /nline
!           uprof = uprof + U /nline
!           vprof = vprof + V /nline
!           iprof = iprof + I*line(il)%icont /nline
           qprof = qprof + Q
           uprof = uprof + U
           vprof = vprof + V
           !store only absorption signature in
           !I. This absorption signature is
           !subtracted from 1 after the loops
           !over lines & atmospheres
           if (old_norm.eq.1) then
              iprof = iprof + I - FF(ia)
           else if (old_norm.eq.0) then
              iprof = iprof + I - FF(ia)
           else if (old_norm.eq.2) then
              iprof = iprof + I
           endif
        end if                !!use line

        !add straypolarization fit
        if ((line(il)%straypol_par(ia)%width.gt.1e-5).or.&
             (line(il)%straypol_par(ia)%damp.gt.1e-5).or.&
             (line(il)%straypol_par(ia)%dopp.gt.1e-5)) then
           if (voigt.eq.1) then
              if (modeval.eq.1) then
                 straydamp=line(il)%straypol_par(ia)%damp
              else
                 if ((modeval.eq.2).or.(modeval.eq.3).or.(modeval.eq.4)) then
                    straydamp=line(il)%straypol_par(ia)%damp*line(il)%wl**2 / &
                         (4*3.1415927*2.99792512e8* &
                         line(il)%straypol_par(ia)%dopp)
                 end if
              end if
              call voigtfunc(line(il)%wl* &
                   (1.+line(il)%straypol_par(ia)%vlos/2.99792512e8), &
                   straydamp, &
                   line(il)%straypol_par(ia)%dopp, &
                   wl,nwl,hprof,fprof)
              gsub=hprof * FF(ia)*line(il)%par%straypol_eta0
           else
              call gaussfunc(line(il)%par%straypol_amp * FF(ia), &
                   line(il)%wl* &
                   (1.+line(il)%straypol_par(ia)%vlos/2.99792512e8), &
                   2*line(il)%straypol_par(ia)%width**2, &
                   wl,nwl,gsub,addnum)
              gsub=gsub-addnum
           end if
           !       gsub=gsub * line(il)%f
           if (line(il)%straypol_par(ia)%sign.eq.-1) gsub=-gsub
           !        if (abs(line(il)%straypol_par(ia)%slitori_solar-999).lt.1) then
           !correction along B (Hanle)
           qsub=+gsub*cos((atm(ia)%par%azi*2+hanle_azi)*0.01745329)
           usub=+gsub*sin((atm(ia)%par%azi*2+hanle_azi)*0.01745329)
           !        else !correction along limb ()
           !           qsub=+gsub*cos(2*obs_par%slit_orientation*0.01745329)
           !           usub=+gsub*sin(2*obs_par%slit_orientation*0.01745329)
           !        end if
           qprof=qprof+qsub
           uprof=uprof+usub
        end if

     end do !!atm

  end do !!line
  !compute

  !use old normalization (changes SGRAD
  !when changing number of lines!)
  if (old_norm.eq.1) then
     iprof=(1.+iprof)*line(1)%icont
     iprof=(iprof-line(1)%icont)/nline+line(1)%icont
     qprof=qprof/nline
     uprof=uprof/nline
     vprof=vprof/nline     
  else if (old_norm.eq.0) then
     iprof=(1.+iprof)*line(1)%icont
  else if (old_norm.eq.2) then
     iprof=iprof*line(1)%icont/nline
     qprof=qprof/nline
     uprof=uprof/nline
     vprof=vprof/nline     
  end if

!profiles normalized top I
  if (norm_stokes_val.eq.1) then
     qprof=qprof/(iprof/line(1)%icont)
     uprof=uprof/(iprof/line(1)%icont)
     vprof=vprof/(iprof/line(1)%icont)
  end if

  !straylight correction
  !(non polarized, non-dispersive)
  iprof=iprof*(1.-gen%par%straylight)+gen%par%straylight

  !fit continuum level + straylight reduction of Q,U,V
   iprof=iprof*gen%par%ccorr
   qprof=qprof*gen%par%ccorr!*(1.-gen%par%straylight)
   uprof=uprof*gen%par%ccorr!*(1.-gen%par%straylight)
   vprof=vprof*gen%par%ccorr!*(1.-gen%par%straylight)

   !calculate voigt profile for blend
  do ib=1,nblend
     if (blend(ib)%par%a0.ne.0) then
        call voigtfunc(blend(ib)%par%wl,blend(ib)%par%damp, &
             blend(ib)%par%width,wl,nwl,hprof,fprof)
        iprof=iprof*(1-blend(ib)%par%a0*hprof)
        qprof=qprof*(1-blend(ib)%par%a0*hprof)
        uprof=uprof*(1-blend(ib)%par%a0*hprof)
        vprof=vprof*(1-blend(ib)%par%a0*hprof)
     end if
  end do

  if (doconv.ne.0) then !perform convolution
     CALL RFFTI( int(nwl), WSAVE )
     convfft=convval
     CALL RFFTF( int(nwl), convfft, WSAVE )
     call convolve(int(nwl),iprof,convfft,wsave)
     call convolve(int(nwl),qprof,convfft,wsave)
     call convolve(int(nwl),uprof,convfft,wsave)
     call convolve(int(nwl),vprof,convfft,wsave)
     if (nret_wlidx.gt.0) then
        iprof(1:nret_wlidx)=iprof(ret_wlidx(1:nret_wlidx))
        qprof(1:nret_wlidx)=qprof(ret_wlidx(1:nret_wlidx))
        uprof(1:nret_wlidx)=uprof(ret_wlidx(1:nret_wlidx))
        vprof(1:nret_wlidx)=vprof(ret_wlidx(1:nret_wlidx))
     end if
   end if

END SUBROUTINE compute_profile
!!    ========================================================
