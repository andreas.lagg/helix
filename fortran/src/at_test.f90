module at_test
  implicit none


  public
  integer(2), parameter :: maxlin=6 !!Max number of lines
  !maxlin must be equal to maxatm
  integer(2), parameter :: maxatm=6 !!Max number of atmospheres
  integer(2), parameter :: maxspec=8 !!Max number of spectra in profile.dat
  integer(2), parameter :: maxwl=384 !!Max number of WL-points
  integer(2), parameter :: idlen=12  !!length of ID string
  integer(2), parameter :: maxmi=4   !!max. number of multi-iterations
  integer(2), parameter :: maxcmt=100 !!max. # of lines for comments
  integer(2), parameter :: maxipt=1000  !!Max # of lines in input file
  integer(2), parameter :: maxllen=160  !!Max len of input lines
  integer(2), parameter :: maxstr=160  !!Max len of strings
  integer(2), parameter :: maxlonstr=300  !!Max len of long strings
  integer(2), parameter :: npartot=15  !!total # of possible parameters 
  integer(2), parameter :: nlinpartot=2  !!total # of line parameters 
  integer(2), parameter :: maxplotprof=6  !!max number of plot profiles
  integer(2), parameter :: maxavg=512 !!Max number of profiles to average 
  !!                                    in one dimension
  integer(2), parameter :: maxpi=13,maxsigma=12 !!max. quantum numbers J
  !!                               defines max. size for zeeman pattern arrays
  !                                =2*minval([maxjl,maxju])+1, max # of pi-comp
  !                                =maxjl+maxju , max # of sigma-comp.

  !!dirtyp structure
  type dirtyp
     sequence
     character(LEN=maxllen) :: ps,profile,sav,atm,atm_suffix,wgt
  end type dirtyp


  !!     parameter set for one component: values
  !!     must be npartot values!!!
  type partyp
     sequence
     real(4) :: b=0
     real(4) :: azi=0
     real(4) :: inc=0
     real(4) :: vlos=0
     real(4) :: width=0
     real(4) :: damp=0
     real(4) :: dopp=0
     real(4) :: a0=0
     real(4) :: sgrad=0
     real(4) :: etazero=0
     real(4) :: gdamp=0
     real(4) :: vmici=0
     real(4) :: densp=0
     real(4) :: tempe=0
     real(4) :: ff=0    
  end type partyp

  type iptyp
     sequence
     character(idlen) :: ipt,prg
  end type iptyp

  type parnametyp
     sequence
     type(iptyp) :: b
     type(iptyp) :: azi
     type(iptyp) :: inc
     type(iptyp) :: vlos
     type(iptyp) :: width
     type(iptyp) :: damp
     type(iptyp) :: dopp
     type(iptyp) :: a0
     type(iptyp) :: sgrad
     type(iptyp) :: etazero
     type(iptyp) :: gdamp
     type(iptyp) :: vmici
     type(iptyp) :: densp
     type(iptyp) :: tempe
     type(iptyp) :: ff
  end type parnametyp

  type namtyp
     sequence
     character(idlen) :: b
     character(idlen) :: azi
     character(idlen) :: inc
     character(idlen) :: vlos
     character(idlen) :: width
     character(idlen) :: damp
     character(idlen) :: dopp
     character(idlen) :: a0
     character(idlen) :: sgrad
     character(idlen) :: etazero
     character(idlen) :: gdamp
     character(idlen) :: vmici
     character(idlen) :: densp
     character(idlen) :: tempe
     character(idlen) :: ff
  end type namtyp

  !!     parameter set for one component: fit< flag
  type fittyp
     sequence
     integer(2) :: b=0
     integer(2) :: azi=0
     integer(2) :: inc=0
     integer(2) :: vlos=0
     integer(2) :: width=0
     integer(2) :: damp=0
     integer(2) :: dopp=0
     integer(2) :: a0=0
     integer(2) :: sgrad=0
     integer(2) :: etazero=0
     integer(2) :: gdamp=0
     integer(2) :: vmici=0
     integer(2) :: densp=0
     integer(2) :: tempe=0
     integer(2) :: ff=0
  end type fittyp

  !!     min/max structure
  type mmtyp
     sequence
     real(4) :: min=0,max=0
  end type mmtyp

  !!     parameter set for scaling and default values
  type scltyp
     sequence
     type (mmtyp) :: b,azi,inc,vlos,width,damp,dopp,a0,sgrad,etazero, &
          gdamp,vmici,densp,tempe,ff
  end type scltyp

  !!			structure holding all atmospheric parameters
  type atmtyp
     sequence
     type (fittyp) :: fit
     type (fittyp) :: idx
     type (partyp) :: par
     integer(4) :: npar
     integer(4) :: linid
     integer(1) :: use_line(idlen,maxlin)=0
  end type atmtyp

end module at_test
