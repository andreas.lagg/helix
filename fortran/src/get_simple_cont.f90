subroutine get_simple_cont(prof,nwl,icont)
  use all_type
  real(4),dimension(maxwl) :: prof
  real(4) :: icont
  integer(2) :: nwl,bdry,bdr1,bdr2

  bdry=real(nwl,kind=4)*0.05
  if (bdry.lt.5) bdry=5
  bdr1=minval( (/bdry,nwl/)   )
  bdr2=maxval( (/nwl-bdry,1_2/) )

  icont=maxval(prof(bdr1:bdr2))

end subroutine get_simple_cont
