subroutine idl_prog
  use all_type
  implicit none

  EXTERNAL IDL_Init !$pragma C(IDL_Init)    
  EXTERNAL IDL_Cleanup !$pragma C(IDL_Cleanup)    
  EXTERNAL IDL_Execute !$pragma C(IDL_Execute)
  EXTERNAL IDL_ExecuteStr !$pragma C(IDL_ExecuteStr)
  EXTERNAL IDL_ImportNamedArray !$pragma C(IDL_ImportNamedArray)
  EXTERNAL IDL_FindNamedVariable !$pragma C( IDL_FindNamedVariable )

!   Define arguments for IDL_Init routine
  INTEGER*4 ARGC
  INTEGER*4 ARGV(1)
  DATA ARGC, ARGV(1) /2 * 0/
        INTEGER*4 ISTAT 

!   Initialize Callable IDL
  ISTAT = IDL_Init(%VAL(0), ARGC, ARGV(1))
  
  
end subroutine idl_prog
