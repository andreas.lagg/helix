subroutine read_ccx(file,nwl,nx,ny,wl_disp,wl_off,wl_vec,icont,xerr,verbose)
  use all_type
  use ipt_decl
  use tools
  implicit none
  real(4) :: icont(nx,ny)
#ifdef MPI
  include 'mpif.h'
#endif
  character(LEN=maxstr) :: file
  character(LEN=72) :: comment,nmode
  real(8) :: wl_disp,wl_off,wl_vec(maxwl)
  logical :: error,verbose,anynull
  integer(4) :: ccxunit,xstat,blocksize,xnaxes(2),nfound, &
       wstat,hdutype,wnaxes,nx,ny
  integer(2) :: nwl,j,xerr
  integer(4) :: status_slt,status_avg
  integer(4) :: fpix(2),lpix(2),inc(2)
  real(4) :: icntslt,icntavg
!  real(8) :: dicntslt,dicntavg
  real(4) :: icint(ny)

  error=.false.
  inc=1
  fpix=2

  !check if auxiliary file is present (ccx)
  xstat=0
  status_slt=0
  status_avg=0
  call ftgiou(ccxunit,xstat)  !open new unit
  call ftopen(ccxunit,trim(file),0,blocksize,xstat)
  if (xstat.eq.0) then
     if (verbose) then
        write(*,*) 'AUX (ccx) file: '//trim(file)
        write(*,*) 'Using continuum level calculation '//&
             'from AUX (ccx) file'
     end if
     call ftgknj(ccxunit,'NAXIS',1,2,xnaxes,nfound,xstat)
     if (nx.ge.2) then  !TIP
        fpix=(/1,1/)
        lpix=(/xnaxes(1),xnaxes(2)/)
        call ftgsve(ccxunit,1,2,xnaxes,fpix,lpix,inc,0,icont,anynull,xstat)
    else !HINODE
        lpix=(/ny,1/)
        fpix=(/1,1/)
!        call ftgsve(ccxunit,1,2,xnaxes,fpix,lpix,inc,0,icint,anynull,xstat)
        call ftgsve(ccxunit,1,1,xnaxes(1),fpix(1),lpix(1),inc(1),0, &
             icint,anynull,xstat)

        icont(1,1:ny)=icint(1:ny)
     end if

     call ftgkye(ccxunit,'ICNTSLT',icntslt,comment,status_slt)
     call ftgkye(ccxunit,'ICNTAVG',icntavg,comment,status_avg)
     select case (ipt%norm_cont)
     case(0)
        nmode='LOCAL'
     case(1)
        nmode='SLIT'
        if (status_slt.eq.0) then
           icont=icntslt
        else
           write(*,*) 'NORM_CONT SLIT not possible. No info in '//trim(file)
           call stop
        end if
     case(2)
        nmode='IMAGE'
        if (status_avg.eq.0) then
           icont=icntavg
        else
           write(*,*) 'NORM_CONT IMAGE not possible. No info in '//trim(file)
           call stop
        end if
     end select
     if (verbose) then
        write(*,*) 'Using NORM_CONT '//trim(nmode)
     end if
     !retrieve WL-cal
     call ftgkyd(ccxunit,'WL_DISP',wl_disp,comment,xstat)
     call ftgkyd(ccxunit,'WL_OFF',wl_off,comment,xstat)
     !read first extension to get WL-Vector
     wstat=0
     call ftmahd(ccxunit,2,hdutype,wstat)
     if (wstat.eq.0) then
        call ftgknj(ccxunit,'NAXIS',1,1,wnaxes,nfound,wstat)
        call ftgpvd(ccxunit,1,1,wnaxes,0,wl_vec,anynull,wstat)
        if (verbose) &
             write(*,*) 'Using '// &
             'WL-Vector defined in FITS Extension #1:'// &
             ' WL_NUM=',wnaxes
!        wl_off=0
!        wl_disp=0
     else
        do j=1,nwl
           wl_vec(j)=(j-1)*wl_disp+wl_off
        end do
        if (verbose) &
             write(*,*) 'Using '// &
             'WL_OFF',wl_off,' and WL_DISP',wl_disp
     end if
     xerr=0
  else
     if (verbose) then
        write(*,*) 'No Aux (ccx) file found. '// &
             'Using simple continuum level calculation'
     end if
     if (ipt%wl_off.lt.1e-5) then
        write(*,*) 'WL-Calibration must be specified'// &
             'in the input file!'
        write(*,*) 'Use keywords WL_OFF and WL_DISP'
        call stop
     end if
     xerr=1
  end if

  call ftclos(ccxunit, xstat)
  call ftfiou(ccxunit, xstat)

end subroutine read_ccx
