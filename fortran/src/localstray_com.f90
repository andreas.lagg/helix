!module to declare localstray (replacement for common block
module localstray_com
  use all_type
  implicit none
  save

  public
  type (proftyp) :: lsprof
  integer(2) :: dols,lspol

end module localstray_com
