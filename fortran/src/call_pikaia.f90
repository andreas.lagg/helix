!!    compile with:
!!    compute profile_
!!    equivalent to compute_profile.pro
!!    calculates profiule from given atmosphere atm defines in all_type
!!    .f90
!!    =========================================================  
#ifdef IDL
SUBROUTINE call_pikaia_pro(argc, argv) !Called by IDL

#ifdef bit64
  INTEGER(8) argc, argv(*)   !Argc and Argv are integers
#else
  INTEGER(4) argc, argv(*)   !Argc and Argv are integers
#endif

  j = LOC(argc)             !Obtains the number of arguments (argc)

  !Because argc is passed by VALUE.

  !!    Call subroutine compute_profile, converting the IDL parameters
  !!    to standard FORTRAN, passed by reference arguments:
  CALL call_pikaia(%VAL(argv(1)), %VAL(argv(2)), %VAL(argv(3)), &
       %VAL(argv(4)), %VAL(argv(5)), %VAL(argv(6)), %VAL(argv(7)), &
       %VAL(argv(8)), %VAL(argv(9)), %VAL(argv(10)), %VAL(argv(11)), &
       %VAL(argv(12)), %VAL(argv(13)), %VAL(argv(14)), %VAL(argv(15)), &
       %VAL(argv(16)), %VAL(argv(17)), %VAL(argv(18)), %VAL(argv(19)), &
       %VAL(argv(20)), %VAL(argv(21)), %VAL(argv(22)), %VAL(argv(23)), &
       %VAL(argv(24)), %VAL(argv(25)), %VAL(argv(26)), %VAL(argv(27)), &
       %VAL(argv(28)), %VAL(argv(29)), %VAL(argv(30)), %VAL(argv(31)), &
       %VAL(argv(32)), %VAL(argv(33)), %VAL(argv(34)), %VAL(argv(35)), &
       %VAL(argv(36)), %VAL(argv(37)), %VAL(argv(38)), %VAL(argv(39)), &
       %VAL(argv(40)), %VAL(argv(41)), %VAL(argv(42)), %VAL(argv(43)), &
       %VAL(argv(44)), %VAL(argv(45)), %VAL(argv(46)), %VAL(argv(47)), &
       %VAL(argv(48)), %VAL(argv(49)), %VAL(argv(50)), %VAL(argv(51)), &
       %VAL(argv(52)), %VAL(argv(53)), %VAL(argv(54)), %VAL(argv(55)), &
       %VAL(argv(56)), %VAL(argv(57)), %VAL(argv(58)), %VAL(argv(59)), &
       %VAL(argv(60)), %VAL(argv(61)), %VAL(argv(62)), %VAL(argv(63)), &
       %VAL(argv(64)), %VAL(argv(65)), %VAL(argv(66)), %VAL(argv(67)), &
       %VAL(argv(68)), %VAL(argv(69)), %VAL(argv(70)), %VAL(argv(71)))

END SUBROUTINE call_pikaia_pro
#endif
!!    ========================================================


!function to be minimized, used by uobyqua
!!    ========================================================
subroutine calfun(n,opar,fval)
  use all_type
  use pikvar
  use quvstrength
  use piktools
  implicit none
  integer :: n
  real(8) :: opar(n)
  real(8), INTENT (IN OUT) :: fval

  !minimize chisqr instead of maximizing 
  fval=real(1./func(n,real(opar,kind=4)),kind=8)

end subroutine calfun


!!    ========================================================
!function to be minimized, used by LMFIT
!!    ========================================================
subroutine lm_func(N,M,NP,NQ,LDN,LDM,LDNP,BETA,XPLUSD,IFIXB,IFIXX,LDIFX,&
     IDEVAL,F,FJACB,FJACD,ISTOP)
  use real_precision
  use all_type
  use localstray_com
  use pikvar
  use quvstrength
  use piktools
  implicit none
  !n is equal to compnwl in this routine
  real(4) :: par(np)
  real(4), dimension(compnwl) :: ifit,qfit,ufit,vfit
  real(4), dimension(n) :: diffvec
  real(4),dimension(maxwl) :: lsi,lsq,lsu,lsv

  INTEGER(4) :: IDEVAL,ISTOP,LDIFX,LDM,LDN,LDNP,M,N,NP,NQ
  REAL (KIND=R8) :: BETA(NP),F(LDN,NQ)
  REAL (KIND=R8) :: FJACB(LDN,LDNP,NQ), &
       FJACD(LDN,LDM,NQ),XPLUSD(LDN,M)
  !  REAL (KIND=R8) :: FJACB,FJACD,XPLUSD
  INTEGER(4) :: IFIXB(NP),IFIXX(LDIFX,M)


  ISTOP = 0
  !  xplusd=0
  !  ifixb=0
  !  ifixx=0
  !  fjacb=0
  !  fjacd=0
  !  istop=0
  !  write(*,*) 'HIER IDEVAL',ideval
  !write(*,*) 'F=',F
  !write(*,*) 'FJACB=',FJACB
  !write(*,*) 'FJACD=',FJACD
  !  ideval=0
  par=real(beta,kind=4)
  call struct2pikaia(par,np,1_4,0_4,0_4)

  call compute_profile(catm(1:cnatm),cnatm, &
       cline(1:cnline),cnline, &
                                !       xplusd(1:compnwl,1),compnwl, &
       compwl(1:compnwl),compnwl, &
       cblend(1:cnblend),cnblend,cgen, &
       lsi,lsq,lsu,lsv,dols,lspol, &
       cconvval(1:compnwl),doconv,cmode, &
       cprefilterval(1:compnwl),doprefilter, &
       cret_wlidx,cnret_wlidx, &
       cobs_par, &
       0,cvoigt,cmagopt,cuse_geff,cuse_pb,cpb_method, &
       cmodeval,ciprof_only,cuse(1:cnline,1:cnatm),&
       ifit,qfit,ufit,vfit,1_2,cold_norm,-1_2,chanle_azi,cnorm_stokes_val,&
       cold_voigt)

  diffvec(1:compnwl)= &
       ((ciwgt(1:compnwl)*((ifit(1:compnwl)- &
       ciobs(1:compnwl))/cline(1)%icont)**2.)/istr+&
       (cqwgt(1:compnwl)*((qfit(1:compnwl)-cqobs(1:compnwl)))**2.)/qstr + &
       (cuwgt(1:compnwl)*((ufit(1:compnwl)-cuobs(1:compnwl)))**2.)/ustr + &
       (cvwgt(1:compnwl)*((vfit(1:compnwl)-cvobs(1:compnwl)))**2.)/vstr)
  if (n.gt.compnwl) diffvec(compnwl+1:n)=0.

  !set function value to one, seems to better copnverge than setting it to zero.
  !(this is the profdiff value in the odr-call), or is 0 better?
  F(1:N,M)=(real(diffvec(1:N),kind=r8))! + 1_8 )

  !write(*,*) 1./sum(real(diffvec(1:N),4)/1e3),fitness(int(n,2),ifit,qfit,ufit,vfit,ciobs,cqobs,cuobs,cvobs,ciwgt,cqwgt,cuwgt,cvwgt,cline(1)%icont),cline(1)%icont
  !write(*,*) sum(diffvec(1:n)),beta(1),catm(1)%par%b,diffvec(50),cline(1)%icont
end subroutine lm_func

!!    ========================================================
subroutine value2scale(value,par,mm,reverse,noscale)
  use all_type
  implicit none
  type (mmtyp) :: mm
  real(4) :: value,par
  integer(4) :: reverse,noscale

  if (reverse.eq.1) then
     if (noscale.eq.0) then
        value = mm%min + par*(mm%max - mm%min)
     else
        value = par
     end if
  else
     if (noscale.eq.0) then
        par = (value - mm%min)/(mm%max - mm%min)
     else
        par = value
     end if
  end if
end subroutine value2scale
!!    ========================================================
subroutine dblvalue2scale(value,par,mm,reverse,noscale)
  use all_type
  implicit none
  type (mmtyp) :: mm
  real(8) :: value
  real(4) :: par
  integer(4) :: reverse,noscale

  if (reverse.eq.1) then
     if (noscale.eq.0) then
        value = real(mm%min + par*(mm%max - mm%min),kind=8)
     else
        value = par
     end if
  else
     if (noscale.eq.0) then
        par = (real(value,kind=4) - mm%min)/(mm%max - mm%min)
     else
        par = value
     end if
  end if
end subroutine dblvalue2scale
!!    ========================================================

!!    ========================================================
subroutine struct2pikaia(par,npar,reverse,noscale,retname)
  use all_type
  use pikvar
  use tools
  use ipt_decl
  implicit none
  integer(4) :: npar
  integer(4) :: reverse,noscale,retname
  real(4) :: par(npar)
  integer(2) :: ia,il,ib
  type(parnametyp) :: parname
  type(blendnametyp) :: blendname
  type(gennametyp) :: genname
  type(linenametyp) :: linename

  !  external norm_ff
  if (retname.eq.1) then 
     call def_parname(parname)
     call def_genname(genname)
     call def_blendname(blendname)
     call def_linename(linename)
  end if

  if (reverse.eq.0) then    !FF has to be one
     call norm_ff(catm%par%ff,catm%fit%ff,catm%linid,cnatm)
     !     if (abs(sum(catm(1:cnatm)%par%ff)-1).lt.1e-4) then
     !        write(*,*) 'Total FF > 1, scaling all FF to 1.'
     !        catm(1:cnatm)%par%ff= &
     !             catm(1:cnatm)%par%ff/sum(catm(1:cnatm)%par%ff)
     !     endif
  endif
  do ia=1,cnatm
     !Mag field
     if (catm(ia)%fit%b.ne.0.and.catm(ia)%idx%b.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%b)=parname%b%ipt
        call value2scale(catm(ia)%par%b,par(catm(ia)%idx%b), &
             cscale(ia)%b,reverse,noscale)
        catm(ia)%par%b=catm(ia)%par%b*catm(ia)%ratio%b
     end if
     !Azimuth
     if (catm(ia)%fit%azi.ne.0.and.catm(ia)%idx%azi.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%azi)=parname%azi%ipt
        call value2scale(catm(ia)%par%azi,par(catm(ia)%idx%azi), &
             cscale(ia)%azi,reverse,noscale)
        catm(ia)%par%azi=catm(ia)%par%azi*catm(ia)%ratio%azi
     end if
     !Inclination
     if (catm(ia)%fit%inc.ne.0.and.catm(ia)%idx%inc.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%inc)=parname%inc%ipt
        call value2scale(catm(ia)%par%inc,par(catm(ia)%idx%inc), &
             cscale(ia)%inc,reverse,noscale)
        catm(ia)%par%inc=catm(ia)%par%inc*catm(ia)%ratio%inc
     end if
     !LOS velocity
     if (catm(ia)%fit%vlos.ne.0.and.catm(ia)%idx%vlos.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%vlos)=parname%vlos%ipt
        call value2scale(catm(ia)%par%vlos,par(catm(ia)%idx%vlos), &
             cscale(ia)%vlos,reverse,noscale)
        catm(ia)%par%vlos=catm(ia)%par%vlos*catm(ia)%ratio%vlos
     end if
     !Width of line
     if (catm(ia)%fit%width.ne.0.and.catm(ia)%idx%width.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%width)=parname%width%ipt
        call value2scale(catm(ia)%par%width,par(catm(ia)%idx%width), &
             cscale(ia)%width,reverse,noscale)
        catm(ia)%par%width=catm(ia)%par%width*catm(ia)%ratio%width
     end if
     !amplitude of line
     if (catm(ia)%fit%a0.ne.0.and.catm(ia)%idx%a0.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%a0)=parname%a0%ipt
        call value2scale(catm(ia)%par%a0,par(catm(ia)%idx%a0), &
             cscale(ia)%a0,reverse,noscale)
        catm(ia)%par%a0=catm(ia)%par%a0*catm(ia)%ratio%a0
     end if
     !voigt damping
     if (catm(ia)%fit%damp.ne.0.and.catm(ia)%idx%damp.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%damp)=parname%damp%ipt
        call value2scale(catm(ia)%par%damp,par(catm(ia)%idx%damp), &
             cscale(ia)%damp,reverse,noscale)
        catm(ia)%par%damp=catm(ia)%par%damp*catm(ia)%ratio%damp
     end if
     !voigt doppler broadening
     if (catm(ia)%fit%dopp.ne.0.and.catm(ia)%idx%dopp.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%dopp)=parname%dopp%ipt
        call value2scale(catm(ia)%par%dopp,par(catm(ia)%idx%dopp), &
             cscale(ia)%dopp,reverse,noscale)
        catm(ia)%par%dopp=catm(ia)%par%dopp*catm(ia)%ratio%dopp
     end if
     !source function gradient B1
     if (catm(ia)%fit%sgrad.ne.0.and.catm(ia)%idx%sgrad.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%sgrad)=parname%sgrad%ipt
        call value2scale(catm(ia)%par%sgrad,par(catm(ia)%idx%sgrad), &
             cscale(ia)%sgrad,reverse,noscale)
        catm(ia)%par%sgrad=catm(ia)%par%sgrad*catm(ia)%ratio%sgrad
     end if     !source function B0
     if (catm(ia)%fit%szero.ne.0.and.catm(ia)%idx%szero.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%szero)=parname%szero%ipt
        call value2scale(catm(ia)%par%szero,par(catm(ia)%idx%szero), &
             cscale(ia)%szero,reverse,noscale)
        catm(ia)%par%szero=catm(ia)%par%szero*catm(ia)%ratio%szero
     end if
     !opacity ratio at line center
     if (catm(ia)%fit%etazero.ne.0.and.catm(ia)%idx%etazero.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%etazero)=parname%etazero%ipt
        call value2scale(catm(ia)%par%etazero,par(catm(ia)%idx%etazero), &
             cscale(ia)%etazero,reverse,noscale)
        catm(ia)%par%etazero=catm(ia)%par%etazero*catm(ia)%ratio%etazero
     end if
     !damping constant
     if (catm(ia)%fit%gdamp.ne.0.and.catm(ia)%idx%gdamp.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%gdamp)=parname%gdamp%ipt
        call value2scale(catm(ia)%par%gdamp,par(catm(ia)%idx%gdamp), &
             cscale(ia)%gdamp,reverse,noscale)
        catm(ia)%par%gdamp=catm(ia)%par%gdamp*catm(ia)%ratio%gdamp
     end if
     !micro turbulence velocity
     if (catm(ia)%fit%vmici.ne.0.and.catm(ia)%idx%vmici.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%vmici)=parname%vmici%ipt
        call value2scale(catm(ia)%par%vmici,par(catm(ia)%idx%vmici), &
             cscale(ia)%vmici,reverse,noscale)
        catm(ia)%par%vmici=catm(ia)%par%vmici*catm(ia)%ratio%vmici
     end if
     !density parameter
     if (catm(ia)%fit%densp.ne.0.and.catm(ia)%idx%densp.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%densp)=parname%densp%ipt
        call value2scale(catm(ia)%par%densp,par(catm(ia)%idx%densp), &
             cscale(ia)%densp,reverse,noscale)
        catm(ia)%par%densp=catm(ia)%par%densp*catm(ia)%ratio%densp
     end if
     !temperature
     if (catm(ia)%fit%tempe.ne.0.and.catm(ia)%idx%tempe.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%tempe)=parname%tempe%ipt
        call value2scale(catm(ia)%par%tempe,par(catm(ia)%idx%tempe), &
             cscale(ia)%tempe,reverse,noscale)
        catm(ia)%par%tempe=catm(ia)%par%tempe*catm(ia)%ratio%tempe
     end if
     !slab thickness
     if (catm(ia)%fit%dslab.ne.0.and.catm(ia)%idx%dslab.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%dslab)=parname%dslab%ipt
        call value2scale(catm(ia)%par%dslab,par(catm(ia)%idx%dslab), &
             cscale(ia)%dslab,reverse,noscale)
        catm(ia)%par%dslab=catm(ia)%par%dslab*catm(ia)%ratio%dslab
     end if
     !height of slab
     if (catm(ia)%fit%height.ne.0.and.catm(ia)%idx%height.ne.0) then
        if (retname.eq.1) fitparname(catm(ia)%idx%height)=parname%height%ipt
        call value2scale(catm(ia)%par%height,par(catm(ia)%idx%height), &
             cscale(ia)%height,reverse,noscale)
        catm(ia)%par%height=catm(ia)%par%height*catm(ia)%ratio%height
     end if
     !filling factor
     if (catm(ia)%fit%ff.ne.0.and.catm(ia)%idx%ff.ne.0) then
        if (par(catm(ia)%idx%ff).ge.1) par(catm(ia)%idx%ff)=1.
        if (retname.eq.1) fitparname(catm(ia)%idx%ff)=parname%ff%ipt
        call value2scale(catm(ia)%par%ff,par(catm(ia)%idx%ff), &
             cscale(ia)%ff,reverse,noscale)
        if (par(catm(ia)%idx%ff).ge.1) par(catm(ia)%idx%ff)=1.
        catm(ia)%par%ff=catm(ia)%par%ff*catm(ia)%ratio%ff
     end if
  end do

  !! LINE PARAMETERS
  do il=1,cnline
     !straypolarization amplitude
     if (cline(il)%fit%straypol_amp.ne.0.and.&
          &cline(il)%idx%straypol_amp.ne.0) then
        if (retname.eq.1) fitparname(cline(il)%idx%straypol_amp)=linename%straypol_amp%ipt
        call value2scale(cline(il)%par%straypol_amp, &
             par(cline(il)%idx%straypol_amp), &
             cline(il)%scale%straypol_amp,reverse,noscale)
     end if
     if (cline(il)%fit%straypol_eta0.ne.0.and.&
          &cline(il)%idx%straypol_eta0.ne.0) then
        if (retname.eq.1) fitparname(cline(il)%idx%straypol_eta0)=linename%straypol_eta0%ipt
        call value2scale(cline(il)%par%straypol_eta0, &
             par(cline(il)%idx%straypol_eta0), &
             cline(il)%scale%straypol_eta0,reverse,noscale)
     end if
     if (cline(il)%fit%strength.ne.0.and.&
          &cline(il)%idx%strength.ne.0) then
        if (retname.eq.1) fitparname(cline(il)%idx%strength)=linename%strength%ipt
        call value2scale(cline(il)%par%strength, &
             par(cline(il)%idx%strength), &
             cline(il)%scale%strength,reverse,noscale)
     end if
     if (cline(il)%fit%wlshift.ne.0.and.&
          &cline(il)%idx%wlshift.ne.0) then
        if (retname.eq.1) fitparname(cline(il)%idx%wlshift)=linename%wlshift%ipt
        call value2scale(cline(il)%par%wlshift, &
             par(cline(il)%idx%wlshift), &
             cline(il)%scale%wlshift,reverse,noscale)
     end if
  end do

  do ib=1,cnblend 
     if (cblend(ib)%fit%wl.ne.0.and.cblend(ib)%idx%wl.ne.0) then
        if (retname.eq.1) fitparname(cblend(ib)%idx%wl)=blendname%wl%ipt
        call dblvalue2scale(cblend(ib)%par%wl,par(cblend(ib)%idx%wl), &
             cblend(ib)%scale%wl,reverse,noscale)
     end if
     if (cblend(ib)%fit%a0.ne.0.and.cblend(ib)%idx%a0.ne.0) then
        if (retname.eq.1) fitparname(cblend(ib)%idx%a0)=blendname%a0%ipt
        call value2scale(cblend(ib)%par%a0,par(cblend(ib)%idx%a0), &
             cblend(ib)%scale%a0,reverse,noscale)
     end if
     if (cblend(ib)%fit%width.ne.0.and.cblend(ib)%idx%width.ne.0) then
        if (retname.eq.1) fitparname(cblend(ib)%idx%width)=blendname%width%ipt
        call value2scale(cblend(ib)%par%width,par(cblend(ib)%idx%width), &
             cblend(ib)%scale%width,reverse,noscale)
     end if
     if (cblend(ib)%fit%damp.ne.0.and.cblend(ib)%idx%damp.ne.0) then
        if (retname.eq.1) fitparname(cblend(ib)%idx%damp)=blendname%damp%ipt
        call value2scale(cblend(ib)%par%damp,par(cblend(ib)%idx%damp), &
             cblend(ib)%scale%damp,reverse,noscale)
     end if
  end do
  if (cgen%fit%ccorr.ne.0.and.cgen%idx%ccorr.ne.0) then
     if (retname.eq.1) fitparname(cgen%idx%ccorr)=genname%ccorr%ipt
     call value2scale(cgen%par%ccorr,par(cgen%idx%ccorr), &
          cgen%scale%ccorr,reverse,noscale)
  end if
  if (cgen%fit%straylight.ne.0.and.cgen%idx%straylight.ne.0) then
     if (retname.eq.1) fitparname(cgen%idx%straylight)=genname%straylight%ipt
     call value2scale(cgen%par%straylight,par(cgen%idx%straylight), &
          cgen%scale%straylight,reverse,noscale)
  end if
  if (cgen%fit%radcorrsolar.ne.0.and.cgen%idx%radcorrsolar.ne.0) then
     if (retname.eq.1) fitparname(cgen%idx%radcorrsolar)=genname%radcorrsolar%ipt
     call value2scale(cgen%par%radcorrsolar,par(cgen%idx%radcorrsolar), &
          cgen%scale%radcorrsolar,reverse,noscale)
  end if

  if (reverse.eq.1) then    !FF has to be one
     call norm_ff(catm%par%ff,catm%fit%ff,catm%linid,cnatm)
     !     if (cnatm.gt.1) then
     !        catm(cnatm)%par%ff=1.-sum(catm(1:cnatm-1)%par%ff)
     !        if (catm(cnatm)%par%ff.lt.cscale(cnatm)%ff%min) then
     !           catm(cnatm)%par%ff=cscale(cnatm)%ff%min
     !        endif
     !     end if
     !     if (abs(sum(catm(1:cnatm)%par%ff)-1).gt.1e-4) then
     !        catm(1:cnatm)%par%ff= &
     !             catm(1:cnatm)%par%ff/sum(catm(1:cnatm)%par%ff)
     !     endif
  endif

end subroutine struct2pikaia
!!    ========================================================

!!    This subroutine is called by compute_profile_pro and has no
!!    IDL specific code. Here the computation of the line is done.
!!    

!!    =========================================================
SUBROUTINE call_pikaia(line,nline,atm,natm, &
     wl,nwl,blend,nblend,gen, &
     lspi,lspq,lspu,lspv,dolsp,lspolp, &
     wlrg,conv_func,conv_nwl,conv_mode,conv_wljitter,&
     prefilter_func,prefilter_wlerr, &
     obs_par,iobs,qobs,uobs,vobs,iwgt,qwgt,uwgt,vwgt,scl, &
     voigt,magopt,old_norm,use_geff,use_pb,pb_method,modeval, &
     iprof_only,verbose, &
     method,ncalls,pikaia_pop,luse, &
     atmout,blendout,genout,fitval,seed,ifit,qfit,ufit,vfit,wlfit,nwlfit, &
     iwl_compare,nwl_compare,conv_output,hanle_azi,norm_stokes_val,&
     nfreepar,chi2modein,old_voigt, &
     statdim,statfit,statpar,statifit,statqfit,statufit,statvfit)
  use all_type
  use Powell_Optimize
  use quvstrength
  use pikvar
  use piktools
  use localstray_com
  use localstraycomp_com
  use odrpack95
  USE REAL_PRECISION
  use ipt_decl
  use tools
  implicit none
#ifdef PIKMPI
  include 'mpif.h'
  integer ierr,myid,nproc,nslaves
  integer trial,slave,msgtype
#endif
  integer(2) :: nwl,natm,nline,nblend,ncalls,pikaia_pop,method,ntotal,nfreepar,chi2modein
  integer(2) :: voigt,magopt,old_norm,use_geff,use_pb,pb_method,modeval, &
       iprof_only,verbose,conv_nwl,nwlfit,nwl_compare,iwl_compare(maxwl), &
       conv_output,rnwl,rwl(maxwl),old_voigt,conv_mode
  integer(2) :: norm_stokes_val,dolsp,lspolp,nptot,statdim(2)
  character(LEN=maxstr) :: conv_func,prefilter_func
  real(8) :: wl(nwl),wlrg(2),wlfit(maxwl)
  real(4) :: fitval,hanle_azi,conv_wljitter,prefilter_wlerr
  real(4), dimension(nwl) :: iobs,qobs,uobs,vobs
  real(4), dimension(nwl) :: iwgt,qwgt,uwgt,vwgt
  real(4), dimension(maxwl) :: ifit,qfit,ufit,vfit,lspi,lspq,lspu,lspv
  real(4), dimension(maxwl,pik_PMAX) :: statifit,statqfit,statufit,statvfit
  logical, dimension(maxwl) :: inan,qnan,unan,vnan
  integer(2) :: luse(nline,natm)
  type (atmtyp) :: atm(natm),atmout(natm),atmfinal(maxatm),atm2(maxatm)
  type (blendtyp) :: blend(nblend),blendout(nblend),blendfinal(maxblend),blend2(maxblend)
  type (gentyp) :: gen,genout,genfinal,gen2
  type (linetyp) :: line(nline),linefinal(maxlin),line2(maxlin)
  type (obstyp) :: obs_par
  type (scltyp) :: scl(natm)
  integer(2) :: ia,i1,i2,iwmin,iwmax,nodr,is
  !    real(4),allocatable :: par(:)
  real(4) :: statpar(pik_NMAX,pik_PMAX),statfit(pik_PMAX),ptmp(pik_NMAX), par(pik_NMAX),parfit(pik_NMAX)
  REAL (KIND=R8),allocatable :: lm_lower(:),lm_upper(:)
  integer(2) :: icnt=0,qcnt=0,ucnt=0,vcnt=0
  type(convtyp) :: conv
  type(prefiltertyp) :: prefilter

  !pikaia variables (from xpikaia subroutine)
  integer seed, i, status, n
  real ctrl(12), f
  !  real func
  !  external func

  !variables for common block to cimmunicate
  !with minimization function
  real(4) :: tcpu(2)

  INTEGER, PARAMETER  :: dp = SELECTED_REAL_KIND(12, 60)
  real(dp)  :: rhobeg,rhoend
  real(dp),allocatable :: opar(:)
  integer(4)   :: iprint,maxfun,no!,ifitmax(1)

  !variables and functions for odrpack95
  real(kind=r8),allocatable :: profdiff(:,:),lm_wl(:,:)
  integer(4) :: nodr4,m4,no4,nq4,ncalls4
  external lm_func

  call cpu_time(tcpu(1))

  istr=1. ; qstr=1. ; ustr=1. ; vstr=1.
  icnt=0  ; qcnt=0  ; ucnt=0  ; vcnt=0

  !fill lsprof variable
  cdols=dolsp
  dols=dolsp
  clspol=lspolp
  lspol=lspolp
  if (dolsp.eq.1) then
     lsprof%i(1:nwl)=lspi(1:nwl)
     lsprof%q(1:nwl)=lspq(1:nwl)
     lsprof%u(1:nwl)=lspu(1:nwl)
     lsprof%v(1:nwl)=lspv(1:nwl)
     lsprof%wl(1:nwl)=wl(1:nwl)
     lsprof%nwl=nwl
  end if

  inan=check_nan(iobs)
  qnan=check_nan(qobs)
  unan=check_nan(uobs)
  vnan=check_nan(vobs)
  do i=1,nwl
     if ((iwgt(i).gt.1e-4).and.(.not.inan(i))) then
        if (line(1)%icont.le.1e-5) line(1)%icont=1.
        istr=istr+abs((iobs(i)/line(1)%icont)-1)
        icnt=icnt+1
     end if
     if ((qwgt(i).gt.1e-4).and.(.not.qnan(i))) then
        qstr=qstr+abs(qobs(i))
        qcnt=qcnt+1
     end if
     if ((uwgt(i).gt.1e-4).and.(.not.unan(i))) then
        ustr=ustr+abs(uobs(i))
        ucnt=ucnt+1
     end if
     if ((vwgt(i).gt.1e-4).and.(.not.vnan(i))) then
        vstr=vstr+abs(vobs(i))
        vcnt=vcnt+1
     end if
  end do

  if (icnt.gt.0) istr=(istr-1.)/icnt
  if (qcnt.gt.0) qstr=(qstr-1.)/qcnt
  if (ucnt.gt.0) ustr=(ustr-1.)/ucnt
  if (vcnt.gt.0) vstr=(vstr-1.)/vcnt
  if (istr.lt.1e-5) istr=1e-5
  if (qstr.lt.1e-5) qstr=1e-5
  if (ustr.lt.1e-5) ustr=1e-5
  if (vstr.lt.1e-5) vstr=1e-5

  quv_strength = sum(sqrt(1e-8+qobs**2.+vobs**2.+uobs**2.))
  if (quv_strength.lt.1e-5) quv_strength=1.

  !read convolutipon function (needs WL-vector from observation)
  call read_conv(wlrg,conv_func,conv_nwl,conv_mode,conv_wljitter,&
       prefilter_func,wl,nwl,conv,verbose) 

  !restrict fitted profile to the range where weight is not 0
  iwmin=0
  iwmax=nwl+1
  if (conv%doconv.eq.0) then
     do i1=1,nwl
        if (iwmin.eq.0) then
           if ((iwgt(i1).gt.1e-5).or.(qwgt(i1).gt.1e-5).or.&
                (uwgt(i1).gt.1e-5).or.(vwgt(i1).gt.1e-5)) iwmin=i1
        end if
        i2=nwl-i1+1
        if (iwmax.eq.nwl+1) then
           if ((iwgt(i2).gt.1e-5).or.(qwgt(i2).gt.1e-5).or.&
                (uwgt(i2).gt.1e-5).or.(vwgt(i2).gt.1e-5)) iwmax=i2
        end if
     end do
  end if
  if (iwmin.eq.0) iwmin=1
  if (iwmax.eq.nwl+1) iwmax=nwl

  !fill in ipt_decl common block (needed if read_ipt is not used, i.e. when called from IDL)
  act_ncomp=natm
  act_nline=nline
  act_nblend=nblend

  !copy values into common block vars to be
  !used in func.f
  cnatm=natm
  cnblend=nblend
  cnwl=iwmax-iwmin+1
  cnline=nline
  cvoigt=voigt
  cold_norm=old_norm
  cold_voigt=old_voigt
  cmagopt=magopt
  chanle_azi=hanle_azi
  cnorm_stokes_val=norm_stokes_val
  cuse_geff=use_geff
  cuse_pb=use_pb
  cpb_method=pb_method
  cmodeval=modeval
  ciprof_only=iprof_only
  cscale(1:natm)=scl(1:natm)
  cwl(1:cnwl)=wl(iwmin:iwmax)        !copy only values for weight != 0
  ciobs(1:cnwl)=iobs(iwmin:iwmax)
  cqobs(1:cnwl)=qobs(iwmin:iwmax)
  cuobs(1:cnwl)=uobs(iwmin:iwmax)
  cvobs(1:cnwl)=vobs(iwmin:iwmax)
  ciwgt(1:cnwl)=iwgt(iwmin:iwmax)
  cqwgt(1:cnwl)=qwgt(iwmin:iwmax)
  cuwgt(1:cnwl)=uwgt(iwmin:iwmax)
  cvwgt(1:cnwl)=vwgt(iwmin:iwmax)

  call fill_localstray(lsi,lsq,lsu,lsv,dols,cwl(1:cnwl),cnwl)

  doconv=conv%doconv
  cmode=conv%mode
  if (conv%doconv.ne.0) then
     compnwl=conv%nwl
     compwl(1:compnwl)=conv%wl(1:compnwl)
     cconvval(1:compnwl)=conv%val(1:compnwl)
     cret_wlidx=conv%iwl_compare
     cnret_wlidx=conv%nwl_compare
     prefilter%val(1:cnret_wlidx)= prefilter%val(cret_wlidx(1:cnret_wlidx))     
  else
     cconvval(1)=0
     cret_wlidx=conv%iwl_compare*0
     cnret_wlidx=-1
     compnwl=cnwl
     compwl(1:compnwl)=cwl(1:compnwl)
  end if
  call read_prefilter(prefilter_func,prefilter_wlerr,&
       compwl,compnwl,prefilter,verbose)

  cprefilterval=prefilter%val
  doprefilter=prefilter%doprefilter

  cline(1:nline)=line(1:nline)
  catm(1:natm)=atm(1:natm)
  cblend(1:cnblend)=blend(1:nblend)
  cgen=gen
  cuse(1:cnline,1:cnatm)=luse(1:cnline,1:cnatm)
  cobs_par=obs_par

  atmfinal(1:cnatm)=catm
  linefinal(1:cnline)=cline
  blendfinal(1:cnblend)=cblend
  genfinal=cgen

  !allocate
  nptot=atm(1)%npar+line(1)%npar+blend(1)%npar+gen%npar
  !    allocate(par(nptot))
  allocate(opar(nptot))
  allocate(lm_lower(nptot))
  allocate(lm_upper(nptot))
  statdim=0
  statfit=0
  statpar=0

  if (ncalls.gt.0) then

     par=0
     call struct2pikaia(par(1:nptot),int(nptot,4),0_4,0_4,0_4)
     !check scaling of parameters
     if (((minval(par(1:nptot)).lt.0).or.(maxval(par(1:nptot)).gt.1)).and.(verbose.ge.1)) then
        write(*,*) 'Initial Value of one or more parameters is out of range.'
        write(*,*) 'Check your initial values and the SCL_MIN/SCL_MAX'//&
             ' values in the input file.'
     end if

     ntotal=0
     do i=1,cnwl
        if (ciwgt(i).ge.1e-5) ntotal=ntotal+1
        if (cqwgt(i).ge.1e-5) ntotal=ntotal+1
        if (cuwgt(i).ge.1e-5) ntotal=ntotal+1
        if (cvwgt(i).ge.1e-5) ntotal=ntotal+1
     end do

     nfree=ntotal-(atm(1)%npar+line(1)%npar+blend(1)%npar+gen%npar)
     if (nfree.le.1) nfree=1
     nfreepar=nfree
     chi2mode=chi2modein


     select case (method)

     case (int(0,2)) !PIKAIA algorithm
        if (verbose.ge.2) write(*,*) 'Use PIKAIA Minimization'


        !!    First, initialize the random-number generator
        !!    
        !!    write(*,'(/A$)') ' Random number seed (I(4))? '
        !!    read(*,*) seed
        !!    seed=1234
        call rninit(seed)

        !!    
        !!    Set control variables (use defaults)
        do i=1,12
           ctrl(i) = -1
        end do
        n=nptot
        !set some values optimized for the
        !he-line problem
        ctrl(1)=pikaia_pop
        ctrl(2)=ncalls
        !significant bits (6 is default and should 
        !be sufficient for real precission
        ctrl(3)=5 !6
        !ctrl(3)=6
        !        ctrl(4)=0.85
        ctrl(4)=0.85 !0.60
        ctrl(5)=5 !6
        ctrl(6)=0.01 !0.005
        ctrl(7)=0.001 !0.0005
        ctrl(8)=0.05 !0.01
        !                ctrl(6)=0.02
        !                ctrl(7)=0.002
        !                ctrl(8)=0.025
        !        ctrl(11)=1
        ctrl(12)=verbose
        !write(*,*) 'HIER_CP: ',ctrl(1:8)
        !!    Now call pikaia
        !!        cntff1=0 ; cntff2=0 ; cntff3=0 ; cntff4=0
#ifdef PIKMPI
        call mpi_comm_size( MPI_COMM_WORLD, nproc, ierr )
        nslaves=nproc-1
        call pikaia(userff,n,ctrl,par(1:nptot),statpar,f,status)
        trial = -1
        msgtype = 1
        do slave=1,nslaves
           call mpi_send( trial, 1, MPI_INTEGER, slave, &
                msgtype, MPI_COMM_WORLD, ierr )
        enddo
#else
        call pikaia(func,n,ctrl,par(1:nptot),statpar,f,status)
#endif
        !!        write(*,*) cntff1,cntff2,cntff3,cntff4
        !!    
        !!    Print the results
        if (verbose.gt.2) then
           write(*,*) ' status: ',status
           write(*,*) '    par: ',par(1:nptot)
           write(*,20) ctrl
20         format(   '    ctrl: ',6f9.5/10x,6f9.5)
        end if
        if (verbose.gt.1)  write(*,*) 'Fitness: ',f
        parfit=par
        call struct2pikaia(par(1:nptot),int(nptot,4),1_4,0_4,0_4)
        atmfinal(1:cnatm)=catm
        linefinal(1:cnline)=cline
        blendfinal(1:cnblend)=cblend
        genfinal=cgen

        do i=1,int(ctrl(1))
           call get_pikaia_idx(atmfinal,linefinal,blendfinal,genfinal,&
                atm2,line2,blend2,gen2,0_2)
           catm=atm2
           cline=line2
           cgen=gen2
           cblend=blend2
           nptot=catm(1)%npar+cline(1)%npar+cblend(1)%npar+cgen%npar
           ptmp(1:nptot)=statpar(1:nptot,i)
           no=int(nptot,4)
           statfit(i)=func(no,ptmp(1:nptot))
           call struct2pikaia(ptmp(1:nptot),int(nptot,4),1_4,0_4,0_4)
           catm(1:cnatm)%fit=catm(1:cnatm)%fitflag
           call get_pikaia_idx(catm,cline,cblend,cgen,&
                atm2,line2,blend2,gen2,0_2)
           catm=atm2
           cline=line2
           cgen=gen2
           cblend=blend2
           nptot=catm(1)%npar+cline(1)%npar+cblend(1)%npar+cgen%npar
           call struct2pikaia(ptmp(1:nptot),int(nptot,4),0_4,1_4,0_4)
           statpar(1:nptot,i)=ptmp(1:nptot)
        enddo

     case (int(1,2)) !POWELL Code: method flag to 1
        if (verbose.ge.2) write(*,*) 'Use POWELL Minimization'
        rhobeg=1.
        rhoend=1e-8
        maxfun=ncalls
        iprint=verbose
        opar=real(par(1:nptot),8)
        no=int(nptot,2)
        call uobyqa(no,opar,rhobeg,rhoend,iprint,maxfun)
        par(1:nptot)=real(opar,4)
        f=func(no,par(1:nptot))

        call struct2pikaia(par(1:nptot),int(nptot,4),1_4,0_4,0_4)

        !scale par: abs(b), angle range
        do ia=1,cnatm
           if (catm(ia)%par%b.lt.0) then
              catm(ia)%par%b=-catm(ia)%par%b
              catm(ia)%par%inc=180.+catm(ia)%par%inc
           end if
           catm(ia)%par%width=abs(catm(ia)%par%width)
           catm(ia)%par%inc= abs(mod(catm(ia)%par%inc,360.))
           if (catm(ia)%par%inc.gt.180) catm(ia)%par%inc=360.-catm(ia)%par%inc
           catm(ia)%par%azi= mod(catm(ia)%par%azi,180.)
           if (catm(ia)%par%azi.gt.90)  catm(ia)%par%azi=catm(ia)%par%azi-180
           if (catm(ia)%par%azi.lt.-90) catm(ia)%par%azi=catm(ia)%par%azi+180
        end do
        atmfinal(1:cnatm)=catm
        linefinal(1:cnline)=cline
        blendfinal(1:cnblend)=cblend
        genfinal=cgen

     case (int(2,2)) !ODRPACK levenberg-marquardt code
        if (verbose.ge.2) write(*,*) 'Use LMDIF Minimization (ODRPACK95)'

        opar=real(par(1:nptot),8)

        !of parameters must be smaller than # of WL points.
        !since lmdiff only knows about the number of wl-points
        !we have to more points (in fact we have 4 stokes vectors ->4 x nwl)
        nodr=compnwl
        !          if (nptot.gt.compnwl) nodr=nptot


        allocate(profdiff(nodr,1))
        allocate(lm_wl(nodr,1))


        lm_lower=0
        lm_upper=1
        profdiff=0
        lm_wl=0
        lm_wl(1:compnwl,1)=cwl(1:compnwl)

        select case (verbose)
        case(0)
           iprint=0
        case(1)
           iprint=1
        case default
           iprint=2111
        end select

        CALL ODR(FCN=lm_func,N=int(nodr,4),M=1_4,NP=int(nptot,4),&
             NQ=1_4,BETA=opar,iprint=iprint, &
             x=lm_wl, &
             y=profdiff,&
             LOWER=lm_lower,UPPER=lm_upper,&
             MAXIT=int(ncalls,4), &
             SSTOL=1d-10,PARTOL=1d-10,JOB=00111_4)

        par(1:nptot)=real(opar,4)
        f=func(no,par(1:nptot))

        call struct2pikaia(par(1:nptot),int(nptot,4),1_4,0_4,0_4)

        !scale par: abs(b), angle range
        do ia=1,cnatm
           if (catm(ia)%par%b.lt.0) then
              catm(ia)%par%b=-catm(ia)%par%b
              catm(ia)%par%inc=180.+catm(ia)%par%inc
           end if
           catm(ia)%par%width=abs(catm(ia)%par%width)
           catm(ia)%par%inc= abs(mod(catm(ia)%par%inc,360.))
           if (catm(ia)%par%inc.gt.180) catm(ia)%par%inc=360.-catm(ia)%par%inc
           catm(ia)%par%azi= mod(catm(ia)%par%azi,180.)
           if (catm(ia)%par%azi.gt.90)  catm(ia)%par%azi=catm(ia)%par%azi-180
           if (catm(ia)%par%azi.lt.-90) catm(ia)%par%azi=catm(ia)%par%azi+180
        end do

        atmfinal(1:cnatm)=catm
        linefinal(1:cnline)=cline
        blendfinal(1:cnblend)=cblend
        genfinal=cgen

        deallocate(profdiff)
        deallocate(lm_wl)


     end select

  else
     f=1.
  end if

  call norm_ff(catm%par%ff,catm%fit%ff,catm%linid,cnatm)

  if (conv%doconv.eq.0) then
     compwl(1:nwl)=wl(1:nwl)
     compnwl=nwl
  end if


  !prefilter_wlerr set to 0.
  call read_prefilter(prefilter_func,0.,compwl,compnwl,prefilter,0_2)
  !calculate profile for full WL-range, return only compare-WL
  !  call fill_localstray(lsi,lsq,lsu,lsv,dols,cwl(1:cnwl),cnwl) !compwl(1:compnwl),compnwl)
  call fill_localstray(lsi,lsq,lsu,lsv,dols,wl(1:nwl),nwl) !compwl(1:compnwl),compnwl)

  rnwl=conv%nwl_compare
  rwl=conv%iwl_compare
  if (conv_output.eq.1) then
     if (dols.eq.0) then
        rnwl=0
        rwl=-1
        if (verbose.ge.2) &
             write(*,*) &
             'Fitted profile is written out with CONV_NWL wavelength pixels'
     end if
  end if

  statdim(1)=nptot
  statdim(2)=ctrl(1)
  do is=1,int(ctrl(1),2)
     call struct2pikaia(statpar(1:nptot,is),int(nptot,4),1_4,1_4,0_4)
     call compute_profile(catm(1:cnatm),cnatm, &
          cline(1:cnline),cnline, &
          compwl(1:compnwl),compnwl, &
          cblend(1:cnblend),cnblend,cgen, &
          lsi,lsq,lsu,lsv,dols,lspol, &
          cconvval(1:compnwl),conv%doconv,conv%mode, &
          prefilter%val(1:compnwl),prefilter%doprefilter, &
          rwl,rnwl, & !conv%iwl_compare,-1, &
          cobs_par, &
          0,cvoigt,cmagopt,cuse_geff,cuse_pb,cpb_method, &
          cmodeval,ciprof_only,cuse(1:cnline,1:cnatm),&
          ifit,qfit,ufit,vfit,1_2,cold_norm,-1_2,chanle_azi,cnorm_stokes_val, &
          cold_voigt)
     statifit(1:compnwl,is)=ifit
     statqfit(1:compnwl,is)=qfit
     statufit(1:compnwl,is)=ufit
     statvfit(1:compnwl,is)=vfit
  end do

  !          ifitmax=maxloc(statfit(1:int(ctrl(1),2)))
  catm=atmfinal
  cline=linefinal
  cgen=genfinal
  cblend=blendfinal
  nptot=catm(1)%npar+cline(1)%npar+cblend(1)%npar+cgen%npar
  call get_pikaia_idx(atmfinal,linefinal,blendfinal,genfinal,&
       catm,cline,cblend,cgen,0_2)
  par(1:nptot)=parfit(1:nptot)
  call struct2pikaia(par(1:nptot),int(nptot,4),0_4,0_4,0_4)
  !          call struct2pikaia(par(1:nptot),int(nptot,4),0_4,0_4,0_4)

  line=cline(1:cnline)
  atmout=catm(1:cnatm)
  blendout=cblend(1:cnblend)
  genout=cgen
  fitval=f

  if (ncalls.gt.0) &
       call struct2pikaia(par(1:nptot),int(nptot,4),1_4,0_4,0_4)
  call compute_profile(catm(1:cnatm),cnatm, &
       cline(1:cnline),cnline, &
       compwl(1:compnwl),compnwl, &
       cblend(1:cnblend),cnblend,cgen, &
       lsi,lsq,lsu,lsv,dols,lspol, &
       cconvval(1:compnwl),conv%doconv,conv%mode, &
       prefilter%val(1:compnwl),prefilter%doprefilter, &
       rwl,rnwl, & !conv%iwl_compare,-1, &
       cobs_par, &
       0,cvoigt,cmagopt,cuse_geff,cuse_pb,cpb_method, &
       cmodeval,ciprof_only,cuse(1:cnline,1:cnatm),&
       ifit,qfit,ufit,vfit,1_2,cold_norm,-1_2,chanle_azi,cnorm_stokes_val, &
       cold_voigt)
  if (rnwl.eq.0) then 
     wlfit=compwl
     nwlfit=compnwl
     iwl_compare=conv%iwl_compare
     nwl_compare=conv%nwl_compare
  else
     wlfit=wl
     nwlfit=nwl
     nwl_compare=0
  end if

  !    deallocate(par)
  deallocate(opar)
  deallocate(lm_lower)
  deallocate(lm_upper)

  call cpu_time(tcpu(2))

  if (verbose.ge.1) then
     write(*,*) 'CPU-Time: ',tcpu(2)-tcpu(1),' Fitness: ',fitval
  end if


end subroutine call_pikaia
!!    ========================================================
