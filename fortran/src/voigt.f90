!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!  BOTH Subroutines for calc. voigt functions are copied from SPINOR        %
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!                                                                           %
!!  SUBROUTINE FVOIGT (da,dv,dh,dfv)                                         %
!!  --------------------------------                                         %
!!  input                                                                    %
!!  da: damping constant                                                     %
!!  dv: ratio of wavelength from line centre to doppler width                % 
!!      (dlam/dlam_D)                                                        %
!!  output                                                                   %
!!  dh: Voigt function at da and dv                                          %
!!  dfv:Faraday-Voigt function at da and dv                                  %
!!  NEW                                                                      %
!!  hdrv, fdrv : derivatives, see below                                      %
!!                                                                           %
!!===========================================================================%
!!                                                                           % 
!!  COMPUTES COMPLEX PROBABILITY FUNCTION AFTER HUMLICEK (1982)              % 
!!  JQSRT 27, 437. RETURNS REAL PART AS VOIGT FUNCTION                       %
!!                                                                           %
!!  This routine carries out its work in REAL due to an apparent             %
!!  lack of need of the extra (time-consuming) accuracy.                     %
!!                                                                           %
!!===========================================================================%
SUBROUTINE FVOIGT (nwl, a, v, dh, dfv)
  IMPLICIT NONE

  integer(2) :: nwl
  !      REAL(8), INTENT(IN)  :: da
  !      REAL(8), dimension(nwl), INTENT(IN)  :: dv
  REAL(4), dimension(nwl), INTENT(OUT) :: dh, dfv
  !      REAL(8), dimension(nwl), INTENT(OUT) :: hdrv, fdrv

  REAL(4)     :: a
!  REAL(4), dimension(nwl)     :: s
  REAL(8), dimension(nwl)     :: v,s
  COMPLEX(4), dimension(nwl) :: w4, z, t, u, v4
  integer(2) :: i

  z = cmplx( v, a ,kind=4)
  t = cmplx( a, -v,kind=4)

  s = abs ( v ) + a
  u = t*t

  do i=1,nwl
     if ( s(i) .lt. 15.0 ) goto 7002
     !!    Region 1 
     w4(i)=t(i)*0.5641896/(0.5+u(i))
     goto 7000
7002 if ( s(i) .lt. 5.5 ) goto 7003
     !!    Region 2
     w4(i)=t(i)*(1.410474+u(i)*0.5641896)/(0.75+u(i)*(3.0+u(i)))
!     w4(i)=t(i)*(1.4104739589d +u(i)*0.56418958355d)/(0.74999999999d+u(i)*(3.0+u(i)))
     goto 7000
     !!    Region 3
7003 if ( a .lt. 0.195*abs(v(i))-0.176 ) goto 7004
     w4(i)=(16.4955+t(i)*(20.20933+t(i)*(11.96482+ &
          t(i)*(3.778987+t(i)*0.5642236))))          &
          /(16.4955+t(i)*(38.82363+t(i)*(39.27121  &
          +t(i)*(21.69274+t(i)*(6.699398+t(i))))))
     goto 7000
     !!    Region 4
7004 w4(i)= t(i)*(36183.31-u(i)*(3321.9905-u(i)*(1540.787 &
          -u(i)*(219.0313-u(i)*(35.76683-u(i)*(1.320522-u(i)*0.56419))))))
     v4(i) = (32066.6-u(i)*(24322.84-u(i)*(9022.228-u(i)*(2186.181 &
          -u(i)*(364.2191-u(i)*(61.57037-u(i)*(1.841439-u(i))))))))
     w4(i) = exp(u(i)) - w4(i) / v4(i)

7000 continue
  end do

  dh = real(w4,kind=4)
  dfv= real(aimag(w4),kind=4)*0.5


  dfv=dfv*2.

  !!    Armstrong & Nicholls (1972)       
  !!    dw/dz = -2*z*w(z)+ 2i/sqrt(pi)
  !      dw=-2.0*z*w4+cmplx(0.0,1.1283791671)
  !      hdrv= real(dw,kind=8)
  !      fdrv= real(aimag(dw),kind=8)

END SUBROUTINE FVOIGT

!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!
!! subroutine VOIGT (nwl, A,V,dh,df)
!! ----------------------
!! This vectorizable Voigt function is based on the paper by
!! Hui, Armstrong and Wray, JQSRT 19, 509 (1977). It has been
!! checked agains the old Landolt & Boernstein standard voigt. Errors
!! become significant (at the < 1 % level) around the "knee" between
!! the Doppler core and the damping wings for a smaller than 1.e-3.
!! Note that, as written here, the routine has no provision for
!! the trivial case a=0. The normalization is such that the integral
!! is sqrt(pi); i.e., a plain exp(-x**2) should be used at a=0.
!! The new Landolt & Boernstein gives a useful expression for the
!! small but finite a case.
!! If J equals 1 then this function returns the magnetooptical profile.
!! This version cannot handle the case of J=1 and a=0 !!!!!!!!!!!!!!!!!!
!! V & A are dimensionless ( = wavelength/delta_wavelength = frequency/ 
!! delta_frequency = velocity/delta velocity)
!!
!! Coded by: A. Nordlund/16-dec-82.
!!-----------------------------------------------------------------------------

subroutine VOIGT (nwl, A,V,dh,df)
  IMPLICIT NONE

  integer(2) :: nwl
  REAL(4)     :: A
  real(8), dimension(nwl) :: V
  real(4), dimension(nwl) :: dh,df
  !! local
  COMPLEX(8), dimension(nwl) :: Z
  COMPLEX(8), dimension(nwl)     :: VGT  
  !! ---
  REAL(8) :: A0 = 122.607931777104326
  REAL(8) :: A1 = 214.382388694706425
  REAL(8) :: A2 = 181.928533092181549
  REAL(8) :: A3 = 93.155580458138441
  REAL(8) :: A4 = 30.180142196210589
  REAL(8) :: A5 = 5.912626209773153
  REAL(8) :: A6 = 0.564189583562615
  REAL(8) :: B0 = 122.607931773875350
  REAL(8) :: B1 = 352.730625110963558
  REAL(8) :: B2 = 457.334478783897737
  REAL(8) :: B3 = 348.703917719495792
  REAL(8) :: B4 = 170.354001821091472
  REAL(8) :: B5 = 53.992906912940207
  REAL(8) :: B6 = 10.479857114260399
  !! ---
  IF (abs(A).GE.1e-5) THEN
     Z=CMPLX(A,(V),kind=8)
     VGT=(((((((A6*Z+A5)*Z+A4)*Z+A3)*Z+A2)*Z+A1)*Z+A0) &
          /(((((((Z+B6)*Z+B5)*Z+B4)*Z+B3)*Z+B2)*Z+B1)*Z+B0))

     dh=real(vgt,kind=4)
     df=-aimag(vgt)*0.5
  ELSE
     write(*,*) 'Cannot handle a=0 in subroutine voigt'
  END IF

    df=df*2. !confirmed during comparison at ISSI workshop, Feb 1 2011
  END subroutine VOIGT
  
  !!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
