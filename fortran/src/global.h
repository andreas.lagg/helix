/*
  global.h - Global header file
*/
//#define MS_COMPILER //uncomment this line if using microsoft compiler
#ifdef MS_COMPILER
#define isnan _isnan
#endif

//All integers here are declared as Int, so by having typedef long Int I can
// turn all integers into longs; by having typedef int Int I can keep them
// as integers
typedef int Int;

//Add some rudimentary logical support!
typedef enum {False, True} boolean;
