(TeX-add-style-hook "helix"
 (lambda ()
    (LaTeX-add-bibliographies
     "lagg2_new"
     "andi")
    (LaTeX-add-environments
     "ipt")
    (LaTeX-add-labels
     "#1"
     "localinst"
     "compile"
     "compile_f90"
     "compmac"
     "compile_idl"
     "examples"
     "exsynth"
     "exsynth2c"
     "exhinode"
     "inputfile"
     "iptoldnorm"
     "modelatm"
     "straylight"
     "normcont"
     "iclocal"
     "icimage"
     "strategy"
     "piklm_speed"
     "atom"
     "weight"
     "hanle"
     "hanleold"
     "multiiter"
     "pb"
     "azicorr"
     "makesav"
     "xfits"
     "xdisplay"
     "multi"
     "statan"
     "fitsdescr"
     "tipfits"
     "ccx"
     "hinodedata"
     "crispdata"
     "fitsout"
     "tar_prepare"
     "mpichinstall"
     "mpich"
     "mpitest"
     "inputexamples"
     "oldnorm")
    (TeX-add-symbols
     '("colfigfive" 6)
     '("paramvi" 8)
     '("paramiv" 6)
     '("paramii" 2)
     '("link" 1)
     '("codesc" 1)
     '("code" 1)
     '("sref" 1)
     '("tab" 1)
     '("fig" 1)
     "figwidth"
     "helixp"
     "fortran"
     "idl"
     "mpich"
     "mpichi"
     "mpichii"
     "dislin"
     "unix"
     "myfont"
     "°"
     "kw"
     "fr"
     "bls"
     "blsipt")
    (TeX-run-style-hooks
     "geometry"
     "2.cm}"
     "2cm}"
     "color"
     "dvips"
     "inputenc"
     "latin1"
     "natbib"
     "epsfig"
     "times"
     "german"
     "latex2e"
     "article_andi12"
     "article_andi"
     "oneside"
     "titlepage"
     "12pt"
     "a4paper")))

