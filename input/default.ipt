;--------- COMMENTS ---------------------------------------------------
1-component fit
;--------- DIRECTORIES ------------------------------------------------
PS           ./ps/        ;directory for postscript output
SAV          ./sav/       ;directory for storing results (sav-file)
PROFILE_ARCHIVE ./profile_archive/ ;input directory for profiles / observations
ATM_ARCHIVE  ./atm_archive/ ;output directory for results (atmospheres)
ATM_SUFFIX   test         ;add a suffix to the atm-directory to identify this 
                          ;  run
WGT          ./wgt/       ;directory for wgt-files
ATOM         ./atom/      ;directory for atomic data files
;--------- CONTROL ----------------------------------------------------
DISPLAY_MAP  1            ;if 1 then display maps of parameters after 
                          ;  successful run (multiple pixels only!)
DISPLAY_PROFILE 1         ;if 1 then display fitted & observed profiles
DISPLAY_COMP 0            ;if 1 then display individual atm. components of 
                          ;  fitted profiles
VERBOSE      0            ;verbosity of output: 0=quiet, 1=normal, 2=women
SAVE_FITPROF 1            ;if 1 then save fitted profile as sav-file (can be 
                          ;  used as input profile)
FITSOUT      1            ;if 1 then write out map results in FITS format
OUTPUT       X            ;set to 'PS' for postscript output
OLD_NORM     0            ;use code with old normalization of the various 
                          ;  atmospheric components
OLD_VOIGT    0            ;if 1 use old normalization of Faraday/Voigt function
;--------- DATA SET ---------------------------------------------------
OBSERVATION   ;observation in one of the formats: 1.) sav file of 
                          ;  the format 'obsname'.profiles.sav, 2. 
                          ;  spinor-profile, 3. sav-file created with helix 
                          ;  'SAVE_FITPROF' keyword, 4. reduced TIP data file 
                          ;  (extension '.cc') and auxliary file ('.ccx')
WL_RANGE     10825.000000 10835.000000 ;WL-range to be used for analysis (may 
                          ;  be only a part of the observation WL-range)
WL_NUM       256          ;# of WL-points (for synthesis only)
WL_DISP      0.000000     ;WL-calibration: dispersion per WL-bin number
WL_OFF       0.000000     ;WL-calibration: offset (used if != 0)
WL_BIN       1            ;wavelength binning
XPOS         0 1          ;two-elements vector containing xmin,xmax of the 
                          ;  observation map to be analyzed
YPOS         0 1          ;two-elements vector containing ymin,ymax of the 
                          ;  observation map to be analyzed
PROFILE_LIST              ;filename of profile-list (2 columns: xxx yyy). If 
                          ;  present, XPOS and YPOS are ignored. File must be 
                          ;  located in ./ or PROFILE_ARCHIVE.
STEPX        1            ;step size for going from xmin to xmax
STEPY        1            ;step size for going from ymin to ymax
AVERAGE      1            ;if 1 then average observation over the stepx/stepy 
                          ;  size
SCANSIZE     0            ;stepsize of multiple scans within one observation
SYNTH        1            ;if 1 then create synthetic profile
NOISE        0.0100      ;noise level for adding random noise (IQUV)
INOISE       0.00000      ;noise level for adding random noise to Stokes I only
SMOOTH       0 0          ;smooth-value for profiles and smooth-method: 
                          ;  (0=IDL-smooth function,1=FFT Low-Pass)
MEDIAN       0            ;median filter for observed profiles (0=off, >=2 
                          ;  median filter width)
SPIKE        0            ;if >=1 then remove electronic spikes. Higher values 
                          ;  remove broader spikes.
MIN_QUV      0.00000      ;min. mag. signal. If sqrt(Q^2+U^2+V^2)<MIN_QUV then 
                          ;  the fit will be done with B=0.
STRAYPOL_AMP 0.00000      ;amplitude for stray-polarization (only used for 
                          ;  synthesis)
STRAYPOL_CORR     0 B      ;iteration steps and orientation of 
                          ;  scattering-polarization correction. Orientation: 
                          ;  'B' = along B-field, 'X' = a number defining an 
                          ;  angle manually
HANLE_AZI    0.00000      ;Offset for Hanle calc. (experimental)
SLIT_ORIENTATION    0.00      ;slit-orientation (used for straypol-corr)
M1ANGLE         0.00      ;angle of coelostat mirror M1
SOLAR_POS        0.00     0.00 ;pos. of observation (x and y in arcseconds)
SOLAR_RADIUS   950.00     ;radius of sun in arcsec for time of obs.
HELIO_ANGLE      0.00     ;heliocentric angle of observation in degrees (for 
                          ;  full Hanle). For TIP: theta=acos(sqrt(x^2+y^2)/r, 
                          ;  (x,y) = position of observation in terrestrial 
                          ;  coordinates.
+Q2LIMB          0.00     ;pos. Q to limb direction in degrees (for full 
                          ;  Hanle). For TIP: Q2L=atan(y,x), emission vector 
                          ;  angle gamma=360.-Q2L+90 .
HIN_SCANNR       1        ;Hinode Scan# (repetetive scans)
NORM_STOKES  IC           ;normalization of Stokes profiles: I or IC
NORM_CONT    LOCAL        ;type of cont. normalization: local, slit or image
IC_LEVEL     0.00000      ;set level for Ic. A positive number overwrites the 
                          ;  Ic value contained in the observational data.
LOCALSTRAY_RAD 0.0          ;add a local straylight component: take the 
                          ;  average Stokes profile of the surrounding 
                          ;  profiles within the radius specified here and mix 
                          ;  it via alpha to the other components. The radius 
                          ;  is in units of the pixel size defined with the 
                          ;  STEPX / STEPY keywords.
LOCALSTRAY_FWHM 0.0          ;FWHM of the Gaussian weighting used for the 
                          ;  localstraylight profiles
LOCALSTRAY_CORE 0.0          ;exclude core profiles within the radius 
                          ;  specified here
LOCALSTRAY_POL 1            ;1=polarized / 0=unpolarized local straylight
;--------- POST PROCESSING --------------------------------------------
CONV_FUNC                 ;convolution function of instrument
CONV_NWL     0            ;# of bins for convolution (used if number of 
                          ;  WL_bins in data is small
CONV_MODE    FFT          ;convolution method: FFT or MUL
CONV_WLJITTER 0.00000      ;uncertainty in \AA in knowledge of exact 
                          ;  wavelength position for every filter position 
                          ;  (e.g. caused by error in etalon voltage).  Make 
                          ;  sure that the wavelength resolution is high 
                          ;  enough (i.e. increase CONV\_NWL)! Must be set to 
                          ;  ZERO for real data!
CONV_OUTPUT  0            ;flag to control the display/output of the fitted 
                          ;  profile. If 1 then the profile is displayed with 
                          ;  CONV_NWL WL pixels, if 0 the WL-pixels of the 
                          ;  observation are used. NOTE: for local straylight 
                          ;  correction this flag is set to 0.
PREFILTER                 ;prefilter curve
PREFILTER_WLERR 0.00000      ;error in \AA in knowledge of prefilter curve 
                          ;  (e.g. caused by unknown temperature). Must be set 
                          ;  to ZERO for real data!
;--------- ATMOSPHERES ------------------------------------------------
NCOMP        1            ;number of components
; --- atmospheric component 1 ---
; NAME      Value    SCL_MIN    SCL_MAX FIT %RG FIT
BFIEL    1000.000      0.000   3000.000  -4 ;magnetic field value in Gauss
AZIMU      10.000    -90.000     90.000   3 ;azimut of B-vector [deg]
GAMMA      80.000      0.000    180.000   2 ;inclination of B-vector [deg]
VELOS       0.000  -7000.000   5000.000   1 ;line-of-sight velocity in m/s
VDOPP       0.200      0.010      0.700   1 ;doppler broadening (Voigt + 
                          ;  hanle-slab)
EZERO       5.000      0.000     10.000   0 ;amplitude of components of 
                          ;  propagation matrix (Voigt profile only!)
SGRAD       1.000     -2.000      8.000   1 ;gradient of source function
GDAMP       4.000      0.500     10.000   0 ;damping constant (Voigt profile 
                          ;  phys. units only!)
ALPHA       1.000      0.010      0.990   0 ;Filling factor for this component
USE_ATOM     he1083.0.dat ;atomic data file(s) for this component
;--------- LINE PARAMETERS --------------------------------------------
; --- fit parameters for line 1 ---
; NAME            Value   SCL_MIN   SCL_MAX  FIT
LINE_ID 10829.0911  ;wavelength to identify the line
LINE_STRENGTH       1.00      0.00      0.00   0 ;factor for line strength
LINE_WLSHIFT       0.00      0.00      0.00   0 ;adjust central WL of line
; --- fit parameters for line 2 ---
; NAME            Value   SCL_MIN   SCL_MAX  FIT
LINE_ID 10830.2501  ;wavelength to identify the line
LINE_STRENGTH       1.00      0.00      0.00   0 ;factor for line strength
LINE_WLSHIFT       0.00      0.00      0.00   0 ;adjust central WL of line
; --- fit parameters for line 3 ---
; NAME            Value   SCL_MIN   SCL_MAX  FIT
LINE_ID 10830.3397  ;wavelength to identify the line
LINE_STRENGTH       1.00      0.00      0.00   0 ;factor for line strength
LINE_WLSHIFT       0.00      0.00      0.00   0 ;adjust central WL of line
;--------- BLENDS -----------------------------------------------------
NBLEND       1 ;number of telluric blends
; --- telluric blend 1 ---
; NAME            Value   SCL_MIN   SCL_MAX FIT
BLEND_WL           0.000      0.000      0.000   0 ;WL and WL-range for blend
BLEND_WIDTH        0.000      0.000      0.000   0 ;width of voigt-profile
BLEND_DAMP         0.000      0.000      0.000   0 ;damping of voigt-profile
BLEND_A0           0.000      0.000      0.000   0 ;amplitude of voigt-profile 
                          ;  (blend is not used if BLEND_A0=0
;--------- GENERAL FIT PARAMETERS -------------------------------------
; NAME            Value   SCL_MIN   SCL_MAX FIT
CCORR              1.00      1.00      1.00   0 ;factor for cont. correction
STRAYLIGHT         0.00      0.00      1.00   0 ;straylight correction 
                          ;  (non-dispersive, non-polarized)
RADCORRSOLAR       1.00      0.10     10.00   0 ;Correction for the intensity 
                          ;  of the solar radiation field (for Hanle mode only)
;--------- ANALYSIS METHOD --------------------------------------------
IQUV_WEIGHT  1.00000 1.00000 1.00000 1.00000 ;4-element vector defining 
                          ;  relative weighting of IQUV (in that order). 
                          ;  Additionally the code does an automatic weighting 
                          ;  according to the strength of the I signal 
                          ;  compared to the QUV signals. This weighting 
                          ;  scheme is used in the PIKAIA fit routine.
WGT_FILE     he_default.wgt;file with WL-dependent weighting function for IQUV
PROFILE      voigt        ;functional form for pi- and sigma components of 
                          ;  spectral line. Available: gauss, voigt, 
                          ;  voigt_phys, voigt_szero
MAGOPT       1            ;include magneto-optical effects (dispersion 
                          ;  coefficients, (Voigt profile only!))
USE_GEFF     0            ;use effective Lande factor (=1) or real Zeeman 
                          ;  pattern (=0)
USE_PB       1            ;if set, the zeeman-splitting and strength includes 
                          ;  the Paschen-Back effect (from the table by 
                          ;  Socas-Navarro)
PB_METHOD    poly         ;use polynomials (=poly) or table interpolations 
                          ;  (=table) to calculate the PB-effect
;--------- PIKAIA PARAMATERS ------------------------------------------
CODE         FORTRAN      ;PIKAIA code to use. Available: FORTRAN (=fast) or 
                          ;  IDL (=platform independent).
METHOD       PIKAIA       ;minimization method: PIKAIA, POWELL (fast), LMDIF 
                          ;  (fast) or PIK_LM (combined, for maps)
NCALLS       1000         ;number of iterations in PIKAIA routine / max. 
                          ;  number of calls for POWELL or LMDIF
CHI2MODE     0            ;method to calc. chi2 (JM = Borrero method, PLAIN = 
                          ;  weight only depends on IQUV_WEIGHT values). 
                          ;  Default: Weight depends on 1/signal strength
PIXELREP     1            ;number of repetitions per pixel (to perform 
                          ;  statistical analysis)
KEEPBEST     0            ;store result only if fitness is better than 
                          ;  existing result (FITSOUT only)
;--------- END OF INPUT FILE ------------------------------------------
