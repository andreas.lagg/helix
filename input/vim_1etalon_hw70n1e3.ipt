;--------- COMMENTS ---------------------------------------------------
;--------- DIRECTORIES ------------------------------------------------
PS           ./ps/        ;directory for postscript output
SAV          ./sav/       ;directory for storing results (sav-file)
PROFILE_ARCHIVE /data/gbso/home/lagg/data/VIM/johann/single_etalon/
                          ;  ;input directory for profiles / observations
ATM_ARCHIVE  ./atm_archive/ ;output directory for results (atmospheres)
ATM_SUFFIX             ;add a suffix to the atm-directory to identify this 
                          ;  run
WGT          ./wgt/       ;directory for wgt-files
ATOM         ./atom/      ;directory for atomic data files
;--------- CONTROL ----------------------------------------------------
DISPLAY_MAP  1            ;if 1 then display maps of parameters after 
                          ;  successful run (multiple pixels only!)
DISPLAY_PROFILE 1            ;if 1 then display fitted & observed profiles
DISPLAY_COMP 0            ;if 1 then display individual atm. components of 
                          ;  fitted profiles
VERBOSE      0            ;verbosity of output: 0=quiet, 1=normal, 2=women
SAVE_FITPROF 0            ;if 1 then save fitted profile as sav-file (can be 
                          ;  used as input profile)
OUTPUT       X            ;set to 'PS' for postscript output
OLD_NORM     0            ;use code with old normalization of the various 
                          ;  atmospheric components
;--------- DATA SET ---------------------------------------------------
;OBSERVATION 6173_028588 ;observation in one of the 
OBSERVATION m6173single+0_hw70n1e3_scan.fitscc ;observation in one of the 
                          ;  formats: 1.) sav file of the format 
                          ;  'obsname'.profiles.sav, 2. spinor-profile, 3. 
                          ;  sav-file created with linefit 'SAVE_FITPROF' 
                          ;  keyword, 4. reduced TIP data file (extension 
                          ;  '.cc') and auxliary file ('.ccx')
WL_RANGE     6171.0 6175.8 ;6170.000000 6176.800000 ;WL-range to be used for analysis (may be 
                          ;  only a part of the observation WL-range)
WL_NUM       256          ;# of WL-points (for synthesis only)
WL_DISP      0            ;WL-calibration: dispersion per WL-bin number
WL_OFF       0            ;WL-calibration: offset (used if != 0)
WL_BIN       1            ;wavelength binning
XPOS         0 71        ;two-elements vector containing xmin,xmax of the 
                          ;  observation map to be analyzed
YPOS         0 71        ;two-elements vector containing ymin,ymax of the 
                          ;  observation map to be analyzed
STEPX        1            ;step size for going from xmin to xmax
STEPY        1            ;step size for going from ymin to ymax
AVERAGE      1            ;if 1 then average observation over the stepx/stepy 
                          ;  size
SCANSIZE     0            ;stepsize of multiple scans within one observation
SYNTH        0            ;if 1 then create synthetic profile
NOISE        0.000        ;noise level for adding artificial random noise
SMOOTH       0 0          ;smooth-value for profiles and smooth-method: 
                          ;  (0=IDL-smooth function,1=FFT Low-Pass)
MEDIAN       0            ;median filter for observed profiles (0=off, >=2 
                          ;  median filter width)
MIN_QUV      0.0          ;min. mag. signal. If sqrt(Q^2+U^2+V^2)<MIN_QUV then 
                          ;  the fit will be done with B=0.
STRAYPOL_AMP 0.0000       ;amplitude for stray-polarization (only used for 
                          ;  synthesis)
STRAYPOL_CORR     0 B      ;iteration steps and orientation of 
                          ;  scattering-polarization correction. Orientation: 
                          ;  'B' = along B-field, 'X' = a number defining an 
                          ;  angle manually
HANLE_AZI    0.           ;Offset for Hanle calc. (experimental)
SLIT_ORIENTATION    0.00      ;slit-orientation (used for straypol-corr)
SOLAR_POS    0.0 0.0      ;pos. of observation (x and y in arcseconds)
SOLAR_RADIUS 950.         ;radius of sun in arcsec for time of obs.
HIN_SCANNR   1            ;Hinode Scan# (repetetive scans)
NORM_STOKES  IC           ;normalization of Stokes profiles: I or IC
NORM_CONT    LOCAL        ;type of cont. normalization: local, slit or image
;--------- POST PROCESSING --------------------------------------------
CONV_FUNC    ./convolve/6173single+0_hw70.dat ;convolution function of instrument
CONV_NWL     130          ;# of bins for convolution (used if number of 
                          ;  WL_bins in data is small
;--------- ATMOSPHERES ------------------------------------------------
NCOMP        1            ;number of components
; --- atmospheric component 1 ---
; NAME      Value   SCL_MIN   SCL_MAX FIT %RG FIT
BFIEL    200.000      0.00   3000.00   4 ;magnetic field value in Gauss
AZIMU      1.000    -90.00     90.00   3 ;azimut of B-vector [deg]
GAMMA      1.000      0.00    180.00   2 ;inclination of B-vector [deg]
VELOS      0.000  -6000.00   6000.00   1 ;line-of-sight velocity in m/s
VDAMP      0.050      0.001     0.10   1 ;damping constant (Voigt only)
VDOPP      0.020      0.020     0.10   1 ;doppler broading (Voigt only)
EZERO      10.000      0.00    110.00  0 ;amplitude of components of 
                          ;  propagation matrix (Voigt profile only!)
SGRAD      1.000      0.10      4.00   6 ;gradient of source function
ALPHA      1.000      0.01      0.99   0 ;Filling factor for this component 
                          ;  (only with multiple components)
USE_ATOM     fe617.3.dat  ;atomic data file(s) to be used for this component
;--------- LINE PARAMETERS --------------------------------------------
; --- fit parameters for line 1 ---
; NAME            Value   SCL_MIN   SCL_MAX  FIT
LINE_ID 6173.3412  ;wavelength to identify the line
LINE_STRENGTH       1.00      0.00      0.00   0 ;factor to adjust line 
                          ;  strength
LINE_WLSHIFT       0.00      0.00      0.00   0 ;adjust central WL of line
;--------- BLENDS -----------------------------------------------------
NBLEND       1 ;number of telluric blends
; --- telluric blend 1 ---
; NAME            Value   SCL_MIN   SCL_MAX FIT
BLEND_WL           0.00      0.00      0.00   0 ;WL and WL-range for blend
BLEND_WIDTH        0.00      0.00      0.00   0 ;width of voigt-profile
BLEND_DAMP         0.00      0.00      0.00   0 ;damping of voigt-profile
BLEND_A0           0.00      0.00      0.00   0 ;amplitude of voigt-profile 
                          ;  (blend is not used if BLEND_A0=0
;--------- GENERAL FIT PARAMETERS -------------------------------------
; NAME            Value   SCL_MIN   SCL_MAX FIT
CCORR              1.00      1.00      1.00   0 ;factor for continuum 
                          ;  correction
STRAYLIGHT         0.00      0.00      0.00   0 ;straylight correction 
                          ;  (non-dispersive, non-polarized)
;--------- ANALYSIS METHOD --------------------------------------------
IQUV_WEIGHT  1.0000 1.0000 1.0000 1.0000 ;4-element vector defining relative 
                          ;  weighting of IQUV (in that order). Additionally 
                          ;  the code does an automatic weighting according to 
                          ;  the strength of the I signal compared to the QUV 
                          ;  signals. This weighting scheme is used in the 
                          ;  PIKAIA fit routine.
WGT_FILE     fe_6173.wgt  ;file with WL-dependent weighting function for IQUV
PROFILE      voigt        ;functional form for pi- and sigma components of 
                          ;  spectral line. Available: gauss, voigt or 
                          ;  voigt_phys
MAGOPT       1            ;include magneto-optical effects (dispersion 
                          ;  coefficients, (Voigt profile only!))
USE_GEFF     1            ;use effective Lande factor (=1) or real Zeeman 
                          ;  pattern (=0)
USE_PB       0            ;if set, the zeeman-splitting and strength includes 
                          ;  the Paschen-Back effect (from the table by 
                          ;  Socas-Navarro)
PB_METHOD    poly         ;use polynomials (=poly) or table interpolations 
                          ;  (=table) to calculate the PB-effect
;--------- PIKAIA PARAMATERS ------------------------------------------
CODE         FORTRAN      ;PIKAIA code to use. Available: FORTRAN (=fast) or 
                          ;  IDL (=platform independent).
METHOD       PIKAIA       ;minimization method: PIKAIA, POWELL (fast), LMDIF 
                          ;  (fast) or PIK_LM (combined, for maps)
NCALLS       1000          ;number of iterations in PIKAIA routine / max. 
                          ;  number of calls for POWELL or LMDIF
;--------- END OF INPUT FILE ------------------------------------------
