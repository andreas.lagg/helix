;test difference between fortran and idl-version
pro test_cp,ipt_file=ipt_file
  
  if n_elements(ipt_file) eq 0 then ipt_file='test.ipt'
  
  ipt=read_ipt(ipt_file)
  
  ipt.synth=1
  ipt.ncalls(*)=0
  
  ipt.code='IDL'
  helix,fitprof=prof_idl,struct_ipt=ipt,result=res_idl
  
  ipt.code='FORTRAN'
  helix,fitprof=prof_f90,struct_ipt=ipt,result=res_f90
     
  plot_profiles,prof_idl,prof_f90,title='Fortran - IDL Comparison', $
    lthick=[1,1],color=[3,1],lstyle=[0,0], $
    fraction=[1,.4,.4,.4]
  stop
end
