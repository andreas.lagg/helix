;unno solution for inclination angle as defined in Auer, 1977, Sol.Phys
pro fit_inc,s3_hat,inc,s1_hat,pder

;sifit(4)=(sifit(4)>0)<0.5

s1_hat=s3_hat*((sin(inc))^2.0)/(2.0*cos(inc))


pder=s3_hat*sin(inc)*(1+(sin(inc))^2/(cos(inc))^2/2.)
end
