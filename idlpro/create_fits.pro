;example to create fits file in the format of TIP.
pro create_fits
  
                                ;get data: Read in a TIP-FITS file
;  data=readfits('/data/slam/VTT-data/VTT-Apr07/02May07/02may07.004-04cc', $
;                header)
;  data=readfits('/home/lagg/data/GREGOR/08sep14/08sep14.005-??cc', $
;                header)
  dir='/home/lagg/data/GREGOR/08sep14/'
  mask='08sep14.005-??cc'
  common data,data
  read_data,dir=dir,mask=mask
  stop
  fitsout='/home/lagg/data/GREGOR/08sep14/08sep14.005.4d.fits'
  
                                ;now data contains an array with 1009
                                ;WL-pixels, 454 pixels in y direction
                                ;and 12 pixels in x-direction (3x4
                                ;Stokes vectors)
  szd=size(data.sp)
  print,'============ SIZE ======================'
  print,'Size: ',szd
  print,'WL-pixels: ',szd(1)
  print,'y-pixels: ',szd(4)
  print,'x-pixels: ',szd(2)
  print,'Stokes: ',szd(3)
  

                                ;the relevant part of the header is:
;  headout=header(0:5)
;  print,'============ HEADER ===================='
;  print,headout
;  print,'========================================'
  
  dat=float(transpose(data.sp,[0,3,1,2]))
;  byteorder,data,/lswap
  
  print,'======= WRITE NEW FITS ================='
  print,'File: ',fitsout
  
  writefits,fitsout,data,append=0
  print,'Done.'
end
