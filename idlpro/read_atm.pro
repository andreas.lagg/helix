function read_atm,file=file,ipt=ipt,init=init,header=header
  common empty_struct,empty_struct
  
  if size(file,/type) eq 7 then atm_file=file $
  else atm_file=file.path+'/'+file.atm
  
  if keyword_set(init) or n_elements(empty_struct) eq 0 then $
    empty_struct=result_struc(nx=1,ny=1,ipt=ipt,header=header) 
  retval=empty_struct
  atm_out=ipt.atm
  line_out=ipt.line
  blend_out=ipt.blend
  gen_out=ipt.gen
;   line_out=replicate(def_linest(),ipt.nline)
;   blend_out=replicate(def_blendst(),ipt.nblend)
;   gen_out=def_genst()
  
                                ;set blend parameters to 0 (if they
                                ;are not contained in atm-file then
                                ;there should be no blend!)
  for it=0,n_tags(blend_out.par)-1 do $
    if (tag_names(blend_out.par))(it) ne 'DUMMY' then begin
    blend_out.par.(it)=0.
    blend_out.fit.(it)=0
  endif
                                ;same for general parameters
  for it=0,n_tags(gen_out.par)-1 do $
    if (tag_names(gen_out.par))(it) ne 'DUMMY' then begin
    gen_out.par.(it)=0.
    gen_out.fit.(it)=0
  endif
                                ;special default value for ccorr:
  gen_out.par.ccorr=1.  
  gen_out.par.radcorrsolar=1.  
  gen_out.par.straylight=0.
  
  
  openr,unit,/get_lun,atm_file,error=err
  if err ne 0 then begin
    message,/cont,'Error reading atm-file: '+atm_file
    free_lun,unit
    reset
  endif
  
  name=def_parname(tag_names(atm_out(0).par))
  blendname=def_blendname(tag_names(blend_out(0).par))
  genname=def_genname(tag_names(gen_out(0).par))
  section='atm'
  clidx=-1
  repeat begin
    line=''
    readf,unit,line
    line=strtrim(line,2)
    lnocmt=(strsplit(/extract,line,';'))(0)
    word=strsplit(/extract,lnocmt)
    if n_elements(word) gt 1 then begin
      if (strupcase(word(0)) eq 'LINE' and $
          strupcase(word(1)) eq 'PARAMETERS:') then section='line'
      if (strupcase(word(0)) eq 'BLEND' and $
          strupcase(word(1)) eq 'PARAMETERS:') then section='blend'
      if (strupcase(word(0)) eq 'GENERAL' and $
          strupcase(word(1)) eq 'PARAMETERS:') then section='general'
    endif
    nosec=1
    case section of
      'atm': begin
        nosec=0
        case strupcase(word(0)) of
          'FITNESS:': begin
            if strmid(word(1),0,1) eq '*' then fitness=99999999. $
            else fitness=float(word(1))
          end
          else: begin           ;normal atmospheric parameters
            paridx=(where(name.ipt eq strupcase(word(0))))(0)
            if paridx ne -1 then begin
              atm_out(0:ipt.ncomp-1).par.(paridx)=float(word(1:ipt.ncomp))
              atm_out(0:ipt.ncomp-1).fit.(paridx)= $
                fix(word(ipt.ncomp+1:ipt.ncomp*2))
            endif
          end
        endcase
      end
      'blend': begin
        nosec=0
        paridx=(where(blendname.ipt eq strupcase(word(0))))(0)
        if paridx ne -1 then begin
          blend_out(0:ipt.nblend-1).par.(paridx)=float(word(1:ipt.nblend))
          blend_out(0:ipt.nblend-1).fit.(paridx)= $
            fix(word(ipt.nblend+1:ipt.nblend*2))
        endif
      end
      'general': begin
        nosec=0
        paridx=(where(genname.ipt eq strupcase(word(0))))(0)
        if paridx ne -1 then begin
          gen_out.par.(paridx)=float(word(1))
          gen_out.fit.(paridx)=fix(word(2))
        endif
      end
      'line': begin             ;line section
        nosec=0
        lidx=where(strpos(ipt.line.id,line) eq 0)
        if lidx(0) ne -1 then clidx=lidx(0)
        if clidx ge 0 and clidx lt ipt.nline then begin
          case strupcase(word(0)) of
            'VLOS': line_out(clidx).straypol_par(0:ipt.ncomp-1).vlos= $
              float(word(1:ipt.ncomp))
            'WIDTH': line_out(clidx).straypol_par(0:ipt.ncomp-1).width= $
              float(word(1:ipt.ncomp))
            'DAMP': line_out(clidx).straypol_par(0:ipt.ncomp-1).damp= $
              float(word(1:ipt.ncomp))
            'DOPP': line_out(clidx).straypol_par(0:ipt.ncomp-1).dopp= $
              float(word(1:ipt.ncomp))
            'STRAYPOL_AMP': begin
              line_out(clidx).par.straypol_amp=float(word(1))
              line_out(clidx).fit.straypol_amp=fix(word(1))
            end
            'STRAYPOL_ETA0': begin
              line_out(clidx).par.straypol_eta0=float(word(1))
              line_out(clidx).fit.straypol_eta0=fix(word(1))
            end
            'STRENG': begin ;due to error in code some files skipped the
                                ;last two letters of 'STRENGTH'
              line_out(clidx).par.strength=float(word(1))
              line_out(clidx).fit.strength=fix(word(1))
            end
            'WLSHI': begin ;due to error in code some files skipped the
                                ;last two letters of 'WLSHIFT'
              line_out(clidx).par.wlshift=float(word(1))
              line_out(clidx).fit.wlshift=fix(word(1))
            end
            'STRENGTH': begin
              line_out(clidx).par.strength=float(word(1))
              line_out(clidx).fit.strength=fix(word(1))
            end
            'WLSHIFT': begin
              line_out(clidx).par.wlshift=float(word(1))
              line_out(clidx).fit.wlshift=fix(word(1))
            end
            else:
          endcase
        endif
      end
    endcase
  endrep until eof(unit)
  free_lun,unit
  
  if nosec eq 1 or n_elements(fitness) eq 0 then begin
    message,/cont,'Invalid ATM-File: '+atm_file
    return,retval
  endif
  
  old=retval.line
  struct_assign,line_out,old
  retval.line=old
  
  old=retval.fit.atm
  struct_assign,atm_out,old
  retval.fit.atm=old
  
  old=retval.fit.blend
  struct_assign,blend_out,old
  retval.fit.blend=old
  
  old=retval.fit.gen
  struct_assign,gen_out,old
  retval.fit.gen=old
  
  old=retval.fit.linepar
  struct_assign,line_out.par,old
  retval.fit.linepar=old
  
  retval.fit.fitness=fitness
  if size(file,/type) eq 8 then begin
    retval.fit.x=file.x
    retval.fit.y=file.y
  endif
  
  
  return,retval
end

