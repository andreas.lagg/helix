;example to create fits file in the format of TIP.
pro create_fits_choudary
  
                                ;get data: Read sav-files
;  sav='/data/slam/VTT-data/ASP_Data/20011017/oct17_5172_map04'
  sav='/data/slam/VTT-data/ASP_Data/20011017/oct17_6300_map04'
  add=['i','q','u','v']
  for i=0,3 do begin
    sfile=sav+add(i)+'.sav'
    sObj = OBJ_NEW('IDL_Savefile',sfile )
    names=sObj->Names()
    OBJ_DESTROY,sObj
    restore,/v,sfile
    dummy=execute('dat='+names(0))
    if i eq 0 then begin
      iimg=dat
      sz=size(dat)
      nwl=sz(1)
      nx=sz(2)
      ny=sz(3)
      data=make_array(nwl,ny,nx*4,type=sz(0))
    endif
    for ix=0,nx-1 do data(*,*,ix*4+i)=reform(dat(*,ix,*))
  endfor
  
                                ;now data contains an array with 1009
                                ;WL-pixels, 454 pixels in y direction
                                ;and 12 pixels in x-direction (3x4
                                ;Stokes vectors)
  szd=size(data)
  print,'============ SIZE ======================'
  print,'Size: ',szd
  print,'WL-pixels: ',szd(1)
  print,'y-pixels: ',szd(2)
  print,'x-pixels (*4): ',szd(3)
  

                                ;the relevant part of the header is:
;  headout=header(0:5)
;  print,'============ HEADER ===================='
;  print,headout
;  print,'========================================'
  
  data=float(data)
;  byteorder,data,/lswap
  
  froot=strmid(sav,strpos(sav,'/',/reverse_search)+1,strlen(sav))
  fitsout='~/tmp/'+froot+'.fitscc'
  print,'======= WRITE NEW FITS ================='
  print,'File: ',fitsout
  
  writefits,fitsout,data,append=0
  
;get continuum image
  icont=max(iimg,dimension=1)
  
;wl-vector
  lm=findgen(nwl)+1
                                ;for Mg b
  if strpos(sav,'5172') ne -1 then begin
    lmd=[5171.61d,5172.7,5173.75]
    pos=[21.620503d ,123.92684,231.21941]
  endif else if strpos(sav,'6300') ne -1 then begin
                                ;for FeI
    lmd=[6301.5,6302.5]
    pos=[85.508862,163.97693]
  endif else message,'Unknown WL.'
  
  result=poly_fit(pos,lmd,1)
  wloff=result(0)
  wlbin=result(1)
  print,'WL-cal: ',wloff,wlbin

  
;now create ccx file  
  print,'======= WRITE NEW CCX ================='
  filex=fitsout+'x'
                                ;create header
  header=''
  
  writefits,filex,icont,header,append=0
  naxpos=max(where(strpos(header,'NAXIS') eq 0))
  header=[header(0:naxpos), $
          'WL_NUM  = '+string(nwl,format='(i20)')+ $
          ' / WL-bins', $
          'WL_OFF  = '+string(wloff,format='(d20.8)')+' / WL-Offset', $
          'WL_DISP = '+string(wlbin,format='(d20.8)')+' / WL-Dispersion', $
          header(naxpos+1:*)]
  writefits,filex,icont,header,append=0        
  
                                ;write out wl-vector as extension
  wlvec=dindgen(nwl)*wlbin+wloff
  writefits,filex,wlvec,append=1
  print,'Wrote '+filex
  
  
  print,'Done.'
  
end
