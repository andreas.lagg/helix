;this helper routine starts helix with special initial conditions
;(eg for velocity)
pro initial_value
  
                                ;restore save-file containing initial values
  restore,/v,'sav/gauss_vel_13may01.sav'
  szv=size(vmc)
                                ;load inout-file to be used for
                                ;inversion
  ipt=read_ipt('./input/13may01.014_2comp_gdamp.ipt')
  
                                ;get list of profiles  
  list=get_proflist(ipt,xcrd=xcrd,xcexact=xcexact, $
                    ycrd=ycrd,ycexact=ycexact,xcoff=xcoff,ycoff=ycoff)
  
                                ;loop over pixels
  ipt_orig=ipt
  for iix=0,n_elements(xcrd)-1 do for iiy=0,n_elements(ycrd)-1 do begin
    ix=xcrd(iix) & iy=ycrd(iiy)
    if ix lt szv(1) and iy lt szv(2) then begin
      ipt=ipt_orig
                                ;set velocities (km/s -> m/s, invalid if -100.)
                                ;and set scaling according to initial
                                ;values
      redo=0
      print,'Pixel '+n2s(ix)+','+n2s(iy)
      for ic=0,1 do begin
        if ic eq 0 then vini=vmc(ix,iy)*1e3 else vini=vsc(ix,iy)*1e3
        if vini gt -99. then begin          
          ipt.atm(ic).par.vlos=vini
          ipt.scale(ic).vlos.min=( vini -2e3 )>ipt.scale(ic).vlos.min
          ipt.scale(ic).vlos.max=( vini +2e3 )<ipt.scale(ic).vlos.max
          print,'Comp '+n2s(ic+1)+': Changed initial value to '+n2s(vini)+ $
            ' ('+n2s(ipt.scale(ic).vlos.min)+' to '+ $
            n2s(ipt.scale(ic).vlos.max)+')'
          redo=1
        endif
      endfor
      
                                ;run inversion & store results
      if redo then $
        helix,list=[ix,iy],struct_ipt=ipt,result=result,/force_write
      

    endif
  endfor
end
