pro synth
  
  physical_constants
  atm=def_parst()
  he=def_line(line='he')
  he(2).geff=5.25
;  he=he(2) & he.f=1.
  atm.use_line(0:n_elements(he)-1)=he.id
  he.icont=100
  
  atm.par.b=200
  atm.par.a0=.15
  atm.par.width=.005
  atm.par.vlos=0.
  atm.par.inc=73
  atm.par.azi=70
  atm.par.ff=1
  
  nel=1000l & rg=6.
  wl=(findgen(nel+1)-nel/2.)/nel*rg+10830d
  
 ang=findgen(40)*10+1
;  ang= atm.par.inc
; azi=23
 
  for i=0,n_elements(ang)-1 do  for iv=0,0 do begin

   voigt=iv
    atm.par.inc=ang(i)
    
    prof=compute_profile(line=he,atm=atm,wl=wl,/init,voigt=voigt)
    plot_profiles,prof
    
     common for_amoeba,u_signal,q_signal,quv_weight
     quv_weight=prof.u^2+prof.q^2+prof.v^2 ;+(1-prof.i/he(0).icont)^2
;quv_weight(*)=1     
     val=where(abs(quv_weight) ge 0.00001*max(quv_weight))
     dummy=min(prof.v) & idx1=!c
     dummy=max(prof.v) & idx2=!c
     quv_weight(min([idx1,idx2]):max([idx1,idx2]))=0
;     quv_weight=quv_weight(val)
     u_signal=prof.u;(val)
     q_signal=prof.q;(val)
     v_signal=prof.v;(val)
     alpha2=amoeba(1.0e-5,scale=1.,p0=0.,function_value=diff, $
                   function_name='fit_azi_amoeba',ncalls=n_amoeba,nmax=100)
     azi=(alpha2(0)/2./!dtor) mod 360
     unno_azi=azi

     dummy=max(prof.v) & idx=!c
;     unno_azi=atan(prof.u(idx),prof.q(idx))/!dtor/2
    azi=unno_azi
    
    R=2*(prof.q(idx)*sin(2*unno_azi*!dtor)+ $
         prof.u(idx)*cos(2*unno_azi*!dtor))/prof.v(idx)
     unno_inc=acos(0.5*(sqrt(R^2+4) -R))/!dtor
     
     dummy=min(prof.v) & idx0=!c-5
;     quv_weight(min([idx0,idx]):max([idx0,idx]))=0
;     azi=atm.par.azi
     common for_amoeba_inc,s1_hat,s3_hat,s13_weight
     s3_hat=prof.v*1e3
     s1_hat=(prof.q*sin(2*azi*!dtor) + $
             prof.u*cos(2*azi*!dtor))*1e3
     diff=s1_hat/s3_hat-((sin(unno_inc*!dtor))^2/2/cos(unno_inc*!dtor))(0)
     s13_weight=(prof.u^2+prof.q^2+prof.v^2)*(prof.i^2);fltarr(n_elements(s3_hat))+1.

;      dummy=min(prof.v) & idx1=!c
;      dummy=max(prof.v) & idx2=!c
;      s13_weight(min([idx1,idx2])+10:max([idx1,idx2])-10)=0
     
     dummy=min(prof.i) & idx=!c
;    s13_weight(idx-nel/100.:idx+nel/100.)=0
;     
     inc=1.                     ;initial guess
;        s1_fit= CURVEFIT((s3_hat),(s1_hat), s13_weight, inc, $
;                       SIGMA, FUNCTION_NAME='fit_inc',/DOUBLE,ITER=it, $
;                       CHISQ=chi,TOL=1e-8)     
;        unno_inc=inc(0)/!dtor
!p.multi=0     

;s13_weight(*)=1
       inc=amoeba(1.0e-6,scale=.01,p0=atm.par.inc*!dtor,function_value=diff, $
                  function_name='fit_inc_amoeba',ncalls=n_amoeba,nmax=80)
;     plot,(s1_hat) & oplot,color=1,s1_fit

       print,atm.par.inc,inc(0)/!dtor,unno_inc
       dummy=fit_inc_amoeba(atm(0).par.inc*!dtor)
;        stop
       unno_inc=inc(0)/!dtor
     
     
       R=(2*s1_hat/s3_hat)
       inc=acos(0.5*(sqrt(R^2+4)-R))/!dtor
       
       plot,inc
       fi=where(finite(inc))
;       print,max(inc(fi))
    xyouts,0,0,/normal,n2s(ang(i))+' '+n2s(unno_inc)+' '+n2s(unno_azi)
    
    if abs(unno_azi - atm.par.azi) gt 5 then stop
;if finite(unno_inc) eq 0 then stop
    wait,.2
  endfor
  
  stop
end
  
  
