;routine to write out all profiles and atmospheres contained in sav-file
pro write_all
  common pikaia,pikaia_result,oldsav,prpar
  common wgst,wgst
  @common_cppar
  
  line=pikaia_result.line
  ipt=pikaia_result.input
  atm=pikaia_result.fit(0).atm
  if strcompress(ipt.observation) ne '' then $
    atm_path=ipt.dir.atm+'atm_'+ipt.observation $
  else begin
    ifp=get_filepath(ipt.file)
    atm_path=ipt.dir.atm+'atm_'+ifp.base
  endelse

  if strcompress(ipt.dir.atm_suffix,/remove_all) ne '' then $
    atm_path=atm_path+'_'+ipt.dir.atm_suffix
  
  if n_elements(wgst) ne 0 then begin
    wgbase=wgst.base.id
    widget_control,wgst.base.id,hourglass=1,sensitive=0
  endif
  
  
  yn=dialog_message(/question,dialog_parent=wgbase,/default_no, $
                    ['This will overwrite all profiles and atmospheres in', $
                     atm_path, $
                     'with the values currently displayed in xdisplay.', $
                     '','Do you want to continue?'])
  
  if yn eq 'Yes' then begin
    voigt=fix(ipt.profile eq 'voigt')
    modeval=fix(ipt.modeval)
    magopt=fix(ipt.magopt(0))
    norm_stokes_val=ipt.norm_Stokes_val
    hanle_azi=ipt.hanel_azi
    old_norm=fix(ipt.old_norm)
    old_voigt=fix(ipt.old_voigt)
    use_geff=fix(ipt.use_geff(0))
    use_pb=fix(ipt.use_pb(0))
    iprof_only=0
    
    resolve_routine,'xdisplay',/compile_full_file
    resolve_routine,'compute_profile',/compile_full_file,/is_function
    physical_constants
    
    szxy=size(pikaia_result.fit)
    
    
    print,'Writing out profiles: '+atm_path
    for ip=0,n_elements(pikaia_result.fit)-1 do begin
      atm=pikaia_result.fit(ip).atm
      blend=pikaia_result.fit(ip).blend
      x=pikaia_result.fit(ip).x
      y=pikaia_result.fit(ip).y
      
      doplot,x,y,/noplot,obs=obs,icont=icont
      
      if n_elements(conv) eq 0 then begin
                                ;read convolution function (needs
                                ;WL-vector from observation) 
        conv=read_conv(pikaia_result.input.wl_range, $
                       pikaia_result.input.conv_func, $
                       pikaia_result.input.conv_nwl, $
                       pikaia_result.input.conv_mode, $
                       pikaia_result.input.conv_wljitter, $
                       pikaia_result.input.prefilter, $
                       obs.wl, $
                       pikaia_result.input.verbose)
        if conv.doconv eq 1 then convval=conv.val(0:conv.nel-1) else convval=-1
      endif
      if n_elements(prefilter) eq 0 then begin
                                ;prefilter_wlerr set to 0.
        prefilter=read_prefilter(pikaia_result.input.prefilter, $
                                 0.0, $
                                 obs.wl, $
                                 pikaia_result.input.verbose)
      endif
      
      fill_localstray,lsi,lsq,lsu,lsv,dols,obs.wl,n_elements(obs.wl)
      cfitprof=compute_profile(/init,line=line,obs_par=ipt.obs_par, $
                               atm=atm,wl=obs.wl,blend=blend, $
                               lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv,dols=dols, $
                               convval=convval,doconv=conv.doconv, $
                               conv_mode=conv.mode, $
                               prefilterval=prefilter.val, $
                               doprefilter=prefilter.doprefilter, $
                               ret_wlidx=0,nret_wlidx=-1)
      fitprof={ic:icont,wlref:line(0).wl,wl:cfitprof.wl, $
               i:cfitprof.i,q:cfitprof.q,u:cfitprof.u,v:cfitprof.v}
      xstr='x'+n2s(x,format='(i4.4)')
      ystr='y'+n2s(y,format='(i4.4)')
      xyinfo=xstr+ystr
      resultname="/"+xstr+'/'+xyinfo+'.profile.dat'
      ix=x-pikaia_result.input.x(0)
      iy=y-pikaia_result.input.y(0)
      write_atm,atm=pikaia_result.fit(ix,iy).atm,line=pikaia_result.line, $
        fitness=pikaia_result.fit(ix,iy).fitness,fitprof=fitprof, $
        resultname=atm_path+resultname,xyinfo=xyinfo,ipt=pikaia_result.input, $
        blend=pikaia_result.fit(ix,iy).blend, $
        gen=pikaia_result.fit(ix,iy).gen
      prog=float([ip,ip-1])/n_elements(pikaia_result.fit)*80
      if fix(prog(0)) ne fix(prog(1)) then print,'.',format='(a,$)'
    endfor  
    print
    print,'Done.'
  endif
  
  if n_elements(wgst) ne 0 then $
    widget_control,wgst.base.id,hourglass=0,sensitive=1
end
