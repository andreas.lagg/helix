
;find optimum binning for convoukltion function 
pro find_opt_binning,wlrg=wlrg,wlin=wlin,best=best
  maxpar
  
  if n_elements(wlrg) eq 0 or n_elements(wlin) eq 0 then begin
    print,'Routine to determine optimum binning when using convolution function.'
    print,'See helix documentation for details.'
    print,'Example:'
    print,'   find_opt_binning,wlin=6173.3408d + [-2.977387E-01, -1.469849E-01, -7.160804E-02, 3.768844E-03, 7.914573E-02, 1.545226E-01],wlrg=[6171.0,6175.5]'
    reset
  endif
  
@common_maxpar  
  
  nwl=n_elements(wlin)
  diff=dblarr(maxwl,nwl)+1e30
  tdiff=dblarr(maxwl)+1e30
  for j=(nwl>2),maxwl-1 do begin
    diff(j,*)=0
    for i=0,nwl-1 do begin
      diff(j,i)=min(abs(dindgen(j)/(j-1d)*(wlrg(1)-wlrg(0))+wlrg(0)-wlin(i)))
    endfor
    tdiff(j)=total(diff(j,*))/j    
    if j ge 1 then $
      if tdiff(j-1) gt 1e-10 and tdiff(j) le min(tdiff(0:j-1)) then $
      print,'Bin: ',j,' AvgDiff: ',tdiff(j)*1e6,' MaxDiff: ',max(diff(j,*))*1e6
  endfor
  
  
                                ;find local minima:
  lgdiff=(abs(tdiff/shift(tdiff,1)))(1:*)
  locmin=where(lgdiff ge 5* total(median(lgdiff,3))/n_elements(lgdiff))
  imold=-1
  if locmin(0) ne -1 then for i=0,n_elements(locmin)-1 do begin
    il=[(locmin(i)-5)>0,(locmin(i)+5)<(n_elements(tdiff)-1)]
    lmin=min(abs(tdiff(il(0):il(1))))
    imin=il(0)+!c
    if imin ne imold then $
      print,format='(a,i5,2(a,e10.3))','Good binning: bin # =',imin, $
      '  avg.diff: ',lmin,'  max.diff: ',max(diff(lmin+il(0),*))
    imold=imin
  endfor
  mindiff=min(tdiff(0:149))  & minloc=!c
  print,'Best binning (max 150): bin # =',minloc,'  avg. error: ',mindiff
  best=[minloc,mindiff*1e6]
  
  mindiff=min(tdiff(0:299))  & minloc=!c
  print,'Best binning (max 300): bin # =',minloc,'  avg. error: ',mindiff
  best=[minloc,mindiff*1e6]
  
  mindiff=min(tdiff)  & minloc=!c
  print,'Best binning: bin # =',minloc,'  avg. error: ',mindiff
  plot,tdiff(nwl:*),/ytype
  
                                ;automatic determination
  print,'Automatic determination:'
  dwl=(wlin-shift(wlin,1))(1:*)
  nd=n_elements(dwl)
  reso=5d-4                     ;resolution: 1e-3 is for 1 mA
  maxfct=0
  wlshift=1d-3                    ;allow +-wlshift mAngstrom 
  nv=fix(wlshift/reso)*2+1
  pvec=(dindgen(nv)/(nv-1)*2-1)*wlshift
  np=nd^nv
  print,'Testing all possibilities +-',wlshift,' around WLIN'
  print,'number of possibilities: ',np
  ivec=lonarr(nd,np)
  for i=0l,np-1 do begin
    for j=0,nd-1 do ivec(j,i)=(i / (nv^j)) mod nv
  endfor
  for ip=0l,np-1 do begin
    pwl=dwl
    for j=0,nd-1 do pwl(j)=pwl(j)+pvec(ivec(j,ip))
    wli=long(round(pwl*1e3/reso)*reso) ;in mA
                                ;computelargest common divisor
    prfct=lonarr(nd,max(wli)/2)
    nfct=lonarr(nd)
    for i=0,nd-1 do begin
      fct=primfact(wli(i),nfact=nf)
      prfct(i,0:nf-1)=fct
      nfct(i)=nf
    endfor
    fct=1l
    for i=0,nd-2 do begin
      for j=0,nfct(i)-1 do if prfct(i,j) ne 0 then begin
        ii=where(prfct eq prfct(i,j))
        if ii(0) ne -1 then begin
          ix=ii mod nd
          iy=ii  /  nd
          six=ix(sort(ix))            
          uix=six(uniq(six))
          if n_elements(uix) eq nd then begin
            fct=fct*prfct(i,j)
            for k=0,nd-1 do begin
              idx=(where(ix eq uix(k)))(0)
              prfct(ix(idx),iy(idx))=0
            endfor
          endif
        endif
      endif
    endfor
    if fct gt maxfct then begin
      wlbin=fct*1d-3
      N=long((wlrg(1)-wlrg(0))/(wlbin)/2)
      print,format='(a,i8,a,f8.5,a,2(f12.6,a))','CONV_NWL=',2*N+1, $
        ' & dWL=',wlbin, $
        'd &  WL_RANGE= ',wlin(nd/2)-n*wlbin,'  ',wlin(nd/2)+N*wlbin,''
      maxfct=fct
    endif
  endfor
  
  stop
end
