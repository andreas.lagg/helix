pro fehin_test,ps=ps,norun=norun
  common res,resarr,ccorr
  common old_ipt,old_ipt
  
  
  
  ipt=read_ipt('./input/hinode_localstray.ipt')
  iptorig=ipt
  
  nel=21
  bfiel=findgen(nel)/(nel-1)*1000+50
  noise=1e-3
  
  nr=10
  if keyword_set(norun) eq 0 or n_elements(resarr) eq 0 then begin
    for i=0,nel-1 do begin
      if n_elements(old_ipt) ne 0 then dummy=temporary(old_ipt)
      ipt=iptorig
      ipt.ncalls=00 & ipt.ncalls(0)=00 ;# of iterations
      ipt.synth=1
      ipt.atm(0).par.b=bfiel(i)
                                ;create synthetic profile
      helix,struct_ipt=ipt,fitprof=fitprof
                                ;this is our 'observed' profile
      profile=fitprof
      save,file='./profile_archive/synth.sav',profile
      ipt=iptorig
      ipt.ncalls=00 & ipt.ncalls(0)=400 ;# of iterations
      ipt.observation='synth.sav'
      ipt.dir.profile='./profile_archive/'
      ipt.synth=0         ;no synthesis! we want to make an inversion.
      ipt.noise=noise        ;add realistic noise level to observations
      print,i,'/'+n2s(nel),': BFIEL=',bfiel(i)
      for ir=0,nr-1 do begin
        helix,struct_ipt=ipt,result=result,fitprof=fit
        if i eq 0 and ir eq 0 then resarr=replicate(result,nel,nr)
        resarr(i,ir)=result
      endfor
    endfor
  endif
   
  noisestr=n2s(noise,format='(f15.4)')
  
  filename='fehin_test_noise'+noisestr+'.ps'
  psset,ps=ps,file=filename
  
  par=['B','INC','AZI','ETAZERO','SZERO','SGRAD','DOPP','GDAMP','FF','FITNESS']
  tn=tag_names(resarr.fit.atm.par)
  userlct
  !p.multi=[0,1,n_elements(par)<5]
  for i=0,n_elements(par)-1 do begin
    vorig=(fltarr(nr)+1) ## bfiel
    if par(i) ne 'FITNESS' then begin
      it=where(tn eq par(i))
      value=resarr.fit.atm(0).par.(it)
    endif else begin
      value=resarr.fit.fitness
    endelse
    yrg=minmaxp(value)
    yrg=yrg*[.95,1.05]
    plot,xrange=minmaxp(vorig),yrange=yrg,[0,1],/nodata, $
      xtitle=par(i)+' (orig)',ytitle=par(i)+' (fit)'
    plots,psym=4,vorig,value
  endfor
  xyouts,alignment=0.5,/normal,.5,1, $
    '!CNoise = '+noisestr
  xyouts,0,0,filename
  psset,/close
  
end
