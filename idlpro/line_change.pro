
pro line_change
  common greek_letters
  
  par=['vlos','vdopp','bfiel']
;  rg=[[-20000.,+20000],[0.01,1.5],[5.,.5],[0.,5000]]
  step0=4
  ipt=read_ipt('line_movie.ipt')
  
  for im=0,n_elements(par)-1 do begin
    case par(im) of
      'vlos': begin
        paridl='ipt.atm.par.vlos'
        val=[0,-10000.,10000]
        title='Geschwindigkeit'
        unit='km/s'
        scale=1000.
        off=0.
        prof='I'
      end
      'vdopp': begin
        paridl='ipt.atm.par.dopp'
        val=[0.3,0.53,0.07]
        title='Temperatur'
        unit='K'
        scale=1/5000.
        off=3500.
        prof='I'
      end
      'bfiel': begin
        paridl='ipt.atm.par.b'
        val=[2000.,10000.,000.]
        title='Magnetfeld'
        unit='nT'
        scale=10.
        off=0.
        prof='V'
        step0=5
      end
      else: stop
    endcase
    
    ps=1
    file='line_change_'+par(im)
    psset,ps=ps,file=file+'.ps',/no_x,size=[12,20]
    if keyword_set(ps) then device,xoff=0,yoff=0
  userlct
    
    for iv=0,n_elements(val)-2 do begin
      ipt=read_ipt('line_movie.ipt')
      
      steps=fix(abs(val(iv+1)-val(iv))/abs(val(1)-val(0))*step0+1)
      cval=findgen(steps)/(steps-1)*(val(iv+1)-val(iv))+val(iv)

      for ip=0,steps-1 do begin
;        if iv ne 0 or ip ne 0 then erase
        dummy=execute(paridl+'=cval(ip)')
        
;        help,/st,ipt.atm.par    
        ipt.verbose=0 & ipt.display_profile=0
        helix,fitprof=fitprof,struct_ipt=ipt,result=result
;        help,/st,result.fit.atm.par
        if iv eq 0 and ip eq 0 then prof0=fitprof
  
        !p.charsize=1.25
        !p.font=0
;        tit=title
;         if ip eq 0 then begin
;           plot_profiles,prof0,fitprof,title=tit,iquv='I', $
;             lthick=[3,5],color=[3,1],lstyle=[1,0], $
;             fraction=[1],yrange=[0,1]
;         endif else oplot,color=1,thick=5,fitprof.wl,fitprof.i-ip*0.05
        tit='Intensität'
        case prof of 
          'I': begin
            yrg=[-4.4,1.1]
            prf=fitprof.i
          end
          'V': begin
            yrg=[-4.4,0.6]
            prf=fitprof.v
            tit='Polarisationsgrad'
          end
        endcase
        
        if ip eq 0 and iv eq 0 then begin
          plot,fitprof.wl,prf,/xst,yrange=yrg,xthick=5,ythick=5,/nodata, $
          xtitle='Wellenlänge',ytitle=tit,/yst, $
            ytickname=strarr(32)+' ',yticklen=0.,xtickname=strarr(32)+' '
          oplot,color=3,thick=3,fitprof.wl,prf
          oplot,linestyle=1,[0,0]+10827.09,!y.crange
          off=0.30
        endif
        off=off+0.30
        oplot,color=1,thick=4,fitprof.wl,prf-off
        if prof ne 'I' then oplot,linestyle=1,!x.crange,[0,0]-off
        
;        !p.font=0
;        xyouts,.94,.65,alignment=1,/normal,charsize=2, $
;          title+'!C'+string(format='(f8.2)',cval(ip)/scale+off)+' '+unit
      endfor
    endfor

  
  
    psset,/close
    if keyword_set(ps) then begin
      spawn,'psselect -p1 '+file+'.ps >'+file+'_p1.ps'
      spawn,'/opt/imagemagick-4.2.9/bin/convert -density 72x72 '+file+ $
        '_p1.ps '+file+'_p1.gif'
      spawn,'/opt/imagemagick-4.2.9/bin/convert -density 72x72 -delay 50 ' + $
        file+'.ps '+file+'.gif'
      print,'Wrote Gif-Animation '+file+'.gif'
    endif
 
  endfor
  stop
end
