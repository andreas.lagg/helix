pro remove_ambi,id
  common ambiwg,ambwg,amhelp
  common wgst,wgst
  common ambig_start,ambig
  
  widget_control,ambwg.base.id,/destroy
  
  wgst.click.val=amhelp.oldclick
  widget_control,wgst.click.id,sensitive=1,set_droplist_select=wgst.click.val
  ambig.xs=0 & ambig.ys=0 & ambig.zero=0 & ambig.n=0
end

pro ambig_count
  common ambiwg,ambwg,amhelp
  common ambig_start,ambig
  
  widget_control,ambwg.info.id,set_value=n2s(ambig.n)
end

pro ambwg_event,event
  common ambiwg,ambwg,amhelp
  common ambig_start,ambig
  common wgst,wgst
  common store,store
  common param, param
  common pikaia
  
  widget_control,event.id,get_uvalue=uval
  case uval of
    'zero180': ambwg.zero180.val=event.value
    'mapts': begin
      ambwg.mapts.val=event.index
      case event.index of
        0: begin
          ambwg.box.val=[10,10] & ambwg.nperm.val=3
        end
        1: begin
          ambwg.box.val=[2048,4] & ambwg.nperm.val=3
        end
      endcase
      for i=0,1 do $
        widget_control,ambwg.box(i).id,set_value=fix(ambwg.box(i).val)
      widget_control,ambwg.nperm.id,set_value=ambwg.nperm.val
    end
    'boxx': ambwg.box(0).val=event.value
    'boxy': ambwg.box(1).val=event.value
    'nperm': begin
      ambwg.nperm.val=event.value
      if ambwg.nperm.val ge 10 then begin
        ambwg.nperm.val=10
        widget_control,ambwg.nperm.id,set_value=ambwg.nperm.val
      endif
    end
    'control': begin
      case event.value of
        'find': begin
          wgst.ambismooth.val=1
          if n_elements(store) ne 0 then store.ambismooth=0
          ishow=where(param.show)
          display_data,/no_sav,/widget,parshow=ishow
          wgst.ambismooth.val=0
          store.ambismooth=0
        end
        'reset': begin
          ambig.xs=0 & ambig.ys=0 & ambig.zero=0 & ambig.n=0
          ambig.flipmap=0 & ambig.nx=0 & ambig.ny=0
          store.ambiguity=-1
          ambig_count
        end
        'load': begin
          pushd,'.' & cd,'./sav'
          fsav=pikaia_result.input.observation+'_flipmap.sav'
          fsav=dialog_pickfile(dialog_parent=wgst.base.id,file=fsav, $
                               title='Save FlipMap',/read,/must_exist)
          popd
          if fsav ne '' then begin
            print,'Loading FlipMap from '+fsav
            restore,fsav
            ambig_count
            store.ambiguity=-1
            ishow=where(param.show)
            display_data,/no_sav,/widget,parshow=ishow
          endif
        end
        'save': begin
          fsav=pikaia_result.input.observation+'_flipmap.sav'
          pushd,'.' & cd,'./sav'
          fsav=dialog_pickfile(dialog_parent=wgst.base.id,file=fsav, $
                               title='Save FlipMap',/write, $
                               /overwrite_prompt)
          popd
          if fsav ne '' then begin
            save,/xdr,/compress,file=fsav,ambig
            print,'FlipMap written to '+fsav
          endif else message,/cont,'No FlipMap written.'
        end
        'cancel': remove_ambi,ambwg.base.id
      endcase
    end
    else: begin
      help,/st,event
      print,uval
    end
  endcase
  
end

pro ambig_add,xval,yval
  common ambiwg,ambwg,amhelp
  common ambig_start,ambig
  
  ambig.xs(ambig.n)=xval
  ambig.ys(ambig.n)=yval
  ambig.zero(ambig.n)=ambwg.zero180.val
  ambig.n=ambig.n+1
  ambig_count
end

pro ambig_label,n=n
  common wgst,wgst
  common ambig_start,ambig
  
  if n_elements(n) eq 1 then n0=[n,n] else n0=[0,ambig.n-1]
  amx=pix2mm(ambig.xs,x=fix(wgst.p2mm.val))
  amy=pix2mm(ambig.ys,y=fix(wgst.p2mm.val))
  
                                ;only print on screen, not on ps
  if !d.name ne 'PS' then begin
    for in=n0(0),n0(1) do begin
      xy=dev2dat(/reverse,[amx(in),amy(in)],/all)
      for ix=0,(size(xy))(1)-1 do for iy=0,(size(xy))(2)-1 do begin
        for ii=0,1 do $
          xyouts,/device,xy(ix,iy,0)+ii,xy(ix,iy,1)+ii,alignment=0.5, $
          (['0','180','90'])(ambig.zero(in)),color=([255,0])(ii)
      endfor
    endfor
  endif
end
                    
pro ambi_widget
  common wgst,wgst
  common ambiwg,ambwg,amhelp
  common ambig_start,ambig
  
  if n_elements(ambwg) eq 0 then begin
    subst={id:0l,val:0.,str:''}
    ambwg={base:subst,zero180:subst,control:subst,info:subst, $
           box:replicate(subst,2),nperm:subst,mapts:subst}
    ambwg.box.val=[10,10]
    ambwg.nperm.val=3
  endif
  
  if n_elements(ambig) eq 0 then $
    ambig={xs:intarr(1024),ys:intarr(1024),zero:intarr(1024),n:0, $
           flipmap:bytarr(2048,2048),nx:0,ny:0}
  
  wgst.click.val=7
  widget_control,wgst.click.id,sensitive=0,set_droplist_select=wgst.click.val
  
  
  ambwg.base.id=widget_base(title='Smooth Ambiguity Removal',/col, $
                            group_leader=wgst.base.id,/float, $
                            kill_notify='remove_ambi')  
  
  
  disp=widget_base(ambwg.base.id,col=1)
  sub=widget_base(disp,col=1,space=0,/frame)
  lab=widget_label(sub,value='Calculate Smooth Ambiguity Solution')
  
  descr=['','Please use the cursor on the maps to define pixels where the', $
         'solution is set to 0�, 180� or 90� (depending on the setting', $
         'of the flag below).','', $
         'When finished, press the ''Find Smooth Ambiguity'' button.']
  for i=0,n_elements(descr)-1 do $
    lab=widget_label(sub,/align_left,value=descr(i))
  
  sss=widget_base(disp,col=1,/frame)
  sub=widget_base(sss,row=1)
  lab=widget_label(sub,value='Click on Plot defines solution: ')
  ambwg.zero180.id=cw_bgroup(sub,uvalue='zero180',row=1,/exclusive, $
                             set_value=ambwg.zero180.val,['0','180','90'])
  sub=widget_base(sss,row=1)
  ambwg.mapts.id=widget_droplist(sub,uvalue='mapts', $
                                 value=['Map-Mode','Time-Series Mode'])
  widget_control,ambwg.mapts.id,set_droplist_select=ambwg.mapts.val

  sub=widget_base(sss,row=1)
  ambwg.box(0).id=cw_field(sub,/all_events,xsize=4,/int, $
                           value=fix(ambwg.box(0).val),uvalue='boxx', $
                           title='Box-Size:')
  ambwg.box(1).id=cw_field(sub,/all_events,xsize=4,/int, $
                           value=fix(ambwg.box(1).val),uvalue='boxy',title=' ')
  ambwg.nperm.id=cw_field(sub,/all_events,xsize=3,/int, $
                         value=fix(ambwg.nperm.val),uvalue='nperm', $
                         title='# permut. (<10):')
  
  
  
  sub=widget_base(sss,row=1)
  lab=widget_label(sub,value='Number of defined starting pixels: ')
  ambwg.info.id=widget_label(sub,value=+n2s(ambig.n))
  
  ambwg.control.id=cw_bgroup(ambwg.base.id, $
                             ['Find Smooth Ambiguity','Reset', $
                              'Load Flipmap','Save Flipmap','Cancel'], $
                             uvalue='control',row=1, $
                             button_uvalue=['find','reset','load', $
                                            'save','cancel'])
  
  widget_control,ambwg.base.id,/realize
  xmanager,'ambwg',ambwg.base.id,no_block=1
end


pro ambismooth_wg
  common wgst,wgst
  common ambiwg,ambwg,amhelp
  
  if n_elements(ambwg) ne 0 then begin
    widget_control,ambwg.base.id,iconify=0,bad_id=bad_id
    print,'ambismooth widget already open ',bad_id
    if bad_id eq 0 then return
  endif
  
  if n_elements(amhelp) eq 0 then $
    amhelp={oldclick:0}
  wgst.ambiguity.val=4
  if wgst.ambiguity.id ne 0 then $
    widget_control,wgst.ambiguity.id,set_value=wgst.ambiguity.val
  amhelp.oldclick=wgst.click.val
  ambi_widget
  
end
