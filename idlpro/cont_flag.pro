function cont_flag,fit
  
  sz=size(fit)
  ncmp=n_elements(fit(0).atm)
  if sz(0) eq 1 then begin
    sz2=sz(1)
    sz3=1
;   ncmp=1
  endif else begin
    sz2=sz(1)
    sz3=sz(2)
;   ncmp=sz(1)
  endelse
  contarr=bytarr(sz2,sz3)
  
  contarr=dist(sz2,sz3)
  return,contarr
  
  
  recalc_ff=0
  if total(fit.atm.fit.ff) eq 0 then recalc_ff=1 
  ffimg=fltarr(ncmp,sz2,sz3)
  for ic=0,ncmp-1 do begin
    fftmp=fit.atm(ic).par.ff
    if recalc_ff then begin
      if ic eq 0 then print,'Calculating FF from SGRAD'
      fftmp=get_ff((fit.atm.par.sgrad),ic,/abs)
    endif
    ffimg(ic,*,*)=fftmp
  endfor

                                ;set contour flag to 1 where second
                                ;component velocity is larger than
                                ;10km/s and FF >0.2
  iu=1 < (ncmp-1)  ;use component 2 if available, otherwise use comp 1
  vlos=reform(fit.atm(iu).par.vlos)
  inc=reform(fit.atm(iu).par.inc)
  sgrad=reform(fit.atm(iu).par.sgrad)
  ffiu=reform(ffimg(iu,*,*))
; contarr=(vlos ge 1.e4 and ffiu gt 0.3 and sgrad gt 1. and $
;          (inc lt 65 or inc gt 115))
  contarr=(vlos ge 1.5e4 and ffiu gt 0.25  and sgrad gt 1.)
  
  
  
  return,contarr
end
