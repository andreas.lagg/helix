pro hanle_zeeman,ps=ps,new=new
   common profile,profile
   @common_cppar
   common resh1,hanle_no,hanle_yes
   
   idl53,prog_dir='./epd_dps_prog/' 

  ipt=['hanle_zeeman_nohanle','hanle_zeeman_withhanle']
  list=[68,14]
  list=[52,32]
  list=[53,26]
  list=[77,51]
  list=[58,28]
  
  if n_elements(hanle_no) eq 0 or keyword_set(new) then $
    helix,ipt=ipt(0)+'.ipt',result=hanle_no,list=list
  if n_elements(hanle_yes) eq 0 or keyword_set(new) then $
    helix,ipt=ipt(1)+'.ipt',result=hanle_yes,list=list
  
  
  
  xpvec=list(0) & ypvec=list(1)
  if hanle_yes.input.average then begin
    xpvec=(list(0)+indgen(hanle_yes.input.stepx))
    ypvec=(list(1)+indgen(hanle_yes.input.stepy))
  endif
  obs=get_profile(profile=profile, $
                  ix=xpvec,iy=ypvec,icont=icont, $
                    wl_range=hanle_yes.input.wl_range)
  
  voigt=strupcase(hanle_yes.input.profile) eq 'VOIGT'
  magopt=hanle_yes.input.magopt(0)
  hanle_azi=hanle_yes.input.hanle_azi
  norm_stokes_val=hanle_yes.input.norm_Stokes_val
  old_norm=hanle_yes.input.old_norm
  old_voigt=hanle_yes.input.old_voigt
  use_geff=hanle_yes.input.use_geff(0)
  use_pb=hanle_yes.input.use_pb(0)
  modeval=hanle_yes.input.modeval(0)
  yesprof=compute_profile(/init,line=hanle_yes.line,atm=hanle_yes.fit.atm, $
                        obs_par=hanle_yes.input.obs_par(0),wl=obs.wl)
  
  voigt=strupcase(hanle_no.input.profile) eq 'VOIGT'
  hanle_azi=hanle_no.input.hanle_azi
  magopt=hanle_no.input.magopt(0)
  norm_stokes_val=hanle_no.input.norm_Stokes_val
  old_norm=hanle_no.input.old_norm
  old_voigt=hanle_no.input.old_voigt
  use_geff=hanle_no.input.use_geff(0)
  use_pb=hanle_no.input.use_pb(0)
  modeval=hanle_no.input.modeval(0)
  noprof=compute_profile(/init,line=hanle_no.line,atm=hanle_no.fit.atm, $
                        obs_par=hanle_no.input.obs_par(0),wl=obs.wl)
  
  save,/xdr,/compress,file='./sav/hanle_zeeman.sav',hanle_no,hanle_yes
  
  !p.font=-1 & set_plot,'X'
  if keyword_set(ps) then begin
    psout=ps_widget(default=[0,0,1,0,8,0,0],size=[20.,22],psname=psn, $
                    file='~/work/aa_2003/figures/hanle_zeeman.eps',/no_x)
    !p.font=0    
  endif
  
  plot_profiles,obs,noprof,yesprof,line=hanle_yes.line,/iic, $
    color=[9,9,9],lstyle=[0,1,2],psym=[0,1,4],symsize=1.,/bw;,iquv='iquv'
  
  if !d.name eq 'PS' then begin
    device,/close
    print,'Created PS-file: '+psn
  endif
  
  !p.font=-1 & set_plot,'X'
  
end
