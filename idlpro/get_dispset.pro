;generate info string from display settings
function get_dispset
  common diswg,diswg
  
  if n_elements(diswg) eq 0 then return,''
  
                                ;read quv-flag
  retval=''
  if max(diswg.quv_flag.val) then begin
    str=n2s(diswg.quv_val.val,format='(e15.1)')+' < ( '
    if diswg.quv_flag.val(0) eq 1 then begin
      str=str+'(|Q|'
    endif
    if diswg.quv_flag.val(1) eq 1 then begin
      if diswg.quv_flag.val(0) eq 1 then $
        str=str+([' OR',' AND'])(diswg.quv_and(0).val)+' |U|' $
      else str=str+'(|U|'
    endif
    if max(diswg.quv_flag.val(0:1)) eq 1 then str=str+')'
    if diswg.quv_flag.val(2) eq 1 then begin
      if max(diswg.quv_flag.val(0:1)) eq 1 then $
        str=str+([' OR',' AND'])(diswg.quv_and(1).val)
      str=str+' |V|'
    endif
    str=str+' ) < '+n2s(diswg.quv_max.val,format='(e15.1)')
    retval=[retval,str]
  endif
  if n_elements(retval) ge 2 then retval=retval(1:*)
  return,retval
end
