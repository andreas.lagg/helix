function read_hinode,datnam,x=xveco,y=yveco,scan=scan,error=error, $
                     norm_cont=norm_cont,lsprof=lsprof
  common hinodedat,hinfo
  common input,ipt
  
  error=1
  
  xvec=xveco
  if n_elements(xvec) eq 1 then xvec=[xvec,xvec]
  yvec=yveco
  if n_elements(yvec) eq 1 then yvec=[yvec,yvec]
  
                                ;check if directory-mode or file mask
  fi=file_info(datnam)
  if fi.directory eq 1 then begin
    hfiles=file_search(datnam+'/*SP4*.fits',count=fcnt) 
    if fcnt eq 0 then hfiles=file_search(datnam+'/*SP3*.fits',count=fcnt) 
  endif else begin
                                ;check if mask contains '.fits', if
                                ;not, addi it
    if strmid(datnam,strlen(datnam)-5,strlen(datnam)) ne '.fits' then $
      datnam=datnam+'.fits'
    hfiles=file_search(datnam,count=fcnt)
  endelse
  
  if fcnt eq 0 then begin
    message,'No Hinode Files found in '+datnam
    reset
  endif
  
  reread=1
  if n_elements(hinfo) ne 0 then begin
    reread=fcnt ne hinfo.n
    if reread eq 0 then reread=min(hfiles eq hinfo.files) ne 1
  endif
  if reread then begin
    scannr=intarr(fcnt)
    tscn=0
    if ipt.verbose ge 1 then $
      print,'Reading Header of '+n2s(fcnt)+' Hinode files.'
    for i=0,fcnt-1 do begin
      dummy=readfits_ssw(hfiles(i),header,/nodata,silent=1)
      hst=fits_header2st(header)      
      if i eq 0 then fheader=hst else begin
        new=fheader(0)
        struct_assign,hst,new
        fheader=[fheader,new]
        if fheader(i-1).slitindx ge fheader(i).slitindx then tscn=tscn+1
      endelse
      scannr(i)=tscn
      if fix((i-1.)/fcnt*75) ne fix((i-0.)/fcnt*75) then $
        print,'.',format='(a,$)'
    endfor
    hinfo={n:fcnt,files:hfiles,header:fheader,scannr:scannr,datnam:datnam, $
           fixed_slit:0}
  endif
  if ipt.verbose ge 2 then begin  
    print,' Done.'
    print,' Found ',n2s(hinfo.n),' Hinode FITS files',' slit pos ', $
      n2s(min(hinfo.header(0:hinfo.n-1).slitindx)),'-', $
      n2s(max(hinfo.header(0:hinfo.n-1).slitindx)), $
      ' and scan# ',n2s(min(hinfo.scannr)),'-',n2s(max(hinfo.scannr))
    
                                ;check if Hinode data are fixed slit
                                ;scan: in that case use the scannr as
                                ;x-position
  endif
  if hinfo.n ge 2 then $
    if (min(hinfo.header(0:hinfo.n-1).slitindx) eq $
        max(hinfo.header(0:hinfo.n-1).slitindx)) then begin
    if ipt.verbose ge 1 then $
      print,'Hinode SP data is ''Fixed Slit Scan''. Using SCANNR for XPOS'
    hinfo.fixed_slit=1
  endif
  
                                ;look for xpos and scan-nummer
  if hinfo.fixed_slit eq 0 then begin
    inidx=where(hinfo.scannr eq ipt.obs_par.hin_scannr and $
                (hinfo.header.slitindx ge xvec(0) and $
                 hinfo.header.slitindx le xvec(1)))
  endif else begin
    inidx=where(hinfo.scannr ge xvec(0) and $
                hinfo.scannr le xvec(1))
  endelse
  if inidx(0) eq -1 then begin
    error=1
    print,'Scan# '+n2s(ipt.obs_par.hin_scannr)+ $
      ' and xpos=('+n2s(xvec(0))+'-'+n2s(xvec(1))+') not found in '+datnam
    print,'Available positions: x=', $
      n2s(min(hinfo.header(0:hinfo.n-1).slitindx)),'-', $
      n2s(max(hinfo.header(0:hinfo.n-1).slitindx)),', y=0-', $
      n2s(hinfo.header(0).naxis2-1), $
      ' and scan# ',n2s(min(hinfo.scannr)),'-',n2s(max(hinfo.scannr))
    return,-1
    if keyword_set(continue_error) eq 0 then begin
      reset
    endif else return,-1
  endif
  
  for ii=0,n_elements(inidx)-1 do begin
    hf=hinfo.files(inidx(ii))
    data=readfits_ssw(hf,hdr,/silent)
    data=hinode_corr(data,hdr)
;    data=hinode_corr(data,hinfo.header)
    sz=size(data)
    nwl=sz(1)
    
    if sz(2) le max(yvec) then begin
      print,'Ypos=('+n2s(yvec(0))+'-'+n2s(yvec(1))+') not found in '+datnam
      error=1
      reset
    endif
    i=float(data(*,yvec(0):yvec(1),0))
    if (size(i))(0) eq 2 then i=total(i,2)
    q=float(data(*,yvec(0):yvec(1),1))
    if (size(q))(0) eq 2 then q=total(q,2)
    u=float(data(*,yvec(0):yvec(1),2))
    if (size(u))(0) eq 2 then u=total(u,2)
    v=float(data(*,yvec(0):yvec(1),3))
    if (size(v))(0) eq 2 then v=total(v,2)
                                ;check if ccx is present
    fi=file_info(hf+'.ccx')
    if fi.exists then begin
      read_ccx,hf+'.ccx',wl_disp=wl_disp,norm_cont=norm_cont, $
        wl_off=wl_off,icont=icimg,error=xerr,wl_vec=wl_vec
      ic=total(icimg(yvec(0):yvec(1)))
    endif else begin
      if ipt.verbose ge 1 and ii eq 0 then $
        message,/cont,'No ccx file found. Using simple cont-level'
      wl_vec=dindgen(nwl)*ipt.wl_disp+ipt.wl_off
      ic=get_simple_cont(i)
    endelse
    if ii eq 0 then begin
      observation={ic:ic,wl:wl_vec,i:i,q:q,u:u,v:v}
    endif else begin
                                ;check if wl is consistent
      if max(abs(wl_vec-observation.wl)) gt 1e-4 then begin
        print,'WL-Vector for pixels x=',xvec,' y=',yvec,' is not matching!'
        error=1
        reset
      endif
      observation.ic=observation.ic+ic
      observation.i=observation.i+i
      observation.q=observation.q+q
      observation.u=observation.u+u
      observation.v=observation.v+v
    endelse
  endfor
  nxy=(max(xvec)-min(xvec)+1)*(max(yvec)-min(yvec)+1)
  observation.ic=observation.ic/nxy
  observation.i=observation.i/nxy/observation.ic
  observation.q=observation.q/nxy/observation.ic
  observation.u=observation.u/nxy/observation.ic
  observation.v=observation.v/nxy/observation.ic
  
  lsprof=get_lsprof(datnam,xvec,yvec,observation.wl, $
                    n_elements(observation.wl), $
                    ipt=ipt,mode=2)

  error=0
  return,observation
  
end
