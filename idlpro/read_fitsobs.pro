function read_fitsobs,x=xpvec,y=ypvec,error=error,file=file, $
                      ipt=ipt,silent=silent,norm_cont=norm_cont,lsprof=lsprof
  
  error=0
  common oldccx,oldccx,icont,wlccx,icontccx,xerr,old_norm
  if n_elements(oldccx) eq 0 then oldccx=''
  
  fuse=file
;  if strmid(file,strlen(file)-2,strlen(file)) ne 'cc' then fuse=file+'cc'
  
  fqm=add_qm(fuse)
  
  xy_fits,fqm,silent=silent,x=xpvec,y=ypvec,i=i,q=q,u=u,v=v, $
    reset=oldccx ne fqm,/average,struct=xyfst

  if error ne 0 then return,-1
                                ;check for aux-file
  if n_elements(old_norm) eq 0 then old_norm=-1
  if oldccx ne fqm or old_norm ne norm_cont then begin
    read_ccx,fqm+'x',wl_disp=wl_disp,norm_cont=norm_cont, $
      wl_off=wl_off,icont=icimg,error=xerr,wl_vec=wl_vec
    old_norm=norm_cont
    if xerr eq 0 then begin
      if keyword_set(init) and ipt.verbose ge 1 then begin
        print,'AUX (ccx) file: '+fqm+'x'
        print,'Using continuum level calculation '+ $
          'from AUX (ccx) file'
      endif
;      wlccx=findgen(n_elements(i))*wl_disp+wl_off
      wlccx=wl_vec
      icontccx=icimg
    endif else begin
      if ipt.verbose ge 1 and keyword_set(init) then $
        message,/cont,'No Aux (ccx) file found. '+ $
        'Using simple continuum level calculation'
       if ipt.wl_off lt 1e-5 then begin
        message,/cont, $
          'WL-Calibration must be specified in the' + $
          ' input file! Use keywords WL_OFF and WL_DISP'
        reset
;         ipt.wl_off=0
;         ipt.wl_bin=1.
      endif ;else begin
;        wlccx=findgen(n_elements(i))*ipt.wl_disp+ipt.wl_off
;      endelse
      xerr=1
    endelse
    oldccx=fqm
  endif
  if xerr eq 0 then begin
    if n_elements(xpvec) eq 1 then xpvec=xpvec(0)+[0,0]
    if n_elements(ypvec) eq 1 then ypvec=ypvec(0)+[0,0]
    sz=size(icontccx)
    tic=icontccx(xpvec(0)<(sz(1)-1):xpvec(1)<(sz(1)-1), $
                 ypvec(0)<(sz(2)-1):ypvec(1)<(sz(2)-1))
    tic=total(tic)/n_elements(tic)
  endif else begin
    tic=get_simple_cont(i)
    wlccx=findgen(n_elements(i))*ipt.wl_disp+ipt.wl_off
  endelse

  observation={ic:0.,wl:i*0d,i:i/tic(0),q:q/tic(0),u:u/tic(0),v:v/tic(0)} 
  observation.ic=tic(0)
  observation.wl=wlccx
  nwl=n_elements(observation.wl)
  
  lsprof=get_lsprof(fqm,xpvec,ypvec,observation.wl,nwl,ipt=ipt,mode=1) 

  return,observation
end
