function def_line,line=line,empty=empty,verbose=verbose
  
  lst=def_linest()
  if keyword_set(empty) then return,lst

  sl=strlowcase(line)
  
  lst.straypol_use=0
  for i=0,n_elements(sl)-1 do if sl(i) ne '' then begin
    case sl(i) of
      'he': begin
        line=replicate(lst,3)   ;he-triplet
        ids=[ 'HeI 10829.1', 'HeI 10830.2', 'HeI 10830.3']
        for il=0,2 do line(il).id(0:strlen(ids(il))-1)=byte(ids(il))
        line.wl=[ 10829.0911d , 10830.2501d , 10830.3397d ]
        line.f= [ 0.111, 0.333, 0.556]
        line.geff=[ 2.0, 1.75, 1.25]
        line.jl=[1,1,1]
        line.ju=[0,1,2]

        ;CUANTEN NUMBERS FOR HE *DAVID
        line.Sl=[1,1,1] & line.Su=[1,1,1]
        line.Ll=[0,0,0] & line.Lu=[1,1,1]
        ;EXECUTE ROUTINE CUANTEN FOR MULTIPLE SPLITTING *DAVID
        line=create_nc(line,verbose=verbose)
        ;END DAVID

        line.mass=4
        
                                ;parameters for correction of
                                ;straypolarization
        line.fit.straypol_amp=0 ;fit flag for straypol
        line.par.straypol_amp=0 ;gauss amplitude for straypol correction
        line(0).straypol_par.sign=-1 ;parameter defining sign of correction
        line(1:2).straypol_par.sign=+1
        line.scale.straypol_amp.min=0;-0.02
        line.scale.straypol_amp.max=+.004
        
        line.fit.straypol_eta0=0 ;fit flag for straypol
        line.par.straypol_eta0=0 ;gauss amplitude for straypol correction
        line.scale.straypol_eta0.min=0;-0.02
;       line.scale.straypol_eta0.max=+.008
        line.scale.straypol_eta0.max=+.100
        line.straypol_use=1
        
        for il=0,2 do line(il)=def_pb(line(il))
      end
      'si': begin
        line=replicate(lst,1)   ;silicon line
        ids='SiI 10827.1'
        line.id(0:strlen(ids)-1)=byte(ids)
        line.wl=10827.088d
        line.f=10.^0.220
        line.f=1.
        line.geff=1.5
        line.mass=28
      end
      'ca': begin
        line=replicate(lst,1)   ;calcium line
        ids='CaI 10829.3'
        line.id(0:strlen(ids)-1)=byte(ids)
        line.wl=10829.268d 
        line.f=10.^(-0.944)
        line.f=1.
        line.geff=1.0
        line.mass=40
      end
      'fe': begin
        line=replicate(lst,2)   ;iron line
        ids=['FeI 6301.5','FeI 6302.5']
        for il=0,1 do line(il).id(0:strlen(ids(il))-1)=byte(ids(il))
        line.wl=[6301.5012d ,6302.4936d]
        line.f=10^([-0.718,-1.235])
        line.f=line.f/total(line.f)
        line.geff=[1.5,2.5]
        line.mass=56
      end
      else: message,'undefined line.'
    endcase
    
;    line.f=line.f/total(line.f) ;normalize f
    
    if n_elements(retline) eq 0 then retline=line else retline=[retline,line]
  endif
  
  return,retline
end
