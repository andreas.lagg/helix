;reads in atmospheric file and returns synthetic profile(s)
;example:
;prof=atm_profile(atm='atm_archive/atm_18may05.016-01cc_free/x0056/x0056y0192_atm.dat',ipt='atm_archive/atm_18may05.016-01cc_free/input.ipt')
;wlvec as defined in input file is used, unless specified with the wl keyword
;profile 0 contains the full profile, profile 1-ncomp the individual components
function atm_profile,atm=atm,ipt=ipt,wl=wlvec
   @common_cppar
 
  if n_elements(atm) eq 0 then message,'specify atmospheric data file with keyword atm=''x0000y0000_atm.dat'''
  if n_elements(ipt) eq 0 then message,'specify input file name or input structure with keyword ipt'
  
  if size(ipt,/type) eq 7 then iptst=read_ipt(ipt) else iptst=ipt
  
  res=read_atm(file=atm,header=header,init=1,ipt=iptst)
  
  if n_elements(wlvec) eq 0 then begin
    nwl=iptst.wl_num
    wlvec=findgen(nwl)/(nwl-1)* $
      (iptst.wl_range(1)-iptst.wl_range(0))+iptst.wl_range(0)
  endif
  
  
  conv=read_conv(iptst.wl_range,iptst.conv_func,iptst.conv_nwl, $
                 iptst.conv_mode,iptst.prefilter, $
                 iptst.conv_wljitter,wlvec,iptst.verbose)
  cnwl=conv.nwl
  if cnwl ge 1 then begin
    convval=conv.val(0:cnwl-1)
    compwl=conv.wl(0:cnwl-1)
  endif
                                ;prefilter_wlerr set to 0.
  prefilter=read_prefilter(iptst.prefilter,0.0, $
                           wlvec,iptst.verbose)
  
  
  all_lines=iptst.line
  tgt=res.fit.linepar
  struct_assign,all_lines.par,tgt
  obs_par=(iptst.obs_par)(0)
          magopt=fix(ipt.magopt(0))
          hanle_azi=ipt.hanle_azi
          norm_stokes_val=ipt.norm_Stokes_val
          old_norm=fix(ipt.old_norm)
          old_voigt=fix(ipt.old_voigt)
          use_geff=fix(ipt.use_geff(0))
          use_pb=fix(ipt.use_pb(0))
          pb_method=fix(ipt.pb_method(0))
          voigt=fix(ipt.profile eq 'voigt')
          modeval=fix(ipt.modeval)
  
  for iia=-1,iptst.ncomp-1 do begin
    ic=iia
    if iia eq -1 then dummy=temporary(ic)
    cprof= $
      compute_profile(/init, $
                      atm=res.fit.atm,line=all_lines,wl=wlvec,obs_par=obs_par, $
                      blend=res.fit.blend,gen=res.fit.gen, $
                      convval=convval,doconv=conv.doconv,conv_mode=conv.mode, $
                      prefilterval=prefilter.val, $
                      doprefilter=prefilter.doprefilter,$
                      ret_wlidx=0,nret_wlidx=-1,normff=0,comp=ic)
    if iia eq -1 then profile=cprof else profile=[profile,cprof]
  endfor
  

  return,profile
end
