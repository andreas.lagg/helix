;combione two sav files with different number of components
pro combine_sav,sav1,sav2,savout
  
  if n_params() ne 3 then begin
    print,'combine_sav - routine to combine sav files of different pikaia'
    print,'              runs from the same data set.'
    print,'Tries to combine sav files with different # of components'
    print,'Usage:'
    print,'combine_sav,''sav/30sep11.002lin-01cc_hesi_v03a.pikaia.sav'',''sav/30sep11.002lin-01cc_hesi_v05a.pikaia.sav'',''sav/30sep11.002lin-01cc_hesi_1+2comp.pikaia.sav'''
    reset
  endif
  
  restore,/v,sav1
  pr1=pikaia_result
  restore,/v,sav2
  pr2=pikaia_result
                                ;get number of components
  comp=[pr1.input.ncomp,pr2.input.ncomp]

  if min(pr1.input.line.id eq pr2.input.line.id) eq 0 then begin
    print,'Only sav files with same spectral lines can be combined'
    reset
  endif
  
                                ;take thesav file with larger number
                                ;of components ase basis
  if comp(1) gt comp(0) then begin
    ipt=pr2.input
  endif else ipt=pr1.input
  
                                ;create an empty structure
  sz1=size(pr1.fit)
  sz2=size(pr2.fit)
  xx1=minmax(pr1.fit.x) & yy1=minmax(pr1.fit.y)
  xx2=minmax(pr2.fit.x) & yy2=minmax(pr2.fit.y)
  xx=minmax([xx1,xx2])
  yy=minmax([yy1,yy2])
  nx=xx(1)-xx(0)+1
  ny=yy(1)-yy(0)+1
  pr=result_struc(nx=nx,ny=ny,ipt=ipt,header=pr1.header)
  pr.line=pr1.line
  prout=replicate(temporary(pr),2)
  
                                ;determine indices for components
  cidx1=intarr(pr1.input.ncomp)-1
  cidx2=intarr(pr2.input.ncomp)-1
  ic1=0 & ic2=0
  i=0
  repeat begin
    if min(pr1.input.atm(ic1).use_line eq $
           pr2.input.atm(ic2).use_line) then begin
      cidx1(ic1)=i & cidx2(ic2)=i
      ic1=ic1+1 & ic2=ic2+1
    endif else begin
      if comp(1) gt comp(0) then begin
        cidx2(ic2)=i & ic2=ic2+1 
      endif else begin
        cidx1(ic1)=i & ic1=ic1+1 
      endelse
    endelse
    i=i+1 
  endrep until i eq ipt.ncomp
  
                                ;fill output structures
  print,'Filling output structures'
  xo1=min(xx)-min(xx1) & yo1=min(yy)-min(yy1)
  xo2=min(xx)-min(xx2) & yo2=min(yy)-min(yy2)
  fitarr=fltarr(2,nx,ny)
  ax1=xx1-xo1 & ay1=yy1-yo1
  ax2=xx2-xo2 & ay2=yy2-yo2
  fitarr(0,ax1(0):ax1(1),ay1(0):ay1(1))=pr1.fit.fitness
  fitarr(1,ax2(0):ax2(1),ay2(0):ay2(1))=pr2.fit.fitness
  bfit=where(fitarr(1,*,*) gt fitarr(0,*,*))
  tn=tag_names(pr1.fit)
  for i=0,1 do begin
    if i eq 0 then begin
      ax=ax1 & ay=ay1 & cidx=cidx1 & pr=pr1
    endif else begin
      ax=ax2 & ay=ay2 & cidx=cidx2 & pr=pr2
    endelse
    for it=0,n_tags(pr.fit)-1 do begin
      if tn(it) ne 'ATM' then begin
        prout(i).fit(ax(0):ax(1),ay(0):ay(1)).(it)=pr.fit.(it)
      endif else begin
        for ic=0,pr.input.ncomp-1 do $
          prout(i).fit(ax(0):ax(1),ay(0):ay(1)).atm(cidx(ic))=pr.fit.atm(ic)
      endelse
    endfor
  endfor
                                ;fill output with pixels where pr2 has
                                ;higher fitness
  print,'Combining structures'
  if bfit(0) ne -1 then begin
    for it=0,n_tags(pr.fit)-1 do begin
      if tn(it) ne 'ATM' then begin
        prout(0).fit(bfit).(it)=prout(1).fit(bfit).(it)
      endif else begin
        for ic=0,prout(0).input.ncomp-1 do $
          prout(0).fit(bfit).atm(cidx(ic))=prout(1).fit(bfit).atm(ic)
      endelse
    endfor
  endif
  
  pikaia_result=temporary(prout(0))
  pikaia_result.input.x=xx
  pikaia_result.input.y=yy
  xarr=(intarr(ny)+1) ## (indgen(nx)+xx(0))
  yarr=(indgen(ny)+yy(0)) ## (intarr(nx)+1)
  isfit=where(fitarr(1,*,*) gt 0 or fitarr(0,*,*) gt 0)
  pikaia_result.fit(isfit).x=xarr(isfit)
  pikaia_result.fit(isfit).y=yarr(isfit)
  
  save,/xdr,file=savout,/compress,pikaia_result
  print,'Combined 2 sav files into '+savout
end
