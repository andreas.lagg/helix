pro diswg_set
  common diswg,diswg
  common param,param
  
  for i=0,param.npar-1 do begin
    widget_control,diswg.zmin(i).id,set_value=diswg.zmin(i).val
    widget_control,diswg.zmax(i).id,set_value=diswg.zmax(i).val
  endfor
  widget_control,diswg.title.id,set_value=diswg.title.str
  widget_control,diswg.subtitle.id,set_value=diswg.subtitle.str
  widget_control,diswg.charsize.id,set_value=diswg.charsize.val
end

pro diswg_event,event
  common diswg,diswg
  common puse,puse
  common param,param
  
  widget_control,event.id,get_uvalue=uval
  uspos=strpos(uval,'_')
  if uspos eq -1 then begin
    ust=uval
    uid=0
  endif else begin
    ust=strmid(uval,0,uspos)
    uid=fix(strmid(uval,uspos+1,strlen(uval)))    
  endelse
  case ust of
    'zmin': diswg.zmin(uid).val=event.value
    'zmax': diswg.zmax(uid).val=event.value
    'azitop': diswg.azitop(uid).val=event.select
    'contour': diswg.contour(uid).val=event.index
    'contcomp': diswg.contcomp(uid).val=event.index
    'aziuseb': diswg.aziuseb.val=event.select
    'azinel': diswg.azinel.val=event.value
    'azilen': diswg.azilen.val=event.value
    'charsize': diswg.charsize.val=event.value
    'fixmargins0': diswg.fix_margins(0).val=event.value
    'fixmargins1': diswg.fix_margins(1).val=event.value
    'inputout': diswg.inputout.val=event.select
    'title': diswg.title.str=event.value
    'subtitle': diswg.subtitle.str=event.value
    'control': begin
      case event.value of
        'done': widget_control,diswg.base.id,/destroy
        'reset': begin
          diswg.zmin.val=0
          diswg.zmax.val=0
          diswg.title.str=''
          diswg.subtitle.str=''
          diswg.charsize.val=0
          diswg_set
        end
      endcase
    end
    else: begin
      help,/st,event
      print,uid,ust
    end
  endcase
end

pro dis_widget,init=init
  common wgst,wgst
  common param,param
  common diswg,diswg
  
  if n_elements(diswg) eq 0 then begin
    subst={id:0l,val:0.,str:''}
    rp=replicate(subst,param.npar)
    diswg={base:subst,zmin:rp,zmax:rp,title:subst,subtitle:subst, $
           charsize:subst,control:subst,azinel:subst,azitop:rp, $
           azilen:subst,aziuseb:subst,inputout:subst,contour:rp,contcomp:rp, $
           fix_margins:replicate(subst,2)}
    incidx=where(param.name eq 'INC')
    if incidx(0) ne -1 then diswg.azitop(incidx(0)).val=1
    diswg.azinel.val=20
    diswg.azilen.val=1.
    diswg.aziuseb.val=1.
    diswg.base.id=-1
    diswg.inputout.id=1
    diswg.contcomp.val=param.ncomp
    diswg.fix_margins.val=[0.08,0.86]
  endif
  if keyword_set(init) then return
  
  screen=get_screen_size()
  diswg.base.id=widget_base(title='Define Display Settings',/col, $
                            y_scroll_size=screen(1)*0.9, $
                            x_scroll_size=500)
  lab=widget_label(diswg.base.id, $
                   value='Set z-range (automatic range: min=0 and max=0)')
  
  
  sub=widget_base(diswg.base.id,row=param.npar+1,space=0)
  lab=widget_label(sub,value='Parameter',xsize=100)  
  lab=widget_label(sub,value='Z-min',xsize=80)  
  lab=widget_label(sub,value='Z-max',xsize=70)
  lab=widget_label(sub,value='Azi-Arrows',xsize=80)
  lab=widget_label(sub,value='Contour',xsize=80)
  lab=widget_label(sub,value='Cont-Comp',xsize=80)
  is='_'+n2s(indgen(param.npar),format='(i2.2)')
  cmpstr=[n2s(indgen(param.ncomp)+1),'Corr']
  for i=0,param.npar-1 do begin
    lab=widget_label(sub,value=param.name(i),xsize=100)
;     diswg.usemin(i).id=cw_bgroup(sub,uvalue='usemin'+is(i),'MinVal:', $
;                                 set_value=diswg.usemin(i).val,/nonexclusive)
    diswg.zmin(i).id=cw_field(sub,/all_events,/floating,xsize=8, $
                              title='', $
                              uvalue='zmin'+is(i),value=diswg.zmin(i).val)
    diswg.zmax(i).id=cw_field(sub,/all_events,/floating,xsize=8, $
                              title='', $
                              uvalue='zmax'+is(i),value=diswg.zmax(i).val)
    diswg.azitop(i).id=cw_bgroup(sub,uvalue='azitop'+is(i), $
                                 set_value=diswg.azitop(i).val, $
                                 /nonexclusive,' ')
    diswg.contour(i).id=widget_droplist(sub,uvalue='contour'+is(i), $
                                        value=['None',param.name])
    widget_control,diswg.contour(i).id,set_droplist_select=diswg.contour(i).val
    diswg.contcomp(i).id=widget_droplist(sub,uvalue='contcomp'+is(i), $
                                          value=cmpstr)
    widget_control,diswg.contcomp(i).id, $
      set_droplist_select=diswg.contcomp(i).val
  endfor
  diswg.azinel.id=cw_field(diswg.base.id,/all_events,xsize=4,/integer, $
                           uvalue='azinel', $
                           title='# of vectors for AZI-overplot' + $
                           ' (x-direction):')
  widget_control,diswg.azinel.id,set_value=diswg.azinel.val
  sub=widget_base(diswg.base.id,row=1)
  diswg.azilen.id=cw_field(sub,/all_events,xsize=8,/floating, $
                           uvalue='azilen', $
                           title='Length of vectors for AZI-overplot:')
  widget_control,diswg.azilen.id,set_value=diswg.azilen.val
  diswg.aziuseb.id=cw_bgroup(sub,uvalue='aziuseb',row=1, $
                             set_value=diswg.aziuseb.val, $
                             /nonexclusive,'Use B*sin(INC) for length')
  
  diswg.title.id=cw_field(diswg.base.id,/all_events,xsize=40,/string, $
                          uvalue='title',title='Title:')
  diswg.subtitle.id=cw_field(diswg.base.id,/all_events,xsize=40,/string, $
                             uvalue='subtitle',title='Sub-Title:')
  sub=widget_base(diswg.base.id,row=1)
  diswg.charsize.id=cw_field(sub,/all_events,xsize=8,/float, $
                             uvalue='charsize',title='Charsize:')
  diswg.fix_margins(0).id=cw_field(sub,/all_events,xsize=8,/float, $
                                   value=diswg.fix_margins(0).val, $
                                   uvalue='fixmargins0',title='Margins Left:')
  diswg.fix_margins(1).id=cw_field(sub,/all_events,xsize=8,/float, $
                                   value=diswg.fix_margins(1).val, $
                                   uvalue='fixmargins1',title='Right:')
  
  diswg.aziuseb.id=cw_bgroup(diswg.base.id,uvalue='inputout',row=1, $
                             set_value=diswg.inputout.val, $
                             /nonexclusive,'Print input file')
  
  
  
  diswg.control.id=cw_bgroup(diswg.base.id,['Done','Reset All'], $
                             uvalue='control',row=1, $
                             button_uvalue=['done','reset'])
  
  widget_control,diswg.base.id,/realize
  xmanager,'diswg',diswg.base.id,no_block=1
  diswg_set
end


pro display_settings,init=init
  common wgst,wgst
  common diswg,diswg
  
  
  if n_elements(diswg) ne 0 then begin
    widget_control,diswg.base.id,iconify=0,bad_id=bad_id
    if bad_id eq 0 then return
  endif
  
  dis_widget,init=init
  
end
