function get_avgbin,arr
  
  uarr=arr(*)
  nn0=where(uarr ne 0)
  if n_elements(nn0) gt 3 then begin
    xpt=uarr(nn0)-min(uarr(nn0))
    i=max(xpt)+1
    nt=n_elements(xpt)
    xavg=0
    arr=fltarr(i)
    repeat begin
      i=i-1
      nn=n_elements(where((xpt mod i) eq 0))
      arr(i)=float(nn)/nt
    endrep until i eq 0
    xavg=max(where(arr eq max(arr)))>1
  endif else xavg=1
  return,xavg
end

