;initialize hanle computations: compute common blocks, temporary matrices ...
pro testmat,mat
  print,([minmaxp(mat),total(mat),total(abs(mat))])(*)
end

pro factrl
  COMMON FACT,FACT
  
  fact=dblarr(302)
  fact(0)=1.
  for i=1,31 do fact(i)=fact(i-1)*double(i)
end

function modulo,a,b
  
  return,a mod b
end

pro COMPROD,A,B,C,D,E,F
; C-----PERFORMS THE PRODUCT OF TWO COMPLEX NUMBERS:
; C-----(A+iB)*(C+iD)=(E+iF)
; C-----ON INPUT:   A,B,C,D
; C-----ON OUTPUT:  E,F
;       IMPLICIT DOUBLE PRECISION(A-H,O-Z)
  E=A*C-B*D
  F=A*D+B*C
END

pro PROFAS,A,V,VO,GA
;      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
;      double COMPLEX Z,Z2,A0C,A1C,A2C,A3C,B1C,B2C,B3C,RES
  Z=dcomplex(0) & Z2=dcomplex(0) & A0C=dcomplex(0) & A1C=dcomplex(0)
  A2C=dcomplex(0) & A3C=dcomplex(0) & B1C=dcomplex(0) & B2C=dcomplex(0)
  B3C=dcomplex(0) & RES=dcomplex(0) & 
  A1=0.4613135d
  A2=0.09999216d
  A3=0.002883894d
  B1=0.1901635d
  B2=1.7844927d
  B3=5.5253437d
  ZERO=0d
  UNO=1d
  Z=dcomplex(V,A)
  Z2=Z*Z
  A0C=dcomplex(ZERO,UNO)
  A1C=dcomplex(A1,ZERO)
  A2C=dcomplex(A2,ZERO)
  A3C=dcomplex(A3,ZERO)
  B1C=dcomplex(B1,ZERO)
  B2C=dcomplex(B2,ZERO)
  B3C=dcomplex(B3,ZERO)
  RES=A0C*Z*(A1C/(Z2-B1C)+A2C/(Z2-B2C)+A3C/(Z2-B3C))
  VO=real_part(RES)      
  GA=imaginary(RES)
END

pro PROFILARCH,A,V,VO,GA
;      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
  IF(A LT 1.E-8) then A=1.E-8
  IF(A GT 3. OR ABS(V) GT 10.) eq 0 then begin ; GOTO 210
    V2=V*V
    A2=A*A*2.
    U0=A*1.7724538509
    U1=-A2
    S0=U0+U1
    N=2
l206:
    U0=U0*A2/N
    N=N+1
    U1=U1*A2/N
    N=N+1
    U=U0+U1
    S0=S0+U
    IF(ABS(S0) LT 1E-20) then GOTO,l206
    IF(ABS(U/S0) GT 1E-8) then GOTO,l206
    N=1
    T=1.
    S=S0
    S1=0.
l207:
    S0=(1-S0)*A2/(2*N-1)
    Q=T*S0
    S1=Q+S1
    T=T*V2/N
    U=T*S0
    S=S+U
    N=N+1
    IF(ABS(S1) LT 1E-20) then goto,l207
    IF(ABS(Q/S1) GT 1E-8) then goto,l207
    IF(ABS(S) LT 1E-20) then goto,l207
    IF(ABS(U/S) GT 1E-8) then goto,l207
    EX=EXP(-V2)
    S=S*EX
    S1=S1*EX*V/A
    A1=A*1.7724538509
    VO=S/A1
    GA=S1/A1
  endif else begin              ;210
    PROFAS,A,V,VO,GA
  endelse
END

;fortran function dsign
function dsign,a,b
  
  if b ge 0 then return,a else return,-a
end

pro tqli1,d,e,n,np,z
; c---------------------------------------------------------------------------
; c-----See "Numerical Recipes"
; c---------------------------------------------------------------------------
;       implicit double precision (a-h,o-z)
;       dimension d(np),e(np),z(np,np)
  if(n gt 1) then begin
    for i=2,n do $
      e(i-1-1)=e(i-1)
    e(n-1)=0.d0
    for l=1,n do begin
      iter=0
l1:
      for m=l,n-1 do begin      ; 12
        dd=abs(d(m-1))+abs(d(m+1-1))
        if(abs(e(m-1))+dd eq dd) then goto,l2
      endfor                    ; 12   continue
      m=n
l2:
      if(m ne l) then begin
        if(iter eq 30) then message,'too many iterations'
        iter=iter+1
        g=(d(l+1-1)-d(l-1))/(2.d0*e(l-1))
        r=sqrt(g^22+1.d0)
        g=d(m-1)-d(l-1)+e(l-1)/(g+dsign(r,g))
        s=1.d0
        c=1.d0
        p=0.d0
        for i=m-1,l,-1 do begin ; 14
          f=s*e(i-1)
          btq=c*e(i-1)
          if(abs(f) ge abs(g)) then begin
            c=g/f
            r=sqrt(c^22+1.d0)
            e(i+1-1)=f*r
            s=1.d0/r
            c=c*s
          endif else begin
            s=f/g
            r=sqrt(s^2+1.d0)
            e(i+1-1)=g*r
            c=1.d0/r
            s=s*c
          endelse
          g=d(i+1-1)-p
          r=(d(i-1)-g)*s+2.d0*c*btq
          p=s*r
          d(i+1-1)=g+p
          g=c*r-btq
          for k=1,n do begin    ; 13
            f=z(k-1,i+1-1)
            z(k-1,i+1-1)=s*z(k-1,i-1)+c*f
            z(k-1,i-1)=c*z(k-1,i-1)-s*f
          endfor                ; 13   continue
        endfor                  ; 14   continue
        d(l-1)=d(l-1)-p
        e(l-1)=g
        e(m-1)=0.d0
        goto,l1
      endif
    endfor                      ; 15   continue
  endif
end

FUNCTION W6JS,J1,J2,J3,L1,L2,L3
;      INTEGER W,WMIN,WMAX
;      INTEGER SUM1,SUM2,SUM3,SUM4
  common fact,fact
  W6JS=0.0
  IA=J1+J2
  IF (IA LT J3) then GOTO,l1000
  IB=J1-J2
  IF (ABS(IB) GT J3) then GOTO,l1000
  IC=J1+L2
  IF (IC LT L3) then GOTO,l1000
  ID=J1-L2
  IF(ABS(ID) GT L3) then GOTO,l1000
  IE=L1+J2
  IF(IE LT L3) then GOTO,l1000
  IFF=L1-J2
  IF(ABS(IFF) GT L3) then GOTO,l1000
  IG=L1+L2
  IF(IG LT J3) then GOTO,l1000
  IH=L1-L2
  IF(ABS(IH) GT J3) then GOTO,l1000
  SUM1=IA+J3
  SUM2=IC+L3
  SUM3=IE+L3
  SUM4=IG+J3
;C     IF(MODULO(SUM1,2).NE.0) GOTO,l1000
;C     IF(MODULO(SUM2,2).NE.0) GOTO,l1000
;C     IF(MODULO(SUM3,2).NE.0) GOTO,l1000
;C     IF(MODULO(SUM4,2).NE.0) GOTO,l1000
  WMIN=MAX([SUM1,SUM2,SUM3,SUM4])
  II=IA+IG
  IJ=J2+J3+L2+L3
  IK=J3+J1+L3+L1
  WMAX=MIN([II,IJ,IK])
  OMEGA=0.0
  for W=WMIN,WMAX,2 do begin
    DENOM=FACT((W-SUM1)/2)*FACT((W-SUM2)/2)*FACT((W-SUM3)/2)* $
      FACT((W-SUM4)/2)*FACT((II-W)/2)*FACT((IJ-W)/2)* $
      FACT((IK-W)/2)
    IF (MODULO(W,4) NE 0) then DENOM=-DENOM
    OMEGA=OMEGA+FACT(W/2+1)/DENOM
  endfor
  THETA1=FACT((IA-J3)/2)*FACT((J3+IB)/2)*FACT((J3-IB)/2)/FACT(SUM1/2+1)
  THETA2=FACT((IC-L3)/2)*FACT((L3+ID)/2)*FACT((L3-ID)/2)/FACT(SUM2/2+1)
  THETA3=FACT((IE-L3)/2)*FACT((L3+IFF)/2)*FACT((L3-IFF)/2)/FACT(SUM3/2+1)
  THETA4=FACT((IG-J3)/2)*FACT((J3+IH)/2)*FACT((J3-IH)/2)/FACT(SUM4/2+1)
  THETA=THETA1*THETA2*THETA3*THETA4
  W6JS=OMEGA*SQRT(THETA)
  IF (ABS(W6JS) LT 1.0E-8) then W6JS=0.0
  l1000:
  RETURN,w6js
END

FUNCTION W3JS,J1,J2,J3,M1,M2,M3
;      INTEGER Z,ZMIN,ZMAX
  COMMON FACT,FACT
  W3JS=0.0
  IF (M1+M2+M3 ne 0) then GOTO,l1000
  IA=J1+J2
  IF (J3 GT IA) then goto,l1000
  IB=J1-J2
  IF(J3 LT ABS(IB)) then goto,l1000
  JSUM=J3+IA
  IC=J1-M1
  ID=J2-M2
;C     IF(MODULO(JSUM,2).NE.0) then goto,l1000
;C     IF(MODULO(IC,2).NE.0) then goto,l1000
;C     IF(MODULO(ID,2).NE.0) then goto,l1000
  IF (ABS(M1) GT J1) then goto,l1000
  IF (ABS(M2) GT J2) then goto,l1000
  IF (ABS(M3) GT J3) then goto,l1000
  IE=J3-J2+M1
  IFF=J3-J1-M2
  ZMIN=MAX([0,-IE,-IFF])
  IG=IA-J3
  IH=J2+M2
  ZMAX=MIN([IG,IH,IC])
  CC=0.0
  for Z=ZMIN,ZMAX,2 do begin
    DENOM=FACT(Z/2)*FACT((IG-Z)/2)*FACT((IC-Z)/2)*FACT((IH-Z)/2)* $
      FACT((IE+Z)/2)*FACT((IFF+Z)/2)
    IF (MODULO(Z,4) NE 0) then DENOM=-DENOM
    CC=CC+1.0/DENOM
  endfor
  CC1=FACT(IG/2)*FACT((J3+IB)/2)*FACT((J3-IB)/2)/FACT((JSUM+2)/2)
  CC2=FACT((J1+M1)/2)*FACT(IC/2)*FACT(IH/2)* $
    FACT(ID/2)*FACT((J3-M3)/2)*FACT((J3+M3)/2)
  CC=CC*SQRT(CC1*CC2)
  IF (MODULO(IB-M3,4) NE 0) then CC=-CC
  W3JS=CC
  IF (ABS(W3JS) LT 1.0E-8) then W3JS=0.0
l1000:
  return,w3js
END

FUNCTION W9JS,J1,J2,J3,J4,J5,J6,J7,J8,J9
  KMIN=ABS(J1-J9)
  KMAX=J1+J9
  I=ABS(J4-J8)
  IF (I GT KMIN) then KMIN=I
  I=J4+J8
  IF (I LT KMAX) then KMAX=I
  I=ABS(J2-J6)
  IF (I GT KMIN) then KMIN=I
  I=J2+J6
  IF (I LT KMAX) then KMAX=I
  X=0.
  for K=KMIN,KMAX,2 do begin
    S=1.
    IF (MODULO(K,2) NE 0) then S=-1.
    X1=W6JS(J1,J9,K,J8,J4,J7)
    X2=W6JS(J2,J6,K,J4,J8,J5)
    X3=W6JS(J1,J9,K,J6,J2,J3)
    X=X+S*X1*X2*X3*double(K+1)
  endfor
  W9JS=X
  RETURN,w9js
END

pro paschen,l2,is2,e0,bp,njlev,e,c
;      implicit double precision (a-h,o-z)
;      dimension e0(0:20),njlev(-20:20),e(-20:20,11),c(-20:20,11,0:20)
;      dimension d(11),ud(11),z(11,11)
  d=dblarr(11)
  ud=dblarr(11)
  z=dblarr(11,11)
  noff=-20 & eoff=[-20,1] & coff=[-20,1,0]
;  factrl                        ;optimize: call necessary?
  bb=bp*4.6686d-5
  j2min=abs(l2-is2)
  j2max=l2+is2
  for m2=-j2max,j2max,2 do begin ; 10
    
    j2a=abs(m2)
    if(j2a lt j2min) then j2a=j2min
    j2b=j2max
    nj=1+(j2b-j2a)/2
    njlev(m2-noff)=nj

    for j=1,nj do begin         ; 20
      j2vero=j2a+2*(j-1)
      d(j-1)=e0(j2vero) 
      x1=1.d0
      if(modulo(2*j2vero+l2+is2+m2,4) ne 0) then x1=-1.d0
      x2=double(j2vero+1)*sqrt(double(is2*(is2+2)*(is2+1))/4.d0)
      x3=w3js(j2vero,j2vero,2,-m2,m2,0)
      x4=w6js(j2vero,j2vero,2,is2,is2,l2)
      d(j-1)=d(j-1)+bb*(double(m2)/2.d0+x1*x2*x3*x4)
    endfor                      ; 20   continue

    if(nj ne 1 ) then begin     ;40
      for jp=2,nj do begin      ; 30
        jp2vero=j2a+2*(jp-1)
        j2vero=jp2vero-2
        x1=1.d0
        if(modulo(j2vero+jp2vero+l2+is2+m2,4) ne 0) then x1=-1.d0
        x2=sqrt(double((j2vero+1)*(jp2vero+1)*is2*(is2+2)*(is2+1))/4.d0)
        x3=w3js(j2vero,jp2vero,2,-m2,m2,0)
        x4=w6js(j2vero,jp2vero,2,is2,is2,l2)
        ud(jp-1)=bb*x1*x2*x3*x4
      endfor                    ; 30   continue     
    endif                       ; 40   continue 
    
    z(*,*)=0d 
    for i=1,nj do z(i-1,i-1)=1.d0
    tqli1,d,ud,nj,11,z
    for jsmall=1,nj do begin    ; 70
      e(m2-eoff(0),jsmall-eoff(1))=d(jsmall-1)
      for jaux=1,nj do begin    ; 80
        j2=j2a+2*(jaux-1)
        c(m2-coff(0),jsmall-coff(1),j2-coff(2))=z(jaux-1,jsmall-1)
      endfor                    ; 80   continue
    endfor                      ; 70   continue
  endfor                        ; 10   continue
end

pro ROTMATSU,KMAX,ALFA,BETA,GAMMA,DR,DI
; C-----CALCULATES THE ROTATION MATRICES OF ORDER 0, 1, 2,....,KMAX
; C-----THE RESULTS ARE GIVEN IN THE MATRICES DR(K,IQ,IQP) AND 
; C-----DI(K,IQ,IQP) REPRESENTING THE REAL AND IMAGINARY PART OF THE 
; C-----ROTATION MATRIX, RESPECTIVELY.
; C-----THE ANGLES ALFA, BETA, AND GAMMA ARE GIVEN IN RADIANS.
; C-----THE MAXIMUM ALLOWED VALUE FOR KMAX IS 6
;       IMPLICIT DOUBLE PRECISION (A-H,O-Z)
;       DIMENSION DR(0:6,-6:6,-6:6), DI(0:6,-6:6,-6:6)
  doff=[0,-6,-6]
  COMMON FACT,FACT
;  FACTRL                        ;optimize ->possibly no call necessary
  dr(*,*,*)=0d
  di(*,*,*)=0d
  
  for K=0,KMAX do begin         ;2
    for IQ=-K,K do begin        ; 3
      for IQP=-K,K do begin     ; 4
        A1=ALFA*Double(IQ)
        A2=GAMMA*Double(IQP)
        CA=COS(A1)
        SA=SIN(A1)
        CG=COS(A2)
        SG=SIN(A2)
        BO2=BETA/2.
        CB2=COS(BO2)
        SB2=SIN(BO2)
        IT1=K+IQ
        IT2=K-IQP
        ITMAX=IT1
        IF(IT2 LT ITMAX) then ITMAX=IT2
        ITMIN=0
        IT3=IQ-IQP
        IF(IT3 GT 0) then ITMIN=IT3
        REDM=0.
        IF(BETA ne 0.) then begin     ;GO TO 6
          for IT=ITMIN,ITMAX do begin ;5 
            S=1.
            IF(MODulo(IT,2) NE 0) then S=-1.
            IE1=2*(K-IT)+IQ-IQP
            IE2=2*IT+IQP-IQ
            DEN=FACT(IT1-IT)*FACT(IT2-IT)*FACT(IT)*FACT(IT-IT3)
            REDM=REDM+S*(CB2^IE1)*(SB2^IE2)/DEN
          endfor                ; 5    CONTINUE
          REDM=REDM*SQRT(FACT(K+IQ)*FACT(K-IQ)*FACT(K+IQP)*FACT(K-IQP))
        endif else begin        ; 6    CONTINUE
          IF(IQ EQ IQP) then REDM=1.
        endelse
        DR(K-doff(0),IQ-doff(1),IQP-doff(2))=REDM*(CA*CG-SA*SG)
        DI(K-doff(0),IQ-doff(1),IQP-doff(2))=-REDM*(CA*SG+SA*CG)
      endfor                    ; 4    CONTINUE
    endfor                      ; 3    CONTINUE
  endfor                        ; 2    CONTINUE
END

pro TKP,K,IP,TR,TI
;C-----CALCULATES THE SYMBOL TKP------------------------
;      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
;      DIMENSION TR(0:3),TI(0:3)
  tr(*)=0d
  ti(*)=0d
  
  IF (K EQ 0 AND IP EQ 0) then begin
    TR(0)=1.D0
    RETURN
  endif else IF (K EQ 1 AND IP EQ 0) then begin
    TR(3)=SQRT(1.5D0)
    RETURN
  endif else IF (K EQ 2 AND IP EQ 0) then begin
    TR(0)=SQRT(0.5D0)
    RETURN
  endif else IF (K EQ 2 AND IP EQ 2) then begin
    A=SQRT(3.D0)/2.D0
    TR(1)=-A
    TI(2)=-A
    RETURN
  endif else IF (K EQ 2 AND IP EQ -2) then begin
    A=SQRT(3.D0)/2.D0
    TR(1)=-A
    TI(2)=A
  endif
END

pro TKQ2,ALFA,BETA,GAMMA,ALFAP,BETAP,GAMMAP,TR,TI
; C-----CALCULATES THE TENSOR TKQ FOR A DOUBLE ROTATION
; C-----ON INPUT: ALFA,BETA,GAMMA=EULER ANGLES IN RADIANS OF FIRST ROT.
; C-----        : ALFAP,BETAP,GAMMAP=SAME FOR SECOND ROTATION
; C-----ON OUTPUT: TR, TI= REAL AND IMAG PARTS OF TENSOR TKQ
;       IMPLICIT DOUBLE PRECISION (A-H,O-Z)
;       DIMENSION TR(0:2,-2:2,0:3),TI(0:2,-2:2,0:3),DR(0:6,-6:6,-6:6),
;      *          DI(0:6,-6:6,-6:6),DRP(0:6,-6:6,-6:6),
;      *          DIP(0:6,-6:6,-6:6),TPR(0:3),TPI(0:3)
  toff=[0,-2,0]                 ;offset for ti tr indices
  tpr=dblarr(4) & tpi=dblarr(4)
  dr=dblarr(7,13,13)
  di=dblarr(7,13,13)
  drp=dblarr(7,13,13)
  dip=dblarr(7,13,13)
  doff=[0,-6,-6]
  ROTMATSU,2,ALFA,BETA,GAMMA,DR,DI
  ROTMATSU,2,ALFAP,BETAP,GAMMAP,DRP,DIP
  for I=0,3 do begin            ; 20
    for K=0,2 do begin          ;1
      for IQ=-K,K do begin      ;2
        TR(K-toff(0),IQ-toff(1),I-toff(2))=0.
        TI(K-toff(0),IQ-toff(1),I-toff(2))=0.
        for IP=-K,K do begin    ;3
          TKP,K,IP,TPR,TPI
          for IR=-K,K do begin  ;4
            COMPROD,TPR(I),TPI(I),DR(K-doff(0),IP-doff(1),IR-doff(2)), $
              DI(K-doff(0),IP-doff(1),IR-doff(2)),E,F
            COMPROD,E,F,DRP(K-doff(0),IR-doff(1),IQ-doff(2)), $
              DIP(K-doff(0),IR-doff(1),IQ-doff(2)),G,H
            TR(K-toff(0),IQ-toff(1),I-toff(2))= $
              TR(K-toff(0),IQ-toff(1),I-toff(2))+G
            TI(K-toff(0),IQ-toff(1),I-toff(2))= $
              TI(K-toff(0),IQ-toff(1),I-toff(2))+H
          endfor                ; 4    CONTINUE
        endfor                  ; 3    CONTINUE
      endfor                    ; 2    CONTINUE
    endfor                      ; 1    CONTINUE
  endfor                        ; 20   CONTINUE
END

pro LUBKSB1,A,N,NP,INDX,B
;      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
;      DIMENSION A(NP,NP),INDX(N),B(N)
  II=0
  for I=1,N do begin            ; 12
    LL=INDX(I-1)
    SUM=B(LL-1)
    B(LL-1)=B(I-1)
    IF (II NE 0) THEN begin
      sum=sum-total(a(i-1,ii-1:i-2)*b(ii-1:i-2))
    endif ELSE IF (SUM NE 0.) THEN begin
      II=I
    ENDif
    B(I-1)=SUM
  endfor                        ;12    CONTINUE
  for I=N,1,-1 do begin         ;14
    SUM=B(I-1)
    IF(I LT N) THEN begin
      sum=sum-total(a(i-1,i+1-1:n-1)*b(i+1-1:n-1))
    ENDIF
    B(I-1)=SUM/A(I-1,I-1)
  endfor                        ;14    CONTINUE
END

pro LUDCMP1,A,N,NP,INDX,D
;      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
;      PARAMETER (NMAX=405,TINY=1.0E-20)
;  DIMENSION A(NP,NP),INDX(N),VV(NMAX)
  nmax=405
  tiny=1d-20
  vv=dblarr(nmax)
  
  D=1.
  aamax=max(abs(a(0:n-1,0:n-1)),dim=1)
  IF (min(AAMAX) EQ 0.) then message,'Singular matrix.'
  VV(0:n-1)=1./AAMAX(0:n-1)
  for J=1,N do begin            ; 19
    IF (J GT 1) THEN begin
      for I=1,J-1 do begin      ; 14
        SUM=A(I-1,J-1)
        IF (I GT 1) THEN begin
                                ;loop optmization here!
          sum=sum-total(a(i-1,0:i-2)*a(0:i-2,j-1))
          A(I-1,J-1)=SUM
        ENDIF
      endfor                    ;14        CONTINUE
    ENDIF
    AAMAX=0.
    for I=J,N do begin          ; 16
      SUM=A(I-1,J-1)
      IF (J GT 1)THEN begin
        sum=sum-total(a(i-1,0:j-2)*a(0:j-2,j-1))
        A(I-1,J-1)=SUM
      ENDIF
    endfor                      ;16      CONTINUE
    aamax=max(vv(j-1:n-1)*abs(a(j-1:n-1,j-1)),imx)
    imax=imx+j
    IF (J NE IMAX) THEN begin
      dum=a(imax-1,0:n-1)
      a(IMAX-1,0:n-1)=a(J-1,0:n-1)
      a(J-1,0:n-1)=DUM
      D=-D
      VV(IMAX-1)=VV(J-1)
    ENDIF
    INDX(J-1)=IMAX
    IF(J NE N)THEN begin
      IF(A(J-1,J-1) EQ 0.) then A(J-1,J-1)=TINY
      DUM=1./A(J-1,J-1)
;       for I=J+1,N do $
;         A(I-1,J-1)=A(I-1,J-1)*DUM
      A(j+1-1:n-1,j-1)=a(j+1-1:n-1,j-1)*DUM
    ENDIF
  endfor                        ;19    CONTINUE
  IF(A(N-1,N-1) EQ 0 ) then A(N-1,N-1)=TINY
END

pro ludcmp0,a,n,indx,d
; 	USE nrtype; USE nrutil, ONLY : assert_eq,imaxloc,nrerror,outerprod,swap
; 	IMPLICIT NONE
; 	REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: a
; 	INTEGER(I4B), DIMENSION(:), INTENT(OUT) :: indx
; 	REAL(SP), INTENT(OUT) :: d
; 	REAL(SP), DIMENSION(size(a,1)) :: vv
; 	REAL(SP), PARAMETER :: TINY=1.0e-20_sp
; 	INTEGER(I4B) :: j,n,imax
; 	n=assert_eq(size(a,1),size(a,2),size(indx),'ludcmp')
	d=1.0
  tiny=1d-20
;  vv=max(abs(a(0:n-1,0:n-1)),dim=1)
  vv=max(abs(a),dim=1)
  stop
;  if (any(vv == 0.0)) call nrerror('singular matrix in ludcmp')
  if (min(vv) EQ 0.) then message,'Singular matrix.'
	vv=1.0/vv
  for j=1-1,n-1 do begin
    dummy=max(vv(j:n-1)*abs(a(j:n-1,j)),iml)
		imax=(j-1)+(iml+1)
    if (j ne imax) then begin
      dummy=a(imax,0:n-1) & a(imax,0:n-1)=a(j,0:n-1) & a(j,0:n-1)=dummy
;			call swap(a(imax,:),a(j,:))
			d=-d
			vv(imax)=vv(j)
		end
		indx(j)=imax+1
    if (a(j,j) eq 0.0) then a(j,j)=TINY
    if (j+1 lt n) then begin
      a(j+1:n-1,j)=a(j+1:n-1,j)/a(j,j)
      a(j+1:n-1,j+1:n-1)=$
        a(j+1:n-1,j+1:n-1)-matrix_multiply(a(j+1:n-1,j),a(j,j+1:n-1))
    endif
	endfor
end


pro lubksb0,a,n,indx,b
; 	USE nrtype; USE nrutil, ONLY : assert_eq
; 	IMPLICIT NONE
; 	REAL(SP), DIMENSION(n,n), INTENT(IN) :: a
; 	INTEGER(I4B), DIMENSION(n), INTENT(IN) :: indx
; 	REAL(SP), DIMENSION(n), INTENT(INOUT) :: b
; 	INTEGER(I4B) :: i,n,ii,ll
; 	REAL(SP) :: summ
  ii=0
	for i=1,n do begin
    ll=indx(i-1)
    summ=b(ll-1)
    b(ll-1)=b(i-1)
    if (ii ne 0) then begin
;			summ=summ-dot_product(a(i-1,ii-1:i-1-1),b(ii-1:i-1-1))
      summ=summ-((a(i-1,ii-1:i-1-1)) # b(ii-1:i-1-1))
		endif else if (summ ne 0.0) then begin
			ii=i
		endif
    b(i-1)=summ
  endfor
;  for i=n-1,1,-1 do begin
  for i=n-1,1,-1 do begin
;		b(i-1) = (b(i-1)-dot_product(a(i-1,i+1-1:n-1),b(i+1-1:n-1)))/a(i-1,i-1)
    b(i-1) = (b(i-1)-((a(i-1,i+1-1:n-1)) # b(i+1-1:n-1)))/a(i-1,i-1)
	endfor
end
 
pro gamma,l2,is2,j2,jp2,g
;      implicit double precision (a-h,o-z)
  g=0.d0
  if(j2 eq jp2) then g=g+sqrt(double(j2*(j2+2)*(j2+1))/4.d0)
  x1=1.d0
  if(modulo(2+l2+is2+j2,4) ne 0) then x1=-1.d0
  x2=sqrt(double((j2+1)*(jp2+1)*is2*(is2+2)*(is2+1))/4.d0)
  x3=w6js(j2,jp2,2,is2,is2,l2)
  g=g+x1*x2*x3
end

pro bigpar,l2,is2,k2,kp2,j2,jp2,js2,jt2,x
;      implicit double precision(a-h,o-z)
  x=0.d0
  if (jp2 ne jt2 and j2 ne js2) then return
  if(jp2 eq jt2) then begin                             ;10
    if(abs(j2-js2) gt 2 or j2+js2 eq 0) eq 0 then begin ; go to 10
      gamma,l2,is2,j2,js2,g
      x1=g
      x2=w6js(k2,kp2,2,js2,j2,jp2)
      x=x1*x2
    endif
  endif
  if(j2 ne js2) then return
  if(abs(jt2-jp2) gt 2 or jt2+jp2 eq 0) then return
  gamma,l2,is2,jt2,jp2,g
  x1=g
  x2=1.d0
  if(modulo(k2-kp2,4) ne 0) then x2=-1.d0
  x3=w6js(k2,kp2,2,jt2,jp2,j2)
  x=x+x1*x2*x3
end


pro ROTMATK,K,ALFA,BETA,GAMMA,DR,DI
; c-----an adaptation of rotmatsu
; C-----CALCULATES THE ROTATION MATRICES OF ORDER K
; C-----THE RESULTS ARE GIVEN IN THE MATRICES DR(IQ,IQP) AND 
; C-----DI(IQ,IQP) REPRESENTING THE REAL AND IMAGINARY PART OF THE 
; C-----ROTATION MATRIX, RESPECTIVELY.
; C-----THE ANGLES ALFA, BETA, AND GAMMA ARE GIVEN IN RADIANS.
;      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
;      DIMENSION DR(-1:1,-1:1), DI(-1:1,-1:1)
  common fact,fact
  doff=[-1,-1]
  factrl
  
  dr=dblarr(3,3)
  di=dblarr(3,3)
  dr(*)=0
  di(*)=0
  
;loop optimization possible!  
  for IQ=-K,K do begin          ;3
    for IQP=-K,K do begin       ;4
      A1=ALFA*Double(IQ)
      A2=GAMMA*Double(IQP)
      CA=COS(A1)
      SA=SIN(A1)
      CG=COS(A2)
      SG=SIN(A2)
      BO2=BETA/2.
      CB2=COS(BO2)
      SB2=SIN(BO2)
      IT1=K+IQ
      IT2=K-IQP
      ITMAX=IT1
      IF(IT2 LT ITMAX) then ITMAX=IT2
      ITMIN=0
      IT3=IQ-IQP
      IF(IT3 GT 0) then ITMIN=IT3
      REDM=0.
      IF(BETA ne 0.) then begin     ;6
        for IT=ITMIN,ITMAX do begin ;5
          S=1.
          IF(MODULO(IT,2) NE 0) then S=-1.
          IE1=2*(K-IT)+IQ-IQP
          IE2=2*IT+IQP-IQ
          DEN=FACT(IT1-IT)*FACT(IT2-IT)*FACT(IT)*FACT(IT-IT3)
          REDM=REDM+S*(CB2^IE1)*(SB2^IE2)/DEN
        endfor                  ; 5    CONTINUE
        REDM=REDM*SQRT(FACT(K+IQ)*FACT(K-IQ)*FACT(K+IQP)*FACT(K-IQP))
      endif else begin          ; 6    CONTINUE
        IF(IQ EQ IQP) then REDM=1.
      endelse                   ; 7    CONTINUE
      DR(IQ-doff(0),IQP-doff(1))=REDM*(CA*CG-SA*SG)
      DI(IQ-doff(0),IQP-doff(1))=-REDM*(CA*SG+SA*CG)
    endfor                      ; 4    CONTINUE
  endfor                        ; 3    CONTINUE
END


pro campo,u1,u2,rio,wl,f,hs,barn,w
;c===============================================================
;c      da commentare...
;c==============================================================
  h=hs/958.d0
  sg=1.d0/(1.d0+h)
  sg2=sg*sg
  cg=sqrt(1.d0-sg2)
  cg2=cg*cg
  cg3=cg2*cg
  fact=cg2*alog((1.d0+sg)/cg)/sg
  a0=1.d0-cg
  a1=cg-.5d0-.5d0*fact
  a2=(cg+2.d0)*(cg-1.d0)/(3.d0*(cg+1.d0))
  x0=(a0+a1*u1+a2*u2)/2.d0
  c0=cg*sg2
  c1=(8.d0*cg3-3.d0*cg2-8.d0*cg+2.d0+(4.d0-3.d0*cg2)*fact)/8.d0
  c2=(cg-1.d0)*(9.d0*cg3+18.d0*cg2+7.d0*cg-4.d0)/(15.d0*(cg+1.d0))
  w=(c0+c1*u1+c2*u2)/(2.d0*(a0+a1*u1+a2*u2))
  const=0.08386d0
  barn=(wl^5)*const*rio*x0*f
end

pro hanle_init,line,obs_par,verbose
                                ;common block to store atmosphere
                                ;independent data
@common_maxpar  
@hanle_common  
  common oldmod,oldmod
  
                                ;angles for emission vector should be
                                ;already defined
  thetad=double(obs_par.heliocentric_angle)
  chid=0d                       ;
  gammad=(360.-double(obs_par.posq2limb)+90.+360.) mod 360.
  
  
  const=0.08386d
  
  
  factrl
;----------------------------------------------------------------------
;-----si definisce la costante "clight" per passare da Kyser in Hertz
;----------------------------------------------------------------------
  clight=2.997925d10
  pigr=acos(-1d)
  tras=pigr/180.d0
  twopi=2.d0*pigr
;----------------------------------------------------------------------
;-----si leggono i dati del modello e si costruisce la tabella
;----------------------------------------------------------------------  
  
                                ;check if new calculation of xam
                                ;matrices is necessary
  emvec=float([thetad,chid,gammad])
  tmod={linid:line.id,emvec:emvec}
  if n_elements(oldmod) eq 0 then begin
    recalc=1
  endif else recalc=0
  if recalc eq 0 then begin
    for it=0,n_tags(oldmod)-1 do begin
      recalc=recalc or min(oldmod.(it) eq tmod.(it)) eq 0
    endfor
  endif
  oldmod=tmod
  
;  RECALC=1 & print,'RECALC=1'
  
  if recalc eq 1 then begin
    if verbose ge 1 then begin
      print,'Emission vector = ',thetad,chid,gammad  
      print,'Recalculating the matrices...'
    endif
    
    qtab=intarr(405)
    q=0 & q2=0 & qp=0 & qp2=0 & qu=0 &  qu2=0 & ql=0 & ql2=0
    bigq=0 & bigq2=0 & bigqu=0 & bigqu2=0 & bigql=0 & bigql2=0
    lsto2=intarr(30)
    energy=dblarr(30,21)
    ntab=lonarr(405) & j2tab=lonarr(405) & jp2tab=lonarr(405) & ktab=lonarr(405)
    irtab=lonarr(405)
    rnutab=dblarr(405)
    gei=dblarr(3)
    a=dblarr(405,405) & am=dblarr(405,405) & a0=dblarr(405,405)
    b=dblarr(405) & indx=intarr(405)
    dr=dblarr(3,3) & di=dblarr(3,3)
    doff=[-1,-1]                ;offset for indices of di and dr
    dsur=dblarr(7,13,13) & dsui=dblarr(7,13,13)
    dslr=dblarr(7,13,13) & dsli=dblarr(7,13,13)
    dsoff=[0,-6,-6]             ;offset for ds array indices 
    aesto=dblarr(50)
    fsto=dblarr(50)
    ntlsto=intarr(50) & ntusto=intarr(50)
    u1sto=dblarr(50) & u2sto=dblarr(50) & riosto=dblarr(50) & wlsto=dblarr(50)
    tr=dblarr(3,5,4) & ti=dblarr(3,5,4)
    toff=[0,-2,0]               ;offset for ti tr indices
    e0=dblarr(21)
    njlevl=intarr(41) & njlevu=intarr(41) & njoff=-20
    autl=dblarr(41,11) & autu=dblarr(41,11) & auoff=[-20,1]
    cl=dblarr(41,11,21) & cu=dblarr(41,11,21) & coff=[-20,1,0]
    eps=dblarr(4,maxwl)
    eta=dblarr(4,maxwl)
    stokes=dblarr(4,maxwl)
    onumvec=dblarr(maxwl)
    argvec=dblarr(maxwl)
    rhor=dblarr(13,13,13,25)
    rhoi=dblarr(13,13,13,25)
    rhomr=dblarr(13,13,13,25)
    rhomi=dblarr(13,13,13,25)
    rholr=dblarr(13,13,13,25)
    rholi=dblarr(13,13,13,25)   
    rhomlr=dblarr(13,13,13,25)
    rhomli=dblarr(13,13,13,25)
    roff=[0,0,0,-12]
    
    if string(line.id) eq 'He1 10829.09' then nemiss=1 $
    else message,'unknown line for Hanle computation.'
    
    is2=line.hanle.is2
    nterm=line.hanle.nterm
    ntrans=line.hanle.ntrans
    lsto2=line.hanle.lsto2
    energy=line.hanle.energy 
    aesto=line.hanle.aesto
    fsto=line.hanle.fsto
    ntlsto=line.hanle.ntlsto
    ntusto=line.hanle.ntusto
    u1sto=line.hanle.u1sto
    u2sto=line.hanle.u2sto
    riosto=line.hanle.riosto
    wlsto=line.hanle.wlsto
    m=0
    for i=1,nterm do begin
      jmin2=abs(is2-lsto2(i-1))      
      jmax2=is2+lsto2(i-1)
      for j2=jmin2,jmax2,2 do begin
        for jp2=j2,jmax2,2 do begin
          rnujjp=clight*(energy(i-1,j2)-energy(i-1,jp2))
          kmin=(jp2-j2)/2
          kmax=(jp2+j2)/2
          for k=kmin,kmax do begin
            for q=-k,k do begin
              if (j2 eq jp2 and q lt 0) ne 1 then begin ; then goto,l55
                for ir=1,2 do begin
                  if(j2 eq jp2 and q eq 0 and ir eq 2) ne 1 then begin ; l60
                    m=m+1
                    ntab(m-1)=line.hanle.ntm(i-1)
                    j2tab(m-1)=j2
                    jp2tab(m-1)=jp2
                    ktab(m-1)=k
                    qtab(m-1)=q
                    irtab(m-1)=ir
                    rnutab(m-1)=rnujjp

                  endif         ;l60:
                endfor          ;60
              endif             ;l55:
            endfor              ;55
          endfor                ; 50
        endfor                  ; 40
      endfor                    ; 30 
    endfor                      ;20
    nunk=m  
    
;c----------------------------------------------------------------------
;c-----si introducono i parametri isti,idep
;c-----isti =1 se si vogliono considerare gli effeti di stimolazione
;c-----idep =1 se si vogliono introdurre le rates depolarizzanti (solo
;c-----        per il livello fondamentale e tutte uguali fra loro
;c-----        indipendentemente dal valore di K, con K.ne.0)
;c----------------------------------------------------------------------
    isti=1
    idep=0
    imag=1
    
                                ;matrices for storage of
                                ;height-independent data
    
    xa0_mat=dblarr(405,405,ntrans)   ;offset
    xa1_mat=dblarr(405,405,ntrans,2) ;to be multiplied with gei(i*2)*ae
    xa2_mat=dblarr(405,405,ntrans,2) ;to be multiplied with gei(i*2)*ae
    xa3_mat=dblarr(405,405,ntrans)   ;offset
    xa4_mat=dblarr(405,405,ntrans)   ;offset
    xa5_mat=dblarr(405,405,ntrans,2) ;to be multiplied with gei(i*2)*ae
    xa6_mat=dblarr(405,405,ntrans,2) ;to be multiplied with gei(i*2)*ae
    xa7_mat=dblarr(405,405,ntrans,2) ;to be multiplied with gei(i*2)*ae
    xa8_mat=dblarr(405,405,ntrans,2) ;to be multiplied with gei(i*2)*ae
    xa9_mat=dblarr(405,405,ntrans,2) ;to be multiplied with gei(i*2)*ae
    xa10_mat=dblarr(405,405,ntrans,2) ;to be multiplied with gei(i*2)*ae
    xa11_mat=dblarr(405,405)          ;offset
    xa12_mat=dblarr(405,405)          ;offset
    x0t10a_vec=dblarr(100000l)
    x0t10b_vec=dblarr(100000l)
    
    for nt=1,ntrans do begin    ;2000
      ae=aesto(nt-1)
      nterml=ntlsto(nt-1)
      ntermu=ntusto(nt-1)
      lu2=lsto2(ntermu-1)
      ll2=lsto2(nterml-1)
;c----------------------------------------------------------------------
;c-----Relaxation rates dovute all'emissione spontanea
;c----------------------------------------------------------------------
      for i=1,nunk do $
        if (ntab(i-1) eq ntermu) then begin
        a(i-1,i-1)=a(i-1,i-1) -ae
        xa0_mat(i-1,i-1,nt-1)=-ae
      endif
;c----------------------------------------------------------------------
;c-----Transfer rates dovute all'assorbimento
;c----------------------------------------------------------------------

      for i=1,nunk do begin     ;200
        if (ntab(i-1) eq ntermu) then begin
          ju2=j2tab(i-1)
          jup2=jp2tab(i-1)
          ku=ktab(i-1)
          ku2=ku*2
          qu=qtab(i-1)
          qu2=qu*2
          iru=irtab(i-1)
          for j=1,nunk do begin ;210
            if(ntab(j-1) eq nterml) then begin
              jl2=j2tab(j-1)
              if (abs(ju2-jl2) gt 2 or ju2+jl2 eq 0) eq 0 then begin
                jlp2=jp2tab(j-1)
                if (abs(jup2-jlp2) gt 2 or jup2+jlp2 eq 0) eq 0 then begin
                  ql=qtab(j-1)
                  ql2=ql*2
                  if (qu2 eq ql2) then begin
                    irl=irtab(j-1)
                    if(iru eq irl) then begin
                      kl=ktab(j-1)
                      kl2=kl*2
                      for kr=0,2,2 do begin ;220
                        kr2=kr*2
                        if (kr gt ku+kl or kr lt abs(ku-kl)) eq 0 then begin
                          x1=double(lu2+1)
                          x2=sqrt(double(3*(ju2+1)*(jup2+1)*(jl2+1)*(jlp2+1)* $
                                         (ku2+1)*(kl2+1)*(kr2+1)))
                          x3=1.d0
                          if (modulo(kl2+ql2+jlp2-jl2,4) ne 0) then x3=-1.d0
                          x4=w9js(ju2,jl2,2,jup2,jlp2,2,ku2,kl2,kr2)
                          x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                          x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                          x7=w3js(ku2,kl2,kr2,-qu2,ql2,0)
;                        x0=x1*x2*x3*x4*x5*x6*x7*ae*gei(kr)
                          xa1_mat(i-1,j-1,nt-1,kr/2)=x1*x2*x3*x4*x5*x6*x7
;                        a(i-1,j-1)=a(i-1,j-1)+x0
                        endif
                      endfor    ;220
                    endif
                  endif
                endif
              endif 
            endif 
          endfor                ;210
;c----------------------------------------------------------------------
;c-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
;c-----e per i rho^k_q(j,j') con j=j' e q < 0)
;c----------------------------------------------------------------------
          for j=1,nunk do begin ; 230
            if (ntab(j-1) eq nterml) then begin
              if (j2tab(j-1) eq jp2tab(j-1) and $
                  qtab(j-1) eq 0) eq 0 then begin
                jl2=jp2tab(j-1)
                if (abs(ju2-jl2) gt 2 or ju2+jl2 eq 0) eq 0 then begin
                  jlp2=j2tab(j-1)
                  if (abs(jup2-jlp2) gt 2 or jup2+jlp2 eq 0) eq 0 then begin
                    ql=-qtab(j-1)
                    ql2=ql*2
                    if (qu2 eq ql2) then begin
                      irl=irtab(j-1)
                      if (iru eq irl) then begin
                        kl=ktab(j-1)
                        kl2=kl*2
                        for kr=0,2,2 do begin ; 240
                          kr2=kr*2
                          if (kr gt ku+kl or kr lt abs(ku-kl)) eq 0 then begin
                            x1=double(lu2+1)
                            x2=sqrt(double(3*(ju2+1)*(jup2+1)*(jl2+1)* $
                                           (jlp2+1)*(ku2+1)*(kl2+1)*(kr2+1)))
                            x3=1.d0
                            if (modulo(kl2+ql2+jlp2-jl2,4) ne 0) then x3=-1.d0
                            x4=w9js(ju2,jl2,2,jup2,jlp2,2,ku2,kl2,kr2)
                            x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                            x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                            x7=w3js(ku2,kl2,kr2,-qu2,ql2,0)
;                          x0=x1*x2*x3*x4*x5*x6*x7*ae*gei(kr)
                            sign=1.d0
                            if (modulo(jl2-jlp2-ql2,4) ne 0) then sign=-1.d0
                            if (irl eq 1) then  sign0=sign
                            if (irl eq 2) then sign0=-sign
;                          a(i-1,j-1)=a(i-1,j-1)+sign0*x0
                            xa2_mat(i-1,j-1,nt-1,kr/2)=sign0*x1*x2*x3*x4*x5*x6*x7
                          endif  
                        endfor
                      endif 
                    endif 
                  endif 
                endif 
              endif 
            endif 
          endfor
        endif
      endfor                    ; 200
;c----------------------------------------------------------------------
;c-----Transfer rates dovute all'emissione spontanea
;c----------------------------------------------------------------------

      for  i=1,nunk do begin    ;300
        if (ntab(i-1) eq nterml) then begin
          jl2=j2tab(i-1)
          jlp2=jp2tab(i-1)
          kl=ktab(i-1)
          kl2=kl*2
          ql=qtab(i-1)
          ql2=ql*2
          irl=irtab(i-1)
          for j=1,nunk do begin ; 310
            if (ntab(j-1) eq ntermu) then begin
              ju2=j2tab(j-1)
              if (abs(ju2-jl2) gt 2 or ju2+jl2 eq 0) eq 0 then begin
                jup2=jp2tab(j-1)
                if (abs(jup2-jlp2) gt 2 or jup2+jlp2 eq 0) eq 0 then begin
                  iru=irtab(j-1)
                  if (iru eq irl) then begin
                    ku=ktab(j-1)
                    if(kl eq ku) then begin
                      qu=qtab(j-1)
                      if(ql eq qu) then begin
                        x1=double(lu2+1)
                        x2=sqrt(double((jl2+1)*(jlp2+1)*(ju2+1)*(jup2+1)))
                        x3=1.d0
                        if (modulo(2+kl2+jlp2+jup2,4) ne 0) then x3=-1.d0
                        x4=w6js(jl2,jlp2,kl2,jup2,ju2,2)
                        x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                        x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                        x0=x1*x2*x3*x4*x5*x6*ae
                        a(i-1,j-1)=a(i-1,j-1)+x0
                        xa3_mat(i-1,j-1,nt-1)=x0
                      endif
                    endif
                  endif
                endif
              endif
            endif
          endfor

;c----------------------------------------------------------------------
;c-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
;c-----e per i rho^k_q(j,j') con j=j' e q < 0)
;c----------------------------------------------------------------------

          for j=1,nunk do begin ; 330
            if (ntab(j-1) eq ntermu) then begin
              if(j2tab(j-1) eq jp2tab(j-1) and qtab(j-1) eq 0) eq 0 then begin
                ju2=jp2tab(j-1)
                if (abs(ju2-jl2) gt 2 or ju2+jl2 eq 0) eq 0 then begin
                  jup2=j2tab(j-1)
                  if (abs(jup2-jlp2) gt 2 or jup2+jlp2 eq 0) eq 0 then begin
                    iru=irtab(j-1)
                    if(iru eq irl) then begin
                      ku=ktab(j-1)
                      if(kl eq ku) then begin
                        qu=-qtab(j-1)
                        qu2=qu*2
                        if(ql eq qu) then begin
                          x1=double(lu2+1)
                          x2=sqrt(double((jl2+1)*(jlp2+1)*(ju2+1)*(jup2+1)))
                          x3=1.d0
                          if (modulo(2+kl2+jlp2+jup2,4) ne 0) then x3=-1.d0
                          x4=w6js(jl2,jlp2,kl2,jup2,ju2,2)
                          x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                          x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                          x0=x1*x2*x3*x4*x5*x6*ae
                          sign=1.d0
                          if (modulo(ju2-jup2-qu2,4) ne 0) then sign=-1.d0
                          if (iru eq 1) then sign0=sign
                          if (iru eq 2) then sign0=-sign
                          a(i-1,j-1)=a(i-1,j-1)+sign0*x0
                          xa4_mat(i-1,j-1,nt-1)=sign0*x0
                        endif
                      endif
                    endif
                  endif
                endif
              endif
            endif
          endfor
        endif 
      endfor                    ;300

;c----------------------------------------------------------------------
;c-----Relaxation rates dovute all'assorbimento
;c----------------------------------------------------------------------

      for i=1,nunk do begin     ; 400
        if (ntab(i-1) eq nterml) then begin
          j2=j2tab(i-1)
          jp2=jp2tab(i-1)
          k=ktab(i-1)
          k2=k*2
          q=qtab(i-1)
          q2=q*2
          ir=irtab(i-1)
          for j=1,nunk do begin ; 410
            if(ntab(j-1) eq nterml) then begin
              js2=j2tab(j-1)
              jt2=jp2tab(j-1)
              if (js2 ne j2 and jt2 ne jp2) eq 0 then begin
                kp=ktab(j-1)
                kp2=kp*2
                qp=qtab(j-1)
                if (q eq qp) then begin
                  qp2=qp*2
                  irp=irtab(j-1)
                  if (ir eq irp) then begin
                    for kr=0,2,2 do begin ; 420
                      kr2=kr*2
                      if(kr le ll2) then begin
                        if (kr gt k+kp or kr lt abs(k-kp)) eq 0 then begin
                          x1=double(lu2+1)
                          x2=sqrt(double(3*(k2+1)*(kp2+1)*(kr2+1)))
                          x3=1.d0
                          if (modulo(2+lu2-is2+j2+qp2,4) ne 0) then x3=-1.d0
                          x4=w6js(ll2,ll2,kr2,2,2,lu2)
                          x5=w3js(k2,kp2,kr2,q2,-qp2,0)
                          x6=x1*x2*x3*x4*x5
                          y0=0.d0
;c--------------------------------------------------------------------
;c-----questa parte si calcola solo se j e` uguale a js
;c--------------------------------------------------------------------
                          if (j2 eq js2) then begin
                            if (kr2 gt jp2+jt2 or kr2 lt abs(jp2-jt2)) eq 0 then begin
                              y1=sqrt(double((jp2+1)*(jt2+1)))
                              y2=w6js(ll2,ll2,kr2,jt2,jp2,is2)
                              y3=w6js(k2,kp2,kr2,jt2,jp2,j2)
                              y0=y1*y2*y3
                            endif
                          endif
;c---------------------------------------------------------------------
;c-----questa parte si calcola solo se jp e` uguale a jt
;c---------------------------------------------------------------------

                          if (jp2 eq jt2) then begin
                            if(kr2 gt j2+js2 or $
                               kr2 lt abs(j2-js2)) eq 0 then begin
                              y1=sqrt(double((j2+1)*(js2+1)))
                              y2=1.d0
                              if(modulo(js2-jp2+k2+kp2+kr2,4) ne 0) then y2=-1.d0
                              y3=w6js(ll2,ll2,kr2,js2,j2,is2)
                              y4=w6js(k2,kp2,kr2,js2,j2,jp2)
                              y0=y0+y1*y2*y3*y4
                            endif
                          endif
;                        x0=.5d0*x6*y0*ae*gei(kr)
;                        a(i-1,j-1)=a(i-1,j-1)-x0
                          xa5_mat(i-1,j-1,nt-1,kr/2)=-.5d0*x6*y0
                        endif
                      endif
                    endfor
                  endif
                endif
              endif
            endif
          endfor                ;410
;c----------------------------------------------------------------------
;c-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
;c-----e per i rho^k_q(j,j') con j=j' e q < 0)
;c----------------------------------------------------------------------
          for j=1,nunk do begin ;460 
            if (ntab(j-1) eq nterml) then begin
              if (j2tab(j-1) eq jp2tab(j-1) and qtab(j-1) eq 0) eq 0 then begin
                js2=jp2tab(j-1)
                jt2=j2tab(j-1)
                if (js2 ne j2 and jt2 ne jp2) eq 0 then begin
                  kp=ktab(j-1)
                  kp2=kp*2
                  qp=-qtab(j-1)
                  if(q eq qp) then begin
                    qp2=qp*2
                    irp=irtab(j-1)
                    if(ir eq irp) then begin
                      for kr=0,2,2 do begin ;470 
                        kr2=kr*2
                        if (kr le ll2) then begin
                          if(kr gt k+kp or kr lt abs(k-kp)) eq 0 then begin
                            x1=double(lu2+1)
                            x2=sqrt(double(3*(k2+1)*(kp2+1)*(kr2+1)))
                            x3=1.d0
                            if(modulo(2+lu2-is2+j2+qp2,4) ne 0) then x3=-1.d0
                            x4=w6js(ll2,ll2,kr2,2,2,lu2)
                            x5=w3js(k2,kp2,kr2,q2,-qp2,0)
                            x6=x1*x2*x3*x4*x5
                            y0=0.d0
;c--------------------------------------------------------------------
;c-----questa parte si calcola solo se j e` uguale a js
;c--------------------------------------------------------------------
                            if(j2 eq js2) then begin
                              if (kr2 gt jp2+jt2 or $
                                  kr2 lt abs(jp2-jt2)) eq 0 then begin
                                y1=sqrt(double((jp2+1)*(jt2+1)))
                                y2=w6js(ll2,ll2,kr2,jt2,jp2,is2)
                                y3=w6js(k2,kp2,kr2,jt2,jp2,j2)
                                y0=y1*y2*y3
                              endif
                            endif
;c---------------------------------------------------------------------
;c-----questa parte si calcola solo se jp e` uguale a jt
;c---------------------------------------------------------------------

                            if (jp2 eq jt2) then begin
                              if (kr2 gt j2+js2 or $
                                  kr2 lt abs(j2-js2)) eq 0 then begin
                                y1=sqrt(double((j2+1)*(js2+1)))
                                y2=1.d0
                                if(modulo(js2-jp2+k2+kp2+kr2,4) ne 0) then y2=-1.d0
                                y3=w6js(ll2,ll2,kr2,js2,j2,is2)
                                y4=w6js(k2,kp2,kr2,js2,j2,jp2)
                                y0=y0+y1*y2*y3*y4
                              endif
                            endif
;                          x0=.5d0*x6*y0*ae*gei(kr)
                            sign=1.d0
                            if(modulo(js2-jt2-qp2,4) ne 0) then sign=-1.d0
                            if(irp eq 1) then sign0=sign
                            if(irp eq 2) then sign0=-sign
;                          a(i-1,j-1)=a(i-1,j-1)-sign0*x0
                            xa6_mat(i-1,j-1,nt-1,kr/2)=-sign0*.5d0*x6*y0
                          endif  
                        endif  
                      endfor    ;470
                    endif
                  endif
                endif
              endif
            endif
          endfor                ;460
        endif 
      endfor                    ;400
; c----------------------------------------------------------------------
; c-----Se non si vogliono prendere in considerazione gli effetti di
; c-----stimolazione si e` finito
; c----------------------------------------------------------------------
; c      if(isti.ne.1) go to 1300
; c----------------------------------------------------------------------
; c-----Altrimenti si continua con le transfer rates e le relaxation 
; c-----rates dovute all'emissione stimolata
; c----------------------------------------------------------------------
; c----------------------------------------------------------------------
; c-----Transfer rates dovute all'emissione stimolata
; c----------------------------------------------------------------------

      for i=1,nunk do begin     ;1100
        if(ntab(i-1) eq nterml) then begin
          jl2=j2tab(i-1)
          jlp2=jp2tab(i-1)
          kl=ktab(i-1)
          kl2=kl*2
          ql=qtab(i-1)
          ql2=ql*2
          irl=irtab(i-1)
          for j=1,nunk do begin ; 1110
            if(ntab(j-1) eq ntermu) then begin
              ju2=j2tab(j-1)
              if(abs(jl2-ju2) gt 2 or jl2+ju2 eq 0) eq 0 then begin
                jup2=jp2tab(j-1)
                if(abs(jlp2-jup2) gt 2 or jlp2+jup2 eq 0) eq 0 then begin
                  qu=qtab(j-1)
                  qu2=qu*2
                  if(ql2 eq qu2) then begin
                    iru=irtab(j-1)
                    if(irl eq iru) then begin
                      ku=ktab(j-1)
                      ku2=ku*2
                      for kr=0,2,2 do begin ; 1120
                        kr2=kr*2
                        if(kr gt kl+ku or kr lt abs(kl-ku)) eq 0 then begin
                          x1=double(lu2+1)
                          x2=sqrt(double(3*(jl2+1)*(jlp2+1)*(ju2+1)* $
                                         (jup2+1)*(kl2+1)*(ku2+1)*(kr2+1)))
                          x3=1.d0
                          if(modulo(kr2+ku2+qu2+jup2-ju2,4) ne 0) then x3=-1.d0
                          x4=w9js(jl2,ju2,2,jlp2,jup2,2,kl2,ku2,kr2)
                          x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                          x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                          x7=w3js(kl2,ku2,kr2,-ql2,qu2,0)
;                        x0=x1*x2*x3*x4*x5*x6*x7*ae*gei(kr)
;                        a(i-1,j-1)=a(i-1,j-1)+x0
                          xa7_mat(i-1,j-1,nt-1,kr/2)=x1*x2*x3*x4*x5*x6*x7
                        endif
                      endfor    ;1120
                    endif
                  endif
                endif
              endif
            endif
          endfor                ;1110
; c----------------------------------------------------------------------
; c-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
; c-----e per i rho^k_q(j,j') con j=j' e q < 0)
; c----------------------------------------------------------------------

          for j=1,nunk do begin ; 1130
            if(ntab(j-1) eq ntermu) then begin
              if(j2tab(j-1) eq jp2tab(j-1) and qtab(j-1) eq 0) eq 0 then begin
                ju2=jp2tab(j-1)
                if(abs(jl2-ju2) gt 2 or jl2+ju2 eq 0) eq 0 then begin
                  jup2=j2tab(j-1)
                  if(abs(jlp2-jup2) gt 2 or jlp2+jup2 eq 0) eq 0 then begin
                    qu=-qtab(j-1)
                    qu2=qu*2
                    if(ql2 eq qu2) then begin
                      iru=irtab(j-1)
                      if(irl eq iru) then begin
                        ku=ktab(j-1)
                        ku2=ku*2
                        for kr=0,2,2 do begin ; 1140
                          kr2=kr*2
                          if(kr gt kl+ku or kr lt abs(kl-ku)) eq 0 then begin
                            x1=double(lu2+1)
                            x2=sqrt(double(3*(jl2+1)*(jlp2+1)*(ju2+1)* $
                                           (jup2+1)*(kl2+1)*(ku2+1)*(kr2+1)))
                            x3=1.d0
                            if(modulo(kr2+ku2+qu2+jup2-ju2,4) ne 0) then x3=-1.d0
                            x4=w9js(jl2,ju2,2,jlp2,jup2,2,kl2,ku2,kr2)
                            x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                            x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                            x7=w3js(kl2,ku2,kr2,-ql2,qu2,0)
;                          x0=x1*x2*x3*x4*x5*x6*x7*ae*gei(kr)
                            sign=1.d0
                            if(modulo(ju2-jup2-qu2,4) ne 0) then sign=-1.d0
                            if(iru eq 1) then  sign0=sign
                            if(iru eq 2) then sign0=-sign
;                          a(i-1,j-1)=a(i-1,j-1)+sign0*x0
                            xa8_mat(i-1,j-1,nt-1,kr/2)=sign0*x1*x2*x3*x4*x5*x6*x7
                          endif
                        endfor  ;1140
                      endif 
                    endif 
                  endif 
                endif 
              endif 
            endif 
          endfor                ;1130
        endif 
      endfor                    ;1100
; c----------------------------------------------------------------------
; c-----Relaxation rates dovute all'emissione stimolata
; c----------------------------------------------------------------------

      for i=1,nunk do begin     ;1200
        if(ntab(i-1) eq ntermu) then begin
          j2=j2tab(i-1)
          jp2=jp2tab(i-1)
          k=ktab(i-1)
          k2=k*2
          q=qtab(i-1)
          q2=q*2
          ir=irtab(i-1)
          for j=1,nunk do begin ;1210
            if(ntab(j-1) eq ntermu) then begin
              js2=j2tab(j-1)
              jt2=jp2tab(j-1)
              if(js2 ne j2 and jt2 ne jp2) eq 0 then begin
                kp=ktab(j-1)
                kp2=kp*2
                qp=qtab(j-1)
                if(q eq qp) then begin
                  qp2=qp*2
                  irp=irtab(j-1)
                  if(ir eq irp) then begin
                    for kr=0,2,2 do begin ;1220
                      kr2=kr*2
                      if(kr le lu2) then begin
                        if(kr gt k+kp or kr lt abs(k-kp)) eq 0 then begin
                          x1=double(lu2+1)
                          x2=sqrt(double(3*(k2+1)*(kp2+1)*(kr2+1)))
                          x3=1.d0
                          if(modulo(2+ll2-is2+j2+kr2+qp2,4) ne 0) then x3=-1.d0
                          x4=w6js(lu2,lu2,kr2,2,2,ll2)
                          x5=w3js(k2,kp2,kr2,q2,-qp2,0)
                          x6=x1*x2*x3*x4*x5
                          y0=0.d0
; c--------------------------------------------------------------------
; c-----questa parte si calcola solo se j e` uguale a js
; c--------------------------------------------------------------------

                          if(j2 eq js2) then begin
                            if(kr2 gt jp2+jt2 or $
                               kr2 lt abs(jp2-jt2)) eq 0 then begin
                              y1=sqrt(double((jp2+1)*(jt2+1)))
                              y2=w6js(lu2,lu2,kr2,jt2,jp2,is2)
                              y3=w6js(k2,kp2,kr2,jt2,jp2,j2)
                              y0=y1*y2*y3
                            endif
                          endif ; 1230 continue
; c---------------------------------------------------------------------
; c-----questa parte si calcola solo se jp e` uguale a jt
; c---------------------------------------------------------------------

                          if(jp2 eq jt2) then begin ; go to 1240
                            if(kr2 gt j2+js2 or $
                               kr2 lt abs(j2-js2)) eq 0 then begin
                              y1=sqrt(double((j2+1)*(js2+1)))
                              y2=1.d0
                              if(modulo(js2-jp2+k2+kp2+kr2,4) ne 0) then y2=-1.d0
                              y3=w6js(lu2,lu2,kr2,js2,j2,is2)
                              y4=w6js(k2,kp2,kr2,js2,j2,jp2)
                              y0=y0+y1*y2*y3*y4
                            endif
                          endif ; 1240 continue
;                        x0=.5d0*x6*y0*ae*gei(kr)
;                        a(i-1,j-1)=a(i-1,j-1)-x0
                          xa9_mat(i-1,j-1,nt-1,kr/2)=-.5d0*x6*y0
                                ;1220 continue
                        endif 
                      endif 
                    endfor      ;1220
                  endif 
                endif 
              endif 
            endif 
          endfor                ;1210
; c----------------------------------------------------------------------
; c-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
; c-----e per i rho^k_q(j,j') con j=j' e q < 0)
; c----------------------------------------------------------------------

          for j=1,nunk do begin ; 1260
            if(ntab(j-1) eq ntermu) then begin
              if(j2tab(j-1) eq jp2tab(j-1) and qtab(j-1) eq 0) eq 0 then begin
                js2=jp2tab(j-1)
                jt2=j2tab(j-1)
                if(js2 ne j2 and jt2 ne jp2) eq 0 then begin
                  kp=ktab(j-1)
                  kp2=kp*2
                  qp=-qtab(j-1)
                  if(q eq qp) then begin
                    qp2=qp*2
                    irp=irtab(j-1)
                    if(ir eq irp) then begin
                      for kr=0,2,2 do begin ; 1270
                        kr2=kr*2
                        if(kr le lu2) then begin
                          if(kr gt k+kp or kr lt abs(k-kp)) eq 0 then begin
                            x1=double(lu2+1)
                            x2=sqrt(double(3*(k2+1)*(kp2+1)*(kr2+1)))
                            x3=1.d0
                            if(modulo(2+ll2-is2+j2+kr2+qp2,4) ne 0) then x3=-1.d0
                            x4=w6js(lu2,lu2,kr2,2,2,ll2)
                            x5=w3js(k2,kp2,kr2,q2,-qp2,0)
                            x6=x1*x2*x3*x4*x5
                            y0=0.d0
; c--------------------------------------------------------------------
; c-----questa parte si calcola solo se j e` uguale a js
; c--------------------------------------------------------------------

                            if(j2 eq js2) then begin
                              if(kr2 gt jp2+jt2 or $
                                 kr2 lt abs(jp2-jt2)) eq 0 then begin
                                y1=sqrt(double((jp2+1)*(jt2+1)))
                                y2=w6js(lu2,lu2,kr2,jt2,jp2,is2)
                                y3=w6js(k2,kp2,kr2,jt2,jp2,j2)
                                y0=y1*y2*y3
                              endif
                            endif ; 1280    continue
; c---------------------------------------------------------------------
; c-----questa parte si calcola solo se jp e` uguale a jt
; c---------------------------------------------------------------------

                            if(jp2 eq jt2) then begin
                              if(kr2 gt j2+js2 or $
                                 kr2 lt abs(j2-js2)) eq 0 then begin
                                y1=sqrt(double((j2+1)*(js2+1)))
                                y2=1.d0
                                if(modulo(js2-jp2+k2+kp2+kr2,4) ne 0) then y2=-1.d0
                                y3=w6js(lu2,lu2,kr2,js2,j2,is2)
                                y4=w6js(k2,kp2,kr2,js2,j2,jp2)
                                y0=y0+y1*y2*y3*y4
                              endif
                            endif ; 1290    continue
;                          x0=.5d0*x6*y0*ae*gei(kr)
                            sign=1.d0
                            if(modulo(js2-jt2-qp2,4) ne 0) then sign=-1.d0
                            if(irp eq 1) then sign0=sign
                            if(irp eq 2) then sign0=-sign
;                          a(i-1,j-1)=a(i-1,j-1)-sign0*x0
                            xa10_mat(i-1,j-1,nt-1,kr/2)=-sign0*.5d0*x6*y0
                          endif  
                        endif  
                      endfor    ; 1270    continue
                    endif 
                  endif 
                endif 
              endif 
            endif 
          endfor                ; 1260    continue
        endif 
      endfor                    ;1200 
    endfor                      ; 2000 
    
; c----------------------------------------------------------------------
; c-----Si aggiunge il termine immaginario dovuto alla differenza di
; c-----energia fra i due livelli della coerenza
; c----------------------------------------------------------------------
    
    for i=1,nunk do begin       ;500
      if(j2tab(i-1) ne jp2tab(i-1)) then begin
        if(irtab(i-1) eq 1) then begin
          a(i-1,i+1-1)=a(i-1,i+1-1)+twopi*rnutab(i-1)
          xa11_mat(i-1,i+1-1)=xa11_mat(i-1,i+1-1)+twopi*rnutab(i-1)
        endif
        if(irtab(i-1) eq 2) then begin
          a(i-1,i-1-1)=a(i-1,i-1-1)-twopi*rnutab(i-1)
          xa11_mat(i-1,i-1-1)=xa11_mat(i-1,i-1-1)-twopi*rnutab(i-1)
        endif
      endif
    endfor                      ;500
    
; c-----------------------------------------------------------------------
; c-----Se idep=1, si aggiunge il termine dovuto alle collisioni 
; c-----depolarizzanti (solo per il livello fondamentale)
; c-----------------------------------------------------------------------

    if(idep eq 1) then begin    ;540
      for i=1,nunk do begin
        if(ntab(i-1) eq 1) then $
          if(ktab(i-1) ne 0) then begin
          a(i-1,i-1)=a(i-1,i-1)-delta
          xa12_mat(i-1,i-1)=-delta
        endif
      endfor
    endif                       ; 540  continue
    
; c==============================================================
; c     Do some calculations for am outside B-loop
; c     For inversion this needs to be only done once.
; c     LAter, for am calculation only matrix mult. is required 
; c==============================================================
    xam1_mat=dblarr(405,405)
    xam2_mat=dblarr(405,405)
    x41_mat=lonarr(405,405,4)
    x42_mat=lonarr(405,405,4)
    qqp1_mat=lonarr(405,405)
    qqp2_mat=lonarr(405,405)
    for i=1,nunk do begin       ; 620
      nterm=ntab(i-1)
      l2=lsto2(nterm-1)
      j2=j2tab(i-1)
      jp2=jp2tab(i-1)
      k=ktab(i-1)
      k2=k*2
      q=qtab(i-1)
      q2=q*2
      ir=irtab(i-1)
      for j=1,nunk do begin     ; 630
        if(ntab(j-1) eq nterm) then begin
          js2=j2tab(j-1)
          jt2=jp2tab(j-1)
          kp=ktab(j-1)
          if(abs(k-kp) gt 1 or k+kp eq 0) eq 0 then begin ; go to 630
            kp2=kp*2
            qp=qtab(j-1)
            if( abs(q-qp) le 1) then begin ;go to 630
              qp2=qp*2
              irp=irtab(j-1)
              x1=1.d0
              if(modulo(j2+jp2-qp2,4) ne 0) then x1=-1.d0
              x2=w3js(k2,kp2,2,-q2,qp2,q2-qp2)
              bigpar,l2,is2,k2,kp2,j2,jp2,js2,jt2,x3
              if(ir eq 1 and irp eq 1) then x41_mat(i-1,j-1,0)=1
              if(ir eq 1 and irp eq 2) then x41_mat(i-1,j-1,1)=1
              if(ir eq 2 and irp eq 1) then x41_mat(i-1,j-1,2)=1
              if(ir eq 2 and irp eq 2) then x41_mat(i-1,j-1,3)=1
              qqp1_mat(i-1,j-1)=qp-q
              x5=sqrt(double((k2+1)*(kp2+1)))
              xam1_mat(i-1,j-1)=x1*x2*x3*x5
            endif
          endif
;          endif
;        endfor                  ; 630  continue
; c----------------------------------------------------------------------
; c-----si ripete il loop (seconda passata per i rho^k_q(j,j') con j>j'
; c-----e per i rho^k_q(j,j') con j=j' e q < 0)
; c----------------------------------------------------------------------
          
          
;        for j=1,nunk do begin              ; 640
;          if(ntab(j-1) eq nterm) then begin ;go to 640
          if(j2tab(j-1) eq jp2tab(j-1) and qtab(j-1) eq 0) eq 0 then begin
            js2=jp2tab(j-1)
            jt2=j2tab(j-1)
            kp=ktab(j-1)
            if(abs(k-kp) gt 1 or k+kp eq 0) eq 0 then begin ; go to 640
              kp2=kp*2
              qp=-qtab(j-1)
              if( abs(q-qp) le 1) then begin ;go to 640
                qp2=qp*2
                irp=irtab(j-1)
                x1=1.d0
                if(modulo(j2+jp2-qp2,4) ne 0) then x1=-1.d0
                x2=w3js(k2,kp2,2,-q2,qp2,q2-qp2)
                bigpar,l2,is2,k2,kp2,j2,jp2,js2,jt2,x3
                if(ir eq 1 and irp eq 1) then x42_mat(i-1,j-1,0)=1
                if(ir eq 1 and irp eq 2) then x42_mat(i-1,j-1,1)=1
                if(ir eq 2 and irp eq 1) then x42_mat(i-1,j-1,2)=1
                if(ir eq 2 and irp eq 2) then x42_mat(i-1,j-1,3)=1
                qqp2_mat(i-1,j-1)=qp-q
                x5=sqrt(double((k2+1)*(kp2+1)))
                sign=1.d0
                if(modulo(js2-jt2-qp2,4) ne 0) then sign=-1.d0
                if(irp eq 1) then sign0=sign
                if(irp eq 2) then sign0=-sign
                xam2_mat(i-1,j-1)=x1*x2*x3*x5*sign0
              endif
            endif
          endif
        endif
      endfor                    ; 640  continue
    endfor                      ; 620  continue
    
                                ;calculate matrix x0t10a_vec
    nl=ntlsto(nemiss-1)
    nu=ntusto(nemiss-1)
    ll2=lsto2(nl-1)
    lu2=lsto2(nu-1)
    jminl2=abs(ll2-is2)
    jmaxl2=ll2+is2
    jminu2=abs(lu2-is2)
    jmaxu2=lu2+is2
                                ;dummy call to paschen to get njlevl
    e0(*)=0
    for j2=jminl2,jmaxl2,2 do $
      e0(j2)=energy(nl-1,j2)
    paschen,ll2,is2,e0,100.,njlevl,autl,cl
    e0(*)=0
    for j2=jminu2,jmaxu2,2 do $
      e0(j2)=energy(nu-1,j2)
    paschen,lu2,is2,e0,100.,njlevu,autu,cu
    
    x0=double(lu2+1)
    n0t10a=0l
    for ml2=-jmaxl2,jmaxl2,2 do begin   ; 3070
      for mu2=-jmaxu2,jmaxu2,2 do begin ; 3080
        q2=ml2-mu2
        if(abs(q2) le 2) then begin          ;go to 3080
          for mup2=-jmaxu2,jmaxu2,2 do begin ; 3090
            qp2=ml2-mup2
            if(abs(qp2) le 2) then begin ;go to 3090
              bigq2=q2-qp2
              bigq=bigq2/2
              bigqu2=mup2-mu2
              bigqu=bigqu2/2
              jal2=abs(ml2)
              if(jal2 lt jminl2) then jal2=jminl2
              for jl2=jal2,jmaxl2,2 do begin    ; 3100
                for jlp2=jal2,jmaxl2,2 do begin ; 3110
                  jau2=abs(mu2)
                  if(jau2 lt jminu2) then jau2=jminu2
                  for ju2=jau2,jmaxu2,2 do begin ; 3120
                    if(abs(ju2-jl2) gt 2 or ju2+jl2 eq 0) eq 0 then begin ;3120
                      for jus2=jau2,jmaxu2,2 do begin ; 3130 
                        jaup2=abs(mup2)
                        if(jaup2 lt jminu2) then jaup2=jminu2
                        for jup2=jaup2,jmaxu2,2 do begin ; 3140
                          if(abs(jup2-jlp2) gt 2 or $
                             jup2+jlp2 eq 0) eq 0 then begin ; go to 3140
                            x1=1.d0
                            if(modulo(2+jup2-mu2+qp2,4) ne 0) then x1=-1.d0
                            x2=sqrt(double((jl2+1)*(jlp2+1)*(ju2+1)*(jup2+1)))
                            x3=w3js(ju2,jl2,2,-mu2,ml2,-q2)
                            x4=w3js(jup2,jlp2,2,-mup2,ml2,-qp2)
                            x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                            x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                            for jsmalll=1,njlevl(ml2-njoff) do begin ; 3150
                              for jsmallu=1,njlevu(mu2-njoff) do begin ; 3160
                                for k=0,2 do begin ; 3170
                                  k2=k*2
                                  x8=w3js(2,2,k2,q2,-qp2,-bigq2)
                                  kumin=abs(jup2-jus2)/2
                                  kumax=(jup2+jus2)/2
                                  for ku=kumin,kumax do begin ; 3180
                                    ku2=ku*2
                                    x9=w3js(jup2,jus2,ku2,mup2,-mu2,-bigqu2)
                                    x10=sqrt(double(3*(k2+1)*(ku2+1)))
                                    n0t10a=n0t10a+1l
                                    x0t10a_vec(n0t10a-1)= $
                                      x0*x1*x2*x3*x4*x5*x6*x8*x9*x10
                                  endfor ; 3180 continue
                                endfor   ; 3170 continue
                              endfor     ; 3160 continue
                            endfor       ; 3150 continue
                          endif 
                        endfor  ; 3140 continue
                      endfor    ; 3130 continue
                    endif 
                  endfor        ; 3120 continue
                endfor          ; 3110 continue
              endfor            ; 3100 continue
            endif 
          endfor                ; 3090 continue
        endif 
      endfor                    ; 3080 continue
    endfor                      ; 3070 continue
    
    n0t10b=0l
    x0=double(lu2+1)
    for ml2=-jmaxl2,jmaxl2,2 do begin   ; 8070
      for mu2=-jmaxu2,jmaxu2,2 do begin ; 8080
        q2=ml2-mu2
        if(abs(q2) le 2) then begin          ;8080
          for mlp2=-jmaxl2,jmaxl2,2 do begin ; 8090
            qp2=mlp2-mu2
            if(abs(qp2) le 2) then begin ;go to 8090
              bigq2=q2-qp2
              bigq=bigq2/2
              bigql2=ml2-mlp2
              bigql=bigql2/2
              jau2=abs(mu2)
              if(jau2 lt jminu2) then jau2=jminu2
              for ju2=jau2,jmaxu2,2 do begin    ; 8100
                for jup2=jau2,jmaxu2,2 do begin ; 8110 
                  jal2=abs(ml2)
                  if(jal2 lt jminl2) then  jal2=jminl2
                  for jl2=jal2,jmaxl2,2 do begin ; 8120
                    if(abs(ju2-jl2) gt 2 or ju2+jl2 eq 0) eq 0 then begin
                      for jls2=jal2,jmaxl2,2 do begin ; 8130
                        jalp2=abs(mlp2)
                        if(jalp2 lt jminl2) then jalp2=jminl2
                        for jlp2=jalp2,jmaxl2,2 do begin ; 8140
                          if(abs(jup2-jlp2) gt 2 or $
                             jup2+jlp2 eq 0) eq 0 then begin
                            x1=1.d0
                            if(modulo(2+jls2-ml2+qp2,4) ne 0) then x1=-1.d0
                            x2=sqrt(double((jl2+1)*(jlp2+1)*(ju2+1)*(jup2+1)))
                            x3=w3js(ju2,jl2,2,-mu2,ml2,-q2)
                            x4=w3js(jup2,jlp2,2,-mu2,mlp2,-qp2)
                            x5=w6js(lu2,ll2,2,jl2,ju2,is2)
                            x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                            for jsmalll=1,njlevl(ml2-njoff) do begin ; 8150
                              for jsmallu=1,njlevu(mu2-njoff) do begin ; 8160
                                onum0=autu(mu2-auoff(0),jsmallu-auoff(1))- $
                                  autl(ml2-auoff(0),jsmalll-auoff(1))
                                for k=0,2 do begin ; 8170
                                  k2=k*2
                                  x8=w3js(2,2,k2,q2,-qp2,-bigq2)
                                  klmin=abs(jlp2-jls2)/2
                                  klmax=(jlp2+jls2)/2
                                  for kl=klmin,klmax do begin ; 8180
                                    kl2=kl*2
                                    x9=w3js(jls2,jlp2,kl2,ml2,-mlp2,-bigql2)
                                    x10=sqrt(double(3*(k2+1)*(kl2+1)))
                                    n0t10b=n0t10b+1l
                                    x0t10b_vec(n0t10b-1)= $
                                      x0*x1*x2*x3*x4*x5*x6*x8*x9*x10
                                  endfor ; 8180 continue
                                endfor   ; 8170 continue
                              endfor     ; 8160 continue
                            endfor       ; 8150 continue
                          endif 
                        endfor  ; 8140 continue
                      endfor    ; 8130 continue
                    endif 
                  endfor        ; 8120 continue
                endfor          ; 8110 continue
              endfor            ; 8100 continue
            endif 
          endfor                ; 8090 continue
        endif 
      endfor                    ; 8080 continue
    endfor                      ; 8070 continue
  endif                         ;recalculation
end

pro hanle_calcprof,bfield,inc,azi,vlos,vdopp,vdamp,dslab,height,obs_par, $
                   wlin,nwlin,hanle_i,hanle_q,hanle_u,hanle_v,radcorrsolar, $
                   verbose
@common_maxpar  
@hanle_common  
;  openw,54,'hanleprof_idl.res'
  clight=2.997925d10
  pigr=acos(-1d)
  tras=pigr/180.d0
  twopi=2.d0*pigr
  
;  timecheck,'00'
  
  q=0 & q2=0 & qp=0 & qp2=0 & qu=0 &  qu2=0 & ql=0 & ql2=0
  bigq=0 & bigq2=0 & bigqu=0 & bigqu2=0 & bigql=0 & bigql2=0
  gei=dblarr(3)
  a=dblarr(405,405) & am=dblarr(405,405) & a0=dblarr(405,405)
  b=dblarr(405) & indx=intarr(405)
  dr=dblarr(3,3) & di=dblarr(3,3)
  doff=[-1,-1]                  ;offset for indices of di and dr
  dsur=dblarr(7,13,13) & dsui=dblarr(7,13,13)
  dslr=dblarr(7,13,13) & dsli=dblarr(7,13,13)
  dsoff=[0,-6,-6]               ;offset for ds array indices 
  tr=dblarr(3,5,4) & ti=dblarr(3,5,4)
  toff=[0,-2,0]                 ;offset for ti tr indices
  e0=dblarr(21)
  eps=dblarr(4,maxwl)
  eta=dblarr(4,maxwl)
  stokes=dblarr(4,maxwl)
  argvec=dblarr(maxwl)
  rhor=dblarr(13,13,13,25)
  rhoi=dblarr(13,13,13,25)
  rhomr=dblarr(13,13,13,25)
  rhomi=dblarr(13,13,13,25)
  rholr=dblarr(13,13,13,25)
  rholi=dblarr(13,13,13,25)   
  rhomlr=dblarr(13,13,13,25)
  rhomli=dblarr(13,13,13,25)
  roff=[0,0,0,-12]
  const=0.08386d
  
                                ;
  onum_flag=bytarr(41,11,41,11) & onum_id=dblarr(41,11,41,11)
  onum_cnt=0 & maxonumcnt=100
  vo_store=dblarr(maxonumcnt,maxwl)
  ga_store=dblarr(maxonumcnt,maxwl)
  
  
                                ;calculate frequency vector
  trasf=-wlsto(nemiss-1)*wlsto(nemiss-1)
;   onumvec=(wlin-wlsto(nemiss-1)*1e4)/trasf
;   dopshift_wl = vlos * wlsto(nemiss-1)*1e4 / clight  
  onumvec=(wlin-10829.0911d0)/trasf
  dopshift_wl = vlos * 10829.0911d0 / clight  
  
  hdbl=double(height)
  
                                ;construct a from predefined matrices 
  a=dblarr(405,405)
  for nt=1,ntrans do begin
    ae=aesto(nt-1)
;c==============================================================
;c     per ogni transizione considerata nel modello atomico chiama 
;c     la subroutine campo (linea 1105) che calcola barn e w al posto 
;c     di D3model.for 
;c     da qui fino alla linea 560 resta uguale a D3hanleprof
;c==============================================================
    campo,u1sto(nt-1),u2sto(nt-1),riosto(nt-1),wlsto(nt-1),fsto(nt-1), $
      hdbl,barn,w
    gei(0)=barn
    gei(2)=barn*w/sqrt(2.d0)
    a=a+xa0_mat(*,*,nt-1)+xa3_mat(*,*,nt-1)+xa4_mat(*,*,nt-1)
    for kr=0,2,2 do $
      a=a+(xa1_mat(*,*,nt-1,kr/2)*gei(kr)*ae+ $
           xa2_mat(*,*,nt-1,kr/2)*gei(kr)*ae+ $
           xa5_mat(*,*,nt-1,kr/2)*gei(kr)*ae+ $
           xa6_mat(*,*,nt-1,kr/2)*gei(kr)*ae+ $
           xa7_mat(*,*,nt-1,kr/2)*gei(kr)*ae+ $
           xa8_mat(*,*,nt-1,kr/2)*gei(kr)*ae+ $
           xa9_mat(*,*,nt-1,kr/2)*gei(kr)*ae+ $
           xa10_mat(*,*,nt-1,kr/2)*gei(kr)*ae)
  endfor
  a=a+xa11_mat+xa12_mat

;  timecheck,'01'
  
  chibd=double(azi)
  thetabd=double(inc)
  bgauss=double(bfield)
  
;   print,'chib = ',chibd
;   print,'thetab = ',thetabd
;   print,'b(gauss) = ',bgauss
  
  thb=thetabd*pigr/180.d0
  chb=chibd*pigr/180.d0
  rotmatk,1,0.d0,-thb,-chb,dr,di
  
  

  flarmor=bgauss*1.3996d6
  bcoeff=twopi*flarmor   
; c-----------------------------------------------------------------------
; c-----si azzera la matrice am(i,j)
; c-----------------------------------------------------------------------

  am(*,*)=0
; c-----------------------------------------------------------------------
; c-----si costruisce la matrice am(i,j)
; c-----------------------------------------------------------------------
  
  
                                ;apply predefined matrices
  am=bcoeff*(xam1_mat* $
             (x41_mat(*,*,0)*di(qqp1_mat*0-doff(0),qqp1_mat-doff(1)) + $
              x41_mat(*,*,1)*dr(qqp1_mat*0-doff(0),qqp1_mat-doff(1)) + $
              x41_mat(*,*,2)*(-dr(qqp1_mat*0-doff(0),qqp1_mat-doff(1))) + $
              x41_mat(*,*,3)*di(qqp1_mat*0-doff(0),qqp1_mat-doff(1))))
  am=am+bcoeff*(xam2_mat* $
                (x42_mat(*,*,0)*di(qqp2_mat*0-doff(0),qqp2_mat-doff(1)) + $
                 x42_mat(*,*,1)*dr(qqp2_mat*0-doff(0),qqp2_mat-doff(1)) + $
                 x42_mat(*,*,2)*(-dr(qqp2_mat*0-doff(0),qqp2_mat-doff(1))) + $
                 x42_mat(*,*,3)*di(qqp2_mat*0-doff(0),qqp2_mat-doff(1))))
  
;  timecheck,'02'
; c----------------------------------------------------------------------- 
; c-----Scrive in un file separato le matrici dei coefficienti
; c-----------------------------------------------------------------------
; c      open(unit=7,file='coeff.dat',status='unknown')
; c      do 800 i=1,nunk
; c      do 810 j=1,nunk
; c      if(imag.ne.1) write(7,7778) i,j,a(i,j)
; c 7778 format(2(1x,i3),3x,e15.7)
; c      if(imag.eq.1) write(7,7779) i,j,a(i,j),am(i,j)
; c 7779 format(2(1x,i3),2(3x,e15.7))
; c 810  continue
; c 800  continue
; c      close(unit=7)
; c-----------------------------------------------------------------------
; c-----si aggiunge la matrice magnetica alla matrice dei coefficienti
; c-----e si azzera il vettore b
; c-----------------------------------------------------------------------
  
  b(*)=0d
  a0=a+am
; 600  continue
; c-----------------------------------------------------------------------
; c-----si sostituisce la prima riga della matrice con l'equazione della
; c-----traccia (moltiplicata per 10**7)
; c-----------------------------------------------------------------------

  a0(1-1,1-1:nunk-1)=(ktab eq 0) * 1.d7*sqrt(double(j2tab+1))
;         for j=1,nunk do begin   ; 720
;           a0(1-1,j-1)=0.d0
;           if(ktab(j-1) eq 0) then begin
;             j2=j2tab(j-1)
;             a0(1-1,j-1)=1.d7*sqrt(double(j2+1))
;           endif
;         endfor                  ; 720  continue
  b(1-1)=1.d7
; c-----------------------------------------------------------------------
; c-----Soluzione
; c-----------------------------------------------------------------------
; c-----------------------------------------------------------------------
; c-----Chiama le subroutines (tratte da `Numerical Recipies') per
; c-----invertire la matrice
; c-----------------------------------------------------------------------
  
;;  timecheck,'02a'
;  ludcmp0,a0,nunk,indx,d
;  stop
  ludcmp1,a0,nunk,405,indx,d
;  a0o=a0 & nunko=nunk & indxo=indx & bbo=b
;  timecheck,'02b'
  lubksb1,a0,nunk,405,indx,b
;  timecheck,'02c'
;  lubksb0,a0,nunk,indx,b
;  timecheck,'03'
; c-----------------------------------------------------------------------
; c-----Scrive la soluzione
; c-----------------------------------------------------------------------

; c      open(unit=7,file='tanti.res',status='unknown')
; c      open(unit=112,file='112.res',status='unknown')
; c      do 950 i=1,nunk
; c      write(7,8888) i,ntab(i),j2tab(i),jp2tab(i),ktab(i),qtab(i),
; c     *              irtab(i),b(i)
; c 8888 format(7(1x,i5),1x,e15.7)
; c 950  continue
; c      close(unit=7)
; c-----------------------------------------------------------------------
; c-----Trova i profili dei parametri di Stokes a partire dalla soluzione
; c-----L'emissione e` calcolata secondo l'Eq.(7.71e) del libro.
; c-----Si noti che tale equazione e` valida nel riferimento del campo
; c-----magnetico
; c-----I profili sono inizialmente calcolati in numero d'onda
; c-----nell'intervallo (omin,omax)
; c-----------------------------------------------------------------------

  theta=thetad*tras
  chi=chid*tras
  gamma=gammad*tras
  tkq2,-gamma,-theta,-chi,chb,thb,0.d0,tr,ti
  
  
; c===============================================================
; c     a questo punto del programma D3hanleprof legge la lunghezza
; c     d'onda in Angstrom, pero' in wlsto  e' in microns
; c===============================================================
  wl=wlsto(nemiss-1)*1.d4
; c------------------------------------------------------------------------
; c-----trasforma la velocita` in numero d'onda e calcola la costante
; c-----di damping ridotta
; c------------------------------------------------------------------------
  dnum=vdopp*1.d5/(wl*1.d-8*clight)
                                ;use non-thermal broadening only if
                                ;vdamp is explicitely specified in
                                ;input file
  if vdamp lt 0 then adamp=aesto(nemiss-1)/(dnum*4.d0*pigr*clight) $
  else adamp=double(vdamp)
; c-------------------------------------------------------------------------
; c-----si calcolano autovalori e autovettori
; c-------------------------------------------------------------------------
  nl=ntlsto(nemiss-1)
  nu=ntusto(nemiss-1)
  ll2=lsto2(nl-1)
  lu2=lsto2(nu-1)
  jminl2=abs(ll2-is2)
  jmaxl2=ll2+is2
  jminu2=abs(lu2-is2)
  jmaxu2=lu2+is2
; c---------------------------------------------------------------------------
; c-----si scrive in una grossa matrice la parte della soluzione che
; c-----interessa (la matrice densita` del termine superiore, nel caso
; c-----dell'emissione)
; c---------------------------------------------------------------------------
  e0(*)=0
  for j2=jminu2,jmaxu2,2 do $
    e0(j2)=energy(nu-1,j2)
  paschen,lu2,is2,e0,bgauss,njlevu,autu,cu
  e0(*)=0
  for j2=jminl2,jmaxl2,2 do $
    e0(j2)=energy(nl-1,j2)
  paschen,ll2,is2,e0,bgauss,njlevl,autl,cl
;  timecheck,'04'
  rhor(*,*,*,*)=0
  rhoi(*,*,*,*)=0
  
  for i=1,nunk do begin         ; 4040
    if(ntab(i-1) eq nu) then begin
      if(irtab(i-1) eq 1) then $
        rhor(j2tab(i-1)-roff(0),jp2tab(i-1)-roff(1), $
             ktab(i-1)-roff(2),qtab(i-1)-roff(3))=b(i-1)
      if(irtab(i-1) eq 2) then $
        rhoi(j2tab(i-1)-roff(0),jp2tab(i-1)-roff(1), $
             ktab(i-1)-roff(2),qtab(i-1)-roff(3))=b(i-1)
      if(j2tab(i-1) eq jp2tab(i-1)) then begin
        if(qtab(i-1) eq 0) then begin
          rhoi(j2tab(i-1)-roff(0),jp2tab(i-1)-roff(1), $
               ktab(i-1)-roff(2),qtab(i-1)-roff(3))=0.d0
        endif else begin
          sign=1.d0
          if(modulo(qtab(i-1),2) ne 0) then sign=-1.d0
          if(irtab(i-1) eq 1) then $ 
            rhor(j2tab(i-1)-roff(0),jp2tab(i-1)-roff(1), $
                 ktab(i-1)-roff(2),-qtab(i-1)-roff(3))=sign*b(i-1)
          if(irtab(i-1) eq 2) then $
            rhoi(j2tab(i-1)-roff(0),jp2tab(i-1)-roff(1), $
                 ktab(i-1)-roff(2),-qtab(i-1)-roff(3))=-sign*b(i-1)
        endelse
      endif else begin 
        sign=1.d0
        if(modulo(j2tab(i-1)-jp2tab(i-1)-2*qtab(i-1),4) ne 0) then $
          sign=-1.d0
        if(irtab(i-1) eq 1) then $
          rhor(jp2tab(i-1)-roff(0),j2tab(i-1)-roff(1), $
               ktab(i-1)-roff(2),-qtab(i-1)-roff(3))=sign*b(i-1)
        if(irtab(i-1) eq 2) then $
          rhoi(jp2tab(i-1)-roff(0),j2tab(i-1)-roff(1), $
               ktab(i-1)-roff(2),-qtab(i-1)-roff(3))=-sign*b(i-1)
      endelse
    endif
  endfor                        ; 4040 continue

;  timecheck,'05'
; c---------------------------------------------------------------------------
; c-----scrive i valori di rhor e rhoi (nel riferimento della verticale)
; c---------------------------------------------------------------------------

; c      open(unit=8,file='junk.dat')
; c     do 7777 j2=jminu2,jmaxu2,2
; c      do 7777 jp2=jminu2,jmaxu2,2
; c     kmin=iabs(jp2-j2)/2
; c      kmax=(jp2+j2)/2
; c      do 7777 k=kmin,kmax
; c      do 7777 q=-k,k
; c      write(8,1778) j2,jp2,k,q,rhor(j2,jp2,k,q),rhoi(j2,jp2,k,q)
; c 1778 format(4(1x,i3),2(1x,e15.7))
; c 7777 continue
; c      close(unit=8)
; c---------------------------------------------------------------------------
; c-----si trova la matrice densita` nel riferimento del campo magnetico
; c---------------------------------------------------------------------------
  kmax=jmaxu2
  rotmatsu,kmax,0.d0,-thb,-chb,dsur,dsui
  for j2=jminu2,jmaxu2,2 do begin          ; 4100
    for jp2=jminu2,jmaxu2,2 do begin       ;4110 
      kkmin=abs(j2-jp2)/2
      kkmax=(j2+jp2)/2
      for k=kkmin,kkmax do begin  ; 4120
        for q=-k,k do begin       ; 4130
          rhomr(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))=0.d0
          rhomi(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))=0.d0
          for qp=-k,k do begin  ;4140 
            comprod,rhor(j2-roff(0),jp2-roff(1),k-roff(2),qp-roff(3)), $
              rhoi(j2-roff(0),jp2-roff(1),k-roff(2),qp-roff(3)), $
              dsur(k-dsoff(0),q-dsoff(1),qp-dsoff(2)), $
              dsui(k-dsoff(0),q-dsoff(1),qp-dsoff(2)),x1,x2
            rhomr(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))= $
              rhomr(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))+x1
            rhomi(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))= $
              rhomi(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))+x2
          endfor                ; 4140 continue
        endfor                  ; 4130 continue
      endfor                    ; 4120 continue
    endfor                      ; 4110 continue
  endfor                        ; 4100 continue
;  timecheck,'06'
; c=========================================================================
; c     scrive i valori di rhomr e rhomi (nel riferimento mag)
; c========================================================================
; c      open(unit=89,file='kk.dat')
; c      do 7779 j2=jminu2,jmaxu2,2
; c      do 7779 jp2=jminu2,jmaxu2,2
; c        kmin=iabs(jp2-j2)/2
; c       kmax=(jp2+j2)/2
; c      do 7779 k=kmin,kmax
; c      do 7779 q=-k,k
; c	 write(89,7778) j2,jp2,k,q,rhomr(j2,jp2,k,q),rhomi(j2,jp2,k,q)
; c 7779 continue
; c      close(unit=89)

; c---------------------------------------------------------------------------
; c-----azzera i profili
; c---------------------------------------------------------------------------
  
  eps(*,*)=0d
; c---------------------------------------------------------------------------
; c-----si fa la somma dell'equazione (7.71e) [(7.71b)]
; c---------------------------------------------------------------------------

  nloop=0
  n0t10a=0l
  x0=double(lu2+1)

  for ml2=-jmaxl2,jmaxl2,2 do begin   ; 3070
;c         type *,ml2
    for mu2=-jmaxu2,jmaxu2,2 do begin ; 3080
      q2=ml2-mu2
      if(abs(q2) le 2) then begin                ;go to 3080
        for mup2=-jmaxu2,jmaxu2,2 do begin       ; 3090
          qp2=ml2-mup2
          if(abs(qp2) le 2) then begin ;go to 3090
            bigq2=q2-qp2
            bigq=bigq2/2
            bigqu2=mup2-mu2
            bigqu=bigqu2/2
            jal2=abs(ml2)>jminl2
            for jl2=jal2,jmaxl2,2 do begin          ; 3100
              for jlp2=jal2,jmaxl2,2 do begin       ; 3110
                jau2=abs(mu2)>jminu2
                for ju2=jau2,jmaxu2,2 do begin ; 3120
                  if(abs(ju2-jl2) gt 2 or ju2+jl2 eq 0) eq 0 then begin ;3120
                    for jus2=jau2,jmaxu2,2 do begin ; 3130 
                      jaup2=abs(mup2)>jminu2
                      for jup2=jaup2,jmaxu2,2 do begin ; 3140
                        if(abs(jup2-jlp2) gt 2 or $
                           jup2+jlp2 eq 0) eq 0 then begin ; go to 3140
;                                x1=1.d0
;                                if(modulo(2+jup2-mu2+qp2,4) ne 0) then x1=-1.d0
;                                x2=sqrt(double((jl2+1)*(jlp2+1)*(ju2+1)*(jup2+1)))
;                                x3=w3js(ju2,jl2,2,-mu2,ml2,-q2)
;                                x4=w3js(jup2,jlp2,2,-mup2,ml2,-qp2)
;                                x5=w6js(lu2,ll2,2,jl2,ju2,is2)
;                                x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                          for jsmalll=1,njlevl(ml2-njoff) do begin ; 3150
                            for jsmallu=1,njlevu(mu2-njoff) do begin ; 3160
                              if onum_flag(mu2-auoff(0),jsmallu-auoff(1),ml2-auoff(0),jsmalll-auoff(1)) eq 0 then begin
                                onum_flag(mu2-auoff(0),jsmallu-auoff(1),ml2-auoff(0),jsmalll-auoff(1))=1b
                                onum_id(mu2-auoff(0),jsmallu-auoff(1),ml2-auoff(0),jsmalll-auoff(1))=onum_cnt
                                onum0=autu(mu2-auoff(0),jsmallu-auoff(1))- $
                                  autl(ml2-auoff(0),jsmalll-auoff(1)) 
                                argvec(0:nwlin-1)= $
                                  (onum0-onumvec(0:nwlin-1))/dnum+dopshift_wl/dnum/trasf
                                pvoigt,adamp,argvec(0:nwlin-1),vo,ga & ga=ga*2
                                vo_store(onum_cnt,0:nwlin-1)=vo
                                ga_store(onum_cnt,0:nwlin-1)=ga
                                onum_cnt=onum_cnt+1
                                if onum_cnt ge maxonumcnt then message, $
                                  'Increase variable maxonumcnt'
                              endif
                              vo=vo_store(onum_id(mu2-auoff(0),jsmallu-auoff(1),ml2-auoff(0),jsmalll-auoff(1)),0:nwlin-1)
                              ga=ga_store(onum_id(mu2-auoff(0),jsmallu-auoff(1),ml2-auoff(0),jsmalll-auoff(1)),0:nwlin-1)
                              x7=cl(ml2-coff(0),jsmalll-coff(1),jl2-coff(2))*cl(ml2-coff(0),jsmalll-coff(1),jlp2-coff(2))*cu(mu2-coff(0),jsmallu-coff(1),ju2-coff(2))*cu(mu2-coff(0),jsmallu-coff(1),jus2-coff(2))
; ;                                 profilarch,adamp,arg,vo,ga
;if n_elements(onumstore) eq 0 then onumstore=onum0 else onumstore=[onumstore,onum0] 
                              for k=0,2 do begin ; 3170
;                                    k2=k*2
;                                    x8=w3js(2,2,k2,q2,-qp2,-bigq2)
                                kumin=abs(jup2-jus2)/2
                                kumax=(jup2+jus2)/2
                                for ku=kumin,kumax do begin ; 3180
                                  ku2=ku*2
;                                       x9=w3js(jup2,jus2,ku2,mup2,-mu2,-bigqu2)
;                                       x10=sqrt(double(3*(k2+1)*(ku2+1)))
                                  n0t10a=n0t10a+1l
                                  for istok=0,3 do begin ; 3200
                                    comprod,tr(k-toff(0),bigq-toff(1),istok-toff(2)),ti(k-toff(0),bigq-toff(1),istok-toff(2)),rhomr(jup2-roff(0),jus2-roff(1),ku-roff(2),bigqu-roff(3)),rhomi(jup2-roff(0),jus2-roff(1),ku-roff(2),bigqu-roff(3)),x11,x12
                                    comprod,x11,x12,vo,ga,x13,x14
                                    eps(istok,0:nwlin-1)=eps(istok,0:nwlin-1)+ $
                                      x0t10a_vec(n0t10a-1)*x7*x13
;                                          x0*x1*x2*x3*x4*x5*x6*x7*x8*x9*x10*x13
                                    nloop=nloop+1l
;c      if(mod(nloop,1000000).eq.0) type *,nloop

                                  endfor     ;3200 continue
                                endfor       ; 3180 continue
                              endfor         ; 3170 continue
                            endfor           ; 3160 continue
                          endfor             ; 3150 continue
                        endif 
                      endfor     ; 3140 continue
                    endfor       ; 3130 continue
                  endif 
                endfor          ; 3120 continue
              endfor            ; 3110 continue
            endfor              ; 3100 continue
          endif 
        endfor                  ; 3090 continue
      endif 
    endfor                      ; 3080 continue
  endfor                        ; 3070 continue
  
;  timecheck,'07'
; c==================================================================
; c     qui inizia la parte per aggiungere la polarizazione del
; c     termine inferiore
; c==================================================================
  
  rholr(*,*,*,*)=0d
  rholi(*,*,*,*)=0d
  
  for i=1,nunk do begin                  ; 7040
    if(ntab(i-1) eq nl) then begin       ;go to 7040
      if(irtab(i-1) eq 1) then $
        rholr(j2tab(i-1)-roff(0),jp2tab(i-1)-roff(1), $
              ktab(i-1)-roff(2),qtab(i-1)-roff(3))=b(i-1)
      if(irtab(i-1) eq 2) then $
        rholi(j2tab(i-1)-roff(0),jp2tab(i-1)-roff(1), $
              ktab(i-1)-roff(2),qtab(i-1)-roff(3))=b(i-1)
      if(j2tab(i-1) eq jp2tab(i-1)) then begin
        if(qtab(i-1) eq 0) then begin
          rholi(j2tab(i-1)-roff(0),jp2tab(i-1)-roff(1), $
                ktab(i-1)-roff(2),qtab(i-1)-roff(3))=0.d0
        endif else begin
          sign=1.d0
          if(modulo(qtab(i-1),2) ne 0) then sign=-1.d0
          if(irtab(i-1) eq 1) then $ 
            rholr(j2tab(i-1)-roff(0),jp2tab(i-1)-roff(1), $
                  ktab(i-1)-roff(2),-qtab(i-1)-roff(3))=sign*b(i-1)
          if(irtab(i-1) eq 2) then $
            rholi(j2tab(i-1)-roff(0),jp2tab(i-1)-roff(1), $
                  ktab(i-1)-roff(2),-qtab(i-1)-roff(3))=-sign*b(i-1)
        endelse
      endif else begin
        sign=1.d0
        if(modulo(j2tab(i-1)-jp2tab(i-1)-2*qtab(i-1),4) ne 0) then $
          sign=-1.d0
        if(irtab(i-1) eq 1) then $
          rholr(jp2tab(i-1)-roff(0),j2tab(i-1)-roff(1), $
                ktab(i-1)-roff(2),-qtab(i-1)-roff(3))=sign*b(i-1)
        if(irtab(i-1) eq 2) then $
          rholi(jp2tab(i-1)-roff(0),j2tab(i-1)-roff(1), $
                ktab(i-1)-roff(2),-qtab(i-1)-roff(3))=-sign*b(i-1)
      endelse
    endif
  endfor                        ; 7040 continue

  kmax=jmaxl2
  rotmatsu,kmax,0.d0,-thb,-chb,dslr,dsli
  for j2=jminl2,jmaxl2,2 do begin          ; 7100
    for jp2=jminl2,jmaxl2,2 do begin       ; 7110
      kkmin=abs(j2-jp2)/2
      kkmax=(j2+jp2)/2
      for k=kkmin,kkmax do begin  ; 7120
        for q=-k,k do begin       ; 7130
          rhomlr(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))=0.d0
          rhomli(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))=0.d0
          for qp=-k,k do begin  ; 7140
            comprod,rholr(j2-roff(0),jp2-roff(1),k-roff(2),qp-roff(3)), $
              rholi(j2-roff(0),jp2-roff(1),k-roff(2),qp-roff(3)), $
              dslr(k-dsoff(0),q-dsoff(1),qp-dsoff(2)), $
              dsli(k-dsoff(0),q-dsoff(1),qp-dsoff(2)),x1,x2
            rhomlr(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))= $
              rhomlr(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))+x1
            rhomli(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))= $
              rhomli(j2-roff(0),jp2-roff(1),k-roff(2),q-roff(3))+x2
          endfor                ; 7140 continue
        endfor                  ; 7130 continue
      endfor                    ; 7120 continue
    endfor                      ; 7110 continue
  endfor                        ; 7100 continue


;  timecheck,'08'

; c      open(unit=8,file='junk.dat')
; c	do 7777 j2=jminl2,jmaxl2,2
; c      do 7777 jp2=jminl2,jmaxl2,2
; c      kmin=iabs(jp2-j2)/2
; c      kmax=(jp2+j2)/2
; c      do 7777 k=kmin,kmax
; c      do 7777 q=-k,k
; c      write(8,1778) j2,jp2,k,q,rhomlr(j2,jp2,k,q),rhomli(j2,jp2,k,q)
; c 1778 format(4(1x,i3),2(1x,e15.7))
; c 7777 continue
; c      close(unit=8)

  eta(*,*)=0

  nloopa=0
  n0t10b=0l
  x0=double(lu2+1)
  for ml2=-jmaxl2,jmaxl2,2 do begin         ; 8070
    for mu2=-jmaxu2,jmaxu2,2 do begin       ; 8080
      q2=ml2-mu2
      if(abs(q2) le 2) then begin                ;8080
        for mlp2=-jmaxl2,jmaxl2,2 do begin       ; 8090
          qp2=mlp2-mu2
          if(abs(qp2) le 2) then begin ;go to 8090
            bigq2=q2-qp2
            bigq=bigq2/2
            bigql2=ml2-mlp2
            bigql=bigql2/2
            jau2=abs(mu2)>jminu2
            for ju2=jau2,jmaxu2,2 do begin          ; 8100
              for jup2=jau2,jmaxu2,2 do begin       ; 8110 
                jal2=abs(ml2)>jminl2
                for jl2=jal2,jmaxl2,2 do begin ; 8120
                  if(abs(ju2-jl2) gt 2 or ju2+jl2 eq 0) eq 0 then begin
                    for jls2=jal2,jmaxl2,2 do begin ; 8130
                      jalp2=abs(mlp2)>jminl2
                      for jlp2=jalp2,jmaxl2,2 do begin ; 8140
                        if(abs(jup2-jlp2) gt 2 or $
                           jup2+jlp2 eq 0) eq 0 then begin
;                               x1=1.d0
;                               if(modulo(2+jls2-ml2+qp2,4) ne 0) then x1=-1.d0
;                               x2=sqrt(double((jl2+1)*(jlp2+1)*(ju2+1)*(jup2+1)))
;                               x3=w3js(ju2,jl2,2,-mu2,ml2,-q2)
;                               x4=w3js(jup2,jlp2,2,-mu2,mlp2,-qp2)
;                               x5=w6js(lu2,ll2,2,jl2,ju2,is2)
;                               x6=w6js(lu2,ll2,2,jlp2,jup2,is2)
                          for jsmalll=1,njlevl(ml2-njoff) do begin ; 8150
                            for jsmallu=1,njlevu(mu2-njoff) do begin ; 8160
                              if onum_flag(mu2-auoff(0),jsmallu-auoff(1),ml2-auoff(0),jsmalll-auoff(1)) eq 0 then begin
                                onum_flag(mu2-auoff(0),jsmallu-auoff(1),ml2-auoff(0),jsmalll-auoff(1))=1b
                                onum_id(mu2-auoff(0),jsmallu-auoff(1),ml2-auoff(0),jsmalll-auoff(1))=onum_cnt
                                onum0=autu(mu2-auoff(0),jsmallu-auoff(1))- $
                                  autl(ml2-auoff(0),jsmalll-auoff(1)) 
                                argvec(0:nwlin-1)= $
                                  (onum0-onumvec(0:nwlin-1))/dnum+dopshift_wl/dnum/trasf
                                pvoigt,adamp,argvec(0:nwlin-1),vo,ga & ga=ga*2
                                vo_store(onum_cnt,0:nwlin-1)=vo
                                ga_store(onum_cnt,0:nwlin-1)=ga
                                onum_cnt=onum_cnt+1
                                if onum_cnt ge maxonumcnt then message, $
                                  'Increase variable maxonumcnt'
                              endif
                              vo=vo_store(onum_id(mu2-auoff(0),jsmallu-auoff(1),ml2-auoff(0),jsmalll-auoff(1)),0:nwlin-1)
                              ga=ga_store(onum_id(mu2-auoff(0),jsmallu-auoff(1),ml2-auoff(0),jsmalll-auoff(1)),0:nwlin-1)
                              x7=cl(ml2-coff(0),jsmalll-coff(1),jl2-coff(2))*cl(ml2-coff(0),jsmalll-coff(1),jls2-coff(2))*cu(mu2-coff(0),jsmallu-coff(1),ju2-coff(2))*cu(mu2-coff(0),jsmallu-coff(1),jup2-coff(2))
;if n_elements(onumstore) eq 0 then onumstore=onum0 else onumstore=[onumstore,onum0]
                              for k=0,2 do begin ; 8170
                                k2=k*2
;                                    x8=w3js(2,2,k2,q2,-qp2,-bigq2)
                                klmin=abs(jlp2-jls2)/2
                                klmax=(jlp2+jls2)/2
                                for kl=klmin,klmax do begin ; 8180
                                  kl2=kl*2
;                                       x9=w3js(jls2,jlp2,kl2,ml2,-mlp2,-bigql2)
;                                       x10=sqrt(double(3*(k2+1)*(kl2+1)))
                                  n0t10b=n0t10b+1l
                                  for istok=0,3 do begin ;8200 
                                    comprod,tr(k-toff(0),bigq-toff(1),istok-toff(2)),ti(k-toff(0),bigq-toff(1),istok-toff(2)),rhomlr(jls2-roff(0),jlp2-roff(1),kl-roff(2),bigql-roff(3)),rhomli(jls2-roff(0),jlp2-roff(1),kl-roff(2),bigql-roff(3)),x11,x12
                                    comprod,x11,x12,vo,ga,x13,x14
                                    eta(istok,0:nwlin-1)=eta(istok,0:nwlin-1)+ $
                                      x0t10b_vec(n0t10b-1)*x7*x13
;                                            x0*x1*x2*x3*x4*x5*x6*x7*x8*x9*x10*x13
                                    nloopa=nloopa+1l
;c     if(mod(nloopa,1000000).eq.0) type *,nloopa
                                  endfor     ; 8200 continue
                                endfor       ; 8180 continue
                              endfor         ; 8170 continue
                            endfor           ; 8160 continue
                          endfor             ; 8150 continue
                        endif 
                      endfor     ; 8140 continue
                    endfor       ; 8130 continue
                  endif 
                endfor          ; 8120 continue
              endfor            ; 8110 continue
            endfor              ; 8100 continue
          endif 
        endfor                  ; 8090 continue
      endif 
    endfor                      ; 8080 continue
  endfor                        ; 8070 continue
  
  
  stokes(*,*)=0d

;  timecheck,'09'
; c==================================================================
; c     qui calcola la variazione centro-bordo nel caso theta=/0.
; c     nel centro del disco ritheta=riosto(nemiss), cioe' l'intensita'
; c     del continuo dell'Allen.
; c     (wlsto(nemiss)**5)*const e' lo stesso fattore che si utilizza
; c     per rendere adimensionale barn
; c      ritheta=riosto(nemiss)*(1-u1sto(nemiss)*(1.d0-dcos(theta))
; c==================================================================

  ritheta=riosto(nemiss-1)*(1-u1sto(nemiss-1)*(1.d0-cos(theta))-$
                            u2sto(nemiss-1)*(1.d0-cos(theta)*cos(theta)))

  ritheta2=ritheta*(wlsto(nemiss-1)^5)*const
  
  
;  print,'I_0 =  ',ritheta2,ritheta,const
;         for io=1,nwlin do begin ; 3600
;           onum=onumvec(io-1)
;           wavel=trasf*onum
;           printf,54,format='(12(1x,e12.5))', $
;             wavel,eps(0:3,io-1),eta(0:3,io-1),thetabd,chibd,bgauss
;         endfor                  ; 3600 continue

;  timecheck,'10'


;  close,54

;  timecheck,'total'
  
                                ;compute stokes profiles from eta and eps
                                ;index 0,1,2,3 = I,Q,U,V
  etai=reform(eta(0,0:nwlin-1))
  etaq=reform(eta(1,0:nwlin-1))
  etau=reform(eta(2,0:nwlin-1))
  etav=reform(eta(3,0:nwlin-1))
  epsi=reform(eps(0,0:nwlin-1))
  epsq=reform(eps(1,0:nwlin-1))
  epsu=reform(eps(2,0:nwlin-1))
  epsv=reform(eps(3,0:nwlin-1))
  
  epsi0=ritheta2*radcorrsolar
  epsq0=0.
  epsu0=0.
  epsv0=0.

  tau=dslab*etai

  int=epsi0*exp(-tau)+(epsi/etai)*[1-exp(-tau)]

  qu=epsq0*exp(-tau)+(epsi/etai)*(epsq/epsi)*[1-exp(-tau)] $
    -(epsi/etai)*(etaq/etai)*[1-exp(-tau)]+(etaq/etai)*tau*exp(-tau) $
    *((epsi/etai)-epsi0)

  uu=epsu0*exp(-tau)+(epsi/etai)*(epsu/epsi)*[1-exp(-tau)]$
    -(epsi/etai)*(etau/etai)*[1-exp(-tau)]+(etau/etai)*tau*exp(-tau) $
    *((epsi/etai)-epsi0)

  vu=epsv0*exp(-tau)+(epsi/etai)*(epsv/epsi)*[1-exp(-tau)]$
    -(epsi/etai)*(etav/etai)*[1-exp(-tau)]+(etav/etai)*tau*exp(-tau) $
    *((epsi/etai)-epsi0)
  
  
  trasf=-wlsto(nemiss-1)*wlsto(nemiss-1) ; *1.d-4
  wavelength=trasf*onumvec(0:nwlin-1)+wlsto(nemiss-1)*1d4
  ic=max(int)
  ic=epsi0(0)
;   profile={wl:wavelength,i:int/ic,q:qu/ic,u:uu/ic,v:vu/ic}
  
  hanle_i=int/ic
  hanle_q=qu/ic
  hanle_u=uu/ic
  hanle_v=vu/ic
  
end

;compute profile with full hanle treatment in narrow slab module.
;based on hanleprof.f by Laura Merenda
pro hanle_module,init,line,b,inc,azi,vlosms,vdopp,vdamp,dslab,height, $
                 obs_par,wl,nwl, $
                 hanle_i,hanle_q,hanle_u,hanle_v, $
                 radcorrsolar,verbose
  
  if init eq 1 then begin       ;fill in all required varaibles, store
                                ;varaibles in common blocks
    hanle_init,line,obs_par,verbose
  endif
  vloscms=vlosms*100.
  hanle_calcprof,b,inc,azi,vloscms,vdopp,vdamp,dslab,height,obs_par,wl,nwl, $
    hanle_i,hanle_q,hanle_u,hanle_v,radcorrsolar,verbose
end
