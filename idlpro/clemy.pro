pro clemy
  physical_constants
  
  x=56 & y=192
  
;  make_sav,data_dir='atm_archive/atm_18may05.016-01cc_free',/over
;  make_sav,data_dir='atm_archive/atm_18may05.016-01cc_couple',/over
  restore,/v,'./sav//18may05.016-01cc_couple.pikaia.sav'
  idx=where(pikaia_result.fit.x eq x and pikaia_result.fit.y eq y)
  sc=statistic(idx)
  restore,/v,'./sav//18may05.016-01cc_free.pikaia.sav'
  idx=where(pikaia_result.fit.x eq x and pikaia_result.fit.y eq y)
  sf=statistic(idx)
  
  xs=n2s(x,format='(i4.4)')
  ys=n2s(y,format='(i4.4)')
  
  for i=0,1 do begin
    if i eq 1 then begin
      add='couple'
      st=sc
    endif else begin
      add='free'
      st=sf
    endelse
    ipt=read_ipt('atm_archive/atm_18may05.016-01cc_'+add+'/input.ipt')
    ipt.verbose=2
    observation=read_fitsobs(x=x+[0,1],y=y+[0,3],error=error, $
                             file=ipt.dir.profile+ipt.observation, $
                             ipt=ipt,silent=ipt.verbose le 1, $
                             norm_cont=ipt.norm_cont)
    dummy=max(st.fitness,imax)
    pix='.'+n2s(imax,format='(i5.5)')
    if imax eq 0 then pix=''
    atmfile='atm_archive/atm_18may05.016-01cc_'+ $
      add+'/x'+xs+'/x'+xs+'y'+ys+pix+'_atm.dat'
    cprof=atm_profile(atm=atmfile,ipt=ipt,wl=observation.wl)
    atm=read_atm(file=atmfile,header=header,init=1,ipt=ipt)
    print_atm,atm.fit.atm
    if i eq 0 then prof=cprof(0) else prof=[prof,cprof(0)]

  endfor

  if max(tag_names(ipt) eq 'SMOOTH') eq 1 then smoothval=ipt.smooth $
  else smoothval=0
  if smoothval ne 0 or ipt.median ne 0 then $
    observation=remove_noise(observation,filter=smoothval,mode=filter_mode, $
                             median=ipt.median)
                                ;WL-binning
  if ipt.wl_bin gt 1 then begin
    nwl=n_elements(observation.wl)
    if ipt.wl_bin ge nwl then begin
      message,/cont,'Error in WL-binning, bin-size: '+n2s(ipt.wl_bin)
      reset
    endif else begin
      iw=0
      for i=0,nwl-1,ipt.wl_bin do begin
        j=(i+ipt.wl_bin-1)<(nwl-1)
        observation.wl(iw)=total(observation.wl(i:j))/(j-i+1)
        observation.i(iw)=total(observation.i(i:j))/(j-i+1)
        observation.u(iw)=total(observation.u(i:j))/(j-i+1)
        observation.q(iw)=total(observation.q(i:j))/(j-i+1)
        observation.v(iw)=total(observation.v(i:j))/(j-i+1)
        iw=iw+1
      endfor
      observation={ic:observation.ic,wl:observation.wl(0:iw-1), $
                   i:observation.i(0:iw-1),q:observation.q(0:iw-1), $
                   u:observation.u(0:iw-1),v:observation.v(0:iw-1)}
    endelse
  endif
  
  observation.ic=1.
  userlct
  plot_profiles,observation,prof(0),prof(1),ipt=ipt,line=ipt.line

  window,2
  userlct
  
  fcmom=moment(sc.fitness)
  ffmom=moment(sf.fitness)

  plot,sf.fitness,yrange=minmaxp([sf.fitness,sc.fitness])
  oplot,color=1,sc.fitness

  print,'Coupled: mean fitness: ',fcmom(0),', SDEV (B)=',sqrt(sc.var.b)
  print,'  Free : mean fitness: ',ffmom(0),', SDEV (B)=',sqrt(sf.var.b)
  
;  print,max(sc.fitness),!c,max(sf.fitness),!c

  wset,4

end
