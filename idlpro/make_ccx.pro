;voigt function + derivatives for curvefit
PRO voigt_der,X,A,PROF,PDER
;
       ON_ERROR,0
;
;  Calculate the function.
;
       V = (X - A(1)) / A(2)
       AA = ABS(A(3) / A(2))
       PVOIGT, AA, V, H, F
       STREN = A(0)
       DOPPW = A(2)
       SQPI = SQRT(!PI)
 RAT = STREN / (DOPPW*SQPI)
 PROF = RAT*H
 IF N_PARAMS() EQ 3 THEN RETURN
;
;  Calculate the partial derivatives.
;
 PDER = FLTARR(N_ELEMENTS(X),4)
 DHDV = 2.*(AA*F - V*H)
 DHDA = 2.*(AA*H + V*F - 1./SQPI)
 DVDW = -V/DOPPW
 DADW = -AA/DOPPW
 PDER(*,0) = H/DOPPW/SQPI
 PDER(*,1) = -RAT*DHDV/DOPPW
 PDER(*,2) = -PROF/DOPPW + RAT*(DHDA*DADW + DHDV*DVDW)
 PDER(*,3) = RAT*DHDA/DOPPW
;
END

pro fe_voigt,bin,par,prof,pder

 voigt_der,bin,par(0:3),fe6301,fe6301der
 voigt_der,bin,par(4:7),fe6302,fe6302der

 der0=intarr(4) ## (bin*0)
 prof=fe6301+fe6302
 pder=[[fe6301der],[der0]]+[[der0],[fe6302der]]
end

;read calibrated Hinode fits files and create a CCX file containing
;continuum level and WL  calibration for every slit position
pro make_ccx,hinode_dir=hinode_dir,scan=scan,slit=slit,mask=mask, $
             level_quiet=level_quiet
 common fstore,fstore

 if n_elements(hinode_dir) eq 0 then begin
   print,'Routine to create auxiliary (.ccx) data files for Hinode.'
   print,'These files contain for every slit position:'
   print,' - the wavelength calibration (WL_OFF, WL_DISP, WL_NUM)'
   print,' - the continuum level for every single profile'
   print,'Usage:'
   print,'make_ccx,hinode_dir=''/data/slam/Hinode/SP4/10_11_2007/fits_files/'',level_quiet=0.001'
   print,'Additional parameters: scan=[1,3],slit=[10,30],mask=''SP4D20070909_1133*.fits'''
   retall
 endif
 
 if n_elements(mask) eq 0 then mask=['/*.fits','/*.fits.gz','/*.fits.Z']
 ff=file_search(hinode_dir+mask,count=cnt)
 if cnt eq 0 then begin
   print,'No Hinode fits files found in '+hinode_dir
   retall
 endif

 if n_elements(scan) eq 0 then scan=[0,0]
 if n_elements(scan) eq 1 then scan=[scan,scan]
 if n_elements(level_quiet) eq 0 then begin
   level_quiet=0.001
   print,'Using level_quiet='+n2s(level_quiet)
 endif
 if n_elements(slit) eq 0 then begin
   all=1
   slit=-1
 endif else all=0
 if n_elements(slit) eq 1 then slit=[slit,slit]
 scan=scan(sort(scan))
 slit=slit(sort(slit))

 !p.multi=0

 fe=[6301.5012d,6302.4936d]

 reread=0
 if n_elements(fstore) eq 0 then reread=1 else begin
   print,'Recalculate icont, wl_disp and wl_off? [y/N]'
   repeat key=get_kbrd() until key ne ''
   if strupcase(key) eq 'Y' then reread=1
 endelse

 sccnt=-1
 icnt=0l
 if reread then begin
   window,0 & userlct & erase   
   for i=0,cnt-1 do begin
     tfits=readfits_ssw(ff[i],header,/silent)
     tfits=hinode_corr(tfits,header)
     headst=fits_header2st(header)
                               ;look for correct scan number (count
                               ;the n-th occurence of the slitindx
     if headst.slitindx eq slit(0) then sccnt=sccnt+1
     if (headst.slitindx ge slit(0) and headst.slitindx le slit(1) and $
         sccnt ge scan(0) and sccnt le scan(1)) or all eq 1 then begin
       sz=size(tfits)
       nwl=sz(1)
       ny=sz(2)
       if n_elements(fst) eq 0 then $
         fst={wl_disp:0d,wl_off:0d,nwl:0, $
              icont:fltarr(ny),magsig:fltarr(ny),file:''}
       if n_elements(fstore) eq 0 then begin
         fstore=replicate(fst,cnt)
       endif else if n_elements(fstore) ne cnt then fstore=replicate(fst,cnt)
       icont=fltarr(ny)
       iprof=long(tfits(*,*,0))
       qprof=tfits(*,*,1)      ;tbc!
       uprof=tfits(*,*,2)      ;tbc!
       vprof=tfits(*,*,3)      ;tbc!
       for iy=0,ny-1 do begin
         dummy=max(iprof(*,iy),maxidx,/nan)
         icont(iy)=median(float(iprof((maxidx-5)>0:(maxidx+5)<(nwl-1),iy)))
       endfor

       magsig=sqrt(total(tfits(*,*,1:3)^2,1))/icont/nwl
       iquiet=where(magsig lt level_quiet)
       if n_elements(iquiet) eq 1 then begin
         print,'No quiet profiles (<'+n2s(level_quiet)+') found.'
         print,'Using 50 weakest profiles'
         iquiet=(sort(magsig))(0:49)
       endif

                               ;sum of quiet profiles
       if total(icont(iquiet)) ge 1e-5 then $
         prof=total(iprof(*,iquiet),2)/total(icont(iquiet)) $
       else prof=total(iprof(*,iquiet),2)
       plot,title='quiet profile '+ff[i],prof,/xst


                               ;do voigt fit to both lines
       wgt=1./prof       ;weighting scheme, max weight at line center
       if n_elements(par) eq 0 then  $ ;use parameters from last file
         par=[6d,30,2d,5.,4.,80,3d,5.]
       ifit=0
       repeat begin
         ifit=ifit+1
         fit=curvefit_lagg(dindgen(nwl),1d -prof,wgt,par,sigma,iter=iter, $
                      chisq=chisq,itmax=200,status=status, $
                      function_name='fe_voigt',tol=1d-12,/double)
         paruse=par
         pold=paruse & chiold=chisq & fitold=fit & itold=iter
         par=[6d,30,2d,5.,4.,80,3d,5.]
       endrep until chisq lt 1e-4 or ifit eq 2
       if ifit eq 2 then if chiold lt chisq then begin
         paruse=parold & chisq=chiold & fit=fitold & iter=itold
       endif
       par=paruse
       if status eq 0 then begin
         oplot,color=1,findgen(nwl),1.-fit
         wl_disp=(fe(1)-fe(0))/(par(5)-par(1))
         wl_off=fe(0)-wl_disp*par(1)
       endif else begin
         wl_disp=!values.f_nan
         wl_off=!values.f_nan
       endelse
       for il=0,1 do $
         oplot,color=3,linestyle=1,par(il*4+1)+[0,0],!y.crange
       print,i,': Chisqr=',chisq,', ITER=',iter,', WL_DISP=',wl_disp, $
         ', WL_OFF=',wl_off, $
         format='(i4.4,a,e8.2,a,i4,a,e12.6,a,f15.5)'
                               ;store wl-calib and continuum to
                               ;variable
       fstore(icnt).wl_disp=wl_disp
       fstore(icnt).wl_off=wl_off
       fstore(icnt).icont=icont
       fstore(icnt).magsig=magsig
       fstore(icnt).file=ff[i]
       fstore(icnt).nwl=nwl
       icnt=icnt+1
     endif else begin
       print,'slit/scan pos not found in '+ff[i]
     endelse
   endfor
 endif
 ncnt=max(where(fstore.file ne ''))+1
 sz=size(fstore.icont)
 nx=sz(2)
 ny=sz(1)

 print,'Total # of processed files: ',ncnt
 if ncnt le 0 then retall

                               ;look for variation of wl_disp, wl_off
 window,0 & userlct & erase
 !p.charsize=1.5
 !p.multi=[0,1,3]
 !x.margin=[15,12]
 
                               ;determine quiet sun continuum level
                               ;individually for the slit positions
 icont_qs=fltarr(ncnt)
 for i=0,ncnt-1 do begin
                               ;use the median value of the 50
                               ;highest value as the cont-level for
                               ;this slit position
   ihi=median((fstore[i].icont(*))(*),7)
   ihi=ihi(reverse(sort(ihi)))
   imed=median(ihi(0:49<(ny-1)))
   icont_qs[i]=imed
 endfor
 
                                ;do polynomial fits to offset and
                               ;dispersion
 x=dindgen(ncnt)
 deg=2>(6<(ncnt/10))           ;max degree 6
 print,'Do polynomial fit to WL_OFF, degree: ',deg
 fin=where(finite(fstore(0:ncnt-1).wl_off) and icont_qs ge 1e4)
 fitpar=poly_fit(x(fin),fstore(fin).wl_off,deg,chisq=chisq,yfit=fitoff, $
                 status=status)
 plot,/xst,ystyle=9,x,fstore.wl_off,title='WL_OFF', $
   ytitle='WL [A]',xtitle='Slit Pos'
 fitoff=fltarr(ncnt)
 for i=0,deg do fitoff=fitoff+fitpar[i]*(x^float(i))
 oplot,color=1,x,fitoff
 
                                ;convert wloff to m/s
 l0=total(!y.crange)/2.
 mpsrg=(!y.crange/l0-1.)*2.99792e8
 axis,yaxis=1,/yst,yrange=mpsrg,ytitle='m/s'

 deg=2                         ;max degree 2
 print,'Do polynomial fit to WL_DISP, degree: ',deg
 fin=where(finite(fstore(0:ncnt-1).wl_disp) and icont_qs ge 1e4)
 fitpar=poly_fit(x(fin),fstore(fin).wl_disp,deg,chisq=chisq,yfit=fitbin, $
                 status=status)
 plot,/xst,/yst,x,fstore(0:ncnt-1).wl_disp*1e3,title='WL_DISP', $
   ytitle='delta WL [mA]',xtitle='Slit Pos'
 fitbin=fltarr(ncnt)
 for i=0,deg do fitbin=fitbin+fitpar[i]*(x^float(i))
 oplot,color=1,x,fitbin*1e3
 
                                ;cont level
 nv=where(icont_qs le 1e-5)
 if nv(0) ne -1 then icont_qs(nv)=!values.f_nan
 icont_avg=median(icont_qs)
 userlct
 plot,icont_qs,title='Cont-Level for every slit', $
   xtitle='Slit Pos',ytitle='Icont',/xst,/yst
 deg=4
 print,'Do polynomial fit to ICONT_QS, degree: ',deg
 fin=where(finite(icont_qs))
 x=findgen(ncnt)
 fitpar=poly_fit(x(fin),icont_qs(fin),deg,chisq=chisq,yfit=icont_qs_fit, $
                 status=status)
 icont_qs_fit=fltarr(ncnt)
 for i=0,deg do icont_qs_fit=icont_qs_fit+fitpar[i]*(x^float(i))
 oplot,color=1,x,icont_qs_fit
 oplot,color=2,!x.crange,[0,0]+icont_avg
 if n_elements(where(icont_qs lt 1e3)) ge 5 then begin
   message,/cont,'WARNING: ICONT is less than 1e3 for some slit positions.'+$
     ' (Off-limb data?)'
   message,/cont,'The individual values will be used for ICONT!'
   icont_qs_fit=icont_qs
 endif

 window,2,xsize=ncnt,ysize=ys & userlct,/full & erase
 tvscl,fstore.icont
 userlct,/nologo
 xyouts,/normal,0,1,'!C ICONT',color=9

 window,3,xsize=ncnt,ysize=ys & userlct,/full & erase
 tvscl,fstore.magsig
 userlct,/nologo
 xyouts,/normal,0,1,'!C MAGSIG',color=9


 print,'Write out offset / dispersion /icont to ccx-files? [y/n] '
 repeat key=strupcase(get_kbrd()) until key eq 'Y' or key eq 'N'
 if key eq 'Y' then begin
   print,'Write out [p]olynomial fit or [i]ndividual values? [p/i] '
   repeat key=strupcase(get_kbrd()) until key eq 'I' or key eq 'P'
   if key eq 'I' then begin
     woff=fstore.wl_off
     wbin=fstore.wl_disp
     add=''
   endif else begin
     woff=fitoff
     wbin=fitbin
     icont_qs=icont_qs_fit
     add=' (poly-fit)'
   endelse
   for i=0,ncnt-1 do begin
     filex=fstore[i].file+'.ccx'
     header=''
     writefits,filex,fstore[i].icont,header,append=0
     naxpos=max(where(strpos(header,'NAXIS') eq 0))
     header=[header(0:naxpos), $
             'WL_NUM  = '+string(fstore[i].nwl,format='(i20)')+ $
             ' / WL-bins'+add, $
             'WL_OFF  = '+string(woff[i],format='(d20.8)')+ $
             ' / WL-Offset'+add, $
             'WL_DISP = '+string(wbin[i],format='(d20.8)')+ $
             ' / WL-Dispersion'+add, $
             'ICNTSLT = '+string(icont_qs[i],format='(d20.8)')+ $
             ' / Slit averaged quiet Sun Continuum'+add, $
             'ICNTAVG = '+string(icont_avg,format='(d20.8)')+ $
             ' / Total averaged quiet Sun Continuum'+add, $
               header(naxpos+1:*)]
       writefits,filex,fstore[i].icont,header,append=0
       print,'Wrote '+filex
   endfor
 endif

end
