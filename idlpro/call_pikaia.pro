;test pikaia minimization routine
function call_pikaia,line=cline,atm=catm,observation=cobservation, $
                     scale=cscale,weight=cweight,ncalls=ncalls, $
                     pikaia_pop=pikaia_pop, $
                     ret_atm=ret_atm,obs_par=cobs_par, $
                     fitness=fitness,verbose=verbose, $
                     voigt=voigt,magopt=magopt,old_norm=old_norm, $
                     use_geff=use_geff,hanle_azi=hanle_azi, $
                     use_pb=use_pb,pb_method=pb_method, $
                     blend=cblend,ret_blend=ret_blend, $
                     lsi=lspi,lsq=lspq,lsu=lspu,lsv=lspv, $
                     dols=dolsp,lspol=lspolp, $
                     gen=cgen,ret_gen=ret_gen, $
                     wlrg=wlrg,conv_func=conv_func,conv_nwl=conv_nwl, $
                     conv_mode=conv_mode,conv_wljitter=wljitter, $
                     prefilter_func=prefilter_func, $
                     prefilter_wlerr=prefilter_wlerr, $
                     iwl_compare=iwl_compareret,nwl_compare=nwl_compareret, $
                     norm_stokes_val=norm_stokes_val,conv_output=conv_output, $
                     old_voigt=old_voigt, $
                     statdim=statdim,statfit=statfit,statpar=statpar, $
                     statifit=statifit,statqfit=statqfit, $
                     statufit=statufit,statvfit=statvfit
  
  common scale,scale
  common atm,atm
  common blend,blend
  common gen,gen
  common weight,weight
  common observation,observation
  common pikline,line
  common profile_shape,profile_shape
  common quv_strength,quv_strength,istr,qstr,ustr,vstr,nfree,chi2mode
  common obs_par,obs_par
  common convcom,convval,doconv,cmode
  common prefiltercom,prefilterval,doprefilter
  common funcpar,iwl_compare,nwl_compare,compwl
  @common_localstray
  @common_localstraycalc
  @common_maxpar
  
  if keyword_set(voigt) then profile_shape='voigt' $
  else profile_shape='gauss'
  
  if n_elements(ncalls) eq 0 then ncalls=100
  atm=catm
  blend=cblend
  gen=cgen
  line=cline
  scale=cscale
  obs_par=cobs_par
  
  statdim=intarr(2)
  statpar=fltarr(pik_NMAX,pik_PMAX)
  statfit=fltarr(pik_PMAX)
  statifit=fltarr(maxwl,pik_PMAX)
  statqfit=fltarr(maxwl,pik_PMAX)
  statufit=fltarr(maxwl,pik_PMAX)
  statvfit=fltarr(maxwl,pik_PMAX)
  
                                ;fill lsprof variable
  cdols=dolsp
  clspol=lspolp
  lspol=lspolp
  if dols eq 1 then begin
    n=n_elements(cobservation.wl)-1
    lsprof.i(0:n)=lspi(0:n)
    lsprof.q(0:n)=lspq(0:n)
    lsprof.u(0:n)=lspu(0:n)
    lsprof.v(0:n)=lspv(0:n)
    lsprof.wl(0:n)=cobservation.wl(0:n)
  endif
 
                                ;read convolution function (needs
                                ;WL-vector from observation) 
  conv=read_conv(wlrg,conv_func,conv_nwl,conv_mode,wljitter, $
                 prefilter_func,cobservation.wl,verbose)
  
  
                                ;restrict fitted profile to the range
                                ;where weight is not 0 
  if conv.doconv eq 0 then begin
    wguse=(cweight.i gt 1e-5 or cweight.q gt 1e-5 or $
           cweight.u gt 1e-5 or cweight.v gt 1e-5)
    iwg=where(wguse eq 1)
  endif else iwg=-1
  
  
  if iwg(0) eq -1 then iwg=[0,n_elements(cweight.i)-1]
  iwmin=min(iwg) & iwmax=max(iwg)
  nwl=iwmax-iwmin+1
  
  cmode=conv.mode
  doconv=conv.doconv
  if conv.doconv eq 1 then begin
    cnwl=conv.nwl
    convval=conv.val(0:cnwl-1)
    compwl=conv.wl(0:cnwl-1)
  endif else begin
    convval=0.
    compwl=cobservation.wl(iwmin:iwmax)
  endelse
  
  ; observation={ic:cobservation.ic,wl:cobservation.wl(iwmin:iwmax), $
  ;              i:cobservation.i(iwmin:iwmax),q:cobservation.q(iwmin:iwmax), $
  ;              u:cobservation.u(iwmin:iwmax),v:cobservation.v(iwmin:iwmax)}
  observation={wl:cobservation.wl(iwmin:iwmax), $
               i:cobservation.i(iwmin:iwmax),q:cobservation.q(iwmin:iwmax), $
               u:cobservation.u(iwmin:iwmax),v:cobservation.v(iwmin:iwmax)}
  weight={wl:cweight.wl(iwmin:iwmax), $
          i:cweight.i(iwmin:iwmax),q:cweight.q(iwmin:iwmax), $
          u:cweight.u(iwmin:iwmax),v:cweight.v(iwmin:iwmax)}
  prefilter=read_prefilter(prefilter_func,prefilter_wlerr,compwl,verbose)
  prefilterval=prefilter.val
  doprefilter=prefilter.doprefilter
  
  fill_localstray,lsi,lsq,lsu,lsv,dols, $
    observation.wl,n_elements(observation.wl)

  
  
                                ;get weighting
  get_wgtstr,observation=observation,line=line,weight=weight
  
  iwl_compare=conv.iwl_compare
  nwl_compare=conv.nwl_compare
  
  if n_tags(observation) ne 5 and n_tags(observation) ne 6 then message, $
    'observation has to be a structure containing the wl,i,q,u,v profile'
  
  
  
  tn_scl=tag_names(scale)
  tn_atm=tag_names(atm(0).par)
  for i=0,n_tags(scale)-1 do if max(tn_atm eq tn_scl(i)) ne 1 then begin
    message,/cont,'atmosphere has to be same structure as scaling'
    help,/st,scale
    stop
  endif
  
                                ;define initial values for parameters
                                ;use medium values for all components
  na=n_elements(atm)
  for ia=0,na-1 do for it=1,n_tags(atm(0).par)-1 do $
    
  if atm(ia).fit.(it) then begin
    atm(ia).par.(it)=(atm(ia).par.(it)>scale(ia).(it).min)<scale(ia).(it).max
;     atm(ia).par.(it)=(scale(ia).(it).min + scale(ia).(it).max)/2.
    
  endif

                                ;convert atmosphere structure to
                                ;pikaia format (scaling and struct->vector)
  par=struct2pikaia(atm=atm,par=par,line=line,blend=blend,gen=gen)
  n=n_elements(par)
  
  ntotal=total(weight.i ge 1e-5)+total(weight.q ge 1e-5)+ $
    total(weight.u ge 1e-5)+total(weight.v ge 1e-5)
;  nfree=(4*n_elements(compwl)-n)>1
  nfree=(ntotal-n)>1
  
  if ncalls gt 0 then begin
;================================================================  
                                ;call pikaia
;================================================================  
    common share2,iv,iy,idum2
;
;     Sample driver program for pikaia.f
;
    NTAB=32 
    idum2=123456789
    iv=lonarr(NTAB) 
    iv(*)= 0
    iy=0

;
;     (two_d ("func" here) is an example fitness function, a 
;     smooth 2-d landscape)
;
;     First, initialize the random-number generator
;
;  read, "Random number seed (I*4)? ",seed
    seed=long(randomu(undefinedvar)*1e5)
    seed = -abs(long(seed))
    urand_init,seed
;
;     Set control variables (use defaults)
;
    ctrl = fltarr(12)
    ctrl(*) = -1
    
                                ;set some values optimized for the
                                ;he-line problem
    ctrl(0)=pikaia_pop
    ctrl(1)=ncalls
    ctrl(2)=5            ;significant bits (6 is default and should be
                                ;sufficient for real precission
    ctrl(3)=0.85 ;0.60 ;0.85
    ctrl(4)=2                   ;idl version only allows imut=1 or 2,
                                ;5 seems to converge better, but is
                                ;only available for the fortran version!
    ctrl(5)=0.01 ;0.005 ;0.01
    ctrl(6)=0.001 ;0.0005 ;0.001
    ctrl(7)=0.05 ; 0.01 ;0.05
;    ctrl(5)=0.005
;    ctrl(6)=0.0005
;    ctrl(7)=0.25
    if n_elements(verbose) eq 0 then verbose=0
    ctrl(11)=verbose            ;verbose
    
;
;     Now call pikaia
;
;**********************************************************************
;     note, I've had to change the call of all the procedures to not
;     include the function name, and the function throughout
;     the code is referred to as func. 
;**********************************************************************
;
    pikaia,n,ctrl,par,statpar,f,status
;
;     Print the results
    if verbose ge 2 then begin
      print," status: ",status
      print,"    par: ",par
      print,"      f: ",f
      print,"      ctrl: ",ctrl
    endif
    
    for i=0,ctrl[0]-1 do begin
      ptmp=reform(statpar[0:n-1,i])
      tline=line & tgen=gen & tblend=blend & tatm=atm
      
      
      tatm=struct2pikaia(/reverse,atm=tatm,par=ptmp, $
                         line=tline,blend=tblend,gen=tgen)
      tatm.fit=tatm.fitflag
      tatm=get_pikaia_idx(tatm,/silent)
      tline=get_pikaia_idx(tline,nadd=tatm(0).npar,/silent)
      tblend=get_pikaia_idx(tblend,nadd=tatm(0).npar+tline(0).npar,/silent)
      tgen=get_pikaia_idx(tgen,nadd=tatm(0).npar+tline(0).npar+ $
                                  tblend(0).npar,/silent)
      ptmp=struct2pikaia(reverse=0,atm=tatm, $
                         line=tline,blend=tblend,gen=tgen,/noscale)
      statpar[0:n_elements(ptmp)-1,i]=ptmp
                                ;stat-fitness only calculated in fortran version!
    endfor
    statdim[0]=n_elements(ptmp)
    statdim[1]=ctrl[0]
    
;================================================================  
                                ;end of pikaia call
;================================================================  
  endif else begin
    f=1
  endelse
  atm.par.ff=norm_ff(atm.par.ff,atm.fit.ff,atm.linid)
  
                                ;set angles according to allowed range
  for ia=0,na-1 do begin
    if (atm(ia).par.b lt 0) then begin
      atm(ia).par.b=-atm(ia).par.b
      atm(ia).par.inc=180.+atm(ia).par.inc
    endif
    atm(ia).par.width=abs(atm(ia).par.width)
    atm(ia).par.inc= abs((atm(ia).par.inc mod 360.))
    if (atm(ia).par.inc gt 180) then atm(ia).par.inc=360.-atm(ia).par.inc
    atm(ia).par.azi= atm(ia).par.azi mod 180.
    if (atm(ia).par.azi gt 90)  then atm(ia).par.azi=atm(ia).par.azi-180
    if (atm(ia).par.azi lt -90) then atm(ia).par.azi=atm(ia).par.azi+180
  endfor
  
  
  atm=struct2pikaia(/reverse,atm=atm,par=par,line=line,blend=blend,gen=gen)
  ret_atm=atm
  ret_blend=blend
  ret_gen=gen
  cline=line
  fitness=f
  
  if conv.doconv eq 0 then begin
    compwl=cobservation.wl
  endif
  
                                ;call compute profile for full WL-range
                                ;only WL of observations are returned.
                                ;since the local straylight is only defined for
                                ;the observed wavelength points!
                                ;prefilter_wlerr set to 0.
  prefilter=read_prefilter(prefilter_func,0.0,cobservation.wl,0)
  fill_localstray,lsi,lsq,lsu,lsv,dols, $
    cobservation.wl,n_elements(cobservation.wl)
  
  nrwl=conv.nwl_compare
  rwl=conv.iwl_compare
  if conv_output eq 1 then begin
    if dols eq 0 then begin
      nrwl=0
      rwl=-1
      if verbose ge 2 then $
        print,'Fitted profile is written out with CONV_NWL wavelength pixels'
    endif
  endif
  
  for is=0,statdim[1]-1 do begin
      ptmp=reform(statpar[0:n-1,is])
      tline=line & tgen=gen & tblend=blend & tatm=atm
      tatm=struct2pikaia(/reverse,atm=tatm,par=ptmp, $
                         line=tline,blend=tblend,gen=tgen,/noscale)
      tfp=compute_profile(atm=tatm,line=tline,wl=compwl,obs_par=obs_par, $
                          blend=tblend,gen=tgen, $
                          convval=convval,doconv=conv.doconv, $
                          conv_mode=conv.mode,$
                          lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv, $
                          dols=dols,lspol=lspol, $
                          prefilterval=prefilterval, $
                          doprefilter=doprefilter, $
                          ret_wlidx=rwl,nret_wlidx=nrwl)  
      tnwl=n_elements(tfp.i)
      statifit[0:tnwl-1,is]=tfp.i
      statqfit[0:tnwl-1,is]=tfp.q
      statufit[0:tnwl-1,is]=tfp.u
      statvfit[0:tnwl-1,is]=tfp.v
  endfor
  
  fp=compute_profile(atm=atm,line=line,wl=compwl,obs_par=obs_par, $
                     blend=blend,gen=gen,convval=convval,doconv=conv.doconv, $
                     conv_mode=conv.mode,$
                     lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv,dols=dols,lspol=lspol, $
                     prefilterval=prefilterval, $
                     doprefilter=doprefilter, $
                     ret_wlidx=rwl,nret_wlidx=nrwl)  
  if nrwl eq 0 then begin
    iwl_compareret=conv.iwl_compare
    nwl_compareret=conv.nwl_compare
  endif else begin
    nwl_compare=0
  endelse
  
  return,fp
  
end
