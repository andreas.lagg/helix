function fitsout_append,iptin,data
  common fitsout,fst,atm_data,prof_data,prof_ic,prof_wl,asciistore,asidx
@common_maxpar
 
  append=0b
  atm_data=readfits(fst.atm_file,header)
  
                                ;check # of fitparameters
  nparold=sxpar(header,'NFITPAR')
  if (nparold ne fst.npar) then begin
    print,'Cannot extend FITS file: Different number of free parameters.'
    reset
  endif
  
                                ;check name of fitparameters
  samepar=1b
  
  oldpar=strtrim(sxpar(header,'PAR*'))
  for i=0,nparold-1 do begin
    samepar=samepar and (strmid(oldpar(i),0,idlen-1) eq $
                         strmid(iptin.fitpar(i),0,idlen-1))
  endfor
  if (samepar eq 0) then begin
    print,'Cannot extend FITS file: Free parameters changed.'
    reset
  endif
  
                                ;size & offset of old data
  naxes=fix(sxpar(header,'NAXIS*'))
  xoffold=fix(sxpar(header,'XOFFSET'))
  yoffold=fix(sxpar(header,'YOFFSET'))
  xmm=[xoffold,xoffold+naxes(0)]
  ymm=[yoffold,yoffold+naxes(1)]
  xmmold=xmm
  ymmold=ymm
  
                                ;same size? -> do nothing
  if ((fst.xoff ge xoffold) and (fst.yoff ge yoffold) and $
      (fst.xoff+fst.nx le naxes(0)+xoffold) and $
      (fst.ny+fst.yoff le naxes(1)+yoffold) and $
      (fst.nz le naxes(2))) then begin
    fst.xoff=xoffold
    fst.yoff=yoffold
    fst.nx=naxes(0)
    fst.ny=naxes(1)
    fst.nz=naxes(2)
    print,format='(a,3i5)','Updating atmosphere FITS file. Size: ',naxes
    append=1b
  endif else begin
                                ;determine new size
    if (fst.xoff lt xmm(0)) then xmm(0)=fst.xoff
    if (fst.yoff lt ymm(0)) then ymm(0)=fst.yoff
    if (fst.xoff+fst.nx gt xmm(1)) then xmm(1)=fst.xoff+fst.nx
    if (fst.yoff+fst.ny gt ymm(1)) then ymm(1)=fst.yoff+fst.ny
    if (naxes(2) gt fst.nz) then nz=naxes(2)
    fst.nx=xmm(1)-xmm(0)
    fst.ny=ymm(1)-ymm(0)
    fst.xoff=xmm(0)
    fst.yoff=ymm(0)
    xmmold=xmmold-fst.xoff;+1
    ymmold=ymmold-fst.yoff;+1
    
    print,format='(a,3i5)','Extending atmosphere FITS file size to: ',naxes
    dataold=temporary(atm_data)
    atm_data=fltarr(fst.nx,fst.ny,fst.nz)
    atm_data(xmmold(0):xmmold(1)-1,ymmold(0):ymmold(1)-1,0:naxes(2)-1)=dataold
    naxes=[fst.nx,fst.ny,fst.nz]
    append=1b
  endelse
  return,append
end

function fitsout_pappend
  common fitsout,fst,atm_data,prof_data,prof_ic,prof_wl,asciistore,asidx
  
  append=0b
  prof_data=readfits(fst.prof_file,header)
;  prof_ic=mrdfits(fst.prof_file,1,hdric,status=status_icont,/silent)    
  prof_ic=readfits(fst.prof_file,hdric,exten=1,/silent)    
;  prof_wl=double(mrdfits(fst.prof_file,2,hdrwl,status=status_wl,/silent))
  prof_wl=double(readfits(fst.prof_file,hdrwl,exten=2,/silent))
  nwlold=sxpar(header,'NWL')
  if fst.nwl ne nwlold  then begin
    print,'Cannot extend profile FITS file: # of WL-points changed.'
    reset
  endif
 
  naxes=fix(sxpar(header,'NAXIS*'))
  xoffold=fix(sxpar(header,'XOFFSET'))
  yoffold=fix(sxpar(header,'YOFFSET'))
  if n_elements(naxes) eq 2 then naxes=[naxes,1,1]
  xmm=[xoffold,xoffold+naxes(2)]
  ymm=[yoffold,yoffold+naxes(3)]
  xmmold=xmm
  ymmold=ymm
  
   if ((fst.xoffp ge xoffold) and (fst.yoffp ge yoffold) and $
      (fst.xoffp+fst.nxp le naxes(2)+xoffold) and $
      (fst.nyp+fst.yoffp le naxes(3)+yoffold) and $
      (fst.nwl*fst.npixrep_prof eq naxes(0))) then begin
     fst.xoffp=xoffold
     fst.yoffp=yoffold
     fst.nzp=naxes(0)
     fst.npixrep_prof=fst.nzp/fst.nwl
     fst.nxp=naxes(2)
     fst.nyp=naxes(3)
     print,format='(a,4i5)','Updating profile FITS file. Size: ',naxes
    append=1b
  endif else begin
                                 ;determine new size
    if (fst.xoffp lt xmm(0)) then xmm(0)=fst.xoffp
    if (fst.yoffp lt ymm(0)) then ymm(0)=fst.yoffp
    if (fst.xoffp+fst.nxp gt xmm(1)) then xmm(1)=fst.xoffp+fst.nxp
    if (fst.yoffp+fst.nyp gt ymm(1)) then ymm(1)=fst.yoffp+fst.nyp
    fst.nxp=xmm(1)-xmm(0)
    fst.nyp=ymm(1)-ymm(0)
    fst.nzp=naxes(0)
    if naxes(0) lt fst.nwl*fst.npixrep_prof then $
      fst.nzp=fst.nwl*fst.npixrep_prof
    fst.npixrep_prof=fst.nzp/fst.nwl
    fst.xoffp=xmm(0)
    fst.yoffp=ymm(0)
    xmmold=xmmold-fst.xoffp;+1
    ymmold=ymmold-fst.yoffp;+1

    naxes=[fst.nzp,fst.nstokes,fst.nx,fst.ny]
    print,format='(a,4i5)','Extending profile FITS file size to: ',naxes
    dataold=temporary(prof_data)
    prof_data=fltarr(fst.nzp,fst.nstokes,fst.nxp,fst.nyp)
    prof_data(0:naxes(0)-1,0:3, $
              xmmold(0):xmmold(1)-1,ymmold(0):ymmold(1)-1)=dataold
    icold=temporary(prof_ic)
    prof_ic=fltarr(fst.nxp,fst.nyp)
    prof_ic(xmmold(0):xmmold(1)-1,ymmold(0):ymmold(1)-1)=icold
    wlold=temporary(prof_wl)
    prof_wl=dblarr(fst.nwl,fst.nxp,fst.nyp)
    prof_wl(0:fst.nwl-1,xmmold(0):xmmold(1)-1,ymmold(0):ymmold(1)-1)=wlold
    append=1b
  endelse
  return,append
end

pro fitsout_asciifile,fits_file,file,path,cmt
  common fitsout,fst,atm_data,prof_data,prof_ic,prof_wl,asciistore,asidx
  
  
  if asidx eq 0 then begin
    dowrite=1 
  endif else begin
    dowrite=max(asciistore(0:asidx-1) eq file) eq 0
  endelse
  
  if dowrite then begin
    openr,unit,/get_lun,path+file,error=error
    if error ne 0 then begin
      message,/cont,'Error in reading file: '+path+file
      return
    endif
    line=''
    repeat begin
      tl='' & readf,unit,tl
      line=[line,tl]
    endrep until eof(unit)
    free_lun,unit
    nl=n_elements(line)-1
    line=line(1:nl)
    
    
                                ;add ascii table extension
    len=max(strlen(line))
    len=(len/80 +1)*80
    h = strarr(15) + string(' ',format='(a80)')
    h[0] = 'END' + string(replicate(32b,77))
    sxaddpar, h, 'XTENSION', 'TABLE   ',' ASCII table extension'
    sxaddpar, h, 'BITPIX', 8,' 8 bit bytes'
    sxaddpar, h, 'NAXIS', 2,' 2-dimensional ASCII table'
    sxaddpar, h, 'NAXIS1', len,' Width of table in bytes'
    sxaddpar, h, 'NAXIS2', nl,' Number of rows in table'
    sxaddpar, h, 'PCOUNT', 0,' Size of special data area'
    sxaddpar, h, 'GCOUNT', 1,' one data group (required keyword)'
    sxaddpar, h, 'TFIELDS', 1,' Number of fields in each row'
    sxaddpar, h, 'TTYPE1', 'ASCII-Lines','label for field   1'
    sxaddpar, h, 'TBCOL1', 1,' beginning column of field   1'
    sxaddpar, h, 'TFORM1', 'A'+n2s(len),' Fortran-77 format of field'
    sxaddpar, h, 'EXTNAME', cmt,'name of this ASCII table extension'
    sxaddpar, h, 'ASCIINAM',file,'filename'
    sxaddpar, h, 'ASCIIPTH',path,'original path'
    
    
                                ;write new or updated fits file
    bipt=bytarr(len,nl)+32b
    for i=0,nl-1 do if strlen(line(i)) ge 1 then $
      bipt(0:strlen(line(i))-1,i)=byte(line(i))
;  writefits,fits_file,bipt,h,/append
    write_fits_dlm,bipt,fits_file,h,/append
    if asidx eq 0 then asciistore=file else asciistore=[asciistore,file]
    asidx=asidx+1
  endif
end  

  

pro fitsout_open,atm_file,prof_file,xin,yin,iptin
  @common_maxpar
  common fitsout,fst,atm_data,prof_data,prof_ic,prof_wl,asciistore,asidx
  
  fst.atm_file=atm_file
  fst.prof_file=prof_file
  fst.nx=xin(1)-xin(0)+1
  fst.ny=yin(1)-yin(0)+1
  fst.nxp=xin(1)-xin(0)+1
  fst.nyp=yin(1)-yin(0)+1
  fst.xoff=xin(0)
  fst.yoff=yin(0)
  fst.xoffp=xin(0)
  fst.yoffp=yin(0)
  
  fst.nwl=-1
  fst.nzp=-1
  
  fst.npar=iptin.nfitpar
  if iptin.pikaia_stat_cnt ge 1 then fst.npixrep_atm=iptin.pikaia_stat_cnt $
  else fst.npixrep_atm=iptin.pixelrep>1
  fst.npixrep_prof=1
  fst.verbose=iptin.verbose  
  
  naxis=3
  fst.nz=(fst.npar+1)*fst.npixrep_atm           ;parameters + fitness
  naxes=[fst.nx,fst.ny,fst.nz]
  fi=file_info(fst.atm_file)
  
  append=0b  
  if fi.exists ne 0 then begin
    print,'Atmosphere FITS File already exists: ',fst.atm_file
    append=fitsout_append(iptin,atm_data)
    if append eq 0 then begin
      print,'Please remove atmosphere FITS file first.'
      reset
    endif
    naxes=[fst.nx,fst.ny,fst.nz]
  endif else begin
    atm_data=fltarr(fst.nx,fst.ny,fst.nz)
  endelse
 
  mkhdr,header,4,naxes,/extend
  
;  @idlpro/version.txt
;  read_version,svn_datstr,svn_timestr,svn_revstr
  read_version,gitrevision
  
  sxaddpar,header,'CODE','HELIX+','inversion code'
  ;; sxaddpar,header,'RELDATE',svn_datstr,'SVN release date'
  ;; sxaddpar,header,'RELTIME',svn_timestr,'SVN release time'
  ;; sxaddpar,header,'REVISION',svn_revstr,'SVN reversion number'
  sxaddpar,header,'GITREV',gitrevision,'GIT reversion string'
  linfo=get_login_info()
  sxaddpar,header,'HOSTNAME',linfo.machine_name,'hostname'
  sxaddpar,header,'USERNAME',linfo.user_name,'username'
  
  sxaddpar,header,'XOFFSET',fst.xoff,'X-offset'
  sxaddpar,header,'YOFFSET',fst.yoff,'Y-offset'
  
                                ;write out input file as ascii comment
  fstatus=file_info(iptin.file)
  sxaddpar,header,'IPTFILE',iptin.file,'name of input file'
  sxaddpar,header,'IPTSIZE',fstatus.size,'input file size'
  sxaddpar,header,'IPTDATE',systime(0,fstatus.ctime),'input file mod. date'
  sxaddpar,header,'N_INPUT',n_elements(iptin.ascii), $
    'number of lines in input file'
  sxaddpar,header,'COMMENT','FREE PARAMETERS',after='XYZ'
  sxaddpar,header,'NFITPAR',fst.npar,'# of free parameters',after='XYZ'
  
  for i=0,iptin.nfitpar-1 do begin
    sxaddpar,header,'PAR'+n2s(i+1),iptin.fitpar(i),after='XYZ'
  endfor
  sxaddpar,header,'PAR'+n2s(iptin.nfitpar+1),'FITNESS',after='XYZ'    
  
                                ;write new or updated fits file
;  writefits,fst.atm_file,atm_data,header
  sza=size(atm_data)
  write_fits_dlm,atm_data,fst.atm_file,header
  
  asidx=0
  fitsout_asciifile,fst.atm_file,iptin.file,'','INPUTFILE'
  for i=0,maxatm-1 do for j=0,maxlines-1 do $
    if strlen(iptin.atom_file(i,j)) ge 2 then $
    fitsout_asciifile,fst.atm_file,iptin.atom_file(i,j),iptin.dir.atom,'ATOM'
  for i=0,maxmi-1 do if strlen(iptin.wgt_file(i)) ge 2 then begin
    fitsout_asciifile,fst.atm_file,iptin.wgt_file(i),iptin.dir.wgt,'WEIGHT'
  endif
  if strlen(iptin.prefilter) ge 2 then $
    fitsout_asciifile,fst.atm_file,iptin.prefilter,'','PREFILTER'
  if strlen(iptin.conv_func) ge 2 then $
    fitsout_asciifile,fst.atm_file,iptin.conv_func,'','CONVOLUTION'
end

pro fitsout_popen,stokes_nwl
  common fitsout,fst,atm_data,prof_data,prof_ic,prof_wl,asciistore,asidx
   
  fst.nstokes=4
  
  fst.nwl=stokes_nwl
  if fst.nzp lt fst.nwl*fst.npixrep_prof then fst.nzp=fst.nwl*fst.npixrep_prof
  fst.npixrep_prof=fst.nzp/fst.nwl
  naxis=4
  
  fi=file_info(fst.prof_file)
  append=0b  
  if fi.exists ne 0 then begin
    print,'Profile FITS File already exists: ',fst.prof_file
    append=fitsout_pappend()
    if append eq 0 then begin
      print,'Please remove profile FITS file first.'
      reset
    endif
  endif else begin
    prof_data=fltarr(fst.nzp,fst.nstokes,fst.nxp,fst.nyp)
    prof_ic=fltarr(fst.nxp,fst.nyp)
    prof_wl=dblarr(fst.nwl,fst.nxp,fst.nyp)
  endelse
  naxes=[fst.nzp,fst.nstokes,fst.nxp,fst.nyp]
  
  mkhdr,header,4,naxes,/extend
  sxaddpar,header,'CODE','HELIX+','inversion code'
  sxaddpar,header,'ATMFITS',fst.atm_file,'atmospheric data file'
  sxaddpar,header,'STOKES','IQUV','order of stokes parameters'
  sxaddpar,header,'XOFFSET',fst.xoffp,'X-offset'
  sxaddpar,header,'YOFFSET',fst.yoffp,'Y-offset'
  sxaddpar,header,'NWL',fst.nwl,'# of wavelength points'
  sxaddpar,header,'PIXELREP',fst.npixrep_prof,'max. pixel repetitions'
;  writefits,fst.prof_file,prof_data,header
  szp=size(prof_data)
  if szp[0] eq 2 then prof_data=reform(prof_data,szp[1],szp[2],1,1)
  if szp[0] eq 3 then prof_data=reform(prof_data,szp[1],szp[2],szp[3],1)
  write_fits_dlm,prof_data,fst.prof_file,header
                                ;continuum image
  mkhdr,headeric,4,[naxes(2),naxes(3)],/image
  sxaddpar,headeric,'ICONTIMG','','continuum image'
;  writefits,fst.prof_file,prof_ic,headeric,append=1
  write_fits_dlm,prof_ic,fst.prof_file,headeric,append=1
  mkhdr,headerwl,5,[naxes(0),naxes(2),naxes(3)],/image
  sxaddpar,headerwl,'WLVEC','','wavelength vector'
;  writefits,fst.prof_file,prof_wl,headerwl,append=1
  write_fits_dlm,prof_wl,fst.prof_file,headerwl,append=1
end

pro fitsout_aclose
  common fitsout,fst,atm_data,prof_data,prof_ic,prof_wl,asciistore,asidx
  
  if fst.verbose ge 1 then $
  print,'FITS-file (atmospheres) written: ',fst.atm_file
;  print,'FITSOUT-CLOSE ',fst.atm_file
end

pro fitsout_pclose
  common fitsout,fst,atm_data,prof_data,prof_ic,prof_wl,asciistore,asidx
  
  if fst.verbose ge 1 then $
  print,'FITS-file (profiles) written: ',fst.prof_file
;  print,'FITSOUT-CLOSE ',fst.prof_file
end

pro fitsout_close
  fitsout_aclose
  fitsout_pclose
end

pro fitsout_write,xp,yp,ipr,par,stokes,fitness,keepbest,nowrite=nowrite
  common fitsout,fst,atm_data,prof_data,prof_ic,prof_wl,asciistore,asidx
  common kb_count,kb_count
  
  if fst.nwl eq -1 then begin
    fitsout_popen,n_elements(stokes.wl)
  endif
  nowrite=keyword_set(nowrite)
  
  if ipr le 1 then kb_count=0
  
  oldfitness=atm_data(xp-fst.xoff,yp-fst.yoff,(ipr-1)*(fst.npar+1)+fst.npar)
  if keepbest eq 0 or fitness gt oldfitness then begin
    if oldfitness gt 0 and keepbest eq 1 then begin
      if fst.verbose ge 3 then $
        print,format='(''x'',i4.4,''y'',i4.4,'', REP='',i4.4,a,f8.3,a,f8.3)', $
              xp,yp,ipr,': KEEPBEST: old=',oldfitness,', new=',fitness
      kb_count=kb_count+1
    endif
    if fst.npar ge 1 then $
      atm_data(xp-fst.xoff,yp-fst.yoff, $
                (ipr-1)*(fst.npar+1):(ipr-1)*(fst.npar+1)+fst.npar)= $
      [par,fitness] $
    else atm_data(xp-fst.xoff,yp-fst.yoff,0)=fitness
    iprp=ipr
    dowrite=1b
                                ;for pixrep mode: write out only fittest profile
    if iprp ge fst.npixrep_prof then begin
      iprp=fst.npixrep_prof
      tfit=atm_data(xp-fst.xoff,yp-fst.yoff, $
                    (indgen((ipr-1)>1))*(fst.npar+1)+fst.npar)
      maxfit=max(tfit)
      dowrite=ipr eq 1 or fitness gt maxfit
    endif
    if dowrite then begin
      iwl=(iprp-1)*fst.nwl+[0,fst.nwl-1]
      prof_data(iwl(0):iwl(1),0,xp-fst.xoffp,yp-fst.yoffp)=stokes.i
      prof_data(iwl(0):iwl(1),1,xp-fst.xoffp,yp-fst.yoffp)=stokes.q
      prof_data(iwl(0):iwl(1),2,xp-fst.xoffp,yp-fst.yoffp)=stokes.u
      prof_data(iwl(0):iwl(1),3,xp-fst.xoffp,yp-fst.yoffp)=stokes.v
      prof_ic(xp-fst.xoffp,yp-fst.yoffp)=stokes.ic
      prof_wl(0:fst.nwl-1,xp-fst.xoffp,yp-fst.yoffp)=stokes.wl
    endif
    if nowrite eq 0 then begin
      modfits_al,fst.atm_file,atm_data
      modfits_al,fst.prof_file,prof_data
      modfits_al,fst.prof_file,prof_ic,exten_no=1
      modfits_al,fst.prof_file,prof_wl,exten_no=2
    endif
  endif
  if keepbest ge 1 and ipr eq fst.npixrep_atm and kb_count ge 1 then begin
     print,format='(''  x'',i4.4,''y'',i4.4,a,i3,''/'',i3,a)', $
           xp,yp,': KEEPBEST replaced ',kb_count,fst.npixrep_atm, $
           ' genes of PIKAIA population.'
  endif
end

pro fitsout
  common fitsout,fst,atm_data,prof_data,prof_ic,prof_wl,asciistore,asidx
  
  fst={atm_unit:0l,prof_unit:0l,atm_file:'',prof_file:'', $
       verbose:0,nx:0,ny:0,nz:0,npar:0,npixrep_atm:0,npixrep_prof:0, $
       xoff:0,yoff:0,nxp:0,nyp:0,nzp:0,nwl:0,xoffp:0,yoffp:0,nstokes:0}
  
end
