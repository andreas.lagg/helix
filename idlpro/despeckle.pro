;this programs allows to 'despeckle' maps inverted with helix. It
;uses the result of a successful run tests this map for spikes. The
;identified spike points are recalculated using an avaerage atmosphere
;from the surrounding pixels as a starting value.

pro despeckle,sav=sav,fits=fitsfile, $
              par=par,dev_perc=dev_perc,full=full,all=all,maxchisqr=maxchisqr, $
              minfitness=minfit,input=input,minval=minval,maxval=maxval, $
              comp=comp,position=pos,reread=reread,xy=xy,happy=happy, $
              smoothval=smoothval,default=default
  common pikaia,pikaia_result,oldfile,prpar
  
  
  if n_elements(par) eq 0 and n_elements(minfit) eq 0 and $
    keyword_set(default) eq 0and n_elements(maxchisqr) eq 0  then begin
    message,/cont,'No parameter given for despeckling'
    print,'Usage: '
    print,'  despeckle,sav=''./sav/21aug03.002_1comp_si_v01.pikaia.sav'',' + $
      'minfit=1.5,input=''xdisplay.ipt'''
    print,'or'
    print,'  despeckle,sav=''./sav/21aug03.002_1comp_si_v01.pikaia.sav'',' + $
      'par=''INC'',dev_perc=10.,position=[llx,lly,urx,ury]'
    print,'or'
    print,'  despeckle,sav=''./sav/13may01.019_bfree_1x1_noff_v01.pikaia.sav'',' + $
      'par=''DOPP'',maxval=0.55,input=''13may01.019_2comp_bfree_1x1_noff.ipt'',comp=1'
    print,'or'
    print,'  despeckle,fits=''./atm_archive/atm_30sep11.002lin-01cc_hesi_v05.fits'',' + $
      'par=''INC'',smooth=5,dev_perc=10,comp=1,/full'
    print,'or'
    print,'  despeckle,fits=''./atm_archive/atm_30sep11.002lin-01cc_hesi_v05.fits'',' + $
      '/default'
    print,'default is: /full,par=''FITNESS'',smooth=5,dev_perc=20.'
    reset
  endif
  
  if keyword_set(default) then begin
    full=1
    smoothval=5
    dev_perc=20.
    par='FITNESS'
  endif
  
; if n_elements(dev_perc) eq 0 then dev_perc=10.
  if n_elements(sav) eq 1 then begin
    if n_elements(oldfile) ne 0 then if oldfile eq sav then $
      reread=keyword_set(reread)
    if n_elements(reread) eq 0 then reread=1
    if reread then read_sav,sav
    oldfile=sav
    fitsmode=0
    allpar=[tag_names(pikaia_result.fit.atm.par),'FITNESS']
    if n_elements(input) ne 0 then ipt=read_ipt('./input/'+input) $
    else ipt=pikaia_result.input
    fitness=pikaia_result.fit.fitness
    if n_elements(comp) eq 0 then $
      comp=indgen(ipt.ncomp)
  endif else begin
    fitsmode=1
    common fits,fits
    read_fitsdata,fitsfile
    ipt=fits.ipt
    allpar=fits.par
    if fits.code eq 'helix' then begin
      FITPAR='FITNESS' 
      if n_elements(maxchisqr) ne 0 then minfit=temporary(maxchisqr)
    endif else begin
      FITPAR='CHISQ'
      if n_elements(minfit) ne 0 then maxchisqr=temporary(minfit)
    endelse
    ifit=(where(fits.par eq FITPAR))(0)
    fitness=fits.data(*,*,ifit)
    print,'Available components: Comp ',min(fits.comp),' - Comp ',max(fits.comp)
    if n_elements(comp) eq 0 then $
      comp=indgen(n_elements(where(allpar eq strupcase(fits.par(0)))))+1
  endelse
  if keyword_set(all) then all=1 else all=0
  ncomp=n_elements(comp)
  
  pidx=-1
  if n_elements(par) ne 0 then begin
    for ip=0,n_elements(par)-1 do begin
      pidx=[pidx,(where(allpar eq strupcase(par(ip))))(0)]
    endfor
    good=where(pidx ne -1)
    if max(good) eq -1 then begin
      message,/cont,'invalid parameter for despeckling of map: '+ $
        add_comma(par,sep=', ')
      message,/cont,'valid parameters are: '+ $
        add_comma([allpar,FITPAR],sep=', ')
      reset
    endif
    pidx=pidx(good)
    par=allpar(pidx)
    
    print,'Parameters for despeckling: '+add_comma(par,sep=', ')
    if n_elements(dev_perc) ne 0 then $
      print,'Max. deviation in percent of total range: '+n2s(dev_perc)
    if n_elements(minval) ne 0 then $
      print,'Min. value for parameters '+add_comma(par,sep=',')+': '+ $
      add_comma(n2s(minval),sep=',')
    if n_elements(maxval) ne 0 then $
      print,'Max. value for parameters '+add_comma(par,sep=',')+': '+ $
      add_comma(n2s(maxval),sep=',')
  endif else begin
    print,'minfit/maxchisqr mode'
    par=FITPAR
    pidx=(where(allpar eq strupcase(par)))(0)
  endelse
  if n_elements(minfit) ne 0 then print,'Minimum Fitness: ',n2s(minfit)
  if n_elements(maxchisqr) ne 0 then print,'Maximum Chisqr: ',n2s(maxchisqr)
  
;  ipt.verbose=0
  
  if fitsmode eq 1 then begin
    xcoff=fits.xoff
    xcexact=findgen(fits.nx)+fits.xoff
    ycoff=fits.yoff
    ycexact=findgen(fits.ny)+fits.yoff
  endif else begin
    list=get_proflist(ipt,xcrd=xcrd,xcexact=xcexact,/nolist, $
                      ycrd=ycrd,ycexact=ycexact,xcoff=xcoff,ycoff=ycoff)
    sz=size(fitness)
    xcexact=remove_multi(xcexact)
    ycexact=remove_multi(ycexact)
  endelse
  
  if fitsmode eq 1 then begin
    mapx=(intarr(fits.ny)+1) ## indgen(fits.nx) + fits.xoff
    mapy=indgen(fits.ny) ## (intarr(fits.nx)+1) + fits.yoff
  endif else begin
    mapx=resize(pikaia_result.fit.x,xcexact-xcoff,ycexact-ycoff)
    mapy=resize(pikaia_result.fit.y,xcexact-xcoff,ycexact-ycoff)
  endelse
  
  
  xbox=[-1,-1,-1,0,1,1,1,0]
  ybox=[-1,0,1,1,1,0,-1,-1]
  
  
                                ;mark region in plot for despeckling
  if par(0) eq FITPAR then map=fitness $
  else begin
    if fitsmode eq 1 then begin
      map=fits.data(*,*,pidx(0))
    endif else begin
      map=pikaia_result.fit.atm(comp).par.(pidx(0))
      map=resize(map,xcexact-xcoff,ycexact-ycoff)
    endelse
  endelse
  sz=size(map)
  
   if sz(1) lt 3 or sz(2) lt 3 then begin
     message,/cont,'Region too small for despeckle.'
     message,/cont,'Check your sav file or the XPOS/YPOS'+$
       ' keywords in your input file'
     reset
   endif
  
    
  screen=get_screen_size()
  fc=((screen(1)-100)/(sz(2)*ncomp))<6
  if fc*sz(1) ge screen(0) then fc=(screen(0)-100)/(sz(1))<6
  window,2,xsize=sz(1)*fc,ysize=sz(2)*fc*ncomp
  userlct,/full
  tv,bytscl(congrid(reform(map),sz(1)*fc,sz(2)*fc),top=254)+1, $
    0*sz(1)*fc,sz(2)*0*fc
  print,'Select region for despeckling (4 points):'
  print,'       (Right mouse button for whole map)'
  print
  
  posx=intarr(4)
  posy=intarr(4)
  ipos=0
  predef=n_elements(xy) eq 8 
  if predef then predef=(size(xy))(2) eq 4
  if predef then begin
      print,'Use predefined rectangle: xy='
      print,xy
      posx=reform(xy(0,*))
      posy=reform(xy(1,*))
  endif else if n_elements(pos) eq 4 then begin
    posx=(pos([0,0,2,2])>0)<sz(1)
    posy=(pos([1,3,3,1])>0)<sz(2)
  endif else begin
    repeat begin
      print,'Point '+n2s(ipos+1)+': ',format='(a,$)'
      if keyword_set(full) then button=4 else begin
        cursor,x,y,1,/down,/device
        button=!mouse.button
      endelse
      case button of
        1: begin
          posx(ipos)=x/fc
          posy(ipos)=y/fc
          print,'Pixel '+add_comma(n2s([posx(ipos),posy(ipos)]),sep='|')
        end
        4: begin
          ipos=3
          posx=[0,0,sz(1)-1,sz(1)-1]
          posy=[0,sz(2)-1,sz(2)-1,0]
          print,'All pixels'
        end
      endcase
      if ipos ge 1 then $
        plots,/device,[posx(ipos-1)+.5,posx(ipos)+.5]*fc, $
        [posy(ipos-1)+.5,posy(ipos)+.5]*fc
      ipos=ipos+1
    endrep until ipos ge 4
  endelse
  
  odev=!d.name
  set_plot,'Z'
  device,set_resolution=sz(1:2)>2
  erase
  polyfill,/device,[posx,posx(0)],[posy,posy(0)],color=1
  inarr=tvrd() eq 1
  set_plot,odev
  userlct,/full
  
  first=1
  for ip=0,n_elements(par)-1 do begin
    for iac=min(comp),max(comp) do begin
      ia=iac-min(comp)
      if fitsmode eq 1 then begin
        if par(ip) eq FITPAR then map=fitness $
        else begin
          tidx=(where(fits.par eq par(ip) and fits.comp eq iac))(0)
          map=fits.data(*,*,tidx)
        endelse
        fmap=fitness
      endif else begin
        if par(ip) eq FITPAR then map=fitness $
        else map=pikaia_result.fit.atm(ia).par.(pidx(ip))
        fmap=resize(fitness,xcexact-xcoff,ycexact-ycoff)
        map=resize(map,xcexact-xcoff,ycexact-ycoff)
      endelse
      if n_elements(smoothval) ne 0 then smap=smooth(map,smoothval)
      sz=size(map)
      rg=[min(map),max(map)]
      if max(iac eq comp) eq 1 then begin
        if n_elements(dev_perc) ne 0 then $
          smooth_perc=(rg(1)-rg(0))*dev_perc/100.
        if first then speckarr=intarr(ncomp,sz(1),sz(2))
        first=0
        for ix=0,sz(1)-1 do for iy=0,sz(2)-1 do if inarr(ix,iy) then begin
          
                                ;check minimum fitness
          if n_elements(minfit) ne 0 then if fmap(ix,iy) lt minfit(0) then $
            speckarr(ia,ix,iy)=speckarr(ia,ix,iy)+1
          if n_elements(maxchisqr) ne 0 then $
              if fmap(ix,iy) ge maxchisqr(0) then $
            speckarr(ia,ix,iy)=speckarr(ia,ix,iy)+1
          
                                ;check min and maxval of a given parameter
          if n_elements(minval) ne 0 then if map(ix,iy) lt minval(ip) then $
            speckarr(ia,ix,iy)=speckarr(ia,ix,iy)+1
          if n_elements(maxval) ne 0 then if map(ix,iy) gt maxval(ip) then $
            speckarr(ia,ix,iy)=speckarr(ia,ix,iy)+1
          
          if n_elements(dev_perc) ne 0 then begin
                                ;get surrounding elements
            xsurr=ix+xbox
            ysurr=iy+ybox
            ok=where(xsurr ge 0 and xsurr lt sz(1) and $
                     ysurr ge 0 and ysurr lt sz(2))
            if n_elements(ok) ge 5 then begin ;do not use edge points
              xsurr=xsurr(ok) & ysurr=ysurr(ok)
              surrvec=median(map(xsurr,ysurr),3)
              center=map(ix,iy)
                                ;is central pixel smooth?
              if center lt min(surrvec)-smooth_perc or $
                center gt max(surrvec)+smooth_perc or all then $
                speckarr(ia,ix,iy)=speckarr(ia,ix,iy)+1
            endif
          endif
          
                                ;check deviation from smoothed map
          if n_elements(smoothval) ne 0 then begin
            diff=(map(ix,iy)-smap(ix,iy))/smap(ix,iy)*100.
            if par(ip) eq FITPAR then diff=abs(diff<0) else diff=abs(diff)
            speckarr(ia,ix,iy)=speckarr(ia,ix,iy)+diff ge dev_perc
          endif
        endif
      endif
      tv,bytscl(congrid(reform(map),sz(1)*fc,sz(2)*fc),top=254)+1, $
        0*sz(1)*fc,sz(2)*ia*fc
      if first eq 0 then begin
        idx=where(reform(speckarr(ia,*,*)) ne 0)
        xcrd=idx mod sz(1)
        ycrd=idx / sz(1)
        plots,/device,xcrd*fc+fc*.5,ycrd*fc+fc*.5+sz(2)*ia*fc,psym=1
      endif
      plots,/device, $
        [posx(0),posx(1),posx(2)+1,posx(3)+1,posx(0)]*fc-[0,0,1,1,0], $
        [posy(0),posy(1)+1,posy(2)+1,posy(3),posy(0)]*fc-[0,1,1,0,0]+sz(2)*ia*fc
    endfor
  endfor
  
  
                                ;get indices of original map
  total_speck=total(speckarr,1) 
  speckidx=where(total_speck ne 0)
  if speckidx(0) ne -1 then begin
    ns=n_elements(speckidx)
    print,'Found '+n2s(ns)+' speckles.'
    list=transpose([[mapx(speckidx)],[mapy(speckidx)]])
    if fitsmode eq 0 then new_result=pikaia_result
    
    if keyword_set(happy) eq 0 then begin
      print,'Are you happy with the selection of the pixels [Y/N]?'
      repeat begin
        key=strupcase(get_kbrd(1))
      endrep until key eq 'Y' or key eq 'N'
      if key eq 'N' then begin
        print,'Please restart despeckle to change parameters'
        reset
      endif
    endif
    
    
    dsp_file='./lists/despeckle.list'
    openw,wunit,/get_lun,dsp_file,error=err
    if err eq 0 then begin
      printf,wunit,';despeckle profile list'
      printf,wunit,';use ''profile_list'' parameter in the input file'
      for ip=0l,ns-1 do $
        printf,wunit,n2s(mapx(speckidx(ip))),' ',n2s(mapy(speckidx(ip)))
      free_lun,wunit
      print,'Wrote profile_list to '+dsp_file
      print,'To use the profile list in the input file, add:' 
      print,'   PROFILE_LIST ./lists/despeckle.list'
    endif else message,/cont,'Error in writing out profile list to '+dsp_file
    
    if fitsmode then if fits.code eq 'spinor' then begin
      print,'Write out parameter map with interpolated values for bad pixels.'
      goodbyt=total(speckarr,1) eq 0
      speckidx=intarr(2,sz(1),sz(2))
                                ;find closest good pixel
      for ix=0,sz(1)-1 do for iy=0,sz(2)-1 do if goodbyt(ix,iy) eq 0 then begin
        distarr=sqrt( ((intarr(sz(2))+1) ## (findgen(sz(1))-ix))^2 + $
                      ((findgen(sz(2))-iy) ## (intarr(sz(1))+1))^2 )
        dummy=min(distarr/goodbyt,/nan,idx)
        speckidx(*,ix,iy)=[idx mod sz(1),idx / sz(1)]
      endif else speckidx(*,ix,iy)=[ix,iy]
      newdat=fits.data
      for ix=0,sz(1)-1 do for iy=0,sz(2)-1 do $
        newdat(ix,iy,*)=fits.data(speckidx(0,ix,iy),speckidx(1,ix,iy),*)
      slpos=(strpos(fitsfile,'/',/reverse_search))(0)
      newfile=strmid(fitsfile,0,slpos+1)+'new_'+ $
        strmid(fitsfile,slpos+1,strlen(fitsfile))
      fits_write,newfile,newdat,fits.header
      print,'Wrote parameter file '+newfile
      print,'Usage: in inv.inp add the line'
      print,'  ''FILE''   '''+newfile+''''
    endif
    
    
    if fitsmode eq 1 then begin
      reset
    endif
    
    if keyword_set(happy) eq 0 then begin
      if n_elements(input) eq 0 then $
        print,'Starting helix using original input file for speckles [Y/N]?' $
      else $
        print,'Starting helix using ipt='''+input+''' for speckles [Y/N]?'
      repeat begin
        key=strupcase(get_kbrd(1))
      endrep until key eq 'Y' or key eq 'N'
      if key eq 'N' then begin
        reset
      endif
    endif
    cnt=0
    
    pfmt='(i4.4)'
    dummy=file_search(ipt.dir.profile+'/'+ipt.observation+'/x????',count=cnt4)
    dummy=file_search(ipt.dir.profile+'/'+ipt.observation+'/x???',count=cnt3)
    if cnt4 eq 0 and cnt3 ne 0 then pfmt='(i3.3)'
  
    
    t=systime(1)
    print,'Started at '+systime(0)
    for i=0,ns-1 do begin
      
      ix=mapx(speckidx(i)) & iy=mapy(speckidx(i))
      xcrd=speckidx(i) mod sz(1)
      ycrd=speckidx(i) / sz(1)
     
      if speckidx(i)+1 lt n_elements(mapx) then xnxt=mapx(speckidx(i)+1) $
      else xnxt=n_elements(mapx)-1
      xavg=xnxt-mapx(speckidx(i))
      if speckidx(i)+1 lt n_elements(mapy) then ynxt=mapy(speckidx(i)+1) $
      else ynxt=n_elements(mapy)-1
      yavg=ynxt-mapy(speckidx(i))
      
      xsurr=ix+xbox*xavg
      ysurr=iy+ybox*yavg
      ok=where(xsurr ge 0 and ysurr ge 0)
      

      idx=where(pikaia_result.fit(xsurr(ok),ysurr(ok)).fitness gt 1e-4)
      if idx(0) ne -1 then begin
                                ;get initial atmosphere & straypol parameter
        fit=pikaia_result.fit(xsurr(ok(idx)),ysurr(ok(idx)))
        navg=n_elements(idx)
        atm_ini=fit(0).atm
        line_ini=ipt.line
         for ia=0,ncomp-1 do $
           for it=0,n_tags(fit.atm(ia).par)-1 do begin
           tn=(tag_names(fit.atm(ia).par))(it) 
           atm_ini(ia).par.(it)=total(fit(*).atm(ia).par.(it))/navg
          
           for il=0,ipt.nline-1 do begin
             for ip=0,n_tags(ipt.line(il).straypol_par)-1 do begin
               ipt.line(il).straypol_par(ia).(ip)= $
                 total(fit.straypol_par(ia,il).(ip))/navg
             endfor
             for ip=0,n_tags(ipt.line(il).par)-1 do $
               ipt.line(il).par.(ip)=total(fit.linepar(il).(ip))/navg
           endfor
         endfor
         
                                ;do not fit gauss-profiles for
                                ;stray-polarization
;         if ipt.straypol_corr ne 0 then begin
;            ipt.straypol_corr=0
;            ipt.nmi=ipt.nmi-1
;         endif
        
;        ipt.verbose=2
        
        helix,list=[ix,iy],struct_ipt=ipt,result=result,fitprof=fitprof ;,atm_ini=atm_ini
        ixp=ix-pikaia_result.input.x(0)
        iyp=iy-pikaia_result.input.y(0)
        print,'Old fitness: '+ $
          n2s( pikaia_result.fit(ixp,iyp).fitness,format='(f15.3)')+ $
          ', new fitness: '+n2s(result.fit.fitness,format='(f15.3)'), $
          format='(a,$)'
        clr=4
        if result.fit.fitness gt pikaia_result.fit(ixp,iyp).fitness then begin
          old=new_result.fit(ixp,iyp)
          struct_assign,result.fit,old
          new_result.fit(ixp,iyp)=old
          cnt=cnt+1
          print,'  REPLACED!',format='(a,$)'
          clr=15
                                ;output of atm and profile
          xstr='x'+n2s(ix,format=pfmt)
          ystr='y'+n2s(iy,format=pfmt)
          xyinfo=xstr+ystr
          resultname="/"+xstr+'/'+xyinfo+'.profile.dat'
          atm_path=ipt.dir.atm+'atm_'+ $
            ipt.observation
          if strcompress(ipt.dir.atm_suffix, $
                         /remove_all) ne '' then $
            atm_path=atm_path+'_'+ipt.dir.atm_suffix        
          write_atm,atm=result.fit.atm,line=result.line, $
            fitness=result.fit.fitness,fitprof=fitprof, $
            resultname=atm_path+resultname,xyinfo=xyinfo,ipt=result.input, $
            blend=result.fit.blend,gen=result.fit.gen
        endif
        wset,2
        for ia=0,ncomp-1 do $
          plots,/device,xcrd*fc+fc*.5,ycrd*fc+fc*.5+sz(2)*ia*fc, $
          psym=1,color=clr
      endif
      tend=(systime(1)-t)/((i+1)/float(ns))+t
      print,' End: '+conv_time(old='sec',new='date_dot',tend,/idl)
    endfor
    
    pikaia_result=new_result    
    print,n2s(cnt)+' results replaced in sav file.'
    save,file=sav,pikaia_result,/compress,/xdr
    print,'Stored in new sav-file: '+sav
  endif
;  write_all
end
