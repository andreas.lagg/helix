pro arr2prof
  
  restore,/v,'profile_archive/profiles_test.sav'
  sz=size(profiles)
  
  profile={wl:double(axis),y:indgen(sz(3)),x:0,ic:fltarr(1,sz(3))+1, $
        i:fltarr(sz(1),1,sz(3)),q:fltarr(sz(1),1,sz(3)), $
        u:fltarr(sz(1),1,sz(3)),v:fltarr(sz(1),1,sz(3))}
  profile.i=profiles(*,0,*)
  profile.q=profiles(*,1,*)
  profile.u=profiles(*,2,*)
  profile.v=profiles(*,3,*)
                                ;normalize profile
  for ix=0,1-1 do for iy=0,sz(3)-1 do begin
    profile.q(*,ix,iy)=profile.q(*,ix,iy)/profile.i(0,ix,iy)
    profile.u(*,ix,iy)=profile.u(*,ix,iy)/profile.i(0,ix,iy)
    profile.v(*,ix,iy)=profile.v(*,ix,iy)/profile.i(0,ix,iy)
    profile.i(*,ix,iy)=profile.i(*,ix,iy);+(1-max(profile.i(*,ix,iy)))
    profile.i(*,ix,iy)=profile.i(*,ix,iy)/profile.i(0,ix,iy)
  endfor
  
  save,/xdr,/compress,file='profile_archive/profiles_test_helix_norm.sav',profile
  
  plot,profile.wl,profile.i(*,0)
;  plot,profile.i(0,*,*)
  tv,profile.i(0,*,*)
end
