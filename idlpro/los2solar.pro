pro los2solar,btot=btot,gamma=gamma,chi=chiorig,obs=obs, $
              bx_sol=bx_sol,by_sol=by_sol,bz_sol=bz_sol, $
              inc_sol=inc_sol,azi_sol=azi_sol, $
              xoff=xoff,yoff=yoff,ambig=ambigin,window=window, $
              xval=xval,yval=yval, $
              init=init,header=header, $
              tip=tip,hinode=hinode,imax=imax, $
              findsmooth=findsmooth;,mininc=mininc,maxinc=maxinc,smooth=smooth
  common cnvst,cnvst
  common ambig_start,ambig
  common ambiwg,ambwg,amhelp
  common pikaia,pikaia_result,oldsav,prpar
 
  if (keyword_set(hinode) eq 0 and keyword_set(tip) eq 0 and $
      keyword_set(imax)) eq 0 then tip=1
  
  if n_elements(init) eq 0 then init=1
  error=0
  if keyword_set(hinode) then begin
    tip=0 & hinode=1 & imax=0
    if init then begin
      hhmmss=strmid(header.dateobs,11,100)
      date=strmid(header.dateobs,0,10)
      date=strmid(date(0),8,2)+'-'+ $
        (['jan','feb','mar','apr','may','jun','jul', $
          'aug','sep','oct','nov','dec'])(fix(strmid(date,5,2))-1)+'-'+ $
        strmid(date,2,2)
      atime=anytim2ints(hhmmss+' '+date)
      out=get_rb0p(atime,/deg)
      r0=total(out(0))
      b0=total(out(1))
      p0=total(out(2))
      sz=size(btot)
                                ;only approximate!!!
      message,/cont,'CAREFUL: no proper treatment of Hinode data set yet.'+$
        ' Missing slits are not treated!'
      xvec=header.xcen+0.16*(findgen(sz(1))-sz(1)/2.)
      yvec=header.ycen+0.16*(findgen(sz(2))-sz(2)/2.)
      xobs=(fltarr(sz(2))+1) ## xvec
      yobs=yvec ## (fltarr(sz(1))+1)
      cnvst={nx:sz(1),ny:sz(2),xobs:xobs,yobs:yobs,time:hhmmss,date:date,r0:r0}
    endif
    r0=cnvst.r0
  endif else if keyword_set(imax) then begin
    tip=0 & hinode=0 & imax=1
    message,/cont,'IMaX not yet implemented!!!
    stop
  endif else begin
    tip=1 & hinode=0 & imax=0
    if init then $
      cnvst=c2solar_tipst(obs,error=error,/quiet)
    r0=950.
    if n_tags(cnvst) ge 1 then $
      r0=cnvst.header.r0radius     $
    else begin
      error=1
      message,/cont,'Error in converting LOS 2 SOLAR.'
    endelse
  endelse
  if error ne 0 then begin
    bx_sol=btot*0
    by_sol=bx_sol
    bz_sol=bx_sol
    return
  endif
                                ;get surface normale vector for every
                                ;pixel from observer point of view 
                                ;therefore the B0 angle is set to 0,
                                ;since we do not want to find the real
                                ;sun center, but the center from the
                                ;observers viewpoint
;  xyobs=transpose([[cnvst.xobs(*)],[cnvst.yobs(*)]])
;  lonlat=xy2lonlat(xyobs,b0=0.,radius=cnvst.header.r0radius)
;  lon=reform(lonlat(0,*),cnvst.nx,cnvst.ny)
;  lat=reform(lonlat(1,*),cnvst.nx,cnvst.ny)
  
  if n_elements(xoff) eq 0 then xoff=0
  if n_elements(yoff) eq 0 then yoff=0
  
  szb=size(btot)
  if szb(0) eq 2 then begin
    nx=szb(1) & ny=szb(2)
    xobs=cnvst.xobs
    yobs=cnvst.yobs
    if n_elements(xval) eq 1 and n_elements(yval) eq 1 then begin
      xobs=fltarr(nx,ny)+xobs(xval,yval)
      yobs=fltarr(nx,ny)+yobs(xval,yval)
    endif
  endif else if szb(0) eq 1 then begin
    nx=szb(1) & ny=1
    xobs=cnvst.xobs(*,0)
    yobs=cnvst.yobs(*,0)
  endif else begin
    nx=1 & ny=1
    xobs=cnvst.xobs(0,0)
    yobs=cnvst.yobs(0,0)
  endelse
  if n_elements(window) ne 4 then win=[0,0,nx-1,ny-1] $
  else win=window
;  win([0,2])=(win([0,2])>0)<(nx-1)
;  win([1,3])=(win([1,3])>0)<(ny-1)
  win([0,2])=(win([0,2])-min(win([0,2])))<(nx-1)
  win([1,3])=(win([1,3])-min(win([1,3])))<(ny-1)
;  if nx ne cnvst.nx or ny ne cnvst.ny then $
;    message,/cont,'Image size not compatible!'
  
;  obs='data/13may01.014cc'
  if n_elements(ambigin) eq 0 then ambigin='0'
  lambig=strlowcase(ambigin)
  
  flip=byte(chiorig*0)
  
  nflip_new=n_elements(btot)
  nflip_min=nflip_new+1
  minflip=flip
  
  if tip eq 1 then begin
    if n_elements(pikaia_result) ne 0 then  $
      slitorie=pikaia_result.input.obs_par.slit_orientation
    ; chi=azi_qu2xy(chiorig,/reverse,header=cnvst.header,obs=obs, $
    ;               slit_orientation=slitorie)
    chi=azi_qu2xy(chiorig,header=cnvst.header)
  endif else begin
    chi=chiorig
  endelse
  
  
  first=1
  ok=0 & cnt=0
  
  lat=asin(yobs/r0)
  lon=asin(xobs/(cos(lat)*r0))
  
  nflip=nflip_new
  
  chi=((chi+180) mod 360) -180
  
;  gamma=fltarr(cnvst.nx,cnvst.ny)
;  chi=fltarr(cnvst.nx,cnvst.ny)+45.
;  btot=fltarr(cnvst.nx,cnvst.ny)+1.
  
                                ;b x,y,z in LOS system:
                                ;+z=along LOS
                                ;+y=along +Q (=terrestrial North)
                                ;chi is assumed to start with 0 at
                                ;x-axis in anticlockwise direction
  bz_los=btot*cos(gamma*!dtor) 
  bx_los=btot*sin(gamma*!dtor)*cos(chi*!dtor)
  by_los=btot*sin(gamma*!dtor)*sin(chi*!dtor)
;    if mminc then begin
  bx180_los=btot*sin(gamma*!dtor)*cos((chi+180.)*!dtor)
  by180_los=btot*sin(gamma*!dtor)*sin((chi+180.)*!dtor)
;    endif
  
  
                                ;define coordinate system:
                                ;+z= sun center to observer
                                ;+x= solar west from observer
                                ;+y= solar north from observer
  
;  rotmat=dblarr(3,3,cnvst.nx,cnvst.ny)
  bx_sol=fltarr(nx,ny) & bx_sol180=fltarr(nx,ny) & bx_sol0=fltarr(nx,ny)
  by_sol=fltarr(nx,ny) & by_sol180=fltarr(nx,ny) & by_sol0=fltarr(nx,ny)
  bz_sol=fltarr(nx,ny) & bz_sol180=fltarr(nx,ny) & bz_sol0=fltarr(nx,ny)
  
  szl=size(lat)
  if xoff lt nx and yoff lt ny then begin
    for ix=xoff,nx-1 do for iy=yoff,ny-1 do  $
      if ix lt szl(1) and iy lt szl(2) then begin
                                ;rotation matrix:
                                ;LOS-vector has to be rotated
                                ;firs rotate -lon
                                ;around solar N direction,
                                ;then -lat around W direction,
                                ;this moves pixel to the center
      slat=sin(-lat(ix,iy)) & clat=cos(-lat(ix,iy))
      slon=sin(-lon(ix,iy)) & clon=cos(-lon(ix,iy))
      mlonrot=[[ clon, 0., slon], $
               [   0., 1.,   0.], $
               [-slon, 0., clon]]
      mlatrot=[[ 1.,   0.,   0.], $
               [ 0., clat, slat], $
               [ 0.,-slat, clat]]
;    rotmat(*,*,ix,iy)=mlatrot ## mlonrot
      bvec_sol=(mlatrot ## mlonrot) ## $
        [bx_los(ix,iy),by_los(ix,iy),bz_los(ix,iy)]
      bx_sol0(ix,iy)=bvec_sol(0)
      by_sol0(ix,iy)=bvec_sol(1)
      bz_sol0(ix,iy)=bvec_sol(2)
      
      bvec180_sol=(mlatrot ## mlonrot) ## $
        [bx180_los(ix,iy),by180_los(ix,iy),bz_los(ix,iy)]
      bx_sol180(ix,iy)=bvec180_sol(0)
      by_sol180(ix,iy)=bvec180_sol(1)
      bz_sol180(ix,iy)=bvec180_sol(2)
      
      if lambig eq 'mininc' or lambig eq 'maxinc' or lambig eq '90' then begin
        inc=acos(bvec_sol(2)/btot(ix,iy))
        inc180=acos(bvec180_sol(2)/btot(ix,iy))
        if lambig eq 'mininc' then begin
          if inc gt inc180 then bvec_sol=bvec180_sol
          if n_elements(showthis) eq 0 then $
            print,'Using ambiguity solution INCMin'
        endif else if lambig eq 'maxinc' then begin
          if inc lt inc180 then bvec_sol=bvec180_sol
          if n_elements(showthis) eq 0 then $
            print,'Using ambiguity solution INCMax'
        endif else if lambig eq '90' then begin
          diff0=abs(inc-!pi/2.)
          diff180=abs(inc180-!pi/2.)
          if diff180 le diff0 then bvec_sol=bvec180_sol
          if n_elements(showthis) eq 0 then $
            print,'Using ambiguity solution INC close to 90�'  
        endif
      endif else begin
        if lambig eq '180' then begin
          bvec_sol=bvec180_sol
          if n_elements(showthis) eq 0 then $
            print,'Using ambiguity solution 180�'
        endif else if lambig eq 'smooth' then begin
          if keyword_set(findsmooth) eq 0 then begin
            mapfound=0
            if n_elements(ambig) ne 0 then begin
              if ambig.nx eq nx and ambig.ny eq ny then begin
                if n_elements(showthis) eq 0 then $
                  print,'Using smooth ambiguity solution from FlipMap'
                mapfound=1
                if ambig.flipmap(ix,iy) eq 1 then  bvec_sol=bvec180_sol
              endif
            endif
            if mapfound eq 0 then begin
              if n_elements(showthis) eq 0 then begin
                message,/cont,'No FlipMap found. Using 0� solution.'
              endif
            endif
          endif
        endif else if n_elements(showthis) eq 0 then $
          print,'Using ambiguity solution 0�'
      endelse
      showthis=1
          
      

      bx_sol(ix,iy)=bvec_sol(0)
      by_sol(ix,iy)=bvec_sol(1)
      bz_sol(ix,iy)=bvec_sol(2)
;    endfor
      
    endif
  endif
  
  
                                ;determine smooth chi/azi from
                                ;starting pixel
  if keyword_set(findsmooth) then begin
    if n_elements(ambig) eq 0 then begin
      message,/cont, $
        'No starting point(s) for smooth ambiguity removal defined.'
      reset
    endif
    if ambig.n eq 0 then begin
      message,/cont, $
        'No starting point(s) for smooth ambiguity removal defined.'
      reset
    endif
    xs=ambig.xs(0:ambig.n-1)
    ys=ambig.ys(0:ambig.n-1)
    
                                ;define pixels which are allowed to be flipped
    flipallow=bytarr(nx,ny)
    flipallow(win(0):win(2),win(1):win(3))=1b
    checked=bytarr(nx,ny)+1b
    checked(win(0):win(2),win(1):win(3))=0b
    nstart=total(checked)
    
                                ;start with a 3x3 area where flipping
                                ;is not allowed
    flip=bytarr(nx,ny)
    szboxx=fix(ambwg.box(0).val) ;use box of this size for smoothness comparison
    szboxy=fix(ambwg.box(1).val) ;use box of this size for smoothness comparison
    neladd=fix(ambwg.nperm.val) ;always add neladd elements for the smoothness comparison
    iradx=szboxx/2+1
    irady=szboxy/2+1
    
    inc0=acos(bz_sol/btot)
    inc180=acos(bz_sol180(2)/btot)
    for is=0,n_elements(xs)-1 do begin
      xx=(win(0)>(xs(is)+[-1,1]*iradx)<win(2))<(nx-1)
      yy=(win(1)>(ys(is)+[-1,1]*irady)<win(3))<(ny-1)
      flipallow(xx(0):xx(1),yy(0):yy(1))=0
      tflip=ambig.zero(is) eq 1 ;0 or 180�
      if ambig.zero(is) eq 2 then $
        if (abs(inc0(xs(is),ys(is))-!pi/2.) gt $
            abs(inc180(xs(is),ys(is))-!pi/2.)) then tflip=1b
      flip(xx(0):xx(1),yy(0):yy(1))=tflip
    endfor
    
    window,8,xs=nx,ys=ny*2 & userlct & erase
    print,'smooth azi-ambiguity ',format='(a20,$)'
    fracold=0.
    repeat begin
      irunx=lindgen(2*iradx+1)-iradx
      iruny=lindgen(2*irady+1)-irady
      for is=0,n_elements(xs)-1 do begin
        xrun=irunx+xs(is)
        wx=where(xrun ge win(0) and xrun le win(2))
        yrun=iruny+ys(is)
        wy=where(yrun ge win(1) and yrun le win(3))
        if wx(0) ne -1 then xrun=xrun(wx)
        if wy(0) ne -1 then yrun=yrun(wy)
        nnx=n_elements(wx)-1
        nny=n_elements(wy)-1
;        if nnx ge 1 and nny ge 1 then begin
          yfix=xrun*0+ys(is)
          xx=[yrun*0+xrun(0),xrun(1:*),yrun(1:*)*0+xrun(nnx),reverse(xrun(1:*))]
          yy=[yrun,xrun(1:*)*0+yrun(nny),reverse(yrun(1:*)),xrun(1:*)*0+yrun(0)]
          nxy=n_elements(xx)
          for ii=0,nxy-1,neladd do begin
            xxi=xx(ii:(ii+neladd-1)<(nxy-1))
            yyi=yy(ii:(ii+neladd-1)<(nxy-1))
            if (max(xxi) ge 0 and min(xxi) lt nx and $
                max(yyi) ge 0 and min(yyi) lt ny) then begin
              xbxmin=min([min(xxi),max(xxi)-(szboxx-1)])>min(xx)
              xbxmax=max([max(xxi),min(xxi)+(szboxx-1)])<max(xx)
              ybxmin=min([min(yyi),max(yyi)-(szboxy-1)])>min(yy)
              ybxmax=max([max(yyi),min(yyi)+(szboxy-1)])<max(yy)
              inx=(intarr(ybxmax-ybxmin+1)+1) ## (indgen(xbxmax-xbxmin+1)+xbxmin)
              iny=(indgen(ybxmax-ybxmin+1)+ybxmin) ## (intarr(xbxmax-xbxmin+1)+1)
              nin=n_elements(inx)
              flp=bytarr(xbxmax-xbxmin+1,ybxmax-ybxmin+1)
              for jj=0,(neladd-1)<(nxy-ii-1) do $
                flp=flp or (inx eq xxi(jj) and iny eq yyi(jj))
              flp=flp and flipallow(inx,iny) eq 1
              iflp=where(flp)             
              if iflp(0) ne -1 then begin
                tflip=flip(inx,iny)
                nflp=n_elements(iflp)
                devbest=0.
;print,iradx,irady,total(checked), long(nx)*long(ny), 2l^(nflp)
                if 2l^(nflp-1) gt 2l^(neladd) then stop
                for iper=0l,2l^(nflp) do begin
                  dec2bin,iper,b,/quiet
                  doflp=where(reverse(b) eq 1)
                  flp=tflip
                  if doflp(0) ne -1 then $
                    flp(iflp(doflp))=tflip(iflp(doflp)) eq 0
                  cbx=bx_sol(inx,iny)*(flp eq 0)+bx_sol180(inx,iny)*(flp eq 1)
                  cby=by_sol(inx,iny)*(flp eq 0)+by_sol180(inx,iny)*(flp eq 1)
                  cbz=bz_sol(inx,iny)*(flp eq 0)+bz_sol180(inx,iny)*(flp eq 1)
                  cbxavg=total(cbx)/nin
                  cbyavg=total(cby)/nin
                  cbzavg=total(cbz)/nin                  
;                 cbxavg=median(cbx)
;                 cbyavg=median(cby)
;                 cbzavg=median(cbz)
                  dev=total((cbx-cbxavg)^2+(cby-cbyavg)^2+(cbz-cbzavg)^2)
                  if iper eq 0 or dev lt devbest then begin
                    bestflip=flp
;print,iper,total(bestflip) ,devbest,dev ,total(b)                
                    devbest=dev
                  endif
                endfor
                flip(inx,iny)=bestflip

                flipallow(inx(iflp),iny(iflp))=0b
;plots,xxi,yyi,/device,color=(ii mod 255)+1
              endif
              checked(inx,iny)=1b
            endif
          endfor
;        endif 
      endfor                    ;loop over different starting points
      iradx=iradx+1
      irady=irady+1
      frac=(total(checked)-nstart)/(float(long(nx)*long(ny))-nstart)
      if fix(frac*60) ne fix(fracold*60) then begin
        tv,flip & tv,checked,0,ny ;& tv,flipallow0,ny*2
        plots,/device,[0,nx],[ny,ny],color=2
        xyouts,/device,0,ny,'!C!C  Flipped Pixels',color=255
        xyouts,/device,0,2*ny,'!C!C  Checked Area ('+ $
          n2s(fix(frac*100))+'% done)',color=255
        print,'.',format='(a,$)'
      endif
      fracold=frac
    endrep until total(checked) eq long(nx)*long(ny)
    print
    bx_sol=bx_sol*(flip eq 0)+bx_sol180*(flip eq 1)
    by_sol=by_sol*(flip eq 0)+by_sol180*(flip eq 1)
    bz_sol=bz_sol*(flip eq 0)+bz_sol180*(flip eq 1)
    
                                ;store flipmap
    ambig.nx=(size(flip))(1)
    ambig.ny=(size(flip))(2)
    ambig.flipmap(0:ambig.nx-1,0:ambig.ny-1)=flip
    
    message,/cont,'AMBI-Smooth still experimental!'
  endif
  
  inc_sol=acos(bz_sol/btot)/!dtor
  azi_sol=((atan(by_sol,bx_sol)/!dtor+540.) mod 360)-180.
end
