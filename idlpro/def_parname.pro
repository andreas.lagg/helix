function def_parname,tagnam
  
  par_name=tagnam
  for it=0,n_elements(tagnam)-1 do case tagnam(it) of
    'B': par_name(it)='BFIEL'
    'AZI': par_name(it)='AZIMU'
    'INC': par_name(it)='GAMMA'
    'VLOS': par_name(it)='VELOS'
    'DAMP': par_name(it)='VDAMP'
    'DOPP': par_name(it)='VDOPP'
    'HEIGHT': par_name(it)='SLHGT'
    'ETAZERO': par_name(it)='EZERO'
    'A0': par_name(it)='AMPLI'
    'FF': par_name(it)='ALPHA'
    else:
  endcase
  name={ipt:par_name,idl:tagnam}
  return,name
end
