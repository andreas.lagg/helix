function adapt_struct,old
  
  sz=size(old.fit)
  if sz(0) eq 1 then nxy=[0,sz(1)] else nxy=[sz(1),sz(2)]
  
  tn=tag_names(old)  
  rdhead= max(tn eq 'HEADER') eq 0 
  if rdhead eq 0 then rdhead=size(old.header,/type) ne 8
  if rdhead eq 0 then rdhead=max(tag_names(old.header) ne 'ERROR')
  if rdhead then begin
    header=read_tip_header(old.input.dir.profile+'/'+old.input.observation)
  endif else header=old.header
  
  tni=tag_names(old.input)
  genok=max(tni eq 'GEN')
  blendok=max(tni eq 'BLEND')
  oldnormok=max(tni eq 'OLD_NORM')
  oldvoigtok=max(tni eq 'OLD_VOIGT')
  tna=tag_names(old.input.atm)
  tnl=tag_names(old.input.line)
  atmusestraypol_ok=max(tna eq 'STRAYPOL_USE')
  linusestraypol_ok=max(tnl eq 'STRAYPOL_USE')
  
  new=result_struc(nx=nxy(0),ny=nxy(1),ipt=old.input,header=header)
  
  struct_assign,old,new,/nozero
  for il=0l,old.input.nline-1 do begin
    newline=new.line(0)
    struct_assign,old.line(il),newline
    new.line(il)=newline
  endfor
  for i=0l,n_elements(new.fit)-1 do begin
    newfit=new.fit(0)
    struct_assign,old.fit(i),newfit,/nozero
    new.fit(i)=newfit
    if genok eq 0 then new.fit(i).gen=def_genst()
    if blendok eq 0 then new.fit(i).blend=def_blendst()
  endfor
  for i=0l,n_elements(new.input)-1 do begin
    newinput=new.input(0)
    struct_assign,old.input(i),newinput,/nozero
    new.input(i)=newinput
    if genok eq 0 then new.input(i).gen=def_genst()
    if blendok eq 0 then new.input(i).blend=def_blendst()
  endfor
                                ;use old normalization if not present
                                ;in old input structure
  if oldnormok eq 0 then new.input.old_norm=1
  if oldvoigtok eq 0 then new.input.old_voigt=1
  if atmusestraypol_ok eq 0 then begin
    new.input.atm.straypol_use=1
    new.fit.atm.straypol_use=1
  endif
  if linusestraypol_ok eq 0 then begin
    new.input.line.straypol_use=1
    new.line.straypol_use=1
  endif
  
  newheader=new.header
  struct_assign,header,newheader,/nozero
  new.header=newheader
  
  if new.input.nblend eq 0 then new.input.nblend=1
  
  tnlp=tag_names(old.input.line.par)
  if max(tnlp eq 'STRENGTH') eq 0 then begin
    new.input.line.par.strength=1.
    new.fit.linepar.strength=1.
  endif
 dummy=temporary(old)
 
  return,new
  
end
