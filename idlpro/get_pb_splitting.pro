function get_pb_splitting,line,B,pb_method
  
  
  splitting=fltarr(line.pb.nr_zl)
  case pb_method of
    1: begin                    ;tabulated
      for i=0,line.pb.nr_zl-1 do begin
        splitting(i)=interpol(line.pb.splitting(i,0:line.pb.nr_b-1), $
                              line.pb.b(0:line.pb.nr_b-1),b,/quadratic)
      endfor
    end
    0: begin                    ;polynomial
      x2=(b*1e-3)^2
      x3=(b*1e-3)^3
      x4=(b*1e-3)^4
      x5=(b*1e-3)^5
      bwl2=b*line.wl^2*4.6686411e-13
      for i=0,line.quan.n_sig-1 do begin
        splitting(i)= $
          bwl2*line.quan.NUR(i) + $
          1e-3*(line.pb.cr(i,0)*x2 + line.pb.cr(i,1)*x3 + $
                line.pb.cr(i,2)*x4 + line.pb.cr(i,3)*x5)
        splitting(i+line.quan.n_sig+line.quan.n_pi)= $
          bwl2*line.quan.NUB(i) + $
          1e-3*(line.pb.cb(i,0)*x2 + line.pb.cb(i,1)*x3 + $
                line.pb.cb(i,2)*x4 + line.pb.cb(i,3)*x5)
      endfor
      for i=0,line.quan.n_pi-1 do begin
        splitting(i+line.quan.n_sig)= $
          bwl2*line.quan.NUP(i) + $
          1e-3*(line.pb.cp(i,0)*x2 + line.pb.cp(i,1)*x3 + $
                line.pb.cp(i,2)*x4 + line.pb.cp(i,3)*x5)
      endfor
    end
    -1: begin                   ;no pb-correction, zeeman only (test purposes)
      bwl2=b*line.wl^2*4.6686411e-13
      for i=0,line.quan.n_sig-1 do begin
        splitting(i)=bwl2*line.quan.NUR(i)
        splitting(i+line.quan.n_sig+line.quan.n_pi)=bwl2*line.quan.NUB(i)
      endfor
      for i=0,line.quan.n_pi-1 do begin
        splitting(i+line.quan.n_sig)=bwl2*line.quan.NUP(i)
      endfor
    end
  endcase

  return,splitting
end
