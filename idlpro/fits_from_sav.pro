;create fits file from a a pikaia-result sav file. This can be used as
;a new observation.
pro fits_from_sav,sav
  @common_maxpar
  maxpar
  
  idlso_dir='./idl.so/'
  
                                ;restore the map
  restore,/v,sav
  
                                ;define the WL-vector uset to write
                                ;out the ccx file
  restore,/v,'/data/slam/home/lagg/data/VIM/johann/double_etalon/m6173_hw0n0e0.sav'
  wlvec=wl_scan+6173.3412d
  nwl=n_elements(wlvec)
  
  ipt=pikaia_result.input
  doconv=0 & convval=-1
  if ipt.conv_func ne '' then begin
    conv=read_conv(ipt.wl_range,ipt.conv_func,ipt.conv_mode, $
                   ipt.conv_wljitter,ipt.prefilter, $
                   ipt.conv_nwl,wl,ipt.verbose)
    if conv.nwl gt 0 then begin
      nwl=conv.nwl
      compwl=conv.wl(0:nwl-1)
      convval=conv.val(0:cnwl-1)
      doconv=1
    endif
  endif
  
  doprefilter=0 & prefilterval=-1
  if ipt.prefilter ne '' then begin
                                ;prefilter_wlerr set to 0.
    prefilter=read_prefilter(ipt.prefilter,0.0,wl,ipt.verbose)
  endif
  
  natm=ipt.ncomp
  nblend=ipt.nblend
  nline=ipt.nline
  sinit=1
  old_norm=ipt.old_norm         ;flag for old normalization
  old_voigt=ipt.old_voigt         ;flag for old normalization
  voigt=fix(ipt.profile eq 'voigt')
  modeval=fix(ipt.modeval)
  magopt=fix(ipt.magopt(0))
  hanle_azi=ipt.hanle_azi
  norm_stokes_val=ipt.norm_Stokes_val
  old_norm=fix(ipt.old_norm)
  old_voigt=fix(ipt.old_voigt)
  use_geff=fix(ipt.use_geff(0))
  use_pb=fix(ipt.use_pb(0))
  pb_method=fix(ipt.pb_method(0))
  obs_par=(ipt.obs_par)(0)
  cmode=ipt.conv_mode
  iprof_only=0
  normff=1
  sz=size(pikaia_result.fit)
  nx=sz(1) & ny=sz(2)
  iprof=float(wlvec*0) & qprof=iprof & uprof=iprof & vprof=iprof
  prof=replicate({ic:1.,wl:wlvec,i:iprof,q:qprof,u:uprof,v:vprof},nx,ny)
  print,'.',format='(a,$)'
  for ix=0,nx-1 do begin
    for iy=0,ny-1 do begin
      
      atm=pikaia_result.fit(ix,iy).atm(0:natm-1)
      blend=pikaia_result.fit(ix,iy).blend(0:nblend-1)
      lines=get_full_line(pikaia_result,x=ix,y=iy)
      gen=pikaia_result.fit(ix,iy).gen
      use=intarr(nline,natm)
      for il=0,ipt.nline-1 do for ia=0,natm-1 do $
        use(il,ia)=max(id2s(atm(ia).use_line) eq id2s(lines(il).id))
      fill_localstray,lsi,lsq,lsu,lsv,dols,wlvec,nwl
      fcall=call_external(idlso_dir+'compute_profile.so', $
                          'compute_profile_pro_',/unload, $
                          atm,natm, $
                          lines,nline, $
                          wlvec,nwl, $
                          blend,nblend, $
                          gen, $
                          lsi,lsq,lsu,lsv,dols, $
                          convval,doconv,cmode, $
                          prefilter.val,prefilter.doprefilter,$
                          intarr(maxwl),-1, $
                          obs_par, $
                          sinit,voigt,magopt,use_geff,use_pb,pb_method, $
                          modeval,iprof_only,use, $
                          iprof,qprof,uprof,vprof,normff,old_norm,-1, $
                          hanle_azi,norm_stokes_val,old_voigt)
      prof(ix,iy).i=iprof
      prof(ix,iy).q=qprof
      prof(ix,iy).u=uprof
      prof(ix,iy).v=vprof
      if fix(float(ix*nx+iy)/nx/ny*79) ne fix(float(ix*nx+iy+1)/nx/ny*79) then $
        print,'.',format='(a,$)'
    endfor
  endfor
  print
  data=make_array(nwl,ny,nx*4,type=4)    
  for ix=0,nx-1 do begin
    data(*,*,ix*4+0)=reform(prof(ix,*).i)
    data(*,*,ix*4+1)=reform(prof(ix,*).q)
    data(*,*,ix*4+2)=reform(prof(ix,*).u)
    data(*,*,ix*4+3)=reform(prof(ix,*).v)
  endfor

  froot=strmid(sav,strpos(sav,'/',/reverse_search)+1,strlen(sav))
  spos=strpos(froot,'.sav')
  if spos gt 0 then froot=strmid(froot,0,spos)

  fitsout='./profile_archive/'+froot+'.fitscc'
  print,'======= WRITE NEW FITS ================='
  print,'File: ',fitsout
  
  case size(data,/type) of
    4: bp='-32'
  endcase
  header='BITPIX = '+bp
  header=''
  writefits,fitsout,data,header,append=0

;get continuum image
  icont=reform(prof.ic)
  wloff=0. & wlbin=0.

;now create ccx file  
  print,'======= WRITE NEW CCX ================='
  filex=fitsout+'x'
                                ;create header
  header=''

  writefits,filex,icont,header,append=0
  naxpos=max(where(strpos(header,'NAXIS') eq 0))
  header=[header(0:naxpos), $
          'WL_NUM  = '+string(nwl,format='(i20)')+ $
          ' / WL-bins', $
          'WL_OFF  = '+string(wloff,format='(d20.8)')+' / WL-Offset', $
          'WL_DISP = '+string(wlbin,format='(d20.8)')+' / WL-Dispersion', $
          header(naxpos+1:*)]
  writefits,filex,icont,header,append=0        

                                ;write out wl-vector as extension
  writefits,filex,wlvec,append=1
  print,'Wrote '+filex
  print,'Done.'
 
  iii=readfits(fitsout)
  jjj=readfits_ssw(fitsout)
  kkk=rfits_im_al(fitsout,1)
  stop
end
