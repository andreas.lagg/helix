pro read_par,word,tag=it,line=nl,count=count, $
             atmtyp=atmtyp,blendtyp=blendtyp,generaltyp=generaltyp
  common input,ipt,cnt          ;,comp_cnt
  
  it=it(0)
  if it eq -1 then begin
    print,'invalid line: '+n2s(nl)
    return
  endif
  
  atmtyp=1
  if keyword_set(blendtyp) then begin
    blendtyp=1
    atmtyp=0
    generaltyp=0
  endif else blendtyp=0
  if keyword_set(generaltyp) then begin
    blendtyp=0
    atmtyp=0
    generaltyp=1
  endif else generaltyp=0
  
  case 1 of
    atmtyp: begin
      typ='ATM'
      var=ipt.atm
      scl=ipt.scale
    end
    blendtyp: begin
      typ='BLEND'
      var=ipt.blend
      scl=ipt.blend.scale
    end
    generaltyp: begin
      typ='GEN'
      var=ipt.gen
      scl=ipt.gen.scale
    end
  endcase
  
  if it ge n_tags(var.par) then begin
    message,/cont,'Problem with parameter, check line '+n2s(nl)
    reset
  endif
  
  nw=n_elements(word)
  if nw eq 1 then begin
    word=[word(0),word(0),word(0),'0']
  endif else if nw lt 4 or (nw mod 2) ne 0 then begin
    print,'Wrong '+typ+' Parameter in line '+n2s(nl)+': '+ $
      (tag_names(var.par))(it)
    print,'Check number of entries for this line'
    print,'Note: You must not use a parameter name as a first word' + $
      ' in the comment block.'
    print,'Format: '
    print,'NAME    ini_val   min_value   max_val  fit/coupling'
    reset
  endif
  
  
  var(count.(it)).par.(it)=float(word(0))
  var(count.(it)).fit.(it)=float(word(3))
  scl(count.(it)).(it).min=float(word(1))
  scl(count.(it)).(it).max=float(word(2))
  
  case 1 of
    atmtyp: begin
      var(count.(it)).fitflag.(it)=fix(word(3)) ne 0
                                ;read multi-iteration value (only
                                ;atmospheric parameters)
      for im=0,fix((nw-4)/2)-1 do begin
        ipt.mi(count.(it)).(it).perc_rg(im)=float(word(4+im*2))
        ipt.mi(count.(it)).(it).fit(im)    =  fix(word(4+im*2+1))
        var(count.(it)).fitflag.(it)=(var(count.(it)).fitflag.(it) or $
                                      fix(word(4+im*2+1)) ne 0)
      endfor
      ipt.atm=var
      ipt.scale=scl
    end
    blendtyp: begin
      ipt.blend=var
      ipt.blend.scale=scl
    end
    generaltyp: begin
      ipt.gen=var
      ipt.gen.scale=scl
    end
  endcase
  

  
  count.(it)=count.(it)+1
end

function read_ipt,ipt_file,empty=empty,ipath=ipath,struct=struct,ascii=ascii, $
                  parin=parin
  common input,ipt,cnt          ;,comp_cnt
  common ipt_path,ipt_path
  common name,name
  @common_maxpar
  common iptgroup,iptgroup
  common verbose,verbose
  common scale,scale
  @common_localstray
  
  if (n_params() eq 0 and n_elements(ascii) eq 0 and $
      keyword_set(struct) eq 0 and keyword_set(empty) eq 0) then $
    message,'read_ipt: if no input file is specified, the ascii keyword' + $
    ' must be a stringarray containing the lines of the input file.'
  
  
  dols=0               ;default: do not do local straylight correction
  lspol=1
  
  if n_elements(ipt_file) eq 0 then ipt_file='./input/default.ipt'
  if n_elements(ipt_path) eq 0 then ipt_path=['./input/','']
  
;define max. values
  if n_elements(maxatm) eq 0 then maxpar

  
                                ;read input file into string array
  if n_elements(ascii) ge 1 then begin
    lall=ascii
    line=strtrim(lall,2)
    inc=where(strmid(line,0,1) ne ';')
    if inc(0) ne -1 then line=line(inc)
    lnr=inc+1
    date=systime(0)
  endif else begin
    lall=strarr(maxipt)
    if keyword_set(empty) eq 0 and n_elements(struct) eq 0 then $
      read_file,ipt_file,ipt_path,line,lnr,lall=lall,ipath=ipath,date=date $
    else line=lall
  endelse
;   if n_elements(struct) ne 0 then begin
;     date=struct.date.file
;     line=struct.ascii
;     no=where(strcompress(line,/remove_all) ne '')
;     if no(0) ne -1 then line=line(no) else message,'No valid lines'
  
;     lnr=n_elements(line)
;     lnr=indgen(lnr)
;     lall=line
;   endif

  
                                ;check for parameters important for
                                ;structure definition (eg voigt
                                ;profile)
  voigt=0
  for i=0,n_elements(line)-1 do begin
    word=strsplit(/extract,line(i))
    if strupcase(word(0)) eq 'PROFILE' then begin
      if strupcase(word(1)) eq 'VOIGT' then voigt=1
    endif
  endfor
  
  
  
                                ;define structure containing all
                                ;information on controlling the
                                ;helix procedure
  fp=def_parst(scale=scale,multi_iter=mi,obs_par=obs_par)
  bl=def_blendst()
  gen=def_genst()
  gen.par.ccorr=1.
  gen.par.straylight=0.
  gen.par.radcorrsolar=1.
  imi=intarr(maxmi)
  fmi=fltarr(maxmi)
  smi=strarr(maxmi)
  wgtcnt=intarr(maxmi)
  
  
;  he=def_line(line='he',/empty)
;  linest=he(0)
  linest=def_linest()
  
  

  lpar={id:0., par:linest.par(0), scale:linest.scale(0), fit:linest.fit(0)}
  lptmp=replicate(lpar,maxlin)
  lp_idx=-1

  
  ipt_empty={file:ipt_file, $
             ascii:strtrim(lall,0), $              ;all lines as ascii-file
             comment:strarr(maxcmt), $  ;comment lines
             n_cmt:0, $                 ;number of comment lines
             date:{run:systime(0),file:''}, $ ;date for input file and run time
             dir:{ps:'./ps/',profile:'./profile_archive/',sav:'./sav/', $
                  atm:'./atm_archive/',atm_suffix:'',wgt:'./wgt/', $
                  atom:'./atom/'}, $ ;default
                                ;directory settings
  display_map:0, $              ;flag for display at end of run
    display_profile:1, $        ;flag for display of profiles
    display_comp:0, $ ;flag for display of individual components in profile
    old_norm: 0, $    ;use old normalization method (SGRAD change!)
    old_voigt: 0, $    ;if 1 use old normalization of Faraday/Voigt function
    save_fitprof:0, $ ;if set, the fitted profile is saved (IDL-sav)
    fitsout: 0, $     ;map results are written to FITS file
  ps:0  , $                          ;set output: X (=def.) or PS
    ncomp:0, $        ;number of components
    nblend:0, $       ;number of blends
    atm:replicate(fp,maxatm), $      ;parameter values
    scale:replicate(scale,maxatm), $ ;scaling for fit parameters
    mi:replicate(mi,maxatm), $       ;multi iteration variable
    blend:replicate(bl,maxblend), $  ;ar + fit of telluric blends
    gen:gen, $                       ;general fit parameters
    obs_par:obs_par, $ observation parameters
    nline:0, $                       ;number of lines
    line:replicate(linest,maxlin), $ ;structure containing line parameters
;   ccorr:1.,$           ;factor for I (constant continuum correction)
  straypol_corr: 0, $    ;correction for straylight-polarization  
    straypol_amp: 0., $  ;amplitude for straypol-correction (only used
                                ;for synthesis)
  hanle_azi: 0., $              ;offset for Hanle (experimental)
    wl_range:dblarr(2), $       ;WL-range to be used
    wl_num:200, $               ;number of WL-points for synthesis
    wl_disp:0d, $               ;wavelength dispersion
    wl_off:0d, $                ;offset for WL-calibration
    wl_bin:1, $                 ;WL-binning
    approx_dir:imi, $      ;if set to 1 then use Unno-solution for
                                ;determination of inclination. In this case
                                ;the inlcination is not fitted by pikaia.
                                ;Additionally, the azimuthal angle is only
                                ;calculated using Q and U observed profile.
  approx_azi:imi, $             ;only fit azimuth using unno solution,
                                ;inclination is a free parameter
  approx_dev:imi, $ ;if set then the Unno solution is used as an estimate for
                                ;the pikaia fit: the pikaia values for
                                ;inclination and azimuth are only allowed to
                                ;vary within a narrow range of the Unno
                                ;solution
  approx_dev_inc:fmi, $    ;allowed deviation of inclination from Unno
                                ;solution (in degrees)
  approx_dev_azi:fmi, $        ;allowed deviation of azimuth from Unno
                                ;solution (in degrees)
  iquv_weight:fltarr(4,maxmi)+1, $ ;4-element vector defining relative weighting
                                ;of IQUV. Additionally the code does an
                                ;automatic weighting according to the
                                ;strength of the I signal compared to
                                ;the QUV signals. Not used for Unno solution.
  wgt_file:smi, $               ;file containing weighting function
    atom_file:strarr(maxatm,maxlines), $ ;contains filenames of atomic data
    stray:0, $      ;flag: if set to 1 then the first component
                                ;is treated as straylight component, that
                                ;means, no mag-field is fitted
  observation:'', $ ;filename of IDL-save file containing the
                                ;profiles
  nosav:1, $        ;if set to 1, no sav-file of the result is created
    x:intarr(2), $  ;two-elements vector containing xmin,xmax of
                                ;the observation map to be analyzed
  y:intarr(2), $          ;two-elements vector containing ymin,ymax of
                                ;the observation map to be analyzed
  profile_list:'', $            ;filename containing list of profiles
    stepx:4, $                  ;step size for going from xmin to xmax
    stepy:4, $                  ;step size for going from ymin to ymax
    scansize:0, $               ;number of steps for one scan in a
                                ;repetitive observation  
  average:1, $            ;if set to one then average observation over
                                ;the xstep/ystep size
  nfitpar:0, $                  ;number of free parameters
  norm_stokes:'', $        ;normalization of stokes profiles: I or Ic
    norm_stokes_val: 0, $  ;integer for norm stokes: 0=Ic, 1=I  
    norm_cont: 0, $        ;method for continuum normalization  
    ic_level: 0., $ ;specify continuum level manually
  localstray_rad:0., $     ;local straylight correction
    localstray_fwhm:0., $  ;local straylight correction
    localstray_core:0., $  ;local straylight correction
    localstray_pol:1, $    ;local straylight correction pol/unpol flag
    profile:'', $          ;define profile shape of pi/sigma
                                ;components (voigt / gauss). default: Gauss
  magopt: imi+1,$            ;include mag-optical effects (voigt only)
    use_geff:imi+1, $ ;use effective Lande factor (=1) or real Zeeman pattern (=0)
    use_pb:imi, $  ;if set, the zeeman-splitting and strength includes
                                ;  the Paschen-Back effect (from the table by
                                ;                           Socas-Navarro)
  pb_method:imi, $              ; use polynomials (=poly=0) or table
                                ;interpolations (=table=1) to calculate
                                ;the PB-effect
  synth:0b, $                   ;synthetic profile mode
    conv_func: '', $ ;filename of convolution function to simulate measurement  
    conv_nwl:0, $    ;# of WL-points for convolution
    conv_mode:0, $ ;convolution method: 0=FFT, 1=MULTIPLICATION
    conv_wljitter:0.,$ ;uncertainty in knowledge of wavelength for filter position
    conv_output:0, $   ;flag to control the output # of WL points
    prefilter:'', $  ;name of prefilter curve
    prefilter_wlerr:0., $;error in knowledge of prefilter curve (e.g. caused by unknown temperature). Must be set to ZERO for real data!
  chi2mode:0, $        ;method to calc. chi2
  noise:0., $          ;add artificial random noise to synth. profile 
  Inoise:0., $          ;add artificial random noise to synth. Stokes I profile 
    min_quv:0., $    ;minimum signal in QUV
    smooth:0, $      ;smooth profiles
    median:0, $      ;median profiles
    spike:0, $      ;remove spikes
    filter_mode:0, $ ;filtering method (0=IDL-smooth,1=FFT Low-Pass)
    verbose:1, $     ;control verbosity (pikaia)
    code:'', $       ;use code fortran or idl
    method:imi, $      ;minimization method
    pikaia_stat:'', $       ;switch on and control pikaia statistic module
    piklm_bx: 1, $   ;size for combined pikaia / lmdiff minimization
    piklm_by: 1, $   ;box 
    piklm_minfit:0., $  ;rerun with PIKAIA if fitness drops 
                                ;to piklm_minfit percent of previos value
  nmi:0, $                      ;number of multi iterations
    mode:'', $          ;atmosphere parameters are consistent with:
                                ; gauss, voigt-par or voigt-phys
  modeval:0,$           ;mode as int-index: 0=gauss, 1=voigt-par
                                ;and 2=voigt-phys
  fitpar:strarr(maxfitpar), $ ;name of free parameters
  parset:strarr(20), $  ;list of allowed parameters in different modes
    hanle_slab:strarr(12), $    ;parset for hanle slab model
    voigt_phys:strarr(12), $    ;parset for voigt - physical pars  
    voigt_par:strarr(12), $     ;parset for normal voigt
    voigt_gdamp:strarr(12), $   ;parset for voigt - physical pars  
    gauss_par:strarr(12), $     ;parset for gauss
    voigt_szero:strarr(12), $   ;parset for voigt - physical pars  
    pixelrep:1l, $              ;repeat one pixel several times
    pikaia_stat_cnt:1l, $ ;number of elements in pikaia stat mode
    keepbest:1, $              ;store result only if fitness is better than
                                ; existing result (FITSOUT only)
    ncalls:imi, $       ;number of pikaia iterations
    pikaia_pop:0 $          ;number of PIKAIA populations
    }
  

  def_iptgroup
  
  ipt_empty.wgt_file=''
  
  ipt_empty.hanle_slab(0:8)=['B','AZI','INC','VLOS','DOPP','DAMP', $
                             'DSLAB','HEIGHT','FF']
  ipt_empty.voigt_phys(0:9)=['B','AZI','INC','VLOS','GDAMP','VMICI','DENSP', $
                             'TEMPE','SGRAD','FF']
  ipt_empty.voigt_par(0:8)= $
    ['B','AZI','INC','VLOS','DAMP','DOPP','SGRAD','ETAZERO','FF']
  ipt_empty.voigt_gdamp(0:8)= $
    ['B','AZI','INC','VLOS','GDAMP','DOPP','SGRAD','ETAZERO','FF']
  ipt_empty.voigt_szero(0:9)= $
    ['B','AZI','INC','VLOS','GDAMP','DOPP','SZERO','SGRAD','ETAZERO','FF']
  ipt_empty.gauss_par(0:7)=['B','AZI','INC','VLOS','WIDTH','A0','SGRAD','FF']
  
  if keyword_set(empty) then return,ipt_empty

  ipt=ipt_empty

  
  
  ipt.ncalls(0)=10
  
                                ;counter for parameters of
                                ;atm. components
  cnt=fp.par
  for it=0,n_tags(cnt)-1 do cnt.(it)=0
  blcnt=bl.fit
  for it=0,n_tags(blcnt)-1 do blcnt.(it)=0
  gencnt=gen.fit
  for it=0,n_tags(gencnt)-1 do gencnt.(it)=0
  
;  comp_cnt=cnt
  tn=tag_names(ipt.atm.par)
  ul_idx=0                      ;counter for 'USE_LINE' keyword
  ua_idx=0                      ;counter for 'USE_ATOM' keyword
  linid=0
  
  natmfit=0
  
  name=def_parname(tag_names(ipt.atm.par))
  blname=def_blendname(tag_names(ipt.blend.par))
  gnname=def_genname(tag_names(ipt.gen.par))
  
  if n_elements(verbose) eq 0 then verbose=1
  pre=check_scaling(ipt=ipt,/predef)
  
  if n_elements(struct) eq 0 then begin ;read input from file or from
                                ;existing structure?
    
    for i=0,n_elements(line)-1 do if line(i) ne '' then begin
      lnocmt=(strsplit(/extract,line(i),';'))(0)
      word=strsplit(/extract,lnocmt)
;    icmt=where(strmid(word,0,1) eq ';') ;remove comment appendix of every line
;    if icmt(0) ne -1 then word=word(0:(icmt(0)-1)>0)
      nw=n_elements(word)
      nw1=nw-1
      keyword_noopt=['PROFILE_LIST','ATM_SUFFIX','OBSERVATION','CONV_FUNC', $
                     'PREFILTER','ATM_INPUT']
      if nw1 eq 0 and max(keyword_noopt eq word(0)) eq 0 then begin
        message,/cont,'Error in input file. Check line: '+n2s(i)
        print,line(i)
        reset
      endif
      case strupcase(word(0)) of
        'PS': ipt.dir.ps=word(1)
        'SAV': ipt.dir.sav=word(1)
        'PROFILE_ARCHIVE': ipt.dir.profile=word(1)
        'ATM_ARCHIVE': ipt.dir.atm=word(1)
        'ATM_SUFFIX': if nw1 ge 1 then ipt.dir.atm_suffix=word(1)
        'WGT': ipt.dir.wgt=word(1)
        'ATOM': ipt.dir.atom=word(1)
        'DISPLAY_MAP': ipt.display_map=fix(word(1))
        'DISPLAY_PROFILE': ipt.display_profile=fix(word(1))
        'DISPLAY_COMP': ipt.display_comp=fix(word(1))
        'OLD_NORM': ipt.old_norm=fix(word(1))
        'OLD_VOIGT': ipt.old_voigt=fix(word(1))
        'SAVE_FITPROF': ipt.save_fitprof=fix(word(1))
        'FITSOUT': ipt.fitsout=fix(word(1))
        'OUTPUT': ipt.ps=strpos(strupcase(word(1)),'PS') ne -1
        'OBSERVATION': if nw1 ge 1 then ipt.observation=word(1)
        'XPOS': ipt.x(*)=long(word(1:2))
        'YPOS': ipt.y(*)=long(word(1:2))
        'PROFILE_LIST': if nw1 ge 1 then ipt.profile_list=word(1)
        'STEPX': ipt.stepx=long(word(1))
        'STEPY': ipt.stepy=long(word(1))
        'SCANSIZE': ipt.scansize=long(word(1))
        'AVERAGE': ipt.average=fix(word(1))
        'SYNTH': ipt.synth=fix(word(1)) eq 1
        'NOISE': ipt.noise=float(word(1))
        'INOISE': ipt.Inoise=float(word(1))
        'MIN_QUV': ipt.min_quv=float(word(1))
        'NORM_STOKES': ipt.norm_stokes=strupcase(word(1))
        'NORM_CONT': begin
          case strupcase(word(1)) of
            'LOCAL': ipt.norm_cont=0
            'SLIT': ipt.norm_cont=1
            'IMAGE': ipt.norm_cont=2
            else: begin
              message,/cont,'Unknown method for continuum normalization: '+ $
                word(1)+'. Available methods: LOCAL, SLIT or IMAGE'
              reset
            end
          endcase
        end
        'IC_LEVEL': ipt.ic_level=float(word(1))
        'PREFILTER': if nw1 ge 1 then ipt.prefilter=word(1)
        'CONV_FUNC': if nw1 ge 1 then ipt.conv_func=word(1)
        'CONV_NWL': ipt.conv_nwl=(fix(word(1)))<maxwl
        'CONV_MODE': begin
          case strupcase(word(1)) of
            'FFT': ipt.conv_mode=0
            'MUL': ipt.conv_mode=1
            else: begin
              message,/cont,'Unknown method for CONV_MODE: '+ $
                strupcase(word(1))+'. Available methods: FFT, MUL'
              reset
            end
          endcase
          
        end
        'CONV_WLJITTER': ipt.conv_wljitter=float(word(1))
        'CONV_OUTPUT': ipt.conv_output=(fix(word(1)))
        'PREFILTER_WLERR': ipt.prefilter_wlerr=float(word(1))
        'MEDIAN': ipt.median=fix(word(1))
        'SPIKE': ipt.spike=fix(word(1))
        'SMOOTH': begin
          ipt.smooth=fix(word(1))
          if nw gt 2 then ipt.filter_mode=fix(word(2))
        end
        'NCOMP': begin
          ipt.ncomp=fix(word(1))
          if ipt.ncomp gt maxatm then begin
            message,/cont,'Maximum number of components is '+n2s(maxatm)
            reset
          endif
        end
        'NBLEND': begin
          ipt.nblend=fix(word(1))
          if ipt.nblend gt maxblend then begin
            message,/cont,'Maximum number of blends is '+n2s(maxatm)
            reset
          endif
        end
        'BFIEL': read_par,word(1:*),tag=where(name.ipt eq 'BFIEL'), $
          line=lnr(i),count=cnt
        'AZIMU': read_par,word(1:*),tag=where(name.ipt eq 'AZIMU'), $
          line=lnr(i),count=cnt
        'GAMMA': read_par,word(1:*),tag=where(name.ipt eq 'GAMMA'), $
          line=lnr(i),count=cnt
        'VELOS': read_par,word(1:*),tag=where(name.ipt eq 'VELOS'), $
          line=lnr(i),count=cnt
        'WIDTH': read_par,word(1:*),tag=where(name.ipt eq 'WIDTH'), $
          line=lnr(i),count=cnt
        'VDAMP': read_par,word(1:*),tag=where(name.ipt eq 'VDAMP'), $
          line=lnr(i),count=cnt
        'VDOPP': read_par,word(1:*),tag=where(name.ipt eq 'VDOPP'), $
          line=lnr(i),count=cnt
        'SGRAD': read_par,word(1:*),tag=where(name.ipt eq 'SGRAD'), $
          line=lnr(i),count=cnt
        'SZERO': read_par,word(1:*),tag=where(name.ipt eq 'SZERO'), $
          line=lnr(i),count=cnt
        'EZERO': read_par,word(1:*),tag=where(name.ipt eq 'EZERO'), $
          line=lnr(i),count=cnt
        'AMPLI': read_par,word(1:*),tag=where(name.ipt eq 'AMPLI'), $
          line=lnr(i),count=cnt
        'ALPHA': read_par,word(1:*),tag=where(name.ipt eq 'ALPHA'), $
          line=lnr(i),count=cnt
        'GDAMP': read_par,word(1:*),tag=where(name.ipt eq 'GDAMP'), $
          line=lnr(i),count=cnt
        'VMICI': read_par,word(1:*),tag=where(name.ipt eq 'VMICI'), $
          line=lnr(i),count=cnt
        'DENSP': read_par,word(1:*),tag=where(name.ipt eq 'DENSP'), $
          line=lnr(i),count=cnt
        'TEMPE': read_par,word(1:*),tag=where(name.ipt eq 'TEMPE'), $
          line=lnr(i),count=cnt
        'DSLAB': read_par,word(1:*),tag=where(name.ipt eq 'DSLAB'), $
          line=lnr(i),count=cnt
        'SLHGT': read_par,word(1:*),tag=where(name.ipt eq 'SLHGT'), $
          line=lnr(i),count=cnt
        'BLEND_WL': read_par,word(1:*), $
          tag=where(blname.ipt eq 'BLEND_WL'),line=lnr(i),count=blcnt,/blend
        'BLEND_A0': read_par,word(1:*), $
          tag=where(blname.ipt eq 'BLEND_A0'),line=lnr(i),count=blcnt,/blend
        'BLEND_WIDTH': begin
          read_par,word(1:*),tag=where(blname.ipt eq 'BLEND_WIDTH'), $
            line=lnr(i),count=blcnt,/blend
          ibb=blcnt.width-1
          if (ipt.blend(ibb).scale.width.min lt pre.dopp.min or $
              ipt.blend(ibb).scale.width.max gt pre.dopp.max) then begin
            ipt.blend(ibb).scale.width.min= $
              ipt.blend(ibb).scale.width.min>pre.dopp.min
            ipt.blend(ibb).scale.width.max= $
              ipt.blend(ibb).scale.width.max<pre.dopp.max
            if ipt.verbose ge 1 then print,'Changed scaling: BLEND_WIDTH', $
              ipt.blend(ibb).scale.width.min,' ', $
              ipt.blend(ibb).scale.width.max, ', Blend ',ibb
          endif
        end
        'BLEND_DAMP': begin
          read_par,word(1:*),tag=where(blname.ipt eq 'BLEND_DAMP'), $
            line=lnr(i),count=blcnt,/blend
          ibb=blcnt.damp-1
          if (ipt.blend(ibb).scale.damp.min lt pre.damp.min or $
              ipt.blend(ibb).scale.damp.max gt pre.damp.max) then begin
            ipt.blend(ibb).scale.damp.min= $
              ipt.blend(ibb).scale.damp.min>pre.damp.min
            ipt.blend(ibb).scale.damp.max= $
              ipt.blend(ibb).scale.damp.max<pre.damp.max
            if ipt.verbose ge 1 then print,'Changed scaling: BLEND_DAMP', $
              ipt.blend(ibb).scale.damp.min,' ', $
              ipt.blend(ibb).scale.damp.max, ', Blend ',ibb
          endif
        end
        'LINE_ID': begin
          lp_idx=lp_idx+1
          lptmp(lp_idx).id=double(word(1))
        end
        'LINE_STRENGTH': begin
          if lp_idx lt 0 then begin
            message,/cont,'Please define LINE_ID prior to LINE_STRENGTH'
            reset
          endif
          lptmp(lp_idx).par.strength=float(word(1))
          lptmp(lp_idx).scale.strength.min=float(word(2))
          lptmp(lp_idx).scale.strength.max=float(word(3))
          lptmp(lp_idx).fit.strength=fix(word(4))
        end
        'LINE_WLSHIFT': begin
          if lp_idx lt 0 then begin
            message,/cont,'Please define LINE_ID prior to LINE_WLSHIFT'
            reset
          endif
          lptmp(lp_idx).par.wlshift=float(word(1))
          lptmp(lp_idx).scale.wlshift.min=float(word(2))
          lptmp(lp_idx).scale.wlshift.max=float(word(3))
          lptmp(lp_idx).fit.wlshift=fix(word(4))
        end
        'CCORR': begin
          read_par,word(1:*),tag=where(gnname.ipt eq 'CCORR'), $
            line=lnr(i),count=gencnt,/general
          if ipt.gen.fit.ccorr eq 1 then $
            if (ipt.gen.par.ccorr lt ipt.gen.scale.ccorr.min or $
                ipt.gen.par.ccorr gt ipt.gen.scale.ccorr.max) $
            then begin
            ipt.gen.par.ccorr= $
              (ipt.gen.par.ccorr>ipt.gen.scale.ccorr.min) $
              <ipt.gen.scale.ccorr.max
            if verbose ge 1 then $
              print,'Changed initial value for: CCORR', $
              ipt.gen.par.ccorr
          endif
        end
        'STRAYLIGHT': begin
          read_par,word(1:*),tag=where(gnname.ipt eq 'STRAYLIGHT'), $
            line=lnr(i),count=gencnt,/general
          if ipt.gen.fit.straylight eq 1 then $
            if (ipt.gen.par.straylight lt ipt.gen.scale.straylight.min or $
                ipt.gen.par.straylight gt ipt.gen.scale.straylight.max) $
            then begin
            ipt.gen.par.straylight= $
              (ipt.gen.par.straylight>ipt.gen.scale.straylight.min) $
              <ipt.gen.scale.straylight.max
            if verbose ge 1 then $
              print,'Changed initial value for: STRAYLIGHT', $
              ipt.gen.par.straylight
          endif
        end
        'RADCORRSOLAR': begin
          read_par,word(1:*),tag=where(gnname.ipt eq 'RADCORRSOLAR'), $
            line=lnr(i),count=gencnt,/general
          if ipt.gen.fit.radcorrsolar eq 1 then $
            if (ipt.gen.par.radcorrsolar lt ipt.gen.scale.radcorrsolar.min or $
                ipt.gen.par.radcorrsolar gt ipt.gen.scale.radcorrsolar.max) $
            then begin
            ipt.gen.par.radcorrsolar= $
              (ipt.gen.par.radcorrsolar>ipt.gen.scale.radcorrsolar.min) $
              <ipt.gen.scale.radcorrsolar.max
            if verbose ge 1 then $
              print,'Changed initial value for: RADCORRSOLAR', $
              ipt.gen.par.radcorrsolar
          endif
        end
        'ATM_INPUT': if nw1 ge 1 then begin
          bw=byte(word(1)) & lbw=n_elements(bw)
          ipt.atm(ua_idx).atm_input[0:lbw-1]=bw
        end
        'USE_ATOM': begin
          ipt.atom_file(ua_idx,0:nw1-1)=word(1:nw1)
                                ;test if this atomic file was already
                                ;used. If yes, set linid to the index
                                ;of the component which already used
                                ;this atom, otherwise increase linid
                                ;by one
          ipt.atm(ua_idx).linid=-1
          for iu=ua_idx-1,0,-1 do begin
            if (min(ipt.atom_file(iu,0:nw1-1) eq $
                    ipt.atom_file(ua_idx,0:nw1-1)) eq 1) then $
              ipt.atm(ua_idx).linid=ipt.atm(iu).linid  
          endfor
          if ipt.atm(ua_idx).linid eq -1 then $
            ipt.atm(ua_idx).linid=max(ipt.atm(0:ua_idx).linid)+1
          ua_idx=ua_idx+1
        end
        'USE_LINE': begin
          if nw1 gt 1 then message,'Only one line group can act on an' + $
            ' atmospheric component. Define a linegroup in def_line.pro'
          ipt.atm(ul_idx).use_line(0:strlen(word(1))-1,0)=byte(word(1))
          newlinegroup=1
          for iu=0,ul_idx-1 do begin
            if (min(ipt.atm(iu).use_line(0:strlen(word(1))-1,0) eq $
                    byte(word(1))) eq 1) then newlinegroup=0
          endfor
          linid=linid+newlinegroup
          ipt.atm(ul_idx).linid=linid
          message,/cont, $
            'Please use the new ''USE_ATOM'' keyword instead of ''USE_LINE''!'
          ul_idx=ul_idx+1
        end
        'WL_NUM': ipt.wl_num=(fix(word(1))>2)<maxwl
        'WL_OFF': ipt.wl_off=double(word(1))
        'WL_BIN': ipt.wl_bin=fix(word(1))>1
        'WL_DISP': ipt.wl_disp=double(word(1))
        'WL_RANGE': begin
          ipt.wl_range=double(word(1:2))
;          if total(ipt.wl_range^2) lt 1e-5 then ipt.wl_range=[0,1e6]
        end
        'LINES': ;obsolete keyword: lines from 'USE_LINE' parameter are used
;       'LINES': for iw=1,nw1 do begin
;         lin=def_line(line=strlowcase(word(iw)))
;         for il=0,n_elements(lin)-1 do begin
;           new=ipt.line(ipt.nline)
;           struct_assign,lin(il),new
;           ipt.line(ipt.nline)=new
;           ipt.nline=ipt.nline+1
;         endfor
;       endfor
;       'CCORR': ipt.ccorr=float(word(1))
        'STRAYPOL_AMP': ipt.straypol_amp=float(word(1))
        'HANLE_AZI': ipt.hanle_azi=float(word(1))
        'STRAYPOL_CORR': begin
          ipt.straypol_corr=fix(word(1))
          if nw1 lt 2 then begin
            message,/cont,'Please specify slit orientation (to solar NS-dir)' + $
              ' for straypol correction!'
          endif
          if word(2) eq 'B' then slitori_solar=999 $
          else slitori_solar=float(word(2))
        end
        'HIN_SCANNR': ipt.obs_par.hin_scannr=fix(word(1))
        'SLIT_ORIENTATION': ipt.obs_par.slit_orientation=float(word(1))
        'M1ANGLE': ipt.obs_par.m1angle=float(word(1))
        'SOLAR_RADIUS': ipt.obs_par.solar_radius=float(word(1))
        'SOLAR_POS': begin
          ipt.obs_par.posx=float(word(1))
          ipt.obs_par.posy=float(word(2))
        end
        'HELIO_ANGLE': ipt.obs_par.heliocentric_angle=float(word(1))
        '+Q2LIMB': ipt.obs_par.posq2limb=float(word(1))
        'LOCALSTRAY_RAD':ipt.localstray_rad=float(word(1))
        'LOCALSTRAY_FWHM':ipt.localstray_fwhm=float(word(1))
        'LOCALSTRAY_CORE':ipt.localstray_core=float(word(1))
        'LOCALSTRAY_POL':ipt.localstray_pol=fix(word(1))
        'APPROX_DIR':ipt.approx_dir(0:nw1-1)=fix(word(1:nw1))
        'UNNO': ipt.approx_dir=fix(word(1)) ;old keyword for approx_dir
        'APPROX_AZI': ipt.approx_azi(0:nw1-1)=fix(word(1:nw1))
        'AZI_UNNO': ipt.approx_azi=fix(word(1)) ;old keyword for approx_azi
        'ESTIMATE_UNNO': approx_dev=fix(word(1)) ;obsoltete keyword: if
                                ;approx_dev value is set then this keyword
                                ;is set to one automatically
        'APPROX_DEV_INC': ipt.approx_dev_inc(0:nw1-1)=float(word(1:nw1))
        'APPROX_DEV_AZI': ipt.approx_dev_azi(0:nw1-1)=float(word(1:nw1))
        'ESTIMATE_UNNO_RG_INC': ipt.approx_dev_inc=float(word(1)) ;old keyword
        'ESTIMATE_UNNO_RG_AZI': ipt.approx_dev_azi=float(word(1)) ;old keyword
        'IQUV_WEIGHT': begin
          if nw1 mod 4 ne 0 then $
            message,'weight vector has to be vector with 4xn elements'
          ipt.iquv_weight(*,0:nw1/4-1)=float(word(1:nw1))
          wgtcnt(0:nw1/4-1)=1
        end
        'WGT_FILE': begin
          ipt.wgt_file(0:nw1-1)=word(1:nw1)
          if nw1 eq 1 then ipt.wgt_file=word(1)
        end
        'PROFILE': begin
          ip=1
          case strlowcase(word(ip)) of
            'gauss': ipt.profile(ip-1)='gauss'
            'voigt': ipt.profile(ip-1)='voigt'
            else: begin
              ipt.profile(ip-1)='gauss'
              print,'undefined profile-shape, use gauss.'
            endelse
          endcase
        end
        'MAGOPT': begin
          ipt.magopt(0:nw1-1)=fix(word(1:nw1))
          if nw1 eq 1 then ipt.magopt=ipt.magopt(0)
        end
        'USE_GEFF': begin
          ipt.use_geff(0:nw1-1)=fix(word(1:nw1))
          if nw1 eq 1 then ipt.use_geff=ipt.use_geff(0)
        end
        'USE_PB': begin
          ipt.use_pb(0:nw1-1)=fix(word(1:nw1))
          if nw1 eq 1 then ipt.use_pb=ipt.use_pb(0)
        end
        'PB_METHOD': begin
          for j=1,nw1 do begin
            case strlowcase(word(j)) of
              'poly':  ipt.pb_method(j-1)=0
              'table':  ipt.pb_method(j-1)=1
              else: ipt.pb_method(j-1)=0
            endcase
          endfor
        end
        'VERBOSE': ipt.verbose=fix(word(1))
        'CODE': ipt.code=strupcase(word(1))
        'PIKAIA_STAT': ipt.pikaia_stat=strupcase(word(1))
        'METHOD': begin
          for iw=1,nw1 do begin
            case strupcase(word(iw)) of
              'PIKAIA': ipt.method(iw-1:*)=0
              'POWELL': ipt.method(iw-1:*)=1
              'LMDIF': ipt.method(iw-1:*)=2
              'LMDIFF': ipt.method(iw-1:*)=2
              else:
            endcase
          endfor
          if word(1) eq 'PIK_LM' then begin
            ipt.method(0:*)=3
            if nw1 ge 2 then ipt.piklm_bx=fix(word(1+1))
            if nw1 ge 3 then ipt.piklm_by=fix(word(1+2))
            if nw1 ge 4 then ipt.piklm_minfit=float(word(1+3))
          endif
        end
        'PIXELREP': ipt.pixelrep=fix(word(1))>1
        'KEEPBEST': ipt.keepbest=fix(word(1))
        'CHI2MODE': begin
          if strupcase(word(1)) eq 'JM' then ipt.chi2mode=1
          if strupcase(word(1)) eq 'PLAIN' then ipt.chi2mode=2
        end
        'NCALLS': ipt.ncalls(0:nw1-1)=fix(word(1:nw1))
        'PIKAIA_POP': begin
          ipt.pikaia_pop=fix(word(1))
          if ipt.pikaia_pop gt pik_PMAX then begin
            print,'PIKAIA_POP larger than maximum allowed value.'
            print,'PIKAIA_POP=',ipt.pikaia_pop,', MAX=',pik_PMAX
            print,'setting PIKAIA_POP=',pik_PMAX
            ipt.pikaia_pop=pik_PMAX
          endif
        end
        else: begin            ;all other words are treated as comment
          ipt.comment(ipt.n_cmt)=line(i)
          ipt.n_cmt=ipt.n_cmt+1
        end
      endcase
    endif ;input file lines
    
    if ipt.code eq 'IDL' then $
      if ipt.method(0) eq 2 or ipt.method(0) eq 3 then begin
      message,/cont,'METHOD must be PIKAIA or POWELL in the IDL version.'
      reset
    endif
    
    case strmid(ipt.pikaia_stat,0,3) of
      'NON':
      '': ipt.pikaia_stat='NONE'
      'POP': if ipt.verbose ge 1 then $
        print,'PIKAIA_STAT mode: ',ipt.pikaia_stat
      else: begin
        print,'Undefined PIKAIA_STAT mode. Available modes are:' + $
              ' ''NONE'', ''POPxx'''
        print,'Example: POP75 - take the fittest 75% of the PIKAIA population'
        print,'Taking default value: ''NONE'''
        ipt.pikaia_stat='NONE'
      end
    endcase
    
    if ipt.pikaia_stat ne 'NONE' and $
      (ipt.fitsout eq 0 or ipt.pixelrep ge 2) then begin
      print,'PIKAIA_STAT requires FITSOUT=1 and PIXELREP=1.'
      reset
    endif
        
;    if ipt.pikaia_stat ne 'NONE' and ipt.keepbest ge 1 then begin
;      if ipt.verbose ge 1 then print,'PIKAIA_STAT mode is on. Set KEEPBEST=0.'
;      ipt.keepbest=0
;    endif
  
    if ipt.norm_stokes eq '' then begin
      ipt.norm_stokes='IC'
      if ipt.verbose ge 1 then $
        message,/cont,'No normalization for Stokes profiles specified. ' + $
        'Assuming '+ipt.norm_stokes
    endif
    if ipt.norm_stokes ne 'I' and ipt.norm_stokes ne 'IC' then begin
      message,'Unknown normalization of Stokes profiles: '+ipt.norm_stokes
      reset
    endif
    if ipt.verbose ge 2 then print,'Stokes normalization: ',ipt.norm_stokes
    case ipt.norm_stokes of
      'I': ipt.norm_stokes_val=1
      'IC': ipt.norm_stokes_val=0
    endcase
    
    if ipt.synth ne 0 then ipt.keepbest=0
    
    nbl=0
    for i=0,n_tags(blcnt)-1 do nbl=max([nbl,blcnt.(i)])
    if ipt.nblend ne nbl and nbl ne 0 then begin
      message,/cont,'Wrong number of blends in your input file:'
      message,/cont,'NBLEND='+n2s(ipt.nblend)+', but # of blends = '+n2s(nbl)
      reset
    endif
    if ipt.nblend eq 0 then begin
      ipt.blend.par.a0=0
      ipt.nblend=1
    endif

    
    if ul_idx ne 0 then begin   ;use old 'USE_LINE' keyword
      if ul_idx eq 0 or  ul_idx ne ipt.ncomp then begin
        message,/cont, $
          'check USE_ATOM parameter: must be set for all components. Use USE_ATOM instead of USE_LINE.'
        reset
      endif
                                ;define lines
      all_lines=(remove_multi(/no_empty, $
                              strmid(string(ipt.atm.use_line),0,2)))(*)
      lin=def_line(line=all_lines,verbose=0)
      ipt.nline=0
      for il=0,n_elements(lin)-1 do begin
        new=ipt.line(ipt.nline)
        struct_assign,lin(il),new
        ipt.line(ipt.nline)=new
        ipt.nline=ipt.nline+1
      endfor
                                ;check if 'USE_LINE' parameter is set
      for ic=0,ipt.ncomp-1 do begin
        lin=def_line(line=string(ipt.atm(ic).use_line), $
                     verbose=ipt.verbose*(ic ne 0))
        nl=n_elements(lin)
        ipt.atm(ic).use_line(*,0:nl-1)=lin.id
      endfor
    endif                       ;use_line keyword
    
    if ul_idx eq 0 then begin   ;use new 'USE_ATOM' keyword
      if ua_idx eq 0 or  ua_idx ne ipt.ncomp then begin
        message,/cont, $
          'check USE_ATOM parameter: must be set for all components'
        reset
      endif
      
                                ;read in atomic data
      ipt.nline=0
      for ic=0,ipt.ncomp-1 do begin
        lines=read_atom(ipt.atom_file(ic,*),ipt.dir.atom,nline,ipt.verbose, $
                        max(ipt.use_pb))

;      ipt.atm(ic).use_line=lines.id
        for il=0,nline-1 do begin
          ipt.atm(ic).use_line(*,il)=lines(il).id
        endfor
        
                                ;remove multiple lines
        if ic eq 0 then begin
          ipt.line=lines
          nl=nline
        endif else begin
          nl=0
          for il=0,nline-1 do begin
            newline=1
            for ip=0,ipt.nline-1 do $
              if min(ipt.line(ip).id eq lines(il).id) eq 1 then newline=0
            if newline then begin
              if ipt.nline+nl ge maxlin then begin
                message,/cont, $
                  'Exceeding max. number of lines. maxlin='+n2s(maxlin)
                reset
              endif
              ipt.line(ipt.nline+nl)=lines(il)
              nl=nl+1
            endif
          endfor
        endelse
        ipt.nline=ipt.nline+nl
      endfor
    endif
    
                                ;check if component is straypol
                                ;capable
    for ic=0,ipt.ncomp-1 do begin
      ipt.atm(ic).straypol_use=0
      for il=0,ipt.nline-1 do $
        if string(ipt.atm(ic).use_line(0:1,il)) eq 'He' then $
        ipt.atm(ic).straypol_use=1
    endfor
    
                                ;fill the line-structure with the
                                ;line-parameter values from the input
                                ;file
    for il=0,lp_idx do begin
      idx=(where(abs(ipt.line.wl - lptmp(il).id) lt 1e-3))(0)
      if idx eq -1 then begin
        message,/cont,'LINE_ID '+n2s(lptmp(il).id,format='(f12.5)')+ $
          ' not found in atomic data file.'
        reset
      endif
      ipt.line(idx).par.strength=lptmp(il).par.strength
      ipt.line(idx).fit.strength=lptmp(il).fit.strength
      ipt.line(idx).scale.strength=lptmp(il).scale.strength
      ipt.line(idx).par.wlshift=lptmp(il).par.wlshift
      ipt.line(idx).fit.wlshift=lptmp(il).fit.wlshift
      ipt.line(idx).scale.wlshift=lptmp(il).scale.wlshift
    endfor
  endif else begin              ;read input from structure
    struct_assign,struct,ipt
    for it=0,n_tags(cnt)-1 do cnt.(it)=ipt.ncomp
    lines=ipt.line
  endelse
  
  
                                ;check compatibility of settings with
                                ;lines
  change_usepb=1
  for ic=0,ipt.ncomp-1 do begin
    for il=0,maxlin-1 do begin
      if ipt.atm(ic).use_line(0,il) ne 0 then begin
        f2l=string(ipt.atm(ic).use_line(0:1,il))
        if f2l eq 'He' then change_usepb=0
      endif
    endfor
  endfor
  if max(ipt.use_pb) gt 0 and change_usepb eq 1 then begin
    message,/cont,'USE_PB=1 only works for He 1083 nm.'
    ipt.use_pb=0
  endif

  if n_elements(struct) eq 0 then ipt.nmi=(max(where(ipt.ncalls gt 0)))>0+1
  
                                ;check old keyword estimate_unno 
  if n_elements(approx_dev) ne 0 then if approx_dev eq 0 then begin
    ipt.approx_dev_inc=0 & ipt.approx_dev_azi=0
  endif
                                ;set approx_dev flag
  ipt.approx_dev=(ipt.approx_dev_inc ne 0 or ipt.approx_dev_azi ne 0)
  
  for ic=0,ipt.ncomp-1 do $
    if ipt.verbose ge 1 then print,'Using lines (comp '+n2s(ic+1)+'): '+ $
    add_comma(id2s(ipt.atm(ic).use_line),sep=', ',/no_empty)
  
  
  if ipt.code eq '' then begin
    print,'computation method not specified, use FORTRAN'
    ipt.code='FORTRAN'
  endif
  
                                ;define atmospheric parameter mode
  if n_elements(struct) eq 0 then begin
    vp_only=['GDAMP','VMICI','DENSP','TEMPE']
    v_only=['DAMP','DOPP','ETAZERO']
    g_only=['WIDTH','A0']
    vg_only=['GDAMP','DOPP','ETAZERO']
    vs0_only=['GDAMP','DOPP','ETAZERO','SZERO']
    hs_only=['HEIGHT','DOPP','DSLAB']
    mode=['gauss','voigt-par','voigt-phys','voigt-gdamp','voigt-szero', $
          'hanle-slab']
    imode=0
    ipt.mode='-'
    for i=0,5 do begin
      if i eq 0 then begin
        apar=ipt.gauss_par & cpar=g_only
        modeval=0
      endif else if i eq 1 then begin
        apar=ipt.voigt_par & cpar=v_only
        modeval=1
      endif else if i eq 2 then begin
        apar=ipt.voigt_phys & cpar=vp_only
        modeval=2
      endif else if i eq 3 then begin
        apar=ipt.voigt_gdamp & cpar=vg_only 
        modeval=3
      endif else if i eq 4 then begin
        apar=ipt.voigt_szero & cpar=vs0_only
        modeval=4
      endif else begin
        apar=ipt.hanle_slab & cpar=hs_only
        modeval=5
      endelse
      icp=0
      for ip=0,n_elements(cpar)-1 do begin
        it=(where(name.idl eq cpar(ip)))(0)
        if it eq -1 then message,apar(ip)+' not defined. Check Code!'
        icp=icp+(cnt.(it) ne 0)
      endfor
      if icp eq n_elements(cpar) then begin
        ipt.mode=mode(i)
        ipt.modeval=modeval
        ipt.parset(0:n_elements(apar)-1)=apar
      endif
    endfor

    if ipt.mode eq '-' then begin
      message,/cont,'Incomplete atmospheric parameters.'
      message,/cont,'Available parameter sets are:'
      print
      for i=0,n_elements(mode) -1 do begin
        case i of 
          0: apar=ipt.gauss_par
          1: apar=ipt.voigt_par
          2: apar=ipt.voigt_phys
          3: apar=ipt.voigt_gdamp
          4: apar=ipt.voigt_szero
          5: apar=ipt.hanle_slab
          else:
        endcase
        print,'MODE: ',mode(i)
        print,'Parameters: '
        a='' 
        for ip=0,n_elements(apar)-1 do if apar(ip) ne '' then begin
          ii=where(name.idl eq apar(ip))
          a=a+' '+name.ipt(ii)
        endif
        print,a
      endfor
      reset
    endif
    if ipt.verbose ge 2 then print,'Parameter mode: '+ipt.mode

                                ;profile dependent settings
    done=0
    repeat begin
      case ipt.profile of
        'gauss': begin
          ipt.magopt=0
          ipt.use_geff=1
          ipt.use_pb=0
          ipt.pb_method=0
          ipt.modeval=0
          ipt.mode='gauss'
          ipt.parset=''
          ipt.parset(0:n_elements(ipt.gauss_par)-1)=ipt.gauss_par
          done=1
        end
        'voigt': begin
          ipt.magopt=ipt.magopt eq 1      
          if ipt.magopt(0) eq 0 then print,'Ignoring magneto-optical effects'
;          ipt.parset(0:n_elements(ipt.voigt_par)-1)=ipt.voigt_par
          done=1
        end
        else: ipt.profile='voigt'
      endcase
    endrep until done
    
;  magopt=ipt.magopt
;  modeval=ipt.modeval
    
    
                                ;set unused parameters to 'no fit'
    for it=0,n_elements(name.idl)-1 do begin
      if max(ipt.parset eq name.idl(it)) eq 0 then begin ;not used
        ipt.atm.par.(it)=0 & ipt.atm.fit.(it)=0
      endif
    endfor
    
    
    
                                ;check for missing parameters
    par_error=0
    for i=0,n_elements(ipt.parset)-1 do if ipt.parset(i) ne '' then begin
      idx=(where(name.idl eq ipt.parset(i)))(0)
      ncmp=ipt.ncomp
      cmax=0
      for it=0,n_tags(ipt.atm(0).fit)-1 do cmax=max(abs(ipt.atm.fit.(it)))>cmax
      case 1 of
        name.ipt(idx) eq 'ALPHA': ncmp=(ncmp-1)>0
;        name.ipt(idx) eq 'SGRAD' or $
        name.ipt(idx) eq 'ETAZERO': begin
          if cnt.(idx) eq 1 then begin
            if ipt.atm(0).fit.(idx) gt 0 then ipt.atm.fit.(idx)=-cmax-1
            ipt.atm.par.(idx)=ipt.atm(0).par.(idx)
            for ia=1,ipt.ncomp-1 do ipt.scale(ia).(idx)=ipt.scale(0).(idx)
            ncmp=1
          endif
        end
        else:
      endcase
      if cnt.(idx) lt ncmp then begin
        if ipt.modeval eq 5 and name.ipt(idx) eq 'VDAMP' then begin
          il=min(where(ipt.line.use_hanle_slab eq 1))
          if il ge 0 then begin
            print,'   Setting VDAMP for all components to default value for '+ $
              'hanle_slab mode.'
            print,'   The default value is calculated from DOPP and corresponds'
            print,'   to thermal broadening effects only.'
            ipt.atm(0:ncmp-1).fit.damp=0
            ipt.atm(0:ncmp-1).par.damp=-1
          endif else par_error=1
        endif else begin
          print,'Missing parameter: '+name.ipt(idx)+' for component '+ $
            add_comma(sep='+',n2s(fix(indgen(ncmp-cnt.(idx))+cnt.(idx)+1))) + $
            ',  Suggestion:'
          print,name.ipt(idx),(pre.(idx).min+pre.(idx).max)/2., $
            pre.(idx).min,pre.(idx).max,1,format='(a5,3f12.2,i3)'
        endelse
      endif
    endif
;print_atm,ipt.atm(0:ipt.ncomp-1),scale=ipt.scale(0:ipt.ncomp-1) & stop
    if par_error then reset
    
;    ipt=check_scaling(cnt=cnt,ipt=ipt)
    
                                ;set weighting equal to first
                                ;weighting for multi iteration
    for i=1,ipt.nmi-1 do begin
      if wgtcnt(i) eq 0 then $
        ipt.iquv_weight(*,i)=ipt.iquv_weight(*,i-1)
    endfor
    
    
                                ;add gauss fit to I only if
                                ;straylight-polarization correction is
                                ;chosen
    if ipt.straypol_corr gt 0 then begin
      pre=check_scaling(/predef,ipt=ipt)
      ipt.nmi=ipt.nmi+1
      n=ipt.nmi-1
      ipt.ncalls(n)=ipt.straypol_corr
      ipt.method(n)=max(ipt.method(0:n-1))
                                ;use the same fitting rules as for the
                                ;normal components in the input file
                                ;(including coupling and fixing of
                                ;parameters)
      for it=0,n_elements(name.idl)-1 do begin
        ipt.mi.(it).fit(n)=ipt.atm.fit.(it)
        ipt.mi.(it).perc_rg(n)=0
      endfor
;       for it=0,n_elements(name.idl)-1 do begin
;         ipt.mi.(it).fit(n)=0
;         ipt.mi.(it).perc_rg(n)=0
;       endfor      
;       ipt.mi.sgrad.fit(n)=0
;       ipt.mi.vlos.fit(n)=1    & ipt.mi.vlos.perc_rg(n)=100
;       ipt.mi.width.fit(n)=1   & ipt.mi.width.perc_rg(n)=100
;       ipt.mi.a0.fit(n)=1      & ipt.mi.a0.perc_rg(n)=100
;       ipt.scale.a0.min=pre.a0.min
;       ipt.scale.a0.max=pre.a0.max
;       ipt.scale.width.min=pre.width.min
;       ipt.scale.width.max=pre.width.max
;      ipt.use_geff(n)=1
;      ipt.magopt(n)=0
;      ipt.approx_dir(n)=0
;      ipt.approx_azi(n)=0
      ipt.iquv_weight(*,n)=[1,0,0,0]
    endif

                                ;cannot have use_geff=1 and use_pb=1
    geffwarn=max((ipt.use_pb(0:ipt.nmi-1) eq 1) and $
                 (ipt.use_geff(0:ipt.nmi-1) eq 1))
    if geffwarn eq 1  then begin
      print,'Incompatible parameters: USE_GEFF=1 and USE_PB=1'
      reset
    endif
    
    
    if ipt.modeval eq 5 then begin ;consistency checks for He slab model
                                ;check if one He line carries the flag
                                ;'use_hanle_slab'
      if total(ipt.line.use_hanle_slab) ne 1 then begin
        message,/cont,'Problem. Code should have set use_hanle_slab=1 for 1 line'
        reset
      endif
    endif
                                ;check if FF are within minmax values
    normff=0
    for ia=0,ipt.ncomp-1 do begin
      if (ipt.atm(ia).par.ff lt ipt.scale(ia).ff.min or $
          ipt.atm(ia).par.ff gt ipt.scale(ia).ff.max) then begin
        normff=1
;        ipt.atm(ia).par.ff= $
;          (ipt.scale(ia).ff.min+ipt.scale(ia).ff.max)/2.+ipt.scale(ia).ff.min
        if ipt.verbose ge 1 then $
          message,/cont,'FF for comp. '+n2s(ia+1)+ $
          ' is out of range. New value: '+n2s(ipt.atm(ia).par.ff)
      endif
    endfor
    if normff eq 1 then $
      ipt.atm(0:ipt.ncomp-1).par.ff=norm_ff(ipt.atm(0:ipt.ncomp-1).par.ff, $
                             ipt.atm(0:ipt.ncomp-1).fit.ff*0+1, $
                             ipt.atm(0:ipt.ncomp-1).linid)
    
                                ;add localstraylight component
    if ipt.localstray_rad ge 1e-5 then begin
      if ipt.verbose ge 2 then print,'Adding local straylight component.'
      dols=1
      lspol=ipt.localstray_pol
      ipt.ncomp=ipt.ncomp+1
      na1=ipt.ncomp-1
      if total(ipt.atm(0:na1-1).fit.ff) ne 0 then ipt.atm(na1).fit.ff=1
      ipt.scale(na1).ff=pre.ff
      
                                ;if fitflag for FF of all other components is
                                ;zero, then fit the first component
;      if total(ipt.atm(0:ipt.ncomp-1).fit.ff) le 1 then ipt.atm(0).fit.ff=1 
      ipt.atm(na1).par.ff=(1.-total(ipt.atm(0:ipt.ncomp-2).par.ff))>0.01
                                ;reduce FFs for other components since a new
                                ;FF was added
;       ipt.atm(0:ipt.ncomp-2).par.ff=ipt.atm(0:ipt.ncomp-2).par.ff/ $
;         (total(ipt.atm(0:ipt.ncomp-2).fit.ff)+1)* $
;         total(ipt.atm(0:ipt.ncomp-2).fit.ff)      
                                ;make consistency checks for FF
      ffmin=total(ipt.scale(0:ipt.ncomp-2).ff.min)
      ffmax=total(ipt.scale(0:ipt.ncomp-2).ff.max)
      ipt.scale(na1).ff.min=1.-ffmax
      ipt.scale(na1).ff.max=1.-ffmin
;      ipt.atm(na1).par.ff= $
;        (ipt.scale(na1).ff.max-ipt.scale(na1).ff.min)/2.+ipt.scale(na1).ff.min
      ipt.atm(na1).linid=ipt.atm(0).linid   
    endif
    
                                ;check scaling
    verbose=ipt.verbose
    scale=ipt.scale(0:ipt.ncomp-1)
    ipt=check_scaling(cnt=cnt,ipt=ipt)
    
    
                                ;set sum of FF to one
    ipt.atm(0:ipt.ncomp-1).par.ff= $
      norm_ff(ipt.atm(0:ipt.ncomp-1).par.ff, $
              ipt.atm(0:ipt.ncomp-1).fit.ff*0+1, $
              ipt.atm(0:ipt.ncomp-1).linid)
                                ;if filling factor of at least one
                                ;component is a free parameter, then
                                ;the FF of another component must also
                                ;be a free parameter

    if ipt.ncomp gt 1 then $
      if total(ipt.atm(0:ipt.ncomp-1).fit.ff) eq 1 then begin
      message,/cont,"PROBLEM:"
      message,/cont,"    Filling factor of only one component is free parameter!"
      message,/cont,"    Code has no freedom to adjust filling factors."
    endif
    
                                ;do not fit FF if only one component
    if ipt.ncomp eq 1 then begin
      ipt.atm.par.ff=1
      ipt.atm.fit.ff=0
      ipt.mi.ff.fit=0
    endif
    
                                ;convert atm to parameter and back:
                                ;coupled parameters are treated
                                ;correctly
    ipt.atm(0:ipt.ncomp-1)=get_pikaia_idx(ipt.atm(0:ipt.ncomp-1))
    ipt.line(0:ipt.nline-1)=get_pikaia_idx(ipt.line(0:ipt.nline-1), $
                                           nadd=ipt.atm(0).npar)
    ipt.blend(0:ipt.nblend-1)=get_pikaia_idx(ipt.blend(0:ipt.nblend-1), $
                                             nadd=ipt.atm(0).npar+ $
                                             ipt.line(0).npar)
    ipt.gen=get_pikaia_idx(ipt.gen,nadd=ipt.atm(0).npar+ $
                           ipt.line(0).npar+ipt.blend(0).npar)
    
    dpar=struct2pikaia(atm=ipt.atm(0:ipt.ncomp-1),par=par, $
                       line=ipt.line(0:ipt.nline-1), $
                       blend=ipt.blend(0:ipt.nblend-1),gen=ipt.gen)    
    ipt.atm(0:ipt.ncomp-1)=struct2pikaia(atm=ipt.atm(0:ipt.ncomp-1),par=par, $
                                         line=ipt.line(0:ipt.nline-1), $
                                         blend=ipt.blend(0:ipt.nblend-1), $
                                         gen=ipt.gen, $
                                         /reverse)
                                ;extract file date
   if n_elements(struct) eq 0 then ipt.date.file=date
    
  endif                         ;struct mode
  
  
;   if ipt.profile_list ne '' then begin
;     ipt.piklm_bx=10000l
;     ipt.piklm_by=10000l
;   endif
  
                                ;calculate heliocentric angle and +Q direction
  if ipt.verbose ge 2 and ipt.modeval eq 5 then begin
    print,'Emission Vector: read from input file'
    print,'   Heliocentric Angle    = ', $
      string(ipt.obs_par.heliocentric_angle,format='(f7.2)')
    print,'   +Q to limb direction  = ', $
      string(ipt.obs_par.posq2limb,format='(f7.2)')
    print,'   Hanle emission vector = ', $
      string([ipt.obs_par.heliocentric_angle,0., $
              (360.-ipt.obs_par.posq2limb+90.+360.) mod 360.], $
             format='(3f7.2)')
  endif
  
  if ipt.conv_output eq 1 and dols eq 1 then begin
    message,/cont,'CONV_OUTPUT must be 0 when local straylight correction' + $
      ' is used. Continue with CONV_OUTPUT=0'
    ipt.conv_output=0
  endif
  
                                ;fill in parameters required for fitting
  scale=ipt.scale(0:ipt.ncomp-1)  
  for i=0,ipt.ncomp-1 do for it=0,n_tags(ipt.atm(i).fitflag)-1 do $
    ipt.nfitpar=ipt.nfitpar+ipt.atm(i).fitflag.(it)
  ipt.nfitpar=ipt.nfitpar+ipt.line(0).npar+ipt.blend(0).npar+ipt.gen(0).npar
  catm=ipt.atm(0:ipt.ncomp-1)
  catm.fit=catm.fitflag
  catm=get_pikaia_idx(catm)
  cline=get_pikaia_idx(ipt.line(0:ipt.nline-1),nadd=catm(0).npar)
  cblend=get_pikaia_idx(ipt.blend(0:ipt.nblend-1), $
                        nadd=catm(0).npar+cline(0).npar)
  cgen=get_pikaia_idx(ipt.gen,nadd=catm(0).npar+cline(0).npar+cblend(0).npar)
  fitparname=strarr(maxfitpar)
  par=struct2pikaia(atm=catm,par=parin,line=cline,blend=cblend,gen=cgen, $
                    reverse=n_elements(parin) ge 1,noscale=1,retname=1, $
                    fitparname=fitparname)
  if n_elements(parin) ge 1 then begin
    ipt.atm(0:ipt.ncomp-1).par=catm.par
    ipt.gen.par=cgen.par
    ipt.blend(0:ipt.nblend-1).par=cblend.par
    ipt.line(0:ipt.nline-1).par=cline.par
  endif
  ipt.fitpar=fitparname
  
  if ipt.pikaia_pop eq 0 then begin
    npar=n_elements(par)
;    ipt.pikaia_pop=(36>((npar*5)/2*2)<256)<pik_PMAX
    ipt.pikaia_pop=(36>(npar*5)*2)<pik_PMAX
    if ipt.verbose ge 2 then print,'setting PIKAIA_POP=',ipt.pikaia_pop
  endif
  
  ipt.pikaia_stat_cnt=0
  if ipt.pikaia_stat ne 'NONE' then begin
    frc=float(strmid(ipt.pikaia_stat,3,10))/100.
    ipt.pikaia_stat_cnt=fix(ipt.pikaia_pop*frc)>1
    if ipt.verbose ge 1 then  $
      print,'PIKAIA_STAT=',ipt.pikaia_stat,': use ', $
            ipt.pikaia_stat_cnt,' of ',ipt.pikaia_pop,' populations'
  endif
 
  
  if ipt.verbose ge 1 then begin
    print,'Done reading input file: '+ipt.file+ $
      ' (mod. time: '+ipt.date.file+')'
    if ipt.verbose ge 2 then begin
      print,'Comments: '
      for ic=0,ipt.n_cmt-1 do print,ipt.comment(ic)
    endif
  endif
  
  return,ipt
end

