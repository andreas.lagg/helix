function def_blendname,tagnam
  
  blend_name=tagnam
  for it=0,n_elements(tagnam)-1 do case tagnam(it) of
    'WL': blend_name(it)='BLEND_WL'
    'A0': blend_name(it)='BLEND_A0'
    'WIDTH': blend_name(it)='BLEND_WIDTH'
    'DAMP': blend_name(it)='BLEND_DAMP'
    else:
  endcase
  name={ipt:blend_name,idl:tagnam}
  return,name
end
