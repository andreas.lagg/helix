function call_cp,ipt=ipt,line=line,atm=atm,obs_par=obs_par, $
            wl=wlvec,conv=conv,blend=blend,gen=gen, $
            prefilterval=prefilterval, $
            doprefilter=doprefilter,dols=dols, $
            ret_wlidx=ret_wlidx,nret_wlidx=nret_wlidx,norm_ff=norm_ff, $
            comp=comp
  common verbose,verbose
  common sodir,idlso_dir
  common cpinit,fsinit,isinit
@common_cppar
@common_maxpar

  if n_elements(idlso_dir) eq 0 then idlso_dir='./idl.so/'
  
  if n_elements(comp) eq 0 then comp=-1
  
  if n_elements(norm_ff) eq 0 then normff=1 else normff=keyword_set(norm_ff)
  
  nwl=fix(n_elements(wlvec))
  natm=n_elements(atm)
  nline=fix(ipt.nline)
  use=intarr(nline,natm)
  for il=0,nline-1 do for ia=0,natm-1 do $
    use(il,ia)=max(id2s(atm(ia).use_line) eq $
                         id2s(line(il).id))
  old_norm=ipt.old_norm         ;flag for old normalization
  old_voigt=ipt.old_voigt         ;flag for old voigt
  voigt=fix(ipt.profile eq 'voigt')
  modeval=fix(ipt.modeval)
  nblend=n_elements(blend)
  
  
  compwl=wlvec
  cnwl=conv.nwl
  if cnwl ge 1 then begin
    convval=conv.val(0:cnwl-1)
    compwl=conv.wl(0:cnwl-1)
  endif else convval=-1
  cnwl=n_elements(compwl)
  doconv=conv.doconv
  cmode=conv.mode
  if ipt.conv_output eq 0 or dols eq 1 then begin
    ret_wlidx=conv.iwl_compare
    nret_wlidx=conv.nwl_compare
    fill_localstray,lsi,lsq,lsu,lsv,dols, $
      wlvec(conv.iwl_compare),conv.nwl_compare
  endif else begin
    ret_wlidx=intarr(maxwl)
    nret_wlidx=-1
      fill_localstray,lsi,lsq,lsu,lsv,dols,wlvec,nwl   
  endelse
  
  code=ipt.code
;  print,'call_cp Code: IDL' &  code='IDL'  
  if code eq 'IDL' then begin
      if n_elements(isinit) eq 0 then isinit=1 else isinit=0
     synthprof=compute_profile(init=isinit,line=line,atm=atm, $
                              obs_par=obs_par, $
                              wl=compwl,blend=blend,gen=gen, $
                              lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv, $
                               dols=dols,lspol=ipt.localstray_pol, $
                               convval=convval,doconv=doconv, $
                               conv_mode=cmode, $
                              prefilterval=prefilterval, $
                              doprefilter=doprefilter,normff=normff, $
                               ret_wlidx=ret_wlidx,nret_wlidx=nret_wlidx, $
                              comp=comp)
;     if nret_wlidx eq -1 then $
;      synthprof={ic:1.,wl:synthprof.wl,i:synthprof.i, $
;                 q:synthprof.q,u:synthprof.u,v:synthprof.v}
   endif else begin
    if n_elements(fsinit) eq 0 then fsinit=1 else fsinit=0
                                ;do fortran subroutine call
    iprof=float(compwl*0) & qprof=iprof & uprof=iprof & vprof=iprof
    iprof_only=0
    compf=comp
    if compf ne -1 then compf=compf+1
    fcall=call_external(idlso_dir+'compute_profile.so', $
                        'compute_profile_pro_',/unload, $
                        atm,natm, $
                        line,nline, $
                        compwl,cnwl, $
                        blend,nblend, $
                        gen, $
                        lsi,lsq,lsu,lsv,dols,ipt.localstray_pol, $
                        convval,doconv,cmode, $
                        prefilterval,doprefilter, $
                        ret_wlidx+1,nret_wlidx, $
                        obs_par, $
                        fsinit,voigt,magopt,use_geff,use_pb,pb_method, $
                        modeval,iprof_only,use, $
                        iprof,qprof,uprof,vprof,normff,old_norm,compf, $
                        hanle_azi,norm_stokes_val,old_voigt)   
    if nret_wlidx le 0 then $
       synthprof={ic:1.,wl:compwl,i:iprof,q:qprof,u:uprof,v:vprof} $
     else $
       synthprof={ic:1.,wl:compwl(ret_wlidx(0:nret_wlidx-1)), $
                  i:iprof(0:nret_wlidx-1),q:qprof(0:nret_wlidx-1), $
                  u:uprof(0:nret_wlidx-1),v:vprof(0:nret_wlidx-1)}
    
  endelse
;    synthprof={ic:1.,wl:wlvec,i:iprof,q:qprof,u:uprof,v:vprof}
;          fp=synthprof
;          ip=compute_profile(/init,line=all_lines,atm=total_atm, $
;                             obs_par=obs_par,wl=wlvec)
;          !p.multi=[0,1,4] & userlct & erase & !p.charsize=1.5
;          plot,ip.wl,fp.i-ip.i
;          plot,ip.wl,fp.q-ip.q
;          plot,ip.wl,fp.u-ip.u
;          plot,ip.wl,fp.v-ip.v
  return,synthprof
end
