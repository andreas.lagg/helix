;routine to read in convolution function
function read_prefilter,file,prefilter_wlerr,wlin,verbose
  @common_maxpar
  
  prefilter=def_prefilterst()
  prefilter.nwl=0
  prefilter.doprefilter=0
  
  if file eq '' then return,prefilter
  openr,unit,/get_lun,file,error=err
  if err ne 0 then begin
    message,/cont,'Could not open prefilter file: '+file
    reset
  endif
  nwl=n_elements(wlin)
  if verbose ge 1 then print,'Read prefilter file '+file
  
  iw=0
  wl=dblarr(maxcwl) & val=fltarr(maxcwl)
  repeat begin
    line=''
    readf,unit,line
    cline=strtrim(line,2)
    
    if cline ne '' and strmid(cline,0,1) ne ';' then begin
      if iw ge maxcwl then begin
        message,/cont,'Max. # of WL points in prefilter-file exceeded: '+ $
          n2s([iw,maxcwl])
        prefilter.doprefilter=0
        message,/cont,'Change MAXCWL in all_type.f90 and maxpar.pro'
        message,/cont,'WARNING: no prefilter applied!'
        reset
      endif
      word=strsplit(cline,' ',/extract)
      if n_elements(word) eq 2 then begin
        wl(iw)=double(word(0))
        val(iw)=float(word(1))
        iw=iw+1
      endif
    endif
  endrep until eof(unit)
  ncwl=iw 
  free_lun,unit
  
  if prefilter_wlerr gt 0 then begin
    if verbose ge 1 then $
      print,'Adding WL error (\AA) to prefilter (as normal noise): ', $
      prefilter_wlerr
    wl=wl+randomu(seed,/normal)*prefilter_wlerr
  endif
  
  
  show=0
  if show then begin
    plot,wl(0:ncwl-1),val(0:ncwl-1),ytype=1,/xst,title=file,xtitle='WL [A]',ytitle='prefilter function'
  endif
  
  prefilter.doprefilter=1
  
                                ;the WL bins for the prefilter must be the
                                ;same as for the observations.
  
                                ; manual interpolation routine (to be
                                ; consistent with the fortran version)
  if min(wlin) lt min(wl(0:ncwl-1)) or max(wlin) ge max(wl(0:ncwl-1)) then begin
    message,cont,'Prefilter curve does not cover the full WL range of the' + $
      ' observations. Please extend the WL coverage for the prefilter curve.'
    reset
  endif
  prefilter.wl(0:nwl-1)=wlin
  for i=0,nwl-1 do begin
    dummy=min(abs(wlin(i)-wl(0:ncwl-1)),idx)
    prefilter.val(i)=(val(idx+1)-val(idx))/(wl(idx+1)-wl(idx))* $
      (wlin(i)-wl(idx))+val(idx)
    endfor
  
;  
                                ;normalize prefilter function to it's max
;  prefilter.val(0:nwl-1)= prefilter.val(0:nwl-1)/max(prefilter.val(0:nwl-1))
  
;  plot,wl(0:ncwl-1),val(0:ncwl-1),/xst & oplot,color=1,prefilter.wl(0:nwl-1),prefilter.val(0:nwl-1) & stop
  return,prefilter
end
