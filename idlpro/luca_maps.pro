;write out maps for Luca, needs call to xdisplay first
pro luca_maps
  @common_user_coords
  common param
  common pikaia,pikaia_result,oldsav,prpar
  common wgst
  
  print,'Writing out Maps for Luca (combined observations with SUMER)'
  print,'Make sure that the correct map is loaded in xdsiplay'
  print,'Make sure that correct alignment is loaded (align_mdi sav-file)'
  
  wgst.wlrange.val=[10825d , 10826.] ;get cont image
  
  img=get_img(comp=1,/no_sav,xcexact=xcexact,ycexact=ycexact)
  xtip=pix2mm(xcexact,x=3) ;make sure that coordinate system in xdisplay
  ytip=pix2mm(ycexact,y=3)     ;is set to 'Other' and load the correct
                                ;align_mdi image!!!
  
  title=pikaia_result.input.observation+'_'+pikaia_result.input.dir.atm_suffix
  
  b=reform(img(5,0,*,*))
  azi=reform(img(6,0,*,*))
  inc=reform(img(7,0,*,*))
  vlos=reform(img(8,0,*,*))
  icont=reform(img(0,0,*,*))
  time=uc_tref
  
  wgst.wlrange.val=[10826.d , 10827.1] ;get si magnetogram
  img=get_img(comp=1,/no_sav)
  si_stokes_v=reform(img(4,0,*,*))
  wgst.wlrange.val=[10826.d , 10828] ;get si intensity
  img=get_img(comp=1,/no_sav)
  si_i=reform(img(4,0,*,*))
  
  wgst.wlrange.val=[10829.5d , 10830.35] ;get he magnetogram
  img=get_img(comp=1,/no_sav)
  he_stokes_v=reform(img(4,0,*,*))
  
  wgst.wlrange.val=[10829.5d , 10831.5] ;get he intensity
  img=get_img(comp=1,/no_sav)
  he_i=reform(img(0,0,*,*))
  
  map={xtip:xtip,ytip:ytip,time:time,b:b,azi:azi,inc:inc,vlos:vlos, $
       he_sigv:he_stokes_v,he_sigi:he_i,icont:icont, $
       si_sigv:si_stokes_v,si_sigi:si_i}
  
  sf='./sav/luca_'+title+'.sav'
  save,file=sf,/xdr,/compress,map,/verbose
  print,'Wrote sav-file: ',sf
  
end
  
