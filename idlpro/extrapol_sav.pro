pro extrapol_sav,sav=sav,btot=btot,azi=azi,inc=inc,data=data
  
  if n_elements(inc) eq 0 or n_elements(azi) eq 0 or $
    n_elements(btot) eq 0 then return
  
  if n_elements(data) ne 0 then begin
  data_dir='/samfs/sun/irpolar/data_may01/13may01/'
  df=data_dir+data
  dummy=findfile(df,count=cnt)
  if cnt eq 0 then begin
    message,/cont,'Observation not found: '+df
  endif else cal_lb,df,header=header
  endif
  
  bz=btot*cos(inc*!dtor)
  bx=btot*sin(azi*!dtor)*sin(inc*!dtor)
  by=btot*cos(azi*!dtor)*sin(inc*!dtor)
  
  
  info='No correction for LOS-angle to surface normale.' + $
    ' 180� ambiguity not removed!'
  if n_elements(header) ne 0 then info=info+header.pos
  save,file=sav,/compress,/xdr,bx,by,bz,btot,azi,inc,info
  print,'Wrote sav file for Field extrapolation (T. Wiegelmann): '+sav  
end
