pro h_anal,ps=ps
  
  !p.charsize=1.3
  psset,ps=ps,size=[18,16],file='h_anal.ps',/no_x
  userlct & erase
  
  sf=file_search('./sav/13may01.014cc_fix_h??.pikaia.sav',count=nsav)
  
  restore,sf(0)
  st={h:0.,fitness:0.,stat:statistic}
  sav=replicate(st,nsav)
  for is=0,nsav-1 do begin
    restore,sf(is)
    sav(is).h=float(strmid(sf(is),23,2))
    sav(is).stat=statistic
    sav(is).fitness=max(statistic.fitness)
  endfor
  
  xrange=[0.,20]*0.7
  yrange=minmaxp(sav.fitness)
  yrange=yrange+[-1,1]*.2
  plot,/xst,/yst,sav.h*0.7,sav.fitness,xrange=xrange,yrange=yrange,xtitle='Height [Mm]',ytitle='Best Fitness of 100 HeLiX+ inversions'
  
  psset,/close
  
end
  
  
