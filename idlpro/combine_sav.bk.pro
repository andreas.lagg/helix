pro combine_sav
  
  restore,/v,'/scratch/lagg/sav/he_3comp_orig.pikaia.sav'  
  pik3=temporary(pikaia_result)
  restore,/v,'/scratch/lagg/sav/he_2comp_str.pikaia.sav'
  pik2=temporary(pikaia_result)

 xpix_16=[81,82,83]  
  xpix_17=[79,80,81,82,83,84]  
  xpix_18=[79,80,81,82,83,84]  
  xpix_19=[79,80,81,82,83,84,85]  
  xpix_20=[79,80,81,82,83,84,85]  
  xpix_21=[79,80,81,82,83,84,85,86]  
  xpix_22=[79,80,81,82,83,84,85,86]  
  xpix_23=[79,80,81,82,83,84,85,86]  
  xpix_24=[81,82,83,84,85,86,87]  
  xpix_25=[81,82,83,84,85,86,87]  
  xpix_26=[80,81,84,85,86,87,88]  
  xpix_27=[80,81,84,85,86,87,88]  
  xpix_28=[77,78,79,80,81,85,86,87,88]  
  xpix_29=[76,77,78,79,80,81,85,86,87,88]  
  xpix_30=[76,77,78,79]
  xpix_31=[76,77,78,82]
  xpix_32=[76,77,84]
  xpix_33=[82,83,84]

  ypix_16=intarr(n_elements(xpix_16))+16
  ypix_17=intarr(n_elements(xpix_17))+17
  ypix_18=intarr(n_elements(xpix_18))+18
  ypix_19=intarr(n_elements(xpix_19))+19
  ypix_20=intarr(n_elements(xpix_20))+20
  ypix_21=intarr(n_elements(xpix_21))+21
  ypix_22=intarr(n_elements(xpix_22))+22
  ypix_23=intarr(n_elements(xpix_23))+23
  ypix_24=intarr(n_elements(xpix_24))+24
  ypix_25=intarr(n_elements(xpix_25))+25
  ypix_26=intarr(n_elements(xpix_26))+26
  ypix_27=intarr(n_elements(xpix_27))+27
  ypix_28=intarr(n_elements(xpix_28))+28
  ypix_29=intarr(n_elements(xpix_29))+29
  ypix_30=intarr(n_elements(xpix_30))+30
  ypix_31=intarr(n_elements(xpix_31))+31
  ypix_32=intarr(n_elements(xpix_32))+32
  ypix_33=intarr(n_elements(xpix_33))+33

  xpix=[xpix_16,xpix_17,xpix_18,xpix_19,xpix_20,xpix_21,xpix_22,xpix_23,$
        xpix_24,xpix_25,xpix_26,xpix_27,xpix_28,xpix_29,xpix_30,xpix_31,$
        xpix_32,xpix_33]
  ypix=[ypix_16,ypix_17,ypix_18,ypix_19,ypix_20,ypix_21,ypix_22,ypix_23,$
        ypix_24,ypix_25,ypix_26,ypix_27,ypix_28,ypix_29,ypix_30,ypix_31,$
        ypix_32,ypix_33]

;stop

  xpix=xpix-60.
  ypix=ypix-5.

  sz=size(pik2.fit)
  for ix=0,sz(1)-1 do for iy=0,sz(2)-1 do begin
    if max(xpix eq ix and ypix eq iy) then begin
;      
    endif else begin
      pik3.fit(ix,iy).atm(0:1)=pik2.fit(ix,iy).atm
      pik3.fit(ix,iy).fitness=pik2.fit(ix,iy).fitness
      for it=0,n_tags(pik3.fit.atm.fit)-1 do begin
        pik3.fit(ix,iy).atm(2).par.(it)=0.
        pik3.fit(ix,iy).atm(2).fit.(it)=0.
        pik3.fit(ix,iy).atm(2).idx.(it)=0
        pik3.fit(ix,iy).atm(2).ratio.(it)=1.
      endfor
      pik3.fit(ix,iy).atm(2).linid=0
      pik3.fit(ix,iy).atm(2).use_line=0
    endelse
  endfor
  
  pikaia_result=pik3
  
  save,file='~/tmp/test.sav',/xdr,/compress,pikaia_result
end
