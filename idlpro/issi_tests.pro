pro issi_tests
  ipt=read_ipt('./input/issi_synth_01.ipt')
  iptorig=ipt
 
  B=[10.,100.,500.,1500.,3000]
  INC=[0.,45.,90]
  AZI=[30.]
  ETA0=10.
  S0=0.5
  S1=0.5
  DAMP=0.1
  DOPP=0.03
  VLOS=0.
  
  for ib=0,n_elements(b)-1 do for ii=0,n_elements(inc)-1 do $
    for ia=0,n_elements(azi)-1 do for ie=0,n_elements(eta0)-1 do $
    for is0=0,n_elements(s0)-1 do for is1=0,n_elements(s1)-1 do $
    for idmp=0,n_elements(damp)-1 do for idop=0,n_elements(dopp)-1 do $
    for iv=0,n_elements(vlos)-1 do begin
    
    ipt.atm(0).par.b=b(ib)
    ipt.atm(0).par.inc=inc(ii)
    ipt.atm(0).par.azi=azi(ia)
    ipt.atm(0).par.vlos=vlos(iv)
    ipt.atm(0).par.etazero=eta0(ie)
    ipt.atm(0).par.szero=s0(is0)
    ipt.atm(0).par.sgrad=s1(is1)
    ipt.atm(0).par.dopp=dopp(idop)
    ipt.atm(0).par.damp=damp(idmp)
    
    helix,struct_ipt=ipt,fitprof=fitprof
    
    st={b:b(ib),inc:inc(ii),azi:azi(ia),vlos:b(iv),etazero:eta0(ie), $
        szero:s0(is0),sgrad:s1(is1),dopp:dopp(idop),damp:damp(idmp), $
        wl:fitprof.wl,i:fitprof.i,q:fitprof.q,u:fitprof.u,v:fitprof.v}

    if n_elements(hlx) eq 0 then hlx=replicate(st,n_elements(b),n_elements(inc))
    hlx(ib,ii)=st
  endfor
  save,/xdr,/compress,file='~/work/projects/ISSI_InversionWorkshop/MEtest//helix_synth.sav',hlx
  
  stop
end
  
