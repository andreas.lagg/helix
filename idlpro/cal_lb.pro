pro cal_lb,file,l=l,b=b,NEWS=NEWS,header=header,flat=flat
  common reduce,reduce
  
  
  if n_elements(flat) eq 0 then flat=0 else flat=keyword_set(flat)
  
  for ii=0,n_elements(file)-1 do begin
    
                                ;empty header
    date='000000'
    step_sx=0.
    r0=0.
    time='0000'
    b0=0.
    p0=0.
    l0=0.
    sp_x=0. & sp_1=1 & sp_x1=0.
    sp_y=0. & sp_2=1 & sp_y1=0.
    s_or=0.
    naxis=0
    naxis1=0
    naxis2=0
    naxis3=0
    acc=0
    exp_time=0.
    reps=0
    steps=0

    
    if n_elements(file(ii)) eq 0 then err=1 else begin
      print,'Opening '+file(ii)
      get_lun,unit
      openr,unit,file(ii),err=err
    endelse
    if err ne 0 then begin
      if n_elements(file(ii)) ne 0 then $
        message,/cont,'error opening file '+file(ii) $
      else message,/cont,'no filename specified'
      message,/cont,'using empty header'
    endif else begin

      head=bytarr(2880,/nozero)
      start=0 & count=0 & no_ti=0 & sp_1=0 & sp_2=0


      while(start eq 0)do begin
        readu,unit,head
        z=where(head eq 0)

        if(z(0) ne -1) then head(z)=32

        pos=where(strpos((head),'END') ne -1)
        if (pos(0) ne -1) then start=1

        pos=strpos((head),'DATE-OBS=')
        if (pos ne -1) then date=strtrim(head(pos+11:pos+28))

        pos=strpos((head),'STEPSIZE=')
        if (pos ne -1) then step_sx=float(strtrim(head(pos+10:pos+29))) $
        else step_sx=0.

        pos=strpos((head),'R0RADIUS=')
        if (pos ne -1) then begin
          r0=float(strtrim(head(pos+10:pos+29)))
                                ;correct r0 (from 2008 the r0 is
                                ;stored in 1/100 of arcsec)
          if r0 ge 9e4 and r0 le 1e5 then r0=r0/100.
        endif
          
        pos=strpos((head),'UT      =')
;      if (pos ne -1 and no_ti eq 0) then begin
        if pos ne -1 then begin
          if n_elements(time) eq 0 then time=strtrim(head(pos+11:pos+28)) $
          else time=[time,strtrim(head(pos+11:pos+28))]
;        no_ti=1
        endif

        pos=strpos((head),'B0ANGLE =')
        if (pos ne -1) then b0=float(strtrim(head(pos+10:pos+29)))

        pos=strpos((head),'P0ANGLE =')
        if (pos(0) ne -1) then p0=float(strtrim(head(pos+10:pos+29)))

        pos=strpos((head),'L0ANGLE =')
        if (pos(0) ne -1) then l0=float(strtrim(head(pos+10:pos+29)))

        pos=strpos((head),'SLITPOSX=')
        if (pos(0) ne -1 and sp_1 eq 0) then begin
          sp_x=float(strtrim(head(pos+10:pos+29)))
          sp_1=1
        endif
        
        if (pos(0) ne -1 and sp_1 eq 1) then begin
          sp_x1=float(strtrim(head(pos+10:pos+29)))
        endif

        pos=strpos((head),'SLITPOSY=')
        if (pos ne -1 and sp_2 eq 0) then begin
          sp_y=float(strtrim(head(pos+10:pos+29)))
          sp_2=1
        endif

        if (pos ne -1  and sp_2 eq 1) then begin
          sp_y1=float(strtrim(head(pos+10:pos+29)))
        endif

        pos=strpos((head),'SLITORIE=')
        if (pos ne -1) then s_or=float(strtrim(head(pos+10:pos+29)))

        pos=strpos((head),'NAXIS   =')
        if (pos ne -1) then naxis=fix(strtrim(head(pos+10:pos+29)))

        pos=strpos((head),'NAXIS1  =')
        if (pos ne -1) then naxis1=fix(strtrim(head(pos+10:pos+29)))

        pos=strpos((head),'NAXIS2  =')
        if (pos ne -1) then naxis2=fix(strtrim(head(pos+10:pos+29)))

        pos=strpos((head),'NAXIS3  =')
        if (pos ne -1) then naxis3=fix(strtrim(head(pos+10:pos+29)))

        pos=strpos((head),'ACCUMULA=')
        if (pos ne -1) then acc=fix(strtrim(head(pos+10:pos+29)))

        pos=strpos((head),'EXPTIME =')
        if (pos ne -1) then exp_time=float(strtrim(head(pos+10:pos+29)))

        pos=strpos((head),'REPETITI=')
        if (pos ne -1) then reps=fix(strtrim(head(pos+10:pos+29)))

        pos=strpos((head),'STEPS   =')
        if (pos ne -1) then steps=fix(strtrim(head(pos+10:pos+29)))

      endwhile
      free_lun,unit
    endelse
  endfor
  
  if n_elements(sp_x1) eq 0 then begin
    print,'No slit position found in '+file
    sp_x=0. & sp_y=0.
  endif else begin
    sp_x=(sp_x+sp_x1)/2.d0      ; average slit position
    sp_y=(sp_y+sp_y1)/2.d0
  endelse
  
  if n_elements(p0) eq 0 then begin
    print,'No P0 found in '+file
    p0=0.
  endif
  if n_elements(b0) eq 0 then begin
    print,'No B0 found in '+file
    b0=0.
  endif
  if n_elements(l0) eq 0 then begin
    print,'No L0 found in '+file
    l0=0.
  endif
  if n_elements(s_or) eq 0 then begin
    print,'No SLITORIE found in '+file
    s_or=0.
  endif
  
                                ;get r0,b0,... using SSW
  date=strmid(date(0),8,2)+'-'+ $
    (['jan','feb','mar','apr','may','jun','jul', $
      'aug','sep','oct','nov','dec'])(fix(strmid(date,5,2))-1)+'-'+ $
    strmid(date,2,2)
  hhmmss=strcompress(time(0),/remove_all)
  atime=anytim2ints(hhmmss+' '+date)
  out=get_rb0p(atime,/deg)
  nel=n_elements(out(0,*))
  r0ssw=total(out(0,*))/nel
  b0ssw=total(out(1,*))/nel
  p0ssw=total(out(2,*))/nel
  if max(abs(r0ssw-r0)) gt .01 then begin
    print,'R0 in TIP header: '+n2s(r0)+' differs from calculated R0: '+ $
      n2s(r0ssw)
    print,'Using calculated R0'
    r0=r0ssw
  endif
  if max(abs(p0ssw-p0)) gt .01 then begin
    print,'P0 in TIP header: '+n2s(p0)+' differs from calculated P0: '+ $
      n2s(p0ssw)
    print,'Using calculated P0'
    p0=p0ssw
  endif
  if max(abs(b0ssw-b0)) gt .01 then begin
    print,'B0 in TIP header: '+n2s(b0)+' differs from calculated B0: '+ $
      n2s(b0ssw)
    print,'Using calculated B0'
    b0=b0ssw
  endif
  
                                ;tip header contains central position
                                ;of slit. Before 01 May 07 this was in
                                ;VTT coordinates, 01may07 and later
                                ;contains slit in solar coordinates.
  if (atime.day)(0) lt 10348l then begin
    print,date+': Using SLITPOSX/Y in VTT coordinates' 
  endif else begin
    print,date+': Using SLITPOSX/Y in SOLAR coordinates' 
    cp0=cos(p0*!dtor)
    sp0=sin(p0*!dtor)
    slitx_vtt= sp_x*cp0 - sp_y(0)*sp0
    slity_vtt= sp_x*sp0 + sp_y(0)*cp0
    sp_x=slitx_vtt
    sp_y=slity_vtt
  endelse
  

  r=sqrt(sp_x^2+sp_y^2)


  if (sp_x ge 0. and sp_y ge 0.) then omega =360.d0-atan(sp_x/sp_y)*1.d0/!dtor
  if (sp_x ge 0. and sp_y lt 0.) then omega =180.d0-atan(sp_x/sp_y)*1.d0/!dtor
  if (sp_x lt 0. and sp_y lt 0.) then omega =180.d0-atan(sp_x/sp_y)*1.d0/!dtor
  if (sp_x lt 0. and sp_y ge 0.) then omega =360.d0-atan(sp_x/sp_y)*1.d0/!dtor
  
  semi=(r0*!dpi)/(180.d0*60.*60.)
  rho=asin(r/r0)-(semi*r/r0)
  po=(p0-omega)*!dtor
  bor=b0*!dtor

  b =asin(cos(rho)*sin(bor)+sin(rho)*cos(bor)*cos(po))
  l =asin(sin(po)*sin(rho)/cos(b))
  l=l/!dtor & b=b/!dtor

  ls='W'
  bs='N'

  if l lt 0 then ls='E'
  if b lt 0 then bs='S'
  NEWS=ls+strmid(strcompress(string(sqrt(l^2)),/remove_all),0,4) $
    +' '+bs+strmid(strcompress(string(sqrt(b^2)),/remove_all),0,4)
  print,'LonLat: ',NEWS
  
  header={date:date,stepsize:step_sx,r0:r0,time:time(0),ut:time, $
          b0:b0,p0:p0,l0:l0, $
          slitposx:sp_x,slitposy:sp_y,slitori:s_or, $
          naxis:naxis,naxis1:naxis1,naxis2:naxis2,naxis3:naxis3, $
          lon:l,lat:b,pos:news, $
          acc:acc,exp_time:exp_time,steps:steps}
                                ;use slit orientation from red-file
  if n_elements(reduce) ne 0 then begin
    if reduce.slitori ne 0 then  begin
      if reduce.slitori ne header.slitori then begin
        print,'Slit Orientation in data file header: ',header.slitori
        print,'Slit Orientation in red-file: ',reduce.slitori
        print,'------ Using Slit Orientation from red-file!! -------'
        header.slitori=reduce.slitori
      endif
    endif else reduce.slitori=header.slitori
    
                                ;use slit POSx/POSy from red-file
                                ;tip header contains central position
                                ;of slit. Before 01 May 07 this was in
                                ;VTT coordinates, 01may07 and later
                                ;contains slit in solar coordinates.
    if (atime.day)(0) lt 10348l then sys='VTT' else sys='SOLAR'
    
                                    ;check if FF is at disk center
    if flat then if sqrt(header.slitposx^2+header.slitposy^2) gt 50. then begin
      print,'Flat Field was not done at disk center.'
      print,'FF coordinates: ',header.slitposx,header.slitposy
      print,'Is this correct ? (any key to continue, press ''0'' to set FF to disk center)'
      repeat key=get_kbrd() until key ne ''
      if key eq '0' then begin
        print,'Setting FF to disk center!!!'
        header.slitposx=0.
        header.slitposy=0.
      endif
    endif

    
    if reduce.slitposx ge -9998. then begin
      if flat eq 0 then if reduce.slitposx ne header.slitposx then begin
        print,'SlitposX ('+sys+') in header: ',header.slitposx
        print,'SlitposX ('+sys+') red-file: ',reduce.slitposx
        print,'------ Using SlitposX from red-file!! -------'
        header.slitposx=reduce.slitposx
      endif
    endif
    if reduce.slitposy ge -9998. then begin
      if flat eq 0 then if reduce.slitposy ne header.slitposy then begin
        print,'SlitposY ('+sys+') in header: ',header.slitposy
        print,'SlitposY ('+sys+') red-file: ',reduce.slitposy
        print,'------ Using SlitposY from red-file!! -------'
        header.slitposy=reduce.slitposy
      endif
    endif
  endif  
end
