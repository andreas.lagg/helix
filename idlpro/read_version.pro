pro read_version,gitrevision
  
  
  gitrevision='not available'
  
  openr,unit,/get_lun,'gitrevision',error=error
  if error eq 0 then begin
    gitrevision=''
    readf,unit,gitrevision
    free_lun,unit
  endif
end
