;define some operations between compnents, select with 'Operation'
;Button in widget application
function define_operations,n=n
  
                                ;coltab feature: use coltab=7 for
                                ;vlos-coltab centered around 0
  opst={cmd:'',title:'',coltab:0}
  
  oper=replicate(opst,3)
  
  oper(0).cmd='C1/C2'           
  oper(0).title='Ratio: Comp1 / Comp2'
  
  oper(1).cmd='C1-C2'           
  oper(1).title='Difference: Comp1 - Comp2'
  oper(1).coltab=7
  
  oper(2).cmd='C1 gt C2'           
  oper(2).title='Flag: C1 > C2'
  
  
  n=n_elements(oper)
  return,oper
end
