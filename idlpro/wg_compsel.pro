pro cswg_event,event
  common cswg,cswg
  common puse,puse
  common param,param
  
  widget_control,event.id,get_uvalue=uval
  case uval of
    'cselect': begin
      cswg.selval(event.value)=event.select
    end
    'control': begin
      case event.value of
        'done': widget_control,cswg.base.id,/destroy
      endcase
    end
    else: begin
      help,/st,event
    end
  endcase
  
  
end
    
pro cs_widget
  common wgst,wgst
  common cswg,cswg
  common pikaia
  common selval,selval
  
  newst=0
  nc=pikaia_result.input.ncomp
  if n_elements(cswg) eq 0 then newst=1 $
  else if n_elements(cswg.selval) ne nc then newst=1
  if newst then begin
    subst={id:0l,val:0.,str:''}
    cswg={base:subst,select:subst,selval:intarr(nc)+1,control:subst}
    if n_elements(selval) eq nc then cswg.selval=selval
  endif
  
  cswg.base.id=widget_base(title='Select Components',/col)
  lab=widget_label(cswg.base.id,value='Display components:')
  
  cswg.select.id=cw_bgroup(cswg.base.id,uvalue='cselect',row=1,/nonexclusive, $
                         'Comp '+n2s(indgen(nc)+1),set_value=cswg.selval)
   
  cswg.control.id=cw_bgroup(cswg.base.id,['Done'], $
                            uvalue='control',row=1, $
                            button_uvalue=['done'])
   
  widget_control,cswg.base.id,/realize
  xmanager,'cswg',cswg.base.id,no_block=1
end


pro wg_compsel
  common wgst,wgst
  common cswg,cswg
  common selval,selval
  
  if n_elements(cswg) ne 0 then begin
    selval=cswg.selval
    widget_control,cswg.base.id,iconify=0,bad_id=bad_id
    if bad_id eq 0 then begin
      return
    endif else dummy=temporary(cswg)
  endif
  
  cs_widget
end
