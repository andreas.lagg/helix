;output of input-file info on one PS-page
pro print_ipt,ipt,erase=erase
  
  if max(tag_names(ipt) eq 'ASCII') eq 0 then return
  
  if keyword_set(erase) then erase
  
  
  nl=n_elements(ipt.ascii)
                                ;replace ! by !!
  all_lines=ipt.ascii
  for i=0,nl-1 do begin
     if strpos(all_lines(i),'!') ne -1 then $
       all_lines(i)=strjoin('!!'+strsplit(all_lines(i),'!',/extract))
  endfor
  all_lines=add_comma(sep='!C',all_lines)
  
  if !d.name eq 'PS' then begin
    device,/portrait
    device,xoffset=2,yoffset=2,xsize=21.-4,ysize=29.7-4
  endif
  
  fnt=!p.font
  !p.font=0
  device,set_font='Courier';,/tt_font
  dummy=max(strlen(ipt.ascii)) & maxlen=!c
  xyouts,/normal,0,-1,ipt.ascii(maxlen),charsize=1.0,noclip=0,width=wd 
  chsz=(1./wd)<0.8
  xyouts,/normal,0,1,'!C'+all_lines,charsize=chsz
  device,set_font='Helvetica';,/tt_font
  !p.font=fnt

end
