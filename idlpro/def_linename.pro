function def_linename,tagnam
  
  line_name=tagnam
  for it=0,n_elements(tagnam)-1 do case tagnam(it) of
    'STRENGTH': line_name(it)='LINE_STRENGTH'
    'WLSHIFT': line_name(it)='LINE_WLSHIFT'
    else:
  endcase
  name={ipt:line_name,idl:tagnam}
  return,name
end
