pro opwg_event,event
  common opwg,opwg
  common puse,puse
  common param,param
  
  widget_control,event.id,get_uvalue=uval
  case uval of
    'oper': begin
      opwg.oper.val=0
      opwg.oper.val(event.value)=1
    end
    'control': begin
      case event.value of
        'done': widget_control,opwg.base.id,/destroy
      endcase
    end
    else: begin
      help,/st,event
    end
  endcase
  
end
    
pro op_widget
  common wgst,wgst
  common opwg,opwg
  
  oper=define_operations(n=no)
  
  newst=0
  if n_elements(opwg) eq 0 then newst=1 $
  else if n_elements(opwg.oper) ne no then newst=1
  if newst then begin
    subst={id:0l,val:0.,str:''}
    opst={id:0l,val:bytarr(no),str:''}
    opwg={base:subst,oper:opst,control:subst}
    opwg.oper.val(0)=1
  endif
  
  opwg.base.id=widget_base(title='Select Component Operation',/col)
  lab=widget_label(opwg.base.id,value='Use operation on Components ' + $
                   '(see define_operations.pro)')
  
  opwg.oper.id=cw_bgroup(opwg.base.id,uvalue='oper',col=1, $
                         oper.cmd+'  ('+oper.title+')',/exclusive)
  widget_control,opwg.oper.id,set_value=(where(opwg.oper.val eq 1))(0)
   
  opwg.control.id=cw_bgroup(opwg.base.id,['Done'], $
                            uvalue='control',row=1, $
                            button_uvalue=['done'])
   
  widget_control,opwg.base.id,/realize
  xmanager,'opwg',opwg.base.id,no_block=1
end


pro wg_oper
  common wgst,wgst
  common opwg,opwg
  
  if n_elements(opwg) ne 0 then begin
    widget_control,opwg.base.id,iconify=0,bad_id=bad_id
    if bad_id eq 0 then begin
      return
    endif else dummy=temporary(opwg)
  endif
  
  op_widget
  
end
