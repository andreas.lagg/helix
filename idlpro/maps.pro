pro oplot_azi,az,inc=inc,color=clr,xcoord=xcoord,ycoord=ycoord,pos=pos
  common color,color
  
  clr_old=color
  color=1
  userlct
  
  if n_elements(clr) eq 0 then clr=2
  
                                ;reduce size
  nx=n_elements(xcoord)
  ny=n_elements(ycoord)
  
  nvx=25.
  nvy=nvx/nx*ny
  
  vaz=congrid(az,nvx,nvy)
  vx=congrid(xcoord,nvx)
  vy=congrid(ycoord,nvy)
  
  vxarr=cos(vaz*!dtor) * sin(inc*!dtor)
  vyarr=sin(vaz*!dtor) * sin(inc*!dtor)
  
  npos=(convert_coord(/device,/to_normal, $
                      [[pos(0),pos(1)],[pos(2),pos(3)]]))([0,1,3,4])
  
  
  velovect_lagg,vxarr,vyarr,vx,vy,color=clr,length=1.5,/noerase,xst=5,yst=5, $
    position=npos,hsize=0
  
  color=clr_old
  userlct
end

pro draw_box,box1=box1,line1=line1
  common color,color
  
  l1=1 & b1=1
  if keyword_set(box1) eq 1 then begin
    l1=0
  endif
  if keyword_set(line1) eq 1 then begin
    b1=0
  endif
    
  color=1
  userlct

                                ;box current sheet
  if b1 then begin
    coordx=[21,28,39,32,21]
    coordy=[24,16,25,33,24]
    
    vec=transpose([[coordx],[coordy]])
    p0=[coordx(0),coordy(0)]
    p1=[coordx(1),coordy(1)]
    
    userlct,/nologo
    plots,vec,color=7,/data,thick=2
    dx=p1(0)-p0(0)
    dy=p1(1)-p0(1)
    arrow,color=1,/data,p0(0),p0(1),p0(0)+dx*0.5,p0(1)+dy*0.5,thick=3
    xyouts,/data,coordx(3)+1,coordy(3),'!C Region 2',color=7,charthick=3
  endif
  
                                ;cut along loop
  if l1 eq 1 then begin
    coordx=[12.771,30.474]
    coordy=[6.947,20.209]
    vec=transpose([[coordx],[coordy]])
  
    tvlct,/get,r,g,b & r(14)=255 & g(14)=255 & b(14)=40 & tvlct,r,g,b
    plots,vec,color=14,/data,thick=3
    xyouts,/data,total(coordx)/2.,total(coordy)/2.,'!CCut 1',color=14,charthick=3
endif
  
end


function med_filter,csav

  mval=3                        ;value for median filter
  sz=size(csav)
  csav=median(csav,mval)        ;median filter over whole image
  
                                ;median filter over boundaries
  csav(0,*)=median((csav(0,*))(*),mval)
  csav(*,0)=median(csav(*,0),mval)
  csav(sz(1)-1,*)=median((csav(sz(1)-1,*))(*),mval)
  csav(*,sz(2)-1)=median(csav(*,sz(2)-1),mval)
  
  return,csav
end

pro test_azi
  common savs,savs
  
  savs=['he_maps_unc_3x3_161-181_q-0.0002.sav','he_maps_unc_3x3_161-181_q-0.0003.sav','he_maps_unc_3x3_161-181_q-0.002.sav','he_maps_unc_3x3_161-181_q+0.0002.sav','he_maps_unc_3x3_161-181_u-0.0002.sav','he_maps_unc_3x3_161-181_q-0.0005.sav','he_maps_unc_3x3_161-181_equal_weight.sav','he_maps_unc_3x3_161-181_amoeba1.sav','he_maps_unc_3x3_amoeba2.sav','he_maps_unc_amoeba1.sav','he_maps_unc_amoeba2.sav']
;  savs=['he_maps_unc_3x3_161-181_amoeba1.sav','he_maps_unc_3x3_amoeba2.sav','he_maps_unc_1x1_161-181_amoeba1.sav','he_maps_unc_1x1_161-181_amoeba2.sav']
  savs=['he_maps_unc_amoeba1.sav','he_maps_unc_amoeba2.sav','he_maps_unc_3x3_q-0.00050_amoeba1.sav','he_maps_unc_3x3_q-0.00500_amoeba1.sav']
;  savs=reverse(savs)
  savs='he_maps_unc_amoeba1.sav'
;  savs='/home/lagg/work/polarimeter/joachimidl/he_maps_unc_amoeba1.sav'
  
  userlct & erase
  for i=0,n_elements(savs)-1 do begin
    restore,savs(i)
    da=dop_azihe
;    da=da+90
;    da=(da+180) mod 180
    
    
    h=histogram(da,bin=2,min=0,max=180)
    xc=findgen(180/2)*2
    if i eq 0 then plot,xc,h,thick=2,/xst,xrange=[-90,90] $
    else oplot,xc,h,thick=2,color=i
    xyouts,color=i,0,360-i*!d.y_ch_size*2,savs(i),/device,charsize=2,charthick=2
;  tv,congrid((da ge 150 and da le 152),300,150)
  endfor
  
end

pro make_cont,img,msk,xcoord,ycoord
  common color,color
  
  ccol=[2,3,3,1]
;  ccol=!p.color
  ocol=color
  color=1 & userlct
  
  imgplot=img
  och=msk.charsize
  msk.charsize=och*.6
  msk_plot,imgplot,msk,$
    /exact,xcoord=xcoord,ycoord=ycoord, $
    levels=[1200,1600,2000,2400],c_labels=[1,0,0,1], $
;    levels=[1600,1800],c_labels=[0,0], $
    /noimage,novalid=-1,$
    color=ccol,$ ;color=[1,10,3], $
    label_range=[0.,2000],smooth=2
  msk.charsize=och
  color=ocol & userlct
  
end

;based on Potsdam idl-Program
pro maps,ps=ps,typ=typ,gif=gif,nobox=nobox,show=show,left=left,right=right,box1=box1,line1=line1,azivec=azivec,black_white=black_white,vec_col=vec_col,sort=srt
  common greek_letters
  common imgst,imgst
  common sig,sig_si,sig_he
  common prof,profile
  common img,img_si,img_he
  common color,color
  common savs,savs
  common corri,corri
  
  corri=0
  
   idl53,prog_dir='~/epd_dps/prog/'
;   test_azi
   
;   if n_elements(savs) ne 0 then he_maps_sav=savs else begin
    bin=1
;    if bin gt 1 then $
;      add='_'+n2s(bin)+'x'+n2s(bin) ; else add=''
;      savs='he_maps_unc'+add+'.sav'
;  endelse
;savs='he_maps_unc_1x1_161-181_amoeba2.sav'      
;savs=['he_maps_unc_3x3_161-181_amoeba1.sav','he_maps_unc_3x3_amoeba2.sav','he_maps_unc_1x1_161-181_amoeba1.sav','he_maps_unc_1x1_161-181_amoeba2.sav']
;savs=['he_maps_unc_amoeba1.sav','he_maps_unc_amoeba2.sav']
    
    add=['_amoeba1']
;    savs='he_maps_unc'+add+'.sav'
    savs='he_maps_unc_amoeba1.sav'
        
   addn='.new'
;    addn='.old'
     addn='.old'
    addg='.geff'
    
;  savs='/home/lagg/work/polarimeter/joachimidl/he_maps_unc_old_amoeba1.sav'
     savs='/home/lagg/work/polarimeter/joachimidl/he_maps_unc'+addn+addg+add+'.sav'
       savs='/home/lagg/work/polarimeter/joachimidl/he_maps_unc_amoeba1.best.sav'
     
      if n_elements(left) eq 0 then left=1
      if n_elements(right) eq 0 then right=1
        
  !p.multi=0
  !p.position=0
  case typ of
    1: begin

;      show=[1,1,1,1,1,1,1,1,1,1,1]
     if n_elements(show) eq 0 then show=[1,0,0,0,1,1,1,1,0,0,0]
;     show=[0,0,0,0,0,0,0,1,0,0,0]
     pre=''
     if left then pre='si_'
     if right then pre=pre+'he_'
     plname=pre+add_comma(n2s(show),sep='')
      if n_elements(savs) gt 1 then plname=plname+'_'+savs
      nshow=n_elements(where(show))

      if left+right eq 2 then wd=18 else wd=10.
      hg=3+nshow*3.5
;      tleft='Silicon 10827'
;      tright='Helium 10829 / 10830'
      tleft='Photosphere'
      tright='Upper Chromosphere'
    end
    2: begin
      plname='v_slow_fast'
     if n_elements(show) eq 0 then  show=[0,0,0,0,0,1]
      wd=18 & hg=5.7
      nshow=n_elements(where(show))
      tleft='HeI 10830 Velocity'
      tright='HeI 10830 Velocity'
    end
    3:begin
      plname='b_grad'
      wd=12 & hg=7
    end
    4: begin
      plname='profiles'
      wd=18 & hg=24
    end
    else: message,'undefined plot type.'
  endcase
  if n_elements(azivec) ne n_elements(show) then $
    azivec=bytarr(n_elements(show))
  if n_elements(srt) ne n_elements(show) then srt=indgen(n_elements(show))
  
  !p.charsize=1.
  if left+right eq 1 then !p.charsize=0.7
  
  for isav=0,n_elements(savs)-1 do begin
  
  if keyword_set(ps) then begin
    landscape=0
    if n_elements(azivec) gt 0 then $
    if total(azivec) gt 0 then plname(isav)=plname(isav)+'.vec'
    psout=ps_widget(default=[landscape,0,0,0,8,0,0],size=[wd,hg],psname=psn, $
                    file=plname(isav)+'.eps',/no_x)
    if psout(1) eq 1 then return
    !p.font=0
  endif else begin
    scrsz=get_screen_size()
    fct=0.7
    window,xsize=scrsz(1)*fct,ysize=scrsz(1)*fct/wd*hg
  endelse
  
  userlct & greek & erase
  
  
;==================================== READ DATA
  
  print,'restoring '+savs(isav)
  restore,savs(isav)
  
  vars=['DOP_A0HE','DOP_A0HE2','DOP_AZIHE','DOP_BHE','DOP_BHE2', $
        'DOP_BHEMAX','DOP_CHISQHE','DOP_HEI','DOP_INCHE','DOP_VHE', $
        'DOP_VHE2','DOP_VHEMAX']
  for i=0,n_elements(vars)-1 do dummy=execute(vars(i)+'_bin='+vars(i))
  
  
;  restore,'he_maps_unc.sav'
  
  restore,'/home/lagg/work/polarimeter/joachimidl/13may01.014_selprof.sav'
  if n_elements(imgst) eq 0 then $
     restore,'/home/lagg/work/polarimeter/joachimidl/13may01.014_img.ltau-1.25to-0.00.sav'
  
  if n_elements(sig_si) eq 0 then begin
    restore,'/home/lagg/work/polarimeter/joachimidl/13may01.014_Si10827_sig.sav'
    sig_si=sig
  endif
  
  if n_elements(sig_he) eq 0 then begin
    restore,'/home/lagg/work/polarimeter/joachimidl/13may01.014_HeI10830_sig.sav'
    sig_he=sig
  endif
  
  if n_elements(img_si) eq 0 then begin
    restore,'/home/lagg/work/polarimeter/joachimidl/13may01.014_Si10827_img.sav'
    img_si=obs
  endif
  
  if n_elements(img_he) eq 0 then begin
    restore,'/home/lagg/work/polarimeter/joachimidl/13may01.014_HeI10830_img.sav'
    img_he=obs
  endif
  
;  restore,'he_fits'+add+'.sav' 
;  heprofiles_bin=temporary(heprofiles)
;restore,'/home/lagg/work/polarimeter/joachimidl/he_fits_old.sav'
    restore,'/home/lagg/work/polarimeter/joachimidl/he_fits'+addn+addg+'.sav'
  
  if n_elements(profile) eq 0 then $
    restore,'/home/lagg/work/polarimeter/joachimidl/13may01.014.profiles'+addn+'.sav'
;     restore,'./13may01.014.profiles.sav'
                                
  si_idx=[0,110]                ;si profile: wl-idx   0-110
  he_idx=[120,200]              ;he profile: wl-idx 120-200
   
                                ;calculate (Ic-I)/Ic
  iic_si=1-total(profile.i(si_idx(0):si_idx(1),*,*),1)/(si_idx(1)-si_idx(0)+1)
  iic_he=1-total(profile.i(he_idx(0):he_idx(1),*,*),1)/(he_idx(1)-he_idx(0)+1)
                                ;calculate eq. width
  eqw_si=total(1-profile.i(si_idx(0):si_idx(1),*,*),1)* $
    (profile.wl(si_idx(1))-profile.wl(si_idx(0)))
  eqw_he=total(1-profile.i(he_idx(0):he_idx(1),*,*),1)* $
    (profile.wl(he_idx(1))-profile.wl(he_idx(0)))
  
  sz=size(profile.ic)
  mnarr_he=fltarr(sz(1),sz(2))
  mnarr_si=fltarr(sz(1),sz(2))
  for ix=0,sz(1)-1 do for iy=0,sz(2)-1 do begin
    mnarr_he(ix,iy)=min(profile.i(he_idx(0):he_idx(1),ix,iy))
    mnarr_si(ix,iy)=min(profile.i(si_idx(0):si_idx(1),ix,iy))
  endfor
;  eqw_he=1-mnarr_he
;  eqw_si=1-mnarr_si
;   tvscl,congrid(eqw_he*profile.ic,sz(1)*2,sz(2)*2)
;   tvscl,congrid(eqw_he,sz(1)*2,sz(2)*2),sz(1)*2,sz(2)*2
;   tvscl,congrid(total(profile.i(he_idx(0):he_idx(1),*,*),1),sz(1)*2,sz(2)*2),0,sz(2)*2
;   tvscl,congrid(img_he.i,sz(1)*2,sz(2)*2),sz(1)*2,0
  
  eqw_si=profile.ic/max(profile.ic)
  
  mnew_he=min(eqw_he) & mnew_si=min(eqw_si)
  mxew_he=max(eqw_he) & mxew_si=max(eqw_si)
  
  cut_he=0.1
  cut_si=0.0
  mnew_he=mnew_he+cut_he*(mxew_he-mnew_he)
  mxew_he=mxew_he-cut_he*(mxew_he-mnew_he)
  mnew_si=mnew_si+cut_si*(mxew_si-mnew_si)
  mxew_si=mxew_si-cut_si*(mxew_si-mnew_si)
   
  radius_Mm=700.
  radsun=asin(radius_Mm*1e3/1.48e8)/!dtor*3600
  xfact=(0.42d)/radsun*radius_Mm/cos(10.*!dtor)
  yfact=(0.37d)/radsun*radius_Mm/cos(30.*!dtor)
  
;  fm=[.08,.82]
  fm=[.12,.80]
  yrg=[0,96] 
  xrg=[0,150]
  ycoord=findgen(yrg(1)-yrg(0)+1)+yrg(0)
  xcoord=findgen(xrg(1)-xrg(0)+1)+xrg(0)
   xrg=xrg*xfact
   yrg=yrg*yfact
   xcoord=xcoord*xfact
   ycoord=ycoord*yfact

  img_cont=imgst(11).img
;================================= PLOT 1  
  if typ eq 1 or typ eq 2 then begin
    zrg_left=[[700,1300],[0,0.1],[0,0.1],[0,0.4], $
              [ -2, 2],[0,2000],[0,180],[-90,90],[0,1],[-4,4],[-4,4], $
              [13000,25000],[mnew_si,mxew_si]]
    zrg_right=[[900,1500],[0,0.05],[0,0.05],[0,0.3], $
               [-4,20],[0,2000],[0,180],[-90,90],[0,1],[-10,30],[-4,4], $
               [13000,21000],[mnew_he,mxew_he]]
    zrg_left(*,7)= zrg_right(*,7)
    zf=['(f10.1)','(f10.2)','(f10.2)','(f10.2)', $
        '(f10.1)','(i5)','(i5)','(i5)','(f10.2)','(f10.1)', $
        '(f10.1)','(f10.1)','(f15.2)']
    cyclic=      [0,0,0,0,0,0,0,1,0,0,0,0,0]
;    cyclic=      [0,0,0,0,0,0,0,0,0,0]
    bw=          [1,1,1,1,0,0,0,0,0,0,0,0,1]
    coltab=      [0,0,0,0,7,0,0,0,0,7,7,0,0]
    if keyword_set(black_white) then bw(*)=1
    
    subytit_left=['Intensity','Q','U','V','Velocity','Field Strength', $
                  'Inclination '+f_gamma, $
                  'Azimuth!CAngle '+f_phi, $
                  'Magnetic Filling!CFactor '+f_alpha,'Slow Comp.', $
                  'Velocity','Intensity (Si)','Continuum Intensity']
    subytit_right=subytit_left
;    subytit_right(4)='Combined!CVelocity'
    subytit_right(10)='Velocity!CSlow Comp.'
    subytit_right(9)='Fast Comp.'
    subytit_right(11)='Intensity (He)'
    subytit_right(12)='Equiv. Width (He)'
    subztit_left=['','','','','km/s','Gauss','degrees','degrees',' ','km/s','km/s','','']
    subztit_right=['','','','','km/s','Gauss','degrees','degrees',' ','km/s','km/s','','']
    
    if right+left eq 2 then overlap=0.01 else overlap=0
    if right+left eq 1 then xpos=0. else xpos=0.5
    
    srt1=sort(srt)
    index=indgen(n_elements(zf))
    index=index(srt1)
    zrg_left=zrg_left(*,srt1)
    zrg_right=zrg_right(*,srt1)
    zf=zf(srt1)
;    cyclic=cyclic(srt1)
;    bw=bw(srt1)
;    azivec=azivec(srt1)
;    subytit_left=subytit_left(srt1)
;    subytit_right=subytit_right(srt1)
    subztit_left=subztit_left(srt1)
    subztit_right=subztit_right(srt1)
    
    show=show(srt1)
    ishow=where(show)
    nshow=n_elements(ishow)

    if left eq 1 then begin
    mask_left=mask([1,nshow],title=tleft, $
                 xformat='(i4)',yformat='(i4)',zformat=zf(ishow), $
                 zrange=zrg_left(*,ishow),xrange=xrg,yrange=yrg, $
                 fix_margins=fm, $
                 pos=[0.0,0,1-xpos+overlap,1],headtitle=' ', $
                 small_label=0,short_label=0,$
;               dytitle=subytit_left(ishow), $
    zlog=0,log_min=1e-3, $
      subtitle='',/isotropic, $
      xtitle='x [Mm]',ytitle='y [Mm]', $
      dztitle=subztit_left(ishow),nolabel=0)
    endif
    
    if right eq 1 then begin
    mask_right=mask([1,nshow],title=tright, $
                 xformat='(i4)',yformat='(i4)',zformat=zf(ishow), $
                 zrange=zrg_right(*,ishow),xrange=xrg,yrange=yrg, $
                 fix_margins=fm, $
                 pos=[xpos-overlap,0,1,1],headtitle=' ', $
                 small_label=0,short_label=0,$
;               dytitle=subytit_right(ishow), $
    zlog=0,log_min=1e-3, $
      subtitle='',/isotropic, $
      xtitle='x [Mm]',ytitle=(['','y [Mm]'])(left eq 0), $
      dztitle=subztit_right(ishow),nolabel=0)
    endif
    
;    g0=40 & g1=140
    g0=0 & g1=180
    xyouts,0,0,'!3'
    
    if right eq 1 then begin
;  ff_right=abs(dop_a0he)/max(median(abs(dop_a0he),11))
;  ff_right=reform(sqrt(sig_he.q^2+sig_he.v^2+sig_he.u^2)/(sig_he.i/sig_he.cont))
    ff_right=abs(dop_a0he)/(reform(sig_he.i)/sig_he.cont)
  
  ff_right=ff_right/max(median(ff_right,5))
    
;  ff_right=abs(dop_a0he)

    
    
                                ;calculate equivalent width window
                                ;(=total area under Gauss fits =
                                ;wegabsorbierte Fl�che)
    img=[[[img_he.i]], $
         [[img_he.q]], $
         [[img_he.u]], $
         [[img_he.v]], $
         [[congrid(dop_vhemax,(size(img_he.i))(1),(size(img_he.i))(2))]], $
         [[congrid(dop_bhe,(size(img_he.i))(1),(size(img_he.i))(2))]], $
         [[congrid(dop_inche,(size(img_he.i))(1),(size(img_he.i))(2))]], $
;         [[dop_azihe*((dop_bhe gt 40))]], $
;         [[dop_azihe]], $
         [[congrid(dop_azihe,(size(img_he.i))(1),(size(img_he.i))(2))]], $
         [[congrid(ff_right,(size(img_he.i))(1),(size(img_he.i))(2))]], $
           [[congrid(dop_vhe2,(size(img_he.i))(1),(size(img_he.i))(2))]], $
           [[congrid(dop_vhe,(size(img_he.i))(1),(size(img_he.i))(2))]],$
           [[congrid(dop_hei,(size(img_he.i))(1),(size(img_he.i))(2))]], $
         [[congrid(eqw_he,(size(img_he.i))(1),(size(img_he.i))(2))]] $  
         ]
         
         
    
;     pos=mask_left.plot(0,0).position
;     dx=pos(2)-pos(0) & dy=pos(3)-pos(1)
;     lbadd=[-dx*.01,dy*.1]
    n=0
    az=corr_azi(img(*,*,3+4))

    gm=img(*,*,2+4)
    for k=0,n_elements(show)-1 do begin
      i=index(k)
      if show(k) then begin
        color=bw(i) eq 0

        gclt=coltab(i) & if gclt eq 0 then dummy=temporary(gclt) $
        else center=256.*(0. - mask_right.plot(0,n).zrange(0)) / $
          (mask_right.plot(0,n).zrange(1)-mask_right.plot(0,n).zrange(0))
        userlct,cyclic=cyclic(i),reverse=color eq 0,glltab=gclt, $
          center=center,neutral=coltab(i) eq 7
        color=1
        
        img_plot=img(*,*,i)
        nvarr=byte(img*0)
        
        img_plot=med_filter(img_plot)
        
        nv=-1
        case i of
          7: begin
            img_plot=az
            print,min(img_plot),max(img_plot)
          end
          9: begin
            nvarr=nvarr or (finite(img_plot) ne 1)
            img_plot(*)=median(img_plot,3)
          end
          else:
        endcase
;      nvarr=nvarr or (az lt 60 or az gt 120)
;      nvarr=nvarr or (gm lt g0 or gm gt g1)
        if n_elements(nvarr) gt 0 then nv=where(nvarr) else nv=-1
        msk_plot,0,mask_right.label(n),/nocontour
        msk_plot,img_plot,mask_right.plot(0,n),xcoord=xcoord,ycoord=ycoord, $
          novalid=nv,ex_ps_quality=0.5,exact=1,/nocontour,nv_color=15
        
        if azivec(i) then begin ;overplot arrows for azimuthal field
          oplot_azi,az,inc=gm,xcoord=xcoord,ycoord=ycoord, $
            pos=mask_right.plot(0,n).position,color=vec_col
        endif
        
        make_cont,img_cont,mask_right.plot(0,n),xcoord,ycoord
        
        pos=mask_right.plot(0,n).position
        dx=pos(2)-pos(0) & dy=pos(3)-pos(1)
        lbadd=[-dx*.01,dy*.1]
        lbpos=[pos(0)-lbadd(0),pos(1),pos(2),pos(3)-lbadd(1)]
        label_box,position=lbpos,name=subytit_right(i),charsize=.8*!p.charsize, $
          /new,/blank,/frame,/only_text,color=!p.color
        
        if n eq 0 then if keyword_set(nobox) ne 1 then $
          draw_box,box1=box1,line1=line1
        n=n+1
      endif
    endfor 
  endif
    
    if left eq 1 then begin
    si_use=[-1,-2,-3,-4,14,11,12,13,15,-1,14,1,-5]
    n=0
    nv=-1
    az=corr_azi(imgst(13).img)
    gm=imgst(12).img
    for k=0,n_elements(show)-1 do begin
      i=index(k)
      if show(k) then begin
      nvarr=byte(az*0)
      color=bw(i) eq 0
      gclt=coltab(i) & if gclt eq 0 then dummy=temporary(gclt) $
      else center=256.*(0. - mask_left.plot(0,n).zrange(0)) / $
        (mask_left.plot(0,n).zrange(1)-mask_left.plot(0,n).zrange(0))
      userlct,cyclic=cyclic(i),reverse=color eq 0,glltab=gclt,center=center;,neutral=coltab(i) eq 7  
      color=1

;      if i lt n_elements(si_use)-1 then begin ;silicon parameters from inv
      if si_use(i) gt 0 then img_plot=imgst(si_use(i)).img $
      else case si_use(i) of
        -1: img_plot=img_si.i
        -2: img_plot=img_si.q
        -3: img_plot=img_si.u
        -4: img_plot=img_si.v
        -5: img_plot=congrid(eqw_si,(size(img_he.i))(1),(size(img_he.i))(2))
      endcase
      case si_use(i) of
        13: begin
          img_plot=az
;            nvarr=nvarr or (gm lt 55 or gm gt 125)
        end
        else:
      endcase
      case i of                 ;joachims values
        9: begin
          img_plot=congrid(dop_vhe,(size(img_he.i))(1),(size(img_he.i))(2))
          img_plot(*)=median(img_plot,3)
        end
        else:              ;message,'undefined variable for left plot'
      endcase
      img_plot=med_filter(img_plot)
;      nvarr=nvarr or (az lt 60 or az gt 120)
      nvarr=nvarr or (gm lt g0 or gm gt g1)
      if n_elements(nvarr) gt 0 then nv=where(nvarr) else nv=-1
      msk_plot,0,mask_left.label(n),/nocontour

      msk_plot,img_plot,mask_left.plot(0,n),xcoord=xcoord,ycoord=ycoord, $
        novalid=nv,ex_ps_quality=0.5,exact=1,/nocontour            
      
      if azivec(i) then begin ;overplot arrows for azimuthal field
        oplot_azi,az,inc=gm,xcoord=xcoord,ycoord=ycoord, $
          pos=mask_left.plot(0,n).position,color=vec_col
      endif
      
      
      make_cont,img_cont,mask_left.plot(0,n),xcoord,ycoord
      
      pos=mask_left.plot(0,n).position
      dx=pos(2)-pos(0) & dy=pos(3)-pos(1)
      lbadd=[-dx*.01,dy*.1]
      lbpos=[pos(0)-lbadd(0),pos(1),pos(2),pos(3)-lbadd(1)]
      label_box,position=lbpos,name=subytit_left(i),charsize=.8*!p.charsize, $
        /new,/blank,/frame,/only_text,color=!p.color
      
;       if n eq 0 then if keyword_set(nobox) ne 1 then $
;         draw_box,box1=box1,line1=line1
      
      n=n+1
    endif
    endfor
    endif 
  endif
        color=1
        userlct,cyclic=0,reverse=0

;==================================== Plot 3
  if typ eq 3 then begin
    msk=mask([1,1],title='Magn. Field Gradient', $
                    xformat='(i4)',yformat='(i4)',zformat='(f10.1)', $
                    zrange=[0,0.6],xrange=xrg,yrange=yrg, $
                    fix_margins=fm, $
                    small_label=0,short_label=0,$
                    zlog=0,log_min=1e-3, $
                    headtitle='(difference formation height SiI / HeI)', $
                    /isotropic, $
                    xtitle='x [Mm]',ytitle='y [Mm]', $
                    ztitle='G / km',nolabel=0)
    med_he=dop_bhe
    med_he(*)=median(med_he,4)
    
    med_si=imgst(11).img(0:149,0:95)
    
    med_si(*)=median(med_si,4)
    
    img_plot=(med_si - med_he)/1750
    
    
    okay=(abs(dop_inche-imgst(12).img(0:149,0:95)) lt 30)
    not_idx=where(okay ne 1)

    msk_plot,0,msk.label(0),/nocontour
    
    msk_plot,img_plot,msk.plot(0,0),xcoord=xcoord,ycoord=ycoord, $
      novalid=not_idx,ex_ps_quality=0.5,exact=1,/nocontour            
    make_cont,img_cont,mSK.plot(0,0),xcoord,ycoord
   
  endif
  
;==================================== Plot 3
  if typ eq 4 then begin
    
    physical_constants
    !p.multi=[0,2,1]
    
    np=n_elements(savtgz)
    tot_wl=profile(0).wl+profile(0).wlref
    si_wl=savtgz(0).obs.wl(0:savtgz(0).obs.nel) + 10827.140d
    he_wl=HEPROFILES(0).HEWL(0:95) + 10830.380d
    iv=[0,95]
    ii=[96,191]
    
    xystr=[' ','x'+n2s(savtgz.x)+' y'+n2s(savtgz.y),' ']
    
    title=['I-Profile','V-Profile']
    posy=[0.06,0.96]
    posx=[0.14,0.5]
    add=[0,.48]
    for in=0,1 do begin
      !p.position=[posx(0)+add(in),posy(0),posx(1)+add(in),posy(1)]
      plot,[min(tot_wl),max(tot_wl)],[-1,np-.2],/nodata,xst=1,yst=1, $
        xtickformat='(i15)',yminor=1,ytickname=xystr,yticks=np+1, $
        xtitle='Wavelength ['+f_angstrom+']',ytitle=(['Pixel',''])(in), $
        title=title(in)
      
    scal_i_si=1.25
    scal_i_he=2.5
    scal_v_si=2
    scal_v_he=16
    
    rg_si=[13,109]
    rg_he=[141,141+96]
    
    xyouts,10827.14,np-.2,/data,alignment=0.5,'!C!CSiI 10827 '+f_angstrom, $
      charsize=!p.charsize*1.
    xyouts,10830.38,np-.2,/data,alignment=0.5,'!C!CHeI 10830 '+f_angstrom, $
      charsize=!p.charsize*1.
    
    os=[.55,.65,.8]
    plots,[10825.8,10826.8],[0,0]+np-os(0),color=!p.color
    xyouts,10827.1,np-os(0)-.03,color=!p.color,'observed', $
      charsize=0.8*!p.charsize
    plots,[10825.8,10826.8],[0,0]+np-os(1),color=1
    xyouts,10827.1,np-os(1)-.03,color=!p.color,'inversion', $
      charsize=0.8*!p.charsize
    
    plots,[10829.1,10830.1],[0,0]+np-os(1),color=2
    xyouts,10830.4,np-os(1)-.03,color=2,'multi-Gauss', $
      charsize=0.8*!p.charsize
    
    for i=0,np-1 do begin
      x=savtgz(i).x & y=savtgz(i).y
      
      heidx=where(heprofiles.xstep eq x and heprofiles.ystep eq y)
      if in eq 0 then begin
        siprof=(profile.i)(rg_si(0):rg_si(1),x,y)
        sifit=savtgz(i).fit.i
        heprof=(profile.i)(rg_he(0):rg_he(1),x,y)
        hefit=(heprofiles(heidx).heprof)(ii(0):ii(1))
        scal_si=scal_i_si
        scal_he=scal_i_he
        off_he=max(heprof)
        off_si=max(siprof)
        he_add=1
        aradd=0
      endif else begin
        siprof=(profile.v)(rg_si(0):rg_si(1),x,y)
        sifit=savtgz(i).fit.v
        heprof=(profile.v)(rg_he(0):rg_he(1),x,y)
        hefit=(heprofiles(heidx).heprof)(iv(0):iv(1))        
        scal_si=scal_v_si
        scal_he=scal_v_he
        off_he=0
        off_si=0
        he_add=0
        aradd=-.1
      endelse
        xyouts,10830.7,np-os(2)-.03,color=!p.color,'x '+ $
          n2s(float(scal_he)/scal_si,format='(f10.1)'), $
          charsize=1.0*!p.charsize
      
      oplot,tot_wl(rg_si(0):rg_si(1)),(siprof-off_si)*scal_si+i,thick=2
      
      oplot,tot_wl(rg_si(0):rg_si(1)),(sifit-off_si)*scal_si+i,color=1,thick=2
      
      oplot,tot_wl(rg_he(0):rg_he(1)),(heprof-off_he)*scal_he+i,thick=2
      
      oplot,tot_wl(rg_he(0):rg_he(1)),(hefit-off_he+he_add)*scal_he+i,color=2,thick=2
      
      for ia=0,1 do begin
        wl_1=heprofiles(heidx).fitpar(1+ia*4)*10830.380d/(!c_light/1e3) + $
          10830.380d
        dummy=min(abs(wl_1 - tot_wl(rg_he(0):rg_he(1)))) & wlidx=!c
        y_1=(hefit(wlidx)-off_he+he_add)*scal_he+i
        if ia eq 0 then arpos=[[wl_1,y_1-.03],[wl_1-.35,y_1-.25]] $
        else  arpos=[[wl_1,y_1-.03],[wl_1+.35,y_1-.25]] 
        arpos(1,*)=arpos(1,*)+aradd
;      pos_1=convert_coord(/data,/to_device,arpos)
        cl=([3,13])(ia)
        arrow,arpos(2),arpos(3),arpos(0),arpos(1),/data,color=cl
        addstr=n2s(heprofiles(heidx).fitpar(1+ia*4),format='(f10.1)')+ $
          ' km/s!C'+n2s(heprofiles(heidx).fitpar(3+ia*4),format='(i10)')+' G'
      xyouts,alignment=0.5,/data,arpos(2),arpos(3),(['!C','!C'])(ia)+addstr, $
        charsize=!p.charsize*0.7,color=cl
    endfor
      
      
    endfor
    endfor
  endif

;====================================
  if typ ne 4 then begin
  suncentervec=[0,1.]
  rotangle=(180-206.8)*!dtor
  sr=sin(rotangle) & cr=cos(rotangle)
  ssvec=[[cr,sr],[-sr,cr]] ## suncentervec
;  pos0=convert_coord([0.85,0.1],/normal,/to_device)
  xps=0.46
  if left+right eq 1 then xps=0.9
  pos0=convert_coord([xps,0.02],/normal,/to_device)
  if n_elements(mask_left) ne 0 then $
    pos0(1)=0.35*min(mask_left.plot.position(1)) $
  else if n_elements(mask_right) ne 0 then $
    pos0(1)=0.35*min(mask_right.plot.position(1))
  len=pos0(1)*2
;  len=!d.y_size*.05
  pos1=pos0+len*ssvec
  arrow,pos0(0),pos0(1),pos1(0),pos1(1),color=1,thick=2,/device
  xyouts,/device,color=1,pos0(0),pos0(1),alignment=0.5,'!C to sun center'
; xyouts,0.05,0.05,'60� < '+f_gamma+' < 120�',/normal
  
  if n_elements(azivec) ne 0 then $
    if total(azivec) gt 0 then begin
;    xyouts,/device,.05,pos0(1),color=2,'!Clines: azimuth of B'
  endif
  endif
  
  if !d.name eq 'PS' then begin
    xyouts,0,0,/normal,'!C!C'+psn
    device,/close
    print,'Created PS-file: '+psn
    if psout(3) eq 1 then  spawn_gv,psn
  endif
  
  endfor
  !p.font=-1 & set_plot,'X'
end


pro tenerife
  
  ps=1
;   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,0,1,0,0,0] ;azimuth
;   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,1,0,0,0,0] ;inclination
;   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,1,0,0,0,0,0] ;Magn. Field
;   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,1,0,0,0,0,0,0] ;Vel + Comb Vel.
;   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,0,0,0,0,1] ;Vel + Comb Vel.
  
;  maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,1,0,0,0,0],left=0 ;azi he
;  maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,1,0,0,0,0],right=0 ;azi si
;   maps,typ=1,/box1,left=0,ps=ps,show=[0,0,0,0,0,0,1,0,0,0,0] ;inclination
;   maps,typ=1,/nobox,left=0,ps=ps,show=[0,0,0,0,0,0,0,0,0,0,0,1] ;A0He
   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,1,0,0,0,0,0],/azivec,/black_white;inc + vec
end

pro fb ;fachbeirat
  
  ps=1
  av=[ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
  
;   maps,typ=1,ps=ps,vec_col=0,azivec=av, $
;     show=  [ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],/nobox
;   maps,typ=1,ps=ps,vec_col=0,azivec=av*0, $
;     show=  [ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],/nobox
;   maps,typ=1,ps=ps,vec_col=0,azivec=av, $
;     show=  [ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],/nobox
;  maps,typ=1,ps=ps,vec_col=0,azivec=av, $
;    show=  [ 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],/nobox
  maps,typ=1,ps=ps,vec_col=0,azivec=av, $
    show=  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],/nobox
  
;   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,0,1,0,0,0] ;azimuth
;   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,1,0,0,0,0] ;inclination
;   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,1,0,0,0,0,0] ;Magn. Field
;   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,1,0,0,0,0,0,0] ;Vel + Comb Vel.
;   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,0,0,0,0,1] ;Vel + Comb Vel.
  
;  maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,1,0,0,0,0],left=0 ;azi he
;  maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,1,0,0,0,0],right=0 ;azi si
;   maps,typ=1,/box1,left=0,ps=ps,show=[0,0,0,0,0,0,1,0,0,0,0] ;inclination
;   maps,typ=1,/nobox,left=0,ps=ps,show=[0,0,0,0,0,0,0,0,0,0,0,1] ;A0He
;   maps,typ=1,/nobox,ps=ps,show=[0,0,0,0,0,0,1,0,0,0,0,0,0],/azivec,/black_white;inc + vec
end

pro paper,ps=ps
  
  if n_elements(ps) eq 0 then ps=0
;   maps,typ=1,ps=ps,vec_col=0, $
;     show=  [ 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1],  $
;     azivec=[ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],  $
;     sort=  [12, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11, 0]
   maps,typ=1,ps=ps,vec_col=0, $
     show=  [ 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1],  $
     azivec=[ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],  $
     sort=  [-1, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11, 0]+1
 ;    sort=  [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12]
;    maps,typ=1,ps=ps,vec_col=0, $
;      show=  [ 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1],  $
;      azivec=[ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],  $
;      sort=  [-1, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11, 0]+1
;   maps,typ=4,ps=ps
end

pro ccmag,ps=ps
    if n_elements(ps) eq 0 then ps=0

end
