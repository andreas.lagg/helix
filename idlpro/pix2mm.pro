;convert pixel to Mm
;x/y =0 : no conversion done
;x/y =1 : pixel to Mm
;x/y =2 : pixel to Arcsec
;x/y =3 : pixel to user defined coords (see def in user_xy.pro)
function pix2mm,val,reverse=reverse,x=x,y=y
  @common_user_coords
  common pikaia
  common wgst,wgst
  common pwarn,warncnt
  
  done=0
  retval=float(val)                    ;treat user_xy information
  if keyword_set(x) then if x(0) eq 3 then begin
    for i=0,n_elements(val)-1 do $
      if keyword_set(reverse) then begin
      dummy=min(abs(uc_xvec-val(i)))
      retval(i)=float(!c)
    endif else retval(i)=uc_xvec((val(i)>0)<(n_elements(uc_xvec)-1))    
    done =1
  endif
  if keyword_set(y) then if y(0) eq 3 then begin
    for i=0,n_elements(val)-1 do $
      if keyword_set(reverse) then begin
      dummy=min(abs(uc_yvec-val(i)))
      retval(i)=float(!c)
    endif else retval(i)=uc_yvec((val(i)>0)<(n_elements(uc_yvec)-1))
    done =1
  endif
  if done then return,retval
  
  if n_elements(mm) eq 0 then mm=1
  if keyword_set(arcsec) then mm=0 else arcsec=0
  
  r_sun=700000.
  if mm eq 1 then arcsec2mm=r_sun/950./1e3 else arcsec2mm=1.
  p2mmx=0.38
  p2mmy=0.38
  if n_elements(pikaia_result) ne 0 then begin
    hdr=pikaia_result.header
    if max(tag_names(hdr) eq 'CAMERA') then begin
      if strcompress(hdr.telescop,/remove_all) eq 'GREGOR' then pix=0.126 $
      else if strcompress(hdr.camera,/remove_all) eq 'IR1024' then $
        pix=0.17 else pix=0.38
      if hdr.r0radius le 800 then begin
        if n_elements(warncnt) eq 0 then warncnt=0
        if warncnt lt 20 then $
          message,/cont,'Warning: R0 for Sun is wrong (R0='+n2s(hdr.r0radius)+$
          '). R0 is set to 950.'
        hdr.r0radius=950.
        warncnt=warncnt+1
      endif
      arcsec2mm=r_sun/hdr.r0radius/1e3
      p2mmy=pix
      if hdr.stepsize eq 0 then begin
        p2mmx=1.
      endif else p2mmx=hdr.stepsize
    endif else if max(tag_names(hdr) eq 'INSTRUME') then $
      if strcompress(/remove_all,hdr.instrume) eq 'SOT/SP' then begin
      p2mmx=hdr.xscale
      p2mmy=hdr.yscale
    endif else if wgst.p2mm.val ge 1 then $
      message,/cont,'Warning: No stepsize in FITS header.'
  endif
   
  if keyword_set(x) then begin
    if x(0) eq 1 then p2mm=p2mmx*arcsec2mm else p2mm=p2mmx
  endif else if keyword_set(y) then begin
    if y(0) eq 1 then p2mm=p2mmy*arcsec2mm else p2mm=p2mmy
  endif else p2mm=1.
  
  if keyword_set(reverse) then retval=val/p2mm else retval=val*p2mm
  
  return,retval
end
