
pro tw_extract,ps=ps
  common pikaia,pikaia_result,oldsav,prpar
  common hhh,data_sp,oldff
  
  print,'Extract sav-files to be used for Thomas'' extrapolations.'
  
  sz=size(pikaia_result.fit)
  ncomp=pikaia_result.input.ncomp
  sz(1)=sz(1)/pikaia_result.input.stepx
  sz(2)=sz(2)/pikaia_result.input.stepy
  
  fa=fltarr(ncomp,sz(1),sz(2))
  fa1=fltarr(sz(1),sz(2))
  data={ncomp:ncomp,nx:sz(1),ny:sz(2),xscl:0.,yscl:0., $
        btot:fa,azi:fa,inc:fa,vlos:fa,ff:fa, $
        icont:fa1,vred:fa1,vblue:fa1}
  
  xvec=(intarr(sz(2))+1) ## indgen(sz(1))*pikaia_result.input.stepx
  yvec=indgen(sz(2))*pikaia_result.input.stepy ## (intarr(sz(1))+1)
  data.btot=pikaia_result.fit[xvec,yvec].atm.par.b
  data.inc=pikaia_result.fit[xvec,yvec].atm.par.inc
  data.azi=pikaia_result.fit[xvec,yvec].atm.par.azi
  data.vlos=pikaia_result.fit[xvec,yvec].atm.par.vlos
  data.ff=pikaia_result.fit[xvec,yvec].atm.par.ff
  
                                ;read hinode data
  ff=findfile(pikaia_result.input.dir.profile+'/'+pikaia_result.input.observation+'/*.fits')
  reread=1
  if n_elements(oldff) ne 0 then if min(oldff eq ff) eq 1 then reread=0
  oldff=ff
  if reread then read_sot,ff,index_sp,data_sp
  icont=reform(transpose(total(data_sp(90:100,*,0,*),1)/11))
  vred=reform(transpose(total(data_sp(10:30,*,3,*),1)/21))
  vblue=reform(transpose(total(data_sp(35:55,*,3,*),1)/21))
  
  data.icont=congrid(icont,sz(1),sz(2))
  data.vred=congrid(vred,sz(1),sz(2))
  data.vblue=congrid(vblue,sz(1),sz(2))
  
  fsav='./sav/expol_'+pikaia_result.input.observation+'.sav'
  save,/xdr,/compress,data,file=fsav
  print,'Wrote sav-file: '+fsav
  
  wset,0
  psset,ps=ps
  userlct,/full,coltab=0
  image_cont_al,data.icont,contour=0,/aspect
  userlct,/full
  image_cont_al,reform(data.btot(0,*,*)),contour=0,/aspect
  image_cont_al,reform(data.inc(0,*,*)),contour=0,/aspect
  image_cont_al,reform(data.azi(0,*,*)),contour=0,/aspect
  image_cont_al,reform(data.ff(0,*,*)),contour=0,/aspect
  image_cont_al,reform(data.btot(0,*,*)*data.ff(0,*,*)),contour=0,/aspect
  psset,/close
  stop
  
  stop
end
