pro all_spectra,ps=ps,new=new
@common_cppar  
  common profile,profile
  common resa,c1,c2,weight
   
   idl53,prog_dir='./epd_dps_prog/' 

  ipt=['all_spectra_1'];,'all_spectra_2']
  list=[108,71]
  
  if n_elements(c1) eq 0 or keyword_set(new) then $
    helix,ipt=ipt(0)+'.ipt',result=c1,list=list,ret_weight=weight
  
  xpvec=list(0) & ypvec=list(1)
  if c1.input.average then begin
    xpvec=(list(0)+indgen(c1.input.stepx))
    ypvec=(list(1)+indgen(c1.input.stepy))
  endif
  obs=get_profile(profile=profile, $
                  ix=xpvec,iy=ypvec,icont=icont, $
                    wl_range=c1.input.wl_range)
  
  voigt=strupcase(c1.input.profile) eq 'VOIGT'
  magopt=c1.input.magopt(0)
  norm_stokes_val=c1.input.norm_stokes_val
  hanle_azi=c1.input.hanle_azi
  old_norm=c1.input.old_norm
  old_voigt=c1.input.old_voigt
  use_geff=c1.input.use_geff(0)
  use_pb=c1.input.use_pb(0)
  modeval=c1.input.modeval(0)
  prof1=compute_profile(/init,line=c1.line,atm=c1.fit.atm, $
                        obs_par=c1.input.obs_par(0),wl=obs.wl)
  
;    voigt=strupcase(c2.input.profile) eq 'VOIGT'
;   magopt=c2.input.magopt(0)
;  old_norm=c2.input.old_norm
;   use_geff=c2.input.use_geff(0)
;   use_pb=c2.input.use_pb(0)
;   modeval=c2.input.modeval(0)
;   prof2=compute_profile(/init,line=c2.line,atm=c2.fit.atm, $
;                         obs_par=c2.input.obs_par(0),wl=obs.wl)
 
  
  !p.font=-1 & set_plotx
  if keyword_set(ps) then begin
    psout=ps_widget(default=[0,0,1,0,8,0,0],size=[20,20],psname=psn, $
                    file='~/work/aa_2003/figures/all_spectra.eps',/no_x)
    !p.font=0
  endif
  
  plot_profiles,obs,line=c1.line,/iic,iquv='IQUV', $;prof1,prof2, $
    color=[9,9,9],lstyle=[0,0,2],psym=[0,1,4],symsize=1.2,weight=weight,/bw
  
  if !d.name eq 'PS' then begin
    device,/close
    print,'Created PS-file: '+psn
  endif
  
  
  !p.font=-1 & set_plotx
 
end

