pro wlwg_event,event
  common wlwg,wlwg
  common wgst,wgst
  common param,param
  common pikaia
  
  widget_control,event.id,get_uvalue=uval
  if strpos(uval,'wlimg') eq 0 then begin
    wnr=fix(strmid(uval,6,2))
    cnr=fix(strmid(uval,8,2))
    uval='wlimg'
  endif
  if strpos(uval,'wlrange') eq 0 then begin
    wnr=fix(strmid(uval,8,2))
    uval='wlrange'
  endif
  case uval of
    'wlrange': begin
      wgst.wlrange(wnr).val=event.value
;      if wnr eq 0 then pikaia_result.input.wl_range(0)=wgst.wlrange(0).val
;        pikaia_result.input.wl_range(0)<wgst.wlrange(0).val
;      if wnr eq 1 then pikaia_result.input.wl_range(1)=wgst.wlrange(1).val
;        pikaia_result.input.wl_range(1)>wgst.wlrange(1).val
    end
    'wlimg': begin
        wgst.wlimg(wnr,cnr).val=event.value
      end
    'compall': begin
      wgst.compall.val=event.select
      wlsetgrey
    end
    'control': begin
      case event.value of
        'done': widget_control,wlwg.base.id,/destroy
      endcase
    end
    else: begin
      help,/st,event
    end
  endcase
end

pro wlsetgrey
  common wlwg,wlwg
  common param,param
  common wgst,wgst
  
  for ia=1,param.ncomp-1 do for iz=0,1 do begin
    widget_control,wgst.wlimg(iz,ia).id,sensitive=wgst.compall.val eq 0
  endfor
end

pro wl_widget,init=init
  @common_maxpar
  common wgst,wgst
  common wlwg,wlwg
  common param,param

  if keyword_set(init) or n_elements(wlwg) eq 0 then begin
    subst={id:0l,val:0.,str:''}
    wlwg={base:subst,control:subst,compall:subst}
    wgst.compall.val=1
  endif
  
  wlwg.base.id=widget_base(title='Select WL-Range', $
                           group_leader=wgst.base.id,col=1)

  lbl=['WL(A):','to']
  sub0=widget_base(wlwg.base.id,col=1,/frame,space=0)
  tmp=widget_label(sub0,value='WL-Range for profile plot:',/align_left)
  sub=widget_base(sub0,col=2,space=0)
  for iz=0,1 do begin
    is='_'+n2s(iz,format='(i2.2)')
    wgst.wlrange(iz).id=cw_field(sub,uvalue='wlrange'+is,title=lbl(iz), $
                                 value=(wgst.wlrange(iz).val), $
                                 /all_events,/float,xsize=7)
  endfor
  
  sub0=widget_base(wlwg.base.id,col=1,/frame,space=0)
  tmp=widget_label(sub0,value='WL-Range for Images:',/align_left)
  sub=widget_base(sub0,col=2,space=0)
  for iz=0,1 do begin
    for ia=0,param.ncomp-1 do begin
      isia='_'+n2s(iz,format='(i2.2)')+n2s(ia,format='(i2.2)')
      if iz eq 0 then lab='Comp. '+n2s(ia+1)+': WL(a):' else lab='to'
      wgst.wlimg(iz,ia).id=cw_field(sub,uvalue='wlimg'+isia,title=lab, $
                                    value=(wgst.wlimg(iz,ia).val), $
                                    /all_events,/float,xsize=7)
    endfor
  endfor
  wgst.compall.id=cw_bgroup(sub0,uvalue='compall',set_value=wgst.compall.val, $
                            /nonexclusive,'Same WL-range for all components')
  wlsetgrey
  
  
  wlwg.control.id=cw_bgroup(wlwg.base.id,['Done'],uvalue='control',row=1, $
                            button_uvalue=['done'])
  
  widget_control,wlwg.base.id,/realize
  xmanager,'wlwg',wlwg.base.id,no_block=1
end

pro select_wl_widget
  common wlwg,wlwg
  
  if n_elements(wlwg) ne 0 then begin
    widget_control,wlwg.base.id,iconify=0,bad_id=bad_id
    if bad_id eq 0 then return
  endif
  
  wl_widget,init=init

end
