pro crisp2tip,fits=fits,wlref=wlref,wlvec=wlvec
  
  
  
  userlct,coltab=0,/full,/reverse
  
  if (n_elements(fits) eq 0 or n_elements(wlref) eq 0 or $
      n_elements(wlvec) eq 0) then begin
    print,'Converting CRISP fits files to TIP format for HeLIx+ inversions.'
    print,'Usage:'
    print,'  crisp2tip,fits=''/data/slam/home/lagg/data/gautam_data/andreas.fits'', $'
    print,'            wlref=6302.4936d,wlvec=dindgen(9)*0.048d -.240d'
    retall
  endif
  
  lslpos=strpos(fits,'/',/reverse_search)
  dotfits=strpos(fits,'.fits')
  froot=strmid(fits,lslpos+1,dotfits-lslpos-1)
  if lslpos ge 0 then fpath=strmid(fits,0,lslpos+1) else fpath=''
  
  dat=readfits_ssw(fits,dathead)
  order_crisp=['I','Q','U','V']
  sz=size(dat)
  nwl=sz(1)
  nx=sz(2)
  ny=sz(4)
  
  data=make_array(nwl,ny,nx*4,type=sz(0))
  order_fits=['I','Q','U','V']
  for is=0,3 do begin
    iu=where(order_fits eq order_crisp(is))
    for ix=0,nx-1 do data(*,*,ix*4+iu)=reform(dat(*,ix,is,*))
  endfor
    
  szd=size(data)
  print,'============ SIZE ======================'
  print,'Size: ',szd
  print,'WL-pixels: ',szd(1)
  print,'y-pixels: ',szd(2)
  print,'x-pixels (*4): ',szd(3)
  
  data=float(data)
;  byteorder,data,/lswap
    
  fitsout=fpath+froot+'.fitscc'
  print,'======= WRITE NEW FITS ================='
  print,'File: ',fitsout
  
  writefits,fitsout,data,append=0
    
 ;now create ccx file  
  print,'======= WRITE NEW CCX ================='
  filex=fitsout+'x'
                                ;create header
  header=''
  
  
                                ;determine maximum continuum level
                                ;over te whole image (take median of
                                ;100 highest values))
  ihi=median(icont(*),7)
  ihi=ihi(reverse(sort(ihi)))
  icont_avg=median(ihi(0:99))
  
  icont=reform(float(dat(0,*,0,*)))
  writefits,filex,icont,header,append=0
  naxpos=max(where(strpos(header,'NAXIS') eq 0))
  header=[header(0:naxpos), $
          'WL_NUM  = '+string(nwl,format='(i20)')+ $
          ' / WL-bins', $
          'WL_OFF  = '+string(wlref,format='(d20.8)')+' / WL-Offset', $
          'WL_DISP = '+string(wlvec(1)-wlvec(0),format='(d20.8)')+' / WL-Dispersion', $
          'ICNTAVG = '+string(icont_avg,format='(d20.8)')+ $
          ' / Total averaged quiet Sun Continuum', $
          header(naxpos+1:*)]
  writefits,filex,icont,header,append=0        
  
                                ;write out wl-vector as extension
  writefits,filex,wlvec+wlref,append=1
  print,'Wrote '+filex
  print,'Done.'
 
  stop
end
