  common xa_mat,xa0_mat,xa1_mat,xa2_mat,xa3_mat,xa4_mat,xa5_mat,xa6_mat, $
    xa7_mat,xa8_mat,xa9_mat,xa10_mat,xa11_mat,xa12_mat, $
    xam1_mat,xam2_mat,x41_mat,x42_mat,qqp1_mat,qqp2_mat,x0t10a_vec,x0t10b_vec
  common hanle,is2,nterm,ntrans,lsto2,energy,aesto,fsto,ntlsto,ntusto, $
    u1sto,u2sto,riosto,wlsto,nunk,nemiss
  common hanle1,ntab,j2tab,jp2tab,ktab,irtab,rnutab,qtab
  common hanle2,njlevl,njlevu,njoff,autl,autu,auoff,cl,cu,coff
  common hanle3,thetad,chid,gammad 
  common hanle4,onumvec,trasf
  njoff=-20
  auoff=[-20,1]
  coff=[-20,1,0]
