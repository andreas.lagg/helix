;calculate ff from sgrad
function get_ff,sgrad,icomp,abs=abs
  
  sz=size(sgrad)
  if sz(0) le 2 then ff=sgrad*0+1. $
  else begin
    if keyword_set(abs) then $
      ff=(abs(sgrad(icomp,*,*)))/total(abs(sgrad),1) $
    else $
      ff=(sgrad(icomp,*,*)>0)/total(sgrad>0,1)
  endelse

  return,ff
end

