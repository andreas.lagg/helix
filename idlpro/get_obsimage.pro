function get_obsimage,obs,par=par,wlrange=wlrangein,error=error,title=title, $
                      maximg=maximg,fitstype=fitstype,icont=icont, $
                      pixelrep=pixelrep,absval=absval,xyinfo=xyinfo
  common profile,profile
  common getobscomm,oldobs,oldtyp,oldpixelrep
  common pstore,profile_store
  common pikaia,pikaia_result,oldsav,prpar
  common obsfile,obsfile
  common prof_set,prof_set
@greeklett.pro  
  
  reread=0
  if n_elements(oldobs) eq 0 then reread=1 $
  else reread=reread or (oldobs ne obs) or (oldtyp ne fitstype)
  if n_elements(profile) eq 0 then reread=1
  if n_elements(profile_store) eq 0 then reread=1
  absval=keyword_set(absval)
  if n_elements(pixelrep) ne 0 and n_elements(oldpixelrep) ne 0 then $
    reread=reread or (oldpixelrep ne pixelrep)
  wlrange=wlrangein
  wlunit=f_angstrom & wlscl=1. & wlfmt='(f15.2)'
  if n_elements(prof_set) ne 0 then if prof_set.wlunit eq 1 then begin
    wlunit='nm' & wlscl=0.1 & wlfmt='(f15.3)'
  endif
  if reread then begin
    greek
    error=1
    avpar=['I','Q','U','V','I/Ic','Ic']
    paridx=where(strupcase(avpar) eq strupcase(par(0)))
    if paridx(0) eq -1 then $
      message,/trace,'Invalid Stokes-Parameter: '+par(0)
    
                                ;find sav file of observation
    path=['','./','./profile_archive/']
    
    if n_elements(pikaia_result) ne 0 then begin
      path=remove_multi([pikaia_result.input.dir.profile,path])
      emptydata=pikaia_result.fit.fitness*0.     
    endif else emptydata=0.
    oadd=['','.profiles.sav','.sav']
;    if strpos(obs,'profiles.sav') eq -1 then obssav=obs+'.profiles.sav' $
;    else obssav=obs
    found=0 & i=0 & j=0
    repeat begin 
      obssav=obs+oadd(j)
      fi=file_info(path(i)+obssav)
      cpi=i & cpj=j
      j=j+1
      if j eq n_elements(oadd) then begin
        j=0
        i=i+1
      endif
;      good=(fi.exists eq 1 and fi.directory eq 0)
      good=(fi.exists eq 1)
    endrep until (good or i ge n_elements(path))
    fnopath=strmid(fi.name,strpos(fi.name,'/',/reverse_search)+1, $
                   strlen(fi.name))
    if good ne 0 and strpos(obssav,'.sav') eq strlen(obssav)-4 and $
      strpos(obssav,'.sav') ge 1 then begin
      cpath=path(cpi)
      print,'Restoring OBS-Image from sav '+cpath+obssav+' ... ',format='(a,$)'
      restore,cpath+obssav
      print,'Done.'
    endif else if fi.directory then begin ;Hinode directory
      cpath=path(cpi)
      hindat=get_hinode_full(cpath+obssav,error=error)
      if error ne 0 then begin  ;return empty image
        message,/cont,'Error in reading Hinode data: ',cpath+obssav
        return,emptydata        
      endif else begin
        icont=hindat.icont
        obsimg=icont*0
        maximg=icont*0
        if total(wlrange) eq 0 then wlrange=minmax(hindat.wl)
        for i=0,hindat.nx-1 do if hindat.valid(i) then begin
          inwl=where(hindat.wl(i,*) ge min(wlrange) and $
                     hindat.wl(i,*) le max(wlrange))
          if inwl(0) eq -1 then begin
            if i eq 0 then begin
              iwstr='['+add_comma(n2s(wlrange,format='(f15.2)'),sep=',')+']'
              message,/cont,'Image WL '+iwstr+' invalid - returning empty image.'
              error=1
            endif
          endif else begin
            nwl=n_elements(inwl)
            case par(0) of 
              'I': begin
                if absval then $
                  obsimg(i,*)=total(abs(hindat.sp(inwl,*,0,i)),1)/nwl $
                else obsimg(i,*)=total(hindat.sp(inwl,*,0,i),1)/nwl
                maximg(i,*)=max(abs(hindat.sp(inwl,*,0,i)),dim=1)
              end
              'I/Ic': begin
                if absval then $
                obsimg(i,*)=total(abs(hindat.sp(inwl,*,0,i)),1)/nwl/icont(i,*)$
                else $
                obsimg(i,*)=total(hindat.sp(inwl,*,0,i),1)/nwl/icont(i,*)
                maximg(i,*)=max(abs(hindat.sp(inwl,*,0,i)),dim=1)/icont(i,*)
              end
              'Ic': begin
                if absval then $
                  obsimg(i,*)=abs(icont(i,*)) $
                else obsimg(i,*)=icont(i,*)
                maximg(i,*)=icont(i,*)                
              end
              'Q': begin
                if absval then $
                obsimg(i,*)=total(abs(hindat.sp(inwl,*,1,i)),1)/nwl/icont(i,*)$
                else $
                  obsimg(i,*)=total(hindat.sp(inwl,*,1,i),1)/nwl/icont(i,*)
                maximg(i,*)=max(abs(hindat.sp(inwl,*,1,i)),dim=1)/icont(i,*)
              end
              'U': begin
                if absval then $
              obsimg(i,*)=total(abs(hindat.sp(inwl,*,2,i)),1)/nwl/icont(i,*)$
                else $
                  obsimg(i,*)=total(hindat.sp(inwl,*,2,i),1)/nwl/icont(i,*)
                maximg(i,*)=max(abs(hindat.sp(inwl,*,2,i)),dim=1)/icont(i,*)
              end
              'V': begin
                if absval then $
                obsimg(i,*)=total(abs(hindat.sp(inwl,*,3,i)),1)/nwl/icont(i,*)$
                else $
                  obsimg(i,*)=total(hindat.sp(inwl,*,3,i),1)/nwl/icont(i,*)
                maximg(i,*)=max(abs(hindat.sp(inwl,*,3,i)),dim=1)/icont(i,*)
              end
            endcase
          endelse
        endif
        error=0
        title=par(0)+', '+ $
          add_comma(sep='-', $
                    n2s(remove_multi([min(wlrange),max(wlrange)]*wlscl), $
                        format=wlfmt))+' '+wlunit
        return,obsimg
      endelse
    endif else if (strpos(strlowcase(fnopath),'imax') ge 0 and $
                   strpos(fnopath,'4D') eq -1) then begin ;IMAX
      dummy=read_imax(fi.name,x=0,y=0,do_ls=0)
      common imaxinfo,imaxinfo,imaxdata,imaxheader,imaxwl
      if total(wlrange) eq 0 then wlrange=minmax(imaxwl)
      inwl=where(imaxwl ge min(wlrange) and imaxwl le max(wlrange))
      if inwl(0) eq -1 then begin
        if i eq 0 then begin
          iwstr='['+add_comma(n2s(wlrange,format='(f15.2)'),sep=',')+']'
          message,/cont,'Image WL '+iwstr+' invalid - returning empty image.'
          error=1
          maximg=fltarr(imaxinfo.nx,imaxinfo.ny)
          return,maximg
        endif
      endif else begin
        nwl=n_elements(inwl)
        if nwl eq 1 then begin  ;articifially increase dimension for
                                ;total and max procedure below
          inwl=[inwl,inwl]
          nwl=2
        endif
        icont=imaxdata(*,*,imaxinfo.icidx,0)
        case par(0) of 
          'I': begin
            if absval then img=total(abs(imaxdata(*,*,inwl,0)),3)/nwl $
            else img=total(imaxdata(*,*,inwl,0),3)/nwl
            maximg=max(abs(imaxdata(*,*,inwl,0)),dim=3)
          end
          'I/Ic': begin
            if absval then img=total(abs(imaxdata(*,*,inwl,0)),3)/nwl/icont $
            else img=total(imaxdata(*,*,inwl,0),3)/nwl/icont
            maximg=max(abs(imaxdata(*,*,inwl,0)),dim=3)/icont
          end
          'Ic': begin
            if absval then img=icont $
            else img=abs(icont)
            maximg=icont
          end
          'Q': begin
            if absval then img=total(abs(imaxdata(*,*,inwl,1)),3)/nwl/icont $
            else img=total(imaxdata(*,*,inwl,1),3)/nwl/icont
            maximg=max(abs(imaxdata(*,*,inwl,1)),dim=3)/icont
          end
          'U': begin
            if absval then img=total(abs(imaxdata(*,*,inwl,2)),3)/nwl/icont $
            else img=total(imaxdata(*,*,inwl,2),3)/nwl/icont
            maximg=max(abs(imaxdata(*,*,inwl,2)),dim=3)/icont
          end
          'V': begin
            if absval then img=total(abs(imaxdata(*,*,inwl,3)),3)/nwl/icont $
            else  img=total(imaxdata(*,*,inwl,3),3)/nwl/icont
            maximg=max(abs(imaxdata(*,*,inwl,3)),dim=3)/icont
          end
        endcase
        error=0
        title=par(0)+', '+ $
          add_comma(sep='-', $
                    n2s(remove_multi([min(wlrange),max(wlrange)]*wlscl), $
                        format=wlfmt))+' '+wlunit
        return,img
      endelse
    endif else $                ;SPINOR Inversion FITS Files
      if (strmid(fi.name,strlen(fi.name)-5,5) eq '.fits' or $
          strmid(fi.name,strlen(fi.name)-8,8) eq '.fits.gz') then begin
      common oldfitsdata,fdat,fdaticont,oldfitsobs,oldfitstype,wlavg,szf
      reread=n_tags(fdat) eq 0 or n_elements(oldfitsobs) eq 0
      if n_elements(fitstype) eq 0 then fitstype=0
      if reread eq 0 then reread=oldfitsobs ne obs or oldfitstype ne fitstype
      typstr=([' (obs)',' (fit)',' (deconvolved fit)'])(fitstype)
      if reread then begin
        head=headfits(obs)
        code=strtrim(strlowcase(sxpar(head,'CODE')),2)
        if code eq '0' then $
          if (strpos(obs,'inverted_obs') ne -1 or $
              strpos(obs,'inverted_profs') ne -1) then code='spinor'
        if code eq 'spinor' or strmid(code,0,5) eq 'helix' then begin
          read_fitsstokes,obs,fdat,fitstype=fitstype,error=error,/full, $
            pixelrep=pixelrep
        endif else begin
;          if code eq 'spinor' then order='IVQU' else order='IQUV'          
          read_fits4d,obs,fdat,error=error,/full,order=order
;          imgic=get_histocont(fdat.icont)
        endelse
        if error ne 0 then begin
          message,/cont,'Error in reading data: '+obs
          return,0
        endif
        oldfitsobs=obs
        oldfitstype=fitstype
;        fdaticont=max((fdat.i),dim=1)
        fdaticont=fdat.icont 

                                ;calculate average wl-vector and do
                                ;wl-selection based on average WL
        szf=size(reform((*fdat.data)(*,fdat.order(0),*,*)))
        if (size(fdat.wl))(0) eq 1 then wlavg=fdat.wl $
        else wlavg=total(total(fdat.wl,2),2)/(szf(2)*szf(3))
        icont=fdat.icont
      endif
      if n_tags(fdat) eq 0 or (size(*fdat.data))[0] ne 4 then begin
        error=1
        message,/cont,'Error in reading data: '+obs
        stop
        return,0
      endif
;fdat.order=[0,2,3,1]        
      if max(strpos(tag_names(fdat),'XOFF')) ne -1 then $
        xyinfo=[fdat.xoff,fdat.yoff,fdat.nx,fdat.ny] $
      else xyinfo=[0,0,fdat.nx,fdat.ny]        
      img=fltarr(szf(2),szf(3))
      maximg=fltarr(szf(2),szf(3))
      if total(wlrange) eq 0 then wlrange=minmax(fdat.wl)
      inwl=where(wlavg ge min(wlrange) and wlavg le max(wlrange))
;if inwl[0] eq -1 and reread then stop
;timecheck,'goi1'      
      if inwl(0) ne -1 then begin
        nwl=n_elements(inwl)
        icont=fdaticont
        divi=1.
        fii=fdat.data
        imgic=get_histocont(icont)
;ACHTUNG!!!! SOMETHING IS ODD HERE!!! 2018-03-01        
imgic=1.        
        case par(0) of 
          'I': begin
            io=0
            divi=imgic/icont
          end
          'I/Ic': begin
            io=0
;            divi=icont
          end
          'Ic': begin
            img=icont
            maximg=icont
          end
          'Q': begin
            io=1
          end
          'U': begin
            io=2
          end
          'V': begin
            io=3
          end
        endcase
        if par(0) ne 'Ic' then begin
          oio=fdat.order(io)
          fimg=(*fii)(inwl,oio,*,*)
          if absval then fimg=abs(fimg)
          szif=size(fimg)
          fimg=reform(fimg,szif[[1,3,4]])
          img=total(fimg,1,/nan)/nwl/divi
          maximg=max(abs(fimg),/nan,dim=1)/divi
;print,'HIER',io,oio,fdat.order        
        endif
        error=0
        title=par(0)+typstr+', '+ $
              add_comma(sep='-', $
                        n2s(remove_multi([min(wlrange),max(wlrange)]*wlscl), $
                            format=wlfmt))+' '+wlunit

;timecheck,'goi2'      
        return,img
      endif else begin
        error=1
        return,0
      endelse
    endif else begin
      if n_elements(obsfile) ne 0 then begin
        obscc=obsfile.cc
        obsccx=obsfile.ccx
      endif else begin
        obscc=obs
        obsccx=obs+'x'
      endelse
      
                                ;use cc-file if present
      if obscc eq '' then begin
        message,/cont,'Sav-file of Observation '+obssav+' not found in '+ $
          add_comma(sep=',',path)
        message,/cont,'NO CC-file of observation found.'
        return,-1
      endif
      if n_elements(pikaia_result) ne 0 then wl_off=pikaia_result.input.wl_off $
      else wl_off=0.
      if wl_off lt 1e-5 then begin
        if obsccx eq '' and obscc ne '' then $
          obsccx=obscc+'x'
        read_ccx,add_qm(obsccx),wl_disp=wl_disp, $
          wl_off=wl_off,icont=icont,error=xerr,wl_vec=wlvec
        if xerr ne 0 then begin
          message,/cont,'No CCX file found. No WL-Calibration possible.' + $
            ' Using BIN#'
          wl_off=0. & wl_disp=1. & wlvec=dindgen(2048)
        endif
        nmax=n_elements(wlvec)
      endif else begin
        wl_disp=pikaia_result.input.wl_disp
        wl_off=pikaia_result.input.wl_off
        nmax=2048
        wlbinvec=indgen(nmax)
        wlvec=double(wlbinvec)*wl_disp+wl_off
      endelse
      wlbinvec=indgen(nmax) ;(just larger than the largest possible range)
      if total(wlrange) eq 0 then wlrange=minmax(wlvec)
      inwl=where(wlvec ge min(wlrange) and wlvec le max(wlrange))
      if inwl(0) eq -1 then begin
        if total((wlvec ge min(wlrange)) or $
                 (wlvec le max(wlrange))) eq nmax and $
          max(wlrange) ge min(wlvec) and min(wlrange) le max(wlvec) then begin
          inwl=min(where(wlvec ge min(wlrange))) 
        endif else if par(0) eq 'Ic' then begin
          error=0
          title=''
          return,icont
        endif else begin
;        message,/cont,'No WL-points found for selected range in FITS image'
          iwstr='['+add_comma(n2s(wlrange,format='(f15.2)'),sep=',')+']'
          message,/cont,'Image WL '+iwstr+' invalid - returning empty image.'
          return,emptydata
        endelse
      endif
      wlbin=wlbinvec(inwl)
      wlvec=wlvec(inwl)
      common oldxydat,oldwlbin,oldname,iimg,uimg,qimg,vimg,icimg, $
        maxi,maxq,maxu,maxv
      if n_elements(oldwlbin) eq 0 then reread=1 $
      else reread=(min(oldwlbin eq wlbin) eq 0 or $
                   n_elements(oldwlbin) ne n_elements(wlbin) or $
                   oldname ne obscc)      
      if reread then begin        
        occ=add_qm(obscc)
        print,'Extracting images from cc-file for WL '+ $
          n2s(min(wlvec),format='(f15.3)')+' to '+ $
              n2s(max(wlvec),format='(f15.3)')+' ...',format='(a,$)'
        xy_fits,occ,silent=0,x=xpvec,y=ypvec,struct=occstruct, $
          i=iimg,u=uimg,q=qimg,v=vimg,bin=wlbin,/save_memory, $
          maxi=maxi,maxq=maxq,maxu=maxu,maxv=maxv,/norm2ic,absval=absval
        icimg=occstruct.ic
        if n_elements(occstruct) ge 2 then begin
          icnew=reform(iimg)*0.
          for ii=0,n_elements(occstruct)-1 do begin
            if ii eq 0 then i0=0 else i0=total(occstruct(0:ii-1).nx)
            i1=i0+occstruct(ii).nx-1
            icnew(i0:i1,*)=icimg(0:occstruct(ii).nx-1,*,ii)
          endfor
          icimg=temporary(icnew)
        endif
        print,' Done.'
        oldname=obscc
        oldwlbin=wlbin
;        print,'HIERU:',total(uimg(*,72:75,192:195))/16.
      endif
      case par(0) of 
        'I': begin
          img=iimg
          maximg=maxi
        end
        'I/Ic': begin
          img=iimg
          maximg=maxi/icimg
        end
        'Ic': begin
          img=icimg
          maximg=icimg
        end
        'Q': begin
          img=qimg
          maximg=maxq;/icimg
        end
        'U': begin
          img=uimg
          maximg=maxu;/icimg
        end
        'V': begin
          img=vimg
          maximg=maxv;/icimg
        end
      endcase
      obsimg=reform(img)
      maximg=reform(maximg)
      tpar=par(0)

      if par(0) eq 'I/Ic' then if n_elements(icimg) ne 0 then begin
        obsimg=obsimg/icimg
        maximg=maximg/icimg
      endif else tpar='I'
      
      error=0
      title=strupcase(tpar)+', '+ $
        add_comma(sep='-',n2s(remove_multi([min(wlvec),max(wlvec)]*wlscl), $
                              format=wlfmt))+' '+wlunit
      return,obsimg
    endelse
    
                                ;solar MHD files
    if n_elements(stokes_sm) ne 0 then begin
      profile={ic:1.,i:reform(stokes_sm(0,*,*,*)), $
               v:reform(stokes_sm(1,*,*,*)), $
               q:reform(stokes_sm(2,*,*,*)), $
               u:reform(stokes_sm(3,*,*,*)),wl:wl+w0}
    endif
    oldobs=obs
    oldtyp=fitstype
    if n_elements(pixelrep) ne 0 then oldpixelrep=pixelrep
    profile_store=profile
  endif else profile=profile_store
  
  if max(tag_names(profile) eq 'WLREF') eq 1 then $
    wl=profile.wl+profile.wlref $
  else wl=profile.wl
  wlrg=wlrange
  if wlrg(0) gt max(wl) then wlrg=wlrg(1)
  if n_elements(wlrg) eq 2 then if wlrg(1) lt min(wl) then wlrg=wlrg(0) 
  wlrg=wlrange(sort(wlrg))
  
  case n_elements(wlrg) of
    2: begin
      inwl=where(wl ge min(wlrg) and wl le max(wlrg))
    end
    1: begin
      dummy=min(abs(wl-wlrg(0))) & inwl=!c
    end
    else: inwl=-1
  endcase
  if inwl(0) eq -1 then begin
    message,/cont,'Invalid WL-range for observation image. Using all WL-points'
    inwl=indgen(n_elements(wl))
  endif
  
  tn=tag_names(profile)
  it=(where(tn eq strupcase(par(0))))(0)
  if it eq -1 then it=(where(tn eq 'I'))(0)
  
  img=profile.(it)
  if tn(it) eq 'IC' then begin
    obsimg=img 
    if (size(profile.ic))(0) eq 0 then obsimg=profile.i ;for mhd data the profile.ic is only one number and is set to 1
  endif else obsimg=total(img(inwl,*,*),1,/nan)/n_elements(inwl)

                                ;multiply by IC
  if strupcase(par(0)) eq 'I' then if max(obsimg) lt 2 then $
    if max(tag_names(profile) eq 'IC') eq 1 then begin
    obsimg=obsimg*profile.ic
  endif
  
  title=strupcase(par(0))+', '+ $
    add_comma(sep='-', $
              n2s(remove_multi([min(wl(inwl)),max(wl(inwl))]/10), $
                  format='(f15.2)'))+' nm'
  error=0
  return,obsimg
end
