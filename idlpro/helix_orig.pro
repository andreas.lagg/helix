;parameters:
;ncomp - number of atmospheric components 
;ncalls - number of pikaia steps
;unno - if set to 1 then use Unno-solution for determination of
;       inclination. In this case the inlcination is not fitted by pikaia.
;       Additionally, the azimuthal angle is only calculated using Q
;       and U observed profile.
;azi_unno - only fit azimuth using unno solution, inclination is a
;           free parameter
;estimate_unno - if set then the Unno solution is used as an estimate
;                for the pikaia fit: the pikaia values for inclination
;                and azimuth are only allowed to vary within a narrow
;                range of the Unno solution
;iquv_weight - 4-element vector defining relative weighting of
;              IQUV. Additionally the code does an automatic weighting
;              according to the strength of the I signal compared to
;              the QUV signals. Not used for Unno solution.
;stray - if set to one then first component is treated as straylight
;        component, that means, no mag-field is fitted
;sav - name of sav-file to be restored
;verbose - set to 1 or 2 for verbose pikaia output
;nosav - if set then no sav file of the results is created
;x - set x-pixel of observation to be analyzed. x can also be a
;    two-elements vector [xmin,xmax]
;y - set y-pixel of observation to be analyzed. y can also be a
;    two-elements vector [ymin,ymax]
;stepx - step size to go from xmin to xmax
;stepy - step size to go from ymin to ymax
;average - average observation over the xstep/ystep size
;voigt - if set then use voigt profiles instead of gaussians
pro helix_orig,ncalls=ncalls,x=x,y=y,ncomp=ncomp,unno=unno,stray=stray, $
            verbose=verbose,sav=sav,nosav=nosav,average=average, $
            stepx=xstep,stepy=ystep,estimate_unno=estimate_unno,voigt=voigt, $
            azi_unno=azi_unno,iquv_weight=iquv_weight
  common profile,profile
  common pikaia_result,pikaia_result
  common init,init
  
  first_run=1
  
  if n_elements(x) eq 0 then x=10
  if n_elements(x) eq 1 then xr=[x,x] else xr=x
  if n_elements(y) eq 0 then y=10
  if n_elements(y) eq 1 then yr=[y,y] else yr=y
  if n_elements(ncalls) eq 0 then ncalls=120l
  if n_elements(verbose) eq 0 then verbose=2
  
  if n_elements(xstep) eq 0 then xstep=4 ;X-step size for processing image
  if n_elements(ystep) eq 0 then ystep=4 ;Y-step size for processing image
  if keyword_set(average) eq 0 then average=0 
  if n_elements(iquv_weight) ne 4 then iquv_weight=[1.,1.,1.,1.]
  
;  xp=35 & yp=27                 ;use profile ix,iy
; xp=20 & yp=20                 ;use profile ix,iy
  xp=33 & yp=18                 ;use profile ix,iy
  xp=39 & yp=24                 ;use profile ix,iy
  xp=35 & yp=27                 ;use profile ix,iy
;  xp=38 & yp=25                 ;use profile ix,iy
;  xp=73 & yp=42                 ;use profile ix,iy
;  xp=86 & yp=11                 ;use profile ix,iy
  
                                ;define spectral line
  he=def_line(line='he') ;helium triplet
  si=def_line(line='si')        ;silicon line;------- PIKAIA PARAMATERS ------

  ca=def_line(line='ca')        ;calcium line
  
  
  
  
;  cont_corr=0.98
  cont_corr=1.
  
  par=def_parst(scale=scl)
  
  
  if n_elements(ncomp) ne 0 then nc_chrom=ncomp $
  else nc_chrom=2 
  chrom_atm=replicate(par,nc_chrom) ;the chromospheric atmosphere consists
                                ;of two components
  for i=0,nc_chrom-1 do chrom_atm.use_line(0:2)=he.id
  
                                  ;test: do not fit B
;  chrom_atm.fit.b=0 & chrom_atm.par.b=1000
  
                                ;do not fit azi (do not forget to set
                                ;weight for q and u to zero!)
;   chrom_atm.fit.azi=0
;   chrom_atm.par.azi=60
;   chrom_atm.fit.inc=0
;   chrom_atm.par.inc=0
  
;    chrom_atm(1).fit.b=0    & chrom_atm(1).par.b=0
;    chrom_atm(1).fit.azi=0  & chrom_atm(1).par.azi=0
;    chrom_atm(1).fit.inc=0  & chrom_atm(1).par.inc=0
  chrom_atm.par.b=1000
  chrom_atm.par.width=.1
  chrom_atm.par.a0=1.
  
                                ;couple azimuth and inclination for
                                ;all components
  chrom_atm.fit.azi=-1
  chrom_atm.fit.inc=-2
  
; chrom_atm.fit.azi=0 & chrom_atm.par.azi=-40
; chrom_atm.fit.inc=0 & chrom_atm.par.inc=90
  
  
  
  nc_phot=1                     ;the photopheric atmosphere consists
  phot_atm=replicate(par,nc_phot) ;of one component                
  for i=0,nc_phot-1 do phot_atm.use_line(0:1)=[si.id,ca.id]
  
  
;  total_atm=[chrom_atm,phot_atm]
  total_atm=chrom_atm
  n_atm=n_elements(total_atm)
  
  if keyword_set(stray) then begin ;set straylight component
    total_atm(0).par.b=0
    total_atm(0).fit.b=0
    total_atm(0).par.azi=0
    total_atm(0).fit.azi=0
    total_atm(0).par.inc=0
    total_atm(0).fit.inc=0    
  endif
  
  qwgt=1. & uwgt=1.
  if keyword_set(unno) eq 0 then unno=0
  if unno or keyword_set(azi_unno)  then begin
                                ;do not fit Q and U profiles to obtain
                                ;azimuthal information, use
                                ;approximation only;
                                ;use unno solution also for getting
                                ;inlcination angle. Since only the
                                ;V-profile is fitted (not q and u),
                                ;the cos(inc) enters the equation only
                                ;as a factor of V and therefore is
                                ;redundant to the filling factor of
                                ;this component. Therefore we do not
                                ;fit the inclination angle and
                                ;determine it after the pikaia routine
                                ;by applying the Unno solution
    total_atm.par.azi=0
    total_atm.fit.azi=0
    if keyword_set(azi_unno) eq 0 then begin
      total_atm.par.inc=0
      total_atm.fit.inc=0    
      qwgt=0
      uwgt=0
    endif
    
  endif
  
                                ;do not use unno solution for
                                ;straylight component
  if unno or keyword_set(estimate_unno)  or keyword_set(azi_unno) then begin
    nostray_idx=indgen(n_atm)
    if keyword_set(stray) then begin
      if n_atm gt 1 then nostray_idx=nostray_idx(1:*) $
      else begin
        message,/cont,'Unno solution not for straylight component.' + $
          ' Setting unno to zero.'
        unno=0
      endelse
    endif
  endif
  
  
  
                                ;define wl-vector for which profile
                                ;should be computed
  nwl=1000
  wl=findgen(nwl)/(nwl-1)*10.+10825d
  
  physical_constants            ;define constants (!c_light)
  
  check_atm,chrom_atm        ;perform consistency checks on atmosphere
  check_line,he                 ;perform consistency checks on lines
  check_line,si                 ;perform consistency checks on lines
  check_line,ca                 ;perform consistency checks on lines

  if n_elements(sav) eq 0 then sav='13may01.014'
  if n_elements(profile) eq 0 then begin
    rsav='./profile_archive/'+sav+'.profiles.sav'
    print,'restoring '+rsav
    restore,rsav
  endif
  
  xr=(xr>0)<(n_elements(profile.x)-1)
  yr=(yr>0)<(n_elements(profile.y)-1)
  
                                ;loop for applying analysis to image
  tstart=systime(1)
  for xp=xr(0),xr(1),xstep do begin
    for yp=yr(0),yr(1),ystep do begin
      
                                ;read observation
      xpvec=xp & ypvec=yp
      if average then begin
        xpvec=xp+indgen(xstep)
        ypvec=yp+indgen(ystep)
      endif
      
      n=0 & icont=0.
      for ixp=0,n_elements(xpvec)-1 do for iyp=0,n_elements(ypvec)-1 do $
        if xpvec(ixp) lt n_elements(profile.x) and $
        ypvec(iyp) lt n_elements(profile.y) then begin
        obs=get_profile(profile=profile,ix=xpvec(ixp),iy=ypvec(iyp),icont=ic, $
                        wl_range=[10826.d,10833d])
;                          wl_range=[10829.9d,10830.7d])
        icont=icont+ic
        if n eq 0 then observation=obs $
        else for ii=0,n_tags(observation)-1 do $
          observation.(ii)=observation.(ii)+obs.(ii)
        
        n=n+1
      endif
      icont=icont/n
      for ii=0,n_tags(observation)-1 do $
        observation.(ii)=observation.(ii)/n
    
;      observation.q=-observation.q
;      observation.u=-observation.u
      
                                ;adjust continuum level!
    icont=icont*cont_corr
    
    he.icont=icont
    si.icont=icont
    ca.icont=icont
    
    
;  all_lines=[he,si,ca]
; all_lines=[si,ca]
    all_lines=he
    
    
                                ;define weighting
    nwl=n_elements(observation.wl)
    emprof=fltarr(nwl)
    
    good_range=[10828.5d,10831.35d]
    good=(where(observation.wl ge good_range(0) and $
                observation.wl le good_range(1)))
;   verygood_range=[10829.6d,10831.35d]
    verygood_range=[10829.6d,10832.5d,10831.35d]
                                ;define very good range where telluric
                                ;blend begins (5 points left of large
                                ;derivative of I)
    istart=min(where(observation.wl ge verygood_range(2)))
    if istart ne -1 then begin
      istop=(min(where(deriv(observation.wl(istart:*), $
                             observation.i(istart:*)) lt -1e4))-5)>0
      verygood_range(1)=observation.wl(istart+istop)
    endif

    
    verygood=(where(observation.wl ge verygood_range(0) and $
                    observation.wl le verygood_range(1)))
    
    
    good_mag_range=[10828.5d,10832.5d]
    good_mag=(where(observation.wl ge good_mag_range(0) and $
                    observation.wl le good_mag_range(1)))
    verygood_mag_range=[10829.6d,10832.5d,verygood_range(1)]
    verygood_mag=(where(observation.wl ge verygood_mag_range(0) and $
                        observation.wl le verygood_mag_range(1)))
    verygood_mag_icont=(where(observation.wl ge verygood_mag_range(0) and $
                              observation.wl le verygood_mag_range(2)))
    
    
    wgt=emprof
    wgt(good)=0.2
    wgt(verygood)=1.
    wgt(verygood)=wgt(verygood)/(observation.i(verygood)/icont)
    wgt=wgt/max(wgt)
    
    wgt_mag=emprof
    wgt_mag(good_mag)=0.2
    wgt_mag(verygood_mag)=1.
    
    wgt_unno=wgt_mag          ;weighting for unno solution: instead of
                                ;multiplying with I we divide by I to
                                ;weight the wings of the lines more
                                ;than the center.
    wgt_mag(verygood_mag_icont)= $
      wgt_mag(verygood_mag_icont)/(observation.i(verygood_mag_icont)/icont)
    wgt_unno(verygood_mag_icont)= $
      wgt_unno(verygood_mag_icont)*(observation.i(verygood_mag_icont)/icont)
    wgt_mag=wgt_mag/max(wgt_mag)
    wgt_unno=wgt_unno/max(wgt_unno)
    
    scl_mag=(max(observation.i)-min(observation.i))/icont/ $
      sqrt(max(observation.q^2+observation.u^2+observation.v^2))
    scl_mag=scl_mag<30
    wgt=wgt/scl_mag
    
                                ;define weights (wl-dependent and
                                ;iquv dependent)
    weight={i:wgt * iquv_weight(0), $
            q:wgt_mag * iquv_weight(1), $
            u:wgt_mag * iquv_weight(2), $
            v:wgt_mag * iquv_weight(3) }

                                ;define scaling vector (pikaia
                                ;requires all parameters to lie within
                                ;[0,1])
    scale=replicate(scl,n_atm)
    
                                ;make one comp fast
                                ;if more than one non-stray components
                                ;are defined
    if (keyword_set(stray) and nc_chrom eq 3) or $
       (keyword_set(stray) eq 0 and nc_chrom eq 2)then begin
      scale(n_atm-2:n_atm-1).vlos.min=[-10,5]*1e3
      scale(n_atm-2:n_atm-1).vlos.max=[10,45]*1e3
    endif
    
    if keyword_set(stray) then begin ;set velocity component of straylight to
                                ;small values
      scale(0).vlos.min=[-3]*1e3
      scale(0).vlos.max=[ 3]*1e3
    endif
    
                                ;do azi/inc fit seperately using Unno solution
    if unno or keyword_set(estimate_unno) or keyword_set(azi_unno) then begin
      common for_amoeba,u_signal,q_signal,quv_weight
;      mag_weight=sqrt(weight.q^2+weight.u^2+weight.v^2)
      mag_weight=wgt_mag       ;do use standard mag-weighting for unno
                                ;solution, ignore iquv_weight keyword
      
                                ;calculate unno solution where weight
                                ;for magnetic components is large
                                ;enough and only in +-(rg_el) A range around
                                ;dominant line (largest log_gf)
      rg_wl=0.5
      dummy=max(all_lines.f) & idom=!c
      dummy=min(abs(observation.wl-all_lines(idom).wl)) & wl_idom=!c
                                ;check for min in I to reflect
                                ;velocity shift
      dummy=max((icont-observation.i)* $
                (observation.wl ge (all_lines(idom).wl-0.5) and $
                 observation.wl le (all_lines(idom).wl+0.5) ))
      wlv_idom=!c
      
      ui=where(mag_weight gt 0.5*max(mag_weight) and $
               (observation.wl ge observation.wl(wlv_idom)-rg_wl and $
                observation.wl le observation.wl(wlv_idom)+rg_wl))

      quv_weight=wgt_unno(ui)  ;unno solution (see Auer, SolPhys 1977)
                                ;best if used in line wings, not in
                                ;line center. Therfore: instead of
                                ;using weighting used for pikaia (line
                                ;center of I is weighted highest) we
                                ;divide by the I-profile to get the
                                ;wings weighted higher.
      quv_weight=(quv_weight-min(quv_weight))/ $
        (max(quv_weight-min(quv_weight)))*0.25+0.75 ;scale weight to [0.75,1]
;      quv_weight(*)=1
;      unno_weight=wgt_unno*0 & unno_weight(ul)=quv_weight
      
      u_signal=observation.u(ui)
      q_signal=observation.q(ui)
      alpha2=amoeba(1.0e-5,scale=1.,p0=1.,function_value=diff, $
                    function_name='fit_azi_amoeba',ncalls=n_amoeba,nmax=100)
                                ;set azimuthal angle for all
                                ;components of the atmosphere to the
                                ;fitted value
      azi=(alpha2(0)/2./!dtor mod 360)

      total_atm(nostray_idx).par.azi=azi

      
                                ;calculate inclination using unno
                                ;approximation:
                                ;use s1_hat and s3_hat from Auer (1977
                                ;SolPhys), do not need to divide by
                                ;S0_cont-S0 because only ration of
                                ;S1/S3 is used.
      
      common for_amoeba_inc,s1_hat,s3_hat,s13_weight
;       s13_weight= $
;         (observation.u^2+observation.q^2+observation.v^2)*(observation.i^2)
      s13_weight=quv_weight
      s3_hat=observation.v(ui)*1e3
      s1_hat=(observation.q(ui)*cos(2*azi*!dtor) + $
              observation.u(ui)*sin(2*azi*!dtor))*1e3
      
;       inc=1.                    ;initial guess
;       s1_fit= CURVEFIT(abs(s3_hat),abs(s1_hat), quv_weight, inc, $
;                      SIGMA, FUNCTION_NAME='fit_inc',/DOUBLE,ITER=it, $
;                      CHISQ=chi,TOL=1e-5)     
      inc=amoeba(1.0e-6,scale=.01,p0=1.,function_value=diff, $
                 function_name='fit_inc_amoeba',ncalls=n_amoeba,nmax=100)
      
                                ;depending on the sign of i_plus and
                                ;i_minus (pi-components of V-signal)
                                ;the inclination angle is in the range
                                ;[0,90] or [90,180].
      inc=inc(0)/!dtor
      inc=abs(inc) mod 180
      dummy=max(mag_weight)  & iwmax=!c
      sign_lo=(total(observation.v(0:iwmax)*mag_weight(0:iwmax)) ge 0)*2-1
      sign_hi=(total(observation.v(iwmax:*)*mag_weight(iwmax:*)) ge 0)*2-1
      if sign_lo lt 0 and sign_hi gt 0 then inc=180-inc
      
;      plot,observation.wl,mag_weight & oplot,color=1,observation.wl(ui),mag_weight(ui) & oplot,color=2,observation.wl(ui),quv_weight & oplot,color=3,observation.wl(ui),s13_weight & print,azi,inc
;      plot,atan(u_signal/q_signal)/!dtor/2
;stop      
      
                                ;set inclination angle for all
                                ;components of the atmosphere to the
                                ;fitted value
      total_atm(nostray_idx).par.inc=inc
      
;print,sign_lo,sign_hi,inc & plot,observation.v*mag_weight & oplot,color=1,(observation.v*mag_weight)(0:iwmax) & stop      
      
      print,'Unno solution: inc='+n2s(inc)+' , azi='+n2s(azi)
      if unno then begin
;        weight.q(*)=0      ;do not use q and u for further fit routine
;        weight.u(*)=0           ;but use it for estimate_unno and azi_unno
      endif
    endif
    
    if keyword_set(estimate_unno) then begin ;set scaling for azi and inc
                                ;according to Unno solution
      
      rg_azi=20.              ;allow pikaia solution for azi to deviate rg_azi
                                ;degreesfrom unno solution
      if keyword_set(azi_unno) then rg_inc=180 $
      else rg_inc=20. ;allow pikaia solution for inc to deviate rg_inc
                                ;degreesfrom unno solution
      
      inc_rg=((inc+[-1,1]*rg_inc)>0)<180
      azi_rg=((azi+[-1,1]*rg_azi)>(-90))<90
      print,'Scale using Unno: ' + $
        add_comma(n2s(inc_rg,format='(f15.1)'),sep=' < inc < ')+'  ' + $
        add_comma(n2s(azi_rg,format='(f15.1)'),sep=' < azi < ')     
      
                                ;do not apply for straylight (0th)
                                ;component
      scale(nostray_idx).azi.min=azi_rg(0)
      scale(nostray_idx).azi.max=azi_rg(1)
      scale(nostray_idx).inc.min=inc_rg(0)
      scale(nostray_idx).inc.max=inc_rg(1)
    endif
    
    
                                ;call pikaia
    t=systime(1)
    inprof=compute_profile(/init,line=all_lines,atm=total_atm, $
                           wl=observation.wl)
    
                                ;normalize weight
    tot_weight=100.
    wgt_sum=0.
    for it=0,3 do wgt_sum=wgt_sum+total(weight.(it))
    if wgt_sum eq 0 then wgt_sum=1.
    for it=0,3 do weight.(it)=weight.(it)*tot_weight/wgt_sum

    fitprof=call_pikaia(line=all_lines,atm=total_atm, $
                        observation=observation,ncalls=ncalls, $
                        weight=weight,scale=scale,voigt=voigt, $
                        ret_atm=atm,fitness=fitness,verbose=verbose)
    
    
    plot_profiles,observation,fitprof,weight=weight, $
    init=0
    
    prc=((xp*1.-xr(0))*(yr(1)-yr(0)+1.)+yp*1.+1-yr(0))/ $
      ((yr(1)-yr(0)+1.)*(xr(1)-xr(0)+1.))
    tdiff=systime(1)-tstart
    tend=tstart+tdiff/(prc)
    
    wgt_str='.'+add_comma(sep='',n2s(fix(iquv_weight)))
    if n_elements(savconstruct) eq 0 then $
      savconstruct=sav+'.pikaia.comp'+n2s(n_elements(atm))+ $
      (['','.unno'])(unno)+ $
      wgt_str+ $
      (['','.azi_unno'])(keyword_set(azi_unno))+ $
      (['','.voigt'])(keyword_set(voigt))+ $
      (['','.est-unno'])(keyword_set(estimate_unno))+ $
      (['','.stray'])(keyword_set(stray))  
  
    xyouts,0,1,/normal,charsize=1.4,'!C'+savconstruct
    
    xyouts,/normal,charsize=1.4,0,0,add_comma(n2s([xp,yp]),sep=' | ')+', '+ $
      'Fitness: '+n2s(fitness,format='(f15.3)')+', Perc: '+n2s(prc*100.,format='(f15.2)')+' end: '+conv_time(old='sec',new='date_dot',tend,/idl)
    print_atm,atm,scale=scale
    
    print,total(atm.par.ff)
    
                                ;store profiles
    if yp eq yr(0) then begin
      fitst={atm:atm,fitness:0d,x:0.,y:0.}
      nx=n_elements(profile.x)
      ny=n_elements(profile.y)
      pikaia_result={line:all_lines,fit:replicate(fitst,ny)}
      first_run=0
    endif  
    pikaia_result.fit(yp).x=xp
    pikaia_result.fit(yp).y=yp
    pikaia_result.fit(yp).atm=atm
    pikaia_result.fit(yp).fitness=fitness
;  pikaia_result.fit(yp).profile=fitprof
    print,'Pikaia-Time: '+n2s(systime(1)-t)+' sec'
  endfor                        ;loop for x/y on obesrvation image
  
                                ;create sav file for every slit-pos
  if keyword_set(nosav) eq 0 then begin
    psav='./sav/'+savconstruct+ $
    '.col'+string(xp,format='(i3.3)')+'.sav'
    save,file=psav,pikaia_result,/compress,/xdr
    print,'Created sav-file: '+psav
  endif
    
endfor                          ;loop for x/y on obesrvation image
  
;  display_data,sav=psav,/reread
end
