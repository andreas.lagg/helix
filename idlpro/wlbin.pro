;du simple wavelength binning, eg. for convolution files
pro wlbin,file,wl_bin
 
  
  openr,unit,/get_lun,file
  a=''
  dd=dblarr(2)
  repeat begin
    readf,unit,a
    if strmid(strtrim(a),0,1) eq ';' then begin
      if n_elements(hdr) eq 0 then hdr=a else hdr=[hdr,a] 
    endif else begin
      reads,a,dd
      if n_elements(data) eq 0 then data=dd else data=[[data],[dd]]
    endelse    
  endrep until eof(unit)
  free_lun,unit
  
  sz=size(data)
  nwl=sz(2)
  
  iw=0
  for i=0,nwl-1,wl_bin do begin
    j=(i+wl_bin-1)<(nwl-1)
    data(0,iw)=total(data(0,i:j))/(j-i+1)
    data(1,iw)=total(data(1,i:j))/(j-i+1)
    iw=iw+1
  endfor
  data=data(*,0:iw-1)
  print,'# of WL-bins: ',iw
  
  ldot=(strpos(file,'.',/reverse_search))(0)
  fnew=strmid(file,0,ldot)+'.bin'+n2s(wl_bin)+strmid(file,ldot,strlen(file))
  
  openw,unit,/get_lun,fnew
  if n_elements(hdr) ne 0 then printf,unit,hdr
  printf,unit,';original file: '+file
  printf,unit,';WL-binning: '+n2s(wl_bin)
  printf,unit,data
  free_lun,unit
  print,'Wrote WL-binned file: '+fnew
  
  
end
