
pro line_change_hanle
  common greek_letters
  
  par='bfiel'
  ipt=read_ipt('hanle_synth.ipt')
  
  case par of
      'vlos': begin
        paridl='ipt.atm.par.vlos'
        val=[0,-10000.,10000]
        title='Geschwindigkeit'
        unit='km/s'
        scale=1000.
        off=0.
        prof='I'
      end
      'vdopp': begin
        paridl='ipt.atm.par.dopp'
        val=[0.3,0.53,0.07]
        title='Temperatur'
        unit='K'
        scale=1/5000.
        off=3500.
        prof='I'
      end
      'bfiel': begin
        paridl='ipt.atm.par.b'
        val=[1.,3.,5.,10.,50.,70.]
;        val=[100.,200.,300.,500.,1000.]
        title='Magnetic Field'
        unit='G'
        scale=1.
        off=0.
        step0=5
      end
      else: stop
    endcase
    
    ps=1
    file='line_change_'+par
    psset,ps=ps,file=file+'.ps',/no_x,size=[18,26]
    userlct
    
    for i=0,n_elements(val)-2 do begin
      ipt=read_ipt('hanle_synth.ipt')
      
      dummy=execute(paridl+'=val(i)')
        
;        help,/st,ipt.atm.par    
      ipt.verbose=0 & ipt.display_profile=0
      helix,fitprof=fitprof,struct_ipt=ipt,result=result
;        help,/st,result.fit.atm.par
      
      dummy=execute('prof'+n2s(i)+'=fitprof')
    endfor
      
    !p.charsize=1.25
    !p.font=0
    plot_profiles,prof0,prof1,prof2,prof3,prof4,prof5,psym=4

  
  
    psset,/close
 
  stop
end
