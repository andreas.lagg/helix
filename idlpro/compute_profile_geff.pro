function gaussfunc,a0=a0,shift=shift,width=width,wl=wl
  

  expo=(-(wl-shift)^2./(2.*width^2.))>(-30)
  return, a0 * exp(expo) / sqrt(2*!pi*width^2)
end

function voigt_prof,a0=a0,dopp=dopp,damp=damp,shift=shift,wl=wl,F=F
;function gaussfunc,a0=a0,shift=shift,width=width,wl=wl
  
  vvec=(wl-shift)/dopp
  
  pvoigt,damp,vvec,H,F
  
                                ;voigt integral to be multiplied by a/!pi
                                ;(see e.g.) Jefferies ApJ 1989 343
  F=2*F
  return,H
  
end

function compute_profile,atm=atm,line=line,wl=wl,init=init,obs_par=obs_par
  common cp,prf,nl,na,use,nu
  common profile_shape,profile_shape
  common verbose,verbose
  @common_cppar
  
  
  addnum=1.

  if keyword_set(init) then begin
    prf=wl*0.
    nl=n_elements(line)
    na=n_elements(atm)
    use=bytarr(nl,na)  ;flag if a atmosphere should be used for a line
    for il=0,nl-1 do for ia=0,na-1 do $
      use(il,ia)=max(id2s(atm(ia).use_line) eq id2s(line(il).id))
    nu=float(total(use))
    if voigt then profile_shape='voigt' else profile_shape='gauss'
    if n_elements(verbose) eq 0 then verbose=1
    if verbose eq 2 then print,'Profile shape: '+profile_shape
    if n_elements(iprof_only) eq 0 then iprof_only=0
  endif
  
;  profile={wl:wl,i:prf,q:prf,u:prf,v:prf,istray:prf}
  profile={wl:wl,i:prf,q:prf,u:prf,v:prf}
                                ;filling factor for stray
                                ;light / unmagnetized component
;  ff_stray=(1-total(atm.par.ff))
  ff_stray=0.
  
  for il=0,nl-1 do begin
    for ia=0,na-1 do begin
      if use(il,ia) then begin
        if modeval eq 1 then begin ;voigt-par mode
          dopp=atm(ia).par.dopp
          damp=atm(ia).par.damp
          etazero=atm(ia).par.etazero
        endif else if modeval eq 2 then begin ;voigt-phys mode
                                ;(see Balasubramaniam,
                                ;ApJ 382, 699-705 1991, original:
                                ;Landi Degl'Innocenti, A&AS 25,
                                ;379-390, 1976)
          atm(ia).par.dopp=line(il).wl/!c_light* $
            sqrt(2*1.3805e-23*atm(ia).par.tempe/(line(il).mass*!amu)+ $
                 atm(ia).par.vmici^2)
          atm(ia).par.damp=atm(ia).par.gdamp * $
            line(il).wl^2/(4*!pi*!c_light*atm(ia).par.dopp)
          atm(ia).par.etazero=atm(ia).par.densp* $
            line(il).wl^2/atm(ia).par.dopp* $
            1e-10*(1-exp(-1.43883e8/(line(il).wl*atm(ia).par.tempe)))
        endif
        
                                ;doppler shift (m/s)
        dopshift_wl = atm(ia).par.vlos * line(il).wl / !c_light
;    print,'dopshift_wl=',dopshift_wl

                                ;zeeman shift (B in Gauss)
        zeeman_shift = 4.67E-13 * line(il).wl^2. * line(il).geff * $
          atm(ia).par.b 
;    print,'dop/zeeman_shift=',dopshift_wl,zeeman_shift, line(il).geff 
                                ;compute unshifted profile, normalized
                                ;to icont
        
        if iprof_only eq 0 then begin ;flag to determine if only fit to
                                ;i-profile for straypol_ri�un is to be done
        
        if profile_shape eq 'voigt' then begin
          i_center= $
            0.5*voigt_prof(dopp=atm(ia).par.dopp, $
                           shift=line(il).wl + dopshift_wl, $
                           damp=atm(ia).par.damp, $
                           wl=wl,F=F)*atm(ia).par.etazero
          if magopt then rho_p=0.5*F*atm(ia).par.etazero $
          else rho_p=0.
        endif else begin
          i_center=0.5*(gaussfunc(a0=atm(ia).par.a0, $
                                    shift=line(il).wl + dopshift_wl, $
                                    width=atm(ia).par.width, $
                                    wl=wl))
          rho_p=0.
        endelse

                                ;calculate magnetic profiles only if b
                                ;is to be fitted
        if atm(ia).fit.b eq 1 or atm(ia).par.b ne 0 then begin
          
                                ;compute shifted profiles (+-), normalized
                                ;to icont
          if profile_shape eq 'voigt' then begin
            i_minus= $
              0.5*voigt_prof(dopp=atm(ia).par.dopp, $
                             shift=line(il).wl + dopshift_wl - zeeman_shift, $
                             damp=atm(ia).par.damp, $
                             wl=wl,F=F)*atm(ia).par.etazero
            if magopt then rho_minus=0.5*F*atm(ia).par.etazero $
            else rho_minus=0.
            i_plus= $
              0.5*voigt_prof(dopp=atm(ia).par.dopp, $
                             shift=line(il).wl + dopshift_wl + zeeman_shift, $
                             damp=atm(ia).par.damp, $
                             wl=wl,F=F)*atm(ia).par.etazero
            if magopt then rho_plus=0.5*F*atm(ia).par.etazero $
            else rho_plus=0.
          endif else begin
            i_minus= $
              0.5*(gaussfunc(a0=atm(ia).par.a0, $
                             shift=line(il).wl + dopshift_wl - zeeman_shift, $
                             width=atm(ia).par.width, $
                             wl=wl))
            rho_minus=0.
            i_plus= $
              0.5*(gaussfunc(a0=atm(ia).par.a0, $
                             shift=line(il).wl + dopshift_wl + zeeman_shift, $
                             width=atm(ia).par.width, $
                             wl=wl))
            rho_plus=0.
          endelse
                                ;compute v-profile: Sum of to
                                ;gaussians with opposite sign, shifted
                                ;by Zeeman splitting value
                                ;see ronan et al, 1987 SoPhys vol 113, p353
          eta_v = line(il).f*1./2.*(i_plus - i_minus) * cos(atm(ia).par.inc*!dtor)
          rho_v=  line(il).f*1./2.*(rho_plus - rho_minus) * cos(atm(ia).par.inc*!dtor)
          
                                ;compute q-profile: +Q direction
                                ;defines azimuth angle zero.
                                ;see ronan et al, 1987 SoPhys vol 113, p353
          quprof = line(il).f*1./2.*( i_center - 1./2.*(i_plus + i_minus) )
          rho_qu = line(il).f*1./2.*( rho_p - 1./2.*(rho_plus + rho_minus) )
          
          eta_q= quprof * (sin(atm(ia).par.inc*!dtor))^2 * $
            cos(atm(ia).par.azi*!dtor*2.)      
          rho_q= rho_qu * (sin(atm(ia).par.inc*!dtor))^2 * $
            cos(atm(ia).par.azi*!dtor*2.)
                                ;u-profile is q profile x tan(2*azi)
                                ;see auer et al., 1977 SoPhys, vol 55,
                                ;p47
          eta_u= quprof * (sin(atm(ia).par.inc*!dtor))^2 * $
            sin(atm(ia).par.azi*!dtor*2.)
          rho_u= rho_qu * (sin(atm(ia).par.inc*!dtor))^2 * $
            sin(atm(ia).par.azi*!dtor*2.)
          
          
        endif else begin  ;sigma components are the same as central pi
                                ;component because of zero lande
                                ;shift.
          eta_q=0 & eta_u=0 & eta_v=0
          i_plus=i_center
          i_minus=i_center
          rho_q=0 & rho_u=0 & rho_v=0
          rho_plus=rho_p
          rho_minus=rho_p
        endelse
        eta_i=line(il).f*1./2.*(i_center * (sin(atm(ia).par.inc*!dtor))^2  + $
                     1./2. * (i_minus + i_plus) * $
                     (1+(cos(atm(ia).par.inc*!dtor))^2))
        
;     endif else begin            ;non-magnetic case
;       i_pol=0 & qprof=0 & uprof=0 & vprof=0
;       i_stray = i_center
;     endelse
        
        
        
                                ;solution of balasubranamian, ApJ
                                ;1991, no rho for magneto-optical
                                ;effects, same as jefferies, ApJ 1989, 343
;     DELTA=(1 + eta_i)^2 * ( (1 + eta_i)^2 - eta_q^2 - eta_u^2 - eta_v^2 )    
;     I =1 + F/DELTA*( (1+eta_i) * (1+eta_i)^2 )
;     Q =-F/DELTA*( (1+eta_i)^2 * eta_q )
;     U =-F/DELTA*( (1+eta_i)^2 * eta_u )
;     V =-F/DELTA*( (1+eta_i)^2 * eta_v )
        
;     DELTA=( (1d + eta_i)^2 - eta_q^2 - eta_u^2 - eta_v^2 ) ;simpler
;     I =+F/DELTA*( (1+eta_i) )
;     Q =-F/DELTA*( eta_q )
;     U =-F/DELTA*( eta_u )
;     V =-F/DELTA*( eta_v )
        
        F=atm(ia).par.ff 
        R=(eta_q*rho_q + eta_u*rho_u +eta_v*rho_v)
        DELTA=( (1 + eta_i)^2 * $
                ( (1 + eta_i)^2 - eta_q^2 - eta_u^2 - eta_v^2 + $
                  rho_q^2 + rho_u^2 + rho_v^2 ) - R^2)
        
                                ;multiply by gradient of source function
                                ;source function at tau=0: B0
       F2DELTA=atm(ia).par.sgrad * F /DELTA

       B0=1. - atm(ia).par.sgrad ;B0 must match continuum level outside line
;        B0=line(il).f - atm(ia).par.sgrad ;B0 must match continuum level outside line
        
        I =B0*F +  $
          F2DELTA*( (1+eta_i) * ((1+eta_i)^2 +rho_q^2+rho_u^2+rho_v^2) )
        Q =-F2DELTA*( (1+eta_i)^2 * eta_q +  $
                      (1+eta_i)*(eta_v*rho_u-eta_u*rho_v) + rho_q*R )
        U =-F2DELTA*( (1+eta_i)^2 * eta_u + $
                      (1+eta_i)*(eta_q*rho_v-eta_v*rho_q) + rho_u*R )
        V =-F2DELTA*( (1+eta_i)^2 * eta_v + $
                      (1+eta_i)*(eta_u*rho_q-eta_q*rho_u) + rho_v*R)
      endif else begin ;calculate only I profile with a simple fit of a voigt
                                ;or gaussian, no rad. transfer...
                                ;This is used only for the
                                ;straypol_run. The idea is to fit the
                                ;I profile with a single voigt or
                                ;gauss to determine the shape of the
                                ;profile to be used for the correction
                                ;in Q and U
        
        if profile_shape eq 'voigt' then begin
          gsub=voigt_prof(dopp=atm(ia).par.dopp, $
                         shift=line(il).wl + dopshift_wl, $
                         damp=atm(ia).par.damp, $
                          wl=wl,F=F) * $
            atm(ia).par.etazero*atm(ia).par.ff*line(il).f
        endif else begin
          gsub=gaussfunc(a0=atm(ia).par.a0*atm(ia).par.ff*line(il).f, $
                         shift=line(il).wl + dopshift_wl, $
                         width=atm(ia).par.width, $
                         wl=wl)
        endelse
        I=1./(1+gsub/2.)/na ;same as for i in 'real' case
        Q=0
        U=0
        V=0
      endelse 
        
        dummy=check_math(mask=32) ;avoid message floating underflow
        
        profile.q = profile.q + Q /nl ;Q/I_c
        profile.u = profile.u + U /nl ;U/I_c
        profile.v = profile.v + V /nl ;V/I_c
        profile.i = profile.i + I*line(il).icont /nl ;I

      endif                   ;use line / comp
      
                                ;add straypolarization fit
      if line(il).straypol_par(ia).width gt 1e-5 or $
        line(il).straypol_par(ia).dopp gt 1e-5 or $
        line(il).straypol_par(ia).damp gt 1e-5 then begin
        
        
        if profile_shape eq 'voigt' then begin
          gsub=voigt_prof(dopp=line(il).straypol_par(ia).dopp, $
                          shift= $
                          line(il).wl*(1.+line(il).straypol_par(ia).vlos/ $
                                       !c_light), $
                          damp=line(il).straypol_par(ia).damp, $
                          wl=wl,F=F)* $
            atm(ia).par.ff*line(il).par(ia).straypol_eta0
        endif else begin
          gsub=gaussfunc(a0=line(il).par(ia).straypol_amp*atm(ia).par.ff, $
                         shift= $
                         line(il).wl*(1.+line(il).straypol_par(ia).vlos/ $
                                      !c_light), $
                         width=line(il).straypol_par(ia).width, $
                         wl=wl)
        endelse
;        gsub=gsub*line(il).f,
        gsub=gsub*line(il).straypol_par(ia).sign
                                ;correction along B (Hanle)
        qsub=+gsub*cos(atm(ia).par.azi*!dtor*2.)
        usub=+gsub*sin(atm(ia).par.azi*!dtor*2.)
        profile.q=profile.q+qsub
        profile.u=profile.u+usub
      endif
    endfor                      ;loop for component
    
  endfor                        ;loop for line
  
;  plot,profile.wl,i_center & oplot,color=1,profile.wl,i_plus & oplot,color=2,profile.wl,i_minus
;  plot_profiles,profile & print_atm,atm & stop
  
  return,profile
end
