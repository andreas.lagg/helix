function gaussfunc,a0=a0,shift=shift,width=width,wl=wl
  
  return, a0 * exp( -(wl-shift)^2./(2.*width^2.))
end


function compute_profile,atm=atm,line=line,wl=wl,init=init
  common cp,prf,nl,na,mult_line,mult_atm,mult_wl,mult_la,wu,nu
  
  
  if keyword_set(init) then begin
    prf=wl*0.
    nl=n_elements(line)
    na=n_elements(atm)
    mult_line=intarr(na)+1d ;vectors for multiplying line / atm to obtain
    mult_atm =intarr(nl)+1d     ;matrix
    mult_wl  =wl*0 +1d
    mult_la  =intarr(na*nl) +1d
    use=bytarr(nl*na)  ;flag if a atmosphere should be used for a line
    for il=0,nl-1 do for ia=0,na-1 do $
      use(il + ia*nl)=max(atm(ia).use_line eq line(il).id)
    wu=where(use)
    nu=n_elements(wu)
  endif
  
  profile={wl:wl,i:prf,q:prf,u:prf,v:prf,istray:prf}
  
                                ;filling factor for stray
                                ;light / unmagnetized component
;  ff_stray=(1-total(atm.ff))
  ff_stray=0.
  
  
  
                                ;doppler shift (m/s)
  dopshift_wl=(((mult_atm # atm.vlos) * (line.wl # mult_line) / $
                !c_light)(wu)) # mult_wl
  
                                 ;zeeman shift (B in Gauss)
  zeeman_shift = ((4.67E-13 * ((line.wl^2. * line.geff) # mult_line) * $
                   (mult_atm # atm.b))(wu)) # mult_wl
  
  a0_arr=((mult_atm # atm.a0)(wu)) # mult_wl ;array for amplitude
  shift_arr=((line.wl # mult_line)(wu)) # mult_wl ;array for shift
  width_arr=((mult_atm # atm.width)(wu)) # mult_wl ;array for width
  wl_arr=mult_la(wu) # wl  ;array for WL
  
  inc_arr=((mult_atm # (atm.inc*!dtor))(wu)) # mult_wl ;array for inclination
  cosinc_arr=cos(inc_arr)
  sin2inc_arr=sin(inc_arr)*sin(inc_arr)
  azi2_arr=2*((mult_atm # (atm.azi*!dtor))(wu)) # mult_wl ;array for 2*azimuth
  
  f_arr=((line.f # mult_line)(wu)) # mult_wl ;array for gf
  ff_arr=((mult_atm # atm.ff)(wu)) # mult_wl ;array for filling factor
  
  icont_arr=((line.icont # mult_line)(wu)) # mult_wl ;array for icont
  
                                ;compute unshifted profile, normalized
                                ;to icont
  i_center= f_arr * ff_arr * $  ;1./icont_arr * $
    gaussfunc(a0=a0_arr,shift=shift_arr + dopshift_wl, $
              width=width_arr,wl=wl_arr)
  
                                ;compute shifted profiles (+-), normalized
                                ;to icont
  i_minus= f_arr * ff_arr * $ ;1./icont_arr * $
    gaussfunc(a0=a0_arr,shift=shift_arr + dopshift_wl - zeeman_shift, $
              width=width_arr, $
              wl=wl_arr)
  i_plus= f_arr * ff_arr * $ ;1./icont_arr * $
    gaussfunc(a0=a0_arr,shift=shift_arr + dopshift_wl + zeeman_shift, $
              width=width_arr,wl=wl_arr)
  
                                ;compute v-profile: Sum of to
                                ;gaussians with opposite sign, shifted
                                ;by Zeeman splitting value
                                ;see ronan et al, 1987 SoPhys vol 113,
                                ;p353
  profile.v=total(cosinc_arr * 1./2.*(i_minus - i_plus),1)/icont_arr
  
  
  quprof = 1./2.*( - i_center + 1./2.*(i_plus + i_minus) )/icont_arr
                                ;u-profile is qu prof x cos(2*azi)
                                ;see auer et al., 1977 SoPhys, vol 55,
                                ;p47
  profile.q =total(sin2inc_arr * cos(azi2_arr) * quprof ,1)
  
                                ;u-profile is qu prof x sin(2*azi)
                                ;see auer et al., 1977 SoPhys, vol 55,
                                ;p47
  profile.u =total(sin2inc_arr * sin(azi2_arr) * quprof ,1)
  
                                ;compute straylight / unmagnetized
                                ;i-profile: Assume same Amplitude and
                                ;Width as for magnetized profiles.
  i_stray = ff_stray / ff_arr * i_center
  profile.istray = total(icont_arr - i_stray,1)/nu
    
                                ;compute total magnetized I-profile,
                                ;normalized to icont
  i_pol = (i_minus + i_plus)
    
                                ;compute magnetized i-profile: This is
                                ;the sum of 2 Gaussians shifted +/- the
                                ;Zeeman splitting
  profile.i = total(icont_arr  - ( i_pol + i_stray ),1)/nu
  
  return,profile
end
