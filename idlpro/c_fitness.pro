function c_fitness,profile,observation,weight,icont
  common quv_strength,quv_strength,istr,qstr,ustr,vstr,nfree,chi2mode
  
  
  if n_elements(istr) eq 0 then begin
    istr=1. & qstr=1. & ustr=1. & vstr=1.
  endif
  
  
;JuanMas definition
  if (chi2mode eq 1) then begin
    noise=1e-3
    weights=fltarr(4) + 1./noise
    weights(0)=(0.8*ABS(1.01-MAX(observation.i)/icont)+0.2)/NOISE
; chi2=(1./nfree * $
;       (weights(0)^2*total(weight.i^2.*((profile.i-observation.i)/icont)^2)+ $
;        weights(1)^2*total(weight.q^2.* (profile.q-observation.q)^2)+ $
;        weights(2)^2*total(weight.u^2.* (profile.u-observation.u)^2)+ $
;        weights(3)^2*total(weight.v^2.* (profile.v-observation.v)^2)) )
    diffvec=(1./nfree * $
          (weights(0)^2*(weight.i^2.*((profile.i-observation.i)/icont)^2)+ $
           weights(1)^2*(weight.q^2.* (profile.q-observation.q)^2)+ $
           weights(2)^2*(weight.u^2.* (profile.u-observation.u)^2)+ $
           weights(3)^2*(weight.v^2.* (profile.v-observation.v)^2)) )
;do not consider NAN values (most likely spikes or 0 prefilter transmission)
    if min(finite(diffvec)) eq 0 then diffvec(where(finite(diffvec) eq 0))=0
    chi2=total(diffvec)
    diff=chi2 /10              ;make fitness smaller
   endif else if (chi2mode eq 2) then begin ;PLAIN weighting (no strength)
      diffvec=((weight.i*(1e3*(profile.i-observation.i)/icont)^2.)+$
            (weight.q*(1e3*(profile.q-observation.q))^2.) + $
            (weight.u*(1e3*(profile.u-observation.u))^2.) + $
            (weight.v*(1e3*(profile.v-observation.v))^2.))/1e3
;do not consider NAN values (most likely spikes or 0 prefilter transmission)
      if min(finite(diffvec)) eq 0 then diffvec(where(finite(diffvec) eq 0))=0
      diff=total(diffvec)
      diff=diff/(nfree/100.)
    endif else begin
      diffvec=((weight.i*(1e3*(profile.i-observation.i)/icont)^2.)/istr+$
            (weight.q*(1e3*(profile.q-observation.q))^2.)/qstr + $
            (weight.u*(1e3*(profile.u-observation.u))^2.)/ustr + $
            (weight.v*(1e3*(profile.v-observation.v))^2.)/vstr)/1e6
;do not consider NAN values (most likely spikes or 0 prefilter transmission)
      if min(finite(diffvec)) eq 0 then diffvec(where(finite(diffvec) eq 0))=0
      diff=total(diffvec)
      diff=diff/(nfree/100.)
    endelse

  if (diff lt 1e-8) then diff=1.
  minval=1./(diff)
  
  return,minval
end

