pro def_iptgroup
  common iptgroup,iptgroup
  
  st={name:'',element:strarr(100),comment:strarr(100),default:strarr(100), $
      nmipar:intarr(100),show:intarr(100)+1}
  iptgroup=replicate(st,11)
  iptgroup(0).name='COMMENTS'
  iptgroup(1).name='DIRECTORIES'
  iptgroup(2).name='CONTROL'
  iptgroup(3).name='DATA SET'
  iptgroup(4).name='POST PROCESSING'
  iptgroup(5).name='ATMOSPHERES'
  iptgroup(6).name='LINE PARAMETERS'
  iptgroup(7).name='BLENDS'
  iptgroup(8).name='GENERAL FIT PARAMETERS'
  iptgroup(9).name='ANALYSIS METHOD'
  iptgroup(10).name='PIKAIA PARAMATERS'
  
  iptgroup(1).element(0:6)=['PS','SAV','PROFILE_ARCHIVE', $
                            'ATM_ARCHIVE','ATM_SUFFIX','WGT','ATOM']
  iptgroup(1).default(0:6)=['./ps/','./sav/','./profile_archive/', $
                            './atm_archive/','v01','./wgt/','./atom/']
  iptgroup(1).comment(0)='directory for postscript output'
  iptgroup(1).comment(1)='directory for storing results (sav-file)'
  iptgroup(1).comment(2)='input directory for profiles / observations'
  iptgroup(1).comment(3)='output directory for results (atmospheres)'
  iptgroup(1).comment(4)='add a suffix to the atm-directory to identify this run'
  iptgroup(1).comment(5)='directory for wgt-files'
  iptgroup(1).comment(6)='directory for atomic data files'
  
  
  iptgroup(2).element(0:8)=['DISPLAY_MAP','DISPLAY_PROFILE','DISPLAY_COMP', $
                            'VERBOSE','SAVE_FITPROF','FITSOUT','OUTPUT', $
                            'OLD_NORM','OLD_VOIGT']
  iptgroup(2).default(0:8)=['0','1','0','2','0','1','X','0','0']
  iptgroup(2).comment(0)='if 1 then display maps of parameters after successful run (multiple pixels only!)'
  iptgroup(2).comment(1)='if 1 then display fitted & observed profiles'
  iptgroup(2).comment(2)='if 1 then display individual atm. components of fitted profiles'
  iptgroup(2).comment(3)='verbosity of output: 0=quiet, 1=normal, 2=women'
  iptgroup(2).comment(4)='if 1 then save fitted profile as sav-file (can be used as input profile)'
  iptgroup(2).comment(5)='if 1 then write out map results in FITS format'
  iptgroup(2).comment(6)='set to ''PS'' for postscript output'
  iptgroup(2).comment(7)='use code with old normalization of the various atmospheric components'
  iptgroup(2).comment(8)='if 1 use old normalization of Faraday/Voigt function'
  
  iptgroup(3).element(0:36)=['OBSERVATION', $
                             'WL_RANGE','WL_NUM','WL_DISP','WL_OFF','WL_BIN', $
                             'XPOS','YPOS','PROFILE_LIST', $
                             'STEPX','STEPY','AVERAGE','SCANSIZE','SYNTH', $
                             'NOISE','INOISE','SMOOTH','MEDIAN','SPIKE', $
                             'MIN_QUV', $
                             'STRAYPOL_AMP', $
                             'STRAYPOL_CORR','HANLE_AZI','SLIT_ORIENTATION', $
                             'M1ANGLE','SOLAR_POS','SOLAR_RADIUS', $
                             'HELIO_ANGLE','+Q2LIMB', $
                             'HIN_SCANNR','NORM_STOKES','NORM_CONT', $
                             'IC_LEVEL', $
                             'LOCALSTRAY_RAD','LOCALSTRAY_FWHM', $
                             'LOCALSTRAY_CORE','LOCALSTRAY_POL']
  iptgroup(3).default(0:36)=['synth_default.profile.sav','10825.0 10835.0', $
                             '256','0','0','1','000','000','', $
                             '1','1','1','0','0', $
                             '0.0','0.0','0 0','0','0','0.0', $
                             '0.0', $
                             '0 B','0.','0','0', $
                             '0.0 0.0','950.','0.','0.',$
                             '1','IC','LOCAL','0.', $
                             '0.','0.','0.','1']
  iptgroup(3).comment(0)='observation in one of the formats: 1.) sav file of the format ''obsname''.profiles.sav, 2. spinor-profile, 3. sav-file created with helix ''SAVE_FITPROF'' keyword, 4. reduced TIP data file (extension ''.cc'') and auxliary file (''.ccx'')'
  iptgroup(3).comment(1)='WL-range to be used for analysis (may be only a part of the observation WL-range)'
  iptgroup(3).comment(2)='# of WL-points (for synthesis only)'
  iptgroup(3).comment(3)='WL-calibration: dispersion per WL-bin number'
  iptgroup(3).comment(4)='WL-calibration: offset (used if != 0)'
  iptgroup(3).comment(5)='wavelength binning'
  iptgroup(3).comment(6)='two-elements vector containing xmin,xmax of the observation map to be analyzed'
  iptgroup(3).comment(7)='two-elements vector containing ymin,ymax of the observation map to be analyzed'
  iptgroup(3).comment(8)='filename of profile-list (2 columns: xxx yyy). If present, XPOS and YPOS are ignored. File must be located in ./ or PROFILE_ARCHIVE.'
  iptgroup(3).comment(9)='step size for going from xmin to xmax'
  iptgroup(3).comment(10)='step size for going from ymin to ymax'
  iptgroup(3).comment(11)='if 1 then average observation over the stepx/stepy size'
  iptgroup(3).comment(12)='stepsize of multiple scans within one observation'
  iptgroup(3).comment(13)='if 1 then create synthetic profile'
  iptgroup(3).comment(14)='noise level for adding random noise (IQUV)'
  iptgroup(3).comment(15)='noise level for adding random noise to Stokes I only'
  iptgroup(3).comment(16)='smooth-value for profiles and smooth-method: (0=IDL-smooth function,1=FFT Low-Pass)'
  iptgroup(3).comment(17)='median filter for observed profiles (0=off, >=2 median filter width)'
  iptgroup(3).comment(18)='if >=1 then remove electronic spikes. Higher values remove broader spikes.'
  iptgroup(3).comment(19)='min. mag. signal. If sqrt(Q^2+U^2+V^2)<MIN_QUV then the fit will be done with B=0.'  
  iptgroup(3).comment(20)='amplitude for stray-polarization (only used for synthesis)'
;  iptgroup(3).comment(18)='factor for I (constant continuum correction)'
  iptgroup(3).comment(21)='iteration steps and orientation of scattering-polarization correction. Orientation: ''B'' = along B-field, ''X'' = a number defining an angle manually'
  iptgroup(3).comment(22)='Offset for Hanle calc. (experimental)'
  iptgroup(3).comment(23)='slit-orientation (used for straypol-corr)'
  iptgroup(3).comment(24)='angle of coelostat mirror M1'
  iptgroup(3).comment(25)='pos. of observation (x and y in arcseconds)'
  iptgroup(3).comment(26)='radius of sun in arcsec for time of obs.'
  iptgroup(3).comment(27)='heliocentric angle of observation in degrees (for full Hanle). For TIP: theta=acos(sqrt(x^2+y^2)/r, (x,y) = position of observation in terrestrial coordinates.'
  iptgroup(3).comment(28)='pos. Q to limb direction in degrees (for full Hanle). For TIP: Q2L=atan(y,x), emission vector angle gamma=360.-Q2L+90 .'
  iptgroup(3).comment(29)='Hinode Scan# (repetetive scans)'
  iptgroup(3).comment(30)='normalization of Stokes profiles: I or IC'
  iptgroup(3).comment(31)='type of cont. normalization: local, slit or image'
  iptgroup(3).comment(32)='set level for Ic. A positive number overwrites the Ic value contained in the observational data.'  
  iptgroup(3).comment(33)='add a local straylight component: take the average Stokes profile of the surrounding profiles within the radius specified here and mix it via alpha to the other components. The radius is in units of the pixel size defined with the STEPX / STEPY keywords.'
  iptgroup(3).comment(34)='FWHM of the Gaussian weighting used for the localstraylight profiles'
  iptgroup(3).comment(35)='exclude core profiles within the radius specified here'
  iptgroup(3).comment(36)='1=polarized / 0=unpolarized local straylight'
  
  iptgroup(4).element(0:6)=['CONV_FUNC','CONV_NWL','CONV_MODE', $
                            'CONV_WLJITTER', $
                            'CONV_OUTPUT','PREFILTER','PREFILTER_WLERR']
  iptgroup(4).default(0:6)=['','0','FFT','0.0','0',' ','0.0']
  iptgroup(4).comment(0)='convolution function of instrument'
  iptgroup(4).comment(1)='# of bins for convolution (used if number of WL_bins in data is small'
  iptgroup(4).comment(2)='convolution method: FFT or MUL'
  iptgroup(4).comment(3)='uncertainty in \AA in knowledge of exact wavelength position for every filter position (e.g. caused by error in etalon voltage).  Make sure that the wavelength resolution is high enough (i.e. increase CONV\_NWL)! Must be set to ZERO for real data!'
  iptgroup(4).comment(4)='flag to control the display/output of the fitted profile. If 1 then the profile is displayed with CONV_NWL WL pixels, if 0 the WL-pixels of the observation are used. NOTE: for local straylight correction this flag is set to 0.'
  iptgroup(4).comment(5)='prefilter curve'
  iptgroup(4).comment(6)='error in \AA in knowledge of prefilter curve (e.g. caused by unknown temperature). Must be set to ZERO for real data!'
  
  iptgroup(5).element(0:22)=['NCOMP','BFIEL','AZIMU','GAMMA','VELOS','WIDTH', $
                             'AMPLI','VDAMP','VDOPP','EZERO','SZERO','SGRAD', $
                             'GDAMP','VMICI','DENSP','TEMPE', $
                             'DSLAB','SLHGT', $
                             'ALPHA','USE_ATOM','USE_LINE', $
                             'LINES','ATM_INPUT']
  iptgroup(5).comment( 0)='number of components'
  iptgroup(5).comment( 1)='magnetic field value in Gauss'
  iptgroup(5).comment( 2)='azimut of B-vector [deg]'
  iptgroup(5).comment( 3)='inclination of B-vector [deg]'
  iptgroup(5).comment( 4)='line-of-sight velocity in m/s'
  iptgroup(5).comment( 5)='line width (Gauss only)'
  iptgroup(5).comment( 6)='line ampl. (Gauss only)'
  iptgroup(5).comment( 7)='damping constant (Voigt + hanle-slab)'
  iptgroup(5).comment( 8)='doppler broadening (Voigt + hanle-slab)'
  iptgroup(5).comment( 9)='amplitude of components of propagation matrix (Voigt profile only!)'
  iptgroup(5).comment(10)='source function at tau=0'
  iptgroup(5).comment(11)='gradient of source function'
  iptgroup(5).comment(12)='damping constant (Voigt profile phys. units only!)'
  iptgroup(5).comment(13)='micro turbulence in m/s (Voigt profile phys. units only!)'
  iptgroup(5).comment(14)='density parameter (Voigt profile phys. units only!)'
  iptgroup(5).comment(15)='temperature (Voigt profile phys. units only!)'
  iptgroup(5).comment(16)='optical thickness of He slab'; (hanle-slab model only!)'
  iptgroup(5).comment(17)='He slab height in arcsec'; (hanle-slab model only!)'
  iptgroup(5).comment(18)='Filling factor for this component'; (only with multiple components)'
  iptgroup(5).comment(19)='atomic data file(s) for this component. This keyword finalizes the definition of one component and must therefore be the last keyword for this component.'
  iptgroup(5).comment(20)='Lines to be used for this component. This allows to use photospheric atmospheres for photospheric lines only and simultaneously fit chromospheric atmospheres with chromospheric lines.'
  iptgroup(5).comment(21)='obsolete keyword: lines from ''USE_LINE'' parameter are used'
  iptgroup(5).comment(22)='filename for input atmosphere: defines initial value for atmospheric parameters or delivers the input to synthesize a map.'
  iptgroup(5).default(22)=' '
  
  iptgroup(6).element(0:2)=['LINE_ID','LINE_STRENGTH','LINE_WLSHIFT']
  iptgroup(6).comment( 0)='wavelength to identify the line'
  iptgroup(6).comment( 1)='factor for line strength'
  iptgroup(6).comment( 2)='adjust central WL of line'
  
  
  
  iptgroup(7).element(0:4)=['NBLEND','BLEND_WL','BLEND_WIDTH', $
                            'BLEND_DAMP','BLEND_A0']
  iptgroup(7).default( 0)='1'
  iptgroup(7).comment( 0)='number of telluric blends'
  iptgroup(7).comment( 1)='WL and WL-range for blend'
  iptgroup(7).comment( 2)='width of voigt-profile'
  iptgroup(7).comment( 3)='damping of voigt-profile'
  iptgroup(7).comment( 4)='amplitude of voigt-profile (blend is not used if BLEND_A0=0'
  
  iptgroup(8).element( 0:2)=['CCORR','STRAYLIGHT','RADCORRSOLAR']
  iptgroup(8).default( 0)='1.'
  iptgroup(8).default( 1)='0.'
  iptgroup(8).default( 2)='1.'
  iptgroup(8).comment( 0)='factor for cont. correction'
  iptgroup(8).comment( 1)='spectral straylight correction (non-dispersive, non-polarized)'
  iptgroup(8).comment( 2)='Correction for the intensity of the solar radiation field (for Hanle mode only)'
  
  
  iptgroup(9).element(0:10)=['APPROX_AZI','APPROX_DIR','APPROX_DEV_INC', $
                            'APPROX_DEV_AZI','IQUV_WEIGHT','WGT_FILE', $
                            'PROFILE','MAGOPT','USE_GEFF','USE_PB','PB_METHOD']
  iptgroup(9).show([0,1,2,3])=0
  iptgroup(9).default(0:10)=['0','0','20.','20.','1. 1. 1. 1.', $
                            'he_default.wgt','voigt','1','0','0','poly']
  iptgroup(9).comment( 0)='use approximation to calculate magnetic field azimuthal angle directly from Q and U profile (Auer, 1977)'
  iptgroup(9).comment( 1)='use approximation for magnetic field direction. This keyword sets APPROX_AZI to 1. This approximation for the inclination is not very accurate for low B and high inclinations'
  iptgroup(9).comment( 2)='The approximation for the inclination angle of the magnetic field direction is used as an initial value for the minimization. The value is allowed to vary +-X� around the approximation.'
  iptgroup(9).comment( 3)='Same for azimuthal angle.'
  iptgroup(9).comment( 4)='4-element vector defining relative weighting of IQUV (in that order). Additionally the code does an automatic weighting according to the strength of the I signal compared to the QUV signals. This weighting scheme is used in the PIKAIA fit routine.'
  iptgroup(9).comment( 5)='file with WL-dependent weighting function for IQUV'
  iptgroup(9).comment( 6)='functional form for pi- and sigma components of spectral line. Available: gauss, voigt, voigt_phys, voigt_szero'
  iptgroup(9).comment( 7)='include magneto-optical effects (dispersion coefficients, (Voigt profile only!))'
  iptgroup(9).comment( 8)='use effective Lande factor (=1) or real Zeeman pattern (=0)'
  iptgroup(9).comment( 9)='if set, the zeeman-splitting and strength includes the Paschen-Back effect (from the table by Socas-Navarro)'
  iptgroup(9).comment(10)='use polynomials (=poly) or table interpolations (=table) to calculate the PB-effect'
;  iptgroup(9).nmipar([0,1,2,3,4,5,7,8,9,10])=1b
  iptgroup(9).nmipar[[0,1,2,3,4,5,7,8,9,10]]=1b
  iptgroup(9).nmipar(4)=4b
  
  
  iptgroup(10).element(0:7)=['CODE','METHOD','NCALLS','CHI2MODE','PIXELREP', $
                             'KEEPBEST','PIKAIA_STAT','PIKAIA_POP']
  iptgroup(10).default(0:7)=['FORTRAN','PIKAIA','200','0','1','1','NONE','0']
  iptgroup(10).comment( 0)='PIKAIA code to use. Available: FORTRAN (=fast) or IDL (=platform independent).'
  iptgroup(10).comment( 1)="minimization method: PIKAIA, POWELL (fast), LMDIF (fast) or PIK_LM (combined, for maps)"
  iptgroup(10).comment( 2)='number of iterations in PIKAIA routine / max. number of calls for POWELL or LMDIF'
  iptgroup(10).comment( 3)='method to calc. chi2 (JM = Borrero method, PLAIN = weight only depends on IQUV_WEIGHT values). Default: Weight depends on 1/signal strength'
  iptgroup(10).comment( 4)='number of repetitions per pixel (to perform statistical analysis)'
  iptgroup(10).comment( 5)='store result only if fitness is better than existing result (FITSOUT only)'
  iptgroup(10).nmipar([1,2])=1b
  iptgroup(10).comment( 6)='switch on and control statistic module based on PIKAIA populations. Requires FITSOUT=1 and PIXELREP=1. Example: POP75'
  iptgroup(10).comment( 7)='number of PIKAIA populations (0=automatic)'
end
