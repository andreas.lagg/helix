;unno solution for inclination angle as defined in Auer, 1977, Sol.Phys
function fit_inc_amoeba,inc
  common for_amoeba_inc,s1_hat,s3_hat,s13_weight

;sifit(4)=(sifit(4)>0)<0.5
  
  d1=abs(s1_hat)
  d2=abs((s3_hat)*(sin(inc(0))^2.0)/(2.0*cos(inc(0))))
  diff=total(s13_weight*abs(d1-d2))
  if diff lt 1e-6 then diff=0
  
;plot,s13_weight*d1 & oplot,color=1,s13_weight*d2  & wait,.1

; print,inc(0)/!dtor,diff(0)

return,diff
end
