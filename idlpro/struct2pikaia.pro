;convert structure variable defining the atmospheric parameters to the
;pikaia vector format and viceversa
function struct2pikaia,atm=atm,par=par,reverse=reverse,line=line,blend=blend, $
                       gen=gen,noscale=noscale,retname=retname,fitparname=fitparname
  common scale,scale
  common ff_info,nfz,ff_idx
  common couple,couple_idx ;contains indices for coupling of parameters
  common verbose,verbose
  
  nt=n_tags(atm.par)
  tn=tag_names(atm.par)
  na=n_elements(atm.par)
  ntl=n_tags(line.par)
  tnl=tag_names(line.par)
  nal=n_elements(line)
  ntb=n_tags(blend.par)
  tnb=tag_names(blend.par)
  nab=n_elements(blend)
  ntg=n_tags(gen.par)
  tng=tag_names(gen.par)
  nag=n_elements(gen)
  noscale=keyword_set(noscale)
  if keyword_set(retname) then begin
    parname=def_parname(tag_names(atm(0).par))
    linename=def_linename(tag_names(line(0).par))
    blendname=def_blendname(tag_names(blend(0).par))
    genname=def_genname(tag_names(gen(0).par))
  endif
  
  if keyword_set(reverse) then begin
    
    ifit=0
    for ia=0,na-1 do for it=0,nt-1 do if atm(ia).fit.(it) ne 0 then  begin
      if noscale eq 0 then $
        atm(ia).par.(it)= scale(ia).(it).min + $
        par(atm(ia).idx.(it)-1)*(scale(ia).(it).max-scale(ia).(it).min)* $
        atm(ia).ratio.(it) $
      else atm(ia).par.(it)=par(atm(ia).idx.(it)-1)
    endif
    for il=0,nal-1 do $
      for it=0,ntl-1 do if line(il).fit.(it) ne 0 then  begin
      if noscale eq 0 then $
        line(il).par.(it)= $
        line(il).scale.(it).min + $
        par(line(il).idx.(it)-1)*(line(il).scale.(it).max- $
                                  line(il).scale.(it).min) $
      else line(il).par.(it)=par(line(il).idx.(it)-1)
    endif
    for ib=0,nab-1 do for it=0,ntb-1 do if tnb(it) ne 'DUMMY' then $
      if blend(ib).fit.(it) ne 0 then  begin
      if noscale eq 0 then $
        blend(ib).par.(it)= blend(ib).scale.(it).min + $
        par(blend(ib).idx.(it)-1)*(blend(ib).scale.(it).max- $
                                   blend(ib).scale.(it).min) $
      else blend(ib).par.(it)=par(blend(ib).idx.(it)-1)
    endif
    for ig=0,nag-1 do for it=0,ntg-1 do if tng(it) ne 'DUMMY' then $
      if gen(ig).fit.(it) ne 0 then  begin
      if noscale eq 0 then $
        gen(ig).par.(it)= gen(ig).scale.(it).min + $
        par(gen(ig).idx.(it)-1)*(gen(ig).scale.(it).max- $
                                 gen(ig).scale.(it).min) $
      else gen(ig).par.(it)=par(gen(ig).idx.(it)-1)
    endif
    
                                ;normalize ff
    atm.par.ff=norm_ff(atm.par.ff,atm.fit.ff,atm.linid)
    retval=atm
  endif else begin              ;convert atm to pikaia par-list.
                                ;This is done only once, speed is not
                                ;so important
    
                                ;normalize ff
    atm.par.ff=norm_ff(atm.par.ff,atm.fit.ff,atm.linid);<scale.ff.max
    par=fltarr((atm(0).npar + line(0).npar + blend(0).npar + gen(0).npar)>1)

                                ;atmospheric parameters
    for ia=0,na-1 do for it=0,nt-1 do if atm(ia).fit.(it) ne 0 then begin
      if noscale eq 0 then begin
      par(atm(ia).idx.(it)-1)= ((atm(ia).par.(it) - scale(ia).(it).min) / $
                                (scale(ia).(it).max-scale(ia).(it).min))
      if noscale eq 0 then if (par(atm(ia).idx.(it)-1) lt 0 or $
          par(atm(ia).idx.(it)-1) gt 1) then begin
        message,/cont,'Initial value out of range: '+tn(it)+', '+ $
          n2s(par(atm(ia).idx.(it)-1))
        reset
      endif
    endif else begin
      par(atm(ia).idx.(it)-1)=atm(ia).par.(it) 
    endelse
    if keyword_set(retname) then $
      fitparname(atm(ia).idx.(it)-1)=parname.ipt(it)
    endif
    
    for il=0,nal-1 do $
      for it=0,ntl-1 do if line(il).fit.(it) ne 0 then begin
      if noscale eq 0 then begin
      par(line(il).idx.(it)-1)= $
        ((line(il).par.(it) - line(il).scale.(it).min) / $
         (line(il).scale.(it).max-line(il).scale.(it).min))
      if noscale eq 0 then if (par(line(il).idx.(it)-1) lt 0 or $
          par(line(il).idx.(it)-1) gt 1) then begin
        message,/cont,'Initial value out of range: '+tnl(it)
        reset
      endif
    endif else begin
      par(line(il).idx.(it)-1)=line(il).par.(it)
    endelse
    if keyword_set(retname) then $
      fitparname(line(il).idx.(it)-1)=linename.ipt(it)
    endif    
    
    for ib=0,nab-1 do for it=0,ntb-1 do if tnb(it) ne 'DUMMY' then  $
      if blend(ib).fit.(it) ne 0 then begin
      if noscale eq 0 then begin
      par(blend(ib).idx.(it)-1)= ((blend(ib).par.(it) - $
                                   blend(ib).scale.(it).min) / $
                                  (blend(ib).scale.(it).max- $
                                   blend(ib).scale.(it).min))
      if noscale eq 0 then if (par(blend(ib).idx.(it)-1) lt 0 or $
          par(blend(ib).idx.(it)-1) gt 1) then begin
        message,/cont,'Initial value out of range: '+tnb(it)
        reset
      endif
    endif else begin
      par(blend(ib).idx.(it)-1)=blend(ib).par.(it)
    endelse
    if keyword_set(retname) then $
      fitparname(blend(ib).idx.(it)-1)=blendname.ipt(it)
    endif
    for ig=0,nag-1 do for it=0,ntg-1 do if tng(it) ne 'DUMMY' then  $
      if gen(ig).fit.(it) ne 0 then begin
      if noscale eq 0 then begin
      par(gen(ig).idx.(it)-1)= ((gen(ig).par.(it) - $
                                   gen(ig).scale.(it).min) / $
                                  (gen(ig).scale.(it).max- $
                                   gen(ig).scale.(it).min))
      if noscale eq 0 then if (par(gen(ig).idx.(it)-1) lt 0 or $
          par(gen(ig).idx.(it)-1)  gt 1) then begin
        message,/cont,'Initial value out of range: '+tng(it)
        reset
      endif
    endif else begin
      par(gen(ig).idx.(it)-1)=gen(ig).par.(it)
    endelse
    if keyword_set(retname) then $
      fitparname(gen(ig).idx.(it)-1)=genname.ipt(it)
    endif
    retval=par
  endelse  
  return,retval
end
