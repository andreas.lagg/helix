function def_genname,tagnam
  
  gen_name=tagnam
  for it=0,n_elements(tagnam)-1 do case tagnam(it) of
    'CCORR': gen_name(it)='CCORR'
    else:
  endcase
  name={ipt:gen_name,idl:tagnam}
  return,name
end
