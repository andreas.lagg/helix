;INPUT: atmosphere, input file structure
;OUTOUT: profiel
;example:
;fitprof=get_stokes(result=pikaia_result,x=x,y=y,/show,verbose=2)
function get_stokes,result=result,x=x,y=y,show=show,verbose=verbose
  
  
  input=result.input
  write_input,'input/dummy.ipt',ipt=input
  input=read_ipt('input/dummy.ipt')
  
  xmin=min(result.fit.x)
  ymin=min(result.fit.y)
  
  nc=result.input.ncomp
  
  
  input.ncalls=0
  input.pixelrep=0
  
  xu=remove_multi(x)
  yu=remove_multi(y)
  
  for ix=0,n_elements(xu)-1 do for iy=0,n_elements(yu)-1 do begin
    ixy=where(result.fit.x eq xu(ix) and result.fit.y eq yu(iy))
    if ixy(0) eq -1 then  begin
      message,/cont,'Pixel '+n2s(fix([xu(ix),yu(iy)]),format='(i4.4)')+ $
        ' not found.'
    endif else begin
      input.atm(0:nc-1)=result.fit(ixy).atm
      input.gen=result.fit(ixy).gen
      input.blend=result.fit(ixy).blend
      for il=0,input.nline-1 do $
        input.line(il).par=result.fit(ixy).linepar(il)
      input.x=xu(ix)
      input.y=yu(iy)
      if keyword_set(verbose) then input.verbose=verbose
  
      if n_elements(show) eq 0 then show=1
      input.display_profile=keyword_set(show)
      helix,x=xu(ix),y=yu(iy),struct_ipt=input,fitprof=fitprof,/noresult
    endelse
  endfor
    
;  if keyword_set(show) then plot_profiles,fitprof,atm=input.atm
  
  return,fitprof
end
