;convert decimal number to binary
function conv2bin,dec
  
  maxval=max(dec)
  digits=fix(alog10(maxval)/alog10(2))+2
  retval=bytarr(digits,n_elements(dec))
  
  
  
  for i=0,n_elements(dec)-1 do begin
    tn=dec(i)
    cnt=0
    repeat begin
      retval(cnt,i)=tn mod 2
      tn=long(tn)/2
      cnt=cnt+1
    endrep until tn lt 1
  endfor
  
  return,retval
end
