restore,/v,'sav/fe_praktikant_gaussfit.x000.y000.fit.sav'
  prof=fitprof
;  prof=observation
  i=prof.i
  q=prof.q
  u=prof.u
  v=prof.v
  
  
  s1=u*cos(2*22.*!dtor)+q*sin(2*22.*!dtor)
  s1hat=s1
  s3hat=v
  r=2d*s1/v
  gamma=acos(0.5*(sqrt(r^2+4)-r))/!dtor 
  
  mxv=max(abs(v)) & print,gamma(!c)

  
  plot,gamma
  
  wgt= v*0
  wgt(135:190)=1.
    
  inc=0.5d
  fit = CURVEFIT(abs(v),abs(s1),wgt, inc, SIGMA, FUNCTION_NAME='sifit_inc',/NODERIVATIVE,/DOUBLE,ITER=it,itmax=300,CHISQ=chi,TOL=1d-10)
  print,chi
  
  plot,abs(s1) & oplot,color=1,fit
  print,it,inc/!dtor
  stop
end
