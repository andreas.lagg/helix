pro pol_diag,nr,ps=ps,connect=connect,color=color
  
  !p.multi=0
  psset,ps=ps,size=[18,15]
  
  if n_elements(color) eq 0 then color=1
  if color eq 0 then connect=1
  
  userlct,/full & erase
;  !p.multi=[0,1,3]
                                ;polarization diagram for Fig. 5.11 in Landi book
  
  sol_y=00.               ;observe at limb (90� scattering)
  sol_x=950.
  sol_rad=950.
                                ;all exampleS: slit_ori=180
  slit_ori=180.
  gamma=5.
  
  list=0
  repeat begin
    if n_elements(nr) eq 0 then begin
      print,'Plots Polarization diagrams for various special cases.'
      print,'Usage: pol_diag,nr    (nr = number of example)'
      print,'Keywords: '
      print,'/connect   - draw isocontour lines for B, azi, inc, sol-pos.'
      print,'color=0    - do not plot color symbols'
      print,'Available examples: '
      nr=1
      list=1
    endif
    case nr of 
      1: begin
        label='Landi-Book, example 5.12'
        inc=findgen(37)/36.*180
        sol_x=950. & sol_y=0.
        azi=0.
        alpha2=findgen(10)/9.*90.
        colvar='alpha2'
        b2los=1
      end
      2: begin
        label='Merenda et al., ApJ 2006: ''mag. fields in Polar Crown Prominence'', Fig. 6'
        sol_x=950. & sol_y=0.
        azi=findgen(37)/36.*180.-90
        inc=findgen(19)/18.*90
        alpha2=90
        colvar='azi'
        b2sol=1
      end
      3: begin
        label='disk center, horizontal B (Bommier et. al A&A 1991, Fig. 10)'
        sol_x=0. & sol_y=0.
        b2los=1
        inc=90.
        azi=findgen(73)/72.*180.
        alpha2=findgen(10)/9.*90.
        colvar='azi'
      end
      4: begin
        label='limb, B par to sol surface (Bommier et. al A&A 1991, Fig. 9)'
        sol_x=950. & sol_y=0.
        b2sol=1
        inc=90.
        azi=findgen(73)/72.*180.
        alpha2=findgen(20)/19.*90.
        colvar='azi'
      end
      5: begin
        label='disk center, horizontal B (Bommier et. al A&A 1991, Fig. 11)'
        sol_x=900. & sol_y=-100.
        b2sol=1
        inc=30.
        azi=findgen(19)/18.*180.
        alpha2=findgen(20)/19.*90.
        colvar='azi'
;        colvar='alpha2'
        slit_ori=90
      end
      6: begin
        label='Merenda et al., ApJ 2006: not quite Fig. 5'
        sol_x=950. & sol_y=0.
        b2sol=1
        inc=90.
        azi=findgen(10)/9.*180.
        alpha2=findgen(20)/19.*90.
        colvar='azi'
;        colvar='alpha2'
      end
      7: begin
        label=''
        lat=findgen(37)/36*!pi
        lon=30.*!dtor
        sol_y=sol_rad*cos(lat)
        sol_x=sol_rad*sin(lat)*sin(lon)
        b2sol=1
        inc=30.
        azi=0.;findgen(37)/36.*360.
        alpha2=80.
        colvar='sol_x'
;        colvar='alpha2'
      end
      else: reset
      
    endcase
    if list then begin
      print,nr,' ... ',label,format='(i2,a,a)'
      nr=nr+1
    endif
  endrep until list eq 0
  
                                ;calculate B or alpha2
  if n_elements(B) eq 0 then B=tan(alpha2*!dtor)/2.*gamma/0.879
  if n_elements(alpha2) eq 0 then alpha2=atan(2*0.879*B/gamma)
  
  ni=n_elements(inc)
  na=n_elements(azi)
  nb=n_elements(b)
  ns=n_elements(sol_x)
  
  beta=inc*!dtor
  alprad=alpha2*!dtor
  
  case colvar of
    'inc': ivar='ii'
    'azi': ivar='ia'
    'sol_x': ivar='is'
    'sol_y': ivar='is'
    'b': ivar='ib'
    'alpha2': ivar='ib'
    else: message,'unknown colvar.'
  endcase

  
  dummy=execute('cv='+colvar)
  image_cont_al,/nodata,dist(2,2),xrange=[-1,1],yrange=[-1,1], $
    zrange=[min(cv,/nan),max(cv,/nan)],/aspect,ztitle=colvar, $
    title=label,xtitle='U/I',ytitle='Q/I'
  
;  plot,/nodata,[0,1],xrange=[-1,1],yrange=[-1,1],title=label
  qs=fltarr(ns,ni,nb,na)
  us=fltarr(ns,ni,nb,na)
  for is=0,ns-1 do begin
    hanle,0,0,0,sol_x(is),sol_y(is),slit_ori,sol_rad,r,/init
    for ii=0,ni-1 do begin
      for ib=0,nb-1 do begin
        for ia=0,na-1 do begin
          hanle,b(ib),inc(ii),azi(ia),sol_x(is),sol_y(is),slit_ori,sol_rad,r, $
            b2los=b2los,b2sol=b2sol
          qi=r(1,0)/r(0,0)
          ui=r(2,0)/r(0,0)
          qs(is,ii,ib,ia)=qi
          us(is,ii,ib,ia)=ui
          
          if keyword_set(connect) then begin
            if is gt 0 then $
              plots,linestyle=3,us(is-1:is,ii,ib,ia),qs(is-1:is,ii,ib,ia)
            if ia gt 0 then $
              plots,linestyle=1,us(is,ii,ib,ia-1:ia),qs(is,ii,ib,ia-1:ia)
            if ii gt 0 then $
              plots,linestyle=2,us(is,ii-1:ii,ib,ia),qs(is,ii-1:ii,ib,ia)
            if ib gt 0 then $
              plots,linestyle=0,us(is,ii,ib-1:ib,ia),qs(is,ii,ib-1:ib,ia)
          endif
          
          if color eq 1 then begin
            dummy=execute('ic='+ivar)
            plots,ui,qi,psym=1, $
              color=(cv(ic)-min(cv,/nan))/(max(cv,/nan)-min(cv,/nan))*252+1
          endif
          
          
                                ;analytical calc. for example 5.12
;      i=3-cos(beta(ii))^2-sin(beta(ii))^2*cos(alprad(ib))^2
;      qic=(sin(beta(ii))^2+(1+cos(beta(ii))^2)*cos(alprad(ib))^2)/i
;      uic=2*cos(beta(ii))*sin(alprad(ib))*cos(alprad(ib))/i
;     plots,uic,qic,psym=4, $
;       color=(alpha2(ib)-min(alpha2))/(max(alpha2)-min(alpha2))*254
        endfor
      endfor
      print,'*',format='(a,$)'
    endfor
  endfor
  print
  oplot,!x.crange,[0,0],linestyle=1
  oplot,[0,0],!y.crange,linestyle=1
  
;  image_cont_al,ui,zrange=[-1.,1.],title='U/I',xrange=[min(inc),max(inc)],yrange=[min(B),max(B)],xtitle='INC',ytitle='B',ytype=1
;  image_cont_al,qi,zrange=[-1.,1.],title='Q/I',xrange=[min(inc),max(inc)],yrange=[min(B),max(B)],xtitle='INC',ytitle='B',ytype=1
;  tvscl,uic & tvscl,ui,(size(uic))(1)+5,0
;  tvscl,qic,0,(size(uic))(2)+5 & tvscl,qi,(size(uic))(1)+5,(size(uic))(2)+5
  
  psset,/close
end
