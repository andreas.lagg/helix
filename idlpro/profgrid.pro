pro profgrid
  
  file=['grid_2C','grid_1C']
  bval=[500,0]
  
  for i=0,n_elements(file)-1 do for ip=0,1 do begin
    if ip eq 0 then add='_nopsf' else add='_psf'
    iptfile='synth_fe15648.ipt'
    ifile=iptfile
    tipt=read_ipt(ifile)
    grid=[7,5]
    gx2=(grid[0]-1)/2.
    gy2=(grid[1]-1)/2.
    rad=sqrt(gx2^2+gy2^2)
    ifile=iptfile
    helix,ipt=ifile,fitprof=fp
    tipt.atm[1].par.b=bval[i]
    prof=replicate(fp,grid[0],grid[1])
    alpha=fltarr(grid[0],grid[1])
    for ix=0,grid[0]-1 do for iy=0,grid[1]-1 do begin
      alpha[ix,iy]=abs(1-sqrt((ix-gx2)^2+(iy-gy2)^2)/rad)<1
      if ip eq 0 then alpha[ix,iy]=ix eq gx2 and iy eq gy2
      tipt.atm[0:1].par.ff=[alpha[ix,iy],1.-alpha[ix,iy]]
      ifile=iptfile
      helix,ipt=ifile,fitprof=fp,struct_ipt=tipt
      prof[ix,iy]=fp
    endfor
    p
    psset,ps=1,size=[16,8],/pdf,file='./ps/'+file[i]+add+'.ps',/no_x
    !p.multi=0
    border=0.0 & sep=0.0
    for ix=0,grid[0]-1 do for iy=0,grid[1]-1 do begin
      dx=(1-2*border-(grid[0]-1)*sep)/grid[0]
      dy=(1-2*border-(grid[1]-1)*sep)/grid[1]
      px=border+ix*dx
      py=border+iy*dy
      color=1
      if abs(alpha[ix,iy]-1) le 1e-3 then color=2
      if abs(alpha[ix,iy]-0) le 1e-3 then color=3
      plot_profiles,prof[ix,iy],position=[px,py,px+dx,py+dy],color=color, $
                    titx='',tity='',range=[-.35,.35]
    endfor
    psset,/close
  endfor
end
