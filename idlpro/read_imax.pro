function read_imax,datnam,x=xveco,y=yveco,error=error,lsprof=lsprof, $
                   norm_cont=norm_cont,do_ls=do_ls
  common imaxinfo,imaxinfo,imaxdata,imaxheader,imaxwl
  common input,ipt
  
   
  error=1
  
  xvec=xveco
  if n_elements(xvec) eq 1 then xvec=[xvec,xvec]
  yvec=yveco
  if n_elements(yvec) eq 1 then yvec=[yvec,yvec]
  
                                ;check if file exists
  fi=file_info(datnam)
  if fi.exists eq 0 then begin
    message,/cont,'No IMaX 4D data file found: '+datnam
    reset
  endif
  
  if n_elements(imaxinfo) eq 0 then $
    imaxinfo={file:'',nx:0,ny:0,nwl:0,nstokes:0,icidx:-1,imgcont:0.}
  
  reread=imaxinfo.file ne datnam
  if reread then begin
    imaxdata=readfits(datnam,imaxheader)
    sz=size(imaxdata)
    imaxinfo.nx=sz(1)
    imaxinfo.ny=sz(2)
    imaxinfo.nwl=sz(3)
    imaxinfo.nstokes=sz(4)
    case imaxinfo.nwl of
      5: begin ;V5-6 data
        imaxwl=[-80., -40., +40., +80., +227.]/1e3 + 5250.208d
        imaxinfo.icidx=4        ;index for cont-level
      end
      5: begin ;V8-4 data
        imaxwl=[-120.,-80., -40., +40., +80.,+120., +227.]/1e3 + 5250.208d
        imaxinfo.icidx=7        ;index for cont-level
      end
      12: begin ;L12-2 data
        imaxwl=(dindgen(12)*35./1000.-0.1925d0) + 5250.208d
        imaxinfo.icidx=0        ;index for cont-level
      end
      else: begin
        message,'IMaX data must contain 5, 8 or 12 WL positions.'
        reset
      end
    endcase
    imaxinfo.file=datnam
                                ;take average over 10% brightest
                                ;profiles as image continuum
    icont=imaxdata(*,*,imaxinfo.icidx,0)
    imgcont=icont(reverse(sort(icont)))
    imgcont=imgcont(0:n_elements(imgcont)*0.10)
    imaxinfo.imgcont=total(imgcont)/n_elements(imgcont)
  endif
  
  vec=fltarr(imaxinfo.nwl)
  observation={ic:0.,wl:imaxwl,i:vec,q:vec,u:vec,v:vec}
  
  cnt=0
  for ix=xvec(0),xvec(1) do for iy=yvec(0),yvec(1) do begin
    if (ix ge 0 and ix lt imaxinfo.nx and $
        iy ge 0 and iy lt imaxinfo.ny) then begin
      observation.ic=observation.ic+imaxdata(ix,iy,imaxinfo.icidx,0)
      observation.i=observation.i+imaxdata(ix,iy,*,0)
      if imaxinfo.nwl eq 5 then begin ;V5-6 data
        observation.q=observation.q+imaxdata(ix,iy,*,1)
        observation.u=observation.u+imaxdata(ix,iy,*,2)
        observation.v=observation.v+imaxdata(ix,iy,*,3)
      endif else begin ;L12-2 data
        observation.v=observation.v+imaxdata(ix,iy,*,1)
        print,'WARNING: IMaX L12-2 Stokes V divided by 0.55!!'
        observation.v=observation.v/0.55
      endelse
      cnt=cnt+1
    endif
  endfor
  if cnt eq 0 then begin
    print,'Could not find XPOS=('+n2s(xvec(0))+','+n2s(xvec(1))+ $
      ') and YPOS=('+n2s(yvec(0))+','+n2s(yvec(1))+') in data file '+datnam
    reset
  endif
  
  
  if ipt.norm_cont eq 0 then icont=observation.ic $
  else icont=imaxinfo.imgcont*cnt

;  observation.ic=observation.ic/cnt
  observation.ic=icont/cnt
  observation.i=observation.i/icont
  observation.q=observation.q/icont
  observation.u=observation.u/icont
  observation.v=observation.v/icont
  
  lsprof=get_lsprof(datnam,xvec,yvec,observation.wl, $
                    n_elements(observation.wl), $
                    ipt=ipt,mode=3,do_ls=do_ls)
  
;  lsprof.i=lsprof.i*icnt/icont
  error=0
  return,observation
  
end
