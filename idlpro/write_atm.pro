pro par_write,unit,nameipt,nameidl,par,fit,ipt=ipt

  val=0
  for ip=0,n_elements(ipt.parset)-1 do begin
    if ipt.parset(ip) eq nameidl then val=1
  endfor

  if val  then begin
    nstr=n2s(ipt.ncomp)
     printf,unit,format='(a10,'+nstr+'(f16.5),'+nstr+'(i5))', $
       nameipt,par(0:ipt.ncomp-1),fit(0:ipt.ncomp-1)
  endif
end

pro linpar_write,unit,name,par,fit,ipt=ipt

  nstr=n2s(ipt.ncomp)
  printf,unit,format='(a16,'+nstr+'(f16.5),'+nstr+'(i5))',name,(par),(fit)
end

pro write_atm,atm=atm,line=line,blend=blend,gen=gen,fitness=fitness, $
              fitprof=fitprof, $
              resultname=resultname,xyinfo=xyinfo,error=error,ipt=ipt
  
  error=0
  nline=n_elements(line)
  natm=n_elements(atm)
  
  file=resultname
  path=strmid(file,0,strpos(file,'/',/reverse_search)+1)
  file_atm =path+xyinfo+'_atm.dat'
  file_prof=path+xyinfo+'_profile.dat'
  
;  spawn,'mkdirhier '+path,output,error
  file_mkdir,path
  fi=file_info(path)
  error=fi.exists eq 0 or fi.directory eq 0
  if error(0) ne '' then begin
    message,/cont,'Could not create directory '+path
    message,/cont,'Profile '+file_prof+' not written'
    message,/cont,'Atmosphere '+file_prof+' not written'
    print,error
    error=1
    return
  endif
  
  sep=strjoin(replicate('-',78))
  big_ben=systime(0)
  openw,unit,/get_lun,file_atm,error=ierr2
  if ierr2 ne 0 then begin
    message,/cont, 'Error writing atmosphere to file '+file_atm
    message,/cont, 'Please check directory structure.'
    message,/cont, 'Result NOT STORED!!!'
    error=1
    return
  endif
                                ;header:
  printf,unit, resultname
  printf,unit, 'Date: '+big_ben
  printf,unit, 'Input File: '+ipt.file
  printf,unit, 'Observation: '+ipt.observation
  printf,unit,format='(a,f12.4)','Fitness: ',fitness
  case fix(ipt.modeval) of
    0: printf,unit, 'MODE: GAUSS'
    1: printf,unit, 'MODE: VOIGT'
    2: printf,unit, 'MODE: VOIGT_PHYS'
    3: printf,unit, 'MODE: VOIGT_GDAMP'
    4: printf,unit, 'MODE: VOIGT_SZERO'
    5: printf,unit, 'MODE: HANLE_SLAB'
  endcase
  
  printf,unit,format='(a,'+n2s(ipt.nmi)+'i6)','NCALLS: ',ipt.ncalls(1:ipt.nmi)
  printf,unit,format='(a,i2)','NCOMP: ',ipt.ncomp
  printf,unit,sep
  parname=def_parname(tag_names(ipt.atm.par))
  for ip=0,n_elements(parname.ipt)-1 do $
    par_write,unit,parname.ipt(ip),parname.idl(ip), $
    atm.par.(ip),atm.fit.(ip),ipt=ipt
  printf,unit,sep
  
                                ;blend parameters
  blendname=def_blendname(tag_names(ipt.blend.par))
  if max(blend.par.a0) gt 0 then begin
    printf,unit,sep
    printf,unit,format='(a)','Blend Parameters:'
    printf,unit,format='(a,i3)','NBLEND:',ipt.nblend
    printf,unit,sep
    nbl=ipt.nblend
    nstr=n2s(nbl)
    printf,unit,format='(a12,'+nstr+'(f16.5),'+nstr+'(i5))', $
      'BLEND_WL',blend(0:nbl-1).par.wl,blend(0:nbl-1).fit.wl
    printf,unit,format='(a12,'+nstr+'(f16.5),'+nstr+'(i5))', $
      'BLEND_WIDTH',blend(0:nbl-1).par.width,blend(0:nbl-1).fit.width
    printf,unit,format='(a12,'+nstr+'(f16.5),'+nstr+'(i5))', $
      'BLEND_DAMP',blend(0:nbl-1).par.damp,blend(0:nbl-1).fit.damp
    printf,unit,format='(a12,'+nstr+'(f16.5),'+nstr+'(i5))', $
      'BLEND_A0',blend(0:nbl-1).par.a0,blend(0:nbl-1).fit.a0
  endif
  
  genname=def_genname(tag_names(ipt.gen.par))
  if (gen.fit.ccorr ne 0 or gen.fit.straylight ne 0 or $
      gen.fit.radcorrsolar ne 0) then begin
    printf,unit,sep
    printf,unit,format='(a)','General Parameters:'
    printf,unit,sep
    printf,unit,format='(a14,(f16.5),(i5))', $
      'CCORR',gen.par.ccorr,gen.fit.ccorr
    printf,unit,format='(a14,(f16.5),(i5))', $
      'STRAYLIGHT',gen.par.straylight,gen.fit.straylight
    printf,unit,format='(a14,(f16.5),(i5))', $
      'RADCORRSOLAR',gen.par.radcorrsolar,gen.fit.radcorrsolar
  endif

                                ;line parameters
  printf,unit,format='(a)','Line Parameters:'
  printf,unit,format='(a,i3)', 'NLINE:',nline
  nofit=intarr(ipt.ncomp)
  nc1=ipt.ncomp-1
  for i=0,ipt.nline-1 do begin
    printf,unit,sep
    printf,unit,format='(20a)',string(line(i).id)
    linpar_write,unit,'VLOS',line(i).straypol_par(0:nc1).vlos,nofit,ipt=ipt
    linpar_write,unit,'WIDTH',line(i).straypol_par(0:nc1).width,nofit,ipt=ipt
    linpar_write,unit,'DAMP',line(i).straypol_par(0:nc1).damp,nofit,ipt=ipt
    linpar_write,unit,'DOPP',line(i).straypol_par(0:nc1).dopp,nofit,ipt=ipt
    linpar_write,unit,'STRAYPOL_AMP', $
      line(i).par.straypol_amp,line(i).fit.straypol_amp,ipt=ipt
    linpar_write,unit,'STRAYPOL_ETA0', $
      line(i).par.straypol_eta0,line(i).fit.straypol_eta0,ipt=ipt
    linpar_write,unit,'STRENGTH', $
      line(i).par.strength,line(i).fit.strength,ipt=ipt
    linpar_write,unit,'WLSHIFT', $
      line(i).par.wlshift,line(i).fit.wlshift,ipt=ipt
  endfor
  free_lun,unit
  if (ipt.verbose ge 1) then  print,'Atmosphere written to '+file_atm
  
  if ipt.save_fitprof eq 1 then $
    write_stopro,file=file_prof,profile=fitprof,wlref=line(0).wl, $
    header=ipt.observation+' '+xyinfo+' '+ipt.file+' '+big_ben,ipt=ipt

;!!!!!!!!!!!!!!!!!!!! profile in stopro format
;   openw,unit,/get_lun,file_prof,error=ierr2
;   if ierr2 ne 0 then begin
;     message,/cont, 'Error writing profile to file '+trim(file_prof)
;     message,/cont, 'Please check directory structure.'
;     message,/cont, 'Result NOT STORED!!!'
;     error=1
;     return
;   endif

;   nprof=1
;   printf,unit,format='(i3,2x,a1,''WIVQU'',a1,3x,a,1x,a,1x,a,1x,a)', $
;     nprof,'''','''',header
;   idstr=" "
;   nwl=n_elements(fitprof.wl)
;   wlref=line(0).wl
;   printf,unit,format='(i5,f16.4,i6,E16.5,''  |  '',4i5,3x,a1,a,a1)', $
;     nprof,wlref,nwl,fitprof.ic,1,nwl,1,nwl,'''',idstr,''''
;   for i=0,nwl-1 do begin
;     printf,unit,format='(5E15.6)', $
;       fitprof.wl(i)-wlref,fitprof.i(i),fitprof.v(i),fitprof.q(i),fitprof.u(i)
;   endfor          
;   free_lun,unit
;   if (ipt.verbose ge 1) then print, 'Fit-Profile written to '+file_prof
  
end
