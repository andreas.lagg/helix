;program to join two sav files of different size, different observations, but ruin with same input file
pro join_sav,sav1,sav2,savout,gap=gap
  
  if n_params() eq 0 then begin
    print,'routine to combine 2 sav files created by make_sav'
    print,'usage:'
    print,'join_sav,''file1.sav'',''file2.sav'',''fileout.sav''[,gap=2]'
    reset
  endif
  
  restore,/v,sav1
  pr1=temporary(pikaia_result)
  
  restore,/v,sav2
  pr2=temporary(pikaia_result)
  
  sz1=size(pr1.fit)
  sz2=size(pr2.fit)
  
                                ;change x-pixel numbering
  if n_elements(gap) eq 0 then gap=0
  pr2.fit.x=pr2.fit.x+sz1(1)+gap
  
  nx=sz1(1)+sz2(1)+gap
  ny=max([sz1(2),sz2(2)])

  ipt=pr1.input
  header=pr1.header
  pikaia_result=result_struc(nx=nx,ny=ny,ipt=ipt,header=header)
 
  new=pikaia_result.fit(0:sz1(1)-1,0:sz1(2)-1)
  struct_assign,pr1.fit,new
  pikaia_result.fit(0:sz1(1)-1,0:sz1(2)-1)=new
  
  new=pikaia_result.fit(sz1(1)+gap:sz1(1)+sz2(1)-1+gap,0:sz2(2)-1)
  struct_assign,pr2.fit,new
  pikaia_result.fit(sz1(1)+gap:sz1(1)+sz2(1)-1+gap,0:sz2(2)-1)=new
  pikaia_result.input.x(1)=max(pr2.fit.x)
  
  for ig=0,gap-1 do $
    pikaia_result.fit(sz1(1)+ig,*).x=pikaia_result.fit(sz1(1)-1,*).x+ig+1
  
  
  new=pikaia_result.line
  struct_assign,pr1.line,new
  pikaia_result.line=new
  
  save,/compress,/xdr,file=savout,pikaia_result
  print,'Wrote '+savout
  stop
end
