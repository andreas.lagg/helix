function resize,img,xc,yc
  
  if (size(img))(0) gt 2 then  timg=reform(img) else timg=img
  
  sz=size(timg)
  nxc=n_elements(xc)
  nyc=n_elements(yc)
  fnew=fltarr((nxc-1)>1,(nyc-1)>1)
  fnew(0,0)=img(0,0)
  for ix=0,nxc-2 do for iy=0,nyc-2 do begin
;    xci=where(xc eq xc(ix))
;    for iy=0,nyc-2 do begin
;      yci=where(yc eq yc(iy))
      if xc(ix) ge 0 and xc(ix) lt sz(1) and $
        yc(iy) ge 0 and yc(iy) lt sz(2) then $
        fnew(ix,iy)=timg(xc(ix),yc(iy))
;    endfor
  endfor
  
  return,fnew
end

