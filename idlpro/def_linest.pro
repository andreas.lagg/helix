function def_linest
  @common_maxpar
  
  maxpar
  mm={min:0.,max:0.}
  fit={straypol_amp:0,straypol_eta0:0,strength:0,wlshift:0}
  par={straypol_amp:0.,straypol_eta0:0.,strength:1.,wlshift:0.}
  scale={straypol_amp:mm,straypol_eta0:mm,strength:mm,wlshift:mm}
                                ;structure to hold hanle information
                                ;from hanleprof.mod
  hanle={is2:0l,nterm:0l,ntrans:0l, $
         ntm:lonarr(30),nts:lonarr(30),lsto2:lonarr(30),adummy2:lonarr(3), $
         energy:dblarr(30,21), $
         ntlsto:lonarr(50),ntusto:lonarr(50), $
         aesto:dblarr(50),fsto:dblarr(50), $
         u1sto:dblarr(50),u2sto:dblarr(50), $
         riosto:dblarr(50),wlsto:dblarr(50)}
         
  nnp=dblarr(maxpi)
  nns=dblarr(maxsigma)
  quan={n_pi:0,n_sig:0,dummy:intarr(2), $
        nub:nns,nur:nns,web:nns,wer:nns,nup:nnp,wep:nnp}
  
  parst={fit:0,val:0.,par:0}
  strst={vlos:0.,width:0.,damp:0.,dopp:0.,sign:0,dummy:0} 
  
  pb={nr_zl:0,nr_b:0,dummy:intarr(2), $
      splitting:dblarr(max_pb_zl,max_pb_nr), $
      strength:dblarr(max_pb_zl,max_pb_nr), $
      cr:fltarr(maxsigma,4),cb:fltarr(maxsigma,4), $
      dr:fltarr(maxsigma,4),db:fltarr(maxsigma,4), $
      cp:fltarr(maxpi,4),dp:fltarr(maxpi,4), $
      b:fltarr(max_pb_nr)}
  
  lst={ $
        wl:0d,geff:0.,f:0.,loggf:-20.,abund:0.,icont:1., $
        ju:0.,jl:0., $
        lu:0.,ll:0., $
        su:0.,sl:0., $
        gu:0.,gl:0., $
        mass:0., $
        fit:fit, $
        idx:fit, $
        par:par, $
        scale:scale, $
        straypol_par:replicate(strst,maxatm), $
;        dummy1:0, $            ;alignment parameter
        quan:quan, $
        straypol_use:1, $      ;flag if straypol is used for this line
        npar:0, $
        ion:0, $
        use_hanle_slab:0, $     ;flag to use hanle slab model 
;        dummy2:intarr(2), $     ;alignment parameter
        id:bytarr(idlen), $
        dummy3:intarr(2), $     ;alignment parameter
        pb:pb, $
        hanle:hanle $
      }   
  
  return,lst
end
