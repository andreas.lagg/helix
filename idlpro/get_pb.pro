pro caller
  
  he3=def_line(line='He',verbose=1)
  
  help,/st,he3               ;information on the line structure
  
  get_pb,b=500.,line=he3(0),splitting=sp,strength=st,/zeeman
  
  print,'Splitting=',sp
  print,'Strength=',st
  
end

;input: b, line
;output: splittng (in A), strength
pro get_pb,b=b,line=line,splitting=splitting,strength=strength,zeeman=zeeman,pb=pb
  
  
  zeeman_shift= 4.6686411E-13 * line.wl^2.*line.geff * B
  
  if keyword_set(zeeman) then begin
    splitting=[-zeeman_shift,0,zeeman_shift]
  endif
  
end
