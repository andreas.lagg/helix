pro test
  
  sol_x=670d                    ;solar-x position in arcsec
  sol_y=670d                    ;solar-y position in arcsec
  
  B=500000000.              ;magnetic field vector in reference system
  inc=30.
  azi=30.
  sol_rad=950.                  ;solar radius for time of obs.
  slit_ori=180.                 ;slit orientation of TIP instrument.
                                ;180� means terrestrial North
  
  hanle,b,inc,azi,sol_x,sol_y,slit_ori,sol_rad,r
  
  print,float(r)
end

pro hanle_init,sol_x,sol_y,slit_ori,sol_rad
  common hanle,gamma,om,ea,eb,omd,ead,ebd
  
  
  p=20.                  ;position angle of solar pole (not used here)
  b0=0.                       ;latitude of disk center (not used here)
  
  
  gamma=5.                      ;lifetime of transition in 10^7 s
                                ;see eq 5.85 in Landi book
  
                                ;coordinate systems: see fig 5.9 in
                                ;Landi Deg'l Innocenti: Polarization
                                ;in Spectral Lines (Kluwer, ASSL Vol 307)
  
                                ;coordinate system "reference":
                                ;without loosing generality we use the
                                ;'system omega' as our reference
                                ;system. This means that all unit
                                ;vectors and the magnetic field vector
                                ;must be written in this coord system.
  
                                ;system omega: to the observer
                                ;+z = LOS direction from sun to observer
                                ;+y = direction of positive Q
                                ;     (=slit direction for TIP obs.)
                                ;+x = crossp(y,z)
                                ;We define the vector omega to be
                                ;[0,0,1]. This makes the
                                ;interpretation of the b-vector easy
                                ;since inc and azi are defined in the
                                ;LOS frame
  om=[0.,0.,1.]                 ;vector omega
  eb=[0.,1.,0.]                 ;vector 2
  ea=crossp(eb,om)              ;vector 1
  
                                ;system omega': incident radiation
                                ;+z = solar surface normal (away from sun)
                                ;+y = parallel to limb
                                ;+x = crossp(y,z)
                                ;the unit vectors for this system are
                                ;calculated from the solar x and y
                                ;position. The x and y directions
                                ;should not be important since the
                                ;illumination from the sun is assumed
                                ;to be uniform.
                                ;The invariance of the result with
                                ;respect to changes in x, y direction
                                ;has to be verified! TBD!
  
                                ;first rotate around slit orientation
                                ;(sign TBC!)
                                ;-180 is TIP specific since
                                ;slit_ori=180 means terrestrial North
                                ;The coord system is such that azi=0
                                ;is perp to the slit. Therefore the
                                ;direction omega' has to be adapted
  sp=sin((slit_ori-180+90)*!dtor) & cp=cos((slit_ori-180.+90)*!dtor)
  sxy=[[cp,sp],[-sp,cp]] ## [sol_x,sol_y]
                                ;first calculate the vectors with
                                ;respect to disk center:
                                ;vector omega':
  omd_c=norm([sxy(0),sxy(1),sqrt((sol_rad^2-sxy(0)^2-sxy(1)^2)>0)]/sol_rad)
  if total(abs(sxy)) le 1e-10 then ebdir=[0.,1.,0.] $
  else ebdir=norm([-sxy(1),sxy(0),0])
  ead_c=norm(crossp(omd_c,ebdir)) ;vector 4
  ebd_c=crossp(ead_c,omd_c)     ;vector 3
;print,omd_c,ead_c,ebd_c & stop  
  
                                ;These vectors must be rotated since
                                ;the LOS vector (omega=[0,0,1]) does
                                ;not point to disk center -> rotation
                                ;around x and y with negative angles
  sinxy=sin(-sxy/3600.*!dtor) & cosxy=sqrt(1-sinxy^2)
  rotx=[[1,0,0],[0,cosxy(0),sinxy(0)],[0,-sinxy(0),cosxy(0)]]
  roty=[[cosxy(1),0,sinxy(1)],[0,1,0],[-sinxy(1),0,cosxy(1)]]
  rot=rotx ## roty
  omd=(rot ## omd_c)(*)
  ead=(rot ## ead_c)(*)
  ebd=(rot ## ebd_c)(*)
end

;inputs:
;sol_x                          solar-x position in arcsec
;sol_y                          solar-y position in arcsec
;B                              magnetic field vector in reference system [G]
;inc                            in degrees  
;azi                            in degrees  
;sol_rad                        solar radius for time of obs. [arcsec]
;outputs:
;R                              rayleigh phase matrix (eq 5.92 Landi book)
;slit_ori                       ;slit orientation of TIP instrument in degrees
                                ;180� means terrestrial North
;keywords:
;/init                          ;necessary for first run at a given
                                ;solar location. calculates unit
                                ;vectors
;/b2sol, /b2los                 ;specifies the coordinate system for
                                ;the magnetic field vector (LOS (=def.) or
                                ;normal to solar surface)
pro hanle,b,inc,azi,sol_x,sol_y,slit_ori,sol_rad,r,init=init, $
          b2los=b2los,b2sol=b2sol
  common hanle,gamma,om,ea,eb,omd,ead,ebd
  
;  if keyword_set(init) then $
    hanle_init,sol_x,sol_y,slit_ori,sol_rad
  
  H=0.879*B/gamma   
  
                                ;calculate vector u_gamma (used for
                                ;direction cosines):
                                ;(see page 158 of Landi-book, see fig 5.1)
                                ;since we chose the reference system
                                ;to be the LOS system "omega" we can
                                ;directly use the inc and azimuth
                                ;values from the magnetic field to
                                ;calculate u0 and u+-1
  cinc=cos(inc*!dtor) & sinc=sin(inc*!dtor)
  cazi=cos(azi*!dtor) & sazi=sin(azi*!dtor)
  uvec=complexarr(3,3)          ;u_-1= uvec(*,0)
                                ;u_0 = uvec(*,1)
                                ;u_+1= uvec(*,2) 
  if keyword_set(b2sol) then begin
                                ;b with respect to solar surface normale
    uvec(*,1)=sinc*cazi*ead + sinc*sazi*ebd + cinc*omd ; u_0
    uvec(*,2)=1./sqrt(2.)*((-cinc*cazi-complex(0,1)*sazi)*ead + $ ;u_+1
                           (-cinc*sazi+complex(0,1)*cazi)*ebd + $
                           (+sinc*omd))
    uvec(*,0)=1./sqrt(2.)*((+cinc*cazi-complex(0,1)*sazi)*ead + $ ;u_-1
                           (+cinc*sazi+complex(0,1)*cazi)*ebd + $
                           (-sinc*omd))
  endif else begin
    uvec(*,1)=sinc*cazi*ea + sinc*sazi*eb + cinc*om ; u_0
    uvec(*,2)=1./sqrt(2.)*((-cinc*cazi-complex(0,1)*sazi)*ea + $ ;u_+1
                           (-cinc*sazi+complex(0,1)*cazi)*eb + $
                           (+sinc*om))
    uvec(*,0)=1./sqrt(2.)*((+cinc*cazi-complex(0,1)*sazi)*ea + $ ;u_-1
                           (+cinc*sazi+complex(0,1)*cazi)*eb + $
                           (-sinc*om))
  endelse
  
;  print,[float(omd ## transpose(uvec(*,1))),float(omd ## transpose(om)),float(om  ## transpose(uvec(*,1)))]
  
                                ;calculation of direction cosines
                                ;eq 5.77 in Landi book
  cab=complexarr(3,2)           ;C_gamma_i
  cabd=complexarr(3,2)          ;C'_gamma_i ('d' for dash)
  for ia=0,2 do begin
    cab(ia,0)=uvec(*,ia) ## transpose(conj(ea))
    cab(ia,1)=uvec(*,ia) ## transpose(conj(eb))
    cabd(ia,0)=uvec(*,ia) ## transpose(conj(ead))
    cabd(ia,1)=uvec(*,ia) ## transpose(conj(ebd))
  endfor
  ccab=conj(cab)
  ccabd=conj(cabd)
  
  
  R=complexarr(4,4)             ;phase matrix
  sigma=complexarr(4,2,2)       ;pauli spin matrices
  sigma(0,*,*)=[[1,0],[0,1]] 
  sigma(1,*,*)=[[1,0],[0,-1]] 
  sigma(2,*,*)=[[0,1],[1,0]]   
  sigma(3,*,*)=[[0,complex(0,-1)],[complex(0,1),0]]   
  
                                ;calculate matrices to avoid loops
  alpha=indgen(3)-1 & beta=indgen(3)-1
  one=[1,1,1]
  amb=(transpose(alpha) ## one) - (transpose(one) ## beta)
  
                                ;calculate phase matrix
  for i=0,3 do for j=0,3 do begin
    rij=complex(0.)
    rklmn=complexarr(2,2,2,2)
    for k=0,1 do for l=0,1 do for m=0,1 do for n=0,1 do begin
      rij=rij + (reform(sigma(i,*,*)))(l,k)*(reform(sigma(j,*,*)))(m,n)* $
        total(((transpose(ccab(*,k))##cab(*,l)) * $
               (transpose(cabd(*,m))##ccabd(*,n))*1./ $
               (1. + complex(0,1)*amb*H)))
;       rij=rij + (reform(sigma(i,*,*)))(l,k)*(reform(sigma(j,*,*)))(m,n)* $
;         total(((transpose(ccab(*,k))##cab(*,l)) * $
;                (transpose(cabd(*,m))##ccabd(*,n))*1./ $
;                (1. + complex(0,1)*amb*H)))
      
;         total(((transpose(ccab(*,k))##one)*(transpose(one)##cab(*,l)) * $
;                (transpose(cabd(*,m))##one)*(transpose(one)##ccabd(*,n))*1./ $
;                (1. + complex(0,1)*amb*H)))
      
;       for ia=0,2 do for ib=0,2 do begin
;         alpha=ia-1 & beta=ib-1
;         rij=rij +((reform(sigma(i,*,*)))(l,k)*(reform(sigma(j,*,*)))(m,n)* $
;                   ccab(ia,k)*cab(ib,l)*cabd(ia,m)*ccabd(ib,n)*1./ $
;                   (1. + complex(0,1)*(alpha-beta)*H))
;       endfor
    endfor
    R(i,j)=3./4.*rij
  endfor
  
                                ;rayleigh phase matrix must be real
  isreal=min(abs(imaginary(r)) le 1e-6)
  if isreal eq 0 then print,'Phase Matrix is complex! Please check!'
  
  R=float(R)                    ;return only real part
end


;special example in chapter 5.8
; slit_ori=180
; inc=90 & azi=0
; sol_x=949.999 & soly=0
; --> inc'=0.0825850 & azi'=90.
; --> mu=0.          & mu'=0.999999
;
; sol_x=350. & sol_y=0.
; inc=0. & azi=0.
; --> azi'=0 & inc'=21.6183
; --> mu=1. & mu'=0.92965904

pro r58,mu,mud
  
  print,[[8./3.+1./3.*(1-3*mu^2)*(1-3*mud^2),(1-3*mu^2)*(1-mud^2),0.,0.], $
         [(1-mu^2)*(1-3*mud^2),3*(1-mu^2)*(1-mud^2),0,0], $
         [0,0,0,0], $
         [0,0,0,4*mu*mud]]*3./8.
  
end
