pro crisp2tip1 ;,mask=mask,ipos=ipos,verbose=verbose,wlref=wlref,show=show,dir_wide=dir_wide,ps=ps;,wlvec=wlvec ;,mpos=mpos
  
                                ;heavily modified from soup2tip.pro
                                ;path to anasoft fzcompress.so
;  linkimage,'f0','/home/lagg/idl/ana/libf0.so',0,'ana_f0', $
;    min_args=2,max_args=3
  
  userlct,coltab=0,/full,/reverse
  dir='./profile_archive/CRISP/ana/'
  st1='Stokes'
  st2=['I','Q' ,'U', 'V']
  st3='_im26May2008.fp3'
  nn=['.2256.2364','.2262.2380','.2268.2396','.2274.2412','.2280.2428','.2286.2444','.2292.2460','.2298.2476','.2304.2492']
  contpos='.2366.2656'
  mm=[2256,2262,2268,2274,2280,2286,2292,2298,2304]
  wlref=6302.4936d
  ifilem=st1+'I'+st3+nn(0)+'*'
  iffm=file_search(dir+'/'+ifilem,count=mcntr)
  print,iffm
  print,mcntr
                                ;for frameno=0,mcntr-1 do begin
                                ;frameno=1
                                ;ifst=n2s(iffm(frameno))
                                ;st4=strmid(ifst,64)
  st4='.764678..765305.f0'
  st5='.764678..765305.f0.fits'
                                ; st5=st4+'.fits'
  
  
                                ;nn=['.2270.2371','.2276.2387','.2282.2403','.2288.2419','.2294.2435','.2300.2451','.2306.2467','.2312.2483','.2318.2499']
                                ;mm=[2270,2276,2282,2288,2294,2300,2306,2312,2318]
  verbose=1
                                ;if n_elements(show) eq 0 then show=1
  
  wfile=dir+st1+st2(0)+st3+contpos+st4
  show=1
  ifile=st1+'I'+st3+nn(*)+st4
  iff=file_search(dir+'/'+ifile,count=icnt)
  print,iff
  print,icnt
                                ;print,'Found '+n2s(icnt)+' SOUP Stokes I files.'
                                ;if icnt eq 0 then reset
  
  init=1
  idone=bytarr(icnt)
  
  
  
  ccname=st1+'IQUV'+st3+st5+'cc'
  print,ccname
  
  psset,ps=ps,size=[18,20],file='./ps/'+ccname+'.ps',/no_x
  
  ok=0

  contfile=f0(wfile)
  icimg=transpose(f0(wfile))
  
  cnt=9
  sto=4
  nwl=cnt
                                ;determine WL from mm 
  wlvec=dblarr(cnt)
  wlbin=intarr(cnt)
  dwl=dblarr(cnt)
  for iw=0,cnt-1 do begin    
    dwl(iw)=(mm(iw)-mm(4))*8.0
    print,'dwl',dwl
    wlvec(iw)=wlref+dwl(iw)/1000.
  endfor
  print,'wlvec=',wlvec
  for iw=0,cnt-1 do begin
    for ii=0,sto-1 do begin
      ff=dir+st1+st2(ii)+st3+nn(iw)+st4
      print,ff
      img=f0(ff)
      sz=size(img)
      if ii eq 0 and iw eq 0 then $
        data=make_array(type=sz(sz(0)+1),nwl,sz(2),sz(1)*4)
      data(iw,*,indgen(sz(1))*4+ii)=transpose(img)
      ok=ok+1
      idx=where(iff eq ff)
      if idx(0) ne -1 then idone(idx)=1
      if show then begin
        if init then if !d.name ne 'PS' then $
          window,0,xsize=sz(1)*.8,ysize=sz(2)*.6
        init=0
        zrange=minmax(img)
        if st2(ii) ne 'I' then zrange=[-1,1]*max(abs(zrange))
        image_cont_al,img,contour=0,/aspect,zrange=zrange,$
          title=st2(ii)+' '+n2s(dwl(iw))
      endif
      
    endfor
    
  endfor

  icont=icimg                   ;
  if show then begin
    image_cont_al,transpose(icont),contour=0,/aspect, $
      zrange=minmax(icont),title='ICONT'
    if n_elements(wfile) ne 0 then $
      xyouts,0,0,/normal,'From wideband image: '+wfile(0), $
      charsize=0.8*!p.charsize
  endif
  
                                ;normalize i to icont
  for iw=0,nwl-1 do begin
    data(iw,*,indgen(sz(1))*4)=data(iw,*,indgen(sz(1))*4);/float(icont)
    data(iw,*,indgen(sz(1))*4+1)=data(iw,*,indgen(sz(1))*4+1);/float(icont)
    data(iw,*,indgen(sz(1))*4+2)=data(iw,*,indgen(sz(1))*4+2);/float(icont)
    data(iw,*,indgen(sz(1))*4+3)=data(iw,*,indgen(sz(1))*4+3);/float(icont)

  endfor

                                ;icont(*)=1.

  newfits=dir+'/'+ccname
  print,'Writing TIP fits file: '+newfits
  writefits,newfits,data,append=0
  newccx=newfits+'x'
  
                                ;determine maximum continuum level
                                ;over te whole image (take median of
                                ;100 highest values))
  ihi=median(icont(*),7)
  ihi=ihi(reverse(sort(ihi)))
  icont_avg=median(ihi(0:99))

  
                                ;write aux file
  
  header=''
  print,'Writing Auxiliary file: '+newccx
  writefits,newccx,transpose(icont),header,append=0
                                ;stop
                                ;writefits,newccx,icont,header,append=0
  naxpos=max(where(strpos(header,'NAXIS') eq 0))
  wloff=0. & wlbin=0.
  header=[header(0:naxpos), $
          'WL_NUM  = '+string(nwl,format='(i20)')+ $
          ' / WL-bins', $
          'WL_OFF  = '+string(wloff,format='(d20.8)')+' / WL-Offset', $
          'WL_DISP = '+string(wlbin,format='(d20.8)')+ $
          ' / WL-Dispersion',$
          'ICNTAVG = '+string(icont_avg,format='(d20.8)')+ $
          ' / Total averaged quiet Sun Continuum', $
          header(naxpos+1:*)]
  writefits,newccx,transpose(icont),header,append=0        
  
                                ;write out wl-vector as extension
  writefits,newccx,wlvec,append=1

  
  psset,/close
;endfor
  stop
  
end
