pro pcwg_event,event
  common pcwg,pcwg
  common puse,puse
  common param,param
  common iwg_change,iwg_change
  
  pold=puse
  widget_control,event.id,get_uvalue=uval
  uspos=strpos(uval,'_')
  if uspos eq -1 then begin
    ust=uval
    uid=0
  endif else begin
    ust=strmid(uval,0,uspos)
    uid=fix(strmid(uval,uspos+1,strlen(uval)))    
  endelse
  case ust of
    'minval': begin
      puse(uid).minval=event.value
      puse(uid).usemin=1 & pcwg.usemin(uid).val=1
      widget_control,pcwg.usemin(uid).id,set_value=pcwg.usemin(uid).val
    end
    'maxval': begin
      puse(uid).maxval=event.value
      puse(uid).usemax=1 & pcwg.usemax(uid).val=1
      widget_control,pcwg.usemax(uid).id,set_value=pcwg.usemax(uid).val
    end
    'usemin': puse(uid).usemin=event.select
    'usemax': puse(uid).usemax=event.select
    'control': begin
      case event.value of
        'done': widget_control,pcwg.base.id,/destroy
        'reset': begin
          puse.usemin=0 & puse.usemax=0
          pcwg.usemin.val=0 & pcwg.usemax.val=0
          for i=0,param.npar-1 do begin
            widget_control,pcwg.usemin(i).id,set_value=pcwg.usemin(i).val
            widget_control,pcwg.usemax(i).id,set_value=pcwg.usemax(i).val
          endfor
        end
      endcase
    end
    else: begin
      help,/st,event
      print,uid,ust
    end
  endcase
  
  if n_elements(iwg_change) eq 0 then iwg_change=0
  iwg_change=iwg_change or (max(puse.minval ne pold.minval) or $
                            max(puse.maxval ne pold.maxval) or $
                            max(puse.usemin ne pold.usemin) or $
                            max(puse.usemax ne pold.usemax))
end
    
pro pc_widget
  common wgst,wgst
  common param,param
  common pcwg,pcwg
  common puse,puse
  
  subst={id:0l,val:0.,str:''}
  rp=replicate(subst,param.npar)
  pcwg={base:subst,usemin:rp,minval:rp,usemax:rp,maxval:rp,control:subst}
  
  screen=get_screen_size()
  pcwg.base.id=widget_base(title='Define Plot Conditions',/col, $
                            y_scroll_size=screen(1)*0.9, $
                            x_scroll_size=500)
  lab=widget_label(pcwg.base.id,value='Criteria for pixels to be plotted:')
  
  sst={usemin:0b,minval:0.,usemax:0b,maxval:0.}
  if n_elements(puse) ne param.npar then puse=replicate(sst,param.npar)
  pcwg.usemin.val=puse.usemin
  pcwg.usemax.val=puse.usemax
  pcwg.minval.val=puse.minval
  pcwg.maxval.val=puse.maxval
  
  for i=0,param.npar-1 do begin
    is='_'+n2s(i,format='(i2.2)')
    sub=widget_base(pcwg.base.id,row=1)
    lab=widget_label(sub,value=param.name(i),xsize=100)
    pcwg.usemin(i).id=cw_bgroup(sub,uvalue='usemin'+is,'MinVal:', $
                                set_value=pcwg.usemin(i).val,/nonexclusive)
    pcwg.minval(i).id=cw_field(sub,/all_events,/floating,xsize=8,title=' ', $
                               uvalue='minval'+is,value=pcwg.minval(i).val)
    pcwg.usemax(i).id=cw_bgroup(sub,uvalue='usemax'+is,'MaxVal:', $
                                set_value=pcwg.usemax(i).val,/nonexclusive)
    pcwg.maxval(i).id=cw_field(sub,/all_events,/floating,xsize=8,title=' ', $
                               uvalue='maxval'+is,value=pcwg.maxval(i).val)
   endfor
   
   pcwg.control.id=cw_bgroup(pcwg.base.id,['Done','Reset All'], $
                            uvalue='control',row=1, $
                            button_uvalue=['done','reset'])
   
  widget_control,pcwg.base.id,/realize
  xmanager,'pcwg',pcwg.base.id,no_block=1
end


pro image_settings
  common wgst,wgst
  common pcwg,pcwg
  
  if n_elements(pcwg) ne 0 then begin
    widget_control,pcwg.base.id,iconify=0,bad_id=bad_id
    if bad_id eq 0 then return
  endif
  
  pc_widget
  
end
