function read_spinor,file,icont=icont,error=error
  common verbose,verbose
  
  if n_elements(verbose) eq 0 then verbose=1
  if verbose ge 1 then print,'reading '+file
  READSTO,file,spec,numwl,wlref,ion,fparam,finfo,bound,silent=1,cont=0, $
    bcont=icont,error=error
  
                                ;check order
  wpos=strpos(fparam,'W')
  ipos=(strpos(fparam,'I')-wpos)(0)
  qpos=(strpos(fparam,'Q')-wpos)(0)
  upos=(strpos(fparam,'U')-wpos)(0)
  vpos=(strpos(fparam,'V')-wpos)(0)
  
                                ;read in spectrum with largest WL
                                ;coverage
  dummy=max(numwl) & is=!c
  if n_elements(wlref) gt 1 then if verbose ge 1 then $
    print,file+' (spectrum #'+n2s(is)+' only)'
  
  if n_elements(numwl) ne 0 then nwl=(numwl(is)-1) else nwl=0
  if nwl lt 1 then begin
    spec=dblarr(1,5,2)
    nwl=1
    spec(is,0,*)=[1,100]
    icont=1
  endif
  
  profile={wl:(spec(is,0,0:nwl))(*), $
           i:(spec(is,ipos,0:nwl))(*), $
           q:(spec(is,qpos,0:nwl))(*), $
           u:(spec(is,upos,0:nwl))(*), $
           v:(spec(is,vpos,0:nwl))(*), $
           ic:icont}
  
  return,profile
end
