pro get_wgt,obs=observation,line=line,wgti=wgt,wgt_mag=wgt_mag,icont=icont
  
  
                                ;----------- weighting part -------------------
                                ;define weighting
    nwl=n_elements(observation.wl)
    emprof=fltarr(nwl)
    
                                ;special helium weighting
    if max(strpos(id2s(line.id),'HeI') ne -1) gt 0 then begin 
      good_range=[10828.5d,10831.5d]
      good=(where(observation.wl ge good_range(0) and $
                  observation.wl le good_range(1)))
;   verygood_range=[10829.6d,10831.35d]
      verygood_range=[10829.6d,10832.5d,10831.5d]
      igood_red_range=[10832.5d,10835.0d]
                                ;define very good range where telluric
                                ;blend begins (12 points left of large
                                ;derivative of I)
      istart=min(where(observation.wl ge verygood_range(2)))
      if istart ne -1 then begin
        istop=(min(where(deriv(observation.wl(istart:*), $
                               observation.i(istart:*)) lt -1e4))-12)>0
        verygood_range(1)=observation.wl(istart+istop)
      endif

      
      verygood=(where(observation.wl ge verygood_range(0) and $
                      observation.wl le verygood_range(1)))
      
      igood_red=(where(observation.wl ge igood_red_range(0) and $
                       observation.wl le igood_red_range(1)))
      
      
      good_mag_range=[10828.5d,10832.5d]
      good_mag=(where(observation.wl ge good_mag_range(0) and $
                      observation.wl le good_mag_range(1)))
      verygood_mag_range=[10829.6d,10832.5d,verygood_range(1)]
      verygood_mag=(where(observation.wl ge verygood_mag_range(0) and $
                          observation.wl le verygood_mag_range(1)))
      verygood_mag_icont=(where(observation.wl ge verygood_mag_range(0) and $
                                observation.wl le verygood_mag_range(2)))
      
      
      wgt=emprof
      if good(0) ne -1 then wgt(good)=0.2
      if verygood(0) ne -1 then begin
        wgt(verygood)=1.
;        wgt(verygood)=wgt(verygood)/(observation.i(verygood)/icont)
      endif
      if igood_red(0) ne -1 then wgt(igood_red)=0.5*0
      wgt=wgt/max(wgt)
      
      wgt_mag=emprof
      if good_mag(0) ne -1 then wgt_mag(good_mag)=0.2
      if verygood_mag(0) ne -1 then wgt_mag(verygood_mag)=1.
      
      wgt_unno=wgt_mag  ;weighting for angle approximation: instead of
                                ;multiplying with I we divide by I to
                                ;weight the wings of the lines more
                                ;than the center.
      if verygood_mag_icont(0) ne -1 then begin
;      wgt_mag(verygood_mag_icont)= $
;        wgt_mag(verygood_mag_icont)/(observation.i(verygood_mag_icont)/icont)
;      wgt_unno(verygood_mag_icont)= $
;        wgt_unno(verygood_mag_icont)*(observation.i(verygood_mag_icont)/icont)
      endif
      wgt_mag=wgt_mag/max(wgt_mag)
      wgt_unno=wgt_unno/max(wgt_unno)
      
      scl_mag=(max(observation.i)-min(observation.i))/icont/ $
        sqrt(max(observation.q^2+observation.u^2+observation.v^2))
      scl_mag=scl_mag<30
      wgt=wgt/scl_mag
    endif else begin            ;normal weighting: everything to one
      dwl=0.7
      nogood=where(observation.wl lt line(0).wl-dwl or $
                   observation.wl gt line(0).wl+dwl)
      normi=1./(observation.i/icont)
      normi=(normi-min(normi))/(max(normi)-min(normi))
      scli=normi*0.5+0.5
      wgt_unno=emprof+1*(1-normi)
      wgt=scli                  ; gt .5
      wgt_mag=wgt
      if nogood(0) ne -1 then begin
        wgt(nogood)=0.
        wgt_unno(nogood)=0.
        wgt_mag(nogood)=0
      endif
    endelse
    
end
