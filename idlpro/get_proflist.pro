function get_proflist,ipt,xcrd=xcrd,xcexact=xcexact, $
                      ycrd=ycrd,ycexact=ycexact,xcoff=xcoff,ycoff=ycoff, $
                      window=window,nolist=nolist
  
  
                                ;retrieve profiles from file
  if ipt.profile_list ne '' and keyword_set(nolist) eq 0 then begin
    unit=file_open(ipt.profile_list,error=error,verbose=ipt.verbose, $
                   path=['./',ipt.dir.profile,ipt.dir.profile+ipt.observation])
    if error ne 0 then begin
      print,'Cannot read profile_list: '+ipt.profile_list
      reset
    endif else begin
      il=0
      xarr=-1 & yarr=-1
      repeat begin
        a='' & readf,unit,a
        a=strtrim(a,2)
        if strpos(['0123456789'],strmid(a,0,1)) ne -1 then begin
          reads,a,x,y
          xarr=[xarr,fix(x)]
          yarr=[yarr,fix(y)]
        endif
      endrep until eof(unit)
      free_lun,unit
      if n_elements(xarr) eq 1 then begin
        print,'Invalid profile list in '+ipt.profile_list
        reset
      endif
      xarr=xarr(1:*)
      yarr=yarr(1:*)
      lst=n2s(fix(xarr)/ipt.stepx,format='(i5.5)')+'.'+ $
        n2s(fix(yarr)/ipt.stepy,format='(i5.5)')
      rm=remove_multi(lst,index=idx)
      xarr=xarr(idx) & yarr=yarr(idx)
      list=transpose([[xarr],[yarr]])
      xcoff=(min(xarr)/ipt.stepx)*ipt.stepx
      ycoff=(min(yarr)/ipt.stepy)*ipt.stepy
      xcexact=indgen((max(xarr)-xcoff+ipt.stepx)/ipt.stepx+1)*ipt.stepx+xcoff
      ycexact=indgen((max(yarr)-ycoff+ipt.stepy)/ipt.stepy+1)*ipt.stepy+ycoff
    endelse
  endif else begin
    
    if n_elements(window) ne 4 then $
      window=[min(ipt.x),min(ipt.y), $
              max(ipt.x),max(ipt.y)]
    x0=min(ipt.x)>window(0)
    x1=max(ipt.x)<window(2)
    y0=min(ipt.y)>window(1)
    y1=max(ipt.y)<window(3)
    
    
    for ix=min([x0,x1]),max([x0,x1]) do begin
      get_profidx,x=ix,y=y0,stepx=ipt.stepx,stepy=ipt.stepy, $
        rgx=[x0,x1],rgy=[y0,y1], $
        vecx=xpvec,vecy=ypvec,average=ipt.average,scansize=ipt.scansize
      if ix eq min([x0,x1]) then xcexact=xpvec(0) $
      else if max(xcexact eq xpvec(0)) eq 0 then xcexact=[xcexact,xpvec(0)]
    endfor

    for iy=min([y0,y1]),max([y0,y1]) do begin
      get_profidx,x=x0,y=iy,stepx=ipt.stepx,stepy=ipt.stepy, $
        rgx=[x0,x1],rgy=[y0,y1], $
        vecx=xpvec,vecy=ypvec,average=ipt.average,scansize=ipt.scansize
      if iy eq min([y0,y1]) then ycexact=ypvec(0) $
      else if max(ycexact eq ypvec(0)) eq 0 then ycexact=[ycexact,ypvec(0)]
    endfor
    xarr=((intarr(n_elements(ycexact))+1) # xcexact)(*)
    yarr=(ycexact # (intarr(n_elements(xcexact))+1))(*)
    list=transpose([[xarr],[yarr]]) 
    
    nx=n_elements(xcexact)
    ny=n_elements(ycexact)
    nxold=nx 
    nyold=ny
    piklm_bx=ipt.piklm_bx>1
    piklm_by=ipt.piklm_by>1
    nx=(nx/piklm_bx)*piklm_bx
    ny=(ny/piklm_by)*piklm_by

                                ;sort according to piklm box size
    nptot=nx*ny
    xyarr=intarr(2,nptot)
    xyarr(*,*)=-1
    for ix=0,nx/piklm_bx-1 do begin
      for iy=0,ny/piklm_by-1 do begin
        for ibx=0,piklm_bx-1 do begin
          for iby=0,piklm_by-1 do begin
            ii=(iby+ibx*piklm_by+ $
                iy*piklm_by*piklm_bx+ $
                ix*piklm_bx*ny );+1
            ixx=ix*piklm_bx+ibx
            iyy=iy*piklm_by+iby
            if ((ixx le nxold-1) and (iyy le nyold-1)) then begin
              xyarr(0,ii)=xcexact(ixx)
              xyarr(1,ii)=ycexact(iyy)
            endif
          endfor
        endfor
      endfor
    endfor
    list=xyarr
    
    xcoff=min(ipt.x)
    ycoff=min(ipt.y)
  endelse

  xcexact=remove_multi([xcexact])
  ycexact=remove_multi([ycexact])
  if n_elements(xcexact) eq 1 then xcexact=[xcexact,xcexact(0)+ipt.stepx] $
  else  xcexact=[xcexact,xcexact(n_elements(xcexact)-1)+ipt.stepx]
;  if n_elements(xcexact) eq 2 then xcexact=[xcexact,xcexact(1)+ipt.stepx]
  if n_elements(ycexact) eq 1 then ycexact=[ycexact,ycexact(0)+ipt.stepy] $
  else ycexact=[ycexact,ycexact(n_elements(ycexact)-1)+ipt.stepy]
;  if n_elements(ycexact) eq 2 then ycexact=[ycexact,ycexact(1)+ipt.stepy]
  xcrd=xcexact(0:(n_elements(xcexact)-2)>0)
  ycrd=ycexact(0:(n_elements(ycexact)-2)>0)
  
;  xcoff=xcoff>window(0)
;  ycoff=ycoff>window(1)
  
  return,list
  
end
