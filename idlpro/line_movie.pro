
pro line_movie
  
  !p.charsize=1.
  par=['vlos','vdopp','sgrad','bfiel']
;  rg=[[-20000.,+20000],[0.01,1.5],[5.,.5],[0.,5000]]
  step0=10
  ipt=read_ipt('line_movie.ipt')
  
  for im=0,n_elements(par)-1 do begin
    case par(im) of
      'vlos': begin
        paridl='ipt.atm.par.vlos'
        val=[0.,20000,-20000,0]
        title='velocity'
        unit='km/s'
        scale=1000.
        off=0.
      end
      'vdopp': begin
        paridl='ipt.atm.par.dopp'
        val=[0.3,1.,0.07,0.3]
        title='temperature'
        unit='K'
        scale=1/5000.
        off=3500.
      end
      'sgrad': begin
        paridl='ipt.atm.par.sgrad'
        val=[2.,.02,2.]
        title='density'
        unit=''
        scale=1.
        off=0.
      end
      'bfiel': begin
        paridl='ipt.atm.par.b'
        val=[1000.,15000.,1000.]
        title='magnetic field'
        unit='nT'
        scale=10.
        off=0.
      end
      else: stop
    endcase
    
    file='line_movie_'+par(im)
    psset,/ps,file=file+'.ps',/no_x,size=[14,10]
    device,xoff=0,yoff=0
    
    
    for iv=0,n_elements(val)-2 do begin
      ipt=read_ipt('line_movie.ipt')
      
      steps=fix(abs(val(iv+1)-val(iv))/abs(val(1)-val(0))*step0+1)
      cval=findgen(steps)/(steps-1)*(val(iv+1)-val(iv))+val(iv)

      for ip=0,steps-1 do begin
        if iv ne 0 or ip ne 0 then erase
        dummy=execute(paridl+'=cval(ip)')
        
        help,/st,ipt.atm.par    
        helix,fitprof=fitprof,struct_ipt=ipt,result=result;,/no_x,ps=0
        help,/st,result.fit.atm.par
        if iv eq 0 and ip eq 0 then prof0=fitprof
;        tit=title
        ;; plot_profiles,prof0,fitprof,title=tit, $
        ;;   lthick=[3,5],color=[3,1],lstyle=[1,0], $
        ;;   fraction=[1,.4,.4,.4]
        plot_profiles,prof0,fitprof,title=tit, $
          lthick=[3,5],color=[3,1],lstyle=[1,0], $
          fraction=[1,.8],iquv='IV'
        
        !p.font=0
        xyouts,.94,.70,alignment=1,/normal,charsize=1.25, $
               title+'!C'+string(format='(f8.2)',cval(ip)/scale+off)+' '+unit
      endfor
    endfor

  
  
    psset,/close
      spawn,'psselect -p1 '+file+'.ps >'+file+'_p1.ps'
      spawn,'convert -density 300x300 '+file+ $
        '_p1.ps '+file+'_p1.gif'
    spawn,'convert -density 300x300 -alpha remove -delay 50 ' + $
      file+'.ps '+file+'.gif'
    print,'Wrote Gif-Animation '+file+'.gif'
 
  endfor
  stop
end
