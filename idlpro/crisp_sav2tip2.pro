pro crisp_sav2tip2,sav=sav,wlref=wlref,wlvec=wlvec,out_dir=out_dir, $
                  icont_idx=icont_idx
  
  
  
  userlct,coltab=0,/full,/reverse
  
  if (n_elements(sav) eq 0 or n_elements(wlref) eq 0 or $
      n_elements(wlvec) eq 0) then begin
    print,'Converting CRISP sav files to TIP format for HeLIx+ inversions.'
    print,'Usage:'
    print,'  crisp_sav2tip2,sav=''/scratch/slam/lagg/wiggle_fe.save'', $'
    print,'            wlref=6302.4936d,wlvec=[-120.,-60.,0.,60,120,680]*1e-3, $'
    print,'            out_dir=''/scratch/slam/lagg/'',$'
    print,'            icont_idx=[5] ;array containing indices for continuum'
    retall
  endif
  if n_elements(icont_idx) eq 0 then begin
    message,/cont,'icont_idx must be specified'
    print,'example: icont_idx=[array containing indices for continuum]'
    retall
  endif
  
  fsav=file_search(sav,count=nwl)
  
  lslpos=strpos(fsav(0),'/',/reverse_search)
  dotfits=strpos(fsav(0),'.sav')
  froot=strmid(fsav(0),lslpos+1,dotfits-lslpos-1)
  if lslpos ge 0 then fpath=strmid(fsav(0),0,lslpos+1) else fpath=''
  if n_elements(out_dir) eq 1 then fpath=out_dir
  
  
  order_crisp=['I','Q','U','V']
  restore,/v,fsav(0)
  sz=size(prof)
  nx=sz(2)
  ny=sz(3)
  nwl=sz(1)/4
  nt=sz(4)
  dat=fltarr(nwl,nx,ny,4,nt)
  for it=0,nt-1 do for is=0,3 do $
    dat(*,*,*,is,it)=prof(indgen(nwl)+nwl*is,*,*,it)
  
  for it=0,nt-1 do begin
    icont=total(dat(icont_idx,*,*,0,it),1)/n_elements(icont_idx)
                                ;remove normalization to Ic
    for is=0,3 do for iwl=0,nwl-1 do $
       dat(iwl,*,*,is,it)=reform(dat(iwl,*,*,is,it))
    
    data=make_array(nwl,ny,nx*4,type=sz(sz(0)+1))
    order_sav=['I','Q','U','V']
    for is=0,3 do begin
      iu=where(order_sav eq order_crisp(is))
      for ix=0,nx-1 do data(*,*,ix*4+iu)=reform(dat(*,ix,*,is,it))
    endfor
  
    print,'============ SIZE ======================'
    print,'WL-pixels: ',nwl
    print,'y-pixels: ',ny
    print,'x-pixels (*4): ',nx
  
    data=float(data)
;  byteorder,data,/lswap
    
    fitsout=fpath+froot+'.t'+n2s(it,format='(i3.3)')+'.fitscc'
    print,'======= WRITE NEW FITS ================='
    print,'File: ',fitsout
  
    writefits,fitsout,data,append=0
    
                                ;now create ccx file  
    print,'======= WRITE NEW CCX ================='
    filex=fitsout+'x'
                                ;create header
    header=''
  
  
                                ;determine maximum continuum level
                                ;over te whole image (take median of
                                ;20% highest values))
  
    ihi=median(icont(*),7)
    ihi=ihi(reverse(sort(ihi)))
    icont_avg=median(ihi(0:n_elements(ihi)*0.10))
  
    writefits,filex,icont,header,append=0
    naxpos=max(where(strpos(header,'NAXIS') eq 0))
    header=[header(0:naxpos), $
            'WL_NUM  = '+string(nwl,format='(i20)')+ $
            ' / WL-bins', $
            'WL_OFF  = '+string(wlref,format='(d20.8)')+' / WL-Offset', $
            'WL_DISP = '+string(wlvec(1)-wlvec(0),format='(d20.8)')+ $
            ' / WL-Dispersion', $
            'ICNTAVG = '+string(icont_avg,format='(d20.8)')+ $
            ' / Total averaged quiet Sun Continuum', $
            header(naxpos+1:*)]
    writefits,filex,icont,header,append=0        
  
                                ;write out wl-vector as extension
    writefits,filex,wlvec+wlref,append=1
    print,'Wrote '+filex
  endfor
  print,'Done.'

end
