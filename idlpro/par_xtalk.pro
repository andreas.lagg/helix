pro par_xtalk,ps=ps,norun=norun
  common res,resarr,ccorr
  common old_ipt,old_ipt
  
  
  if n_elements(old_ipt) ne 0 then dummy=temporary(old_ipt)
  
                                ;create synthetic profile
  helix,ipt='soup_synth.ipt',fitprof=fitprof
  
                                ;extract the wl points corresponding
                                ;to soup observation
  wlsoup=[6302.4436d, 6302.5436d]
;  wlsoup=[6302.1436d, 6302.4436d, 6302.5436d]
  iwl=intarr(n_elements(wlsoup))
  for i=0,n_elements(wlsoup)-1 do begin
    dummy=min(abs(fitprof.wl-wlsoup(i))) & iwl(i)=!c
  endfor
;  iwl=indgen(n_elements(fitprof.i))
  
                                ;this is our 'observed' profile
  profile={ic:fitprof.ic,wlref:fitprof.wlref,wl:fitprof.wl(iwl), $
           i:fitprof.i(iwl),u:fitprof.u(iwl), $
           q:fitprof.q(iwl),v:fitprof.v(iwl)}
  save,file='./profile_archive/synth.sav',profile
  
                                ;read in input file used as a basis
                                ;for our inversions
  ipt=read_ipt('input/soup_synth.ipt')
  iptorig=ipt
  ipt.ncalls=0 & ipt.ncalls(0)=600;# of iterations
  ipt.observation='synth.sav'
  ipt.dir.profile='./profile_archive/'
;  ipt.x=0
;  ipt.y=0
  ipt.synth=0             ;no synthesis! we want to make an inversion.
;  ipt.noise=1e-3            ;add realistic noise level to observations
  
  nel=41
  dopp=findgen(nel)/(nel-1)*0.06+.01

  dopp=dopp>1e-4
  nrun=1
  
  if keyword_set(norun) eq 0 or n_elements(resarr) eq 0 then begin
    ccorr=[1,1.]
                                ;change atmospheric parameters
    for i=0,nel-1 do begin
      ipt.atm(0).par.dopp=dopp(i)
      ipt.atm(0).fit.dopp=0
      print,i,'/',nel,': DOPP=',dopp(i)
      
                                ;run inversion
      for ir=0,nrun-1 do begin
;        ipt.gen.par.ccorr=1.+randomu(seed)*0.2-.1
        ccorr=minmaxp([ccorr,ipt.gen.par.ccorr])
        helix,struct_ipt=ipt,result=result,fitprof=fit
        if i eq 0 and ir eq 0 then resarr=replicate(result,nel,nrun)
        resarr(i,ir)=result
      endfor
    endfor
  endif
  
  
  noisestr=n2s(ipt.noise,format='(f15.4)')
  ccorrstr=add_comma(n2s(ccorr,format='(f15.2)'),sep='-')
  addsoup=(['soup','nosoup'])(ipt.conv_nwl eq 0)
  filename='xtalk_vdopp_'+addsoup+'_noise'+noisestr+'_ccorr'+ccorrstr+'.ps'
  
  psset,ps=ps,file=filename
  par=['B','INC','AZI','VLOS','ETAZERO','FITNESS']
  tn=tag_names(resarr.fit.atm.par)
  userlct
  !p.multi=[0,1,n_elements(par)]
  for i=0,n_elements(par)-1 do begin
    if par(i) ne 'FITNESS' then begin
      it=where(tn eq par(i))
      value=resarr.fit.atm(0).par.(it)
    endif else value=resarr.fit.fitness
    plot,xrange=minmaxp(dopp),yrange=minmaxp(value),[0,1],/nodata, $
      xtitle='VDOPP',ytitle=par(i),ytype=par(i) eq 'FITNESS'
    for ir=0,nrun-1 do plots,psym=4,dopp,value(*,ir)
    if par(i) ne 'FITNESS' then $
      oplot,linestyle=2,!x.crange,[0,0]+iptorig.atm(0).par.(it),color=1
    oplot,linestyle=2,[0,0]+iptorig.atm(0).par.dopp,!y.crange,color=3
  endfor
  xyouts,alignment=0.5,/normal,.5,1, $
    '!CNoise = '+noisestr+', CCORR = '+ccorrstr+', '+strupcase(addsoup)
  xyouts,0,0,filename
  psset,/close
  
  stop
  
end
