function fit_azi_amoeba,alpha2
  common for_amoeba,u_signal,q_signal,quv_weight
  
  diffvec=alpha2(0) - atan(u_signal,q_signal)
;  diffvec=q_signal*sin(alpha2(0)) - u_signal*cos(alpha2(0))
  diff=total(abs(quv_weight*(diffvec)))
;  diff=total(quv_weight*abs())
  if diff lt 1e-6 then diff=0
  
;  plot,diffvec;atan(u_signal,q_signal)/2/!dtor
;   print,alpha2(0)/2/!dtor,diff
  
  return,diff
end
