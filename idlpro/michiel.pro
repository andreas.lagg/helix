data=readfits('/scratch/slam/vannoort/spinor/metest/inverted_profs.fits')    
  wl=dindgen(320)/319*(5250.508d - 5250.008d)+5250.008d                        
  i=data(*,0,0,0)                                                              
  q=data(*,2,0,0)                                                              
  u=data(*,3,0,0)                                                              
  v=data(*,1,0,0)                                                              
  ic=max(i)                                                                    
  profile={ic:1.,i:i/ic,q:q/ic,u:u/ic,v:v/ic,wl:wl}                            
  write_stopro,file='profile_archive/mhd_michiel.dat',profile=profile,wlref=5250.258d,header='',/verbose
  
  
end
