;define max. values of array size (important if FORTRAN routines are
;used)
;MUST be consistent with defninitions in all_type.f
pro maxpar
  @common_maxpar
  
  
  maxlin=20                      ;Max number of lines
                                ;     (maxlin must be equal to maxatm)
  maxatm=20                     ;Max number of atmospheres
  maxfitpar=256                 ;max number of free fit parameters
  maxwl=4096                    ;Max number of WL-points
  maxcwl=32767               ;Max number of WL-points for convolve function
  idlen=12               ;length of ID string
  maxlines=16                    ;max. number of lines per atomic-file
  maxblend=16                   ;max. number of telluric blends
  maxmi=8                       ;max. number of multi-iterations
  maxcmt=100                    ;max. # of lines for comments
  maxpi=17 & maxsigma=16        ;max. quantum numbers J (defines
                                ;        max. size for zeeman pattern arrays)
                                ;  =2*minval([maxjl,maxju])+1, max # of pi-comp
                                ;   =maxjl+maxju , max # of
                                ;   sigma-comp.
  max_pb_zl=17                ;max. number of zeeman levels (storage of
                                ;Paschen-Back table from
                                ;Socas-Navarro)
  max_pb_nr=20         ;max. number of different B-values for PB-table
  maxstr=160           ;Max len of strings
  maxfitpar=100        ;max number of fit parameters
  maxipt=1024          ;max number of input file lines
  ;THE NEXT TWO VALUES MUST BE ADJUSTED ALSO IN PIKAIA.PRO (NMAX, PMAX)
  pik_NMAX=96                   ; max. number of PIKAIA parameters
  pik_PMAX=512                  ; max. number of PIKAIA populations
end

