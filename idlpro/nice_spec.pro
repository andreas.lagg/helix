pro nice_spec,ps=ps
  @common_cppar
  common greek_letters
  
  wd=18. & hg=22.
  if keyword_set(ps) then begin
    psf='ps/nice_prof.ps'
    psout=ps_widget(default=[0,0,0,0,8,0,0],size=[wd,hg],psname=psn, $
                    file=psf,/no_x)
    if psout(1) eq 1 then begin
      return
    endif
    !p.font=1
    !p.charsize=1.5
    device,set_font='helvetica',/tt_font
  endif else begin
    window,0,xsize=640.,ysize=640./wd*hg
    !p.font=-1
    !p.charsize=1.2
  endelse
  userlct & greek & erase
  
  
  atm=def_parst(scale=scale)
  he=def_line(line='he')
  he.icont=1
  atm.par.b=2000
  atm.par.azi=-30
  atm.par.inc=60
  atm.par.dopp=0.2
  atm.par.damp=0.2
  atm.par.sgrad=1.
  atm.par.etazero=1.
  atm.par.ff=1.
  atm.use_line(*,0:2)=he(0:2).id
  
  voigt=1 & modeval=1 & magopt=1 & hanle_azi=0 & norm_Stokes_val=0
  use_geff=0 & use_pb=0 & old_norm=0 & old_voigt=0
  nel=512
  wl=findgen(nel)/(nel-1)*3.3+10828.1
  
  prof=compute_profile(/init,atm=atm,line=he,wl=wl)
    
;  plot_profiles,prof,title='Synthetic Profile'
  
  pst={pos:0.,col:0,scl:0.,tit:'',one:0,lab:''}
  plt=replicate(pst,4)
  
  plt.pos=[0.95,.55,.35,.15]
  plt.scl=[0.35,.12,.25,.25]
  plt(0).one=1
  plt.tit=['I/I!LC!N','Q/I!LC!N','U/I!LC!N','V/I!LC!N']
  plt.col=[1,2,3,4]
  plt.lab=['!CIntensität    !C(unpolarisiert)','!Clinear!Cpolarisiert', $
           'linear!Cpolarisiert!C(45'+f_grad+' gedreht)', $
           '!Czirkular!Cpolarisiert']
  
  plot,wl,wl*0,/xst,/yst,yrange=[0,1],yticks=1,ytickname=[' ',' '], $
    xtickformat='(f15.1)',charsize=!p.charsize*1.25, $
    xtitle='Wellenlänge ['+f_angstrom+']',pos=[.13,.1,.98,.95]
  for i=0,3 do begin
    prf=prof.(i+1)
    if plt(i).one eq 1 then prfscl=(prf-min(prf))/(max(prf)-min(prf))-1. $
    else prfscl=prf/(2*max(abs(prf)))
    prfscl=prfscl*plt(i).scl
    
    oplot,wl,prfscl+plt(i).pos,color=plt(i).col,thick=8
    
    plots,wl,wl*0+plt(i).pos ,linestyle=1
    
    xyouts,color=plt(i).col,/data,!x.crange(0),plt(i).pos, $
      '!C!C  '+plt(i).tit,charsize=1.5*!p.charsize,charthick=2
    
    y1=(convert_coord(/data,/to_normal,[wl(0),plt(i).pos]))(1)
    xyouts,alignment=1.-.5*(i gt 0),/normal,0.0,y1,charsize=1.2*!p.charsize, $
      '!C'+plt(i).lab,orientation=90,charthick=2,color=plt(i).col
  endfor
  
  
  
  show=[1,0,1]
  for i=0,n_elements(he)-1 do begin
    plots,[0,0]+he(i).wl,!y.crange,linestyle=2
    
    y1=(convert_coord(/to_data,/normal,[0,1]))(1)
    
    if show(i) then $
      xyouts,alignment=0.5,/data,he(i).wl,y1,charsize=1.5*!p.charsize, $
      '!C'+string(he(i).id)
  endfor
  
  if !d.name eq 'PS' then begin
    xyouts,0,0,/normal,'!C!C'+psn,charsize=0.5*!p.charsize
    device,/close
    print,'Created PS-file: '+psn
    if psout(3) eq 1 then  spawn_gv,psn
  endif
  
  !p.font=-1 & set_plotx
end
