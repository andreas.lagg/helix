;define user defined coordinates for plot (eg aligned x/y coords with
;other instrumentd
pro load_xy
  @common_user_coords
  common wgst
  
  xyfile=dialog_pickfile(dialog_parent=wgst.base.id, $
                         filter='./sav/align_*.sav', $
                         title='Select xy-coord file')
  uc_unit=''
  if xyfile(0) eq '' then return
  
  xyfile=xyfile(0)
  case 1 of
    strpos(xyfile,'align_mdi') ne -1: begin
      restore,/v,xyfile
      uc_xvec=xtip(*,0)
      uc_yvec=ytip(0,*)
      uc_unit='arcsec (MDI)'
      if n_elements(soho_tref) ne 0 then uc_tref=soho_tref $
      else uc_tref=mdi_tref
    end
    strpos(xyfile,'align_sum') ne -1: begin
      restore,/v,xyfile
      uc_xvec=xtip(*,0)
      uc_yvec=ytip(0,*)
      uc_unit='arcsec (SUM)'
      uc_tref=soho_tref
    end
    else: begin
      message,/cont,'Unknown xy-coord file: '+xyfile
    end
  endcase
end
