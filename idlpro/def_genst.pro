function def_genst
  @common_maxpar
  
                                ;define parameter set for one
                                ;component
  mm={min:0.,max:0.}
  
  
  genscl={ccorr:mm,straylight:mm,radcorrsolar:mm}
  genfit={ccorr:0,straylight:0,radcorrsolar:0,dummy:0}
  genpar={ccorr:1.,straylight:0.,radcorrsolar:1.,dummy:0.}
  
  ;,default values for scaling of generic parameter
  genscl.ccorr.min=0.8
  genscl.ccorr.max=1.2
  genscl.straylight.min=0.
  genscl.straylight.max=1.
  genscl.radcorrsolar.min=0.1
  genscl.radcorrsolar.max=10.
  
  
  genst={fit:genfit,idx:genfit,par:genpar, $
           scale:genscl,npar:0l}
  
  return,genst
end
