pro compare_sav,ps=ps
  common storesav0,pr1,pr2,pr3
  @greeklett
  
  if n_elements(pr1) eq 0 then begin
    restore,'sav/2009y_06m_09d_new_fits4D/imax_163_208.fits.gz_locstray_conv85_200_lsrad50.pikaia.sav'
    pr1=temporary(pikaia_result)
    restore,'sav/2009y_06m_09d_new_fits4D/imax_163_208.fits.gz_conv85_200.pikaia.sav'
    pr2=temporary(pikaia_result)
    restore,'sav/2009y_06m_09d_new_fits4D_nr/imax_nr_163_208.fits.gz_locstray_conv85_200_lsrad50_nr.pikaia.sav'
    pr3=temporary(pikaia_result)
  endif
  
  par1=pr1.fit.atm(0).par
  par2=pr2.fit.atm.par
  par3=pr3.fit.atm(0).par
  
  !p.charsize=1.2
  
  
  psset,ps=ps,size=[24.,12],file='ps/compare_B.eps',/encaps,/no_x
  userlct
  !p.multi=[0,2,1]
  plot,par1.b*par1.ff,par2.b,psym=1,ytitle='B without LS [G]',xtitle='B*FF with LS [G]',xrange=[0.,1500],yrange=[0,1500],/xst,/yst
  oplot,color=1,!x.crange,!y.crange
  plot,par1.b,par2.b,psym=1,ytitle='B without LS [G]',xtitle='B with LS [G]',xrange=[0.,1500],yrange=[0,1500],/xst,/yst  
  oplot,color=1,!x.crange,!y.crange
  psset,/close
  
  psset,ps=ps,size=[24.,12],file='ps/compare_INCAZI.eps',/encaps,/no_x
  userlct
  !p.multi=[0,2,1]
  plot,par1.inc,par2.inc,psym=1,ytitle='INC without LS ['+f_grad+']',xtitle='INC with LS ['+f_grad+']',xrange=[0.,180],yrange=[0,180],/xst,/yst  
  oplot,color=1,!x.crange,!y.crange
  plot,par1.azi,par2.azi,psym=1,ytitle='AZI without LS ['+f_grad+']',xtitle='AZI with LS ['+f_grad+']',xrange=[-90.,90],yrange=[-90,90],/xst,/yst  
  oplot,color=1,!x.crange,!y.crange
  psset,/close
  
  psset,ps=ps,size=[24.,18],file='ps/compare_VLOS.eps',/encaps,/no_x
  userlct
  !p.multi=[0,1]
  plot,par1.vlos/1e3,par2.vlos/1e3,psym=1,ytitle='VLOS without LS [km/s]',xtitle='VLOS with LS [km/s]',xrange=[-3.,3.],yrange=[-3.,3.],/xst,/yst  
  oplot,color=1,!x.crange,!y.crange
  psset,/close
  
  
  psset,ps=ps,size=[24.,12],file='ps/compare_B_nr.eps',/encaps,/no_x
  userlct
  !p.multi=[0,2,1]
  plot,par3.b*par3.ff,par2.b,psym=1,ytitle='B without LS [G]',xtitle='B*FF with LS, NON-RECONSTRUCTED [G]',xrange=[0.,1500],yrange=[0,1500],/xst,/yst
  oplot,color=1,!x.crange,!y.crange
  plot,par3.b,par2.b,psym=1,ytitle='B without LS [G]',xtitle='B with LS, NON-RECONSTRUCTED [G]',xrange=[0.,1500],yrange=[0,1500],/xst,/yst  
  oplot,color=1,!x.crange,!y.crange
  psset,/close
  

end
  
