pro read_version,svn_datstr,svn_timestr,svn_revstr

  read_file,'version.txt','./fortran/src/',line,lnr
  idat=(where(strpos(line,'$Date:') ne -1))[0]
  irev=(where(strpos(line,'$Rev:') ne -1))[0]
  svn_datstr=strmid(line[idat],19,10)
  svn_timestr=strmid(line[idat],30,14)
  svn_revstr=(strsplit(line[irev],' ',/extract))[1]
  
end
