;select latest files from file list
function select_latest,filter=filter,number=number
  
  if !version.release lt 5.5 then begin
    print,'IDL >= 5.5 required'
    return,''
  endif
  
  files=file_search(filter,count=cnt)
  if cnt eq 0 then return,''
  
  mtime=lon64arr(cnt)
  for i=0,cnt-1 do begin
    info=file_info(files(i))
    mtime(i)=info.mtime
  endfor
  
  srt=reverse(sort(mtime))
  latest=(files(srt))(0:(number<cnt)-1)
  nls=n_elements(latest)
  
  print,'Latest files: '
  for i=0,nls-1 do $
    print,i+1,' ... ',latest(i),format='(i2,a5,a)'
  print,'(press key)'
  repeat begin
    key=get_kbrd(1)
  endrep until key ne ''
  fkey=(fix(byte(key))-49)(0)      
  if fkey ge 0 and fkey le nls-1 then ret_file=latest(fkey) $
  else ret_file=''


  return,ret_file

end
