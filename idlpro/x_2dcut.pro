pro make_movie,xsize=xsize,azi=azi,pol=pol,reverse=reverse
  common x2dwg,x2dwg
  
  if n_elements(xsize) eq 0 then xsize=1024
  widget_control,x2dwg.plot.id,get_value=widx
  wset,widx
  
  nimg=30
  az=[40,20]
  if n_elements(azi) eq 0 then azi=findgen(nimg)/(nimg+1)*(az(1)-az(0))+az(0)
  
  pl=[85,25]
  if n_elements(pol) eq 0 then pol=findgen(nimg)/(nimg+1)*(pl(1)-pl(0))+pl(0)
  
  
  if keyword_set(reverse) then revcnt=0 else revcnt=1
  
  chs=!p.charsize
  !p.charsize=2
  !p.thick=2
  !p.charthick=2
  olddev=!d.name
  sz=size(tvrd())
  set_plot,'z' & userlct & erase
  device,set_resolution=[xsize,xsize/float(sz(1))*sz(2)]
  anigif='idens_movie.gif'
  az=azi & pl=pol
  repeat begin
    for i=0,n_elements(azi)-1 do begin
      widget_control,x2dwg.ax.id,set_value=az(i)
      widget_control,x2dwg.az.id,set_value=pl(i)
      x2dwg.ax.val=az(i) & x2dwg.az.val=pl(i)
      cut_twod
      tvlct,r,g,b,/get
      img=tvrd()
      writeg1,anigif,img,r,g,b,/multiple,delay=50,/loop
    endfor
    az=reverse(azi)
    pl=reverse(pol)
    revcnt=revcnt+1
  endrep until revcnt eq 2
  
  writeg1,anigif,/close
  set_plot,olddev
  !p.thick=0
  !p.charthick=0
  !p.charsize=chs
end


function get_zrg,ip
  common sav,sav
  common par,par
  
  cpar=sav.(ip)
;if n_elements(cpar) gt 10 then mcpar=median(cpar,11) else mcpar=cpar
  mcpar=cpar
  zrg=([min(mcpar,/nan),max(mcpar,/nan)]>par.range(0,ip))<par.range(1,ip)
  
  if par.pm_sym(ip) eq 1 then zrg=[-(max(abs(zrg),/nan)),+(max(abs(zrg),/nan))]
  
  par.act_range(*,ip)=zrg
  return,zrg
end

function med_filter,csav

  mval=3                        ;value for median filter
  sz=size(csav)
  csav=median(csav,mval)        ;median filter over whole image
  
                                ;median filter over boundaries
  csav(0,*)=median((csav(0,*))(*),mval)
  csav(*,0)=median(csav(*,0),mval)
  csav(sz(1)-1,*)=median((csav(sz(1)-1,*))(*),mval)
  csav(*,sz(2)-1)=median(csav(*,sz(2)-1),mval)
  
  return,csav
end



function val_select
  common wg2,wg2
  common par,par
   common sav,sav
 
  valid=byte(sav.(0)*0)+1b
  if n_elements(wg2) ne 0 then begin
    for i=0,n_elements(wg2.par)-1 do if wg2.par(i).flag.val eq 1 then begin
      mn=wg2.par(i).min.val
      mx=wg2.par(i).max.val
      if mn lt mx then valid=valid and (sav.(i) ge mn and sav.(i) le mx)
      if mx lt mn and mx eq 0 then valid=valid and (sav.(i) ge mn)
      if mn gt mx and mn eq 0 then valid=valid and (sav.(i) le mx)
    endif
  endif
  return,valid
end

function hnf,x=xarr,y=yarr,k=k,d=d
  
  a=k
  b=-1.
  
  dist=( a*xarr + b*yarr + d)/sqrt(a^2+b^2)
  return,dist
end

function pixel2data,xy,reverse=reverse
  common msk,msk
  common sav,sav
  
  if n_elements(xy) eq 2 then xyuse=reform(xy,1,2) else xyuse=xy
  sz=size(sav.(0))
  xcr=msk.plot(0,0).xrange
  ycr=msk.plot(0,0).yrange
  
  x=xyuse(*,0)
  y=xyuse(*,1)
  if keyword_set(reverse) then begin
    xret=(x-xcr(0))/(xcr(1)-xcr(0))*sz(1)
    yret=(y-ycr(0))/(ycr(1)-ycr(0))*sz(2)
  endif else begin
    xret=(x/sz(1))*(xcr(1)-xcr(0))+xcr(0)
    yret=(y/sz(2))*(ycr(1)-ycr(0))+ycr(0)
  endelse
  
  return,[[xret],[yret]]
end

pro calc_idens
  common coord,coord
  common sav,sav
  common x2dwg,x2dwg
  common msk,msk

  if n_elements(msk) eq 0 then return
  
  print,'calc_idens not yet available.'
  return
  
  print,'calc_idens only works for the first component'
  
                                ;calculate current density using
                                ;mag-field gradient in direction of
                                ;c1->c2
  p0=coord.c1
  case x2dwg.cut_type.val of 
    0: p1=coord.c3
    1: p1=coord.c2 
  endcase
  
                                ;convert data coords to pixels
  p0=pixel2data(/reverse,p0)
  p1=pixel2data(/reverse,p1)
  
  dx=p1(0)-p0(0)
  dy=p1(1)-p0(1)
  
  bfhe=sav.bhe
  bfsi=sav.bsi
  bfsi_inv=sav.inv_si_b
  
  sz=size(bfhe)
  bgradhe=fltarr(sz(1),sz(2))
  idcalche=byte(bgradhe)
  bgradsi=fltarr(sz(1),sz(2))
  idcalcsi=byte(bgradsi)
  bgradsiinv=fltarr(sz(1),sz(2))
  idcalcsiinv=byte(bgradsiinv)
  
  k=(p0(1)-p1(1))/(p0(0)-p1(0))
  xarr0=(fltarr(sz(2))+1) ## findgen(sz(1))
  yarr0=findgen(sz(2)) ## (fltarr(sz(1))+1)
  d2=sqrt(2.)/2.
  for ix=0,sz(1)-1 do for iy=0,sz(2)-1 do if idcalche(ix,iy) eq 0 then begin
                                ;select pixels along p0->p1 path
    d=iy-k*ix
    dist=hnf(x=xarr0,y=yarr0,k=k,d=d)
    use=where(dist ge -d2 and dist lt d2)
    
    nu=n_elements(use)
    if nu gt 3 then begin
      xvec=xarr0(use)
      yvec=yarr0(use)
      xcut=(1./k * xvec + yvec - d)/(k+1./k)
      ycut=k*xcut+d
;      plots,xcut,ycut,psym=3,color=0
      
      xycut_data=pixel2data([[xcut],[ycut]])
      xcut_data=xycut_data(*,0)
      ycut_data=xycut_data(*,1)
      
      rcut_data=sqrt(xcut_data^2+ycut_data)
      srt=reverse(sort(rcut_data))
      rcut_data=rcut_data(srt)
      nrg=(nu/2)>4
      rcut_reg= $
        findgen(nrg)/(nrg-1)*(rcut_data(nu-1)-rcut_data(0))+rcut_data(0)
      
      for is=0,2 do begin
        case is of
          0: begin
            cos_gamma=cos(((sav.inche)(use))(srt)*!dtor)
            bf_data=(bfhe(use))(srt) * cos_gamma
          end
          1: begin
            cos_gamma=cos(((sav.incsi)(use))(srt)*!dtor)
            bf_data=(bfsi(use))(srt) * cos_gamma
          end
          2: begin
            cos_gamma=cos(((sav.inv_si_inc)(use))(srt)*!dtor)
            bf_data=(bfsi_inv(use))(srt) * cos_gamma
          end
        endcase
          
        ipol=interpol(bf_data,rcut_data,rcut_reg,/lsq)
        deripol=deriv(rcut_reg,ipol)
        der=interpol(deripol,rcut_reg,rcut_data)
        
        case is of
          0: begin
            bgradhe(use(srt))=der + bgradhe(use(srt))
            idcalche(use(srt))=idcalche(use(srt)) + 1
          end
          1: begin
            bgradsi(use(srt))=der + bgradsi(use(srt))
            idcalcsi(use(srt))=idcalcsi(use(srt)) + 1
          end
          2: begin
            bgradsiinv(use(srt))=der + bgradsiinv(use(srt))
            idcalcsiinv(use(srt))=idcalcsiinv(use(srt)) + 1
          end
        endcase
      endfor
      
    endif
    
  endif
  valid=where(idcalche gt 0)
  if valid(0) ne -1 then bgradhe(valid)=bgradhe(valid)/idcalche(valid)
  bgradhe=abs(bgradhe)
  valid=where(idcalcsi gt 0)
  if valid(0) ne -1 then bgradsi(valid)=bgradsi(valid)/idcalcsi(valid)
  bgradsi=abs(bgradsi)
  valid=where(idcalcsiinv gt 0)
  if valid(0) ne -1 then bgradsiinv(valid)= $
    bgradsiinv(valid)/idcalcsiinv(valid)
  bgradsiinv=abs(bgradsiinv)
  
                                ;convert Mm->m, Gauss->T, A->mA
  idenshe=bgradhe / 1e4 / 1e6 / (4*!pi * 1e-7) * 1e3
  idenssi=bgradsi / 1e4 / 1e6 / (4*!pi * 1e-7) * 1e3
  idenssi_inv=bgradsiinv / 1e4 / 1e6 / (4*!pi * 1e-7) * 1e3
  
  sav.idenshe=med_filter(idenshe)
  sav.idenssi=med_filter(idenssi)
  sav.inv_idenssi=med_filter(idenssi_inv)

  draw_img
;  nv=where(idcalc eq 0)
;  if nv(0) ne -1 then idens(nv)=!values.f_nan
end

pro calc_2dbox
  common coord,coord
  
  x=[coord.c1(0),coord.c2(0),coord.c3(0)]
  y=[coord.c1(1),coord.c2(1),coord.c3(1)]
  
;  x=x(sort(x))
;  y=y(sort(y))

  x=[x , x(2)-(x(1)-x(0))]
  y=[y , y(2)-(y(1)-y(0))]
  
  for i=0,3 do $
    dummy=execute('coord.c'+n2s(i+1)+'=[x('+n2s(i)+'),y('+n2s(i)+')]')
  
end

function pix2Mm,x=x,y=y,reverse=reverse
  
  radius_Mm=700.
  radsun=asin(radius_Mm*1e3/1.48e8)/!dtor*3600
  xfact=(0.42d)/radsun*radius_Mm/cos(10.*!dtor)
  yfact=(0.37d)/radsun*radius_Mm/cos(30.*!dtor)
  
  if keyword_set(reverse) then begin
    xfact=1./xfact
    yfact=1./yfact
  endif
  
  if n_elements(x) ne 0 then retval=x*xfact
  if n_elements(y) ne 0 then retval=y*yfact
  
  return,retval
end

pro draw_box
  common x2dwg,x2dwg
  common coord,coord
  
  
  case x2dwg.cut_type.val of 
    0: vec=[[coord.c1(0),coord.c3(0)],[coord.c1(1),coord.c3(1)]]
    1: vec=[[coord.c1(0),coord.c2(0),coord.c3(0),coord.c4(0),coord.c1(0)], $
            [coord.c1(1),coord.c2(1),coord.c3(1),coord.c4(1),coord.c1(1)]]
  end
  vec=transpose(vec)
  p0=coord.c1
  ar=0
  color=2
  case x2dwg.cut_type.val of 
    0: begin
      p1=coord.c3
    end
    1: begin
      p1=coord.c2 
      color=9
      ar=1
    end
  endcase
  
  plots,vec,color=color,/data
  
  p0=coord.c1
  case x2dwg.cut_type.val of 
    0: p1=coord.c3
    1: p1=coord.c2 
  endcase
  dx=p1(0)-p0(0)
  dy=p1(1)-p0(1)
  
  if ar then arrow,color=1,/data,p0(0),p0(1),p0(0)+dx*0.5,p0(1)+dy*0.5
end

pro draw_img
  common x2dwg,x2dwg
  common par,par
  common sav,sav
  common wg2,wg2
  common coord,coord
  common msk,msk
  common color,color
  
  if !d.name ne 'PS' then begin
    widget_control,x2dwg.img.id,get_value=widx
    wset,widx
  endif
  
  if par.color(par.img) eq 0 then color=0
  userlct & erase
  color=1
  
  dummy=execute('cpar='+par.var(par.img))
  sz=size(cpar)
  
  xrg=[0,sz(1)] & yrg=[0,sz(2)]
  zrg=get_zrg(par.img)
  xcoord=findgen(sz(1)+1)
  ycoord=findgen(sz(2)+1)  
  
  xcoord=pix2Mm(x=xcoord)
  xrg=pix2Mm(x=xrg)
  ycoord=pix2Mm(y=ycoord)
  yrg=pix2Mm(y=yrg)
  
  msk=mask([1,1],title=par.name(par.img),nolabel=0, $
            xformat='(i4)',yformat='(i4)',zformat='(f10.2)', $
            zrange=zrg,xrange=xrg,yrange=yrg, $
            fix_margins=[0.1,0.86], $
            pos=[0,0,1,1],headtitle=' ', $
            small_label=0,short_label=0,$
            zlog=0,log_min=1e-3, $
            subtitle='',isotropic=1, $
            xtitle='x',ytitle='y', $
           dztitle=par.label(par.img));, $           charsize=!p.charsize)
  valid=val_select()
  novalid=where(valid ne 1)
  
  msk_plot,0,msk.label(0),/nocontour
  msk_plot,cpar,msk.plot(0,0),xcoord=xcoord,ycoord=ycoord, $
    novalid=novalid,ex_ps_quality=0.5,exact=1,/nocontour,nv_color=15
  
end

pro idens_flag,surf=surf,col=col
  common par,par
  common x2dwg,x2dwg
  
  densidx=where(strpos(par.name,'dens') ne -1)

  if n_elements(surf) ne 0 then idcalc=max(surf(0) eq densidx) $
  else idcalc=0
  if x2dwg.cut_type.val eq 1 and n_elements(col) ne 0 then $
    idcalc=idcalc or max(col(0) eq densidx)
  if idcalc then calc_idens
end
  
pro cut_twod
  common x2dwg,x2dwg
  common coord,coord
  common par,par
  common sav,sav
  common color,color
  common msk,msk
  common two_d,two_d
  
  
  
  if x2dwg.cut_type.val eq 0 then return
  
  tuidx=where(two_d.use)
  if tuidx(0) eq -1 then return
  
  if !d.name ne 'PS' and !d.name ne 'Z'  then begin
    widget_control,x2dwg.plot.id,get_value=widx
    wset,widx
    wd=640. & hg=360+60*total(two_d.use)
    widget_control,x2dwg.plot.id,xsize=wd,ysize=hg
  endif
  
  ipt=0
  np=n_elements(tuidx)
  !p.multi=[0,1,np]
  erase
  for iu=0,np-1 do begin
    coli=two_d(tuidx(iu)).col
    surfi=two_d(tuidx(iu)).surf
    irg=get_zrg(coli)
  
    if par.color(coli) eq 0 then color=0
    userlct 
    color=1
  
  cpar=sav.(coli)
  sz=size(cpar)
  x=[coord.c1(0),coord.c2(0),coord.c3(0),coord.c4(0)]
  y=[coord.c1(1),coord.c2(1),coord.c3(1),coord.c4(1)]
  xy=pixel2data(/reverse,[[x],[y]])
  x=xy(*,0)
  y=xy(*,1)
  uidx=polyfillv(x,y,sz(1),sz(2))
  use=byte(cpar*0)
  use(uidx)=1b
  ux=uidx mod sz(2)
  uy=long(uidx / sz(2))
  
  xrg=[x(0),x(1)]
  yrg=[y(0),y(3)]
  
  xarr=(intarr(sz(2))+1) ## findgen(sz(1))
  yarr=findgen(sz(2)) ## (intarr(sz(1))+1)
  
  xrg=pix2Mm(x=xrg)
  yrg=pix2Mm(y=yrg)
  xarr=pix2Mm(x=xarr)
  yarr=pix2Mm(y=yarr)

  valid=val_select()
  novld=where(valid(uidx) ne 1)
  
  if n_elements(uidx) gt 2 then begin
    y=1-float((ipt+[1,0]))/np
;    y=float((ipt+[1,0]))/np
    dy=y(1)-y(0)
    pp=[0.1,y(0)+dy*.03,0.9,y(1)-dy*.03]
;    pp=[0.1,y(1)-dy*.03,0.9,y(0)+dy*.03]
    
    zval=(sav.(surfi))(uidx)
    zcolval=(sav.(coli))(uidx)
    if novld(0) ne -1 then begin
      zval(novld)=!values.f_nan
      zcolval(novld)=!values.f_nan
    endif
    xval=xarr(uidx)
    yval=yarr(uidx)
    triangulate,xval,yval,triarr,boundary
    
;    npt=61
    surf=trigrid(xval,yval,zval,triarr,nx=npt,ny=npt,missing=!values.f_nan, $
                 xgrid=xout,ygrid=yout)
    surf_col=trigrid(xval,yval,zcolval,triarr,missing=!values.f_nan)
    surf_col=(((surf_col>min(par.range(*,coli)))<max(par.range(*,coli))) - $
              irg(0))/(irg(1)-irg(0))*(254-17)+17

;print,!p.font & !p.font=1    
    shade_surf,surf,xout,yout,noerase=1,shade=surf_col,image=image, $
      pixels=2048,xst=1,yst=1,zst=9,/save,ax=x2dwg.ax.val,az=x2dwg.az.val, $
;      xticklen=1,yticklen=1,zticklen=1, $
      xtitle='x [Mm]',ytitle='y [Mm]', $
      ztitle=par.label(surfi)               ;,position=pp
    newplot=0


    if newplot eq 0 then begin
    endif else begin
      edgex=[!x.crange([0,0,0,0]),!x.crange([1,1,1,1])]
      edgey=[!y.crange([0,1,0,1]),!y.crange([0,1,0,1])]
      edgez=[!z.crange([0,0,1,1]),!z.crange([0,0,1,1])]
      edge=transpose([[edgex],[edgey],[edgez]])
      dev=convert_coord(/data,/to_device,edge,/t3d)
      xmin=min(dev(0,*)) & ymin=min(dev(1,*))
      xmax=max(dev(0,*)) & ymax=max(dev(1,*))
      plot_3dbox,xval([0,0]),yval([0,0]),zval([0,0]), $
        xrange=!x.crange,yrange=!y.crange,zrange=!z.crange, $
        /nodata,xst=1,yst=1,zst=1,/noerase, $
;        /save,ax=x2dwg.ax.val,az=x2dwg.az.val, $
      /t3d, $
        xtitle='x [Mm]',ytitle='y [Mm]', $
        ztitle=par.label(surfi) ;,position=pp
      szi=size(image)
      bx=(float(xmax-xmin)/szi(1))>1
      by=(float(ymax-ymin)/szi(2))>1
      for ix=0l,szi(1)-1 do for iy=0l,szi(2)-1 do begin
        if image(ix,iy) ge 17 then begin
          if (!d.flags and 1) ne 0 then $ ;scalable pixels ?
            tv,bytarr(2,2)+image(ix,iy),xmin+ix*bx,ymin+iy*by,xsize=bx,ysize=by $
          else tv,bytarr(bx,by)+image(ix,iy),xmin+ix*bx,ymin+iy*by
        endif
        proc=float(ix*szi(2)+iy)
        if fix((proc-1)/(szi(1)*szi(2))*100) ne $
          fix((proc)/(szi(1)*szi(2))*100) then $
          print,proc/(szi(1)*szi(2))*100
      endfor
    endelse
    surface,surf,xout,yout,noerase=1,xst=5,yst=5,zst=5,/t3d;,pos=pp
    
  
;     print,!x.region,!y.region,!x.window
;    print,size(image) & tv,image
    
    lb=[.95,pp(3)-dy*.4,.99,pp(3)-dy*.1]
  lbpos=(convert_coord(/normal,/to_device, $
                       [[lb(0),lb(1)],[lb(2),lb(3)]]))([0,1,3,4])  
  lbpold=msk.label(0).position
  zt=msk.label(0).ytitle
  yr=msk.label(0).yrange & zr=msk.label(0).zrange
  msk.label(0).yrange=irg & msk.label(0).zrange=irg
  ytn=msk.label(0).ytickname
  msk.label(0).ytickname=''
  msk.label(0).ytitle=par.label(coli)
  msk.label(0).position=lbpos
  msk.label(0).yt_flag=0
  dmy=msk_setrange(msk.label(0))
  msk_plot,0,msk.label(0),/nocontour
  msk.label(0).yt_flag=1
  msk.label(0).position=lbpold
  msk.label(0).ytitle=zt
  msk.label(0).ytickname=ytn
  msk.label(0).yrange=yr & msk.label(0).zrange=zr

  
  if iu eq 0 then begin
  cutstr=''
  for ic=0,3 do $
    cutstr=cutstr+'('+add_comma(sep=',',n2s(fix(coord.(ic))))+')'+ $
    (['-',''])(ic eq 3)
  xyouts,lb(2),0,$;,pp(3)-dy*0.06, $
    /normal,alignment=1,'!CRegion:!C'+cutstr, $
    charsize=!p.charsize*.8
  endif
  
  xyouts,.1,pp(3),/normal,charsize=0.9*!p.charsize, $
  '!CSurface: '+par.name(surfi)+'!CColor: '+par.name(coli)
  
    ipt=ipt+1
  endif
  !p.multi(0)=(!p.multi(2)-iu-1) mod !p.multi(2)
  endfor
  
end

pro cut_oned
  common x2dwg,x2dwg
  common coord,coord
  common par,par
  common sav,sav
  common two_d,two_d
  
  
  !p.multi=0
  sz=size(sav.(0))  
  left=coord.c1
  right=coord.c3
      wd=640. & hg=360+60*total(two_d.use)
    widget_control,x2dwg.plot.id,xsize=wd,ysize=hg

  use=byte(sav.(0)*0)
  xc=[left(0),right(0)] & yc=[left(1),right(1)]
  dx=xc(1)-xc(0) & dy=yc(1)-yc(0)
  nrs=long(sqrt(dx^2+dy^2)*2+1)
  rvec=findgen(nrs)
  for i=0,nrs-1 do begin
    xdat=xc(0)+float(i)/nrs*dx
    ydat=yc(0)+float(i)/nrs*dy
    x=pix2Mm(/reverse,x=xdat)
    y=pix2Mm(/reverse,y=ydat)
    if x ge 0 and x lt sz(1) and y ge 0 and y lt sz(2) then use(x,y)=1b
  endfor
  uidx=where(use)
  nu=n_elements(uidx)
  
  tuidx=where(two_d.use eq 1)
  if tuidx(0) eq -1 then return
  upar=two_d(tuidx).surf
  np=n_elements(upar)
  yrg=fltarr(2,np)
  ytit=strarr(np)
  dytit=par.label(upar)
  incflag=strpos(par.name,'inc') eq 0
  for ip=0,np-1 do begin
    dummy=execute('yvec='+par.var(upar(ip))+'(uidx)')
    if n_elements(yvec) gt 5 then yvec=median(yvec,3)
    yrg(*,ip)=([min(yvec),max(yvec)]> $
               par.range(0,upar(ip)))<par.range(1,upar(ip))
    if incflag(upar(ip)) then begin
      dummy=execute('yvecazi=sav.azi'+strmid(par.var(upar(ip)),7,100)+'(uidx)')
      yrg(*,ip)=[yrg(0,ip)<min(yvecazi),yrg(1,ip)>max(yvecazi)]
      dytit(ip)='Azimuth /!C'+par.label(upar(ip))
    endif
    yrg(*,ip)=yrg(*,ip)+[-.1,.1]*(yrg(1,ip)-yrg(0,ip))
  endfor
  
  if uidx(0) ne -1 then begin
    ruse=(findgen(nu)+.5)/nu*(nrs-1)+sqrt(xc(0)^2+yc(0)^2)
    xuse=(findgen(nu)+.5)/nu*(xc(1)-xc(0))+xc(0)
    yuse=(findgen(nu)+.5)/nu*(yc(1)-yc(0))+yc(0)
    ruse=sqrt((xuse-xuse(0))^2+(yuse-yuse(0))^2)
    
    if !d.name ne 'PS' then begin
      widget_control,x2dwg.plot.id,get_value=widx
      wset,widx
    endif
    userlct & erase
    
    xrg=[min(ruse),max(ruse)]        
    xystr='('+add_comma(n2s(fix(left),format='(i4.4)'),sep=',')+')-('+ $
      add_comma(n2s(fix(right),format='(i4.4)'),sep=',')+')'
    msk2=mask([1,np],nolabel=1,label=0, $
;              headtitle='Cut '+xystr, $
    headtitle='', $
              xformat='(i4)',yformat='(f12.2)', $
              xrange=xrg,yrange=yrg, $
              fix_margins=[0.1,0.96], $
              pos=[0,0,1,1], $
              small_label=0,short_label=0,$
              log_min=1e-3,big_xlabel=0.1, $
              subtitle='',$     ;ylog=[0,1,0,0], $
              xtitle='!CR, X, Y [Mm]', $
              additional_xlabel=[[ruse],[fix(xuse)],[fix(yuse)]], $
              dytitle=dytit);,charsize=!p.charsize)
    
    
    valid=val_select()
    vld=where(valid(uidx) eq 1)
    xc=ruse
    if n_elements(vld) ge 2 then xc=ruse(vld)
    for ip=0,np-1 do begin
      dummy=execute('yvec='+par.var(upar(ip))+'(uidx)')
      if n_elements(vld) ge 2  then yvec=yvec(vld)
      if n_elements(yvec) gt 5 then yv=median(yvec,3) else yv=yvec
      
      pp= msk2.plot(0,ip).position
      dp=[pp(2)-pp(0),pp(3)-pp(1)]
      msk2.plot(0,ip).lab_position=[pp(0)+.05*dp(0),pp(1)+.6*dp(1), $
                                    pp(0)+.5*dp(0),pp(1)]
      msk2.plot(0,ip).lab_sys='device'
      lbs=par.name(upar(ip))
      if incflag(upar(ip)) eq 0 then  dummy=temporary(lbs)
      msk_plot,[[xc],[yv]],msk2.plot(0,ip),/onedim, $
        line_thick=(!d.name eq 'PS')+1,lab_string=lbs
      if incflag(upar(ip)) then begin
        aidx=where(par.name eq 'azi'+ strmid(par.name(upar(ip)),3,100))
        dummy=execute('yvecazi='+par.var(aidx)+'(uidx)')
        if n_elements(vld) ge 2  then yvecazi=yvecazi(vld)
        if n_elements(yvec) gt 5 then yv=median(yvecazi,3) else yv=yvecazi
        msk_plot,[[xc],[yv]],msk2.plot(0,ip),/onedim, $
          line_thick=(!d.name eq 'PS')+1 ,color=3,lab_string=par.name(aidx)
      endif
      if upar(ip) eq 11 then oplot,!x.crange,[0,0],linestyle=1
;       if upar(ip) eq 8 then begin
;         oplot,!x.crange,[90,90],linestyle=1
;         xyouts,/data,!x.crange(0),90,'!C     (horizontal field)', $
;           charsize=0.6*!p.charsize
;       endif
    endfor
    
  endif
end

pro loop_click,x,y,button
  common x2dwg,x2dwg
  common par,par
  common sav,sav
  common x2dold,lo,ro,left,right
  common coord,coord
  
  if n_elements(coord) eq 0 then begin
    coord={c1:fltarr(2),c2:fltarr(2),c3:fltarr(2),c4:fltarr(2)}
    coord.c1=[10,10]
    coord.c2=[20,20]
    coord.c3=[30,25]
    calc_2dbox
  endif
  set_full_unit
  
  if n_elements(button) eq 0 then button=-1
  if button eq 0 then return
  
  
  if n_elements(x) eq 1 then begin
    draw_img
    xydat=convert_coord(/device,/to_data,[x,y])
    x=xydat(0)
    y=xydat(1)
    case button of
      1: coord.c1=[x,y]
      2: coord.c2=[x,y]
      4: coord.c3=[x,y]
      else:
    endcase
    calc_2dbox
    calc_idens    
    draw_box
  endif
  
   
  
  if !d.name ne 'PS' then begin
    widget_control,x2dwg.img.id,get_value=widx
    wset,widx
  endif
  
  case x2dwg.cut_type.val of
    0: cut_oned
    1: cut_twod
  endcase
  
end

pro xwg2_event,event
  common wg2,wg2
  common par,par

  widget_control,event.id,get_uvalue=uval
  
  id=strmid(uval,0,2)
  str=strmid(uval,2,strlen(uval))

  case str of
    'flag': begin
      wg2.par(id).flag.val=event.select
    end
    'clear': begin
      wg2.par(id).min.val=0 & wg2.par(id).max.val=0
      widget_control,wg2.par(id).min.id,set_value=wg2.par(id).min.val
      widget_control,wg2.par(id).max.id,set_value=wg2.par(id).max.val
    end
    'min': begin
      wg2.par(id).min.val=event.value
    end
    'max': begin
      wg2.par(id).max.val=event.value      
    end
    'update': begin
      draw_img
      draw_box
      loop_click
    end
    else: begin
      help,/st,event
      print,uval
    end
  endcase
    
end

pro range_selector
  common wg2,wg2
  common par,par
  common x2dwg,x2dwg
  
  np=n_elements(par.name)
  subst={id:0l,val:0.,str:''}
  wgsub={flag:subst,min:subst,max:subst,clear:subst}
  
  screen=get_screen_size()
  xs=400
  ys=0.8*screen(1)
  
  wg2={base:subst,par:replicate(wgsub,np),update:subst}
  
  wg2.base.id=widget_base(title='Range Selector',col=1, $
                          group_leader=x2dwg.base.id,/floating, $
                          y_scroll_size=ys,x_scroll_size=xs)
  
  for i=0,np-1 do begin
    line=widget_base(wg2.base.id,row=1)
    
    is=n2s(i,format='(i2.2)')
    wg2.par(i).flag.str=par.name(i)

    wg2.par(i).flag.id=cw_bgroup(line,uvalue=is+'flag',xsize=80, $
                                 set_value=wg2.par(i).flag.val, $
                                 wg2.par(i).flag.str ,/nonexclusive)
    wg2.par(i).min.id=cw_field(line,title='min:',uvalue=is+'min',/float, $
                               value=wg2.par(i).min.val,/all_events,xsize=8)
    wg2.par(i).max.id=cw_field(line,title='max:',uvalue=is+'max',/float, $
                               value=wg2.par(i).max.val,/all_events,xsize=8)
    
    wg2.par(i).clear.id=cw_bgroup(line,['Clear'],uvalue=is+'clear')
  endfor
  wg2.update.id=cw_bgroup(wg2.base.id,['          Update          '], $
                          uvalue='  update')
  
  
  widget_control,/realize,wg2.base.id
  xmanager,'xwg2',wg2.base.id,/no_block
  
end
  
pro xwgpar_event,event
  common wgpar,wgpar
  common wgpar_val,wgpar_val
  
  widget_control,event.id,get_uvalue=uval
  
  oval=wgpar_val
  case uval of 
    'par': begin
      wgpar_val=event.value
    end
    'control': begin
      case event.value of
        'cancel': begin
          wgpar_val=-1
          widget_control,wgpar.base.id,/destroy
        end
      endcase  
    end
  endcase
  if wgpar_val ne -1 and oval ne -1 then widget_control,wgpar.base.id,/destroy
end

function wg_par,value=value,add
  common wgpar,wgpar
  common x2dwg,x2dwg
  common par,par
  common sav,sav
  common wgpar_val,wgpar_val
  
  if n_elements(value) eq 0 then value=0
  subst={id:0l,val:0.,str:''}
  wgpar={base:subst,par:subst,control:subst}
  wgpar_val=-1
  
  if n_elements(add) eq 0 then add=''
  wgpar.base.id=widget_base(title='Select Plot Parameters '+add, $
                            group_leader=x2dwg.base.id,col=1,/floating)
  
  
  wgpar.par.id=cw_bgroup(wgpar.base.id,uvalue='par',par.name,col=8, $
                         /exclusive,set_value=value)
  wgpar.control.id=cw_bgroup(wgpar.base.id,['  Cancel  '], $
                            uvalue='control',row=1,frame=1, $
                            button_uvalue=['cancel'])

  
  widget_control,/realize,wgpar.base.id
  xmanager,'xwgpar',wgpar.base.id
  
  
  return,wgpar_val
end


pro x2dwg_event,event
  common x2dwg,x2dwg
  common par,par
  common coord,coord
  common two_d,two_d

  IF TAG_NAMES(event, /STRUCTURE_NAME) EQ $
    'WIDGET_KILL_REQUEST' THEN begin
    widget_control,X2DWG.BASE.ID,/DESTROY
    RETALL
  endif

  widget_control,event.id,get_uvalue=uval
  
  last=strmid(uval,strlen(uval)-1,1)
  if last ge '0' and last le '9' then begin
    uval=strmid(uval,0,strlen(uval)-1)
    upar=fix(last)
  endif
  
  case uval of 
    'par_img': begin
      if event.select then begin
        value=wg_par(value=par.img)
        if value ne -1 then begin
          par.img=value
          calc_idens
          draw_img
          draw_box
          loop_click
        endif
      endif
    end
    'img': begin
      loop_click,event.x,event.y,event.press
    end
    'par_plot': begin
      doit=0
      case event.value of 
        'surf': begin
          surf=wg_par(value=two_d(upar).surf)
          if surf ne -1 then begin
            doit=surf ne two_d(upar).surf
            two_d(upar).surf=surf
          endif  
        end
        'col': begin
          col=wg_par(value=two_d(upar).col)
          if col ne -1 then begin
            doit=col ne two_d(upar).col
            two_d(upar).col=col
          endif  
        end
      endcase
      if doit then begin
        two_d(upar).use=1
        widget_control,x2dwg.par_use(upar).id,set_value=1
        idens_flag,surf=surf,col=col
        if x2dwg.cut_type.val eq 1 then begin
;           calc_2dbox
;           calc_idens    
;           draw_box
          cut_twod
        endif else begin
          cut_oned
        endelse
      endif
    end
    'par_use': begin
      oval=two_d(upar).use
      two_d(upar).use=event.select
      if oval ne two_d(upar).use then begin
        loop_click
      endif
    end
    'ax': begin
      x2dwg.ax.val=event.value
      cut_twod
    end
    'az': begin
      x2dwg.az.val=event.value
      cut_twod
    end
    'cut_type': begin
      oval=x2dwg.cut_type.val
      x2dwg.cut_type.val=event.value
      loop_click
      if x2dwg.cut_type.val eq 0 then begin
        coord.c1=[10,10]
        coord.c2=[20,20]
        coord.c3=[30,25]
      endif
      if x2dwg.cut_type.val ne oval then begin
        if x2dwg.cut_type.val eq 0 then begin
          coord.c1=[12.771,6.947]
          coord.c2=[20,20]
;          coord.c3=[30.474,20.209]
          coord.c3=[29.590,19.704]
        endif
        draw_img
        draw_box
      endif
    end
    'control': begin
      case event.value of
        'print': begin
          och=!p.charsize
          wd=18 & hg=14.
          xystr=add_comma(n2s(fix(coord.c1),format='(i4.4)'),sep='.')+'_'+ $
            add_comma(n2s(fix(coord.c2),format='(i4.4)'),sep='.')
          psfile='He10830_'+xystr
          imgfile=psfile+'_'+par.name(par.img)+'.img.ps'
          psout=ps_widget(default=[0,0,0,0,8,0,0],size=[wd,hg], $
                          psname=psn_img, $
                          file=imgfile,group_leader=x2dwg.base.id)
          if psout(1) ne 1 then begin
            if !d.name eq 'PS' then begin
              cutstr_dev='' & cutstr_dat=''
              if x2dwg.cut_type.val eq 0 then cu=[1,0,1,0] else cu=[1,1,1,1]
              for ic=0,3 do if cu(ic) then begin
                cx=pix2Mm(/reverse,x=(coord.(ic))(0))
                cy=pix2Mm(/reverse,y=(coord.(ic))(1))
                cutstr_dev=[cutstr_dev, $
                            '('+add_comma(sep=',',n2s(fix([cx,cy])))+')']
                cutstr_dat=[cutstr_dat, $
                            '('+add_comma(sep=',',n2s(format='(f11.3)', $
                                                      coord.(ic)))+')']
              endif
              cutstr_dev=add_comma(sep='-',cutstr_dev,/no_empty)
              cutstr_dat=add_comma(sep='-',cutstr_dat,/no_empty)
              cutstr='!CDevice: '+cutstr_dev+'!CData: '+cutstr_dat
              
              set_full_unit
              !p.font=1 & !p.charsize=1.1*!p.charsize
              draw_img
              draw_box
              xyouts,/normal,0,0,cutstr,charsize=0.8*!p.charsize
              device,/close
              wd=20 & hg=7+4*total(two_d.use)
              plotfile=psfile+'_'+add_comma(n2s(par.plot),sep='')+'.par.ps'
              psout2=ps_widget(default=[0,0,0,0,8,0,0],size=[wd,hg], $
                               psname=psn_plot,/no_x, $
                               file=plotfile,group_leader=x2dwg.base.id)
              loop_click
              xyouts,/normal,0,0,cutstr,charsize=0.8*!p.charsize
              device,/close
              !p.charsize=och
              if psout(3) then begin
                spawn_gv,psn_img
                spawn_gv,psn_plot
              endif  
            endif
          endif
          if psout(1) eq 1 then return
          set_plot,'X'
          set_full_unit
          !p.font=-1
          !p.charsize=och
        end
        'range': begin
          range_selector
        end
        'exit': begin
          widget_control,x2dwg.base.id,/destroy
          retall
        end
        else:
      endcase
    end
    else: begin
      help,/st,event
      print,uval
    end
  endcase

end


pro set_full_unit
  common greek_letters
  common par,par
  
  
;   greek
;   par.full=['A0 (He)','A0!L2!N (He)','Azimuth (He)','Field Strength', $
;             'Field Strength 2 (He)','Max. Field Strength (He)','chisqhe', $
;             'Intensity (He)','Inclination','LOS Velocity (He)', $
;             'LOS Velocity 2 (He)','LOS Velocity', $
;             'A0 (Si)','LOS Velocity  (Si)','Field Strength (Si)','chisqsi', $
;             'Intensity (Si)','Azimuth (Si)','Inclination (Si)', $
;             'Current Density (He)','Current Density (Si)', $
;            'inv_si_chisqr','I!Lc!N Obs (Si)','I Fit (si)','I Obs (si)', $
;             'Q Fit (Si)','Q Obs (Si)','U Fit (Si)','U Obs (Si)', $
;             'V Fit (si)','V Obs (Si)','Temperature (Si)','Mag Field (Si)', $
;             'Inclination (Si)','Azimuth (Si)','LOS Velocity (Si)', $
;             'Filling Factor', $
;             'Current Density (Si, inv)']
;   par.unit=['','','['+f_grad+']','[G]','[G]','[G]','','','['+f_grad+']', $
;             '[km/s]','[km/s]','[km/s]','','[km/s]','[G]','', $
;             '','['+f_grad+']','['+f_grad+']','[mA/m!E2!N]','[mA/m!E2!N]', $
;             '','','','', $
;             '','','','', $
;             '','','['+f_grad+']','[G]', $
;             '['+f_grad+']','['+f_grad+']','[km/s]','', $
;             '[mA/m!E2!N]']
end

pro x2d_widget
  common x2dwg,x2dwg
  common par,par
  common greek_letters
  common sav,sav
  common two_d,two_d
  
  
  if n_tags(x2dwg) gt 0 then begin
    widget_control,bad_id=bid,x2dwg.base.id
    if bid eq 0 then if x2dwg.base.id ne 0 then $
      widget_control,/destroy,x2dwg.base.id
  endif

  np=n_tags(sav)
  maxsurf=8
  subst={id:0l,val:0.,str:''}
  x2dwg={base:subst,img:subst,plot:subst,control:subst, $
        par_img:subst,par_plot:replicate(subst,maxsurf), $
        par_use:replicate(subst,maxsurf), $
        cut_type:subst,ax:subst,az:subst}
  
  xs=640
  td={surf:0,col:0,use:0}
  two_d=replicate(td,maxsurf)
  
  x2dwg.cut_type.val=1
  
  screen=get_screen_size()
  if screen(0) lt 1400 then scy=screen(0)*.9
  if screen(1) lt 1024 then scy=screen(1)*.9
  x2dwg.base.id=widget_base(title='He 10830 Analyzer',col=1,xoff=0, $
                           /tlb_kill_request_events, $
                           x_scroll_size=scx, $
                           y_scroll_size=scy)
  
  row=widget_base(x2dwg.base.id,row=1)
  x2dwg.control.id=cw_bgroup(row,['Print','Exit','Select Range'], $
                            uvalue='control',row=1,frame=1, $
                            button_uvalue=['print','exit','range'])
  x2dwg.cut_type.id=cw_bgroup(row,uvalue='cut_type', $
                             ['1-D Cut','2-D Cut'], $
                             row=1,set_value=x2dwg.cut_type.val,/exclusive)
  

  sub=widget_base(x2dwg.base.id,/frame,col=1)
stop  
  sv=intarr(np) & sv(5)=1
;   x2dwg.par_img.id=cw_bgroup(sub,uvalue='par_img',par.name,col=8, $
;                             set_value=par.img,/exclusive)
  
  x2dwg.par_img.id=widget_button(sub,value='Select Image',uvalue='par_img')
  

  ys=300
  x2dwg.img.id=widget_draw(sub,xs=xs,ys=ys,/button_events, $
                          uvalue='img',/view)

  sub=widget_base(x2dwg.base.id,/frame,col=1)
  ys=450
  
;  x2dwg.par_plot.id=cw_bgroup(sub,uvalue='par_plot',par.name,col=8, $
;                            set_value=par.plot,/nonexclusive)
  sub3=widget_base(sub,row=1)
  for i=0,n_elements(x2dwg.par_plot.id)-1 do begin
    sub2=widget_base(sub3,/frame,col=1)
    x2dwg.par_use(i).id=cw_bgroup(sub2,uvalue='par_use'+n2s(i), $
                                 'Plot '+n2s(i+1), $
                                 set_value=x2dwg.par_use(i).val,/nonex)
    x2dwg.par_plot(i).id=cw_bgroup(sub2,col=1,['Surface','Color'], $
                            uvalue='par_plot'+n2s(i), $
                            button_uvalue=['surf','col'])
  endfor
  
  
  sub2=widget_base(sub,row=1)
  x2dwg.plot.id=widget_draw(sub2,xs=xs,ys=ys, $
                           uvalue='plot',/view)
  
  x2dwg.ax.val=40
  x2dwg.ax.id=widget_slider(sub2,uvalue='ax',/vertical,ysize=ys,min=0,max=360, $
                          value=x2dwg.ax.val,/drag)
  x2dwg.az.val=70
  x2dwg.az.id=widget_slider(sub,uvalue='az',xsize=xs,min=0,max=360, $
                           value=x2dwg.az.val,/drag)
  
  widget_control,/realize,x2dwg.base.id
  xmanager,'x2dwg',x2dwg.base.id,/no_block
end

;procedure to display two-d cut (current sheet figure in nature paper)
pro x_2dcut,img=img,zrg=zrg,xcexact=xcexact,ycexact=ycexact,val=val
  common x2dwg,x2dwg
  common wgst,wgst
  common param,param
;  common imgst,imgst
  common par,par
  common sav,sav
  common coord,coord
  common diswg,diswg
  
  sz=size(img)
  np=sz(1)
  nc=sz(2)
  npc=np*nc
  if npc lt 2 then return
  
  par={name:strarr(npc),label:strarr(npc),var:strarr(npc), $
       range:fltarr(2,npc),idx:strarr(npc), $
       img:0l,plot:intarr(npc),act_range:fltarr(2,npc),color:intarr(npc)+1, $
       pm_sym:intarr(npc)}
  par.img=0
  par.plot([3])=1
  par.pm_sym([19])=0
  for i=0,nc-1 do begin
    cs='Comp '+n2s(i)
    par.name(i*np:(i+1)*np-1)=param.name+' '+cs
;    par.var(i*np:(i+1)*np-1)='sav.'+param.name+n2s(i)
    par.var(i*np:(i+1)*np-1)=param.name+n2s(i)
    for ip=0,np-1 do $
      par.idx(i*np+ip)='img('+n2s(ip)+','+n2s(i)+',*,*)'
    par.label(i*np:(i+1)*np-1)=param.label+' '+cs
    par.range(*,i*np:(i+1)*np-1)=transpose(zrg)
    for ip=0,np-1 do begin
      if total(abs(diswg.zmin(ip).val-diswg.zmax(ip).val)) gt 1e-6 then $
        par.range(*,i*np+ip)=[diswg.zmin(ip).val,diswg.zmax(ip).val]
    endfor
  endfor
  
  
  if n_elements(coord) eq 0 then begin
    coord={c1:fltarr(2),c2:fltarr(2),c3:fltarr(2),c4:fltarr(2)}
    coord.c1=[21,24]
    coord.c2=[28,16]
    coord.c3=[39,25]
    calc_2dbox
  endif
                                ;construct sav-variable
  svcmd='sav={'+add_comma(sep=', ',par.var+':reform('+par.idx+')')+'}'
  dummy=execute(svcmd)
  par.var='sav.'+par.var
  
  for i=0,n_tags(sav)-1 do begin
    sav.(i)=med_filter(sav.(i))    
  endfor
  
;  !p.charsize=1.5
  x2d_widget

  draw_img
  draw_box
  loop_click
  
  ende=0
  
  
end
  
