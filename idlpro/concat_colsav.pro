;program to concat column-sav-files
function concat_colsav,sav=sav,no_del=no_del,delete=delete
  
  if n_elements(sav) eq 0 then begin
    sav=dialog_pickfile(filter='sav/*.sav')
    if sav eq '' then message,'Please specify sav-file.'
  endif
  
  colpos=strpos(sav,'.col')   ;read savfiles for different columns and
                                ;concat them into one savfile
  if colpos ne -1 then begin
    savmask=strmid(sav,0,colpos)+'.col*.sav'
    colsavs=findfile(savmask,count=n)
    if n eq 0 then message,'No column sav-files found.'
    colmax=max(long(strmid(colsavs,colpos+4,3))) > 2
    savinfo=strarr(n)
    for i=0,n-1 do begin
      spawn,'ls -l '+colsavs(i),si
      savinfo(i)=si
      print,'restoring '+colsavs(i)
      restore,colsavs(i)
      if i eq 0 then begin
        line=pikaia_result.line
        fit=pikaia_result.fit(0)
        for it=0,n_tags(fit.atm.par)-1 do begin
          fit.atm.par.(it)=0 & fit.atm.fit.(it)=0
          fit.x=0 & fit.y=0
          fit.fitness=0
        endfor
        pikaia={line:line,input:pikaia_result.input, $
                fit:replicate(fit,colmax+1,n_elements(pikaia_result.fit))}
      endif
      profflag=max(tag_names(pikaia_result.fit) eq 'PROFILE')
      for iy=0,n_elements(pikaia_result.fit)-1 do begin
        x=pikaia_result.fit(iy).x      
        for it=0,n_tags(fit)-1 do begin
          src=pikaia_result.fit(iy).(it)
          tgt=pikaia.fit(x,iy).(it)
          if size(src,/type) eq 8 then struct_assign,src,tgt else tgt=src
            pikaia.fit(x,iy).(it)=tgt
        endfor
;         for it=0,n_tags(fit.atm.par)-1 do begin
;           pikaia.fit(x,iy).atm.par.(it)=pikaia_result.fit(iy).atm.par.(it)
;           pikaia.fit(x,iy).atm.fit.(it)=pikaia_result.fit(iy).atm.fit.(it)
;         endfor
;         if profflag then begin
;           tprof=pikaia_result.fit(iy).profile
;           struct_assign,pikaia.fit(x,iy).profile,tprof                        
;         endif
;         pikaia.fit(x,iy).x=pikaia_result.fit(iy).x
;         pikaia.fit(x,iy).y=pikaia_result.fit(iy).y
;         pikaia.fit(x,iy).fitness=pikaia_result.fit(iy).fitness
       endfor
    endfor

    pikaia_result=temporary(pikaia)
    
    ns=n_elements(savinfo)
    timestr=strarr(ns)
    for i=0,ns-1 do begin
      tstr=(strsplit(/extract,savinfo(i)))(5:6)
      timestr(i)=add_comma(tstr,sep='_')
    endfor
    
    savtotal=strmid(sav,0,colpos)+'.'+ $
      (timestr(reverse(sort(timestr))))(0)+'.sav'
    dummy=findfile(savtotal,count=n)

    if n eq 1 then message,/cont,savtotal+' is already existing.' $
    else begin
      exsav=findfile(strmid(sav,0,colpos)+'.????-??-??_??:??.sav',count=nex)
      for i=0,nex-1 do begin
        extime=strmid(exsav(i),colpos+1,16)
        if extime ge min(timestr) and extime lt max(timestr) then begin
          print,'Delete old column-concat sav file: '+exsav(i)
          openw,/get_lun,/delete,unit,exsav(i) & free_lun,unit
        endif
      endfor
      
      print,'Creating sav-file: '+savtotal
      save,file=savtotal,pikaia_result,/compress,/xdr
    endelse
    
    if n_elements(delete) eq 1 then del=delete else del=0
    dummy=findfile(savtotal,count=n)
    if n eq 1 then begin
      yn=''
      if keyword_set(delete) then yn='y' $
      else if n_elements(delete) eq 1 and del eq 0 then yn='n' $
      else read,'Delet column-sav files [y/N]? ',yn
      if strlowcase(yn) eq 'y' then begin
        print,'Deleting column sav-files: '+min(timestr)+' - '+max(timestr)
        for i=0,ns-1 do begin
          print,'Deleting '+colsavs(i) 
          openw,/get_lun,/delete,unit,colsavs(i) & free_lun,unit
        endfor
        print,'All data stored in: '+savtotal
      endif
    endif
    
    retsav=savtotal
  endif else begin              ;restore original sav-file
    retsav=sav
  endelse
  
  
  
  return,retsav
end  
