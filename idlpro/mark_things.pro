pro mark_things,p2mm
  common pikaia
  
  sz=size(pikaia_result.fit)
  szx=pix2mm(sz(1),x=p2mm)
  szy=pix2mm(sz(2),y=p2mm)
  
  print,!x.crange,!y.crange
  
  contour,color=4,dist(szx,szy),xst=5,yst=5,/overplot,nlevels=50

return
  
  
                                ;p2mm is a flag.
                                ;It is 0 for pixel
                                ;plot, and 1 for Mm plot
  
  up2mm=0                       ;scaling of plot is always in pixels
                                ;-> p2mm not needed...
;  mark=1
  
;  if mark eq 1 then begin  
;    message,/cont,'add additional things here (labels, boxes, etc).'  
  
  case strmid(pikaia_result.input.observation,0,11) of
      '13may01.014': begin
        xadd=0. & yadd=0.    
      end
      '13may01.018': begin
        xadd=-29 & yadd=10      ; 14 -> 18
      end
      '13may01.019': begin
        xadd=-19 & yadd=12      ; 14 -> 19
      end
      else: return
    endcase
    box,x=pix2mm([27,41]+xadd,x=up2mm), $
      y=pix2mm([22,38]+yadd,y=up2mm),color=2,thick=4*(1+(!d.name eq 'PS')),/data
    
    plots,color=9,thick=4*(1+(!d.name eq 'PS')),psym=4,/data, $
      [pix2mm(15+19+.5+xadd,x=up2mm),pix2mm(39-12+.5+yadd,y=up2mm)]
;  endif
end
