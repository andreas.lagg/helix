;routine to read in convolution function
function read_conv,wlrg,file,cnwl,cmode,wljitter,prefile, $
                   wlin,verbose,noopt=noopt
  @common_maxpar
  
  conv=def_convst()
  conv.nwl_compare=-1
  conv.nwl=0
  conv.doconv=0
  conv.mode=cmode
  
  if file eq '' then return,conv
  openr,unit,/get_lun,file,error=err
  if err ne 0 then begin
    message,/cont,'Could not open convolution file: '+file
    reset
  endif
  nwl=n_elements(wlin)
  if verbose ge 1 then print,'Read convolution file '+file
  
  iw=0
  wl=dblarr(maxcwl) & val=fltarr(maxcwl)
  repeat begin
    line=''
    readf,unit,line
    cline=strtrim(line,2)
    
    if cline ne '' and strmid(cline,0,1) ne ';' then begin
      if iw ge maxcwl then begin
        message,/cont,'Max. # of WL points in convolve-file exceeded: '+ $
          n2s(iw)+','+n2s(maxcwl)
        conv.doconv=0
        message,/cont,'Change MAXCWL in all_type.f90 and maxpar.pro'
        message,/cont,'WARNING: no convolution done!'
        reset
      endif
      word=strsplit(cline,' ',/extract)
      if n_elements(word) eq 2 then begin
        wl(iw)=double(word(0))
        val(iw)=float(word(1))
        iw=iw+1
      endif
    endif
  endrep until eof(unit)
  ncwl=iw 
  free_lun,unit
  
                                ;check for FP maxima: sum up over one FSR
  cmax=0.95*max(val) ;sum up from 0.95x max value until next position where filter curve reaches this level.
  normval=0.
  dosum=0
;plot,conv.val(0:cnwl-1)    
  for iw=1,ncwl-1 do begin
    if val(iw) ge cmax and val(iw-1) lt cmax then dosum=dosum+1
    if dosum eq 1 then begin
      normval=normval+val(iw)
;plots,iw*[1,1],[0,conv.val(iw)]
    endif
  endfor
  if dosum eq 1 then normval=total(val(0:ncwl-1))
;print,normval,total(val)/normval &   stop
;do normalization over 1 FSR  
  val=val/normval
  
  
  show=0
  if show then begin
    plot,wl(0:ncwl-1),val(0:ncwl-1),ytype=1,/xst,title=file,xtitle='WL [A]',ytitle='filter function'
  endif
  
  conv.doconv=1
  
                                ;check if convolution WL vector is
                                ;exactly the same as the data vector:
  obsmatch=0
  if n_elements(wlin) eq cnwl then $
    if max(abs(wlin-wl(0:cnwl-1))) le 1e-3 then begin
    if verbose ge 1 then print,'Convolution WL-vector matches observation.'
    conv.nwl=cnwl
    conv.wl(0:cnwl-1)=wlin
    conv.val(0:cnwl-1)=val(0:cnwl-1)
    obsmatch=1
  endif
  
  if obsmatch eq 0 then begin
    
                                ;The WL-range of the convolution should cover
                                ;the whole profile
  if total(abs(wlrg)) le 1e-5 then $
    wlrg=[min(wlin),max(wlin)]

                                ;The WL-bins for the convolution must be
                                ; - equally spaced
                                ; - small enough to sample the
                                ;   convolution function.
  
  
                                ;if the keyword CONV_NWL is specified
                                ;then the binning is defined using
                                ;this keyword:
  if cnwl gt 1 then begin
    if cnwl ge nwl then begin
      if verbose ge 1 then $
        print,'Using CONV_NWL for WL-binning'
    endif else begin
      if verbose ge 1 then $
        print,'Observation has more bins than CONV_NWL, using' + $
        ' observation bin #'
      cnwl=nwl
    endelse
    conv.wl(0:cnwl-1)=dindgen(cnwl)/(cnwl-1d)*(wlrg(1)-wlrg(0))+wlrg(0) 
  endif else begin                    
    if verbose ge 1 then $
      print,'Using WL-binning from observation for CONV_FUNC'
    cnwl=nwl
    conv.wl(0:cnwl-1)=wlin
  endelse
  
  
                                ;test equal spacing
  if cnwl ge 3 then begin
    spc=conv.wl(1:cnwl-2)-conv.wl(0:cnwl-3)
    avgspc=total(spc)/(cnwl-2)
    eqspc=min(abs(spc-avgspc) lt 1e-3) ;1 mAngstrom tolerance
    if eqspc eq 0 then begin
      message,/cont,'WL-bins of observation not equally spaced. Equal ' + $
        'spacing is required for the convolution with an instrument function.'
      message,/cont,'Specify the CONV_NWL keyword or use equally spaced' + $
        ' observations.'
      conv.doconv=0
      message,/cont,'ERROR: no convolution done!'
      reset
      return,conv
    endif
  endif
                                ;interpolating convolution function to
                                ;observed WL binning
  conv.nwl=cnwl
;  dummy=max(val(0:ncwl-1)) & loc=!c
;    wlcen=(conv.wl(cnwl-1)+conv.wl(0))/2.
;     for i=0,cnwl-1 do begin
;       conv.val(i)=interpol(val(0:ncwl-1),wl(0:ncwl-1)-wl(loc), $
;  ;                          conv.wl(i)-conv.wl((cnwl-1)/2.))
;                            conv.wl(i)-wlcen)
;       print, 'HIERinter1',i,conv.val(i),total(conv.val(0:i)),wl(loc),conv.wl(i)-wlcen
;     endfor
  
  ;shift filter function to put max
  ;value exactly at the central bin
  ;number of conv.wl
  ; since the convolution function can have many maxima (e.g. a Fabry Perot), look for the maximum +-0.1 Angstrom atround the center only. If there is no maximum in this range, an error is thrown.
  if cnwl mod 2 eq 1 then wlcen=conv.wl(cnwl/2) $ ;odd number of wl-points
  else wlcen=total(conv.wl(cnwl/2-1:cnwl/2))/2d
  dlambda = 1.0 ; 0.1 A
  dummy = max(val(0:ncwl-1) * (abs(wl(0:ncwl-1) - wlcen) lt dlambda)) & loc=!c
  wlloc=wl(loc)
  ; check if location is not at the edge of the dlambda interval. Then it would not be a peak in the filter function
  if loc eq 0 or loc eq ncwl-1 then begin
    message,/cont,'Cannot find maximum in convolution function. The maximum value should be in the center of the function.'
    reset
  endif
  if val(loc) lt val(loc-1) or val(loc) lt val(loc+1) then begin
    message,/cont,'Cannot find maximum in convolution function +-0.1 Angstroem around the center. Update your convolution function to have a maximum in the center.'
    stop
    reset
  endif

  ;now determine the shift needed to put the maximum to the center
;  wlcen=conv.wl((cnwl-1)/2)
  if abs(wlcen-wlloc) ge 0.1 then begin
    if verbose ge 1 then $
      message,/cont,'Maximum of filter function is not close to the central bin of the convolution function. The convolution function was shifted accordingly. This can cause a problem in the wavelength range required for the convolution.'
  endif
  
                                ; manual interpoaltion routine (to be
                                ; consistent with the fortran version) 
  for i=0,cnwl-1 do begin
    idx=min(where(wl(0:ncwl-2)-wlloc le conv.wl(i)-wlcen and $
                  wl(1:ncwl-1)-wlloc ge conv.wl(i)-wlcen))
    if idx(0) eq -1 then begin
        message,/cont,'Convolution function does not cover the necessary ' + $
                'WL-range'
        print,'Convolution function: WL=',wlcen, $
              '+[',min((conv.wl(0:cnwl-1)-wlcen)),',', $
              max((conv.wl(0:cnwl-1)-wlcen)),']'
        print,'Data                : WL=',wlloc, $
              '+[',min((wl(0:ncwl-1)-wlloc)),',', $
              max((wl(0:ncwl-1)-wlloc)),']'
        print,'Extend the WL-range of the convolution fucntion.'
        reset
    endif else conv.val(i)=(val(idx+1)-val(idx))/(wl(idx+1)-wl(idx))* $
                           (conv.wl(i)-wlcen-(wl(idx)-wlloc))+val(idx)
  endfor
                                ;convolution function must cover at
                                ;least 2x the WL range specified in
                                ;input file
;   dwlipt=wlrg(1)-wlrg(0)
;   dwlcnv=max(conv.wl(0:cnwl-1))-min(conv.wl(0:cnwl-1))
;   if dwlcnv/dwlipt lt 2 then begin
;     message,/cont,'WL-range of convolution function must be 2x larger than ' + $
;       'the WL_RANGE specified in the input file.'
;     message,/cont,'Extending convolution WL-range by setting value of ' + $
;       'convolution function to zero outside the specified range.'
;     dwlnew=conv.wl(1)-conv.wl(0)
;     nwlnew=fix(2*dwlipt/dwlnew)+1
;     if nwlnew mod 2 eq 0 then icadd=1 else icadd=0 ;odd wl-numbers preferred
;     wlnew=(dindgen(nwlnew)- nwlnew/2)*dwlnew+wlcen
;     wlrgnew=minmax(wlnew)
;     iwoff=fix((wlrg(0)-wlrgnew(0))/dwlnew)
;     valnew=fltarr(n_elements(conv.val))
;     valnew(iwoff:iwoff+cnwl-1)=conv.val(0:cnwl-1)
; stop    
;      cnwl=nwlnew
;      wlrg=wlrgnew
;      conv.wl(0:cnwl-1)=wlnew
;      conv.val=valnew
;   endif
  
;    plot,wl(0:ncwl-1),val(0:ncwl-1) & oplot,color=1,conv.wl(0:cnwl-1),conv.val(0:cnwl-1) & stop
  
  
                                ;correct normalization after interpolation:
  conv.val=conv.val/(wl(1)-wl(0))*(conv.wl(1)-conv.wl(0))
  
  
;;check if there is significant
;;transmission close to
;;WL-boundaries. This is not good since
;;the convolution might produce large
;;values outside a prefilter!
; convmax=max(conv.val(0:cnwl-1))
; if (max(conv.val(0:(cnwl-1)*.05)/convmax) ge 3e-2 or $
;     max(conv.val((cnwl-1)*.95:cnwl-1)/convmax) ge 3e-2) then begin
;   message,/cont,'Convolution function has significant transmission' + $
;     ' (>3%) close to boundaries. The convolution might produce large' + $
;     ' values outside a prefilter! Check your convolution function' + $
;     ' and/or adapt your WL_RANGE'      
; endif
  
  endif ;obsmatch
                                ;for the FFT convolution a kernel
                                ;function should be used with the
                                ;maximum value of the filter function
                                ;located at index 0.
; conv.val(0:cnwl-1)= [conv.val((cnwl)/2:cnwl-1),conv.val(0:(cnwl)/2-1)]
; conv.val(0:cnwl-1)= [conv.val((cnwl-1)/2:cnwl-1),conv.val(0:(cnwl-1)/2-1) ]
  conv.val(0:cnwl-1)= [conv.val((cnwl-1)/2:cnwl-1),conv.val(0:(cnwl-1)/2-1)]

                                ;retrieve theindices of the original
                                ;WL-vector for which the comparison
                                ;with the observed spectrum is desired
;print, 'HIERrc',conv.val(0:4)
;print, 'HIERrc',conv.wl(0:4)
  
                                ;add WL jitter to simulate
                                ;deficiencies of a Fabry-Perot type
                                ;instrument (SO/PHI: simulate effect
                                ;of uncertainty in voltage applied to
                                ;the etalons)
  if wljitter ge 1e-10 then begin
    jitter=randomn(seed,nwl)*wljitter
  endif else jitter=fltarr(nwl)
  
  info=0
  maxdiff=00.
  for i=0,nwl-1 do begin
    diff=min(abs(conv.wl(0:cnwl-1)-(wlin(i)+jitter(i)))) & loc=!c
    maxdiff=maxdiff>diff
    if diff gt 2e-3 and wljitter lt 1e-10 then begin
      message,/cont,'WARNING: large diff. between binning of obs. and' + $
        ' CONV_FUNC        Obs-bin #'+n2s(i)+ $
        ', diff. '+n2s(diff,format='(e15.3)')
      info=1
    endif
    conv.iwl_compare(i)=loc
  endfor
  conv.nwl_compare=nwl
  
  
  if (info eq 1) then begin
    print,'WL of data: ',wlin(0:nwl-1)
    print,'Average data WL-spacing: ',avgspc,' #bins: ',nwl
    print,'WL-range convolution: ',wlrg,' #bins: ',cnwl
    print,format='(a,f10.3,a1,f10.3,a)', $
      'IDL> find_opt_binning,wlin=[Wl-vector of data], '+ $
      'wlrg=[',wlrg(0),',',wlrg(1),']'
    if keyword_set(noopt) eq 0 then begin
      print,'Execute find_opt_binning now [Y/n]?'
      repeat key=get_kbrd() until key ne ''
      if strupcase(key) ne 'N' then begin
        find_opt_binning,wlin=wlin(0:nwl-1),wlrg=wlrg
        print,'Continue with inversion [y/N]?'
        repeat key=get_kbrd() until key ne ''
        if strupcase(key) ne 'Y' then reset
      endif
    endif
  endif
  if verbose ge 1 or info then $
    print,'Max. diff between obs and conv WL in mA: ',maxdiff*1e3
  
;;after the convolution a normalization
;;has to be done to bring the spectrum
;;to the correct values. The correct
;;normalization requires the knowledge
;;of the prefilter curve.
;   prefilter=read_prefilter(prefile,0,conv.wl(0:cnwl-1),0)
; print,conv.val(0:9)
; print,prefilter.val(0:9)
;  if prefilter.doprefilter eq 1 then begin
;    preval=float(fft(fft(conv.val(0:cnwl-1),-1)* $
;                     fft(prefilter.val(0:cnwl-1),-1),1))
;                                ;only take the region where prefilter
;                                ;is above 10% transmission
;    conv.normval=max(preval*(prefilter.val(0:cnwl-1) ge 0.1))
;  endif else begin
;                                ;normalize convolution function such
;                                ;that value of profile remains
;                                ;unchanged
;    conv.val(0:cnwl-1)=conv.val(0:cnwl-1)/total(conv.val(0:cnwl-1))*cnwl
;    conv.normval=1.
;  endelse
; conv.val=conv.val/conv.normval
; conv.val(0:cnwl-1)=conv.val(0:cnwl-1)/total(conv.val(0:cnwl-1))*cnwl
  
  case conv.mode of
    0: begin
      prefilter=read_prefilter(prefile,0.,conv.wl(0:cnwl-1),0)
;  print,conv.val(0:9)
;  print,prefilter.val(0:9)
      if prefilter.doprefilter eq 1 then begin
        preval=float(fft(fft(conv.val(0:cnwl-1),-1)* $
                         fft(prefilter.val(0:cnwl-1),-1),1))
                                ;only take the region where prefilter
                                ;is above 10% transmission
        conv.normval=max(preval*(prefilter.val(0:cnwl-1) ge 0.1))
      endif else begin
                                ;normalize convolution function such
                                ;that value of profile remains
                                ;unchanged
        conv.val(0:cnwl-1)=conv.val(0:cnwl-1)/total(conv.val(0:cnwl-1))*cnwl
        conv.normval=1.
      endelse
      conv.val=conv.val/conv.normval
      if verbose ge 1 then print,'Using convolution method FFT'
    end
    1: begin
      conv.normval=1.
      conv.val(0:cnwl-1)=conv.val(0:cnwl-1)/conv.normval
      if verbose ge 1 then print,'Using convolution method MUL'
    end
  end
  
                                ;issue warning message if WL range of
                                ;filter function is small
  if verbose ge 1 then begin
    if ((max(wlrg)-min(wlrg))/ $
        (max((conv.wl(conv.iwl_compare(0:conv.nwl_compare-1))))- $
         min((conv.wl(conv.iwl_compare(0:conv.nwl_compare-1)))))) lt 2. then $
      message,/cont,'WARNING: WL_RANGE for convolution function should be ' + $
      'at least 2x larger than WL-range of data.'
  endif
  
;  print,file
;  print,conv.normval
;  print,conv.wl(0:3),conv.wl(cnwl-4:cnwl-1)
;  print,conv.val(0:3),conv.val(cnwl-4:cnwl-1)
;plot,/xst,conv.wl(0:cnwl-1),conv.val(0:cnwl-1) & stop
  return,conv
end
