pro read_atminput
  @common_maxpar
  common input,ipt
  common atminput,atminput
  
  
  aistr=string(ipt.atm.atm_input)
  nx=0 & ny=0 
  npar=intarr(ipt.ncomp)
  for ia=0,ipt.ncomp-1 do begin
    if strcompress(aistr[ia]) ne '' then begin
      if (ipt.verbose ge 1) then $
        print,format='(a,i2,'': '',a)','Reading ATM_INPUT fits file for COMP ',ia, $
        aistr[ia]
      
      data=read_fits_dlm(aistr[ia],0,header,status=status)
      if status ne 0 then message,'Could not find ATM_INPUT file: '+aistr[ia]
                                ;if file is an atm_ file form HeLIx+
                                ;then do the transpose
      if strpos(sxpar(header,'CODE'),'HELIX+') eq 0 then begin
        data=transpose(data,[2,0,1])
      endif
      naxes=size(data,/dim)
      dummy=execute('data'+n2s(ia)+'=temporary(data)')
      
      
      if ipt.verbose ge 2 then print,'npar=',naxes[0],' nx=',naxes[1],' ny=',naxes[2]
      if (ia eq 0) then begin
        nx=naxes[1]
        ny=naxes[2]
        npar[ia]=naxes[0]
      endif else begin
        if ((nx ne naxes[1]) or (ny ne naxes[2])) then begin
          print,'ATM_INPUT error:'
          print,'COMP ',ia,': NX x NY dimension not matching first component'
          reset
        endif
      endelse
      par=sxpar(header,'PAR*')
      dummy=execute('par'+n2s(ia)+'=temporary(par)')
    endif
  endfor
  
  
  atminput=def_atminputst(max(npar),nx,ny)
  atminput=replicate(atminput,ipt.ncomp)
  
  if nx ge 1 and ny ge 1 then $
    for ia=0,ipt.ncomp-1 do begin
    dummy=execute('data=temporary(data'+n2s(ia)+')')
    dummy=execute('par=temporary(par'+n2s(ia)+')')
    
    atminput[ia].par[0:npar[ia]-1]=par
    atminput[ia].data[0:npar[ia]-1,*,*]=data
    atminput[ia].file=aistr[ia]
    atminput[ia].npar=npar[ia]
    atminput[ia].nx=nx
    atminput[ia].ny=ny
    
    
  endfor

end
