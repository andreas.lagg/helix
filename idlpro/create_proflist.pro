;creates a profile list centered around an arbitrary number of pixels.
;INPUT:
;   ipt = name of input file (defines x and y window and binning)
;   x = x-values for centers
;   y = y-values for centers
;OUTPUT:
;   profile list in format to be used for input file.
;EXAMPLE:
; create_proflist,ipt='hanle_07may08.004_nov09.ipt',x=[0,126,159],y=[180,366,72]


pro create_proflist,ipt=input,x=x,y=y
  
  if n_elements(input) ne 1 then begin
    print,'input file not found.' + $
    'Please specify it with keyword ipt=''file.ipt'''
    reset
  endif
    
  if n_elements(x) eq 0 or n_elements(x) ne n_elements(y) then begin
    print,'Specify number of centers to start with: x=[...],y=[...]'
    reset
  endif
  
  ipt=read_ipt('input/'+input)
  
  nc=n_elements(x)
  radx=0
  rady=0
  xystr=''
  xyvec=[-1,-1]
  window,0 & userlct,/full & erase
  plot,ipt.x,ipt.y,/nodata,/xst,/yst
  donearr=bytarr(ipt.x(1)+1,ipt.y(1)+1)
  maxrad=float(max([ipt.x(1)-ipt.x(0),ipt.y(1)-ipt.y(0)]))
  repeat begin
    outside=0
                                ;create an x/y array 
    nx=radx*2+1
    ny=rady*2+1
    xvec=indgen(nx)-radx
    yvec=indgen(ny)-rady
    if radx eq 0 then xarr=0 else $
      xarr=[xvec(1:nx-2),intarr(ny)+xvec(nx-1), $
            reverse(xvec(1:nx-2)),intarr(ny)+xvec(0)]
    if rady eq 0 then yarr=0 else $
      yarr=[intarr(nx-2)+yvec(ny-1),reverse(yvec),intarr(nx-2)+yvec(0),yvec]
    
    
    for ic=0,nc-1 do begin
      xx=x(ic)+xarr
      yy=y(ic)+yarr
      xv=(fix(xx/ipt.stepx)*fix(ipt.stepx));>ipt.x(0))<ipt.x(1)
      yv=(fix(yy/ipt.stepy)*fix(ipt.stepy));>ipt.y(0))<ipt.y(1)
      inx=where(xv le ipt.x(1) and xv ge ipt.x(0))
      if inx(0) eq -1 then outside=outside+1 else begin
        inxy=where(yv(inx) ge ipt.y(0) and yv(inx) le ipt.y(1))
        if inxy(0) eq -1 then outside=outside+1 else begin
          xv=xv(inx(inxy)) & yv=yv(inx(inxy))
          notdone=where(donearr(xv,yv) eq 0)
          if notdone(0) ne -1 then begin
            xv=xv(notdone) & yv=yv(notdone)
            xystr_add=[n2s(xv,format='(i4.4)')+' '+n2s(yv,format='(i4.4)')]
            xv_add=transpose([[xv],[yv]])
            xystr_add=remove_multi(xystr_add,index=idx)
            xv_add=xv_add(*,idx)
            xystr=[xystr,[xystr_add]]
            xyvec=[[xyvec],[xv_add]]
            plots,color=(radx/maxrad)*256  mod 256,xv,yv
            donearr(xv,yv)=1b
          endif
        endelse
      endelse
    endfor
    radx=radx+(ipt.stepx>1)
    rady=rady+(ipt.stepy>1)
;    print,radx,rady,n_elements(xystr),outside
  endrep until outside ge nc
  if n_elements(xystr) eq 1 then begin
    print,'No pixels found.'
    reset
  endif
  xystr=xystr(1:*)
  xyvec=xyvec(*,1:*)
  
  dsp_file='./lists/list.'+strmid(input,0,strpos(input,'.ipt'))+'.list'
  openw,wunit,/get_lun,dsp_file,error=err
  if err eq 0 then begin
    printf,wunit,';despeckle profile list'
    printf,wunit,';use ''profile_list'' parameter in the input file'
    printf,wunit,transpose(xystr)
    free_lun,wunit
    print,'Wrote profile_list to '+dsp_file
    print,'List contains '+n2s(n_elements(xystr))+' pixels.'
    print,'To use the profile list in the input file, add:' 
    print,'   PROFILE_LIST '+dsp_file
  endif else message,/cont,'Error in writing out profile list to '+dsp_file

end
