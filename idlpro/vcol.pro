pro vcol,rg
  
  if n_elements(rg) ne 2 then $
    message,'Usage: vcol,[-2,+40]'
  
  
  if rg(1) eq rg(0) then return
  
  ratio=rg/(rg(1)-rg(0))
  ratio=ratio/max(abs(ratio))
  
  ncol=!d.table_size-2
  if ncol lt 5 then return
  
  gray_idx=float(ncol)/2.
  ci=(fix(gray_idx+gray_idx*ratio)>1)<(!d.table_size-2)
  cold=ci(0)+indgen(ci(1)-ci(0)+1)
  cnew=findgen(ncol)/(ncol-1)*(ci(1)-ci(0))+ci(0)
  
  loadct,1
  tvlct,ro,go,bo,/get
  r=ro & b=bo & g=go
  
  r(1:ncol)=interpol(ro(ci(0):ci(1)),cold,cnew)
  g(1:ncol)=interpol(go(ci(0):ci(1)),cold,cnew)
  b(1:ncol)=interpol(bo(ci(0):ci(1)),cold,cnew)
  
  tvlct,r,g,b
end
