;function to normalize filling factor:
;sum of FF for all components belonging to one line group (eg. He)
;should be one
function norm_ff,ff,fitff,linid
  common verbose,verbose
  
  if n_elements(ff) eq 1 then return,1.
  minid=min(linid)
  maxid=max(linid)
  retff=ff
  for i=minid,maxid do begin
     nsl=0
     sumff=0.
     sumnofitff=0.
     for j=0,n_elements(ff)-1 do begin
       if linid(j) eq i then begin
         nsl=nsl+1
         if (fitff(j) eq 0) then sumnofitff=sumnofitff+abs(retff(j)) $
         else sumff=sumff+abs(retff(j))
       endif
     endfor 
     if sumff eq 0 then sumff=1.
     if (nsl gt 0) then begin
       idx=where(linid eq i and fitff ne 0)
       if idx(0) ne -1 then retff(idx)=retff(idx)/sumff*(1-sumnofitff)
     endif
   endfor
   
   return,retff
end
