function add_tags,ipt,tags
  
  nt=n_elements(tags)
  tn=tag_names(ipt)
  add=strarr(nt)
  for i=0,nt-1 do begin
    it=(where(tn eq tags(i)))(0)
    if it ne -1 then begin
      typ=size(ipt.(it),/type)
      case typ of
        1: par=string(fix(ipt.(it)))
        2: par=string(ipt.(it))
        3: par=string(ipt.(it))
        4: par=strtrim(string(ipt.(it),format='(f15.2)'),2)
        5: par=strtrim(string(ipt.(it),format='(f15.2)'),2)
        7: par=ipt.(it)
        else: par=string(ipt.(it))
      endcase
      paradd=add_comma(/no_empty,sep=' ',par(0:(ipt.nmi<n_elements(par))-1))
      
      add(i)=strcompress(string(tags(i),paradd,format='(a20,a20)'))
    endif else message,'unknown tag: '+tags(i)
  endfor
  add=add_comma(sep=' | ',add)
  
  return,add
end

pro print_atm,atm,scale=scale,x=x,ypos=ypos,ipt=ipt,show=show,line=line, $
              blend=blend,gen=gen,pixx=pixx,pixy=pixy,widget=widget
  @common_cppar
  common obsfile,obsfile
 
  
  
  if n_elements(ipt) ne 0 then begin
    if n_elements(show) eq 0 then show=ipt.parset
    if n_elements(modeval) eq 0 then modeval=ipt.modeval
  endif
    
  
  
  if n_elements(show) eq 0 then show=tag_names(atm(0).par)
  
  show=show(where(show ne ''))
  
  nc=n_elements(atm)
  tn=tag_names(atm(0).par)
  
;  if keyword_set(x) then begin
                                ;atmosphere
    if n_elements(ypos) eq 0 then ypos=1.
    fmx='(a6,'+n2s(nc)+'(''|'',i4,f14.5,2x,''('',f10.2,'' to '',f10.2,'')''))'
    fmx_hd='(a6,'+n2s(nc)+'(''|'',a4,a14,2x,''('',a10  ,'' to '',a10  ,'')''))'
    fmx_ic='(6x,'+n2s(nc)+'(''|'',4x,14x,a15,12x,'' ''))'
    plin=string('Component '+n2s(indgen(nc)+1),format=fmx_ic)
    len=strlen(plin(0))
    sep=strjoin(replicate('-',(len)>60))
    chead=strarr(4,nc)
    for ic=0,nc-1 do chead(*,ic)=['Fit','Value','Scale Min','Scale Max']
    plin=[sep,plin(0),string(format=fmx_hd,'Par ',chead),sep]
    for i=0,n_tags(atm(0).par)-1 do if max(show eq tn(i)) eq 1 then $
      plin=[plin,string(format=fmx,tn(i), $
                        transpose([[atm.fit.(i)],[atm.par.(i)], $
                                   [scale(0:nc-1).(i).min], $
                                   [scale(0:nc-1).(i).max]]))]    
    plin=[plin,sep]
    
    if n_elements(blend) ne 0 then if max(blend.par.a0) ne 0 then begin
      for ib=0,n_elements(blend)-1 do begin
        plin=[plin, $
              'BLEND_WL    '+n2s(blend(ib).par.wl,   format='(f12.5)')+' | '+ $
              'BLEND_WIDTH '+n2s(blend(ib).par.width,format='(f10.4)')+' | '+ $
              'BLEND_DAMP  '+n2s(blend(ib).par.damp, format='(f10.4)')+' | '+ $
              'BLEND_A0    '+n2s(blend(ib).par.a0,   format='(f10.4)')]
      endfor
      plin=[plin,sep]
    endif
    
    if n_elements(gen) ne 0 then begin
      addgen=''
      if gen.fit.ccorr ne 0 then begin
        addgen='CCORR '+n2s(gen.par.ccorr,   format='(f12.5)')
      endif
      if gen.fit.straylight ne 0 then begin
        addgen=add_comma(sep=' | ', /no_empty, $
                         [addgen,'STRAYLIGHT '+n2s(gen.par.straylight, $
                                                   format='(f12.5)')])
      endif
      if gen.fit.radcorrsolar ne 0 then begin
        addgen='RADCORRSOLAR '+n2s(gen.par.radcorrsolar,   format='(f12.5)')
      endif
      if addgen ne '' then begin
        plin=[plin,addgen]
        plin=[plin,sep]
      endif
    endif
    if n_elements(ipt) ne 0 then begin ;other parameters
      plin=[plin,add_tags(ipt,['APPROX_DIR','APPROX_AZI','APPROX_DEV', $
                               'IQUV_WEIGHT'])]
      plin=[plin,add_tags(ipt,['MAGOPT','USE_GEFF','USE_PB','NCALLS','NOISE', $
                               'PROFILE'])]
      plin=[plin,add_tags(ipt,['SYNTH','OBSERVATION','STRAYPOL_CORR'])]
      plin=[plin,sep]
    endif
    
    if n_elements(line) gt 0 then  begin
      if max(abs(line.straypol_par.width)) gt 1e-5 then begin
        plin=[plin,'STRAYPOL_AMP  = ('+ $
              add_comma(sep=' | ', $
                        n2s(line.par.straypol_amp,format='(f15.5)'))+')']
      endif
      if max(abs(line.straypol_par.dopp)) gt 1e-5 then begin
        plin=[plin,'STRAYPOL_ETA0  = ('+ $
              add_comma(sep=' | ', $
                        n2s(line.par.straypol_eta0,format='(f15.5)'))+')']
      endif
      if max(abs(line.par.strength-1.)) gt 1e-5 or $
        max(abs(line.par.wlshift)) gt 1e-5  then begin
        plin=[plin,'STRENGTH  = ('+ $
              add_comma(sep=' | ', $
                        n2s(line.par.strength,format='(f15.5)'))+')' + $
              '   WLSHIFT  = ('+ $
              add_comma(sep=' | ', $
                        n2s(line.par.wlshift,format='(f15.5)'))+')']
      endif
    endif
;  endif
    
  if keyword_set(x) then begin
    if !d.name eq 'X' and (nc ge 3 or keyword_set(widget)) then begin
      common ainfo,ainfo
      bad_id=0
      scxy=get_screen_size()
      if n_elements(ainfo) eq 0 then ainfo={base:0l,text:0l} $
      else widget_control,ainfo.base,bad_id=bad_id
      new_wg=ainfo.base eq 0 or bad_id ne 0
      wtitle='Atmosphere Info: '+ipt.file
      if new_wg then begin
        ainfo.base=widget_base(title=wtitle)
        ainfo.text=widget_text(ainfo.base,/scroll,value=plin, $
                               ysize=i+1,xsize=120)
        widget_control,ainfo.base,/realize
      endif else begin
        widget_control,ainfo.base,base_set_title=wtitle
        widget_control,ainfo.text,set_value=plin
      endelse
    endif
    
    nl=n_elements(plin)
    dy=(ypos)/(nl+.5)
    xytext=add_comma(sep='!C',plin)
    fnt=!p.font
    if !d.name eq 'PS' then begin
      !p.font=0
      device,/courier,/tt
;      device,set_font='Courier',/tt
    endif else begin
      !p.font=-1
    endelse
    xyouts,/normal,0,-1,sep,charsize=1.,noclip=0,width=wd
    chsz=(1./wd)<0.9
;    xyouts,/normal,alignment=0.5,0.5,ypos,xytext,charsize=chsz
    maxwd=0. & cnt=0 & chsz=1.
    maxchsz=chsz
    repeat begin
      for i=0,nl-1 do begin
        doit=cnt eq 1 or (cnt eq 0 and sep ne plin(i))
        if doit then begin
          xyouts,/normal,alignment=0.5,0.5,(ypos-i*dy)*([-10,1])(cnt), $
            plin(i),charsize=chsz,width=wd, $
            color=([!p.background,!p.color])(cnt)
          maxwd=maxwd>wd
        endif
      endfor
      chsz=(1./maxwd)<1.
      maxchsz=maxchsz>chsz
      cnt=cnt+1
    endrep until cnt eq 2
    
    if !d.name eq 'PS' then device,set_font='Helvetica'
    !p.font=fnt
  endif else begin
    if modeval ne 5 then begin
      if n_elements(obsfile) ne 0 then $
        if n_elements(pixx) eq 1 and n_elements(pixy) eq 1 then $
        if max(tag_names(obsfile) eq 'CC') eq 1 then $
        if obsfile.cc ne '' then begin
        nc1=-1
        tinc=atm(0:nc-1).par.inc
        tazi=atm(0:nc-1).par.azi
        tbf=atm(0:nc-1).par.b
        print,'Solar Coordinates:'
        fmt='(a14,'+n2s(nc)+'(i4,f14.5))'
        los2solar,btot=tbf,gamma=tinc,chi=tazi,xval=pixx,yval=pixy, $
          obs=obsfile.cc,ambig='0',inc_sol=inc0,azi_sol=azi0
        if n_elements(inc0) ne 0 then begin
          print,'INC_SOLAR   0', $
            transpose([[atm(0:nc-1).fit.inc],[inc0]]),format=fmt
          print,'AZI_SOLAR   0', $
            transpose([[atm(0:nc-1).fit.azi],[azi0]]),format=fmt
        endif
        los2solar,btot=tbf,gamma=tinc,chi=tazi,xval=pixx,yval=pixy, $
          obs=obsfile.cc,ambig='180',inc_sol=inc180,azi_sol=azi180
        if n_elements(inc180) ne 0 then begin
          print,'INC_SOLAR 180', $
            transpose([[atm(0:nc-1).fit.inc],[inc180]]),format=fmt
          print,'AZI_SOLAR 180', $
            transpose([[atm(0:nc-1).fit.azi],[azi180]]),format=fmt
        endif
      endif
    endif else $
      print,'Hanle-slab mode: B-direction is already in solar coordinates.'
;     if n_elements(line) ne 0 then begin
;       if max(line(0).straypol_par.width) gt 1e-5 then $
;         print,'STRAYPOL_AMP = ('+ $
;         add_comma(sep=' | ',n2s(line.par.straypol_amp, $
;                                 format='(f15.5)'))+')'
;       if max(abs(line(0).straypol_par.dopp)) gt 1e-5 or $
;         max(abs(line(0).straypol_par.dopp)) gt 1e-5 then $
;         print,'STRAYPOL_ETA0 = ('+ $
;         add_comma(sep=' | ',n2s(line.par.straypol_eta0, $
;                                 format='(f15.5)'))+')'
;       if max(abs(line.par.strength-1.)) gt 1e-5 then $
;         print,'STRENGTH = ('+ $
;         add_comma(sep=' | ',n2s(line.par.strength,format='(f15.5)'))+')'
;       if max(abs(line.par.wlshift)) gt 1e-5 then $
;         print,'WLSHIFT = ('+ $
;         add_comma(sep=' | ',n2s(line.par.wlshift,format='(f15.5)'))+')'
;     endif
  endelse
end
