pro fit_accuracy,ps=ps,new=new
   common profile,profile
   common result,r_quiet,r_noise,r_stray
;   @common_cppar
   common greek_letters
   common color,color
   
   color=1
   
   
   idl53,prog_dir='./epd_dps_prog/' 
   
   file_pre='fit_accuracy_new'
  iptfile=file_pre+'.ipt'
  
  
  ipt=read_ipt(iptfile)
  
  ipt.atm.par.azi=40
  ipt.atm.par.inc=170
  
  B=[80,300,600]
  nlevel=[1e-4,1e-3,1e-3]
  good_fit=[0,0,0]+0.
  
;  restore,'sav/fit_accuracy_v2.sav'
;  restore,'sav/'+file_pre+'2003.183224956.sav'
   restore,'sav/fit_accuracy_new2003.351075050.sav'
  nb=n_elements(B)
  
  n=5
  if n_elements(r_quiet) eq 0 or keyword_set(new) then begin
    tstart=systime(1)
    for ib=0,nb-1 do begin
      for i=0,n-1 do begin
        thisipt=ipt
        thisipt.atm.par.inc=ipt.atm.par.inc
        thisipt.atm.par.azi=ipt.atm.par.azi
        thisipt.verbose=0
        thisipt.straypol_corr=0
        thisipt.atm.par.b=b(ib)
        
        thisipt.noise=nlevel(0)
        thisipt.straypol_amp=0.
        thisipt.straypol_corr=0
        write_input,'input/quiet.ipt',ipt=thisipt
        helix,result=quiet,ipt='input/quiet.ipt'
        
        if n_elements(nlevel) ge 2 then begin
        thisipt.noise=nlevel(1)
        thisipt.straypol_amp=0.
        write_input,'input/noise.ipt',ipt=thisipt
        helix,result=noise,ipt='input/noise.ipt'
        endif else noise=quiet
        
        if n_elements(nlevel) ge 3 then begin
        thisipt.straypol_corr=200
        thisipt.straypol_amp=0.002
        thisipt.noise=nlevel(2)
        write_input,'input/noise_hanle.ipt',ipt=thisipt
        helix,result=stray,ipt='input/noise_hanle.ipt'
        endif else stray=quiet
        
        if i eq 0 and ib eq 0 then begin
          r_quiet=replicate(quiet,n,nb)
          r_noise=replicate(noise,n,nb)
          r_stray=replicate(stray,n,nb)
        endif
        
        dummy= r_quiet(i,ib)
        struct_assign,quiet,dummy
        r_quiet(i,ib)=dummy
        
        dummy= r_noise(i,ib)
        struct_assign,noise,dummy
        r_noise(i,ib)=dummy

        dummy= r_stray(i,ib)
        struct_assign,stray,dummy
        r_stray(i,ib)=dummy
        
        tdiff=systime(1)-tstart
        perc=float(ib*n+i+1)/(n*nb)
        tend=tstart+tdiff/perc
        print,string(perc*100,format='(f6.2)')+'%, end time: '+ $
          conv_time(old='sec',new='date_dot',/idl,tend)
      endfor
      
    endfor
    save,/xdr,/compress,file='sav/'+file_pre+conv_time(systime(1),old='sec',new='doy',/idl)+'.sav',r_quiet,r_noise,r_stray,B,nlevel
  endif
  
  ;output as table
  noshow=strupcase(['WIDTH','FF','SGRAD'])
  showpar=''
  for ib=0,nb-1 do begin
    for ip=0,n_tags(r_quiet.fit.atm.par)-1 do begin
      par=(tag_names(r_quiet.fit.atm.par))(ip)
      if max(ipt.parset eq par) eq 1 and max(noshow eq par) eq 0 then begin
        out=string((tag_names(r_quiet.fit.atm.par))(ip),format='(a6,'' & '')')+ $
          string(r_quiet(0,ib).input.atm(0).par.(ip),format='(f8.1)')
        for ir=0,2 do begin
          case ir of 
            0: res=r_quiet
            1: res=r_noise
            2: res=r_stray
          endcase
          goodidx=where(res(*,ib).fit.fitness ge good_fit(ib))
          if n_elements(goodidx) gt 3 then res=res(goodidx,ib) $
          else res=res(*,ib)
          mm=[min(res.fit.atm.par.(ip)),max(res.fit.atm.par.(ip))]
          mmstr=add_comma(n2s(mm,format='(f15.1)'),sep=' to ')
          rg=ipt.scale(0).(ip).max-ipt.scale(0).(ip).min
          perc=(max(mm)-min(mm))/rg*100
          out=out +' & '+ string(mmstr+' ('+n2s(perc,format='(f15.1)')+'\%)',format='(a25)')
        endfor
        print,out+' \\'
        showpar=[showpar,par]
      endif
    endfor
    print,'    \noalign{\smallskip}'
    print,'    \hline'
    print,'    \noalign{\smallskip}'
  endfor
  
  showpar=showpar(1:*)
  
                                ;output as plot
  if keyword_set(ps) then begin
    psout=ps_widget(default=[0,0,1,0,8,0,0],size=[18.,16],psname=psn, $
                    file='~/work/aa_2003/figures/'+file_pre+'.eps',/no_x)
    !p.font=0    
    userlct
  endif else begin
    !p.font=-1 & set_plotx
    window,0 & userlct & erase
  endelse
  greek
  
  npar=n_elements(showpar)
  frame=[0.1,0.02,0.98,0.95] ;plot frame for all plots
  sep=[0.03,0.03]               ;seperation (x/y) for single plots
  
  for in=0,2 do begin
    for ib=0,nb-1 do begin
      case in of 
        0: res=r_quiet
        1: res=r_noise
        2: res=r_stray
      endcase
;plot,  res(*,ib).fit.fitness & stop    
      goodidx=where(res(*,ib).fit.fitness ge good_fit(ib))
      if n_elements(goodidx) gt 3 then res=res(goodidx,ib) $
      else res=res(*,ib)
      res=res.fit.atm.par
      
      posx=frame(0)+[in,in+1]*(frame(2)-frame(0))/3 -[-1.,1.]*sep(0)
      posy=frame(3)-[ib+1,ib]*(frame(3)-frame(1))/nb-[-1.,1.]*sep(1)
      
      if in eq 0 then begin
        xyouts,0,posy(1),/normal,charsize=1.2*!p.charsize, $
          '!C B = '+n2s(fix(b(ib)))+' G'
        dy=posy(1)-posy(0) & yc=posy(0)+0.5*dy
        ax=frame(0)*0.2 & ay=0.2*dy
        arrow,ax,yc+.01,ax,yc+ay,/normal,color=2
        xyouts,/normal,ax,yc+ay,'  incl. '+f_gamma
        arrow,ax,yc-.01,ax,yc-ay,/normal,color=1
        xyouts,/normal,ax,yc-ay,'!C  azim. '+f_chi
      endif
      
      if ib eq 0 then begin
        expo=fix(alog10(nlevel)) - (alog10(nlevel) ne fix(alog10(nlevel)))
        fct=round(nlevel/10^float(expo))
        estr='Noise Level '+n2s(fct)+'!U.!N10!U'+n2s(expo)+'!N'
        estr(2)=estr(2)+' +Hanle'
        oestr=['Noise Level 10!U-4!N', $
               'Noise Level 10!U-3!N', $
               'Noise Level 10!U-3!N + Hanle']
        xyouts,(posx(1)-posx(0))/2+posx(0),1,/normal, $
          charsize=1.2*!p.charsize, $
          alignment=0.5,'!C '+estr(in)
      endif
;print,oestr(in)+' ',b(ib),n_elements(goodidx)
      
      
      aziplot=[[cos((res.azi-90)*!dtor)*res.b], $
               [sin((res.azi-90)*!dtor)*res.b]]
      incplot=[[cos(res.inc*!dtor)*res.b], $
               [sin(res.inc*!dtor)*res.b]]
      xrg=[-1,1]*2*b(ib)
      yrg=[-1,1]*2*b(ib)
      plot,position=[posx(0),posy(0),posx(1),posy(1)],/noerase, $
        xrange=xrg,yrange=yrg,psym=1,xst=5,yst=5, $
        aziplot(*,0),aziplot(*,1),/nodata
      
      brg=b(ib)+[-1,1]*30.      ;allowed deviation for B in Gauss
      azirg=ipt.atm(0).par.azi+[-1,1]*15. ;allowed dev. in � for azi
      incrg=ipt.atm(0).par.inc+[-1,1]*15. ;allowed dev. in � for inc
      for i=0,1000 do begin      ;
        tb=(randomu(seed)*1-.5)*(brg(1)-brg(0))+b(ib)
        ta=(randomu(seed)*1-.5)*(azirg(1)-azirg(0))+ipt.atm(0).par.azi
        ti=(randomu(seed)*1-.5)*(incrg(1)-incrg(0))+ipt.atm(0).par.inc
        mkusym,'CIRCLE',fill=1,color=([8,1])(color)
        plots,color=8,[cos((ta-90)*!dtor)*tb],[sin((ta-90)*!dtor)*tb],psym=8
        mkusym,'CIRCLE',fill=1,color=([6,2])(color)
        plots,color=6,[cos((ti)*!dtor)*tb],[sin((ti)*!dtor)*tb],psym=8
      endfor
      
      oplot,psym=1,aziplot(*,0),aziplot(*,1);,color=([9,1])(color)
      oplot,psym=4,incplot(*,0),incplot(*,1);,color=([9,2])(color)
      
      axis,0.,/yax,ytickv=yrg,yticks=1,charsize=.8*!p.charsize
      axis,0.,/xax,xtickv=xrg,xticks=1,charsize=.8*!p.charsize

      
    endfor
    
  endfor
  
  if !d.name eq 'PS' then begin
    device,/close
    print,'Created PS-file: '+psn
  endif
  !p.font=-1 & set_plotx
 
end
