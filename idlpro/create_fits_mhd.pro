;example to create fits file in the format of TIP.
pro create_fits_mhd,savmhd
  
  
  restore,/v,savmhd
  
  szs=size(stokes_sm)
;get continuum image
  icont=contin_sm     
  icont(*)=1                  ;since profiles are already normalized
;  wset,0 & plot,icont,/yst    
  
  
  data=fltarr(szs(2),szs(4),szs(3)*4)
  order_sav=['I','Q','U','V']
  order_fits=['I','V','Q','U']
  jf=intarr(4)
  for j=0,3 do jf(j)=where(order_sav eq order_fits(j))
  for i=0,szs(3)-1 do for j=0,3 do begin
    data(*,*,i*4+jf(j))=stokes_sm(j,*,i,*)
  endfor
  
  szd=size(data)
  print,'============ SIZE ======================'
  print,'Size: ',szd
  print,'WL-pixels: ',szd(1)
  print,'y-pixels: ',szd(2)
  print,'x-pixels (*4): ',szd(3)
  
  
  pos=strpos(savmhd,'.sav',/reverse_search)
  if pos lt 0 then pos=strlen(savmhd)
  froot=strmid(savmhd,0,pos)
  fitsout=froot+'.fitscc'
  print,'======= WRITE NEW FITS ================='
  print,'File: ',fitsout
  
  writefits,fitsout,data,append=0
  
;wl-vector
  wlvec=double(w0)+double(wl)
  nwl=n_elements(wl)
  wloff=0. & wlbin=0.
  
;now create ccx file  
  print,'======= WRITE NEW CCX ================='
  filex=fitsout+'x'
                                ;create header
  header=''
  
  writefits,filex,icont,header,append=0
  naxpos=max(where(strpos(header,'NAXIS') eq 0))
  header=[header(0:naxpos), $
          'WL_NUM  = '+string(nwl,format='(i20)')+ $
          ' / WL-bins', $
          'WL_OFF  = '+string(wloff,format='(d20.8)')+' / WL-Offset', $
          'WL_DISP = '+string(wlbin,format='(d20.8)')+' / WL-Dispersion', $
          header(naxpos+1:*)]
  writefits,filex,icont,header,append=0        
  
                                ;write out wl-vector as extension
  writefits,filex,wlvec,append=1
  print,'Wrote '+filex
  print,'Done.'
  stop
end
