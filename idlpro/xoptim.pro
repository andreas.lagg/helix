;detrmine optimum setting for x/y size when using mask
pro xoptim,xs=xs,ys=ys,nx=nx,ny=ny,charsize=charsize,label=label, $
           xrange=xrg,yrange=yrg,isotropic=isotropic,fix_margins=fix_margins
  
  odev=!d.name
  set_plot,'Z'
  sizeok=0
  ntry=0
  xy=[xs,ys]
  maxtry=4
  ratiox=fltarr(maxtry)
  ratioy=fltarr(maxtry)
  repeat begin
    device,set_resolution=[xs,ys]
    pos=0
    dummy=mask([nx,ny],xrange=xrg,yrange=yrg,fix_margins=fix_margins, $
               label=label,isotropic=isotropic, $
               charsize=charsize,pos=pos)
    ratiox(ntry)=(pos(2)-pos(0))/(fix_margins(1)-fix_margins(0))
    ratioy(ntry)=(pos(3)-pos(1))/0.9

    xok=ratiox(ntry) gt 0.95 and ratiox(ntry) lt 1.05
    yok=ratioy(ntry) gt 0.95 and ratioy(ntry) lt 1.05

    if xok eq 0 then begin
      xs=xs*ratiox(ntry)
    endif else if yok eq 0 then begin
      ys=ys*ratioy(ntry)
    endif
    xs=xs>(0.1*ys)
    ys=ys>(0.1*xs)
    
    xy=[[xy],[xs,ys]]
    ntry=ntry+1
  endrep until ntry ge maxtry
  
                                ;use best value
  dummy=min((ratiox-1)^2 + (ratioy-1)^2)
  xs=xy(0,!c) & ys=xy(1,!c)
  
  set_plot,odev
end
