;***********************************************************************
;+
; NAME:
;	READSTO
;
; PURPOSE:
;	This procedure reads an ascii file in the stopro format and
;       returns all (the selected) stokes parameters in one big array
;
; SEQUENCE:
;	readsto, fname, sto [,nwl[,wlref[,ion[,fparam[,finfo]]]][,keywords])
;
; e.g   readsto, '5247.dat', s1, /silent
;       readsto, 'fe12.dat', s2, nwl, wlref, param='IV'
;
; INPUT PARAMETER:
;	fname = string : name of file to be read
;
; KEYWORDS:
;       PARAM=<params> : <params> is a list of params to be read
;                          i.e
;                          'I', 'IV', 'IVQ', IVQU', 'WI', ...
;
;       SELECT=<spec>  : <spec> is the spectrum/line number to be read
;                        default: all spectras/lines are read
;
;       /CONT          : correct cont. intensity (i=i*Bcont)
;
;       /SILENT        : do not print informations
;
; OUTPUT:
;       sto = fltarr(nspec,nparam,nwl)  : "Big" spectrum array
;                            ^--------- : 0=W, 1=I, 2=V, 3=Q, 4=U
; OPTIONAL OUTPUT:
;       nwl    = intarr(nspec)          : number of wl points
;       wlref  = fltarr(nspec)          : wavelength references
;       ion    = strarr(nspec)          : atom/ion
;       fparam = string                 : stokes parameters in file
;       finfo  = string                 : info header string
;
; MODIFICATION HISTORY:
;	Written 1996/1997 by C.Frutiger
;-
;***********************************************************************

PRO  READSTO, fname, sto, numwl, wlref, ion, fparam, finfo, bound, $
              PARAM=param, SELECT=select, CONT=cont, SILENT=silent, $
              bcont=bcont,error=error
if (n_params() lt 2) then begin
  DOC_LIBRARY,'READSTO'
  GOTO,finish
endif


  error=0
NWLMAX=1024

NUMSTOKES=1
ntotal=1
typinp=' '
fparam=' '
finfo=' '
nspec=0
NWL=0
ncol=0
wstart=0.0D
wstep=0.0D
wref=0.0D
ntotal=1
; Read filename and the two first line of the file

ON_IOERROR, IOERR
get_lun,unit
ierr = 0
openr,unit,fname
ierr = 1
readf,unit,NTOTAL, TYPINP
pq1=strpos(typinp,"'")
pq2=strpos(typinp,"'",pq1+1)+1
FPARAM=strmid(typinp,pq1,pq2-pq1)
l=strlen(typinp)
FINFO=strmid(typinp,pq2+1,l-pq2)

if not keyword_set(SILENT) then begin
  print,'fparam =',fparam
  print,'finfo  =',finfo
  print,'nspec  =',ntotal
endif

; allocate array to store true number of wavelength points per line
if ntotal eq 0 then begin
  message,/cont,'no spectra found. returning'
  return
endif

numwl=intarr(ntotal)


; Decide what quantities are listed in the file and which are to be read
FLISTW='n'
FLISTI='n'
FLISTV='n'
FLISTQ='n'
FLISTU='n'
FLISTB='n'
for N=0,7 do begin
  p=STRMID(FPARAM,N,1)
  if p EQ 'W' then FLISTW='y'
  if p EQ 'I' then FLISTI='y'
  if p EQ 'V' or p EQ 'v' then FLISTV='y'
  if p EQ 'Q' or p EQ 'q' then FLISTQ='y'
  if p EQ 'U' or p eq 'u' then FLISTU='y'
  if p EQ 'B' then FLISTB='y'
endfor
if (FLISTU eq 'y') then NUMSTOKES=4
if (FLISTQ eq 'y') AND (FLISTU eq 'n') then NUMSTOKES=3
if (FLISTV eq 'y') AND (FLISTQ eq 'n') then NUMSTOKES=2
if (FLISTI eq 'y') AND (FLISTV eq 'n') then NUMSTOKES=1
if (FLISTB eq 'y') then NUMSTOKES=NUMSTOKES+1

; determines how many columns are in the file

if (FLISTW EQ 'y') then NCOL=NUMSTOKES+1 else NCOL=NUMSTOKES
if not keyword_set(SILENT) then begin
  print,'nstokes=',numstokes
  print,'ncols  =',ncol
endif

; Loop over the different spectra contained in the file -----------

if NTOTAL GT 1 then begin
  if keyword_set(SELECT) then begin
    if SELECT GT NTOTAL then begin
      PRINT,' THE FILE DOES NOT CONTAIN SO MANY SPECTRA!'
      select=ntotal
    endif
    ; Jump over the spectra of which are not to be returned
    for N=1,SELECT-1 do BEGIN
      ierr = 2
      READF,UNIT, NSPEC, WREF, NWL
      data=fltarr(ncol,NWL)
      ierr = 3
      READF,unit,data
    endfor
    ispbeg=select
    ispend=select
  end else begin
    ispbeg=1
    ispend=ntotal
  endelse
endif else begin
  ispbeg=1
  ispend=1
endelse

; Decide which profiles are to be read ---------------------------
IF NUMSTOKES GT 1 THEN BEGIN
  FREADI='n'
  FREADV='n'
  FREADQ='n'
  FREADU='n'
  FREADB='n'
  IF not keyword_set(PARAM) THEN PARAM='IVQUB'
  NUMRD=1
  FOR I=0,5 DO BEGIN
    IF (STRMID(PARAM,I,1) EQ 'I') and (FLISTI EQ 'y') THEN BEGIN
      FREADI='y' & NUMRD=NUMRD+1 & END
    IF (STRMID(PARAM,I,1) EQ 'V') and (FLISTV EQ 'y') THEN BEGIN
      FREADV='y' & NUMRD=NUMRD+1 & END
    IF (STRMID(PARAM,I,1) EQ 'Q') and (FLISTQ EQ 'y') THEN BEGIN
      FREADQ='y' & NUMRD=NUMRD+1 & END
    IF (STRMID(PARAM,I,1) EQ 'U') and (FLISTU EQ 'y') THEN BEGIN
      FREADU='y' & NUMRD=NUMRD+1 & END
    IF (STRMID(PARAM,I,1) EQ 'B') and (FLISTB EQ 'y') THEN BEGIN
      FREADB='y' & NUMRD=NUMRD+1 & END
  ENDFOR
endif else begin
  NUMRD=2
  FREADI='y'
  FREADV='n'
  FREADQ='n'
  FREADU='n'
  FREADB='n'
ENDELSE

; Loop over all the spectra in the file ==============================
n=0
wref = 0.d0
FOR j=ispbeg,ispend DO BEGIN
  ierr = 4
  head = ' '
  readf,unit,head
  if not keyword_set(SILENT) THEN print,HEAD
  if FLISTW EQ 'y' then BEGIN
    reads,head, NSPEC, WREF, NWL, BCONT
  endif else begin
    reads,head, NSPEC, WREF, NWL, BCONT, WSTART, WSTEP
  ENDELSE

  if n eq 0 then begin
    nspec=ispend-ispbeg+1
    sto  =dblarr(nspec,numrd,NWLMAX)
    wlref=dblarr(nspec)
    ion = strarr(nspec)
    numwl=intarr(nspec)
    bound=intarr(nspec,4)
  ENDIF
  wlref(n)=wref
  numwl(n)=nwl
  ; NEW FEATURE: fitting boundaries
  ib = strpos(head,'|')
  IF (ib GT 0) THEN BEGIN
    b = intarr(4)
    reads,strmid(head,ib+1,30),b
    bound(n,*) = b-1
  ENDIF ELSE bound(n,*) = [0,nwl-1,0,nwl-1]
  ; NEW FEATURE: ion/line info
  ii = strpos(head,string(39b))
  IF (ii GT 0) THEN BEGIN
    iie = strpos(head,string(39b),ii+1)
    IF iie LT ii THEN iie = strlen(head)
    ion(n) = strmid(head,ii+1,iie-ii-1)
  ENDIF
  ; read block containing the data --------------------------------

  IF (nwl gt nwlmax) then begin
    PRINT,'NUMBER OF WAVELENGTH POINTS GT',NWLMAX
    PRINT,'ADJUST NWLMAX AND RECOMPILE READSTO.PRO'
    GOTO, FINISH
  ENDIF

  ierr = 5
  data=dblarr(ncol,NWL)
  READF,unit,data
  
  ; creates the x-axis

  if FLISTW EQ 'y' then begin
    XX=WREF+data(0,*)
    w=1
  end else begin
    XX=DINDGEN(NWL)*WSTEP+WREF+WSTART
    w=0
  endelse

  sto(n,0,0:NWL-1)=XX(*)

  ; Prepare individual stokes parameters

  m=1
  IF FREADI EQ 'y' THEN BEGIN
    ; Reset the continuum level if wanted
    if keyword_set(CONT) then data(w,*)=data(w,*)*bcont
    sto(n,m,0:NWL-1)=data(w,*)
    m=m+1
  ENDIF
  IF FREADV EQ 'y' THEN BEGIN
    sto(n,m,0:NWL-1)=data(1+w,*)
    m=m+1
  ENDIF
  IF FREADQ EQ 'y' THEN BEGIN
    sto(n,m,0:NWL-1)=data(2+w,*)
    m=m+1
  ENDIF
  IF FREADU EQ 'y' THEN BEGIN
    sto(n,m,0:NWL-1)=data(3+w,*)
    m=m+1
  ENDIF
  IF FREADB EQ 'y' THEN BEGIN
    sto(n,m,0:NWL-1)=data(ncol-1,*)
  ENDIF
  n = n+1
  
  s = ''
  if j ne ispend then  READF,UNIT,s

ENDFOR
; =================================================================

CLOSE,unit
free_lun,unit
RETURN

IOERR:
  if n_elements(unit) eq 0 then return
  CLOSE,unit
  free_lun,unit
  PRINT, 'READSTO:',FNAME
  IF (ierr EQ 0) THEN BEGIN
    PRINT, 'FILE NOT FOUND/FILE OPEN ERROR'
  ENDIF ELSE BEGIN
    PRINT, 'FILE READ ERROR, CHECK FORMAT, IERR=',ierr
    if n_elements(n) gt 0 and n_elements(head) gt 0 then PRINT,n,'|',head,'|'
  ENDELSE
  error=1
  goto,finish
  
; set all output variables to zero
FINISH:
  STO=0
  NUMWL=0
  WLREF=0.
  FPARAM=0
  INFO=0

  RETURN

END
;********************************************************************





