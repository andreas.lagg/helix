;convert bytarr containing ID to string variable
function id2s,id
  
  sz=size(id)
  if sz(0) eq 1 then ns=1 else ns=sz(2)
  retstr=strarr(ns)
  
  for i=0,ns-1 do retstr(i)=strtrim(string(id(*,i)),2)
  if ns eq 1 then retstr=retstr(0)
  return,retstr
end
