;removes single electronic spikes
function remove_spike,obs,spike,verbose=verbose
  
  retobs=obs
  tg=['I','Q','U','V']
  tn=tag_names(obs)
  nwl=n_elements(obs.wl)
  first=1
  if n_elements(verbose) eq 0 then verbose=0
  for ii=0,3 do begin
    it=where(tn eq tg(ii))
                                ;calculate STDEV of difference between
                                ;WL points of signal
    diffsig=abs(obs.(it)(1:nwl-1)-obs.(it)(0:nwl-2))
    stdev=sqrt(total((diffsig-total(diffsig)/(nwl-1.))^2)/(nwl-1.))
                                ;calculate difference between original and
                                ;median profile
    med=median(obs.(it),2+spike)
    diff=abs(obs.(it)-med)
                                ;identify spike if diff is larger than
                                ;5*stdev
    ispike=where(diff gt 5*stdev)
    if ispike(0) ne -1 then begin
      retobs.(it)(ispike)=med(ispike)
      if verbose eq 1 and first eq 1 then print,'Spike(s) removed.'
      if verbose ge 2 then $
        print,n2s(n_elements(ispike))+' spikes removed in Stokes '+tg(ii)
      first=0
    endif
  endfor
  
  return,retobs
end
