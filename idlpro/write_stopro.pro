pro write_stopro,file=file,profile=profile,wlref=wlref,header=header,ipt=ipt, $
                 verbose=verbose,error=error
  
;!!!!!!!!!!!!!!!!!!!! profile in stopro format
  error=0
  openw,unit,/get_lun,file,error=ierr2
  if ierr2 ne 0 then begin
    message,/cont, 'Error writing profile to file '+trim(file)
    message,/cont, 'Please check directory structure.'
    message,/cont, 'Result NOT STORED!!!'
    error=1
    return
  endif
  
  if n_elements(wlref) eq 0 then message,'Please specify WLREF'
  
  nprof=1
  printf,unit,format='(i3,2x,a1,''WIVQU'',a1,3x,a,1x,a,1x,a,1x,a)', $
    nprof,'''','''',header
  idstr=" "
  nwl=n_elements(profile.wl)
  printf,unit,format='(i5,f16.4,i6,E16.5,''  |  '',4i5,3x,a1,a,a1)', $
    nprof,wlref,nwl,profile.ic,1,nwl,1,nwl,'''',idstr,''''
  for i=0,nwl-1 do begin
    printf,unit,format='(5E15.6)', $
      profile.wl(i)-wlref,profile.i(i),profile.v(i),profile.q(i),profile.u(i)
  endfor          
  printf,unit
  free_lun,unit
  
  if n_elements(ipt) eq 0 then verbose=keyword_set(verbose) $
  else verbose=ipt.verbose
  if verbose ge 1 then print, 'Fit-Profile written to '+file
end
