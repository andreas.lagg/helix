pro reset
  common wgst,wgst
  common diswg,diswg
  common wgp,wgp
  common wgl,wgl
  common profile,profile
  common proforig,proforig
  
  set_plot,'x'
  if n_elements(wgst) ne 0 then begin
    widget_control,wgst.base.id,bad_id=bad
    if bad eq 0 then widget_control,wgst.base.id,hourglass=0,sensitive=1
  endif
  if n_elements(wgl) ne 0 then begin
    widget_control,wgl.base.id,bad_id=bad
    if bad eq 0 then widget_control,wgl.base.id,hourglass=0,sensitive=1
  endif
  if n_elements(diswg) ne 0 then begin
    widget_control,diswg.base.id,bad_id=bad
    if bad eq 0 then widget_control,diswg.base.id,hourglass=0,sensitive=1
  endif
  if n_elements(wgp) ne 0 then begin
    widget_control,wgp.base.id,bad_id=bad
    if bad eq 0 then widget_control,wgp.base.id,hourglass=0,sensitive=1
  endif
  if n_elements(proforig) ne 0 and n_elements(profile) ne 0 then $
    profile=temporary(proforig)
  
  !p.position=0 & !p.multi=0
  !p.charsize=0
  !x.ticklen=0 & !y.ticklen=0
  
  retall
end
