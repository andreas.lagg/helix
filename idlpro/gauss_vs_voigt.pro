pro gauss_vs_voigt,ps=ps,new=new
   common profile,profile
   @common_cppar
   common resg,r_gauss,r_voigt
   
   idl53,prog_dir='./epd_dps_prog/' 

  ipt=['gauss_vs_voigt_damp','gauss_vs_voigt_nodamp']
  list=[26,11]
  
  if n_elements(r_gauss) eq 0 or keyword_set(new) then $
    helix,ipt=ipt(0)+'.ipt',result=r_gauss,list=list
  if n_elements(r_voigt) eq 0 or keyword_set(new) then $
    helix,ipt=ipt(1)+'.ipt',result=r_voigt,list=list
  
  xpvec=list(0) & ypvec=list(1)
  if r_gauss.input.average then begin
    xpvec=(list(0)+indgen(r_gauss.input.stepx))
    ypvec=(list(1)+indgen(r_gauss.input.stepy))
  endif
  obs=get_profile(profile=profile, $
                  ix=xpvec,iy=ypvec,icont=icont, $
                    wl_range=r_gauss.input.wl_range)
  
  voigt=strupcase(r_gauss.input.profile) eq 'VOIGT'
  magopt=r_gauss.input.magopt(0)
  hanle_azi=gauss.input.hanle_azi
  norm_stokes_val=gauss.input.norm_Stokes_val
  old_norm=r_gauss.input.old_norm
  old_voigt=r_gauss.input.old_voigt
  use_geff=r_gauss.input.use_geff(0)
  use_pb=r_gauss.input.use_pb(0)
  modeval=r_gauss.input.modeval(0)
  gprof=compute_profile(/init,line=r_gauss.line,atm=r_gauss.fit.atm, $
                        obs_par=r_gauss.input.obs_par(0),wl=obs.wl)
  
  voigt=strupcase(r_voigt.input.profile) eq 'VOIGT'
  magopt=r_voigt.input.magopt(0)
  hanle_azi=r_voigt.input.hanle_azi
  norm_stokes_val=voigt.input.norm_Stokes_val
  old_norm=r_voigt.input.old_norm
  old_voigt=r_voigt.input.old_voigt
  use_geff=r_voigt.input.use_geff(0)
  use_pb=r_voigt.input.use_pb(0)
  modeval=r_voigt.input.modeval(0)
  vprof=compute_profile(/init,line=r_voigt.line,atm=r_voigt.fit.atm, $
                        obs_par=r_voigt.input.obs_par(0),wl=obs.wl)
  
  
  
  !p.font=-1 & set_plot,'X'
  if keyword_set(ps) then begin
  psout=ps_widget(default=[0,0,1,0,8,0,0],size=[20.,22],psname=psn, $
                  file='~/work/aa_2003/figures/gauss_vs_voigt.eps',/no_x)
  !p.font=0
  endif
  
  plot_profiles,obs,gprof,vprof,line=r_gauss.line,/iic, $
    color=[9,9,9],lstyle=[0,1,1],psym=[0,1,4],symsize=1.2,/bw;, $
;    wl_range=[10828.0,10832]
  
  if !d.name eq 'PS' then begin
    device,/close
    print,'Created PS-file: '+psn
  endif
  
  !p.font=-1 & set_plot,'X'
 
end
