;program to perform more complex helixs depending on result of
;simple helix
pro driver
  
  
                                ;do simple helix
;  helix,ipt='emf_1comp_unnoazi_abs.ipt',savall=savall
;restore,savall  
  restore,'sav/straypol_1comp_gauss_1x1.13may01.014.profiles.abs.sav.pikaia.2003-05-18_12:08.sav'
  
                                ;check for parameters which make a
                                ;more detailed analysis necessary
  idx=where(pikaia_result.fit.atm(0).par.vlos gt 5000.)
  
  
  if idx(0) eq -1 then return
  
  img=(pikaia_result.fit.atm.par.vlos)*0
  sz=size(img)
  fc=4
  window,/free,xsize=sz(1)*fc,ysize=sz(2)*fc
  img(idx)=1
  userlct
  tv,congrid(img,sz(1)*fc,sz(2)*fc)

                                ;call more complex helix
  list=transpose([[pikaia_result.fit(idx).x],[pikaia_result.fit(idx).y]]) 
  atm_ini=pikaia_result.fit(idx).atm
  helix,ipt='straypol_2comp_gauss_1x1.ipt',list=list,atm_ini=atm_ini
  
end
