;example to create fits file in the format of TIP.
pro create_fits_pupil,allsav
  
  if n_elements(allsav) eq 0 then begin
;allsav='/data/slam/home/lagg/data/pupil_apo/pupil_apo_out_frat=050_fwhm=100_n=2.3_tilt=0.0_6173_028588.sav'
allsav='/data/slam/home/lagg/data/pupil_apo/pupil_apo_out_frat=050_fwhm=100_n=2.3_tilt=0.3_6173_028588.sav'
  endif else begin
    mode=1
  endelse

  for ia=0,n_elements(allsav)-1 do begin
;  for ia=0,n_elements(allsav(0))-1 do begin
    sav=allsav(ia)
    lslpos=strpos(sav,'/',/reverse_search)
    dotsav=strpos(sav,'.sav')
    froot=strmid(sav,lslpos+1,dotsav-lslpos-1)
    if lslpos ge 0 then fpath=strmid(sav,0,lslpos+1) else fpath=''
    
    print,'Restoring: '+sav
    restore,/v,sav
;    order_sav=['I','V','Q','U']
    order_sav=['I','Q','U','V']
    order_fits=['I','Q','U','V']
    
                                ;select variable to be written out
    add=''
    
    for isr=0,1 do begin
      
      if isr eq 0 then add='_scan' else add='_ref'
      
      dummy=execute('stokes_sm=stokes_cv'+add)
      contin_sm=reform(stokes_sm(0,0,*,*))
      wl_sm=wl_scan+6173.3412d

      if total(etalon) ge 1e-8 then begin
                                ;set prefilter to one if not present
        if n_elements(prefilter) eq 0 then prefilter=etalon*0+1.
                                ;also write out filter function
        ffilter='./convolve/'+strmid(froot,1,strlen(froot)-1-4)+'.dat'
        ffilter='./convolve/'+froot+'.dat'
        ffilter_fp='./convolve/'+froot+'_fp.dat'
        ffilter_pre='./convolve/'+froot+'_pre.dat'

        openw,/get_lun,unit,ffilter,error=err
        if err ne 0 then message,'unable to open filter file: '+ffilter
        openw,/get_lun,unit_fp,ffilter_fp,error=err
        openw,/get_lun,unit_pre,ffilter_pre,error=err
        printf,unit,' ;convolution file, from '+sav
        printf,unit,' ;simulates the effect of a Fabry-Perot measurement at 617.3 nm '
        printf,unit,' ;Contains FP and prefilter'
        printf,unit,' ;WL(A)       Intensity '
        printf,unit_fp,' ;convolution file, from '+sav
        printf,unit_fp,' ;simulates the effect of a Fabry-Perot measurement at 617.3 nm '
        printf,unit_fp,' ;Contains only FP'
        printf,unit_fp,' ;WL(A)       Intensity '
        printf,unit_pre,' ;convolution file, from '+sav
        printf,unit_pre,' ;simulates the effect of a Fabry-Perot measurement at 617.3 nm '
        printf,unit_pre,' ;Contains only prefilter'
        printf,unit_pre,' ;WL(A)       Intensity '
        if n_elements(lam) eq 0 and $
          n_elements(wl) eq n_elements(prefilter) then lam=wl+6173.3412d
        for i=0l,n_elements(lam)-1 do begin
          printf,unit,lam(i),etalon(i)*prefilter(i),format='(f15.6,e15.6)'
          printf,unit_fp,lam(i),etalon(i),format='(f15.6,e15.6)'
          printf,unit_pre,lam(i),prefilter(i),format='(f15.6,e15.6)'
        endfor
        free_lun,unit
        free_lun,unit_fp
        free_lun,unit_pre
        print,'Wrote out filter functions:'
        print,transpose([ffilter,ffilter_fp,ffilter_pre])
      endif
      
      
      sz=size(stokes_sm)
      nwl=sz(2)
      nx=sz(3)
      ny=sz(4)
      data=make_array(nwl,ny,nx*4,type=sz(0))
      
      for is=0,3 do begin
        iu=where(order_fits eq order_sav(is))
        for ix=0,nx-1 do data(*,*,ix*4+iu)=reform(stokes_sm(is,*,ix,*))
      endfor
                                ;now data contains an array with 1009
                                ;WL-pixels, 454 pixels in y direction
                                ;and 12 pixels in x-direction (3x4
                                ;Stokes vectors)
      szd=size(data)
      print,'============ SIZE ======================'
      print,'Size: ',szd
      print,'WL-pixels: ',szd(1)
      print,'y-pixels: ',szd(2)
      print,'x-pixels (*4): ',szd(3)
      

      data=float(data)
;  byteorder,data,/lswap
      
      fitsout=fpath+froot+add+'.fitscc'
      print,'======= WRITE NEW FITS ================='
      print,'File: ',fitsout
      
      writefits,fitsout,data,append=0
      
;get continuum image
      icont=contin_sm     
;    icont(*)=1                  ;since profiles are already normalized

;wl-vector
      wlvec=wl_sm
      wloff=0. & wlbin=0.
      wset,0 & plot,icont,/yst    
      
;now create ccx file  
      print,'======= WRITE NEW CCX ================='
      filex=fitsout+'x'
                                ;create header
      header=''
      
      writefits,filex,icont,header,append=0
      naxpos=max(where(strpos(header,'NAXIS') eq 0))
      header=[header(0:naxpos), $
              'WL_NUM  = '+string(nwl,format='(i20)')+ $
              ' / WL-bins', $
              'WL_OFF  = '+string(wloff,format='(d20.8)')+' / WL-Offset', $
              'WL_DISP = '+string(wlbin,format='(d20.8)')+' / WL-Dispersion', $
              header(naxpos+1:*)]
      writefits,filex,icont,header,append=0        
      
                                ;write out wl-vector as extension
      writefits,filex,wlvec,append=1
      print,'Wrote '+filex
    endfor 
  endfor 
  print,'Done.'
  
  stop
end
