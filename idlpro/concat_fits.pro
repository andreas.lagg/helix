pro concat_fits,fits,out=fout,gap=gap
  
  if n_params() eq 0 then begin
    print,'Usage:'
    print,'  concat_fits,[''file1.001cc'',''file1.002cc''],out=''new.cc'''
    reset
  endif
  
  nf=n_elements(fits)
  if n_elements(gap) ne nf-1 then begin
    print,'Please specify the gaps between the fits files.'
    reset
  endif
  
  nax3=0l
  header=''
  for i=0,nf-1 do begin
    dum=rfits_im(fits(i),1,dd,hdr,nrhdr)
    nax3=nax3+dd.naxis3
   endfor
   ndark=(nf-1)*2
   ndark=0
   
   nax3=nax3+total(gap)*4-4*ndark
   dd.naxis3=nax3
   
   
                                ;change header
   nax3pos=strpos(hdr(0),'NAXIS3')
   hdr(0)=strmid(hdr(0),0,nax3pos+9)+string(nax3,format='(i21)')+strmid(hdr(0),nax3pos+30,strlen(hdr(0)))
   
                                ;open new file
  get_lun,unit_out
  openw,unit_out,fout
  writeu,unit_out,byte(hdr)
  
  
  if(dd.bitpix eq 8) then begin
    dat_out=assoc(unit_out,bytarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
  endif else if(dd.bitpix eq 16) then begin   
    dat_out=assoc(unit_out,intarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
  endif else if(dd.bitpix eq 32) then begin   
    dat_out=assoc(unit_out,lonarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
  endif
  
  cpos=0
  for i=0,nf-1 do begin
    dum=rfits_im(fits(i),1,dd,hdr,nrhdr)
    get_lun,unit
    openr,unit,fits(i)
    if(dd.bitpix eq 8) then begin
      datos=assoc(unit,bytarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
    endif else if(dd.bitpix eq 16) then begin   
      datos=assoc(unit,intarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
    endif else if(dd.bitpix eq 32) then begin   
      datos=assoc(unit,lonarr(dd.naxis1,dd.naxis2),long(2880)*nrhdr)
    endif
    
    jd=0
;    if i ge 1 then jd=8
    for j=jd,dd.naxis3-1 do begin
      dat_out(j+cpos-jd)=datos(j)
    endfor
    close,unit  & free_lun,unit
    cpos=cpos+dd.naxis3
    if i lt nf-1 then  cpos=cpos+gap(i)*4
  endfor
  close,unit_out  & free_lun,unit_out
  
  img=readfits(fout)
  imgi=total(img(0:10,*,indgen(120)*4+0),1)
  imgv=total(img(170:180,*,indgen(120)*4+3),1)
  window,xs=500,ys=500
  tvscl,congrid(imgv,500,500)

end
