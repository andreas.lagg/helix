
;example:
; compare_inversions,'/samfs/sun/irpolar/atm_sav/13may01.014_bfree_1x1_lzs_v01.pikaia.sav','/samfs/sun/irpolar/atm_sav/13may01.014_bfree_1x1_pb_v01.pikaia.sav'
pro compare_inversions,inv1,inv2
  
  restore,inv1,/v
  pr1=temporary(pikaia_result)
  
  restore,inv2,/v
  pr2=pikaia_result
  
  
;creat a sav-file containing the ratio pr1 / pr2
  tn=tag_names(pr1.fit.atm.par)
  for i=0,n_tags(pr1.fit.atm.par)-1 do begin
    ratio=pr1.fit.atm.par.(i) / pr2.fit.atm.par.(i)
    nofin=where(finite(ratio) eq 0)
    if nofin(0) ne -1 then ratio(nofin)=0
    if tn(i) eq 'VLOS' then ratio=(ratio-1.)*1000
    pikaia_result.fit.atm.par.(i)=ratio
  end
  
  save,file='ratio.sav',/xdr,/compress,pikaia_result,/verbose
  
  stop
  
end
