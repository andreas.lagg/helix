function resize,img,xc,yc
  
  if (size(img))(0) gt 2 then  timg=reform(img) else timg=img
  
  sz=size(timg)
  nxc=n_elements(xc)
  nyc=n_elements(yc)
  fnew=fltarr((nxc-1)>1,(nyc-1)>1)
                                ;do nothing if size is the same
  if min(size(float(timg)) eq size(fnew)) eq 1 then return,float(timg)
  
  
  fnew(0,0)=img(0,0)
  for ix=0,nxc-2 do for iy=0,nyc-2 do begin
;    xci=where(xc eq xc(ix))
;    for iy=0,nyc-2 do begin
;      yci=where(yc eq yc(iy))
      if xc(ix) ge 0 and xc(ix+1)-1 lt sz(1) and $
        yc(iy) ge 0 and yc(iy+1)-1 lt sz(2) then begin
        neq0=timg(xc(ix):xc(ix+1)-1,yc(iy):yc(iy+1)-1) ne 0 and $
          finite(timg(xc(ix):xc(ix+1)-1,yc(iy):yc(iy+1)-1))
        sumneq0=total(neq0)
        if sumneq0 ge 1 then $
          fnew(ix,iy)=(total(timg(xc(ix):xc(ix+1)-1,yc(iy):yc(iy+1)-1),/nan)/ $
                       sumneq0)        
      endif
;    endfor
    endfor
    return,fnew
end

