function add_noise,prof,noise=noise,inoise=inoise, $
                   doprefilter=doprefilter,prefilter=prefilter
  common verbose,verbose
  
  if abs(noise) lt 1e-10 and abs(inoise) lt 1e-10 then return,prof
  
  if (verbose ge 1) then begin
    print,'Adding noise to IQUV-profile: level=',noise
    print,'Adding noise to I-profile: level=',inoise
  endif
                               ;scale noise with light level (where
                                ;there is less light you have more
                                ;noise!)
  
                                ;add random noise to every profile
  inoisei=(randomu(seed,n_elements(prof.wl),/normal))*inoise
  noisei=(randomu(seed,n_elements(prof.wl),/normal))*noise
  noiseq=(randomu(seed,n_elements(prof.wl),/normal))*noise
  noiseu=(randomu(seed,n_elements(prof.wl),/normal))*noise
  noisev=(randomu(seed,n_elements(prof.wl),/normal))*noise
  
                                ;take into account reduced photon flux
                                ;with prefilter
  if keyword_set(doprefilter) eq 1 then begin
    pf=prefilter>1e-4
    noisei=noisei/pf
    noiseq=noiseq/pf
    noiseu=noiseu/pf
    noisev=noisev/pf
  endif
  
  if max(tag_names(prof) eq 'IC') eq 1 then ic=prof.ic else ic=1.
  prof.i=prof.i+noisei*(1./sqrt(prof.i/ic))+inoisei
  prof.q=prof.q+noiseq*(1./sqrt(prof.i/ic))
  prof.u=prof.u+noiseu*(1./sqrt(prof.i/ic))
  prof.v=prof.v+noisev*(1./sqrt(prof.i/ic))
  
  
  if verbose ne 0 then $
    print,'Added artificial random noise, level: '+n2s(noise)
  
  return,prof
end
