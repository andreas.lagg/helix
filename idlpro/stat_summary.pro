;display summary for statistical data obtained using the
;pixel-repetition (PIXEL_REP) in Helix+.
pro stat_summary,sav,x=x,y=y,cancel=cancel
  
  print,'===================================================================='
  print,'Summary of statistical information in '
  print,'    '+sav
  
  restore,sav
  
  sz=size(statistic)
  if sz(0) eq 2 then begin
    szx=sz(1)
    szy=sz(2)
  endif else begin
    szx=n_elements(statistic)
    szy=1
  endelse
  
  if n_elements(x) eq 0 then x=[0,szx-1] $
  else if n_elements(x) eq 1 then x=[0,0]+x(0)
  if n_elements(y) eq 0 then y=[0,szy-1] $
  else if n_elements(y) eq 1 then y=[0,0]+y(0)
  
  stxy=statistic(x(0):x(1),y(0):y(1))
  
  tn=tag_names(statistic.mean)
  sz=size(statistic)
  print,'nx=',sz(1)
  if sz(0) eq 2 then print,'ny=',sz(2)
  print,'max. number of samples=',max(statistic.n)
  natm=n_elements(statistic(0).mean)
  print,'number of atmospheric components=',natm
  
  xyadd='x=['+n2s(x(0))+'...'+n2s(x(1))+'], '+$
    'y=['+n2s(y(0))+'...'+n2s(y(1))+']'
  nel=n_elements(stxy)
  if max(tn eq 'FITNESS') eq 1 then $
    print,'Fitness (mean, min, max):', $
    mean(stxy.fitness),minmaxp(stxy.fitness)
  plot,stxy.fitness,title='Fitness '+xyadd
  print,'Average over (='+n2s(nel)+') pixel(s):'
  print,xyadd
  for ia=0,natm-1 do begin
  print,'--------------------------------------------------------------------'
    print,'Component #'+n2s(ia)
    print,'Parameter','MEAN','STDEV',format='(3a12)'
    for it=0,n_elements(tn)-1 do begin
      mean=total(stxy.mean(ia).(it))/nel
      var=total(stxy.var(ia).(it))/nel
      if var ne 0 then $
        print,format='(a12,2f12.3)',tn(it),mean,sqrt(var)
    endfor
  endfor
  print,'===================================================================='
  print,'(Specify x,y pixel with keywords x=... and y=...)'
  
  if keyword_set(cancel) then return
  
  
                                ;plot average profile.
  
                                ;make_sav.pro puts average values
                                ;already in pikaia_result.
  pr=pikaia_result  
  all=0
  for ix=x(0),x(1) do for iy=y(0),y(1) do begin
    if pr.fit(ix,iy).fitness gt 1e-5 then begin
      xypix='x='+n2s(fix(pr.fit(ix,iy).x),format='(i4.4)')+ $
        ' y='+n2s(fix(pr.fit(ix,iy).y),format='(i4.4)')    
      if all eq 0 then begin
        print,'Plot best fit profile for '+xypix+' [Y/n/[a]ll/[c]ancel]? ', $
          format='(a,$)'
        repeat key=strupcase(get_kbrd()) until key ne ''
        print
        if strupcase(key) eq 'C' then return
        all=strupcase(key) eq 'A'
      endif else key='Y'
      if strupcase(key) ne 'N' then begin
        print,'Pixel: '+xypix
        for is=0l,n_elements(statistic)-1 do begin
          dummy=max(statistic(is).fitness,imax)
          for it=0,n_tags(statistic(is).mean)-1 do $
            for ic=0,pr.input.ncomp-1 do begin
            pr.fit(is).atm(ic).par.(it)= $
              statistic(is).mean(ic).(it)+statistic(is).diff(ic,imax).(it)
          endfor
          pr.fit(is).fitness=statistic(is).fitness(imax)
        endfor
        fp=get_stokes(result=pr,x=pr.fit(ix,iy).x,y=pr.fit(ix,iy).y, $
                      /show,verbose=1)
      endif
    endif
  endfor
end
