;convert structure variable defining the atmospheric parameters to the
;pikaia vector format and viceversa
function struct2pikaia,atm=atm,par=par,reverse=reverse
  common scale,scale
  common ff_info,nfz,ff_idx
  common couple,couple_idx ;contains indices for coupling of parameters
  
  nt=n_tags(atm.par)
  tn=tag_names(atm.par)
  na=n_elements(atm.par)
  if keyword_set(reverse) then begin
    
    ifit=0
    for ia=0,na-1 do for it=0,nt-1 do if atm(ia).fit.(it) ne 0 then  begin
      cpos=it*na+ia
                                ;uncoupled parameters
      if atm(ia).fit.(it) gt 0 or couple_idx(cpos) eq cpos then begin
        atm(ia).par.(it) = scale(ia).(it).min + $
          (par(ifit) * (scale(ia).(it).max-scale(ia).(it).min))
        ifit=ifit+1
      endif else begin          ;coupled parameters
        atm(ia).par.(it)=atm(couple_idx(cpos) mod na).par.(couple_idx(cpos)/na)
      endelse
    endif
    ffold=atm.par.ff
                                ;set FF of last component
    if na gt 1 then atm(na-1).par.ff=1.-total(atm(0:na-2).par.ff)
    fftot=total(abs(atm.par.ff))
    if abs(fftot-1.) gt 1e-4 then begin
      atm.par.ff=abs(atm.par.ff)/fftot
    endif
    
                                ;total filling factor has to be one.
                                ;Do not touch fitted factors but
                                ;adjust the FF which are not fitted
                                ;such that the sum of all FFs is 1.
; if total(atm.par.ff) lt 0.98 then stop     
; oldff=atm.par.ff
;  fftot=total(atm.par.ff)
;       for i0=0,nfz-1 do $
;         atm(ff_idx(i0)).par.ff= atm(ff_idx(i0)).par.ff * $
;         (1-total(atm.par.ff*atm.fit.ff))/total(atm(ff_idx).par.ff)
; if min(finite(atm.par.ff)) eq 0 then stop     
    
    retval=atm
  endif else begin              ;convert atm to pikaia par-list.
                                ;This is done only once, speed is not
                                ;so important
    
                                ;automatically set one FF-value to
                                ;not-fitted
    tot_ff=total(atm.par.ff)
    if tot_ff gt 1. then begin
      message,/cont,'Total FF > 1, scaling all FF to 1.'
      atm.par.ff=atm.par.ff/tot_ff
    endif
    
    ff_fit=atm.fit.ff
    if min(ff_fit) ne 0 then atm(na-1).fit.ff=0
    ff_idx=where(atm.fit.ff eq 0)
    nfz=n_elements(ff_idx)
    
    fit=intarr(na,nt)
    for ia=0,na-1 do for it=0,nt-1 do fit(ia,it)=atm(ia).fit.(it)
    fidx=where(fit ne 0)
    if fidx(0) eq -1 then message,'No parameters to be fitted.'
    nf=n_elements(fidx)
    par=fltarr(nf)
    
    ifit=0
    couple=fit(UNIQ(fit, SORT(fit)))
    cdone=bytarr(n_elements(couple))
    couple_idx=lonarr(na,nt)
    for ia=0,na-1 do for it=0,nt-1 do if fit(ia,it) ne 0 then begin $
      
      add2par=1                 ;check for coupled parameters, include
                                ;it into vector par only once.
      if fit(ia,it) lt 0 then begin
        cidx=(where(couple eq fit(ia,it)))(0)
        couple_idx(ia,it)=min(where(fit eq fit(ia,it)))
        if cdone(cidx) eq 1 then begin
          add2par=0
        endif else begin
          cdone(cidx)=1b
        endelse
      endif
      
                                ;add parameter to vector par
      if add2par then begin
        par(ifit) = $
          ((atm(ia).par.(it) - scale(ia).(it).min) / $
           (scale(ia).(it).max-scale(ia).(it).min))
        ifit=ifit+1
      endif
    endif
    par=par(0:ifit-1)

    retval=par
  endelse
  return,retval
end
