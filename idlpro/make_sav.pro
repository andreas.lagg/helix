;read in data from fortran helix and create a single sav-file for
;use with xdisplay.pro
;

;function with same functionality as file_search, but:
; - assumes x???? structure in first directory
; - assumes _atm and _profile.dat in 2nd level
;should be much faster then normal file_search routine, es
function get_files,dir,mask=mask,count=count
  
  count=0l
  dirs=file_search(dir+'/x*',count=ndir)
  if ndir eq 0 then  begin      ;try localdir
    ndir=1 & dirs=dir+'/.'
  endif
  for i=0l,ndir-1 do begin
    file=file_search(dirs(i)+'/'+mask,count=nf)
    count=count+nf
    if i eq 0 then begin
                                ;estimate size:
      all=strarr((nf>1)*ndir*2)
      na=n_elements(all)
    endif
    if count ge na then begin
      all=[all,strarr((nf>1)*ndir)]
      na=n_elements(all)
    endif
    if nf ge 1 then all(count-nf:count-1)=file
    if fix(i*80./ndir) ne fix((i-1)*80./ndir) then $
      print,'.',format='(a,$)'
  endfor
  print
  print,count,' files.'
  
  return,all(0:(count-1)>0)
end

pro make_sav,data_dir=ddir,nox=nox,overwrite=overwrite
  common wgst,wgst
  
  if n_elements(nox) eq 0 then nox=0
  
  if n_elements(ddir) eq 0 then begin
    print,'create sav-file for xdisplay.pro from atm_archive created by fortran-helix.'
    print,'Usage:'
    print,'   make_sav,data_dir=''atm_archive/atm_13may01.018'''
    print,'To combine multiple runs into one sav-file:'
    print,'   make_sav,data_dir=''atm_archive/''+[''atm_13may01.018_1comp'','+$
      '''atm_13may01.018_2comp'']'
    retall
  endif
  
  ndir=n_elements(ddir)
  
                                ;reading input-files for
                                ;consistency checks: test if profiles
                                ;of different directory match
  ncomp=intarr(ndir)
  for id=0,ndir-1 do begin
    data_dir=ddir(id)
    if (file_info(data_dir)).exists eq 0 then $
      message,/cont,'Not found: '+data_dir $
    else begin
                                ;load corresponding input file: this
                                ;file should be located in the root
                                ;directory of the atm_archive
                                ;directory and must be named input.ipt
      iptfile=data_dir+'/input.ipt'
      ipt=read_ipt(iptfile)
      
                                ;check if file was created before Jan
                                ;19th
                                ;if so then old_norm=1
      fi=file_info(iptfile)
      jan_19_2007=double(conv_time(old='date',new='sec',/idl,'19012007.1800'))
      fitime=systime(1,fi.ctime)
      if fitime-jan_19_2007 lt 0 then begin
        message,/cont,'input file is older than Jan 19, 2007 - set OLD_NORM=1'
        ipt.old_norm=1
      endif
      jan_31_2011=double(conv_time(old='date',new='sec',/idl,'31012011.1800'))
      if fitime-jan_19_2007 lt 0 then begin
        message,/cont,'input file is older than Jan 31, 2011 - set OLD_VOIGT=1'
        ipt.old_voigt=1
      endif
      
                                ;check for 'FIT' flag in all
                                ;multi-iteration steps
      if ipt.nmi gt 1 then begin
        for it=0,n_tags(ipt.mi)-1 do for ic=0,ipt.ncomp-1 do begin
          inn=max(where(ipt.mi(ic).(it).fit ne 0))
          if inn(0) ne -1 then $
            ipt.atm(ic).fit.(it)=ipt.mi(ic).(it).fit(inn(0))
        endfor
      endif
      
      add=n2s(id,format='(i2.2)')
      exs=execute('ipt'+add+'=ipt')
      print,'Input-File: '+iptfile+' (NCOMP='+n2s(ipt.ncomp)+')'
      
      if id gt 0 then begin
                                ;check if observations agree
        if ipt.observation ne ipt01.observation then $
          print,'WARNING: Observations do not agree! ('+ipt.observation+' <> '+ $
          ipt01.observation+')'
        
      endif
      
      ncomp(id)=ipt.ncomp
    endelse
  endfor
  if (file_info(data_dir)).exists eq 0 then return

                                ;read header information
  obsfile=ipt.dir.profile+'/'+ipt.observation
  istip=check_tipmask(obsfile,verbose=verbose)
  if istip or strmid(obsfile,strlen(obsfile)-2,2) eq 'cc' then repeat begin
    print,'Reading TIP file: ',obsfile
    header=read_tip_header(obsfile,verbose=1,/force)
    okay=1b
    if max(tag_names(header) eq 'ERROR') eq 1 then $
      if header.error eq 1 then begin
      print,'The observation '+ipt.observation+' could not be found.'
      print,'Do you want to [c]ontinue with empty header information or'
      print,'do you want to [l]ocate the observation file? (Press ''c'' or ''l'')'
      repeat key=strupcase(get_kbrd()) until key eq 'C' or key eq 'L'
      if key eq 'C' then begin
        message,/cont,'Continue with empty header information.'
      endif else begin
        read,'Please enter the full path for the observation '+ $
          ipt.observation+' : ',obsfile
        okay=0b
      endelse
    endif
;    obsfile.tip=1
  endrep until okay
  ishinode=check_hinmask(obsfile,files=hfile)
  if ishinode then begin
    dummy=readfits_ssw(hfile(0),hdr,/nodata,/silent)
    header=fits_header2st(hdr)  
;    obsfile.hinode=1
  endif
  
                                ;check for existing sav-file
  if ipt.dir.atm_suffix ne '' then suffix='_'+ipt.dir.atm_suffix $
  else suffix=''
  obs=ipt.observation
  if strmid(obs,strlen(obs)-1,1) eq '/' then obs=strmid(obs,0,strlen(obs)-1)
  psav=ipt.dir.sav+'/'+obs+suffix+'.pikaia.sav'
  print,'Creating sav-file: '+psav
  fi=file_info(psav)
  key=''
  if fi.exists eq 1 then begin
    if keyword_set(overwrite) eq 0 then begin
    print,'File '+psav+' already exists.'
    print,'[U]pdate, [O]verwrite, [R]ename or [C]ancel ? ',format='(a,$)'
    repeat begin
      key=strupcase(get_kbrd(1))
    endrep until key eq 'C' or key eq 'O' or key eq 'R' or key eq 'U'
    print
    case key of
      'C': reset
      'O': 
      'U': 
      'R': begin
        presav=strmid(psav,0,strpos(psav,'.sav',/reverse_search))
        sadd=''
        read,prompt='Enter new name for sav-file: '+presav,sadd
        if strpos(sadd,'.sav') eq -1 then sadd=sadd+'.sav'
        psav=strcompress(/remove_all,presav+sadd)
        key='O'
      end
    endcase
  endif 
  endif 
  
  if key eq 'U' then begin
    print,'Restoring old sav-file for update: '+psav+' ...',format='(a,$)'
    restore,psav
    prold=temporary(pikaia_result)
    print,' Done.'
    xvec=total(prold.fit.x,2)/((total(prold.fit.x ge 1,2)>1))
    yvec=total(prold.fit.y,1)/((total(prold.fit.y ge 1,1)>1))
  endif
  
  minx=99999 & maxx=0
  miny=99999 & maxy=0
  for id=0,ndir-1 do begin
    data_dir=ddir(id)
    add=n2s(id,format='(i2.2)')
    exs=execute('ipt=ipt'+add)
    
    print,'Counting atm files, please wait ...'
;    atm_files=file_search(data_dir+'/*/*_atm.dat',count=nfa)
    atm_files=get_files(data_dir+'/',mask='*_atm.dat',count=nfa)
    if nfa lt 1 then message,'no atmospheres found in '+data_dir
    print,'Counting profile files, please wait ...'    
;    prof_files=file_search(data_dir+'/*/*_profile.dat',count=nfp)
    prof_files=get_files(data_dir+'/',mask='*_profile.dat',count=nfp)
    noprof_flag=nfp eq 0

    if nfa ne nfp and noprof_flag eq 0 then $
      message,/cont,'# of profile-files does not match # of atmosphere-files.'
                                ;check which files are different:
                                ;and file size of atm is bigger than
                                ;100 bytes
    
    if noprof_flag eq 0 then begin
    use=bytarr(nfp)+1b
    prof_us=strarr(nfp)
    print,'Checking consistency, please wait ...'
    for i=0l,nfp-1 do begin
      prof_us(i)=strmid(prof_files(i),0, $
                        (strpos(/reverse_search,prof_files(i),'_'))(0))
      fi=file_info(prof_us(i)+'_atm.dat')
      if fi.size lt 100 then begin
        print,'No valid atm-file for '+prof_us(i)
        use(i)=0
      endif
      if fix(i*80./nfp) ne fix((i-1)*80./nfp) then $
        print,'.',format='(a,$)'
    endfor
    print
    wu=where(use eq 1)
    if wu(0) eq -1 then message,'No profile-files match atm-files.'
    prof_files=prof_files(wu)
    atm_files=prof_us(wu)+'_atm.dat'
  endif
    
    nw=n_elements(atm_files)
    if nw ne nfp then print,'Using '+n2s(nw)+' files only (Total: '+ $
      n2s(max([nfa,nfp]))+')'
    nfa=nw
    nfp=nfa
    
    print,'Reading '+n2s(nfa)+' profiles from '+data_dir
    
    filest={atm:'',prof:'',path:'',x:0,y:0,master:0l}
    file=replicate(filest,nfa)
    for i=0l,nfa-1 do begin
      slpos=strpos(/reverse_search,atm_files(i),'/')
      file(i).path= $
        strmid(atm_files(i),0,slpos)
      file(i).atm=strmid(atm_files(i),slpos+1,strlen(atm_files(i)))
      if noprof_flag eq 0 then $
      file(i).prof=strmid(prof_files(i),slpos+1,strlen(prof_files(i)))
      
      ypos=strpos(file(i).atm,'y')
      file(i).x=fix(strmid(file(i).atm,1,ypos))
      file(i).y=fix(strmid(file(i).atm,ypos+1,ypos))
    endfor
    
    
                                ;check for repeated pixels
    i=nfa-1l
    file.master=-1
    use=lonarr(nfa)
    statmode=0
    fxy='x'+string(file.x,format='(i5.5)')+'y'+string(file.y,format='(i5.5)')
    iu=[-1,uniq(fxy)]
    smax=0
    for i=1l,n_elements(iu)-1 do begin
      if iu(i)-iu(i-1) eq 1 then begin
        file(iu(i)).master=iu(i)
      endif else begin
        statmode=1b
        isame=indgen(iu(i)-iu(i-1))+iu(i-1)+1
        file(isame).master=iu(i)
        if (iu(i)-iu(i-1)) gt smax then smax=iu(i)-iu(i-1)
      endelse
    endfor
    
    iuq=uniq(file.master,sort(file.master))
    funiq=file(iuq)
    nfu=n_elements(funiq)
    
    statmode=nfu ne nfa    ;statistic mode is on if more than one file
                                ;exists per pixel 
    if statmode then begin
      message,/cont, $
        'Statistics mode ON: found more than one profile per pixel.'+ $
        ' The results are stored in a separate variable ''statistic''.'
      if key eq 'U' then begin
        message,/cont,'Statistic mode does not work for ''Update''. Setting '+$
          'mode to ''overwrite''.'
        key='O'
      endif
    endif else smax=nfa
    
    
                                ;define output structure
    dx=max(file.x)-min(file.x)
    dy=max(file.y)-min(file.y)
    pikaia_result=result_struc(nx=dx+1,ny=dy+1,ipt=ipt,header=header)
    ipt.x=[min(file.x),max(file.x)]
    ipt.y=[min(file.y),max(file.y)]
    pikaia_result.input.x=ipt.x
    pikaia_result.input.y=ipt.y
    
                                ;fill output structure with line info
    old=pikaia_result.line
    struct_assign,ipt.line,old
    pikaia_result.line=old
    
    
                                ;fill atmospheric info
    iold=0
    minfx=min(file.x)
    minfy=min(file.y)
    
    
    natm=pikaia_result.input.ncomp
    emom=pikaia_result.fit(0).atm.par
    
    diff=replicate(pikaia_result.fit(0).atm(0).par,natm,smax)
    fitness=fltarr(smax)
    if statmode eq 1 then begin
      stat={mean:emom,var:emom,skew:emom,kurt:emom,diff:diff, $
            fitness:fitness,n:0l}
      statistic=replicate(stat,dx+1,dy+1)
    endif
      
    t0=systime(1)
    for i=0l,nfu-1 do begin
      ix=funiq(i).x-minfx
      iy=funiq(i).y-minfy
      
      doread=1
      if key eq 'U' then begin
        ixo=(where(xvec eq funiq(i).x))(0)
        iyo=(where(yvec eq funiq(i).y))(0)
        if ixo ne -1 and iyo ne -1 then $
          doread=prold.fit(ixo,iyo).fitness le 1e-5
      endif
      if doread eq 1 then begin ;read atmosphere
        if statmode then iif=where(file.master eq funiq(i).master) $
        else iif=i
        nif=n_elements(iif)
        maxfitness=0.
        for ii=0,nif-1 do begin
          result=read_atm(file=file(iif(ii)), $
                          ipt=ipt,init=i eq 0 and ii eq 0,header=header) 
          if ii eq 0 then begin
            resfit=result.fit
            resline=result.line
          endif else begin ;only store atm parameters for statistical analysis 
            resfit=[resfit,result.fit]
;           resline=[resline,result.line]
          endelse
          if result.fit.fitness gt maxfitness then begin
            bestresult=result
            maxfitness=result.fit.fitness
          endif
        endfor
        if nif ge 2 then begin ;do statistical analysis for atm-results
          statistic(ix,iy).n=nif
          tn=tag_names(resfit.atm.par)
          stn=tag_names(stat.mean)
          for itr=0,n_elements(tn)-1 do begin
            its=(where(stn eq tn(itr)))(0)
            case tn(itr) of
              'AZI': cycle=[-90,90]
              'INC': cycle=[0,180]
              else: cycle=-1
            endcase
            docycle=n_elements(cycle) eq 2
            if docycle then begin
              range=max(cycle)-min(cycle)
              center=total(cycle)/2.
              mincy=min(cycle)
            endif
            for ia=0,natm-1 do begin
              mom=moment(resfit.atm(ia).par.(itr))
              if docycle then begin
                aa=resfit.atm(ia).par.(itr)
                flip=where(aa lt center)
                if flip(0) ne -1 then begin
                  aa(flip)=aa(flip)+range
                  mom2=moment(aa)
                                ;if variance of cyclic data is better
                                ;then take the cyclic value
                  if mom2(1) lt mom(1)*.99 then  begin
;            print,'NEW',aa
;            print,'VAR',ix,iy,[mv(0:1),mv2(0:1)]
                    mom=mom2
                    mom(0)=((mom(0)-mincy+range) mod range) +mincy
                  endif
                endif
              endif
              statistic(ix,iy).mean(ia).(its)=mom(0)
              statistic(ix,iy).var(ia).(its)=mom(1)
              statistic(ix,iy).skew(ia).(its)=mom(2)
              statistic(ix,iy).kurt(ia).(its)=mom(3)
              statistic(ix,iy).diff(ia,0:nif-1).(its)= $
                transpose(resfit(0:nif-1).atm(ia).par.(itr))-mom(0)
              resfit.atm(ia).par.(itr)=mom(0) ;store mean value
            endfor
          endfor
          statistic(ix,iy).fitness(0:nif-1)=resfit.fitness
          resfit=bestresult.fit
          resline=bestresult.line
        endif
        
        
        old=pikaia_result.fit(ix,iy)
        struct_assign,resfit,old
        pikaia_result.fit(ix,iy)=old
        
        old=pikaia_result.line
        struct_assign,resline,old
        pikaia_result.line=old
        
        for il=0,n_elements(resline)-1 do begin
;         for ia=0,pikaia_result.input.ncomp-1 do begin
          for it=0,n_tags(resline.par)-1 do begin 
            pikaia_result.fit(ix,iy).linepar(il).(it)= $
              resline(il).par.(it)
          endfor
       endfor
        old=pikaia_result.fit(ix,iy).straypol_par
        struct_assign,resline.straypol_par,old
        pikaia_result.fit(ix,iy).straypol_par=old
      endif else begin
;         old=pikaia_result.fit(ix,iy)
;         new=prold.fit(ii(0))
;         struct_assign,new,old
;         pikaia_result.fit(ix,iy)=old        
        pikaia_result.fit(ix,iy)=prold.fit(ixo,iyo)
      endelse
        
                                ;fill input.x + input.y variable if
                                ;stepsize is larger than one
        if 1 eq 0 then begin
        get_profidx,x=funiq(i).x,y=funiq(i).y,stepx=ipt.stepx,stepy=ipt.stepy, $
          rgx=ipt.x,rgy=ipt.y, $
          vecx=xpvec,vecy=ypvec,average=ipt.average,scansize=ipt.scansize
        xpv=((xpvec-min(funiq.x))>0)<dx
        ypv=((ypvec-min(funiq.y))>0)<dy
        if n_elements(xpv) eq 1 then xpv=[xpv,xpv]
        if n_elements(ypv) eq 1 then ypv=[ypv,ypv]
        pikaia_result.fit(xpv(0):xpv(1),ypv(0):ypv(1)).x=funiq(i).x
        pikaia_result.fit(xpv(0):xpv(1),ypv(0):ypv(1)).y=funiq(i).y
        endif
        
      
      
                                ;progress info
      if ((fix(float(i)/nfu*10) ne fix(float(iold)/nfu*10) or i eq 100) and $
          (i-iold gt 100)) or (iold eq 0 and i eq 100) then begin
        t1=systime(1)
        trem=((t1-t0)/i*nfu-(t1-t0))
        tend=systime(0,t1+trem)
        if trem lt 120 then ims=1 else ims=0
        print,'Finished in '+string(trem/([60.,1.])(ims),format='(f5.1)')+ $
          ' '+(['min','sec'])(ims)+', end: '+tend
        iold=i
      endif
      
    endfor 
    print
    
                                ;store results for different directories
    exs=execute('ipt'+add+'=temporary(ipt)')
    exs=execute('pikaia_result'+add+'=temporary(pikaia_result)')
    
    minx=min(funiq.x)<minx & maxx=max(funiq.x)>maxx
    miny=min(funiq.y)<miny & maxy=max(funiq.y)>maxy
    
  endfor
  
                                ;combining multpile directory data
  if ndir gt 1 then begin
;    srtcmp=reverse(sort(ncomp))
    dummy=max(ncomp) & useid=!c
    uadd=n2s(useid,format='(i2.2)')
    exs=execute('ipt=ipt'+uadd)
    print,'Use input-file from'+ddir(useid)
    exs=execute('pruse=pikaia_result'+uadd)
                                ;define output size
    nx=maxx-minx+1
    ny=maxy-miny+1
    efit=pruse.fit(0)           ;reset structure
    for it1=0,n_tags(efit)-1 do $
      if n_tags(efit.(it1)) eq 0 then efit.(it1)=0 $
    else for it2=0,n_tags(efit.(it1))-1 do $
      if n_tags(efit.(it1).(it2)) eq 0 then efit.(it1).(it2)=0 $
    else for it3=0,n_tags(efit.(it1).(it2))-1 do $
      if n_tags(efit.(it1).(it2).(it3)) eq 0 then efit.(it1).(it2).(it3)=0 $
    else for it4=0,n_tags(efit.(it1).(it2).(it3))-1 do $
      efit.(it1).(it2).(it3).(it4)=0

                                ;define output structure
    pikaia_result={line:pruse.line,input:pruse.input, $
                   fit:replicate(efit,nx,ny),header:header}

    pikaia_result.input.x=[minx,maxx]
    pikaia_result.input.y=[miny,maxy]
                                ;fill output structure
    suffix=''
    print
    print,'Combining results from '+n2s(ndir)+' directories:'
    print,'====================================='
    print,'Directory '+n2s(indgen(ndir)+1)+': '+ddir(indgen(ndir))
    print
    print,'  [f] best fitness: store atmosphers with best fitness.'
    print,'  [r] fitness ratio: overwrite atmosphere of first directory only'
    print,'                     when fitness of 2nd (3rd...) directory is'
    print,'                     better by a specified factor'
    print,'  [o] overwrite: overwrite atmospheres from first directory'
    print,'                 with results from 2nd (3rd, ...) directory.'
    print,'  [z] zero fitness: only overwrite the atmospheres where the'
    print,'                    fitness is zero (i.e. no atmosphere present).'
    repeat key=strupcase(get_kbrd()) until $
      key eq 'F' or key eq 'O' or key eq 'Z' or key eq 'R'
    if key eq 'R' then begin
      factor=''
      read,'Fitness ratio factor (default=1.2): ',factor
      if factor eq '' then factor=1.2 else factor=float(factor)
      print,'Replacing atmospheres where fitness of 2nd (3rd, ...) directory'
      print,'is '+n2s(factor,format='(f15.2)')+ $
        'x better than of first directory.'
    endif
    psz=size(pikaia_result.fit)
    for ids=0,ndir-1 do begin
      id=ids
      add=n2s(id,format='(i2.2)')
      exs=execute('tpr=pikaia_result'+add)
      for i=0l,n_elements(tpr.fit)-1 do begin
        x=tpr.fit(i).x-minx & y=tpr.fit(i).y-miny
        if x ge 0 and x lt psz(1) and y ge 0 and y lt psz(2) then begin
          case key of
            'F': replace=pikaia_result.fit(x,y).fitness le tpr.fit(i).fitness
            'R': replace=(pikaia_result.fit(x,y).fitness*factor le $
                          tpr.fit(i).fitness)
            'O': replace=1b
            'Z': replace=pikaia_result.fit(x,y).fitness eq 0
          endcase
          if replace then begin
            ost=pikaia_result.fit(x,y)
            struct_assign,tpr.fit(i),ost
            pikaia_result.fit(x,y)=ost
          endif
        endif
      endfor
      if tpr.input.dir.atm_suffix ne '' then $
        suffix=suffix+'_'+tpr.input.dir.atm_suffix $
      else suffix=suffix+'_'+add
    endfor
  endif else begin
    exs=execute('ipt=temporary(ipt'+add+')')
    exs=execute('pikaia_result=temporary(pikaia_result'+add+')')
;    exs=execute('profile=temporary(profile'+add+')')
    if ipt.dir.atm_suffix ne '' then suffix='_'+ipt.dir.atm_suffix $
    else suffix=''
  endelse
  
                                ;create sav directory
  dir=strmid(psav,0,strpos(psav,'/',/reverse_search)>0)
  if dir ne '' then spawn,'mkdir -p '+dir
                                ;write save-file
  if statmode eq 0 then begin
    save,file=psav,pikaia_result,/compress,/xdr
  endif else begin
    save,file=psav,pikaia_result,statistic,/compress,/xdr
  endelse
  print,'Created sav-file: '+psav
  if statmode then print,'PIXELREP mode:'+ $
      'The sav file contains the best fit of all pixel repetitions.' 
  if statmode then begin
    print,'Statistics stored.'
    print,''
    print,'Best ATM:'
    print_atm,bestresult.fit.atm,scale=bestresult.input.scale, $
      line=bestresult.line,blend=bestresult.fit.blend,gen=bestresult.fit.gen, $
      ipt=bestresult.input
    print,''
    print,'Calling stat_summary,'''+psav+''''
    stat_summary,psav,/cancel
  endif
  dummy=temporary(pikaia_result)
  
  if nfu eq 1 then begin
    if nfa gt 1 then $
      print,'Only one unique pixel. ' + $
      'Sav file can be used for statistical analysis.' $
    else print,'Only one pixel.'
    reset
  endif
    
                                ;call xdisplay
  xdcommand='xdisplay,'''+psav+''''
  if n_elements(wgst) ne 0 then wgst.window.val=0
  if nox eq 0 then begin
    print,'Startinx xdisplay ...'
    dummy=execute(xdcommand)
  endif
  
  print,'Pikaia sav-file created.'
  print,'All results from directory '+add_comma(ddir,sep=':')+' stored in '+psav
  print,'Consider deleting '+add_comma(ddir,sep=':')+' to save disk space.'
  print
  print,'xdisplay-command:'
  print,xdcommand
  

end
