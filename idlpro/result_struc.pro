function result_struc,nx=nx,ny=ny,ipt=ipt,header=header
  common oldobs,oldobs,hdrstore
  
  tn=tag_names(ipt)
  genok=max(tn eq 'GEN')
  blendok=max(tn eq 'BLEND')
  
  if genok then begin
    gtmp=def_genst()
    genok=n_tags(gtmp.par) eq n_tags(ipt.gen.par)
  endif
  
  
  line=replicate(def_linest(),ipt.nline)
  atm=replicate(def_parst(),ipt.ncomp)
  
  if max(tn eq 'NBLEND') eq 0 then nblend=1 else nblend=ipt.nblend>1
  blend=replicate(def_blendst(),nblend)
  gen=def_genst()
  
  if n_elements(header) eq 0 then begin
    read_hdr=0
    if n_elements(oldobs) eq 0 or n_elements(hdrstore) eq 0 then read_hdr=1 $
    else if oldobs ne ipt.observation then  read_hdr=1
    if read_hdr then begin
      header=read_tip_header(ipt.dir.profile+'/'+ipt.observation)
      oldobs=ipt.observation
      hdrstore=header
    endif else header=hdrstore
  endif
  
  
  input=read_ipt(/empty)
  struct_assign,ipt,input,/nozero
  
  if genok eq 0 then input.gen=gen
  if blendok eq 0 then input.blend=blend
  
  fitst={atm:atm,fitness:0d,x:0.,y:0.,blend:blend,gen:gen, $
         linepar:line.par,straypol_par:line.straypol_par}
  
  for it=0,n_tags(atm.par)-1 do begin
    fitst.atm.par.(it)=0
    fitst.atm.fit.(it)=0
  endfor
  for it=0,n_tags(blend.par)-1 do $
    if (tag_names(blend.par))(it) ne 'DUMMY' then begin
    fitst.blend.par.(it)=0
    fitst.blend.fit.(it)=0
  endif
  if nx eq 0 then empty_result={line:line,input:input, $
                                fit:replicate(fitst,ny),header:header} $
  else empty_result={line:line,input:input, $
                     fit:replicate(fitst,nx,ny),header:header}
  
  return,empty_result
end
