pro diswg_set
  common diswg,diswg
  common param,param
  
  for i=0,param.npar-1 do begin
    widget_control,diswg.zmin(0).id, $
      set_value=diswg.zmin(diswg.selpar.val).val
    widget_control,diswg.zmax(0).id, $
      set_value=diswg.zmax(diswg.selpar.val).val
  endfor
  widget_control,diswg.title.id,set_value=diswg.title.str
  widget_control,diswg.subtitle.id,set_value=diswg.subtitle.str
  widget_control,diswg.charsize.id,set_value=diswg.charsize.val
end

pro diswg_selpar
  common diswg,diswg
  common param,param
  common pcwg,pcwg
  common puse,puse
  
  widget_control,diswg.zmin(0).id, $
    set_value=diswg.zmin(diswg.selpar.val).val
  widget_control,diswg.zmax(0).id, $
    set_value=diswg.zmax(diswg.selpar.val).val
  widget_control,diswg.azitop(0).id, $
    set_value=diswg.azitop(diswg.selpar.val).val
  widget_control,diswg.azisolar(0).id, $
    set_droplist_select=diswg.azisolar(diswg.selpar.val).val
  widget_control,diswg.contour(0).id, $
    set_droplist_select=diswg.contour(diswg.selpar.val).val
  widget_control,diswg.contcomp(0).id, $
    set_droplist_select=diswg.contcomp(diswg.selpar.val).val
  
  if diswg.contour(diswg.selpar.val).val ge 1 then $
    if diswg.clevdef(diswg.contour(diswg.selpar.val).val).val eq 1 then begin
    if param.name(diswg.contour(diswg.selpar.val).val-1) eq 'B' then $
      diswg.clevstr(diswg.contour(diswg.selpar.val).val).str= $
      '250,500,750,1000,1250,1500,2000'
    if param.name(diswg.contour(diswg.selpar.val).val-1) eq 'V' then $
      diswg.clevstr(diswg.contour(diswg.selpar.val).val).str='0'
  endif
  diswg.clevdef(diswg.contour(diswg.selpar.val).val).val=0
  widget_control,diswg.clevstr(0).id, $
    set_value=diswg.clevstr(diswg.contour(diswg.selpar.val).val).str
  
  widget_control,diswg.contcol(0).id,set_droplist_select= $
    diswg.contcol(diswg.contour(diswg.selpar.val).val).val
  widget_control,diswg.contthick(0).id,set_droplist_select= $
    diswg.contthick(diswg.contour(diswg.selpar.val).val).val
  
  widget_control,pcwg.minval(0).id,set_value=puse(diswg.selpar.val).minval
  widget_control,pcwg.maxval(0).id,set_value=puse(diswg.selpar.val).maxval
  widget_control,pcwg.usemin(0).id,set_value=puse(diswg.selpar.val).usemin
  widget_control,pcwg.usemax(0).id,set_value=puse(diswg.selpar.val).usemax
 
end

pro diswg_event,event
  common diswg,diswg
  common puse,puse
  common param,param
  common pcwg,pcwg
  common puse,puse
  common iwg_change,iwg_change
  
  if n_elements(puse) eq 0 then begin
    widget_control,diswg.base.id,/destroy
    if n_elements(diswg) ne 0 then dummy=temporary(diswg)
    reset
  endif
  pold=puse
  widget_control,event.id,get_uvalue=uval
  uid=diswg.selpar.val
  case uval of
    'selpar': begin
      diswg.selpar.val=event.index
      if uid ne diswg.selpar.val then diswg_selpar
    end
    'zmin': diswg.zmin(uid).val=event.value
    'zmax': diswg.zmax(uid).val=event.value
    'azitop': diswg.azitop(uid).val=event.select
    'azisolar': diswg.azisolar(uid).val=event.index
    'contour': begin
      oval=diswg.contour(uid).val
      diswg.contour(uid).val=event.index
      if diswg.contour(uid).val ne oval then diswg_selpar
    end
    'contcomp': diswg.contcomp(uid).val=event.index
    'clevstr': diswg.clevstr(diswg.contour(uid).val).str= $
      strcompress(/remove_all,event.value)
    'capplyall': begin
      diswg.contour.val=diswg.contour(uid).val
      diswg.contcomp.val=diswg.contcomp(uid).val
      diswg.clevstr.str=diswg.clevstr(diswg.contour(uid).val).str
      diswg.contcol.val=diswg.contcol(diswg.contour(uid).val).val
      diswg.contthick.val=diswg.contthick(diswg.contour(uid).val).val
    end
    'cdelall': begin
      diswg.contour.val=0
      diswg_selpar
;      widget_control,diswg.contour(diswg.contour(uid).val).id,set_value=diswg.contour(diswg.contour(uid).val).val
    end
    'contcol': diswg.contcol(diswg.contour(uid).val).val=event.index
    'contthick': diswg.contthick(diswg.contour(uid).val).val=event.index
    'aziuseb': begin
      diswg.aziuseb.val=event.select
    end
    'azinel': diswg.azinel.val=event.value
    'azicol': diswg.azicol.val=event.index
    'azithick': diswg.azithick.val=event.index
    'azilen': diswg.azilen.val=event.value
    'azi_quavg': begin
      diswg.azi_quavg.val=event.select
      if diswg.azi_quavg.val eq 1 then begin
        print,'Weight AZI-vectors using strength of linear polarization,'
        print,'       calculated over interval ''Image WL(A)''.'
      endif else print,'normal averaging for AZI-vectors.'
    end
    'charsize': diswg.charsize.val=event.value
    'xrange0': diswg.xrange(0).val=event.value
    'xrange1': diswg.xrange(1).val=event.value
    'yrange0': diswg.yrange(0).val=event.value
    'yrange1': diswg.yrange(1).val=event.value
    'fixmargins0': diswg.fix_margins(0).val=event.value
    'fixmargins1': diswg.fix_margins(1).val=event.value
    'inputout': diswg.inputout.val=event.select
    'info': diswg.info.val=event.select
    'pspages': diswg.pspages.val=event.value
    'title': diswg.title.str=event.value
    'subtitle': diswg.subtitle.str=event.value
    'applycond': puse(uid).applycond=event.value
    'applyquv': diswg.applyquv.val(event.value)=event.select
    'quv_max': diswg.quv_max.val=event.value
    'quv_val': diswg.quv_val.val=event.value
    'quv_and_qu': diswg.quv_and(0).val=event.index
    'quv_and_v': diswg.quv_and(1).val=event.index
    'q_flag': diswg.quv_flag.val(0)=event.select
    'u_flag': diswg.quv_flag.val(1)=event.select
    'v_flag': diswg.quv_flag.val(2)=event.select
    'minval': begin
      puse(uid).minval=event.value
      puse(uid).usemin=1 & pcwg.usemin(uid).val=1
      widget_control,pcwg.usemin(0).id,set_value=pcwg.usemin(uid).val
    end
    'maxval': begin
      puse(uid).maxval=event.value
      puse(uid).usemax=1 & pcwg.usemax(uid).val=1
      widget_control,pcwg.usemax(0).id,set_value=pcwg.usemax(uid).val
    end
    'usemin': puse(uid).usemin=event.select
    'usemax': puse(uid).usemax=event.select
    'control': begin
      case event.value of
        'done': widget_control,diswg.base.id,/destroy
        'reset': begin
          diswg.zmin.val=0
          diswg.zmax.val=0
          diswg.title.str=''
          diswg.subtitle.str=''
          diswg.charsize.val=0
          diswg_selpar
        end
      endcase
    end
    else: begin
      help,/st,event
      print,uid
    end
  endcase
  
  if n_elements(iwg_change) eq 0 then iwg_change=0
  iwg_change=iwg_change or (max(puse.minval ne pold.minval) or $
                            max(puse.maxval ne pold.maxval) or $
                            max(puse.usemin ne pold.usemin) or $
                            max(puse.usemax ne pold.usemax) or $
                            max(puse.applycond ne pold.applycond))
end

pro dis_widget,init=init
  common wgst,wgst
  common param,param
  common diswg,diswg
  common pcwg,pcwg
  common puse,puse
  
  if n_elements(diswg) eq 0 then begin
    subst={id:0l,val:0.,str:''}
    rp=replicate(subst,param.npar)
    rp1=replicate(subst,param.npar+1)
    subst2={id:0l,val:bytarr(64),n:0,str:strarr(64)}
    diswg={base:subst,zmin:rp,zmax:rp,title:subst,subtitle:subst, $
           charsize:subst,control:subst,azinel:subst,azitop:rp,azisolar:rp, $
           azilen:subst,aziuseb:subst,inputout:subst,contour:rp,contcomp:rp, $
           fix_margins:replicate(subst,2),selpar:subst,clevstr:rp1, $
           clevdef:rp1,contcol:rp1,contthick:rp1, $
           capplyall:subst,cdelall:subst, $
           xrange:replicate(subst,2), $
           yrange:replicate(subst,2),info:subst,pspages:subst, $
           azicol:subst,azithick:subst,quv_val:subst,quv_flag:subst2, $
           quv_and:replicate(subst,2),quv_max:subst, $
           azi_quavg:subst,applyquv:subst2}
    incidx=where(param.name eq 'INC')
    if incidx(0) ne -1 then diswg.azitop(incidx(0)).val=1
    diswg.azinel.val=40
    diswg.azilen.val=1.
    diswg.azisolar.val=0.
    diswg.aziuseb.val=0.
    diswg.base.id=-1
    diswg.inputout.val=1
    diswg.contcomp.val=param.ncomp
    diswg.fix_margins.val=[0.08,0.86]
    diswg.selpar.val=0
    diswg.clevdef.val=1
    diswg.contcol.val=0
    diswg.contthick.val=1
    diswg.info.val=1
    diswg.pspages.val=1
    diswg.azicol.val=8
    diswg.azithick.val=2
    diswg.quv_val.val=0.0
    diswg.quv_max.val=1.
    diswg.quv_flag.n=3
    diswg.quv_flag.str(0:2)=['|Q|','|U|','|V|']
    diswg.quv_flag.val(0:2)=[1,1,1]
    diswg.quv_and.val=0
    diswg.applyquv.val(0:3)=[1,0,0,0]
    
    pcwg={usemin:rp,minval:rp,usemax:rp,maxval:rp,applycond:rp}
    sst={usemin:0b,minval:0.,usemax:0b,maxval:0.,applycond:0b}
    if n_elements(puse) ne param.npar then puse=replicate(sst,param.npar)
    pcwg.usemin.val=puse.usemin
    pcwg.usemax.val=puse.usemax
    pcwg.minval.val=puse.minval
    pcwg.maxval.val=puse.maxval
    pcwg.applycond.val=puse.applycond
  endif
  if keyword_set(init) then return
  
  screen=get_screen_size()
  diswg.base.id=widget_base(title='Define Display Settings',/col)  
  
  
  disp=widget_base(diswg.base.id,/frame,col=1)
  sub=widget_base(disp,row=1)
  lab=widget_label(sub,value='Parameter: ')   
  diswg.selpar.id=widget_droplist(sub,uvalue='selpar',value=[param.name])
  widget_control, diswg.selpar.id,set_droplist_select=diswg.selpar.val
  
  
  fr=widget_base(disp,frame=0,col=1)
  pc=widget_base(disp,frame=0,col=1)  
  quv=widget_base(diswg.base.id,/frame,col=1)  
  lab=widget_label(fr,value='Range / Contour Settings',/align_left)  
  lab=widget_label(fr,value='Set z-range (automatic range: min=0 and max=0)')  
  sub=widget_base(fr,col=5,space=0)
  is='_'+n2s(indgen(param.npar),format='(i2.2)')
  cmpstr=[n2s(indgen(param.ncomp)+1),'Corr']
  
  i=diswg.selpar.val
  lab=widget_label(sub,value='Z-min',xsize=80)  
  diswg.zmin(0).id=cw_field(sub,/all_events,/floating,xsize=8, $
                            title='', $
                            uvalue='zmin',value=diswg.zmin(i).val)
  lab=widget_label(sub,value='Z-max',xsize=70)
  diswg.zmax(0).id=cw_field(sub,/all_events,/floating,xsize=8, $
                            title='', $
                            uvalue='zmax',value=diswg.zmax(i).val)
  lab=widget_label(sub,value='Azi-Arrows',xsize=80)
  sub1=widget_base(sub,space=0,row=1)
  diswg.azitop(0).id=cw_bgroup(sub1,uvalue='azitop', $
                               set_value=diswg.azitop(i).val, $
                               /nonexclusive,' ')
  diswg.azisolar(0).id=widget_droplist(sub1,uvalue='azisolar', $
                                       value=['LOS','SOLAR'])
  lab=widget_label(sub,value='Contour',xsize=80)
  diswg.contour(0).id=widget_droplist(sub,uvalue='contour', $
                                      value=['None',param.name])
  widget_control,diswg.contour(0).id,set_droplist_select=diswg.contour(i).val
  lab=widget_label(sub,value='Cont-Comp',xsize=80)
  diswg.contcomp(0).id=widget_droplist(sub,uvalue='contcomp', $
                                       value=cmpstr)
  widget_control,diswg.contcomp(0).id, $
    set_droplist_select=diswg.contcomp(i).val
  
  sub=widget_base(fr,col=1)
  lab=widget_label(sub,value='Contour Levels '+ $
                   '!C(comma separated list, empty for automatic):')
  
  sub=widget_base(fr,row=2)
  diswg.clevstr(0).id=cw_field(sub,/all_events,xsize=30,/string, $
                               uvalue='clevstr',title='', $
                               value=diswg.clevstr(diswg.contour(i).val).str)
  diswg.contcol(0).id=widget_droplist(sub,uvalue='contcol',title='Color:', $
                                      value=['black','red','green','blue', $
                                             'magenta','cyan','orange'])  
  diswg.contthick(0).id=widget_droplist(sub,uvalue='contthick', $
                                        title='thick:',$
                                        value=n2s(indgen(5)+1))
  diswg.capplyall.id=widget_button(sub,uvalue='capplyall', $
                                        value='Apply to all maps')
  diswg.cdelall.id=widget_button(sub,uvalue='cdelall', $
                                        value='Delete from all maps')
  
  lab=widget_label(pc,value='Plot Conditions',/align_left)  
  ll=widget_base(pc,col=2)
  lll=widget_base(ll,col=1)
  llr=widget_base(ll,col=1)
  lab=widget_label(lll,/align_left,value= $
                   '(plot only if the following conditions are fulfilled):')
  sub=widget_base(lll,row=1)
  pcwg.usemin(0).id=cw_bgroup(sub,uvalue='usemin','MinVal:', $
                              set_value=pcwg.usemin(i).val,/nonexclusive)
  pcwg.minval(0).id=cw_field(sub,/all_events,/floating,xsize=8,title=' ', $
                             uvalue='minval',value=pcwg.minval(i).val)
  pcwg.usemax(0).id=cw_bgroup(sub,uvalue='usemax','MaxVal:', $
                              set_value=pcwg.usemax(i).val,/nonexclusive)
  pcwg.maxval(0).id=cw_field(sub,/all_events,/floating,xsize=8,title=' ', $
                             uvalue='maxval',value=pcwg.maxval(i).val)
  applyto=['All','Only AZI']
  lab=widget_label(llr,value='Apply to:')
  pcwg.applycond(0).id=cw_bgroup(llr,uvalue='applycond',applyto,row=1, $
                                 set_value=pcwg.applycond(i).val,/exclusive)
  
  lab=widget_label(quv,/align_left,value='Plot only if peak-value for' + $
                   ' QUV is: (maximum within Image WL-interval)')  
  sublab=widget_base(quv,col=2)  
  sub=widget_base(sublab,row=1)  
  diswg.quv_val.id=cw_field(sub,/all_events,/floating,xsize=8,uvalue='quv_val',$
                            title='Min. value',value=diswg.quv_val.val)
  diswg.quv_max.id=cw_field(sub,/all_events,/floating,xsize=8,uvalue='quv_max',$
                            title='Max. value',value=diswg.quv_max.val)  
  sub=widget_base(sublab,row=1)  
  lab=widget_label(sub,value='Logic: (')
  qflag= cw_bgroup(sub,row=1,/nonexclusive,uvalue='q_flag', $
                   set_value=diswg.quv_flag.val(0), $
                   diswg.quv_flag.str(0),space=0)
  diswg.quv_and(0).id=widget_droplist(sub,uvalue='quv_and_qu', $
                                      value=['OR','AND'])
  widget_control,diswg.quv_and(0).id,set_droplist_select=diswg.quv_and(0).val
  uflag= cw_bgroup(sub,row=1,/nonexclusive,uvalue='u_flag', $
                   set_value=diswg.quv_flag.val(1), $
                   diswg.quv_flag.str(1),space=0)
  lab=widget_label(sub,value=')')
  diswg.quv_and(1).id=widget_droplist(sub,uvalue='quv_and_v', $
                                      value=['OR','AND'])
  widget_control,diswg.quv_and(1).id,set_droplist_select=diswg.quv_and(1).val
  vflag= cw_bgroup(sub,row=1,/nonexclusive,uvalue='v_flag', $
                   set_value=diswg.quv_flag.val(2), $
                   diswg.quv_flag.str(2),space=0)
  applyto=['All','B','AZI','INC']
  sub=widget_base(sublab,col=1)  
  lab=widget_label(sub,value='Apply to:',/align_left)
  diswg.applyquv(0).id=cw_bgroup(sub,uvalue='applyquv',applyto,row=2, $
                                set_value=diswg.applyquv.val(0:3),/nonexclusive)
  
  
  azisub=widget_base(diswg.base.id,col=1,/frame)
  sub=widget_base(azisub,row=1)
  diswg.azinel.id=cw_field(sub,/all_events,xsize=4,/integer, $
                           uvalue='azinel', $
                           title='AZI-vec along x:')
  widget_control,diswg.azinel.id,set_value=diswg.azinel.val
  diswg.azicol.id=widget_droplist(sub,uvalue='azicol',title='Col:', $
                                  value=['black','red','green','blue', $
                                         'magenta','cyan','orange', $
                                         'white','auto'])  
  widget_control,diswg.azicol.id, set_droplist_select=diswg.azicol.val
  diswg.azithick.id=widget_droplist(sub,uvalue='azithick', $
                                        title='thick:',$
                                        value=n2s(indgen(5)+1))
  widget_control,diswg.azithick.id, set_droplist_select=diswg.azithick.val
  diswg.azi_quavg.id=cw_bgroup(sub,uvalue='azi_quavg',row=1, $
                               set_value=diswg.azi_quavg.val,/nonexclusive, $
                               'QU weight avg')
  sub=widget_base(azisub,row=1)
  diswg.azilen.id=cw_field(sub,/all_events,xsize=6,/floating, $
                           uvalue='azilen', $
                           title='Length AZI-arrows:')
  widget_control,diswg.azilen.id,set_value=diswg.azilen.val
  diswg.aziuseb.id=cw_bgroup(sub,uvalue='aziuseb',row=1, $
                             set_value=diswg.aziuseb.val, $
                             /exclusive,['B*sin(INC)','sin(INC)', $
                                         'unity length'])
  
  sub=widget_base(diswg.base.id,row=1)
  diswg.title.id=cw_field(sub,/all_events,xsize=25,/string, $
                          uvalue='title',title='Title:')
  diswg.subtitle.id=cw_field(sub,/all_events,xsize=25,/string, $
                             uvalue='subtitle',title='Sub-Title:')
  sub=widget_base(diswg.base.id,row=1)
  diswg.xrange(0).id=cw_field(sub,/all_events,xsize=8,/float, $
                              value=diswg.xrange(0).val, $
                              uvalue='xrange0',title='X-Range:')
  diswg.xrange(1).id=cw_field(sub,/all_events,xsize=8,/float, $
                              value=diswg.xrange(1).val, $
                              uvalue='xrange1',title='-')
  diswg.yrange(0).id=cw_field(sub,/all_events,xsize=8,/float, $
                              value=diswg.yrange(0).val, $
                              uvalue='yrange0',title='Y-Range:')
  diswg.yrange(1).id=cw_field(sub,/all_events,xsize=8,/float, $
                              value=diswg.yrange(1).val, $
                              uvalue='yrange1',title='-')
  sub=widget_base(diswg.base.id,row=1)
  diswg.charsize.id=cw_field(sub,/all_events,xsize=8,/float, $
                             value=diswg.charsize.val, $
                             uvalue='charsize',title='Charsize:')
  diswg.fix_margins(0).id=cw_field(sub,/all_events,xsize=8,/float, $
                                   value=diswg.fix_margins(0).val, $
                                   uvalue='fixmargins0',title='Margins Left:')
  diswg.fix_margins(1).id=cw_field(sub,/all_events,xsize=8,/float, $
                                   value=diswg.fix_margins(1).val, $
                                   uvalue='fixmargins1',title='Right:')
  
  sub=widget_base(diswg.base.id,row=1)
  diswg.info.id=cw_bgroup(sub,uvalue='info',row=1, $
                          set_value=diswg.info.val,/nonexclusive, $
                          'Display Info (title, subtitle, ...)')
  diswg.inputout.id=cw_bgroup(sub,uvalue='inputout',row=1, $
                             set_value=diswg.inputout.val,/nonexclusive, $
                             'Print ipt-file on last PS-page')
  diswg.pspages.id=cw_field(diswg.base.id,/all_events,xsize=2,/int, $
                            value=fix(diswg.pspages.val), $
                            uvalue='pspages', $
                            title='Plots per page for PS-Output:')
  
  
  
  diswg.control.id=cw_bgroup(diswg.base.id,['Done','Reset All'], $
                             uvalue='control',row=1, $
                             button_uvalue=['done','reset'])
  
  widget_control,diswg.base.id,/realize
  xmanager,'diswg',diswg.base.id,no_block=1
  diswg_selpar
end


pro display_settings,init=init
  common wgst,wgst
  common diswg,diswg
  common pcwg,pcwg
  
  if n_elements(diswg) ne 0 then begin
    widget_control,diswg.base.id,iconify=0,bad_id=bad_id
    if bad_id eq 0 then return
  endif
  
  dis_widget,init=init
  
end
