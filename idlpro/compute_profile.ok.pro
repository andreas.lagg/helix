function gaussfunc,a0=a0,shift=shift,width=width,wl=wl
  
  expo=(-(wl-shift)^2./(2.*width^2.))>(-30)
  return, a0 * exp(expo) / sqrt(2*!pi*width^2)

end

function voigt_prof,a0=a0,dopp=dopp,damp=damp,shift=shift,wl=wl,F=F
;function gaussfunc,a0=a0,shift=shift,width=width,wl=wl
  
  vvec=(wl-shift)/dopp

  pvoigt,damp,vvec,H,F
  
  return,H
  
end

;************************
;************************
;************************

function compute_profile,atm=atm,line=line,wl=wl,init=init,obs_par=obs_par, $
                         blend=blend,gen=gen, $
                         lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv,dols=dols,lspol=lspol,$
                         convval=convval,doconv=doconv, $
                         prefilterval=prefilterval,doprefilter=doprefilter, $
                         ret_wlidx=ret_wlidx,nret_wlidx=nret_wlidx, $
                         normff=normff,comp=comp

  common cp,prf,nl,na,use,nu,nblend
  common profile_shape,profile_shape
  common verbose,verbose
  @common_cppar
  
  addnum=1.
  
  if keyword_set(init) then begin
    prf=wl*0.
    nl=n_elements(line)
    na=n_elements(atm)
    nblend=n_elements(blend)
    use=bytarr(nl,na)  ;flag if a atmosphere should be used for a line
    for il=0,nl-1 do for ia=0,na-1 do begin
      use(il,ia)=max(id2s(atm(ia).use_line) eq id2s(line(il).id))
    endfor      
    nu=float(total(use))
    if voigt then profile_shape='voigt' else profile_shape='gauss'
    if n_elements(verbose) eq 0 then verbose=1
    if verbose eq 2 then print,'Profile shape: '+profile_shape
    if n_elements(iprof_only) eq 0 then iprof_only=0
  endif

;  profile={wl:wl,i:prf,q:prf,u:prf,v:prf,istray:prf}
  profile={wl:wl,i:float(wl)*0,q:float(wl)*0,u:float(wl)*0,v:float(wl)*0}
                                ;filling factor for stray
                                ;light / unmagnetized component
;  ff_stray=(1-total(atm.par.ff))
  ff_stray=0.
  
  if n_elements(comp) eq 1 then begin
    ia1=comp & ia2=comp
  endif else begin
    ia1=0 & ia2=na-1
    comp=-1
  endelse
  
  if keyword_set(normff) eq 0 then FF=atm.par.ff $
  else FF=(norm_ff(atm.par.ff,atm.fit.ff,atm.linid))
  for ia=ia1,ia2 do $          ;normal component (no local straylight)
    if dols eq 0 or ia ne na-1 then begin
    
    sininc2=(sin(atm(ia).par.inc*!dtor))^2
    cosinc=cos(atm(ia).par.inc*!dtor)
    sin2azi=sin(atm(ia).par.azi*!dtor*2.)
    cos2azi=cos(atm(ia).par.azi*!dtor*2.)
    for il=0,nl-1 do begin
      
      if use(il,ia) then begin
                                ;compute profiles with hanle slab
                                ;model.  note that this routine
                                ;computes all 3 He lines at once, so
                                ;use_hanle_slab is only set to 1 for
                                ;the first line
        if modeval eq 5 then begin
          if line(il).use_hanle_slab eq 1 then begin
            hanle_module,keyword_set(init), $
              line(il),atm(ia).par.b,atm(ia).par.inc,atm(ia).par.azi,$
              atm(ia).par.vlos,atm(ia).par.dopp,atm(ia).par.damp, $
              atm(ia).par.dslab,atm(ia).par.height, $
              obs_par,wl,n_elements(wl), $
              hanle_i,hanle_q,hanle_u,hanle_v, $
              verbose
            profile.q = profile.q + hanle_Q ;Q/I_c
            profile.u = profile.u + hanle_U ;U/I_c
            profile.v = profile.v + hanle_V ;V/I_c
            if old_norm eq 1 then      profile.i=profile.i+hanle_I-FF(ia) $
            else if old_norm eq 0 then profile.i=profile.i+hanle_I-FF(ia)  $
            else if old_norm eq 2 then profile.i=profile.i+hanle_I 
          endif  
        endif else begin
          if modeval eq 1 then begin ;voigt-par mode
            dopp=atm(ia).par.dopp
            damp=atm(ia).par.damp
            etazero=atm(ia).par.etazero
          endif else if modeval eq 2 then begin ;voigt-phys mode
                                ;(see Balasubramaniam,
                                ;ApJ 382, 699-705 1991, original:
                                ;Landi Degl'Innocenti, A&AS 25,
                                ;379-390, 1976)
            atm(ia).par.dopp=line(il).wl/!c_light* $
              sqrt(2*1.3805e-23*atm(ia).par.tempe/(line(il).mass*!amu)+ $
                   atm(ia).par.vmici^2)
            atm(ia).par.damp=atm(ia).par.gdamp * $
              line(il).wl^2/(4*!pi*!c_light*atm(ia).par.dopp)
            atm(ia).par.etazero=atm(ia).par.densp* $
              line(il).wl^2/atm(ia).par.dopp* $
              1e-10*(1-exp(-1.43883e8/(line(il).wl*atm(ia).par.tempe)))
          endif else if modeval eq 3 or modeval eq 4 then begin ;voigt-gdamp mode
            atm(ia).par.damp=atm(ia).par.gdamp * $
              line(il).wl^2/(4*!pi*!c_light*atm(ia).par.dopp)
          endif
          
                                ;doppler shift (m/s)
          dopshift_wl = atm(ia).par.vlos * line(il).wl / !c_light
;    print,'dopshift_wl=',dopshift_wl
          
;zeeman shift (B in Gauss)
;*DAVID
          IF use_geff THEN BEGIN
            zeeman_shift = 4.6686411E-13 * line(il).wl^2.*line(il).geff * $
              atm(ia).par.b 
          ENDIF else $
            if use_pb and string(line(il).id(0:1)) eq 'He' then begin
                                ;use tabulated PB-effect
            zeeman_pb_split=get_pb_splitting(line(il),atm(ia).par.b,pb_method)
            zeeman_b=zeeman_pb_split(line(il).quan.n_sig+line(il).quan.n_pi: $
                                     line(il).quan.n_sig*2+line(il).quan.n_pi-1)
            zeeman_r=zeeman_pb_split(0:line(il).quan.n_sig-1)
            zeeman_p=zeeman_pb_split(line(il).quan.n_sig: $
                                     line(il).quan.n_sig+line(il).quan.n_pi-1)
            zeeman_pb_strength=get_pb_strength(line(il),atm(ia).par.b, $
                                               pb_method)/line(il).f
            web=zeeman_pb_strength(line(il).quan.n_sig+line(il).quan.n_pi: $
                                   line(il).quan.n_sig*2+line(il).quan.n_pi-1)
            wer=zeeman_pb_strength(0:line(il).quan.n_sig-1)
            wep=zeeman_pb_strength(line(il).quan.n_sig: $
                                   line(il).quan.n_sig+line(il).quan.n_pi-1)
          endif ELSE BEGIN
            zeeman_B=atm(ia).par.b*line(il).wl^2.*4.6686411e-13*line(il).quan.NUB
            zeeman_P=atm(ia).par.b*line(il).wl^2.*4.6686411e-13*line(il).quan.NUP
            zeeman_R=atm(ia).par.b*line(il).wl^2.*4.6686411e-13*line(il).quan.NUR
            wep=line(il).quan.WEP
            wer=line(il).quan.WER
            web=line(il).quan.WEB
          ENDELSE
;END DAVID
;    print,'dop/zeeman_shift=',dopshift_wl,zeeman_shift, line(il).geff 
                                ;compute unshifted profile, normalized
                                ;to icont
          
          if iprof_only eq 0 then begin ;flag to determine if only fit to
                                ;i-profile for straypol_run is to be done
            
            if profile_shape eq 'voigt' then begin                   
;*DAVID
              IF use_geff THEN BEGIN
;END DAVID                      
                i_center= $
                  0.5*voigt_prof(dopp=atm(ia).par.dopp, $
                                 shift=line(il).wl + dopshift_wl + $
                                 line(il).par(0).wlshift, $
                                 damp=atm(ia).par.damp, $
                                 wl=wl,F=F)* $
                  atm(ia).par.etazero*line(il).par.strength
                if magopt then  $
                  rho_p=0.5*F*atm(ia).par.etazero*line(il).par.strength $
                else rho_p=0.
;*DAVID
              ENDIF ELSE BEGIN
                                ;DEFINITIONS
                i_center=0. & rho_p=0.
                FOR II=0,line(il).quan.N_PI-1 DO BEGIN
                  i_center= i_center+$
                    0.5*voigt_prof(dopp=atm(ia).par.dopp, $
                                   shift=line(il).wl+dopshift_wl-zeeman_P(II) + $
                                   line(il).par.wlshift, $
                                   damp=atm(ia).par.damp, $
                                   wl=wl,F=F) * $
                    atm(ia).par.etazero*wep(ii)*line(il).par.strength
                  if magopt then $
                    rho_p=rho_p+0.5*F*atm(ia).par.etazero*wep(II)* $
                    line(il).par.strength
                ENDFOR
              ENDELSE
;END DAVID
            endif else begin
              i_center=0.5*(gaussfunc(a0=atm(ia).par.a0*line(il).par.strength, $
                                      shift=line(il).wl + dopshift_wl + $
                                      line(il).par.wlshift, $
                                      width=atm(ia).par.width, $
                                      wl=wl))
              rho_p=0.
            endelse
            
                                ;calculate magnetic profiles only if b
                                ;is to be fitted
            if atm(ia).fit.b eq 1 or atm(ia).par.b ne 0 then begin
              
                                ;compute shifted profiles (+-), normalized
                                ;to icont
              if profile_shape eq 'voigt' then begin
;*DAVID
                IF use_geff THEN BEGIN
;END DAVID
                  
                  i_minus= $
                    0.5*voigt_prof(dopp=atm(ia).par.dopp, $
                                   shift=line(il).wl+dopshift_wl-zeeman_shift + $
                                   line(il).par.wlshift,$
                                   damp=atm(ia).par.damp, $
                                   wl=wl,F=F)*atm(ia).par.etazero $
                    *line(il).par.strength
                  if magopt then $
                    rho_minus=0.5*F*atm(ia).par.etazero*line(il).par.strength $
                  else rho_minus=0.
                  i_plus= $
                    0.5*voigt_prof(dopp=atm(ia).par.dopp, $
                                   shift=line(il).wl+dopshift_wl+zeeman_shift + $
                                   line(il).par.wlshift,$
                                   damp=atm(ia).par.damp, $
                                   wl=wl,F=F)*atm(ia).par.etazero $
                    *line(il).par.strength
                  if magopt then $
                    rho_plus=0.5*F*atm(ia).par.etazero*line(il).par.strength $
                  else rho_plus=0.
;*DAVID
                ENDIF ELSE BEGIN
                                ;DEFINITIONS
                  i_minus=0. & i_plus=0.
                  rho_minus=0. & rho_plus=0.
                  FOR II=0,line(il).quan.N_SIG-1 DO BEGIN
                    i_minus= i_minus+$
                      0.5*voigt_prof(dopp=atm(ia).par.dopp, $
                                     shift=line(il).wl+dopshift_wl-zeeman_R(II)+$
                                     line(il).par.wlshift,$
                                     damp=atm(ia).par.damp, $
                                     wl=wl,F=F)* $
                      atm(ia).par.etazero*line(il).par.strength*wer(II)
                    if magopt then rho_minus=rho_minus+$
                      0.5*F*atm(ia).par.etazero*line(il).par.strength*wer(II)

                    i_plus= i_plus+$
                      0.5*voigt_prof(dopp=atm(ia).par.dopp, $
                                     shift=line(il).wl+dopshift_wl-zeeman_B(II)+$
                                     line(il).par.wlshift,$
                                     damp=atm(ia).par.damp, $
                                     wl=wl,F=F) $
                      *atm(ia).par.etazero*line(il).par.strength*web(ii)
                    if magopt then rho_plus=rho_plus+$
                      0.5*F*atm(ia).par.etazero*line(il).par.strength*web(II)

                  ENDFOR
                ENDELSE
;END DAVID
                
              endif else begin
                i_minus= $
                  0.5*(gaussfunc(a0=atm(ia).par.a0*line(il).par.strength, $
                                 shift=line(il).wl+dopshift_wl - zeeman_shift+$
                                 line(il).par.wlshift, $
                                 width=atm(ia).par.width, $
                                 wl=wl))
                rho_minus=0.
                i_plus= $
                  0.5*(gaussfunc(a0=atm(ia).par.a0*line(il).par.strength, $
                                 shift=line(il).wl+dopshift_wl + zeeman_shift+$
                                 line(il).par.wlshift, $
                                 width=atm(ia).par.width, $
                                 wl=wl))
                rho_plus=0.
              endelse

                                ;compute v-profile: Sum of to
                                ;gaussians with opposite sign, shifted
                                ;by Zeeman splitting value
                                ;see ronan et al, 1987 SoPhys vol 113, p353
              eta_v = line(il).f*1./2.*(i_plus - i_minus) * cosinc
              rho_v=  line(il).f*1./2.*(rho_plus - rho_minus) * cosinc
              
                                ;compute q-profile: +Q direction
                                ;defines azimuth angle zero.
                                ;see ronan et al, 1987 SoPhys vol 113, p353
              quprof = line(il).f*1./2.*( i_center - 1./2.*(i_plus + i_minus) )
              rho_qu = line(il).f*1./2.*( rho_p - 1./2.*(rho_plus + rho_minus) )

              
              eta_q= quprof * sininc2 * cos2azi      
              rho_q= rho_qu * sininc2 * cos2azi
                                ;u-profile is q profile x tan(2*azi)
                                ;see auer et al., 1977 SoPhys, vol 55,
                                ;p47
              eta_u= quprof * sininc2 * sin2azi
              rho_u= rho_qu * sininc2 * sin2azi
              
              
            endif else begin ;sigma components are the same as central pi
                                ;component because of zero lande
                                ;shift.
              eta_q=0 & eta_u=0 & eta_v=0
              i_plus=i_center
              i_minus=i_center
              rho_q=0 & rho_u=0 & rho_v=0
              rho_plus=rho_p
              rho_minus=rho_p
            endelse
            
            
            eta_i=line(il).f*1./2.*(i_center * sininc2 + $
                                    1./2. * (i_minus + i_plus) * $
                                    (1+(cosinc)^2))
            
;     endif else begin            ;non-magnetic case
;       i_pol=0 & qprof=0 & uprof=0 & vprof=0
;       i_stray = i_center
;     endelse
            
            
            
                                ;solution of balasubranamian, ApJ
                                ;1991, no rho for magneto-optical
                                ;effects, same as jefferies, ApJ 1989, 343
;     DELTA=(1 + eta_i)^2 * ( (1 + eta_i)^2 - eta_q^2 - eta_u^2 - eta_v^2 )    
;     I =1 + F/DELTA*( (1+eta_i) * (1+eta_i)^2 )
;     Q =-F/DELTA*( (1+eta_i)^2 * eta_q )
;     U =-F/DELTA*( (1+eta_i)^2 * eta_u )
;     V =-F/DELTA*( (1+eta_i)^2 * eta_v )
            
;     DELTA=( (1d + eta_i)^2 - eta_q^2 - eta_u^2 - eta_v^2 ) ;simpler
;     I =+F/DELTA*( (1+eta_i) )
;     Q =-F/DELTA*( eta_q )
;     U =-F/DELTA*( eta_u )
;     V =-F/DELTA*( eta_v )
            
;          F=atm(ia).par.ff
            
            R=(eta_q*rho_q + eta_u*rho_u +eta_v*rho_v)
            DELTA=( (1 + eta_i)^2 * $
                    ( (1 + eta_i)^2 - eta_q^2 - eta_u^2 - eta_v^2 + $
                      rho_q^2 + rho_u^2 + rho_v^2 ) - R^2)
            
                                ;multiply by gradient of source function
                                ;source function at tau=0: B0
            F2DELTA=atm(ia).par.sgrad * FF(ia) /DELTA

            if modeval eq 4 then begin
;              B0=atm(ia).par.szero 
;            B0=(-1+atm(ia).par.szero- atm(ia).par.sgrad)/total(use(*,ia))
;              B0=(1+atm(ia).par.szero)/(total(use(*,ia)))-atm(ia).par.sgrad
              szero=atm(ia).par.szero
            endif else szero=1. - atm(ia).par.sgrad
            B0=szero            ;B0 must match continuum level
                                ;outside line

;        B0=line(il).f - atm(ia).par.sgrad ;B0 must match continuum
;                        level outside line
            I =B0*FF(ia) +  $
              F2DELTA*( (1+eta_i) * ((1+eta_i)^2 +rho_q^2+rho_u^2+rho_v^2) )
            Q =-F2DELTA*( (1+eta_i)^2 * eta_q +  $
                          (1+eta_i)*(eta_v*rho_u-eta_u*rho_v) + rho_q*R )
            U =-F2DELTA*( (1+eta_i)^2 * eta_u + $
                          (1+eta_i)*(eta_q*rho_v-eta_v*rho_q) + rho_u*R )
            V =-F2DELTA*( (1+eta_i)^2 * eta_v + $
                          (1+eta_i)*(eta_u*rho_q-eta_q*rho_u) + rho_v*R)
          endif else begin
                                ;calculate only I profile with a
                                ;simple fit of a voigt
                                ;or gaussian, no rad. transfer...
                                ;This is used only for the
                                ;straypol_run. The idea is to fit the
                                ;I profile with a single voigt or
                                ;gauss to determine the shape of the
                                ;profile to be used for the correction
                                ;in Q and U
            if (line(il).straypol_use eq 1 and $
                atm(ia).straypol_use eq 1) then begin
              if profile_shape eq 'voigt' then begin
                gsub=voigt_prof(dopp=atm(ia).par.dopp, $
                                shift=line(il).wl + dopshift_wl, $
                                damp=atm(ia).par.damp, $
                                wl=wl,F=F) * $
                  atm(ia).par.etazero*line(il).par.strength
              endif else begin
                gsub=gaussfunc(a0=atm(ia).par.a0*line(il).par.strength, $
                               shift=line(il).wl + dopshift_wl, $
                               width=atm(ia).par.width, $
                               wl=wl)
              endelse
            endif else gsub=float(wl)*0.
            eta_i=line(il).f*gsub
            DELTA=( (1 + eta_i)^2 * (1 + eta_i)^2)
            F2DELTA=atm(ia).par.sgrad * FF(ia) /DELTA
            
            if modeval eq 4 then begin
;              B0=atm(ia).par.szero 
;            B0=(-1+atm(ia).par.szero- atm(ia).par.sgrad)/total(use(*,ia))
;              B0=(1+atm(ia).par.szero)/(total(use(*,ia)))-atm(ia).par.sgrad
              szero=atm(ia).par.szero
            endif else szero=1. - atm(ia).par.sgrad
            B0=szero            ;B0 must match continuum level
            
            I =B0*FF(ia) + F2DELTA*( (1+eta_i) * ((1+eta_i)^2))
            Q=0
            U=0
            V=0
          endelse
          dummy=check_math(mask=32) ;avoid message floating underflow
          
          scatter=0
          if scatter eq 1 then begin
                                ;apply rayleigh phase matrix
            hanle,atm(ia).par.b,atm(ia).par.inc,atm(ia).par.azi, $
              obs_par.posx,obs_par.posy, $
              obs_par.slit_orientation,obs_par.solar_radius,rpm
                                ;incident radiation: assumed to be
                                ;unpolarized
            sto_in=(1.-i)*line(il).straypol_par(ia).sign
;       I=I-rpm(0,0)*sto_in
            Q=Q+rpm(0,1)*sto_in
            U=U-rpm(0,2)*sto_in
;       V=V+rpm(0,3)*sto_in   ;(=0)
            

          endif
          
;         profile.q = profile.q + Q /nl ;Q/I_c
;         profile.u = profile.u + U /nl ;U/I_c
;         profile.v = profile.v + V /nl ;V/I_c
;         profile.i = profile.i + I*line(il).icont/nl ;I
          profile.q = profile.q + Q ;Q/I_c
          profile.u = profile.u + U ;U/I_c
          profile.v = profile.v + V ;V/I_c
                                ;store only absorption signature in
                                ;I. This absorption signature is
                                ;subtracted from 1 after the loops
                                ;over lines & atmospheres
;       profile.i = profile.i + I ; - FF(ia) ;I
          if old_norm eq 1 then profile.i=profile.i+I-FF(ia) $
          else if old_norm eq 0 then profile.i = profile.i + I  - FF(ia) $
          else if old_norm eq 2 then profile.i = profile.i + I 
        endelse                 ;end for modeval 0-4 (no hanle slab)  
      endif                     ;use line / comp

                                ;add straypolarization fit
      if modeval ne 5 then if (line(il).straypol_use eq 1 and $
                               atm(ia).straypol_use eq 1) then $
        if line(il).straypol_par(ia).width gt 1e-5 or $
        line(il).straypol_par(ia).dopp gt 1e-5 or $
        line(il).straypol_par(ia).damp gt 1e-5 then begin
        
        
        if profile_shape eq 'voigt' then begin
          if modeval eq 1 then $
            straydamp=line(il).straypol_par(ia).damp $
          else if modeval eq 2 or modeval eq 3 or modeval eq 4 then $
            straydamp=line(il).straypol_par(ia).damp* $
            line(il).wl^2/(4*!pi*!c_light*line(il).straypol_par(ia).dopp)
          gsub=voigt_prof(dopp=line(il).straypol_par(ia).dopp, $
                          shift= $
                          line(il).wl*(1.+line(il).straypol_par(ia).vlos/ $
                                       !c_light), $
                          damp=straydamp, $
                          wl=wl,F=F)*FF(ia)*line(il).par.straypol_eta0
        endif else begin
          gsub=gaussfunc(a0=line(il).par.straypol_amp*FF(ia), $
                         shift= $
                         line(il).wl*(1.+line(il).straypol_par(ia).vlos/ $
                                      !c_light), $
                         width=line(il).straypol_par(ia).width, $
                         wl=wl)
        endelse
;        gsub=gsub*line(il).f,
        if line(il).straypol_par(ia).sign eq -1 then gsub=-gsub
                                ;correction along B (Hanle)
        qsub=+gsub*cos((atm(ia).par.azi*2+hanle_azi)*!dtor)
        usub=+gsub*sin((atm(ia).par.azi*2+hanle_azi)*!dtor)
        profile.q=profile.q+qsub
        profile.u=profile.u+usub        
      endif
    endfor                      ;loop for line
  endif                         ;loop for component
  
;   maxid=max(atm.linid)
;   profile.i=profile.i/maxid
;   profile.q=profile.q/maxid
;   profile.u=profile.u/maxid
;   profile.v=profile.v/maxid
  
;  plot,profile.wl,i_center & oplot,color=1,profile.wl,i_plus & oplot,color=2,profile.wl,i_minus
;  plot_profiles,profile & print_atm,atm
  
                                ;apply prefilter
  if doprefilter ne 0 then begin
    profile.i=profile.i*prefilterval
    profile.q=profile.q*prefilterval
    profile.u=profile.u*prefilterval
    profile.v=profile.v*prefilterval
  endif
                                ;do convolution  
  if doconv ne 0  then begin
;     cfft=float(fft(convval,-1))
    cfft=(fft(convval,-1))    
    profile.i=fft(cfft*fft(profile.i,-1),1)    
    profile.q=fft(cfft*fft(profile.q,-1),1)
    profile.u=fft(cfft*fft(profile.u,-1),1)
    profile.v=fft(cfft*fft(profile.v,-1),1)
    if nret_wlidx gt 0 then begin
      rwl=ret_wlidx(0:nret_wlidx-1)
      profile={wl:profile.wl(rwl), $
               i:profile.i(rwl),q:profile.q(rwl), $
               u:profile.u(rwl),v:profile.v(rwl)}
    endif
  endif
  
  if (dols eq 1) then begin ;add localstraylight component after convolution
    if comp eq -1 or comp eq na-1 then begin
      nwl=n_elements(profile.i)
                                ;add local straylight component 
      profile.i=profile.i+ff(na-1)*(lsi(0:nwl-1)-1.)    
      if lspol ne 0 then begin
        profile.q=profile.q+ff(na-1)*(lsq(0:nwl-1))    
        profile.u=profile.u+ff(na-1)*(lsu(0:nwl-1))    
        profile.v=profile.v+ff(na-1)*(lsv(0:nwl-1))    
      endif
    endif 
  endif

;all other things should be done after local straylight correction
  
  
                                ;use old normalization (changes SGRAD
                                ;when changing number of lines!)
  if old_norm eq 1 then begin
    profile.i=(1.+profile.i)*line(0).icont
    profile.i=(profile.i-line(0).icont)/nl+line(0).icont
    profile.q=profile.q/nl
    profile.u=profile.u/nl
    profile.v=profile.v/nl
  endif else if old_norm eq 0 then begin
    profile.i=(1.+profile.i)*line(0).icont
  endif else if old_norm eq 2 then begin
    profile.i=(profile.i)*line(0).icont/nl
    profile.q=profile.q/nl
    profile.u=profile.u/nl
    profile.v=profile.v/nl
  endif
  
  
  if norm_stokes_val eq 1 then begin ;normalization to I instead of IC
    profile.q=profile.q/(profile.i/line(0).icont)
    profile.u=profile.u/(profile.i/line(0).icont)
    profile.v=profile.v/(profile.i/line(0).icont)
  endif

                                ;straylight correction
                                ;(non polarized, non-dispersive)
  profile.i=profile.i*(1.-gen.par.straylight)+gen.par.straylight
  
                                ;fit continuum level + straylight
  profile.i=profile.i*gen.par.ccorr
  profile.q=profile.q*gen.par.ccorr ;*(1.-gen.par.straylight)
  profile.u=profile.u*gen.par.ccorr ;*(1.-gen.par.straylight)
  profile.v=profile.v*gen.par.ccorr ;*(1.-gen.par.straylight)
  
                                ;telluric blends
  for ib=0,nblend-1 do begin
    if blend(ib).par.a0 ne 0 then begin
      hprof=voigt_prof(dopp=blend(ib).par.width,damp=blend(ib).par.damp, $
                       shift=blend(ib).par.wl,wl=profile.wl)
      profile.i=profile.i*(1-blend(ib).par.a0*hprof)
      profile.q=profile.q*(1-blend(ib).par.a0*hprof)
      profile.u=profile.u*(1-blend(ib).par.a0*hprof)
      profile.v=profile.v*(1-blend(ib).par.a0*hprof)
    endif
  endfor
  
  return,profile
end
