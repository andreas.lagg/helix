
pro lf_auto
    
    file='lf_auto'
    psset,ps=0,file=file+'.ps',/no_x,size=[18,20]
    
    
    val=[10.,50.,100,500]
    
    for iv=0,n_elements(val)-1 do begin
      
      ipt=read_ipt('test_pb.ipt')      
      ipt.atm.par.b=val(iv)
      ipt.use_geff=0
      ipt.use_pb=1
      helix,fitprof=synthprof,struct_ipt=ipt,result=result
      
      ipt.dir.profile='./sav/'
      ipt.observation='test_pb.x000.y000.fit.sav'
      ipt.ncalls(0)=300
      ipt.synth=0
      ipt.use_geff=1
      ipt.use_pb=0
      
      helix,fitprof=fitprof,struct_ipt=ipt,result=result
      
      help,/st,ipt.atm.par
      help,/st,result.fit.atm.par
      
;     plot_profiles,fitprof,synthprof,title='B', $
;       lthick=[2,2],color=[3,1],lstyle=[1,0], $
;       fraction=[1,.4,.4,.4]
stop        
    endfor

  
  
    psset,/close
  stop
end
