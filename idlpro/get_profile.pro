
function get_profile,profile=profile,ix=ix,iy=iy,icont=icont, $
                     wl_range=wl_range
  
  n=0 & icont=0.

  tnp=tag_names(profile)
  if max(tnp eq 'WLREF') then wlref=profile(0).wlref else wlref=0
  wl=profile(0).wl+wlref
  
  if max(tnp eq 'X') then begin
    px=profile.x
    py=profile.y
  endif else begin
    sz=size(profile.i)
    other=1
    if n_elements(ix) ne 0 and n_elements(iy) ne 0 then $
      if sz(0) eq 3 and sz(2) gt max(ix) and sz(3) gt max(iy) then begin
      px=indgen(sz(2)) & py=indgen(sz(3)) ;solar MHD files
      other=0
    endif
    if other then begin
      px=1 & py=1
      ix=0 & iy=0
    endif
  endelse
  
  if n_elements(wl_range) eq 2 then begin
    wlin=where(wl ge wl_range(0) and wl le wl_range(1)) 
  endif else wlin=indgen(n_elements(wl))  
  if wlin(0) eq -1 then begin
    print,'Found WL-range: ',wl_range
    message,/cont,'No data in this invalid wavelength range.'
    pret={i:double(profile.i)*0.+1.,q:double(profile.q)*0., $
          u:double(profile.u)*0.,v:double(profile.v)*0.,wl:profile.wl,ic:1.}
    return,pret
  endif
  
  wl=wl(wlin)
  i=fltarr(n_elements(wlin)) & q=i & u=i & v=i
  
  for ixp=0,n_elements(ix)-1 do for iyp=0,n_elements(iy)-1 do $
    if (ix(ixp) lt n_elements(px) and $
        iy(iyp) lt n_elements(py)) then  begin
    
    mx=max(profile.i(*,ix(ixp),iy(iyp)),/nan)
    if max(tnp eq 'IC') then begin
      if (size(profile.ic))(0) eq 0 then ic=profile.ic $
      else ic=(profile.ic(ix(ixp),iy(iyp)) )
                                ;important for TIP!
      if mx lt 5 and ic ge 5 then $
        profile.i(*,ix(ixp),iy(iyp))=profile.i(*,ix(ixp),iy(iyp))*ic
;stop      
    endif  else begin
                                ; assume that profile is already
                                ; normalized if max value is less than
                                ; 5
      ic=mx
 ;     if mx lt 5 then ic=1. else ic=mx
    endelse
    if mx gt 1e-30 then begin
      icont=icont+ic
      i=i + profile.i(wlin,ix(ixp),iy(iyp)) ;  * ic
      q=q + profile.q(wlin,ix(ixp),iy(iyp))
      u=u + profile.u(wlin,ix(ixp),iy(iyp))
      v=v + profile.v(wlin,ix(ixp),iy(iyp))        
      n=n+1
    endif
  endif
  
;  print,'use profile '+n2s(ix)+','+n2s(iy)
;  n=n>1
  if n ge 1 then icont=icont(0)/n else n=1
  pret={i:double(i)/n,v:double(v)/n,u:double(u)/n,q:double(q)/n, $
        wl:double(wl),ic:icont}
  return,pret
end
