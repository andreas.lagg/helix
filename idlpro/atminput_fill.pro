function atminput_fill,xv,yv
  common input,ipt
  common atminput,atminput
  
  retipt=ipt
  pnam=def_parname(tag_names(ipt.atm.par))
  for ia=0,ipt.ncomp-1 do begin
    for i=0,atminput[ia].npar-1 do begin
        if ((xv ge atminput[ia].nx) or (yv ge atminput[ia].ny)) then begin
          print,'ATMINPUT_FILL: Cannot find x=',xv,'y=',yv,' in ',ipt.atm[ia].atm_input
        endif else begin
          it=(where(strtrim(pnam.ipt) eq strtrim(atminput[ia].par[i])))[0]
          if it eq -1 then begin
            if ipt.verbose ge 1 then $
              message,/cont,'Could not find parameter from ATMINPUT: '+atminput[ia].par[i]
          endif else begin
            retipt.atm[ia].par.(it)=atminput[ia].data[i,xv,yv]
          endelse
        endelse
    endfor
  endfor
  
  return,retipt
  
end


