function check_hinmask,obs,verbose=verbose,files=files
  
  verbose=keyword_set(verbose)
                                ;obs must be directory
  fi=file_info(obs)
  if fi.directory eq 0 then return,0
  
  ffits=file_search(obs+'/*SP4*.fits',count=fcnt)
  if fcnt eq 0 then ffits=file_search(obs+'/*SP3*.fits',count=fcnt)
  if fcnt eq 0 then begin
    if verbose then message,/cont,'No Hinode files in '+obs
    return,0
  endif
  
  files=ffits
  return,1
  
end
