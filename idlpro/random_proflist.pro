pro random_proflist,file,xrange=xr,yrange=yr,n=n
  
  if n_elements(file) ne 1 or n_elements(xr) ne 2 or n_elements(yr) ne 2 or n_elements(n) ne 1 then begin
    print,'Create random profile list.'
    print,'Can be used in the input file with the PROFILE_LIST keyword.'
    print,'Usage:'
    print,'random_proflist,''random_list.dat'',xrange=[0,71],yrange=[0,71],n=500'
    reset
  endif
  
  openw,unit,file,/get_lun,error=err
  if err ne 0 then message,'Cannot open '+file
  
  xarr=lonarr(n)
  yarr=lonarr(n)
  i=0l
  repeat begin
    xarr(i)=(randomu(seed,/uniform)*(xr(1)-xr(0)+1)+xr(0))<xr(1)
    yarr(i)=(randomu(seed,/uniform)*(yr(1)-yr(0)+1)+yr(0))<yr(1)
    redo=0
    if i ge 1 then $ ;avoid double pixels
      if max((xarr(0:i-1) eq xarr(i))*(yarr(0:i-1) eq yarr(i))) then redo=1
    if redo eq 0 then begin
      printf,unit,xarr(i),yarr(i),format='(i4.4,3x,i4.4)'
      i=i+1
    endif
  endrep until i ge N  
  
  free_lun,unit
  print,'Wrote profile list to '+file
end
