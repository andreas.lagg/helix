function man_interpol,fin,xin,xout
  
  nin=n_elements(xin)
  nout=n_elements(xout)
  fout=make_array(nout,type=size(fin,/type))+!values.f_nan
                                ; manual interpoaltion routine (to be
                                ; consistent with the fortran version) 
  for i=0,nout-1 do begin
    idx=min(where(xin(0:nin-2) le xout(i) and xin(1:nin-1) ge xout(i)))
     if idx(0) eq -1 then begin
;       message,/cont,'This routine does not allow extrapolations.'
;       reset
     endif else begin
       fout(i)=(fin(idx+1)-fin(idx))/(xin(idx+1)-xin(idx))* $
         (xout(i)-xin(idx))+fin(idx)
     endelse
  endfor

  return,fout
end
