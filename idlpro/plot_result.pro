pro plot_result
  
  restore,'sav/pix_13may01.018_free_2x2_v01_x002y042.sav',/v
  obs1=observation
  fit1=fit
  wgt1=wgt
  
  restore,'sav/pix_13may01.018_free_2x2_v01_x014y042.sav'
  obs2=observation
  fit2=fit
  
  plot_profiles,obs1,obs2,fit1,fit2,title='Multiple Profiles', $
    lthick=[1,2,3,4],color=[3,1,2,4],lstyle=[0,2,3,4], $
    fraction=[1,.4,.4,.4],iquv='IQUV',weight=wgt1
end
