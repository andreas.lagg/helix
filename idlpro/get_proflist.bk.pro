function get_proflist,ipt,xcrd=xcrd,xcexact=xcexact, $
                      ycrd=ycrd,ycexact=ycexact,xcoff=xcoff,ycoff=ycoff, $
                      window=window
  
  if n_elements(window) ne 4 then $
    window=[ipt.x(0),ipt.y(0), $
            ipt.x(1),ipt.y(1)]
  
                                ;create this list from xpos / ypos parameter
                                ;in input-file
  x0=ipt.x(0)>window(0)
  x1=(ipt.x(1))<window(2)
  y0=ipt.y(0)>window(1)
  y1=(ipt.y(1))<window(3)
  
  xcexact=x0
  for ix=x0,x1 do begin
    get_profidx,x=ix,y=y0,stepx=ipt.stepx,stepy=ipt.stepy, $
      rgx=[x0,x1],rgy=[y0,y1], $
      vecx=xpvec,vecy=ypvec,average=ipt.average,scansize=ipt.scansize
    xcexact=[xcexact,xpvec(0)]
  endfor
  xcexact=remove_multi([xcexact,x1+1])
  xcar=xcexact(0:(n_elements(xcexact)-2)>0)
  if n_elements(xcexact) eq 2 then xcexact=[xcexact,max(xcexact)+1]
  xcrd=xcexact(0:(n_elements(xcexact)-2)>0)
  
  ycexact=y0
  for iy=y0,y1 do begin
    get_profidx,x=x0,y=iy,stepx=ipt.stepx,stepy=ipt.stepy, $
      rgx=[x0,x1],rgy=[y0,y1], $
      vecx=xpvec,vecy=ypvec,average=ipt.average,scansize=ipt.scansize
;    ycexact=[ycexact,ypvec(0)]
    ycexact=[ycexact,ypvec]
  endfor
  ycexact=remove_multi([ycexact,y1+1])
  ycar=ycexact(0:(n_elements(ycexact)-2)>0)
  if n_elements(ycexact) eq 2 then ycexact=[ycexact,max(ycexact)+1]
  ycrd=ycexact(0:(n_elements(ycexact)-2)>0)
;   xcexact=indgen((x1-x0+ipt.stepx)/ipt.stepx+1)*ipt.stepx+x0
;   ycexact=indgen((y1-y0+ipt.stepy)/ipt.stepy+1)*ipt.stepy+y0
;   xcrd=xcexact(0:n_elements(xcexact)-2)
;   ycrd=ycexact(0:n_elements(ycexact)-2)
;   if ipt.average eq 1 and ipt.scansize gt 0 then begin
;     xcrd=x0
;     repeat begin
;       nel=n_elements(xcrd)
;       xn=xcrd(nel-1) + ipt.stepx
;       if fix(xn)/ipt.scansize ne fix(xcrd(nel-1))/ipt.scansize then $
;         xn=(fix(xn)/ipt.scansize)*ipt.scansize
;       xcrd=[xcrd,xn]
;     endrep until xcrd(nel) gt x1
;     xcexact=xcrd
;     xcrd=xcrd(0:nel-1)
;   endif

  xarr=((intarr(n_elements(ycar))+1) # xcar)(*)
  yarr=(ycar # (intarr(n_elements(xcar))+1))(*)
  list=transpose([[xarr],[yarr]]) 
  
  xcoff=min(ipt.x)
  ycoff=min(ipt.y)
  

  return,list
  
end
