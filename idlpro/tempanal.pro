;program for temporal analysis of parameters
pro tempanal,reread=reread,ps=ps,init=init,ao=ao,disp=disp,nobs=nobs, $
             nx=nx,ny=ny,movie=movie,c_flag=c_flag,solar=solar,ambig=ambig
  common presult,pikres
  
  chsz=1.0
  if n_elements(movie) eq 0 then movie=0
  
  maxreps=100                   ;max. number of repetitons
  fmr=fltarr(maxreps)
  
                                ;define display structure
;--------------------------------------------------
  if n_elements(disp) eq 0 or keyword_set(init) then begin
    disp={landscape:0,  $       ;
          par:'VLOS', $         ;parameter
          szx:5., $            ;output size
          szy:25., $
          comp:1, $             ;component
          minff:0., maxff:1.,$  ;min and max value for FF
          scale:1., $           ;scale factor for units
          ztitle:'', $          ;
          smooth:3, $           ;smooth function
          zrg:fltarr(2), $      ;manual z-range
          mark_emission:0, $    ;flag for marking emission (sgrad negative)
          title:'test'} 
    disp.par=strupcase(disp.par)
  endif
  
  if keyword_set(solar) then soladd=' (solar)' else soladd=''
  disp.scale=1.
  case strupcase(disp.par) of
    'VLOS': begin
      disp.ztitle='LOS-velocity (km/s)'
      disp.scale=1000.
    end
    'B': disp.ztitle='Magnetic field [G]'
    'AZI': disp.ztitle='Azimuth'+soladd+' [�]'
    'INC': disp.ztitle='Inclination'+soladd+' [�]'
    'FITNESS': disp.ztitle='Fitness'
    else: disp.ztitle=disp.par
  endcase

                                ;define obs-structure
;--------------------------------------------------
  ost={sav:'', $                ;name of sav-file
       datafile:'', $           ;name of data file
       reps:1, $              ;# of steps for multpile repetition scan
       stepsize:0, $            ;stepsize for multi-rep observation
       wlpix:0d, $       ;Pixel of central WL of line (calibrate VLOS)
       dpix:0d, $               ;pixel width in Angstrom
       wl:0d, $                 ;WL of line in Angstrom 
       vlos_off:0d, $           ;VLOS offset calc. from wlpix
       mm_xpix:0.,  $           ;x-size of pixel in km
       mm_ypix:0.,  $           ;y-size of pixel in km
  shiftx:0,shifty:0, $   ;x/y shift with respect to first observation.
                                ;Obtain the shift values using the routine
                                ;'align_obs.pro' in the lagg_addons
                                ;directory
  x0:fmr,x1:fmr,y0:fmr,y1:fmr, $ ;definition of box edges
    tsec:dblarr(maxreps), $     ;time of observation in seconds
    szx:0,szy:0, $              ;x and y size of observation
    tstr:strarr(maxreps)}
  
;--------------------------------------------------
  if n_elements(nobs) eq 0 then nobs=3 ;define number of obs. files to compare
  
  if n_elements(ao) eq 0 or keyword_set(init) then begin
    ao=replicate(ost,nobs)
    if keyword_set(init) then return
    
                                ;define observations
    ;he
    ao.sav='./sav/'+['13may01.018_bfree_1x1_noff_v02.pikaia.sav', $
                     '13may01.014_bfree_1x1_noff_v02.pikaia.sav', $
                     '13may01.019_bfree_1x1_noff_v02.pikaia.sav']
    ;si
;     ao.sav='./sav/'+['13may01.018_2comp_si_v01.pikaia.sav', $
;                      '13may01.014_2comp_si_v01.pikaia.sav', $
;                      '13may01.019_2comp_si_v01.pikaia.sav']
    ao.datafile='./data/13may01.01'+['8','4','9']
    ao.shiftx=[0, 29,11]
    ao.shifty=[0,-10, 2]
    ao.stepsize=[10,0,20]
    
                                ;define box in first observation
    ao.x0=-5 & ao.x1=15
    ao.y0= 40 & ao.y1=60
  endif else nobs=n_elements(ao)
;--------------------------------------------------
                                ;restore observations
  inam='pikres'+n2s(indgen(nobs))
  if n_elements(pikres) eq 0 or keyword_set(reread) then begin
    for io=0,nobs-1 do begin
      print,'restoring '+ao(io).sav+' ...',format='(a,$)'
      restore,ao(io).sav
      dummy=execute(inam(io)+'=pikaia_result')
      print,'  -> '+inam(io)+',  Done.'
    endfor
    dummy=execute('pikres={'+add_comma(inam+':'+inam,sep=',')+'}')
  endif else begin
    dummy=execute(add_comma(inam+'=pikres.'+inam,sep=' & '))
  endelse
  
;--------------------------------------------------
                                ;calculate VLOS shift between
                                ;observations
  ao.vlos_off=0d                
  refidx=(where(ao.wl ne 0))(0)
  if refidx ne -1 then begin
    for i=0,nobs-1 do if i ne refidx then begin
      ao(i).vlos_off= $
        (-ao(i).wlpix+ao(refidx).wlpix)*ao(i).dpix/ao(refidx).wl*2.99792512d8
    endif
  endif
  print,'VLOS shift: ',ao.vlos_off
;--------------------------------------------------
  
                                ;set output device
  if keyword_set(movie) then begin
    disp.landscape=0
    disp.szx=21 & disp.szy=18
  endif else eps=0
  szxy=[disp.szx,disp.szy]
  if disp.landscape then szxy=reverse(szxy)
  if keyword_set(ps) then begin
    slpos=strpos(ao(0).sav,'/',/reverse_search)
    dtpos=strpos(ao(0).sav,'.',/reverse_search)
    if dtpos eq -1 then dtpos=strlen(ao(0).sav)
    if keyword_set(solar) then add='_sol'+n2s(fix(ambig),format='(i3.3)') $
      else add=''
    psf='./ps/ta_'+strmid(ao(0).sav,slpos+1,dtpos-slpos-1)+ $
      '_comp'+n2s(disp.comp)+ $
      '_'+strlowcase(disp.par)+add
;    if disp.landscape then szr=reverse(szxy) else $
      szr=szxy
    psout=ps_widget(default=[disp.landscape,0,0,0,8,0,0], $
                    size=szr,psname=psn, $
                    file=psf+'.ps',/no_x)
    if psout(1) eq 1 then reset
    !p.font=0
    device,set_font='helvetica',/tt_font
    !p.charsize=1.*chsz
    if movie then begin
      device,xoff=0,yoff=0
      !p.charsize=1.2*chsz
    endif
  endif else begin
    set_plot,'X'
    !p.font=-1
    !p.charsize=1.3*chsz
    screen=get_screen_size()
    xs=800 & ys=xs/szxy(0)*szxy(1)
    window,disp.comp-1,xpos=screen(0),ypos=screen(1),xsize=xs,ysize=ys
;      xsize=screen(1)*0.5/szxy(1)*szxy(0),ysize=screen(1)*0.5
;      xsize=screen(1)*1.0,ysize=screen(1)*1.0*szxy(1)/szxy(0)
  endelse
  userlct & erase & greek &  !p.multi=0
  
;--------------------------------------------------
                                ;calculate box sizes using stepsize
  for io=nobs-1,0,-1 do begin
    dummy=execute('pr='+inam(io))
    sz=size(pr.fit)
    ao(io).szx=sz(1)
    ao(io).szy=sz(2)
    
    if ao(io).stepsize gt 0 then begin
      ao(io).reps=1+sz(1)/ao(io).stepsize - ((sz(1) mod ao(io).stepsize) eq 0)
    endif else  ao(io).stepsize=sz(1)
    nr=ao(io).reps-1
    
    time=get_times(ao(io).datafile,tsec=tsec,error=error)
    for is=0,nr do begin
      i0=is*ao(io).stepsize
      i1=((is+1)*ao(io).stepsize)<(sz(1)-1)
      tmed=total(tsec(i0:i1))/(i1-i0+1)
      tmedstr='   '+strmid(conv_time(old='sec',new='date_dot',tmed),11,5)+'!C '
;      ao(io).tstr(is)=time(i0)+'!C - '+time(i1)
      ao(io).tstr(is)=tmedstr
      ao(io).tsec(is)=tsec(i0)
    endfor
  endfor
  nimg=fix(total(ao.reps))
  
  
                                ;get images
;--------------------------------------------------
  xs=ao(0).x1(0)-ao(0).x0(0)+1
  ys=ao(0).y1(0)-ao(0).y0(0)+1
  rst={img:fltarr(xs,ys)+!values.f_nan, $ ;store image
       cont:fltarr(xs,ys), $    ;c_flag array
       flag:bytarr(xs,ys), $    ;store some flags (eg. emission)
       xcrd:fltarr(xs+1),nxc:0, $ ;x coordinate values and # of xcrd elements
       ycrd:fltarr(ys+1),nyc:0, $ ;y coordinate values and # of ycrd elements
       zrg:fltarr(2), $
       tsec:0d, $               ;for sorting
       io:0,is:0 $           ;index for data set and repetition number
      }
  result=replicate(rst,nimg)
  
                                ;calculate nx, ny for mask
  if n_elements(nx) ne 0 and n_elements(ny) ne 0 and $
    keyword_set(movie) eq 0 then begin
    print,'using manual nx/ny placement'
  endif else begin
    ny=round(sqrt(float(xs)/ys*sqrt(2)*nimg))
    nx=nimg/ny + ((nimg mod ny) ne 0)
    if disp.landscape eq 0 then begin
      ny=round(sqrt(float(ys)/ys*sqrt(2)*nimg))
      nx=nimg/ny + ((nimg mod ny) ne 0)
;   dum=nx & nx=ny & ny=dum
    endif
  endelse
  print,'Images: '+n2s(nimg)+', nx:ny=',n2s(nx)+':'+n2s(ny)
  
  
  in=0 & io=0 & is=0
  for ix=0,nx-1 do for iy=0,ny-1 do begin
    if in lt nimg then begin
      dummy=execute('pr='+inam(io))
      if keyword_set(c_flag) then contarr=cont_flag(pr.fit)
      tn=tag_names(pr.fit.atm.par)
      pidx=(where(tn eq disp.par))(0)
      if strupcase(disp.par) eq 'FITNESS' then ff=1 else begin
        ff=0
        if pidx eq -1 then begin
          print,'available parameters are:'
          print,tn
          message,'Unknown parameter: '+disp.par
        endif
      endelse
      result(in).io=io
      result(in).is=is
      result(in).tsec=ao(io).tsec(is)
      if disp.comp-1 ge n_elements(pr.fit(0).atm) then $
        message,'No comp' +n2s(disp.comp)+' available.'
      
                                ;get image
      if ff eq 1 then timg=reform(pr.fit.fitness) $
      else timg=reform(pr.fit.atm(disp.comp-1).par.(pidx))
      if keyword_set(c_flag) then cimg=contarr
                                ;velocity offset
      if disp.par eq 'VLOS' then timg=timg+ao(io).vlos_off
                                ;scaling of parameters (eg m/s -> km/s)
      
      if keyword_set(solar) then $
      if disp.par eq 'AZI' or disp.par eq 'INC' then begin
        
        los2solar,btot=pr.fit.atm(disp.comp-1).par.b, $
          gamma=pr.fit.atm(disp.comp-1).par.inc, $
          chi=pr.fit.atm(disp.comp-1).par.azi, $
          ambig=(['0','180'])(ambig),smooth=0, $
          obs=ao(io).datafile+'cc',bx_sol=bxs,by_sol=bys,bz_sol=bzs
          
        if disp.par eq 'INC' then begin
            timg= acos(bzs/pr.fit.atm(disp.comp-1).par.b)/!dtor
            ib0=where(abs(pr.fit.atm(disp.comp-1).par.b) le 1e-5)
            if ib0(0) ne -1 then timg(ib0)=0.
          endif
            
          if disp.par eq 'AZI' then $
            timg= atan(bys,bxs)/!dtor
      endif
      timg=timg/disp.scale
        
                                ;use minimum of sgrad for emission
                                ;flag (neg. sgrad = emission)
      sgrad=reform(min(pr.fit.atm.par.sgrad,dim=1))
      ff=reform(get_ff(reform(pr.fit.atm.par.sgrad),disp.comp-1))
      szt=size(timg)

                                ;use only filled image values
      xgood=where(total(abs(timg),2) gt 1e-5)
      ygood=where(total(abs(timg),1) gt 1e-5)
      if n_elements(xgood) gt 1 and n_elements(ygood) gt 1 then begin
                                ;smoothing data
        timg=timg((ygood*0+1) ## xgood , ygood ## (xgood*0+1))
        if keyword_set(c_flag) then $
          cimg=cimg((ygood*0+1) ## xgood , ygood ## (xgood*0+1))
        ff  =  ff((ygood*0+1) ## xgood , ygood ## (xgood*0+1))
        sgrad=sgrad((ygood*0+1) ## xgood , ygood ## (xgood*0+1))
        if disp.smooth gt 2 then begin
        case strlowcase(disp.par) of
          'azi': if keyword_set(solar) then period=[-180,180] $
          else period=[-90,90]
          'inc': period=[0,180]
          else: if n_elements(period) ne 0 then dummy=temporary(period)
        endcase
          timg=smooth_periodic(timg,disp.smooth,period=period,/edge)
          if keyword_set(c_flag) then $
            cimg=smooth(cimg,disp.smooth,/edge)
          ff=smooth(ff,disp.smooth,/edge)
          sgrad=smooth(sgrad,disp.smooth,/edge)
        endif
        xcrd=pr.fit(xgood,0).x - ao(io).shiftx - is*ao(io).stepsize
        ycrd=pr.fit(0,ygood).y - ao(io).shifty        
        x0=ao(io).x0(is)        ; - is*ao(io).stepsize
        x1=ao(io).x1(is)        ; - is*ao(io).stepsize
        inx=where(xcrd ge x0 and xcrd le x1 and $
                  (pr.fit(xgood,0).x-pr.input.x(0)) ge is*ao(io).stepsize and $
                  (pr.fit(xgood,0).x-pr.input.x(0)) lt (is+1)*ao(io).stepsize)
        iny=where(ycrd ge ao(io).y0(is) and ycrd le ao(io).y1(is))
        if n_elements(inx) gt 1 and n_elements(iny) gt 1 then begin
          xarr=(ygood(iny)*0+1) ## xgood(inx)
          yarr=ygood(iny) ## (xgood(inx)*0+1)
          xarr=((iny)*0+1) ## (inx)
          yarr=(iny) ## ((inx)*0+1)
          timg1=timg(xarr,yarr)
          if keyword_set(c_flag) then cimg1=cimg(xarr,yarr)
          ffimg1=ff(xarr,yarr)
          sgradimg1=sgrad(xarr,yarr)
          loff=where(ffimg1 lt disp.minff)
          if loff(0) ne -1 then timg1(loff)=!values.f_nan
          hiff=where(ffimg1 gt disp.maxff)
          if hiff(0) ne -1 then timg1(hiff)=!values.f_nan
          result(in).xcrd=[xcrd(inx), $
                           xcrd(inx(n_elements(inx)-1))+xcrd(1)-xcrd(0)]
          result(in).ycrd=[ycrd(iny), $
                           ycrd(iny(n_elements(iny)-1))+ycrd(1)-ycrd(0)]
          result(in).nxc=n_elements(inx)+1
          result(in).nyc=n_elements(iny)+1
          result(in).img(0:result(in).nxc-2,0:result(in).nyc-2)=timg1
          if keyword_set(c_flag) then $
            result(in).cont(0:result(in).nxc-2,0:result(in).nyc-2)=cimg1
          
          result(in).zrg=[min(timg1,/nan),max(timg1,/nan)]
          if disp.mark_emission then begin
            emiidx=where(sgradimg1 lt 0.0) ;emission threshold
            if emiidx(0) ne -1 then begin
              flag=result(in).flag(0:result(in).nxc-2,0:result(in).nyc-2)
              flag(emiidx)=14b   ;color for emission flag
              result(in).flag(0:result(in).nxc-2,0:result(in).nyc-2)=flag
            endif
          endif
        endif
      endif
      is=is+1
      if is ge ao(io).reps then begin
        is=0 & io=io+1
      endif
      in=in+1      
    endif
  endfor
  
                                ;sort afterr tsec
  srt=sort(result(0:nimg-1).tsec)
  result=result(srt)
  
  if keyword_set(movie) then begin
    nx=1 & ny=1
    movie=1
    nimg=10 
    print,'Do not display image #11'
  endif else movie=0
  
  in=0 & last=0
  repeat begin
  
                                ;define output mask
  tit=add_comma(sep=', ',/no_empty, $
                [disp.title,disp.par+', Comp. '+n2s(disp.comp)])
  zrg=[min(result(0:nimg-1).zrg),max(result(0:nimg-1).zrg)]
  if total(abs(disp.zrg)) gt 1e-5 then zrg=disp.zrg
  sizeok=0
  fm=[0.10,0.86]
  ntry=0
  odev=!d.name
  xydev_orig=[!d.x_size,!d.y_size]
  zbx=1000.
  xydev=zbx*float(xydev_orig)/xydev_orig(0)
  set_plot,'Z' & device,set_resolution=xydev
  sizeyok=0
  maxy=0. & oldmaxy=-1.
  sizexok=0
  maxx=0. & oldmaxx=-1.
  screen=get_screen_size()
  repeat begin
    pos=0
    if sizeyok*sizexok then begin
      xydev=finalsize      
      set_plot,odev
      xydev=xydev*xydev_orig(0)/zbx      
      case !d.name of
        'X': begin
          screen=get_screen_size()
          window,disp.comp-1,xpos=screen(0),ypos=screen(1), $
            xsize=xydev(0),ysize=xydev(1)
        end
        'PS': device,xsize=xydev(0)/1000.,ysize=xydev(1)/1000.
        else:
      endcase
      userlct & erase
    endif
    
    hdt=strarr(nx)
    hdt(0)=tit
    msk=mask([nx,ny],headtitle=hdt, $ ;subtitle=subt,headtitle=hdt, $
             xformat='(i3)',yformat='(i3)',zformat='(f15.2)', $
             xrange=[ao(0).x0(0),ao(0).x1(0)]*ao(0).mm_xpix, $
             yrange=[ao(0).y0(0),ao(0).y1(0)]*ao(0).mm_xpix, $
             fix_margins=fm, $
             zrange=zrg, $
;             label=1, $
             xtitle='!Cx [Mm]',ytitle='y [Mm]', $
             ztitle=disp.ztitle,isotropic=1, $
             charsize=!p.charsize*1.5,pos=pos)
    
    ratioy=(pos(3)-pos(1))/1.
    ratiox=(pos(2)-pos(0))/(fm(1)-fm(0))
;    print,ratiox,maxx,oldmaxx,ratioy,maxy,oldmaxy,sizexok,sizeyok,format='(6f10.5,2i3)'
      oldmaxy=maxy
    if (ratioy gt maxy and ratioy gt oldmaxy) and !d.name eq 'Z' then begin
      maxy=maxy>ratioy
      newxy=[!d.x_size,!d.y_size] 
      finalsize=newxy
      newxy=[newxy(0),newxy(1)*1.05] 
      newxy=newxy*0.5*screen(0)/newxy(0)      
      device,set_resolution=newxy
      xydev=[!d.x_size,!d.y_size]
    endif else begin
      sizeyok=1
      newxy=finalsize
    endelse
    if sizeyok eq 1 then begin
      oldmaxx=maxx     
      if (ratiox gt maxx and ratiox gt oldmaxx) and !d.name eq 'Z' then begin
        maxx=maxx>ratiox
        newxy=[!d.x_size,!d.y_size] 
        finalsize=newxy
        newxy=[newxy(0)/1.05,newxy(1)] 
        newxy=newxy*0.5*screen(0)/newxy(0)      
        device,set_resolution=newxy
        xydev=[!d.x_size,!d.y_size]
      endif else sizexok=1
    endif
    ntry=ntry+1
  endrep until sizeyok*sizexok and !d.name ne 'Z'
  
  if movie then xyouts,0,0,/normal,charsize=!p.charsize*2, $
    'Frame #'+n2s(format='(i2.2)',(in+1)<nimg)
  
  
  if disp.par eq 'VLOS' then begin
    userlct,glltab=7,center=256.*(0.-zrg(0))/(zrg(1)-zrg(0)),/nologo
  endif else if disp.par eq 'AZI' then $
    userlct,/nologo,glltab=8 $
  else userlct,/nologo
  
                                ;set color 14 for emission flag
  tvlct,/get,r,g,b
  ec=14
  r(ec)=180 & g(ec)=180 & b(ec)=180
  tvlct,r,g,b
  
                                ;do plotting
  if movie eq 0 then in=0
  msk_plot,0,msk.label(0,0),/nocontour
  avgfit=0
  for iy=0,ny-1 do for ix=0,nx-1 do begin
    if in lt n_elements(result) then begin
      
      io=result(in).io
      is=result(in).is
;      if ix eq 0 then 
      
      if result(in).nxc gt 0 and result(in).nyc gt 0 then begin
        img=result(in).img(0:result(in).nxc-2,0:result(in).nyc-2)
        flag=result(in).flag(0:result(in).nxc-2,0:result(in).nyc-2)
        xx1=result(in).xcrd(0:result(in).nxc-1)*ao(0).mm_xpix
        yy1=result(in).ycrd(0:result(in).nyc-1)*ao(0).mm_ypix
        msk_plot,img,msk.plot(ix,iy), $
          xcoord=xx1,ycoord=yy1, $
          novalid=where(finite(img) eq 0), $
          flag=flag, $
;          additional_text=ao(io).tstr(is), $
          ex_ps_quality=1.5,exact=1,/nocontour,nv_color=15
        if keyword_set(c_flag) then begin
          xx2=result(in).xcrd(0:result(in).nxc-2)*ao(0).mm_xpix
          yy2=result(in).ycrd(0:result(in).nyc-2)*ao(0).mm_ypix
          ctimg=result(in).cont(0:result(in).nxc-2,0:result(in).nyc-2)
          ctimg=ctimg/(finite(img) eq 1)
          if disp.par eq 'AZI' then ccol=2 else ccol=4
          contour,xst=5,yst=5,thick=2+(!d.name eq 'PS'),color=ccol, $
            ctimg,xx2,yy2,/noerase, $
            pos=msk.plot(ix,iy).position,/device, $
            c_labels=0,levels=[0.45,0.55]
        endif
        if movie then xyouts,/normal,alignment=1,1,0.99, $
          '!CTime: '+strmid(ao(io).tstr(is),0,5)+'  ',charsize=!p.charsize*2 $
        else xyouts,/data,!x.crange(0)+0.1*(!x.crange(1)-!x.crange(0)), $
          !y.crange(0)+0.15*(!y.crange(1)-!y.crange(0)), $
          ao(io).tstr(is),charsize=1.
        if strupcase(disp.par) eq 'FITNESS' then begin
          avgfit=[avgfit,total(img,/nan)/float(total(finite(img)))]
        endif
      endif
    endif
    in=in+1
  endfor
  if strupcase(disp.par) eq 'FITNESS' then begin
    print,'Average Fitness: ',add_comma(n2s(avgfit(1:*)),sep=', ')
  endif
  
                                ;display last image three times
  if in eq nimg then begin
    in=in-1
    last=last+1
  endif
  
;  if movie then if in lt nimg then erase
  endrep until movie eq 0 or last eq 4
  
  
  if !d.name eq 'PS' then begin
    if movie eq 0 then xyouts,0,0,/normal,'!C!C'+psn
    device,/close
    print,'Created PS-file: '+psn
    if psout(3) eq 1 then  spawn,'gv '+psn+'&'
    set_plot,'X'
    !p.font=-1
    
    if movie then begin
      anifile='./ps/'+disp.par+'_anim.gif'
                                ;you can use convert + gifsicle
                                ;or convert from imagemagick 4.2.9
                                ;with gif-support
      spawn,'/opt/imagemagick-4.2.9/bin/convert -density 96x96 -delay 100 -loop 0 '+psn+' '+anifile
                                ;make it smaller
;      spawn,'gifsicle --colors=256 --batch -i '+anifile
      spawn,'animate '+anifile+'&'
      print,'created animated gif: '+anifile
    endif
  endif

end


;define some sample runs
;example: for st. petersburg:
;run1,par='VLOS',nx=6,ny=2,comp=2
pro run1,ps=ps,reread=reread,comp=comp,par=par,nx=nx,ny=ny,movie=movie, $
         zrange=zrange,minff=minff,smooth=smoothval,x=x,y=y,c_flag=c_flag, $
         solar=solar,ambig=ambig
                                ;return structures
  tempanal,/init,ao=ao,disp=disp,nobs=3
  
  if keyword_set(movie) then begin
    print,'Movie: Create a multi page PS-file and use imagemagick to' + $
      ' convert to animated gif.'
    ps=1
  endif
  
                                ;display settings
  if n_elements(par) eq 0 then begin
    disp.par='VLOS'
  endif else disp.par=strupcase(par)
  disp.landscape=1
  disp.minff=0.2
  if n_elements(minff) eq 1 then disp.minff=minff
  disp.title='FF>'+n2s(disp.minff,format='(f15.2)')
  if n_elements(smoothval) eq 0 then disp.smooth=0 else disp.smooth=smoothval
  if n_elements(comp) eq 0 then comp=1
  disp.comp=comp
    disp.mark_emission=1
  if comp eq 2 then begin
    case disp.par of
      'VLOS': disp.zrg=[12.,36]
      else:
    endcase
  endif else begin
;    disp.zrg=[-10,10]
  endelse
  if n_elements(zrange) eq 2 then disp.zrg=zrange
  
                                ;data set
  ao.sav='./sav/'+['13may01.014_bfree_1x1_noff_v02.pikaia.sav', $
                   '13may01.018_bfree_1x1_noff_v02.pikaia.sav', $
                   '13may01.019_bfree_1x1_noff_v02.pikaia.sav']
;   ao.sav='./sav/'+['13may01.014_2comp_pb_nostray_v01.pikaia.sav', $
;                    '13may01.018_2comp_pb_v01.pikaia.sav', $
;                    '13may01.019_2comp_pb_v01.pikaia.sav']
    ;si
;    ao.sav='./sav/'+['13may01.014_2comp_si_v01.pikaia.sav', $
;                     '13may01.018_2comp_si_v01.pikaia.sav', $
;                     '13may01.019_2comp_si_v01.pikaia.sav']
  
;  ao.sav='./sav/'+['13may01.014_bcouple_1x1_noff_v01.pikaia.sav', $
;                   '13may01.018_bcouple_1x1_noff_v01.pikaia.sav', $
;                   '13may01.019_bcouple_1x1_noff_v01.pikaia.sav']
  ao.datafile='./data/13may01.01'+['4','8','9']
  
                                ;special for these datafiles
  ao.wl=10827.088d
  ao.wlpix=[ 62.1667d    ,   62.6371d , 62.5614d    ]
  ao.dpix =[  0.0308143d , 0.0315701d ,  0.0313745d ]
  ao.mm_xpix=700000./950.*0.36/1e3
  ao.mm_ypix=700000./950.*0.36/1e3
  
  ao.shiftx=[0, -29, -19]
  ao.shifty=[0,  10,  12]
  ao.stepsize=[0,10,20]
;ao=ao(2) & ao.shiftx=0 & ao.shifty=0
  
                                ;st petersburg
  ao.x0=-1 & ao.x1=11
  ao.y0= 33 & ao.y1=44
  
                                ;he-image + movie
  ao.x0= 27 & ao.x1=41
  ao.y0= 22 & ao.y1=38
  
  if n_elements(x) eq 2 then begin
    ao.x0=x(0) & ao.x1=x(1)
  endif
  if n_elements(y) eq 2 then begin
    ao.y0=y(0) & ao.y1=y(1)
  endif
  
                                ;silicon image
;   ao.sav='./sav/'+['13may01.014_2comp_si_v01.pikaia.sav', $
;                    '13may01.018_2comp_si_v01.pikaia.sav', $
;                    '13may01.019_2comp_si_v01.pikaia.sav']
;   ao.x0=-2 & ao.x1=18
;   ao.y0= 32 & ao.y1=48
;   ao.wl=0
  
  
  ;run tempanal
  tempanal,ao=ao,disp=disp,ps=ps,reread=reread,nx=nx,ny=ny,movie=movie, $
    c_flag=c_flag,solar=solar,ambig=ambig

end

pro df_paper,reread=reread,ps=ps
  
  sm=3    ;smooth images
  ps=keyword_set(ps) 
  cf=1    ;overplot contours
  solar=1 ;azi & inc in solar coordinates
  ambig=0 ;select 0 or 180
  
  if solar eq 0 then azirg=[-90,90] else azirg=[-180,180]
  incrg=[0,110]
  if solar eq 1 then if ambig eq 180 then incrg=[50,130]
  
  run1,nx=6,ny=2,par='INC',comp=1,ps=ps,zrange=incrg,smooth=sm, $
    reread=reread,c_flag=cf,solar=solar,ambig=ambig
  run1,nx=6,ny=2,par='INC',comp=2,ps=ps,zrange=incrg,smooth=sm, $
    c_flag=cf,solar=solar,ambig=ambig
   run1,nx=6,ny=2,par='AZI',comp=1,ps=ps,zrange=azirg,smooth=sm, $
     c_flag=cf,solar=solar,ambig=ambig
   run1,nx=6,ny=2,par='AZI',comp=2,ps=ps,zrange=azirg,smooth=sm, $
     c_flag=cf,solar=solar,ambig=ambig
   run1,nx=6,ny=2,par='B',comp=1,ps=ps,zrange=[0,1200],smooth=sm,c_flag=cf
   run1,nx=6,ny=2,par='B',comp=2,ps=ps,zrange=[0,1200],smooth=sm,c_flag=cf
   run1,nx=6,ny=2,par='VLOS',comp=1,ps=ps,smooth=sm,c_flag=cf
   run1,nx=6,ny=2,par='VLOS',comp=2,ps=ps,smooth=sm,c_flag=cf

  
end

pro fit
  
  run1,nx=6,ny=2,par='FITNESS',comp=2,minff=0.5,zrange=[1.,3.],/reread,/ps, $
    x=[35,41],y=[30,38]

end
