pro rundiff,ps=ps
  common pikaia
  
  
  vlos=pikaia_result.fit.atm.par.vlos
  sz=size(vlos)
  sx=pikaia_result.input.stepx
  sy=pikaia_result.input.stepy
  ix=transpose(indgen(sz(1)/sx)*sx ## (intarr(sz(2)/sy)+1))
  iy=transpose((intarr(sz(1)/sx)+1) ## indgen(sz(2)/sy)*sy)
  
  ut=pikaia_result.header.ut
  tsec=strmid(ut,0,2)*3600.+strmid(ut,3,2)*60.+strmid(ut,6,2)
  tdiff=mean(tsec(1:*)-tsec(0:n_elements(tsec)-2))
  pixinfo='1 pix = '+n2s(tdiff,format='(f15.2)')+'s'
  
  vlos=vlos(ix,iy)
  
  dist=4
  rd=vlos(dist:*,*)-vlos(0:sz(1)/sx-dist-1,*)
  rd=rd/1e3
  
  rgx=[0,440]
  rgy=[25,75]
  rd=rd(rgx(0):rgx(1),rgy(0):rgy(1))
  
  zrg=minmaxp(rd,perc=99)
  zrg=[-max(abs(zrg)),+max(abs(zrg))]
  
  psset,ps=ps,size=[26.,18.],file='./ps/rundiff.ps',/landscape,pdf=1
  
  userlct,/full,neutral=0,glltab=7,center=256.*(0.-zrg(0))/(zrg(1)-zrg(0))
  image_cont_al,rd,zrange=zrg,contour=0,ztitle='delta VLOS [km/s]', $
    xrange=rgx*sx*tdiff/60.,yrange=rgy*sy,xtitle='Time [min]', $
    ytitle='Slit Position [pix]', $
    title='Running difference '+n2s(dist*sx)+' pix ('+pixinfo+')'
  
  
  psset,/close
end
