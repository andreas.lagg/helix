pro hanle_test
  
  val=-1.
  I={i:val,q:val,m1:val,p1:val,m2:val,p2:val}
  I.i=2  
  
  W=.8
  mu=-0.1
  phi=findgen(360)*!dtor
  
  Itot=I.i+sqrt(W/8.)*(1-3*mu^2)*I.q+ $
    sqrt(3*W)/2.*mu*sqrt(1-mu^2)*(i.p1*cos(phi)+i.m1*sin(phi))+ $
    sqrt(3*W)/4.*(1-mu^2)*(i.p2*cos(2*phi)-i.m2*sin(2*phi))
  
  Qtot=sqrt(W/8.)*3*(1-mu^2)*i.q+ $
    sqrt(3*W)/2.*mu*sqrt(1-mu^2)*(i.p1*cos(phi)+i.m1*sin(phi))- $
    sqrt(3*W)/4.*(1-mu^2)*(i.p2*cos(2*phi)-i.m2*sin(2*phi))
  
  Utot=sqrt(3*W)/2.*sqrt(1-mu^2)*(i.m1*cos(phi)-i.p1*sin(phi))+ $
    sqrt(3*W)/2.*mu*(i.p2*cos(2*phi)+i.m2*sin(2*phi))
  
  
  QoI=Qtot/Itot
  UoI=Utot/Itot
  
  plot,UoI,QoI,/xst,/yst                  ;,xrange=[-4,4],yrange=[-4,4]
;  plot,atan(utot,qtot)/!dtor
  stop
end
