function def_blendst
  @common_maxpar
  
                                ;define parameter set for one
                                ;component
  mm={min:0.,max:0.}
  
  blendscl={wl:mm,width:mm,damp:mm,a0:mm}
  blendfit={wl:0,width:0,damp:0,a0:0}
  blendpar={wl:0d,width:0.,damp:0.,a0:0.,dummy:0.}
  
  blendst={fit:blendfit,idx:blendfit,par:blendpar, $
           scale:blendscl,npar:0l,dummy:0l}
  
  return,blendst
end
