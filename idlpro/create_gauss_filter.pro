;procedure to create a convolution file for Gauss filter
;example:
;create_gauss_filter,central_wl=5250.208d,wl_window=2.,nbin=512,fwhm=0.085
pro create_gauss_filter,central_wl=cwl,nbin=nbin,wl_window=wl_window,fwhm=fwhm
  
  wl=(dindgen(nbin)/(nbin-1)-0.5)*wl_window
  
  sigma=fwhm/(2*sqrt(2*alog(2.)))
  gs=exp(-(wl)^2/(2*(sigma)^2))
  
  wl=wl+cwl
  plot,wl,gs,/xst,/yst,xrange=[-.5,.5]*fwhm+cwl
  
  file='./convolve/gauss_'+n2s(cwl,format='(f10.3)')+'_fwhm'+ $
    n2s(fix(fwhm*1e3),format='(i3.3)')+'mA.dat'
  openw,/get_lun,unit,file,error=error
  if error ne 0 then message,'Unable to open '+file
  
  printf,unit,';convolution file'
  printf,unit,';Gaussian Filter with FWHM='+n2s(fwhm*1e3,format='(f10.1)')+ $
    ' mA, centered at '+n2s(cwl,format='(f10.3)')
  printf,unit,';WL(A)            Intensity '

  for i=0,nbin-1 do printf,unit,wl(i),float(gs(i)),format='(f16.8,2x,e12.5)'
  
  free_lun,unit
  print,'Wrote convolution file '+file
  
end
