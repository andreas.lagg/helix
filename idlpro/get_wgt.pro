pro get_wgt,obs=observation,wgt=wgt,silent=silent,im=im,input=input
  common input,ipt,cnt;,comp_cnt
  
  if n_elements(input) ne 0 then begin
    if n_elements(ipt) ne 0 then iptbk=ipt
    ipt=input
  endif
                                ;----------- weighting part -------------------
                                ;define weighting
  nwl=n_elements(observation.wl)
  emprof=fltarr(nwl)

  wgt=observation
  wgt.i=0
  wgt.q=0
  wgt.u=0
  wgt.v=0
  
  if n_elements(im) eq 0 then im=indgen(ipt.nmi)
  
  for imm=0,n_elements(im)-1 do begin
    im1=im(imm)
    
    usedef=0
    file=ipt.dir.wgt+ipt.wgt_file(im1)
    repeat begin 
      openr,/get_lun,unit,file,error=error
      if error ne 0 then begin
        if usedef eq 1 then begin
          message,/cont,'Weighting file '+file+' not found.'
          reset 
        endif
        filedef='./wgt/default.wgt'
        if keyword_set(silent) eq 0 then $
          message,/cont,'Weighting file '+file+' not found. Using '+filedef
        file=filedef
        usedef=usedef+1
      endif
    endrep until error eq 0
    
    iok=0
    qok=0
    uok=0
    vok=0
    
;    nmi=ipt.nmi-1
;    nmi=im1
;    if ipt.straypol_corr gt 0 then nmi=nmi-1
    
    i=0
    repeat begin
      line='' & readf,unit,line
      word=strsplit(strtrim(line,2),' ',/extract)
      if max(['I','Q','U','V'] eq word(0)) eq 1 then begin
        wgtval=float(word(1))
        wlmin=double(word(2))
        wlmax=double(word(3))
        idx=where(wgt.wl ge wlmin and wgt.wl le wlmax)
;        mwgt=ipt.iquv_weight(*,nmi)
        mwgt=ipt.iquv_weight(*,im1)
        mwgt=mwgt/total(mwgt)*4.
        if idx(0) ne -1 then case word(0) of
          'I': begin
            wgt.i(idx)=wgtval*mwgt(0)
            iok=1
          end
          'Q': begin
            wgt.q(idx)=wgtval*mwgt(1)
            qok=1
          end
          'U': begin
            wgt.u(idx)=wgtval*mwgt(2)
            uok=1
          end
          'V': begin
            wgt.v(idx)=wgtval*mwgt(3)
            vok=1
          end
          else:
        endcase
      endif
      i=i+1
    endrep until eof(unit)
    free_lun,unit
  endfor
  
  if (iok eq 0) then  print, 'WARNING: no weighting defined for ''I'''
  if (qok eq 0) then  print, 'WARNING: no weighting defined for ''Q'''
  if (uok eq 0) then  print, 'WARNING: no weighting defined for ''U'''
  if (vok eq 0) then  print, 'WARNING: no weighting defined for ''V'''

  if keyword_set(silent) eq 0 then $
    if ipt.verbose ge 2 then print,'Weight function: '+file
  
  if n_elements(iptbk) ne 0 then ipt=iptbk
end
