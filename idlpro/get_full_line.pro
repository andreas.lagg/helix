function get_full_line,pikaia_result,x=x,y=y
  
                                ;make sure that line is calculatd
                                ;correctly (zeeman pattern)
  pline=pikaia_result.line
  for il=0,n_elements(pline)-1 do begin
    for it=0,n_tags(pline.par)-1 do begin
      pline(il).par.(it)=pikaia_result.fit(x,y).linepar(il).(it)
    endfor
  endfor
  pline.straypol_par=pikaia_result.fit(x,y).straypol_par
  
                                ;set use_geff to 1 if no info on gu,gl
                                ;is available
  if total(pline.gl^2+pline.gu^2) lt 1e-5 then pikaia_result.input.use_geff=1
  
  
  return,pline
end

