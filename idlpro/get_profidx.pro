pro get_profidx,x=xp,y=yp,stepx=stepx,stepy=stepy,vecx=vecx,vecy=vecy, $
                average=avg,scansize=scansize,rgx=rgx,rgy=rgy
  
;   vecx=xp & vecy=yp
  
;   if avg then begin
    if n_elements(scansize) eq 0 then scansize=0
    if fix(scansize) ne 0 then nsc=fix(xp)/fix(scansize) else nsc=0
    vecx=fix(xp-nsc*scansize)/fix(stepx)*stepx+nsc*scansize+indgen(stepx)
    vecy=fix(yp)/fix(stepy)*stepy+indgen(stepy)
;     mxp=max([max(rgx),min(rgx)+stepx])
;     inrgx=where(vecx ge min(rgx) and vecx le mxp)
;     if inrgx(0) ne -1 then vecx=vecx(inrgx) else vecx=vecx(0)
;     myp=max([max(rgy),min(rgy)+stepy])
;     inrgy=where(vecy ge min(rgy) and vecx le myp)
;     if inrgy(0) ne -1 then vecy=vecy(inrgy) else vecy=vecy(0)
    
    if scansize gt 0 then begin ;averageing should not cross boundary of a scan
                                ;in a multi-scan observation
                                ;(repetitions)
      samescan_idx=fix(vecx)/fix(scansize)
      vecx=vecx(where(samescan_idx eq samescan_idx(0)))
    endif
;  endif
    
    if avg eq 0 then begin
      vecx=vecx(0)
      vecy=vecy(0)
    endif
    
;   print,'X='+add_comma(n2s(vecx),sep='-')+', Y='+add_comma(n2s(vecy),sep='-')
end
