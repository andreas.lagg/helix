common fits,fits
spawn,'rm -f ./atm_archive/atm_StokesIQUV_im27May2008.fp3.1495054..1495688.corr.f0.fitscc_test.fits'
xfits

helix,ipt='gautam_27May2008.ipt',fitprof=fpip,x=388,y=278,obsprof=obip
read_fitsdata,'./atm_archive/atm_StokesIQUV_im27May2008.fp3.1495054..1495688.corr.f0.fitscc_test.fits'
fits1=fits
stop
spawn,'./bin/helix.linux -i gautam_27May2008.ipt -x 388 -y 278'
read_fitsdata,'./atm_archive/atm_StokesIQUV_im27May2008.fp3.1495054..1495688.corr.f0.fitscc_test.fits'
fits2=fits

ipt=fits1.ipt
ipt.ncalls=0
helix,struct_ipt=ipt,x=388,y=278,fitprof=fpst1,obsprof=obst,parin=fits1.data(0,0,*),/noresult
ipt=fits2.ipt
ipt.ncalls=0
helix,struct_ipt=ipt,x=388,y=278,fitprof=fpst2,obsprof=obst,parin=fits2.data(0,0,*),/noresult


plot_profiles,fpip,fpst1,fpst2

print,fpip.v-fpst1.v,fpip.v-fpst2.v,fpst1.v-fpst2.v
stop
end
