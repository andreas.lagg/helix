;perform mathematical operation on components
function comp_oper,map,operation=oper
  
  if n_elements(oper) eq 0 then begin
    print,'Define operation string.'
    oper='C1'
  endif
  
  sz=size(map)
  if sz(0) eq 2 then begin
    nc=1
    nx=sz(1) & ny=sz(2)
    mp=fltarr(1,nx,ny)
    mp(0,*,*)=map
  endif else begin
    nc=sz(1)
    nx=sz(2) & ny=sz(3)
    mp=map
  endelse
  
  
                                ;evaluate operation string
  opstr=strupcase(oper)
  for ic=0,nc-1 do begin
    cstr='C'+string(ic+1,format='(i1)')
    opsplt=strsplit(opstr,cstr,/extract,/regex,/preserve_null,count=c)
    if c ge 2 then $
      opstr=add_comma(opsplt,sep='(mp('+string(ic,format='(i1)')+',*,*))')
  endfor
  
                                ;execute operation
  succ=execute('ret_map='+opstr)
  if succ eq 0 then begin
    ret_map=fltarr(nx,ny)+!values.f_nan
    message,/cont,'Error in execute component operation: '+oper
  endif

  return,reform(ret_map)
end
