pro fill_localstray,lsi,lsq,lsu,lsv,flag,wlout,nwl
  @common_localstray
  @common_maxpar
  
  if n_elements(lsprof) eq 0 and nwl ge 1 then $
    lsprof={wl:wlout,nwl:nwl,ic:0., $
            i:fltarr(nwl),q:fltarr(nwl),u:fltarr(nwl),v:fltarr(nwl)}
  flag=dols
  
  lsi=fltarr(maxwl) & lsq=lsi & lsu=lsi & lsv=lsi
  
  if dols eq 0 then return
  if nwl ne lsprof.nwl then begin ;interpolation necessary
    lsi(0:nwl-1)=man_interpol(lsprof.i,lsprof.wl,wlout)
    lsq(0:nwl-1)=man_interpol(lsprof.q,lsprof.wl,wlout)
    lsu(0:nwl-1)=man_interpol(lsprof.u,lsprof.wl,wlout)
    lsv(0:nwl-1)=man_interpol(lsprof.v,lsprof.wl,wlout)
  endif else begin
    lsi(0:lsprof.nwl-1)=lsprof.i
    lsq(0:lsprof.nwl-1)=lsprof.q
    lsu(0:lsprof.nwl-1)=lsprof.u
    lsv(0:lsprof.nwl-1)=lsprof.v
  endelse
end
  
