pro read_sav,sav,delete=delete,latest=latest
  common pikaia,pikaia_result,oldsav,prpar
  common statistic,statistic
  common pr_orig,pr_orig
  common profile,profile
  common input,ipt,cnt;,comp_cnt
  common obsfile,obsfile
  common fit_orig,fit_orig
  common current_sav,current_sav
  
  if n_elements(sav) eq 0 then begin
    if keyword_set(latest) eq 0 then $
      sav=dialog_pickfile(filter='sav/*pikaia*.sav') $
    else sav=select_latest(filter='sav/[0123456789]*pikaia*.sav',number=9)
    if sav eq '' then begin
      message,/cont,'Please specify sav-file.'
      reset
    endif
  endif
  
  savdir=['./sav/','']
  i=-1
  repeat begin
    i=i+1
    dummy=findfile(savdir(i)+sav,count=cnt)
  endrep until i ge n_elements(savdir)-1 or cnt gt 0
  if cnt eq 0 then begin
    message,/cont,'no sav-file ('+sav+') found in '+add_comma(sep=',',savdir)
    reset
  endif
  sav=savdir(i)+sav
  
  print,'Restoring '+sav+' ... ',format='(a,$)'
  sav=concat_colsav(sav=sav,delete=delete)
  
  
  if n_elements(statistic) ne 0 then begin
    dummy=temporary(statistic)
    dummy=0
  endif
  restore,sav
  current_sav=sav
  
  pikaia_result=adapt_struct(temporary(pikaia_result))
  if n_elements(statistic) ne 0 then pr_orig=pikaia_result
  
  vld=where(pikaia_result.fit.fitness ne 0)
  if vld(0) ne -1 then begin
    pikaia_result.input.x= $
      [min(pikaia_result.fit(vld).x),max(pikaia_result.fit(vld).x)]
    pikaia_result.input.y= $
      [min(pikaia_result.fit(vld).y),max(pikaia_result.fit(vld).y)]
  endif
  ipt=pikaia_result.input
;  if n_elements(profile) eq 0 then begin
;    message,/cont,'No profiles found in sav-file '+sav
;    message,/cont,'Create new sav file with make_sav routine'    
;  endif
  
                                ;try to locate raw data file if file
                                ;is tip-file
  obsfile={raw:'',cc:'',ccx:'',iptobs:pikaia_result.input.observation, $
           tip:0,hinode:0,imax:0}
  istip=check_tipmask(pikaia_result.input.observation,obscore=obscore, $
                      iscc=iscc)
  obsfile.tip=istip ne 0
  
  if istip ne 0 or iscc eq 1 then begin
    if !version.os_family eq 'unix' then spawn,'uname -n',host else host=''
    pathadd=''
;     if host eq 'lxlagg' or host eq 'vttmps' then $
;       pathadd=['/media/LACIE/*','/media/LACIE/*/*', $
;                '/media/LACIE/*/*/*']
    path=[pikaia_result.input.dir.profile, $
          './','./data','./profile_archive', $
          pathadd, $
          '/data/slam/VTT-data/*', $
          '/data/slam/VTT-data/VTT*','/data/slam/VTT-data/VTT*/???????', $
          '/data/slam/VTT-data/data_*','/data/slam/VTT-data/data_*/???????']
    path=remove_multi(path,/no_empty)
    add=['','-??','cc','-??cc']
    ir=0
    repeat begin
      ff=find_file_inpath(path=path,file=obscore+add(ir),error=error,verbose=0)
      ir=ir+1
    endrep until ir ge 4 or error eq 0
    if error eq 0 then begin
      obsfile.raw=ff(0)
      if strmid(ff(0),strlen(ff(0))-2,2) ne 'cc' then addcc='cc' else addcc=''
      fi=file_info(ff(0)+addcc)
      if fi.regular then obsfile.cc=ff(0)+addcc
      fi=file_info(ff(0)+addcc+'x')
      if fi.regular then obsfile.ccx=ff(0)+addcc+'x'
    endif else begin            ;hinode?
      print,'Hinode mode not yet implemented.'
;      ff=file_search()
    endelse
  endif

                                ;store often used parameters
  prpar={sz:size(pikaia_result.fit.fitness),savfile:sav}
  fit_orig=pikaia_result.fit
  print,'Done.'
end

