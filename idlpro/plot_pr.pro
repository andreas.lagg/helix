function plot_pr,x=x,y=y,sav=sav,wl=wl
  @common_cppar
  common ppsav,oldsav
  common pres,pikaia_result
  
  greek
  physical_constants
  if n_elements(oldsav) eq 0 then oldsav=sav
  if n_elements(pikaia_result) eq 0 or oldsav ne sav then restore,/v,sav
  oldsav=sav
  
  ipt=pikaia_result.input
  inew=read_ipt(/empty)
  ipt=read_ipt(struct=ipt)
;  struct_assign,ipt,inew
;  ipt=inew
  
  if n_elements(wl) eq 0 then begin
    wl_num=ipt.wl_num
    if wl_num eq 0 then wl_num=256
    wl=findgen(wl_num)/wl_num* $
    (ipt.wl_range(1)-ipt.wl_range(0))+ $
      ipt.wl_range(0)
  endif
  
  nmi1=ipt.nmi-1
  voigt=ipt.profile eq 'voigt'
  magopt=ipt.magopt(nmi1)
  hanle_azi=ipt.hanle_azi
  norm_stokes_val=ipt.norm_Stokes_val
  use_geff=ipt.use_geff(nmi1)
  modeval=ipt.modeval(nmi1)
  use_pb=ipt.use_pb(nmi1)
  pb_method=ipt.pb_method(nmi1)
  old_norm=ipt.old_norm
  old_voigt=ipt.old_voigt
  pline=get_full_line(pikaia_result,x=x,y=y)
  lst=replicate(def_linest(),n_elements(pline))
  struct_assign,pline,lst,/nozero
  pline=lst

  
  prfit=pikaia_result.fit(x,y)
  empty=result_struc(nx=1,ny=1,ipt=ipt) 
  prempty=empty.fit
  struct_assign,prfit,prempty,/nozero
  prfit=prempty
  fill_localstray,lsi,lsq,lsu,lsv,dols,wl,n_elements(wl)
  fit=compute_profile(/init,line=pline,obs_par=ipt.obs_par, $
                      lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv,dols=dols, $
                      atm=prfit.atm,wl=wl, $
                      blend=prfit.blend, $
                      gen=prfit.gen,doconv=0,doprefilter=0)
  
  retfit=fit
  for i=0,ipt.ncomp-1 do begin
    cmp=compute_profile(/init,line=pline,obs_par=ipt.obs_par,$
                        lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv,dols=dols, $
                        atm=prfit.atm(i),wl=wl, $
                        blend=prfit.blend, $
                        gen=prfit.gen,doconv=0,doprefilter=0)
    dummy=execute('cmp'+n2s(i)+'=cmp')
    retfit=[retfit,cmp]
  endfor
  
  get_wgt,input=ipt,wgt=wgt,obs=fit,im=nmi
  
  plot_profiles,fit,cmp0,cmp1,cmp2,cmp3,cmp4,cmp5,cmp6,cmp7,cmp8,cmp9, $
    cmp10,cmp11,cmp12,cmp13,cmp14,cmp15,cmp16,cmp17,cmp18, $
    weight=wgt,atm=prfit.atm,ipt=ipt,scale=ipt.scale,line=pline
  
  return,retfit
end
