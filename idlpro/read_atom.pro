;read in atomic data
function read_atom,file,path,nline,verbose,use_pb
  @common_maxpar
  maxpar
  
  lst=def_linest()
  atom=replicate(lst,maxlin)
  
  nline=0
  first2=1b
  for i=0,n_elements(file)-1 do if file(i) ne '' then begin
    read_file,path+file(i),'',line,lnr,lall=lall,ipath=ipath,date=date
    nl=n_elements(line)
    if verbose ge 2 then print,'Reading '+n2s(nl)+' lines from '+path+file(i)
    hanlemode=0 
    for il=0,nl-1 do begin
      word=strsplit(/extract,line(il))
      
      if strupcase(word(0)) eq 'HANLEPROF' then hanlemode=1
      if hanlemode then begin
        hanle=atom(0).hanle ;read in data first and then distribute it
                                ;to the correct atom 
        ih=il+1
        reads,line(ih),is2 & hanle.is2=fix(is2)
        ih=ih+1 & reads,line(ih),nterm & hanle.nterm=fix(nterm)
        for in=1,nterm do begin
          ih=ih+1 & reads,line(ih),n,itmp
          hanle.ntm(in-1)=fix(n) & hanle.lsto2(in-1)=fix(itmp)
          jmin2=abs(hanle.is2-hanle.lsto2(in-1))
          jmax2=hanle.is2+hanle.lsto2(in-1)
          for j2=jmin2,jmax2,2 do begin
            ih=ih+1 & reads,line(ih),edum & hanle.energy(in-1,j2)=double(edum)
          endfor
        endfor
        ih=ih+1 & reads,line(ih),ntrans & hanle.ntrans=fix(ntrans)
        for nt=1,ntrans do begin ;2000
          ih=ih+1 & reads,line(ih),n,nterml,ntermu,ae
          hanle.nts(nt-1)=fix(n) & nterml=fix(nterml) & ntermu=fix(ntermu)
          u1=0.d0 & u2=0.d0 & rio=0.d0 & wl=0.d0 & f=0.d0
          ih=ih+1 & reads,line(ih),u1,u2,rio,wl,f
          hanle.aesto(nt-1)=ae
          hanle.fsto(nt-1)=f
          hanle.ntlsto(nt-1)=nterml
          hanle.ntusto(nt-1)=ntermu
          hanle.u1sto(nt-1)=u1
          hanle.u2sto(nt-1)=u2
          hanle.riosto(nt-1)=rio
          hanle.wlsto(nt-1)=wl
        endfor
                      
                                ;He 10830 line is transition #1 in
                                ;Hanle part.  Take only this one,
                                ;saves computation time!  BUT: in rare
                                ;cases leads to singular matrix in
                                ;hanle_module
        ; hanle.ntrans=1
        ; if verbose ge 1 then begin
        ;   print,'Atomic Data File: Using only transition #1 (=He 10830 Triplet)'
        ; endif

      
        
        
                                ;since this is the end of the atomic data file
                                ;the loop variable il is now set to nl
        il=fix(nl)
        
                                ;fill in the hanle info to the atoms 'He1'
        first=1
        for in=0,nline-1 do if string(atom(in).id(0:2)) eq 'He1' then begin
          atom(in).hanle=hanle
                                ;set flag to use hanle_slab for the
                                ;first of the He lines. The hanle-slab
                                ;module computesall 3 lines at once
          if first then atom(in).use_hanle_slab=1
          first=0
        endif

      endif else begin
        wl=double(word(0))
        bids=byte(word(1)+word(2)+string(wl,format='(f9.2)'))
        newline=1b
        for ip=0,nline+il-1 do begin
          if min(atom(ip).id(0:n_elements(bids)-1) eq bids) eq 1 then newline=0b
        endfor
        
        if newline eq 1 then begin
          
          if nline ge maxlin then begin
            message,/cont,'Maximum allowed spectral lines: '+n2s(maxlin)
            reset
          endif
          
          atom(nline).id(0:n_elements(bids)-1)=bids
          atom(nline).wl=wl
          atom(nline).ion=fix(word(2))
                                ;treatment of log(gf): The loggf was
                                ;interpreted as natural log until
                                ;Dec. 14 2009, which is wrong. This
                                ;was corrected on Dec. 14 2009. For
                                ;compatibility with the old results we
                                ;specially treat the he1083.0.dat file
                                ;here. There the log(gf) values are
                                ;given as natural log.
          atom(nline).loggf=float(word(3))
          if file(i) eq 'he1083.0.dat' then begin
            if first2 and verbose ge 1 then begin
              print,'WARNING:'
              print,'  Using old loggf values for He 10830.'
              print,'  New inversions should use the atomic data file'+ $
                ' ''he1083.0.new.dat''.'
              first2=0b
            endif
            atom(nline).f=exp(atom(nline).loggf)
          endif else begin
            atom(nline).f=10^(atom(nline).loggf)
          endelse
          atom(nline).abund=float(word(4))
          atom(nline).sl=float(word(6))
          atom(nline).ll=float(word(7))
          atom(nline).jl=float(word(8))
          atom(nline).su=float(word(9))
          atom(nline).lu=float(word(10))
          atom(nline).ju=float(word(11))
          atom(nline).straypol_use=0
          atom(nline)=create_nc(atom(nline),verbose=verbose)
          
          geff=float(word(5))
          if geff le 20 then if abs(geff-atom(nline).geff) gt 1e-4 then begin
            if verbose ge 1 then begin
              message,/cont,'Atom '+string(bids)+':'
              message,/cont,'    geff (calculated) = '+n2s(geff)
              message,/cont,'    geff (atomic data file) = '+word(5)
            endif
            atom(nline).geff=geff
          endif
          
          
          case strupcase(word(1)) of
            'HE': begin
              atom(nline).mass=4.
                                ;paschen back treatment
              if use_pb then $
                atom(nline)=def_pb(atom(nline))
              
                                ;parameters for correction of
                                ;straypolarization
              case atom(nline).ju of
                0: atom(nline).straypol_par.sign=-1
                else: atom(nline).straypol_par.sign=+1
              endcase
              atom(nline).fit.straypol_amp=0 ;fit flag for straypol
              atom(nline).par.straypol_amp=0 ;gauss ampl. for straypol correction
              atom(nline).scale.straypol_amp.min=0 ;-0.02
              atom(nline).scale.straypol_amp.max=+0.004
              
              atom(nline).fit.straypol_eta0=0 ;fit flag for straypol
              atom(nline).par.straypol_eta0=0 ;ety0 for straypol correction
              atom(nline).scale.straypol_eta0.min=0 ;-0.02
;            atom(nline).scale.straypol_eta0.max=+0.008
              atom(nline).scale.straypol_eta0.max=+0.500
              atom(nline).straypol_use=1
              
              
            end
            'FE': atom(nline).mass=55.845
            'NI': atom(nline).mass=58.693
            'TI': atom(nline).mass=47.867
            'NA': atom(nline).mass=22.
            'SI': atom(nline).mass=28.
            'CA': atom(nline).mass=40.
            'MG': atom(nline).mass=24.
            'CR': atom(nline).mass=51.996
            else: begin
              message,/cont,'Unknown element: '+word(1)
              reset
            end
          endcase
          
          nline=nline+1
        endif
      endelse                   ;IF for hanle part
    endfor
  endif
  
  if verbose ge 3 then for i=0,nline-1 do begin
    print
    print,'Atom: ',string(atom(i).id)
    print,'    WL=',atom(i).wl
    print,'    LOGGF=',atom(i).loggf
    print,'    n_pi=',atom(i).quan.n_pi,'   n_sig=',atom(i).quan.n_sig
    print,'    gl,gu,geff: ',atom(i).gl,atom(i).gu,atom(i).geff
    zeeman_B=atom(i).wl^2.*4.6686411e-13*atom(i).quan.NUB*1e3
    zeeman_P=atom(i).wl^2.*4.6686411e-13*atom(i).quan.NUP*1e3
    zeeman_R=atom(i).wl^2.*4.6686411e-13*atom(i).quan.NUR*1e3
    wep=atom(i).quan.WEP
    wer=atom(i).quan.WER
    web=atom(i).quan.WEB
    print,'    Transition type b:'
    for j=0,atom(i).quan.n_sig-1 do $
      print,'      Shift (mA): ',zeeman_b(j),'  Strength: ',web(j)
    print,'    Transition type p:'
    for j=0,atom(i).quan.n_pi-1 do $
      print,'      Shift (mA): ',zeeman_p(j),'  Strength: ',wep(j)
    print,'    Transition type r:'
    for j=0,atom(i).quan.n_sig-1 do $
      print,'      Shift (mA): ',zeeman_r(j),'  Strength: ',wer(j)
  endfor

  return,atom
end
