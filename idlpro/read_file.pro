pro read_file,ipt_file,ipt_path,line,lnr,lall=lall,ipath=ipath,date=date

                                ;fill strcture variable from
                                ;input-file:
  i=0
  repeat begin
    openr,unit,/get_lun,ipt_path(i)+ipt_file,error=error
    i=i+1
  endrep until error eq 0 or i ge n_elements(ipt_path)
  if error ne 0 then begin
    message,/cont,'Input file '+ipt_file+' not found.'
    reset
  endif
  ipath=ipt_path(i-1)
  
                                ;extract file date
;  spawn,'ls -l '+ipath+ipt_file,lsl
;  lsls=strsplit(/extract,lsl(0))
;  date=lsls(5)+', '+lsls(6)
  fi=file_info(ipath+ipt_file)
  date=systime(0,fi.ctime)
  
                                ;read lines of input file
  line=''
  lnr=0l
  lc=1
  lall=''
  repeat begin
    a='' & readf,unit,a
    lall=[lall,a]
    if strcompress(/remove_all,a) ne '' and $ ;do not read comment lines
                                ;indicated by ';' at the beginning
    strmid(strtrim(a,2),0,1) ne ';' then begin
      line=[line,a]
      lnr=[lnr,lc]
    endif
    lc=lc+1
  endrep until eof(unit)
  free_lun,unit
  
  if n_elements(line) le 1 then begin
    message,/cont,'No valid lines in '+ipath+ipt_file
    reset
  endif
  
  
  line=line(1:*)
  lall=lall(1:*)
  lnr=lnr(1:*)
  ipt_file=ipath+ipt_file
end

