;function to be maximized by pikaia
function func,n,par
  common observation,observation
  common quv_strength,quv_strength,istr,qstr,ustr,vstr,nfree,chi2mode
  common weight,weight
  common atm,atm
  common blend,blend
  common gen,gen
  common pikline,line
  common obs_par,obs_par
  common convcom,convval,doconv,cmode
  common prefiltercom,prefilterval,doprefilter
  common funcpar,iwl_compare,nwl_compare,compwl
  @common_localstraycalc
  
  atm=struct2pikaia(/reverse,par=par,atm=atm,line=line,blend=blend,gen=gen)
  
  profile=compute_profile(atm=atm,line=line,wl=compwl, $
                          obs_par=obs_par,blend=blend,gen=gen, $
                          lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv, $
                          dols=cdols,lspol=clspol, $
                          convval=convval,doconv=doconv,conv_mode=cmode, $
                          prefilterval=prefilterval,doprefilter=doprefilter,$
                          ret_wlidx=iwl_compare,nret_wlidx=nwl_compare)
  
  
;multiply by 1e3 to prevent numerical errors for small differences 
;   diff=(total(weight.i*(1e3*(profile.i-observation.i)/line(0).icont)^2.)/istr+$
;         total(weight.q*(1e3*(profile.q-observation.q))^2.)/qstr + $
;         total(weight.u*(1e3*(profile.u-observation.u))^2.)/ustr + $
;         total(weight.v*(1e3*(profile.v-observation.v))^2.)/vstr)/1e6
  
;   if (diff lt 1e-8) then diff=1.
;   minval=1./(diff)
  return,c_fitness(profile,observation,weight,line(0).icont)
end
