function conv_z,z
  common zconv,zconv
  
  return,(z-zconv.zr0)*zconv.fct+zconv.hgdat
end

function comp_select,mappar,ff
  common wgl,wgl
  common ncomp,ncomp
  
  if ncomp eq 1 then retpar=mappar $
  else begin
    if wgl.comp.val eq ncomp then begin ;max FF component
      sz=size(mappar)
      dummy=max(ff,imax,dimension=1)
      retpar=rebin((mappar)(imax),sz(2),sz(3))
    endif else retpar=reform(mappar(wgl.comp.val,*,*))
  endelse
  return,retpar
end

pro plot_loop,x=xo,y=yo,z=z,colpar=colpar,loop=loop,proj_label=proj_label
  common lpar,lpar
  common wgst
  common xmap,xmap
  
  x=pix2mm(xo,x=xmap.p2mm)
  y=pix2mm(yo,y=xmap.p2mm)
  n=n_elements(x)
  if n_elements(colpar) eq 0 then colpar=n_elements(lpar.name)
  if colpar eq n_elements(lpar.name)+1 then return
  if colpar eq n_elements(lpar.name) then col=intarr(n)+loop.col $
  else begin
    col=(loop.(lpar(colpar).tag))(0:n-1)
    rg=lpar(colpar).rg
    col=(col-rg(0))/(rg(1)-rg(0))*(254)+1
    if lpar(colpar).name eq 'VLOS' then $
      userlct,glltab=7,/full, $
      center=256.*(0.-rg(0))/(rg(1)-rg(0)),/nologo $
    else if lpar(colpar).name eq 'AZI' then $
      userlct,glltab=8,/full $
    else userlct,/nologo,/full
    
    if n_elements(proj_label) ne 0 then begin
      xl=!x.crange(0)+.1*(!x.crange(1)-!x.crange(0))
      yl=!y.crange(1)-.1*(!y.crange(1)-!y.crange(0))
      iz=where(lpar.name eq 'Z')
      zr=lpar(iz).rg
      zl=conv_z(zr(1)-.2*(zr(1)-zr(0)))
      case proj_label of
        'xy': begin
          z(*)=0. & zl=z(0) & tax=0 & ori=0
        end
        'xz': begin
          y(*)=!y.crange(1) & yl=y(0) & tax=1 & ori=0
        end
        'yz': begin
          x(*)=!x.crange(1) & xl=x(0) & tax=5 & ori=270
        end
      endcase
      xyouts,/t3d,/data,text_axes=tax,charsize=!p.charsize,orientation=ori, $
        xl,yl,lpar(colpar).name,z=zl
    endif
  endelse
  
  mm=minmaxp(col(0:n-2))
  if mm(0) eq mm(1) then userlct,/nologo
  for i=0,n-2 do plots,/t3d,/clip,color=col(i),x(i:i+1),y(i:i+1),z(i:i+1)

end

function get_range,arr,n=n
  
  if n_elements(n) eq 0 then rg=[min(arr),max(arr)] $
  else begin
    rg=[1e10,-1e10]
    for i=0,n_elements(n)-1 do $
      rg=[min(arr(0:n(i)-1,*))<rg(0),max(arr(0:n(i)-1,*))>rg(1)]
  endelse
  drg=rg(1)-rg(0)
  rg=rg+[-1,1]*0.02*drg
  
  return,rg
end

pro colorbar,pos=pos,par=par,left=left,right=right
  
  if keyword_set(right) then begin
    left=0
    title='Map scaling'
  endif else begin
    left=1
    title='Loop scaling'
  endelse
  dy=pos(3)-pos(1)
  
  ysep=0.05
  xsep=0.07
  np=n_elements(par)
  for i=0,np-1 do begin
    
    if left then begin
      posi=[pos(0)+0.001,pos(1)+float(dy)/np*i, $
            pos(2)-xsep,pos(1)+float(dy)/np*(i+1)-ysep]
    endif else begin
      posi=[pos(0)+xsep,pos(1)+float(dy)/np*i, $
            pos(2)-0.001,pos(1)+float(dy)/np*(i+1)-ysep]
    endelse
    posi(3)=posi(3)>(posi(1)+ysep)
    
    rg=par(i).rg
    
    col=findgen(256)/(255)*(rg(1)-rg(0))+rg(0)
    col=(col-rg(0))/(rg(1)-rg(0))*(254)+1
    if par(i).name eq 'VLOS' and abs(rg(1)-rg(0)) ge 1e-6 then  $
      userlct,glltab=7, $
      center=256.*(0.-rg(0))/(rg(1)-rg(0)),/nologo,/full $
    else if par(i).name eq 'AZI' then  $
      userlct,/full,glltab=8 $
    else userlct,/nologo,/full
    
    colarr=transpose(col # [1,1])
    
    if !d.name eq 'PS' then $
      tv,/normal, $
      colarr,posi(0),posi(1),xsize=posi(2)-posi(0),ysize=posi(3)-posi(1) $
    else begin
      devpos=(convert_coord(/normal,/to_device, $
                            [posi([0,2])],[posi([1,3])]))([0,1,3,4])
      tv,/normal, $
        congrid(colarr,devpos(2)-devpos(0),devpos(3)-devpos(1)),posi(0),posi(1)
    endelse
    
    
    plot,/noerase,position=posi,/xst,/yst,[0,1],rg, $
      xticks=1,/nodata,charsize=0.4*!p.charsize,xtickname=[' ',' '], $
      ytickname=strarr(32)+' '
    if left then yax=1 else yax=0
    if max(rg)-min(rg) gt 10. then ytf='(i15)' else ytf=''
    axis,yax=yax,yax,0,charsize=0.4*!p.charsize,/yst,yrange=rg, $
      ytitle=par(i).name,ytickformat=ytf
    if i eq 0 then  $
      xyouts,/normal,alignment=left eq 0,charsize=0.5*!p.charsize, $
      posi(([2,0])(left)),posi(1),'!C!C'+title
  endfor
end


pro dev_var,store=store,restore=restore,xdev=xdev,ydev=ydev,xdat=xdat,ydat=ydat
  common dev,dev
  common zconv,zconv
  
  
  if keyword_set(store) or n_elements(dev) eq 0 then begin
    dev={p:!p,x:!x,y:!y,z:!z,zconv:zconv}
  endif else begin
    !p=dev.p
    !x=dev.x
    !y=dev.y
    !z=dev.z
    zconv=dev.zconv
    
;    check = check_math(1,1) 
    if n_elements(xdev) eq 1 and n_elements(ydev) eq 1 then begin
      plane=convert_coord(/data,/to_device,/t3d, $
                           !x.crange([0,0,1,1]),!y.crange([0,1,1,0]),conv_z(0))
      plane=float(plane(0:1,*))
      gx_dy=(plane(1,3)-plane(1,0))
      gx_dx=(plane(0,3)-plane(0,0))
      gy_dx=(plane(0,1)-plane(0,0))
      gy_dy=(plane(1,1)-plane(1,0))
      
      mx00=max([plane(0,0),plane(0,2)])
      mx01=max([plane(0,1),plane(0,3)])
      mx00=plane(0,0)
      mx01=plane(0,1)
      if abs(gx_dy) lt 1e-3 then begin
        xdat=(xdev-mx00)/(gx_dx)*(!x.crange(1)-!x.crange(0))
        ydat=(ydev-mx01)/(gy_dy)*(!y.crange(1)-!y.crange(0))
      endif else if abs(gx_dx) lt 1e-3 then begin
        xdat=-(ydev-mx01)/(gy_dx)*(!x.crange(1)-!x.crange(0))
        ydat=-(xdev-mx00)/(gx_dy)*(!y.crange(1)-!y.crange(0))
      endif else begin
        
      kx=gx_dy/gx_dx
      dx=plane(1,0)-kx*plane(0,0)
      ddx=ydev-kx*xdev
      
      ky=gy_dy/gy_dx
      dy=plane(1,0)-ky*plane(0,0)
      ddy=ydev-ky*xdev
      
      gycut_x=(ddx-dy)/(ky-kx)
      gycut_y=ky*gycut_x+dy      
      gxcut_x=(ddy-dx)/(kx-ky)
      gxcut_y=kx*gxcut_x+dx
      
      ydat=!y.crange(1)+ $
        (gycut_x-plane(0,1))/gy_dx*(!y.crange(1)-!y.crange(0)) 
        
      xdat=(gxcut_x-plane(0,0))/gx_dx*(!x.crange(1)-!x.crange(0))
      endelse
    endif
;    check = check_math(0,0) 
    
  endelse
end

pro draw_loop,map=map,loop=loop,ps=ps
  common wgl,wgl
  common ppar,ppar
  common lpar,lpar
  common zconv,zconv
  common draw,draw
  common wglc,wglc
  common wgst
  common pikaia,pikaia_result,oldsav,prpar
  
  sori=pikaia_result.input.obs_par.slit_orientation
  if max(tag_names(pikaia_result) eq 'HEADER') then $
    use_header=pikaia_result.header
                                ;define scaling
  tm=tag_names(map)
  for i=0,n_elements(ppar.name)-1 do begin
    midx=(where(tm eq ppar(i).name))(0)
    if midx ne -1 then ppar(i).rg=get_range(comp_select(map.(midx),map.ff)) 
  endfor
  if n_elements(loop) gt 0 then begin
    tlp=tag_names(loop)
    for i=0,n_elements(lpar.name)-1 do begin
      midx=(where(tm eq lpar(i).name))(0)
      if wgl.loopscale.val eq 1 then midx=-1
      lidx=(where(tlp eq lpar(i).name))(0)
      if midx ne -1 then lpar(i).rg=get_range(comp_select(map.(midx),map.ff)) $
      else if lidx ne -1 then $
        lpar(i).rg=get_range(loop.(lidx),n=loop.n) 
    endfor
  endif
  
  owin=!d.window
  widget_control,wgl.draw.id,get_value=widx
  wset,widx
  !p.charsize=2.
  if keyword_set(ps) then begin
    psf='ps/loops.ps'
    wd=18. & hg=wd/draw.xs*draw.ys
    ; psout=ps_widget(default=[0,0,0,0,8,0,0],size=[wd,hg],psname=psn, $
    ;                 file=psf,group_leader=gl)
    psout=ps_wg(size=[wd,hg],psname=psn,file=psf,group_leader=gl,/keep_settings)
    if psout(1) eq 1 then begin
      wset,owin
      return
    endif
    !p.font=1
;    device,/helvetica
;    device,/symbol
    device,set_font='helvetica',/tt_font
    !p.charsize=1.25*!p.charsize
  endif else begin
    !p.font=-1
  endelse
  userlct & greek & erase
  !p.position=0 & !p.region=0 & !p.multi=0
  
  osz=size(map.x)
  redfct=([1,2,4,8])(wgl.qual.val)
  szx=osz(1)/float(redfct)
  szy=osz(2)/float(redfct)
  
  mapx=map.x
  mapy=map.y
  
;  if redfct ne 1 then begin
    xred=congrid(mapx,szx+1,szy+1,/minus_one)
    yred=congrid(mapy,szx+1,szy+1,/minus_one)
;   endif else begin
;    xred=fltarr(szx+1,szy+1)
;    xred(0:szx-1,0:szy-1)=mapx
;    xred(*,szy)=xred(*,szy-1)
;    xred(szx,*)=xred(szx-1,*)+xavg
;    yred=fltarr(szx+1,szy+1)
;    yred(0:szx-1,0:szy-1)=mapy
;    yred(szx,*)=yred(szx-1,*)
;    yred(*,szy)=yred(*,szy-1)+yavg
;  endelse
  
  xr=[min(xred),max(xred)]
  yr=[min(yred),max(yred)]
  if n_elements(loop) ne 0 then zr=get_range(loop.z,n=loop.n) else zr=[0,10�]
  if wgl.zaspect.val eq 1 then zr=[0,max([max(xr)-min(xr),max(yr)-min(yr)])]
  zr=zr>0
  
  
                                ;set 3d trafo
  scale3,xrange=xr,yrange=yr,zrange=zr,ax=wgl.ax.val,az=wgl.az.val
  
                                ;draw maps
  nt=total(ppar.show)
  ws=where(ppar.show eq 1)
  pos=[.1,.1,.9,.9,.1,.9]
  
  if ws(0) ne -1 then $
    colorbar,pos=[pos(2),pos(1),1,pos(3)],par=ppar(reverse(ws)),/right
  
  if wgl.loopscale.val eq 1 then begin
  lps=remove_multi([wgl.colcode.val,wgl.xyproj.val, $
                    wgl.xzproj.val,wgl.yzproj.val])
  ils=where(lps lt n_elements(lpar.name))
  if ils(0) ne -1 then $
    colorbar,pos=[0,pos(1),pos(0),pos(3)],par=lpar(reverse(lps(ils))),/left
  endif
  
  init=0
  for it=(nt-1)>0,0,-1 do begin
    if nt le 1 then hg=0. else $
      hg=(nt-1.-it)/(nt-1.)*(pos(5)-pos(4))*wgl.frac.val/100.
    hgdat=(convert_coord(/t3d,[0,0,hg+pos(4)],/normal,/to_data))(2)
    zconv={zr0:zr(0),fct:(pos(5)-pos(4)-hg),hgdat:hgdat}
    
    plot,/t3d,/noerase,/nodata,[0,1],xst=5,yst=5,xrange=xr,yrange=yr, $
      zrange=zr,position=[pos(0:3),pos(4)+hg,pos(5)]
    if nt gt 0 then begin
;     tmap=resize(comp_select(map.(ppar(ws(it)).tag)),xavg,yavg)
      tmap=comp_select(map.(ppar(ws(it)).tag),map.ff)
      if ppar(ws(it)).name eq 'AZI' then begin
        ; tmap=azi_qu2xy(tmap,slit_orientation=sori,header=use_header, $
        ;                obs=pikaia_result.input.observation, $
        ;                par=pikaia_result.input.obs_par, $
        ;                hanle=pikaia_result.input.modeval eq 5)
        tmap=azi_qu2xy(tmap)
        tmap=((tmap+270) mod 180)-90
        cycle=[-90,90]
      endif else cycle=-1
                                ;set data points to no valid where in
                                ;all components no magnetic field is present
      if ppar(ws(it)).name eq 'AZI' or ppar(ws(it)).name eq 'INC' or $
        ppar(ws(it)).name eq 'BTOT' then begin
        noval=total(map.btot,1) eq 0 and total(map.azi,1) eq 0 and $
          total(map.inc,1) eq 0
        if total(noval) ge 1 then tmap(where(noval))=!values.f_nan
      endif
      if wgl.smoothmap.val eq 1 and $
        (wglc.smooth_val.val lt min(osz(1:2))*2 and $
         wglc.smooth_val.val ge 2) then begin
;        tmap=median(tmap,wglc.smooth_val.val) 
        tmap=avgbox(tmap,binx=wglc.smooth_val.val,biny=wglc.smooth_val.val, $
                    cycle=cycle)
        if ppar(ws(it)).name eq 'AZI' then tmap=((tmap+270) mod 180)-90        
        tmap=congrid(tmap,osz(1),osz(2))
      endif
      
      
;      img=congrid(tmap,szx,szy)
      img=avgbox(tmap,nx=szx,ny=szy,cycle=cycle)
      err=where(finite(img) eq 0)
      sz=size(img)
      rg=ppar(ws(it)).rg
      
      if ppar(ws(it)).name eq 'VLOS' then begin
        userlct,glltab=7, $
          center=256.*(0.-rg(0)) / (rg(1)-rg(0)),/nologo,/full
      endif else  if ppar(ws(it)).name eq 'AZI' then begin
        userlct,glltab=8,/full
      endif else userlct,/nologo,/full
;      imgscl=(img-rg(0))/(rg(1)-rg(0))*(254-17)+17
      imgscl=(img-rg(0))/(rg(1)-rg(0))*(254)+1
      if err(0) ne -1 then imgscl(err)=!p.background
      for ix=0,szx-1 do for iy=0,szy-1 do begin
        x=xred([ix,ix+1,ix+1,ix],[iy,iy,iy+1,iy+1])
        y=yred([ix,ix+1,ix+1,ix],[iy,iy,iy+1,iy+1])
        ncorners=remove_multi(n2s(x)+'.'+n2s(y))
        if n_elements(ncorners) eq 4 then begin
          polyfill,x,y,color=imgscl(ix,iy),/t3d,z=hgdat
        endif
    endfor
    endif
    unit=(['pix','Mm','Arcsec',wgst.p2mm.str])(fix(map.p2mm))
    if init then begin
      tnam=strarr(32)+' ' 
      xytit=strarr(2)
    endif else begin
      xytit=['X','Y']+' ('+unit+')'
    endelse
    plot,/t3d,/noerase,/nodata,[0,1],xst=1,yst=1,xrange=xr,yrange=yr, $
      zrange=zr,position=[pos(0:3),pos(4)+hg,pos(5)], $
      xtickname=tnam,ytickname=tnam,xtitle=xytit(0),ytitle=xytit(1)
    dev_var,/store
    
    init=1
  endfor
  
  userlct,/nologo
  
                                ;draw axis frames
  axis,zaxis=1,xr(0),yr(1),zrange=zr,/xst,/yst,/zst,/data,/t3d, $
    ztick_get=zt,ztitle='Z ('+unit+')'
  axis,zaxis=0,xr(1),yr(1),zrange=zr,/xst,/yst,/zst,/data,/t3d, $
    ztickname=strarr(32)+' '
  axis,zaxis=0,xr(1),yr(0),zrange=zr,/xst,/yst,/zst,/data,/t3d

  for i=0,n_elements(zt)-1 do begin
    plots,/t3d,/data,linestyle=1,xr(1)+[0,0],yr,conv_z(zt(i))+[0,0]
    plots,/t3d,/data,linestyle=1,xr,yr(1)+[0,0],conv_z(zt(i))+[0,0]
  endfor
  axis,xaxis=1,0,yr(1),conv_z(zr(1)),xrange=xr, $
    /xst,/yst,/zst,/data,/t3d,xtickname=strarr(32)+' ',xtick_get=xt
  axis,yaxis=1,xr(1),0,conv_z(zr(1)),xrange=xr, $
    /xst,/yst,/zst,/data,/t3d,ytickname=strarr(32)+' ',ytick_get=yt
  for i=0,n_elements(yt)-1 do $
    plots,/t3d,/data,linestyle=1,xr(1)+[0,0],yt(i)+[0,0],conv_z(zr)
  for i=0,n_elements(xt)-1 do $
    plots,/t3d,/data,linestyle=1,xt(i)+[0,0],yr(1)+[0,0],conv_z(zr)
  
  nshow=[1,2,4,10]
  if wgl.loopshow.val eq 1 then begin
    for il=0,n_elements(loop)-1,nshow(wgl.nshow.val) do begin ;draw projections
      for ip=1,3 do begin
        case ip of
          1: begin 
            pj='xy' & cp=wgl.xyproj.val
          end
          2: begin 
            pj='xz' & cp=wgl.xzproj.val
          end
          3: begin 
            pj='yz' & cp=wgl.yzproj.val
          end
        endcase
        plot_loop,colpar=cp,loop=loop(il),proj_label=pj, $
          x=loop(il).x(0:loop(il).n-1), $
          y=loop(il).y(0:loop(il).n-1), $
          z=conv_z(loop(il).z(0:loop(il).n-1))
      endfor      
;       plot_loop,colpar=wgl.xyproj.val,loop=loop(il),proj_label='xy', $ ;xyproj
;         x=loop(il).x(0:loop(il).n-1), $
;         y=loop(il).y(0:loop(il).n-1), $
;         z=conv_z(fltarr(loop(il).n))
;       plot_loop,colpar=wgl.xzproj.val,loop=loop(il),proj_label='xz', $ ;xzproj
;         x=loop(il).x(0:loop(il).n-1), $
;         y=yr(1)+fltarr(loop(il).n), $
;         z=conv_z(loop(il).z(0:loop(il).n-1))
;       plot_loop,colpar=wgl.yzproj.val,loop=loop(il),proj_label='yz', $ ;yzproj
;         x=xr(1)+fltarr(loop(il).n), $
;         y=loop(il).y(0:loop(il).n-1), $
;         z=conv_z(loop(il).z(0:loop(il).n-1))
    endfor
    
    for il=0,n_elements(loop)-1,nshow(wgl.nshow.val) do begin ;draw loops
      plot_loop,colpar=wgl.colcode.val,loop=loop(il), $ 
        x=loop(il).x(0:loop(il).n-1), $
        y=loop(il).y(0:loop(il).n-1), $
        z=conv_z(loop(il).z(0:loop(il).n-1))
    endfor
  endif
  
  if !d.name eq 'PS' then begin
    xyouts,0,0,/normal,'!C!C'+psn,charsize=0.5*!p.charsize
    device,/close
    print,'Created PS-file: '+psn
    if psout(3) eq 1 then  spawn_gv,psn
  endif
  
  !p.font=-1 & set_plotx
  !p.t3d=0
  wset,owin
end

function loopst
  maxel=500
  elp={n:0,len:0.,col:9,solar:0, $
       x:fltarr(maxel),y:fltarr(maxel),z:fltarr(maxel), $
       btot:fltarr(maxel),dz:fltarr(maxel),vlos:fltarr(maxel), $
       inc:fltarr(maxel),azi:fltarr(maxel), $
       ix:intarr(maxel),iy:intarr(maxel), $
       gradb:fltarr(maxel),gradv:fltarr(maxel)}
  return,elp
end

function remove_double_loops,loop
  
  if n_tags(loop) eq 1 then return,loop
  
  nlp=n_elements(loop)
  if nlp eq 0 then return,loop
  dbl=bytarr(nlp)
  for i=0,nlp-2 do begin
    idx=where(loop(i+1:*).x(0) eq loop(i).x(0) and $
              loop(i+1:*).y(0) eq loop(i).y(0) and $
              loop(i+1:*).col eq loop(i).col)
    if idx(0) ne -1 then for ii=0,n_elements(idx)-1 do begin
      lpi=loop(idx(ii)+i+1)
      dbl(idx(ii)+i+1)=dbl(idx(ii)+i+1) or $
        (lpi.x(lpi.n-1) eq loop(i).x(loop(i).n-1) and $
         lpi.y(lpi.n-1) eq loop(i).y(loop(i).n-1))
    endfor
  endfor
  
  wdbl=where(dbl eq 0)
  if wdbl(0) ne -1 then begin
    loop=loop(wdbl)
    print,'removed '+n2s(n_elements(where(dbl)))+' double loops.'
  endif
  
  return,loop
end

pro trace_loop,map=map,loop=loop,x=xo,y=yo,all=all,radius=radius
  common wgl,wgl  
  common wglc,wglc
  common pikaia,pikaia_result,oldsav,prpar
  common wgst
  
;  x=57.  & y=26.
  widget_control,wgl.base.id,hourglass=1,sensitive=0
  
  szo=size(map.x)
  xfct=(!x.crange(1)-!x.crange(0))/(szo(1))
  yfct=(!y.crange(1)-!y.crange(0))/(szo(2))
  
;   if n_elements(xo) ne 0 then x=(xo-!x.crange(0))/xfct
;   if n_elements(yo) ne 0 then y=(yo-!y.crange(0))/yfct
  if n_elements(xo) ne 0 then x=xo
  if n_elements(yo) ne 0 then y=yo
  
  xavg=xfct & yavg=yfct
;  xavg=get_avgbin(map.x)
;  yavg=get_avgbin(map.y)
  
  sz=szo
  sz(1)=szo(1)/xavg
  sz(2)=szo(2)/yavg
;  mapx=resize(map.x,xavg,yavg)
;  mapy=resize(map.y,xavg,yavg)
  mapx=map.x
  mapy=map.y
  
  elp=loopst()
  maxel=n_elements(elp.x)
  xr=[min(mapx),max(mapx)]
  yr=[min(mapy),max(mapy)]
  zr=[0.,100.]                  ;zrange in same units as xr and yr
  
                                ;assume equidistant grid
; xdist=total((mapx(*,0)-shift(mapx(*,0),1))(1:*))/(n_elements(mapx(*,0))-1)
; ydist=total((mapy(0,*)-shift(mapy(0,*),1))(1:*))/(n_elements(mapy(0,*))-1)
; rdist=sqrt(xdist^2+ydist^2)
; rdist=1.
  rdist=0.5*sqrt(xavg^2+yavg^2)
  
;  sz=size(mapx)
  if keyword_set(all) or n_elements(radius) ne 0 then begin

;    xarr=(indgen(szo(1),szo(2)) mod szo(1))
;    yarr=(indgen(szo(1),szo(2))  /  szo(1))
    
;    xarr=(indgen(sz(1),sz(2)) mod sz(1))*xavg
;    yarr=(indgen(sz(1),sz(2))  /  sz(1))*yavg
    szx=szo(1)/([0.5,1,2,3,4])(wgl.loopmesh.val)
    szy=szo(2)/([0.5,1,2,3,4])(wgl.loopmesh.val)
;     xarr=(indgen(szx,szy) mod szx)/float(szx)*(xr(1)-xr(0))+xr(0)
;     yarr=(indgen(szx,szy)  /  szx)/float(szy)*(yr(1)-yr(0))+yr(0)
    xarr=((fltarr(szy)+1) ## findgen(szx))/(szx)*(xr(1)-xr(0))+xr(0)
    yarr=(findgen(szy) ## (fltarr(szx)+1))/(szy)*(yr(1)-yr(0))+yr(0)
;    xarr=congrid(float(xarr),szx,szy,/interp,/minus_one)
;    yarr=congrid(float(yarr),szx,szy,/interp,/minus_one)
    if n_elements(radius) ne 0 then begin
      print,'Finding loops (r='+n2s(radius,format='(f10.2)')+'):'
      dist=sqrt((xarr-x(0))^2+(yarr-y(0))^2)
      good=where(dist lt radius(0) and finite(xarr) and finite(yarr))
      if good(0) ne -1 then begin
        xarr=xarr(good) & yarr=yarr(good)
      endif else begin
        xarr=x & yarr=y
      endelse
    endif
  endif else begin
    xarr=x & yarr=y    
  endelse
  
;   xarr=xarr+xavg/2.
;   yarr=yarr+yavg/2.
  nxy=n_elements(xarr)
  
  ravg=sqrt((xavg^2+yavg^2)/2.)
  
  sori=pikaia_result.input.obs_par.slit_orientation
  if max(tag_names(pikaia_result) eq 'HEADER') then $
    use_header=pikaia_result.header
  sminc =comp_select(map.inc,map.ff)
  smiinc=sminc
  incsol_flag=0
  if wgl.incsolar.val eq 1 then begin
    sminc_solar=comp_select(map.inc_solar,map.ff)
    if total(abs(sminc_solar)) gt 1e-3 then begin
      sminc=sminc_solar 
      message,/cont,'Using INC_SOLAR for loop tracing'
      icsol_flag=1
    endif else $
      message,/cont,'INC_SOLAR: No conversion to Solar coordinates done,'+ $
      ' using INC (LOS). Close Loop-Trace, perform a conversion to solar' + $
      ' coordinates and open Loop-Trace again.'
  endif
  smazi =comp_select(map.azi,map.ff)
  ; smazi=azi_qu2xy(smazi,slit_orientation=sori,header=use_header, $
  ;                 obs=pikaia_result.input.observation, $
  ;                 par=pikaia_result.input.obs_par, $
  ;                 hanle=pikaia_result.input.modeval eq 5)
  smazi=azi_qu2xy(smazi)
  smazi=((smazi+270) mod 180)-90
  smbtot=comp_select(map.btot,map.ff)
  smvlos=comp_select(map.vlos,map.ff)
  
  if wglc.smooth_val.val lt min(sz(1:2))*2 and $
    wglc.smooth_val.val ge 2 then begin
    sminc=smooth(sminc,wglc.smooth_val.val,/edge)
    smbtot=smooth(smbtot,wglc.smooth_val.val,/edge)
    smvlos=smooth(smvlos,wglc.smooth_val.val,/edge)
    smazi=avgbox(smazi,binx=wglc.smooth_val.val,biny=wglc.smooth_val.val, $
                 cycle=[-90,90])
    smazi=congrid(smazi,szo(1),szo(2))
  endif
  smazi=smazi*!dtor
  sminc=sminc*!dtor
  
  cosinc=cos(sminc)
  
  failed={bad_gradb:0l,maxdev_inc:0l,maxdev_azi:0l,max_dz:0l,min_pix:0l, $
          outofbox:0l,tol_vlos:0l,bad_gradv:0l,reverse:0l}
  
  for i=0l,nxy-1 do begin
;     if nxy gt 1000 then begin
;       prog=float(i)/(nxy-1)
;       if fix(prog*100) ne fix(float(i-1)/(nxy-1)*100) then $
;         print,format='(i3,''%'',$)',prog*100
;     endif
    cloop=elp  
    n=0
    loopfound_col=9
    
    idir=0 & dirok=0
    posx=float(xarr(i)) & posy=float(yarr(i)) & posz=0.    
;    posx=float(xarr(i))*xfct & posy=float(yarr(i))*yfct & posz=0.
    ox=posx & oy=posy & oz=posz
    loopend=0 & bestloop=0 
    bad_loop=0
    bad_countb=0 
    bad_countv=0 
    gradb_inival=0.
    gradv_inival=0.
    repeat begin
      if posx lt min(xr) or posx gt max(xr) or $
        posy lt min(yr) or posy gt max(yr) then begin
        bad_loop=3
        failed.outofbox=failed.outofbox+1
      endif
      cloop.x(n)=posx & cloop.y(n)=posy & cloop.z(n)=posz
;     cloop.x(n)=posx*xavg & cloop.y(n)=posy*yavg & cloop.z(n)=posz
      dr=sqrt((mapx-posx)^2+(mapy-posy)^2)
      minr=min(dr) & imap=!c
;      if minr gt 1.5*rdist then bad_loop=1
      if bad_loop eq 0 then begin
        cloop.ix(n) = fix(imap) mod szo(1) 
        cloop.iy(n) = fix(imap)  /  szo(1)
        cloop.btot(n)=smbtot(imap)
        cloop.vlos(n)=smvlos(imap)
        cloop.inc(n)=sminc(imap)/!dtor
        cloop.azi(n)=smazi(imap)/!dtor
        
        
                                ;loop criteria: direction
        if n gt 0 then begin
          if abs(cloop.inc(n-1)-cloop.inc(n)) gt wglc.max_inc.val then begin
            bad_loop=2
            failed.maxdev_inc=failed.maxdev_inc+1
          endif
                                ;check if other azi-solution 180�
                                ;is smoother
          dazi=(cloop.azi(n-1)-cloop.azi(n))
          if abs((cloop.azi(n-1)-(cloop.azi(n)+180))) lt abs(dazi) then $
            cloop.azi(n)=cloop.azi(n)+180
          if abs((cloop.azi(n-1)-(cloop.azi(n)-180))) lt abs(dazi) then $
            cloop.azi(n)=cloop.azi(n)-180          
          if abs((cloop.azi(n-1)-cloop.azi(n))) gt wglc.max_azi.val then begin
            bad_loop=2
            failed.maxdev_azi=failed.maxdev_azi+1
          endif
        endif
        
        repeat begin
          dx=cos((cloop.azi(n)*!dtor+idir*180*!dtor))*rdist
          dy=sin((cloop.azi(n)*!dtor+idir*180*!dtor))*rdist
          dz=cosinc(imap)*sqrt(dx^2+dy^2)*([1,-1])(idir)
          if n eq 0 then begin ;check for correct direction: z must increase
                                ;at starting point
            if dz lt 0 then idir=1 else dirok=1
          endif
          dirok=dirok+1
        endrep until dirok ge 2 or n gt 0 
        cloop.dz(n)=dz
                                ;loop criteria: sign of gradb
        if n gt 0 then begin
          if cloop.dz(n) ne 0 then $
            gradb=(cloop.btot(n)-cloop.btot(n-1)) / cloop.dz(n) $
          else gradb=0.
          if gradb_inival eq 0 and gradb ne 0 then gradb_inival=gradb
          cloop.gradb(n)=gradb
                                ;gradb must be wrong for two
                                ;subsequent pixels
          if (gradb_inival*cloop.gradb(n-1) lt 0 and $
              gradb_inival*cloop.gradb((n-2)>0) lt 0) or $
            abs(cloop.btot(n)-cloop.btot(n-1)) gt wglc.tol_btot.val then begin
            bad_countb=bad_countb+1
            bad_loop=bad_countb ge wglc.max_badb.val
            if bad_loop then failed.bad_gradb=failed.bad_gradb+1
          endif
          
                                ;check if loop 'turned around' (same
                                ;pixel as previous)
          if n ge 2 then begin
            if (abs(cloop.x(n)-cloop.x(n-2)) lt 1e-5 and $
                abs(cloop.y(n)-cloop.y(n-2)) lt 1e-5) then begin
;              bad_loop=8
              loopend=1
            endif
          endif
          
                                ;loop criteria: sign of gradvlos
          if cloop.dz(n) ne 0 then $
            gradv=(cloop.vlos(n)-cloop.vlos(n-1)) / cloop.dz(n) $
          else gradv=0.
          if gradv_inival eq 0 and gradv ne 0 then gradv_inival=gradv
          cloop.gradv(n)=gradv
                                ;gradv must be wrong for three
                                ;subsequent pixels
          if (gradv_inival*cloop.gradv(n-1) lt 0 and $
              gradv_inival*cloop.gradv((n-2)>0) lt 0) or $
            abs(cloop.vlos(n)-cloop.vlos(n-1)) gt wglc.tol_vlos.val then begin
            bad_countv=bad_countv+1
            bad_loop=bad_countv ge wglc.max_badv.val
            if bad_loop then failed.bad_gradv=failed.bad_gradv+1
          endif

                                ;max jump in vlos
          if abs(cloop.vlos(n)-cloop.vlos(n-1)) gt wglc.tol_vlos.val then begin
            failed.tol_vlos=failed.tol_vlos+1
            bad_loop=4
          endif
        endif
        
;        if same_pix eq 0 then begin
        len=0
        for in=0,n-2 do len=len+sqrt( (cloop.x(in)-cloop.x(in+1))^2 + $
                                      (cloop.y(in)-cloop.y(in+1))^2 + $
                                      (cloop.z(in)-cloop.z(in+1))^2 )
        cloop.len=len
        
        if len ge wglc.min_pix.val then begin
          case wglc.end_cond.val of
            0: begin          ;loop criterium: similar z at footpoints
              n_0=abs(cloop.z(0)-cloop.z(n))
              n1_0=abs(cloop.z(0)-cloop.z(n-1))
              diff=wglc.max_zdiff.val
            end
            1: begin          ;loop criterium: similar B at footpoints
              n_0=abs(cloop.btot(0)-cloop.btot(n))
              n1_0=abs(cloop.btot(0)-cloop.btot(n-1))
              diff=wglc.tol_btot.val
            end
            2: begin       ;loop criterium: similar VLOS at footpoints
              n_0=abs(cloop.vlos(0)-cloop.vlos(n))
              n1_0=abs(cloop.vlos(0)-cloop.vlos(n-1))
              diff=wglc.tol_vlos.val
            end
          endcase
          
          if cloop.dz(0)*cloop.dz(n) lt 0 then begin ;up / down
            if n_0 lt diff then begin
                                ;check for better loopend
              if n1_0 lt n_0 then begin
                n=n-1
                bestloop=1
              endif else if loopend eq 1 then bestloop=1
              loopend=1
            endif else if loopend then bestloop=1
          endif else if loopend then bestloop=1
        endif
;        endif else n=n-1
        
        posx=posx+dx & posy=posy+dy
        posz=posz+dz            ;cloop.dz(n)   ;*([1,-1])(idir)
        
      endif      
;print,n,cloop.x(n),cloop.y(n) & plots,/t3d,cloop.x(n),cloop.y(n),0,/data,psym=4 
      n=n+1
    endrep until n ge maxel or (bestloop eq 1) or bad_loop ne 0

                                ;turn loop around if xend gt xstart
    if cloop.x(0) gt cloop.x(n-1) then $
      for it=0,n_tags(cloop)-1 do $
      if n_elements(cloop.(it)) eq maxel then begin
      cloop.(it)(0:n-1)=reverse(cloop.(it)(0:n-1))
    endif
    cloop.dz=-cloop.dz & cloop.gradb=-cloop.gradb & cloop.gradv=-cloop.gradv
                                ;convert loopx/y
    cloop.x(0:n-1)=pix2mm(cloop.x(0:n-1),x=map.p2mm,/reverse)
    cloop.y(0:n-1)=pix2mm(cloop.y(0:n-1),y=map.p2mm,/reverse)
    cloop.x(n:*)=0
    cloop.y(n:*)=0
    
    
    cloop.azi=((cloop.azi+270) mod 180)-90
    cloop.solar=incsol_flag
    cloop.n=n
                                ;make loop start at z=0
;  cloop.z(0:n-1)=cloop.z(0:n-1)-min(cloop.z(0:n-1))
    if bad_loop eq 0 then begin
;    if n ge wglc.min_pix.val/ravg then begin ;minimum n is 4
                                ;add this loop if foot points are at
                                ;approx. same height 
      loopdiff=abs(cloop.z(0)-cloop.z(n-1))
      loopsign=[cloop.dz(0),cloop.dz(n-1)]
;      if loopdiff lt wglc.max_zdiff.val and $
;        cloop.dz(0)*cloop.dz(n-1) lt 0 then begin
      
      loopfound_col=2
      prt=-1
      if n_elements(loop) eq 0 then begin
        loop=cloop 
        prt=0
      endif else begin
                                ;check if loop is alredy covered by
                                ;another loop
        
        for in=0,n-1 do begin
          nlp=n_elements(loop)
          il=-1
          oidx=-1
          repeat begin
            il=il+1
            oidx=(where(loop(il).ix eq cloop.ix(in) and $
                        loop(il).iy eq cloop.iy(in)))(0)
          endrep until il ge nlp-1 or oidx ge 0
          if oidx eq -1 then begin ;add new loop
            lp=replicate(elp,nlp+1)
            for il=0,nlp-1 do begin
              struct_assign,loop(il),elp
              lp(il)=elp
            endfor
            loop=lp
            il=n_elements(loop)-1 ;index of current loop
            loop(il)=cloop
            prt=il
          endif else begin      ;replace old loop if length is larger
            if loop(il).len lt cloop.len then begin
              print,'Replace loop '+n2s(il)+': old-len=',loop(il).len, $
                ', new-len=',cloop.len
              clp=loop(il)
              struct_assign,cloop,clp & loop(il)=clp
              prt=-1
            endif
          endelse
        endfor
      endelse
      if prt ge 0 then begin
        print,'Loop '+n2s(prt)+': n=',loop(prt).n,', dz=', $
          loop(prt).z(0)-loop(prt).z(n-1),', len=',len
      endif
    endif else failed.min_pix=failed.min_pix+1
;    plots,psym=1,ox*xfct+!x.crange(0),oy*yfct+!y.crange(0), $
;    plots,psym=1,ox+!x.crange(0),oy+!y.crange(0), $
    plots,psym=1,ox,oy,conv_z(oz),/t3d,color=loopfound_col
  endfor
  
                                ;convert loopx/y to pixel coordinates
;  for i=0,n_elements(loop)-1 do begin
;    n=loop(i).n
;    loop(i).x(0:n-1)=pix2mm(loop(i).x(0:n-1),x=map.p2mm,/reverse)
;    loop(i).y(0:n-1)=pix2mm(loop(i).y(0:n-1),y=map.p2mm,/reverse)
;  endfor  
  
  
  print,'Statistics:'
  ntot=0l
  for it=0,n_tags(failed)-1 do ntot=ntot+failed.(it)
  for it=0,n_tags(failed)-1 do $
    print,n2s(100.*failed.(it)/ntot,format='(f15.2)')+ $
    '% failed due to '+(tag_names(failed))(it)
  
  loop=remove_double_loops(loop)
  print,'Total number of loops: '+n2s(n_elements(loop))
  widget_control,wgl.base.id,hourglass=0,sensitive=1
end

pro delete_loops_old,loop=loop,x=xdat,y=ydat,radius=radius
  
  nl=n_elements(loop)
  if nl eq 0 then return
  
  del=bytarr(nl)
  for i=0,nl-1 do begin
    dr1=sqrt((loop(i).x(0)-xdat)^2+(loop(i).y(0)-ydat)^2)
    dr2=sqrt((loop(i).x(loop(i).n-1)-xdat)^2+(loop(i).y(loop(i).n-1)-ydat)^2)
    if dr1 le radius or dr2 le radius then del(i)=1b
  endfor
  keepidx=where(del eq 0)
  if keepidx(0) ne -1 then loop=loop(keepidx) else dummy=temporary(loop)
  print,'Deleted '+n2s(nl-n_elements(loop))+ $
    ' loops (r='+n2s(radius,format='(f10.2)')+').'
end

pro delete_loops,loop=loop,x=xdat,y=ydat,radius=radius
  common curr_loop,mindist,minidx,old_minidx
  
  nl=n_elements(loop)
  if nl eq 0 then return
  
  if n_elements(minidx) eq 0 then return
  if minidx eq -1 then return
  
  if nl eq 1 then dummy=temporary(loop) else begin
    loop(minidx).n=0
    gl=where(loop.n gt 0)
    if gl(0) ne -1 then loop=loop(gl)
    minidx=minidx<(n_elements(loop)-1)
  endelse
  
  print,'Deleted Loop #'+n2s(minidx)+', total loops: '+n2s(n_elements(loop))
end

pro reset_wglc
  common wglc,wglc
  
  if n_elements(wglc) eq 0 then begin
    subst={id:0l,val:0.,str:''}
    wglc={base:subst,max_badb:subst,max_badv:subst, $
          max_inc:subst,max_azi:subst, $
          tol_btot:subst,max_zdiff:subst,tol_vlos:subst, $
          min_pix:subst,smooth_val:subst,control:subst,end_cond:subst}
  endif
  
  wglc.max_badb.val=10 ;max number of bad conditions (gradb) to end loop
  wglc.max_badv.val=4 ;max number of bad conditions (gradv) to end loop
  wglc.tol_btot.val=50.             ;tolerance for btot when bad bgrad in Gauss
  wglc.tol_vlos.val=5000.       ;tolerance for vlos in km/s
  wglc.max_zdiff.val=1.                ;max difference for footpoints in height
  wglc.min_pix.val=15               ; minimum number of pixels for loop
  wglc.smooth_val.val=7                  ;smoothing value for angle and bfield
  wglc.max_inc.val=15           ;max deviation in inclination
  wglc.max_azi.val=25           ;max deviation in azimuth
  wglc.end_cond.val=0           ;end condition: z-difference must be 0
  
  wglc.max_badb.val=40 ;max number of bad conditions (gradb) to end loop
  wglc.max_badv.val=40 ;max number of bad conditions (gradv) to end loop
  
  widget_control,bad_id=bid,wglc.base.id
  if bid eq 0 then if wglc.base.id ne 0 then begin
    widget_control,wglc.max_badb.id,set_value=wglc.max_badb.val
    widget_control,wglc.max_badv.id,set_value=wglc.max_badv.val
    widget_control,wglc.max_inc.id,set_value=wglc.max_inc.val
    widget_control,wglc.max_azi.id,set_value=wglc.max_azi.val
    widget_control,wglc.tol_btot.id,set_value=wglc.tol_btot.val
    widget_control,wglc.tol_vlos.id,set_value=wglc.tol_vlos.val
    widget_control,wglc.max_zdiff.id,set_value=wglc.max_zdiff.val
    widget_control,wglc.min_pix.id,set_value=wglc.min_pix.val
    widget_control,wglc.smooth_val.id,set_value=wglc.smooth_val.val
    widget_control,wglc.end_cond.id,set_value=wglc.end_cond.val
  endif
end

pro xwglc_event,event
  common wglc,wglc
  
  widget_control,event.id,get_uvalue=uval
  case uval of
    'max_badb': wglc.max_badb.val=event.value
    'max_badv': wglc.max_badv.val=event.value
    'max_inc': wglc.max_inc.val=event.value
    'max_azi': wglc.max_azi.val=event.value
    'tol_btot': wglc.tol_btot.val=event.value
    'tol_vlos': wglc.tol_vlos.val=event.value
    'max_zdiff': wglc.max_zdiff.val=event.value
    'min_pix': wglc.min_pix.val=event.value
    'end_cond': wglc.end_cond.val=event.value
    'smooth_val': wglc.smooth_val.val=event.value
    'control':  case event.value of
      'done': widget_control,wglc.base.id,/destroy
      'reset': reset_wglc
    endcase
    else: help,/st,event
  endcase
end

pro loopcond_wg
  common wgl,wgl
  common wglc,wglc
  
  
  if n_elements(wglc) eq 0 then begin
    reset_wglc
  endif
  
  wglc.base.id=widget_base(title='Loop Conditions',/col, $
                           group_leader=wgl.base.id)
  
  wg=['max_badb','max_badv','max_inc','max_azi','tol_btot','max_zdiff', $
      'min_pix','tol_vlos','smooth_val']
  ti=['max number of bad conditions (gradb) to end loop', $
      'max number of bad conditions (gradv) to end loop', $
      'max deviation in inclination between pixels', $
      'max deviation in azimuth between pixels', $
      'tolerance for btot when bad bgrad in Gauss', $
      'max difference for footpoints in height', $
      'minimum number of pixels for loop', $
      'tolerance for variations in VLOS', $
  'median filter for angle, bfield and vlos before doing loop tracing'     ]
  tn=tag_names(wglc)
  for i=0,n_elements(wg)-1 do begin
    idx=(where(tn eq strupcase(wg(i))))(0)
    if idx ne -1 then begin
      wglc.(idx).id=cw_field( wglc.base.id,/all_events,/floating,xsize=8, $
                              uvalue=wg(i),col=2, $
                              title=ti(i),value=wglc.(idx).val)
      
    endif
  endfor
  
  sb=widget_base(wglc.base.id,row=1)
  lb=widget_label(sb,value='End condition:')
  wglc.end_cond.id=cw_bgroup(sb,uvalue='end_cond',row=1, $
                             set_value=wglc.end_cond.val,/exclusive, $
                             ['diff height = 0','diff Btot = 0', $
                              'diff VLOS = 0','loop reversal'])

   
  wglc.control.id=cw_bgroup(wglc.base.id,['Done','Reset to default'], $
                            uvalue='control',row=1, $
                            button_uvalue=['done','reset'])
       
  widget_control,wglc.base.id,/realize
  xmanager,'xwglc',wglc.base.id,no_block=1
end


function fill_elp,loop,color=color
  
  elp=loopst()
  if n_elements(color) ne 0 then elp.col=color
  
  tnelp=tag_names(elp)
  tnloop=tag_names(loop)
  
  retloop=replicate(elp,n_elements(loop))
  for il=0,n_elements(loop)-1 do begin
    for it=0,n_elements(tnelp)-1 do begin
      itl=(where(tnloop eq tnelp(it)))(0)
      if itl ne -1 then begin
        nel=n_elements(loop(il).(itl))
        rlp=retloop(il).(it)
        rlp(0:nel-1)=loop(il).(itl)
        retloop(il).(it)=rlp
      endif
    endfor
    if max(tnloop eq 'N') eq 0 then retloop(il).n=nel
  endfor
  
  return,retloop
end

function concat_loops,loop1,loop2
  
  nlp1=n_elements(loop1)
  nlp2=n_elements(loop2)
  
  retloop=replicate(loopst(),nlp1+nlp2)
  
  rlp1=retloop(0:nlp1-1)
  struct_assign,loop1,rlp1
  retloop(0:nlp1-1)=rlp1

  rlp2=retloop(nlp1:nlp1+nlp2-1)
  struct_assign,loop2,rlp2
  retloop(nlp1:nlp1+nlp2-1)=rlp2

  return,retloop
end

function load_loops,old_loop=old_loop
  common wgl,wgl
  
  sav=dialog_pickfile(filter='sav/*loop*.sav',dialog_parent=wgl.base.id)
  if sav(0) eq '' then return,old_loop
  
  restore,sav
  elp=loopst()
  if n_elements(loop) ne 0 then begin ;my loop-format
    loop=fill_elp(loop,color=9)
  endif else if n_elements(twloop) ne 0 then begin
    loop=fill_elp(twloop,color=1)
  endif else begin
    message,/cont,'unkonown loop format'
    if n_elements(old_loop) ne 0 then loop=old_loop else loop=-1
  endelse
  
  if n_elements(old_loop) ne 0 then begin
    loop=concat_loops(fill_elp(old_loop),loop)
  endif

  loop=remove_double_loops(loop)
  return,loop
end

pro plot_looppar,loop,ps=ps,nr=nr
  common wgl,wgl
  common lpar,lpar
  
  x=loop.x(0:loop.n-1)
  y=loop.y(0:loop.n-1)
  z=loop.z(0:loop.n-1)
  
  r=sqrt((x-x(0))^2+(y-y(0))^2)
  xrg=[min(r),max(r)]
  
  lidx=where(lpar.show eq 1)  
  if min(lidx eq -1) eq 1 then return
  
  lidx=lidx(where(lidx ne -1))
  
  ;fill inc + azi variables for calculated loops
  dx=deriv(loop.x)
  dy=deriv(loop.y)
  dz=deriv(loop.z)
  if total(abs(loop.azi)) eq 0 then $
      loop.azi=(atan(dy,dx)/!dtor)
  if total(abs(loop.inc)) eq 0 then $
      loop.inc=(acos(dz/sqrt(dx^2+dy^2+dz^2))/!dtor)
  if total(abs(loop.dz)) eq 0 then loop.dz=dz
  if total(abs(loop.gradb)) eq 0 then loop.gradb=deriv(loop.btot)
  if total(abs(loop.gradv)) eq 0 then loop.gradv=deriv(loop.vlos)
  
  yrg=lpar.rg
  for i=0,n_elements(lpar)-1 do $
    yrg(*,i)=get_range(median((loop.(lpar(i).tag))(0:loop.n-1),5))
  
  label=lpar.name
                                ;combine azi + inc + inc_solar
  incazi=0
  aziidx=(where(lpar(lidx).name eq 'AZI'))(0)
  incidx=(where(lpar(lidx).name eq 'INC'))(0)
  if incidx ne -1 and aziidx ne -1 then begin
    yrg(*,aziidx)=[min(yrg(0,[aziidx,incidx])), $
                   max(yrg(1,[aziidx,incidx]))]
    incazi=1
    label(aziidx)= $
      add_comma(sep=', ',lpar(lidx([aziidx,incidx])).name)
    lidx=lidx(where(lidx ne incidx))
  endif
  np=n_elements(lidx)
  
  if n_elements(nr) eq 0 then nrstr='' else nrstr=' #'+n2s(fix(nr))
  
  ox=!x & oy=!y
  owin=!d.window
  wd=18. & hg=26.
  
  if keyword_set(ps) then begin
    psf='ps/loop_pars.ps'
    ; psout=ps_widget(default=[0,0,0,0,8,0,0],size=[wd,hg],psname=psn, $
    ;                 file=psf,group_leader=gl)
    psout=ps_wg(size=[wd,hg],psname=psn,file=psf,group_leader=gl,/keep_settings)
    if psout(1) eq 1 then begin
      wset,owin
      return
    endif
    !p.font=1   
    !p.charsize=1.25*!p.charsize
  endif else begin
    win_nr=4
    catch,error
    !p.font=-1
    if error ne 0 then $
      window,win_nr,xsize=500,ysize=500/wd*hg,title='Loop Parameters'
    wset,win_nr
    catch,/cancel
    erase
  endelse
  och=!p.charsize
  
  
  !p.charsize=1.5
  userlct & greek
  
  msk=mask([1,np],nolabel=1,label=0,title='Loop '+nrstr, $
           headtitle='', $
           xformat='(i4)',yformat='(f12.1)', $
           xrange=xrg,yrange=yrg(*,lidx), $
           fix_margins=[0.15,0.96], $
           pos=[0,0,1,1], $
           small_label=0,short_label=0,$
           log_min=1e-3,big_xlabel=1., $
           subtitle='',$        ;ylog=[0,1,0,0], $
           xtitle='!CR, X, Y,Z [pix]', $
           additional_xlabel=[[r],[x],[y],[z]], $
           dytitle=label(lidx))       ;,charsize=!p.charsize)
  
  zeropar=['VLOS','AZI','GRADB','GRADV','Z','DZ']
  for i=0,np-1 do begin
    it=lpar(lidx(i)).tag
    parname=lpar(lidx(i)).name
    
    pp= msk.plot(0,i).position
    dp=[pp(2)-pp(0),pp(3)-pp(1)]
    msk.plot(0,i).lab_position=[pp(0)+.05*dp(0),pp(1)+.6*dp(1), $
                                pp(0)+.5*dp(0),pp(1)]
    msk.plot(0,i).lab_sys='device'
    
    ia_mode=incazi eq 1 and parname eq 'AZI'
    
    if ia_mode eq 0 then lbs='' else lbs=parname
    
    pv=(loop.(it))(0:loop.n-1)
    msk_plot,[[r],[pv]],msk.plot(0,i),/onedim, $
      line_thick=(!d.name eq 'PS')+1,lab_string=lbs,color=1
    
    if ia_mode then begin
      it=(where(lpar.name eq 'INC'))(0)
      lbs=lpar(it).name
      pv=(loop.(lpar(it).tag))(0:loop.n-1)
      msk_plot,[[r],[pv]], $
        msk.plot(0,i),/onedim, $
        line_thick=(!d.name eq 'PS')+1,lab_string=lbs,color=2
    endif
    
    if max(zeropar eq parname) then $
      plots,!x.crange,[0,0],linestyle=1,noclip=0
    
  endfor
  
  if !d.name eq 'PS' then begin
    xyouts,0,0,/normal,'!C!C'+psn,charsize=0.5*!p.charsize
    device,/close
    print,'Created PS-file: '+psn
    if psout(3) eq 1 then  spawn_gv,psn
  endif
  
  !p.charsize=och
  !p.font=-1 & set_plotx
  wset,owin
  !p.t3d=0
  !x=ox & !y=oy
end

pro xwgl_event,event
  common wgl,wgl
  common draw,draw
  common wlsize,wlsize
  common xmap,xmap
  common loop,loop
  common ppar,ppar
  common curr_loop,mindist,minidx,old_minidx
  common wgst
  
  widget_control,event.id,get_uvalue=uval
  sname=tag_names(event,/structure_name)

                                ;resize of base widget
  ps=0
  replot=1
  if sname eq 'WIDGET_BASE' then begin
    dx=event.x-wlsize(0)
    dy=event.y-wlsize(1)
    draw.xs=(draw.xs+dx)>100
    draw.ys=(draw.ys+dy)>100
    widget_control,wgl.draw.id,xsize=draw.xs,ys=draw.ys
    widget_control,wgl.frac.id,ysize=draw.ys
    widget_control,wgl.az.id,xsize=draw.xs
    widget_control,wgl.ax.id,ysize=draw.ys
    widget_control,wgl.base.id,tlb_get_size=wlsize
  endif else case uval of 
    'draw': begin
      replot=0
      if max(tag_names(event) eq 'X') eq 1 then begin     
        widget_control,wgl.draw.id,get_value=widx
        wset,widx
        dev_var,/restore,xdev=event.x,ydev=event.y,xdat=xdat,ydat=ydat
        widget_control,wgl.xy.id, $
          set_value=add_comma(n2s([xdat,ydat],format='(f15.1)'),sep=' | ')
        mindist=1e10
        for il=0,n_elements(loop)-1 do begin
          lpx=pix2mm(loop(il).x(0:loop(il).n-1),x=xmap.p2mm)
          lpy=pix2mm(loop(il).y(0:loop(il).n-1),y=xmap.p2mm)
          dist=min((lpx-xdat)^2+(lpy-ydat)^2)
          if dist lt mindist then begin
            mindist=dist & minidx=il
          endif
        endfor
        if n_elements(loop) eq 0 then begin
          minidx=-1 & old_minidx=-1
        endif
        if n_elements(minidx) ne 0 then begin
          if n_elements(old_minidx) eq 1 then $
            if old_minidx ne -1 and old_minidx lt n_elements(loop) then $
            plot_loop,colpar=wgl.colcode.val,loop=loop(old_minidx), $
            x=loop(old_minidx).x(0:loop(old_minidx).n-1), $
            y=loop(old_minidx).y(0:loop(old_minidx).n-1), $
            z=conv_z(loop(old_minidx).z(0:loop(old_minidx).n-1))
          if minidx ne -1 then begin
            colold=loop(minidx).col
            loop(minidx).col=13
            plot_loop,loop=loop(minidx), $
              x=loop(minidx).x(0:loop(minidx).n-1), $
              y=loop(minidx).y(0:loop(minidx).n-1), $
              z=conv_z(loop(minidx).z(0:loop(minidx).n-1))
            newplot=0
            if n_elements(old_minidx) eq 1 then newplot=old_minidx ne minidx $
            else newplot=1
            if newplot or event.press ne 0 then $
              plot_looppar,loop(minidx),nr=minidx, $
              ps=event.press ne 0 and wgl.click.val eq 2
            old_minidx=minidx
            loop(minidx).col=colold
          endif
        endif
        if event.press eq 1 then begin
          replot=1
          case wgl.click.val of
            0: trace_loop,map=xmap,loop=loop,x=xdat,y=ydat,radius=wgl.rad.val 
            1: delete_loops,loop=loop,x=xdat,y=ydat,radius=wgl.rad.val
            else: replot=0
          endcase
        endif
      endif
    end
    'frac': wgl.frac.val=event.value
    'ax': wgl.ax.val=event.value
    'az': wgl.az.val=event.value
    'loopshow': wgl.loopshow.val=event.select
    'loopscale': wgl.loopscale.val=event.select
    'smoothmap': wgl.smoothmap.val=event.select
    'incsolar': wgl.incsolar.val=event.select
    'zaspect': begin
      wgl.zaspect.val=event.select
      if wgl.zaspect.val eq 1 then if xmap.p2mm ne 1 then begin
        message,/cont,'Correct z-aspect ration only available for maps in' + $
          ' Mm scale. Exit Loop Trace, switch to Mm and start' + $
          ' Loop Trace again.'
        wgl.zaspect.val=0
        widget_control, wgl.zaspect.id,set_value=0
      endif
    end
    'loopcond': loopcond_wg
    'par': ppar(event.value).show=event.select
    'comp': wgl.comp.val=event.index
    'xyproj': wgl.xyproj.val=event.index
    'xzproj': wgl.xzproj.val=event.index
    'yzproj': wgl.yzproj.val=event.index
    'colcode': wgl.colcode.val=event.index
    'qual': wgl.qual.val=event.index
    'loopmesh': begin
      wgl.loopmesh.val=event.index
      replot=0
    end
    'nshow': wgl.nshow.val=event.index
    'click': begin
      wgl.click.val=event.value
      replot=0
    end
    'rad': begin
      wgl.rad.val=event.value
      replot=0
    end
    'control': begin
      case event.value of
        'exit': begin
          replot=0
          widget_control,wgl.base.id,/destroy
        end
        'save': begin
          sfile='./sav/loops.sav'
          save,file=sfile,loop,/compress,/xdr
          print,'Created sav-file: '+sfile
          replot=0
        end
        'load': begin
          loop=load_loops(old_loop=loop)
          if n_tags(loop) eq 1 then dummy=temporary(loop)
        end
        'calcall': begin
          if n_elements(loop) gt 0 then dummy=temporary(loop)
          trace_loop,map=xmap,loop=loop,/all
        end
        'delete': begin
          if n_elements(loop) gt 0 then dummy=temporary(loop)
        end
        'print': begin
          print,'printing'
          ps=1
        end    
      endcase
    end
    else: help,/st,event
  endcase
  
  if replot then draw_loop,map=xmap,loop=loop,ps=ps

end

pro xloop,map=map,loop=loop,x=x,y=y,all=all,radius=radius
  common wgl,wgl
  common draw,draw
  common wlsize,wlsize
  common xmap,xmap
  common ppar,ppar
  common lpar,lpar
  common ncomp,ncomp
  
  sz=size(map.btot)
  if sz(0) eq 2 then ncomp=1 else ncomp=sz(1)
  
  ax=40 & az=70 & frac=30.
  if n_tags(wgl) gt 0 then begin
    widget_control,bad_id=bid,wgl.base.id
    if bid eq 0 then if wgl.base.id ne 0 then $
      widget_control,/destroy,wgl.base.id
    ax=wgl.ax.val & az=wgl.az.val & frac=wgl.frac.val
  endif
  
  reset_wglc
  pparst={show:1,name:'',tag:0,rg:fltarr(2)}
  noshow=['X','Y','P2MM']
  tn=tag_names(map)
  for i=0,n_elements(noshow)-1 do tn=tn(where(tn ne noshow(i)))
  ppar=replicate(pparst,n_elements(tn))
  ppar.name=tn
  for i=0,n_elements(ppar)-1 do $
    ppar(i).tag=where(tag_names(map) eq ppar(i).name)
  
  lparst={show:1,name:'',tag:0,rg:fltarr(2)}
  noshow=['X','Y','N','LEN','IX','IY','COL','SOLAR']
  elp=loopst()
  tn=tag_names(elp)
  for i=0,n_elements(noshow)-1 do tn=tn(where(tn ne noshow(i)))
  lpar=replicate(lparst,n_elements(tn))
  lpar.name=tn
  for i=0,n_elements(lpar)-1 do $
    lpar(i).tag=where(tag_names(elp) eq lpar(i).name)
  
  subst={id:0l,val:0.,str:''}
  wgl={base:subst,control:subst,draw:subst,ax:subst,az:subst,frac:subst, $
       loopshow:subst,par:subst,colcode:subst,click:subst,xy:subst, $
       xyproj:subst,yzproj:subst,xzproj:subst,qual:subst,nshow:subst, $
       rad:subst,loopscale:subst,loopcond:subst,loopmesh:subst,comp:subst, $
       smoothmap:subst,zaspect:subst,incsolar:subst}
  
  wgl.base.id=widget_base(title='Loop Tracer',/col,tlb_size_events=1)
  
  
  sb=widget_base(wgl.base.id,/frame)
  wgl.control.id=cw_bgroup(sb,[' Print ','Load Loops','Save Loops', $
                               'Calc. All Loops','Delete All Loops', $
                               ' Exit '], $
                           uvalue='control',row=1, $
                           button_uvalue=['print','load','save','calcall', $
                                          'delete','exit'])
  
  sb=widget_base(wgl.base.id,/frame,row=1)
  wgl.loopshow.val=1
  wgl.loopshow.id=cw_bgroup(sb,uvalue='loopshow',row=1, $
                            set_value=wgl.loopshow.val,/nonexclusive, $
                            'Show Loops')
  wgl.nshow.id=widget_droplist(sb,uvalue='nshow',/dynamic, $
                               value='Loops: '+['All','50%','25%','10%'])
  wgl.nshow.val=0
  widget_control,wgl.nshow.id,set_droplist_select=wgl.nshow.val
  wgl.qual.id=widget_droplist(sb,uvalue='qual',/dynamic, $
                              value='Image: '+['1:1','nice','normal','fast'])
  wgl.qual.val=2
  widget_control,wgl.qual.id,set_droplist_select=wgl.qual.val
  
  wgl.click.val=0
  wgl.click.id=cw_bgroup(sb,uvalue='click',row=1, $
                         set_value=wgl.click.val,/exclusive, $
                         ['New Loops','Delete Loops','Print Par'])
  
  wgl.rad.val=3.
  wgl.rad.id=cw_field(sb,/all_events,/floating,xsize=8,uvalue='rad', $
                     title='Radius:',value=wgl.rad.val)
  
  sdr=widget_base(wgl.base.id,row=1)
  
  sd=widget_base(sdr,col=1)
  cc=widget_base(sd,/frame,col=1)
  wgl.comp.val=0
  wgl.comp.id=widget_droplist(cc,uvalue='comp',/dynamic, $
                              value=['Comp: '+n2s(indgen(ncomp)+1),'Max FF'])
  widget_control,wgl.comp.id,set_droplist_select=wgl.comp.val
  
  
  cc=widget_base(sd,/frame,col=1)
  lab=widget_label(cc,value='Layers:')
  wgl.par.id=cw_bgroup(cc,col=1,/nonexclusive, $
                       uvalue='par',set_value=ppar.show, $
                       ppar.name)
  
  wgl.colcode.val=n_elements(lpar.name)
  cc=widget_base(sd,/frame,col=1)
  lab=widget_label(cc,value='ColCode:')
  wgl.colcode.id=widget_droplist(cc,uvalue='colcode',/dynamic, $
                                 value=[lpar.name,'black','none'])
  widget_control,wgl.colcode.id,set_droplist_select=wgl.colcode.val
  wgl.xyproj.val=n_elements(lpar.name)+1
  cc=widget_base(sd,/frame,col=1)
  lab=widget_label(cc,value='XY-Proj:')
  wgl.xyproj.id=widget_droplist(cc,uvalue='xyproj', $
                                /dynamic,value=[lpar.name,'black','none'])
  widget_control,wgl.xyproj.id,set_droplist_select=wgl.xyproj.val
  wgl.xzproj.val=where(lpar.name eq 'VLOS')>0
  cc=widget_base(sd,/frame,col=1)
  lab=widget_label(cc,value='XZ-Proj:')
  wgl.xzproj.id=widget_droplist(cc,uvalue='xzproj', $
                                /dynamic,value=[lpar.name,'black','none'])
  widget_control,wgl.xzproj.id,set_droplist_select=wgl.xzproj.val
  wgl.yzproj.val=where(lpar.name eq 'BTOT')>0
  cc=widget_base(sd,/frame,col=1)
  lab=widget_label(cc,value='YZ-Proj:')
  wgl.yzproj.id=widget_droplist(cc,uvalue='yzproj',/dynamic, $
                                value=[lpar.name,'black','none'])
  widget_control,wgl.yzproj.id,set_droplist_select=wgl.yzproj.val
  
  cc=widget_base(sd,/frame,col=1)
   wgl.loopscale.val=1
  wgl.loopscale.id=cw_bgroup(cc,uvalue='loopscale',row=1, $
                            set_value=wgl.loopscale.val,/nonexclusive, $
                            'Scale to Loops')
  wgl.loopcond.id=cw_bgroup(cc,['Loop Conditions'],uvalue='loopcond')
  wgl.loopmesh.id=widget_droplist(cc,uvalue='loopmesh',/dynamic, $
                                  value='Loop-Mesh: '+['0.5','1','2','3','4'])
  wgl.loopmesh.val=2
  widget_control,wgl.loopmesh.id,set_droplist_select=wgl.loopmesh.val
  cc=widget_base(sd,/frame,col=1,space=0)
   wgl.smoothmap.val=0
  wgl.smoothmap.id=cw_bgroup(cc,uvalue='smoothmap',row=1, $
                            set_value=wgl.smoothmap.val,/nonexclusive, $
                            'Smooth Maps')
  wgl.zaspect.id=cw_bgroup(cc,uvalue='zaspect',row=1, $
                           set_value=wgl.zaspect.val,/nonexclusive, $
                           'z-aspect ratio (Mm)')
  wgl.incsolar.id=cw_bgroup(cc,uvalue='incsolar',row=1, $
                           set_value=wgl.incsolar.val,/nonexclusive, $
                           'Use INC_SOLAR for L-Trace')
  widget_control,wgl.incsolar.id,sensitive=0
  
  
  draw={xs:600,ys:600}
  sd=widget_base(sdr,/frame,col=3)
  wgl.frac.val=frac
  wgl.frac.id=widget_slider(sd,uvalue='frac',/vertical,ysize=draw.ys, $
                            min=0,max=100,value=wgl.frac.val,/drag)
  sb=widget_base(sd,col=1)
  wgl.xy.id=widget_label(sb,value='         (    |    )          ')
  wgl.draw.id=widget_draw(sb,xs=draw.xs,ys=draw.ys, $
                          /frame,/button_events,$
                          uvalue='draw',/motion_events,/track,/view)
  wgl.az.val=az
  wgl.az.id=widget_slider(sb,uvalue='az',xsize=draw.xs, $
                          min=0,max=360,value=wgl.az.val,/drag)
  wgl.ax.val=ax
  wgl.ax.id=widget_slider(sd,uvalue='ax',/vertical,ysize=draw.ys, $
                          min=0,max=180,value=wgl.ax.val,/drag)
  
  lmax=500
  
  
  widget_control,/realize,wgl.base.id
  widget_control,wgl.base.id,tlb_get_size=wlsize
  xmanager,'xwgl',wgl.base.id,no_block=1 
  
;  trace_loop,map=map,loop=loop,x=x,y=y,all=all,radius=radius
  xmap=map
  draw_loop,map=map,loop=loop
end
