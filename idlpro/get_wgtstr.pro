pro get_wgtstr,observation=observation,line=line,weight=weight
  common quv_strength,quv_strength,istr,qstr,ustr,vstr,nfree,chi2mode
  
  istr=1. & icnt=0.
  qstr=1. & qcnt=0.
  ustr=1. & ucnt=0.
  vstr=1. & vcnt=0.
  for i=0,n_elements(observation.wl)-1 do begin
    
    if (weight.i(i) gt 1e-4) and finite(observation.i(i)) then begin
      istr=istr+abs(observation.i(i)/line(0).icont-1)
      icnt=icnt+1
    endif
    if (weight.q(i) gt 1e-4) and finite(observation.q(i)) then begin
      qstr=qstr+abs(observation.q(i))
      qcnt=qcnt+1
    endif
    if (weight.u(i) gt 1e-4) and finite(observation.u(i)) then begin
      ustr=ustr+abs(observation.u(i))
      ucnt=ucnt+1
    endif
    if (weight.v(i) gt 1e-4) and finite(observation.v(i)) then begin
      vstr=vstr+abs(observation.v(i))
      vcnt=vcnt+1
    endif
  endfor
  if (icnt gt 0) then istr=((istr-1.)/icnt)>1e-5
  if (qcnt gt 0) then qstr=((qstr-1.)/qcnt)>1e-5
  if (ucnt gt 0) then ustr=((ustr-1.)/ucnt)>1e-5
  if (vcnt gt 0) then vstr=((vstr-1.)/vcnt)>1e-5
  
  quv_strength=total(sqrt(1e-8+observation.q^2+observation.v^2+observation.u^2))

end
