;program to resize pikaia_result structure according to the
;stepx/stepy information in the input file
function resize_pr,pr
  
  nc=n_elements(pr.fit(0).atm)
  sz=size(pr.fit)
  
  nx=sz(1)
  ny=sz(2)
  
  nxnew=nx/pr.input.stepx +1
  nynew=ny/pr.input.stepy +1
  
  prnew=result_struc(nx=nxnew,ny=nynew,ipt=pr.input,header=pr.header)
  
  ixvec=indgen(nxnew)*pr.input.stepx
  iyvec=indgen(nynew)*pr.input.stepy
  
  ixarr=(intarr(nynew)+1) ## ixvec
  iyarr=iyvec ## (intarr(nxnew)+1)
  
  prnew.fit.fitness=pr.fit([ixarr],[iyarr]).fitness
  for it=0,n_tags(pr.fit.atm.par)-1 do begin
    prnew.fit.atm.par.(it)=pr.fit([ixarr],[iyarr]).atm.par.(it)
    prnew.fit.atm.fit.(it)=pr.fit([ixarr],[iyarr]).atm.fit.(it)
  endfor
  
  return,prnew
end
