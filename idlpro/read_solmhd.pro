function read_solmhd,ic=ic,stokes=stokes,label_pol=label,w0=w0,wl=wl, $
                     verbose=verbose
  
  if verbose ge 1 then print,'Reading solar MHD profiles'
  
  
  sz=size(stokes)
  nst=sz(1)
  nwl=n_elements(wl)
  nxp=sz(3)
  nyp=sz(4)
  
  ll=strlowcase(label)
  ii=where(ll eq 'i')
  iq=where(ll eq 'q')
  iu=where(ll eq 'u')
  iv=where(ll eq 'v')
  
                                ;profiles in sav file are normalized.
  ic(*)=1
  
  profile={i:reform(float(stokes(ii,*,*,*))), $
           q:reform(float(stokes(iq,*,*,*))), $
           u:reform(float(stokes(iu,*,*,*))), $
           v:reform(float(stokes(iv,*,*,*))), $
           ic:float(ic), $
           wl:double(w0(0))+wl, $
           x:indgen(nxp),y:indgen(nyp)}
  

  return,temporary(profile)
  
end
