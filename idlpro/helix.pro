;parameters:
;defined in input file (see default.ipt)
;helix,ipt='default.ipt'
;list - (2,n) array containing x/y pixel of ponts to be calculated
;       (overwrites input file xpos / ypos parameter)
;atm_ini - give initital atmosphere as starting condidtion
;delete_sav - delete column sav files from previous runs with this
;             input file
pro helix,ifile,ipt=input_file,list=plist,savall=savall, $
          atm_ini=atm_ini,delete_sav=delete_sav,idl=idl,fortran=fortran, $
          struct_ipt=struct_ipt,result=thisresult,nice=nice, $
          ret_weight=weight,keep_colsav=keep_colsav,x=xpix,y=ypix,ps=ps, $
          fitprof=fitprof,noresult=noresult,force_write=force_write, $
          obsprof=observation,parin=parin,group_leader=group_leader, $
          comp_store=comp_store,compprof=compprof
  common profile,profile
  common pikaia_result,pikaia_result,oldsav,prpar
  common init,init
  common verbose,verbose
  @common_cppar
  @common_maxpar
  @common_localstray
  common ipt_path,ipt_path
  common compiled,compiled
  common proforig,proforig
  common prefiltercom,prefilterval,doprefilter
  common firstrun,firstrun
  common quv_strength,quv_strength,istr,qstr,ustr,vstr,nfree,chi2mode
  common scale,scale
  common atminput,atminput
  
  helixdir=getenv('HELIXDIR')
  if helixdir eq '' then helixdir='./' $
  else if strmid(helixdir,strlen(helixdir)-1,1) ne '/' then $
    helixdir=helixdir+'/'
  
  if n_elements(first_run) eq 0 then begin
    if n_elements(verbose) eq 0 then showver=1 else showver=verbose ge 1
    setenv,'HELIXDIR='+helixdir
    if showver eq 1 then begin
;      read_version,svn_datstr,svn_timestr,svn_revstr
      read_version,gitrevision
      print,'*********************************************************'
      print,'* HeLIx+                 Andreas Lagg (lagg@mps.mpg.de) *'
      print,'* GIT-revision:                 ',gitrevision,' *'
      print,'*********************************************************'   
    endif
  endif
  
  
  if n_elements(profile) ne 0 then proforig=profile

  tlstart=systime(1)
  atm_created=0
  
  !p.font=-1 & !p.charsize=1
  ipt_path=['./input/']
  
  idlso_dir=helixdir+'idl.so/'
  setenv,'LD_LIBRARY_PATH='+idlso_dir
;  PREF_SET, 'IDL_DLM_PATH',idlso_dir+'/:<IDL_DEFAULT>',/COMMIT 

  iprof_only=0
  maxpar
  made_sav=0
  
  delsav=keyword_set(delete_sav)
  if n_elements(compiled) eq 0 then resolve_routine,'pikaia'
  compiled=1
  fitsopen=0
  
  struct_mode=0
  if n_elements(struct_ipt) eq 0 then begin
    if n_params() eq 0 then ifile=-1
;    if max([2,3] eq size(ifile,/type)) ne 1 then itype=-1
    if max([7] eq size(ifile,/type)) eq 1 then ifile=-1
    if n_elements(input_file) eq 0 or ifile gt 0 then begin
                                ;using 5 latest input files    
      for i=n_elements(ipt_path)-1,0,-1 do begin
;        spawn,'ls -tr '+ipt_path(i)+'*.ipt 2>/dev/null | tail -4',lstmp
        fipt=file_info(file_search(ipt_path(i)+'*.ipt',count=nipt))
        if nipt gt 0 then begin
          lstmp=reverse((fipt(reverse(sort(fipt.mtime))).name)(0:4<(nipt-1)))
          if n_elements(lstr) eq 0 then lstr=lstmp else lstr=[lstr,lstmp]
        endif
      endfor
      if n_elements(lstr) ne 0 then begin
        lstr=remove_multi(reverse(lstr),/no_empty)
        nls=n_elements(lstr)
        for i=0,nls-1 do begin
          splt=strsplit(/extract,lstr(i),'/')
          lstr(i)=splt(n_elements(splt)-1)
        endfor
        maxlen=n2s(max(strlen(lstr)))
        if ifile eq -1 then begin
          print,'Latest input-files: '
          for i=0,nls-1 do $
            print,i+1,' ... ',lstr(i),format='(i2,a5,a'+maxlen+')'
          print,'(press key)'
          repeat begin
            key=get_kbrd(1)
          endrep until key ne ''
          fkey=(fix(byte(key))-49)(0)      
        endif else fkey=ifile-1
        if fkey ge 0 and fkey le nls-1 then input_file=lstr(fkey)
      endif
      if n_elements(input_file) eq 0 then begin
        message,/cont,'Usage: helix,ipt=''input_file.ipt'''
        reset
      endif
    endif
    
    
    if n_elements(input_file) eq 0 then input_file='./input/default.ipt'
    ipt=read_ipt(input_file,ipath=ipath,parin=parin)
    if strpos(input_file,'xdisplay.ipt') ne -1 then struct_mode=1
  endif else begin
    ipt=read_ipt(struct=struct_ipt,parin=parin)
    struct_mode=1    
  endelse

                                ;reading input atmosphere (ATM_INPUT keyword)
  read_atminput
  
  if n_elements(xpix) eq 2 then ipt.x=xpix $
  else if n_elements(xpix) eq 1 then ipt.x=[xpix(0),xpix(0)]
  if n_elements(ypix) eq 2 then ipt.y=ypix $
  else if n_elements(ypix) eq 1 then ipt.y=[ypix(0),ypix(0)]
  if n_elements(ps) ne 0 then begin
    ipt.ps=1 & no_x=1
    if size(ps,/type) eq 7 then begin
      psfile=ps
      no_x=0
    endif
  endif
  if keyword_set(ps) then ipt.ps=1
  xy_input=n_elements(xpix) ge 1 and n_elements(ypix) ge 1 
  
  ipt_tag=tag_names(ipt)
  sfp_par=max(ipt_tag eq 'SAVE_FITPROF')
  
  if keyword_set(idl) then ipt.code='IDL'
  if keyword_set(fortran) then ipt.code='FORTRAN'
  verbose=ipt.verbose
  
  
  for i=0,n_tags(ipt.dir)-1 do begin
    if (tag_names(ipt.dir))(i) ne 'ATM_SUFFIX' then begin
      if strmid(ipt.dir.(i),strlen(ipt.dir.(i))-1,1) ne '/' then $
        ipt.dir.(i)=ipt.dir.(i)+'/'
      check_dirs,ipt.dir.(i)    ;check if directory structure is okay
    endif
  endfor
  
  
  all_lines=ipt.line(0:ipt.nline-1)

  total_atm=ipt.atm(0:ipt.ncomp-1)
  n_atm=fix(ipt.ncomp)
  scale=ipt.scale(0:ipt.ncomp-1)
  predef=check_scaling(ipt=ipt,/predef)
  obs_par=(ipt.obs_par)(0)

  physical_constants            ;define constants (!c_light)
  
  check_atm,total_atm        ;perform consistency checks on atmosphere
  check_line,all_lines       ;perform consistency checks on lines
  
  synth_mode=ipt.synth 
  if synth_mode then if ipt.fitsout then force_write=1
;-------------------------------------- -> read_observation  
  
                                ;get prefix for output filenames
  rsplit=strsplit(/extract,ipt.file,'/')
  red_nopath=rsplit(n_elements(rsplit)-1)
  ipos=strpos(red_nopath,'.ipt')
  if ipos ne -1 then red_nopath=strmid(red_nopath,0,ipos)
  if ipt.observation ne '' and synth_mode ne 1 then $
    obs='.'+ipt.observation else obs=''
  filestr=red_nopath+obs+'.pikaia'  
  
                                ;loop for applying analysis to image
  if verbose ge 1 then begin
    print,'Map Scan x: '+add_comma(n2s([ipt.x(0),ipt.x(1),ipt.stepx]),sep=',')
    print,'Map Scan y: '+add_comma(n2s([ipt.y(0),ipt.y(1),ipt.stepy]),sep=',')
  endif
  
  
  
  tstart=systime(1)
  
  if n_elements(plist) ne 0 then begin
                                ;use pixels given in a vector of size
                                ;(2,n) containing x and y position of pixels
                                ;to be calculated
                                ;sort plist according to x-pos (column)
    szl=size(plist)
    if szl(1) ne 2 then message,'list has to be a vector of size (2,n)'
    srt=sort(plist(0,*))
    plist(0,*)=plist(0,srt)
    plist(1,*)=plist(1,srt)
    if verbose ge 1 then begin
      print,'Use '+n2s(n_elements(srt))+' profiles from list.'
    endif
  endif else begin       
    plist=get_proflist(ipt,nolist=xy_input eq 1)
  endelse
  nlst=n_elements(plist)/2

  n_ini=n_elements(atm_ini)
  if n_ini gt 0 then begin
    n_atm_ini=fix(n_ini/nlst)
    if n_atm_ini ne float(n_ini)/nlst then  begin
      
;     szini=size(atm_ini)
;     if szini(0) eq 1 then n_atm_ini=1 else n_atm_ini=szini(1)
;     n_ini=szini(szini(0))
      
;    if n_atm_ini ne n_atm then begin
      message,/cont,'number of initial components <> actual component. Use only first component of initial atmosphere'
      atm_ini.fit.ff=1     
;      atm_ini.par.ff=1./n_atm
      atm_ini.par.ff=norm_ff(atm_ini.par.ff,atm_ini.fit.ff,atm_ini.linid)
    endif
  endif
  
                                ;check for old (3-digit) or new
                                ;directory structure
  pfmt='(i4.4)'
  dummy=file_search(ipt.dir.profile+'/'+ipt.observation+'/x????',count=cnt4)
  dummy=file_search(ipt.dir.profile+'/'+ipt.observation+'/x???',count=cnt3)
  if cnt4 eq 0 and cnt3 ne 0 then pfmt='(i3.3)'
  
                                ;calculate number of computation boxes
                                ;according to piklm_bx/piklm_by
  xmm=[min(plist(0,*)),max(plist(0,*))]
  ymm=[min(plist(1,*)),max(plist(1,*))]
  nbx=(xmm(1)-xmm(0))/ipt.piklm_bx+1
  nby=(ymm(1)-ymm(0))/ipt.piklm_by+1
  
  chi2mode=ipt.chi2mode
  
  pixelrep=ipt.pixelrep>1
;  for ipix=0l,n_elements(plist(0,*))-1 do begin
  ipixcnt=0l
;====================================================================  
  
                                ;create / open fits file
  if ipt.fitsout eq 1 and keyword_set(noresult) eq 0 then begin
    if strcompress(ipt.observation) ne '' then begin
      atm_path=ipt.dir.atm+'atm_'+ipt.observation 
      prof_path=ipt.dir.atm+'prof_'+ipt.observation
    endif else begin
      ifp=get_filepath(ipt.file)
      atm_path=ipt.dir.atm+'atm_'+ifp.base
      prof_path=ipt.dir.atm+'prof_'+ifp.base
    endelse
    if strmid(atm_path,strlen(atm_path)-1,1) eq '/' then $
      atm_path=strmid(atm_path,0,strlen(atm_path)-1)
    if strmid(prof_path,strlen(prof_path)-1,1) eq '/' then $
      prof_path=strmid(prof_path,0,strlen(prof_path)-1)
                                ;remofe .fits before adding suffix
    if strmid(atm_path,strlen(atm_path)-5,5) eq '.fits' then $
      atm_path=strmid(atm_path,0,strlen(atm_path)-5)
    if strmid(prof_path,strlen(prof_path)-5,5) eq '.fits' then $
      prof_path=strmid(prof_path,0,strlen(prof_path)-5)
    if strcompress(ipt.dir.atm_suffix, $
                   /remove_all) ne '' then begin
      atm_path=atm_path+'_'+ipt.dir.atm_suffix
      prof_path=prof_path+'_'+ipt.dir.atm_suffix
    endif
    if strmid(atm_path,strlen(atm_path)-5,5) ne '.fits' then $
      atm_path=atm_path+'.fits'
    if strmid(prof_path,strlen(prof_path)-5,5) ne '.fits' then $
      prof_path=prof_path+'.fits'
    fitsout

    fitsout_open,atm_path,prof_path,xmm,ymm,ipt
  endif
  
;  for iby=0,nby-1 do for ibx=0,nbx-1 do begin ;mainloop (x/y pixels)
                                ;find profiles in [piklm_bx,piklm_by]
;    plboxx=[ibx,ibx+1]*ipt.piklm_bx+xmm(0)
;    plboxy=[iby,iby+1]*ipt.piklm_by+ymm(0)
  piktry=0
  for ibb=0l,n_elements(plist(0,*))-1 do begin
;    plboxx=[plist(0,ibb),plist(0,ibb)+1]*ipt.piklm_bx+xmm(0)
;    plboxy=[plist(1,ibb),plist(1,ibb)+1]*ipt.piklm_by+ymm(0)
    
    
;    inbox=where(plist(0,*) ge plboxx(0) and plist(0,*) lt plboxx(1) and $
;                plist(1,*) ge plboxy(0) and plist(1,*) lt plboxy(1))
;    if inbox(0) eq -1 then icnt=0 else icnt=n_elements(inbox)
;stop    
    iptorig=ipt
    fitness_old=0.
;   ii=0
;    while ii lt icnt do begin   ;inbox loop
;    for ii=0,icnt-1 do begin 
      
;      ipixcnt=ipixcnt+1
;      xp=plist(0,inbox(ii))
;      yp=plist(1,inbox(ii))
      xp=plist(0,ibb)
      yp=plist(1,ibb)
;    for xp=ipt.x(0),ipt.x(1),ipt.stepx do begin
;    for yp=ipt.y(0),ipt.y(1),ipt.stepy do begin
      
      if xp ne -1 and yp ne -1 then $
        for ipr=1,pixelrep do begin ;loop for pixel repetitions
        
                                ;define method (piklm-mode)
        dols=fix(ipt.localstray_rad ge 1e-5)
        method=fix(ipt.method)
        
        piklmfirst=((xp mod ipt.piklm_bx) eq 0 and $
                    (yp mod ipt.piklm_by) eq 0)
        if method(0) eq 3 then begin
          if piklmfirst then begin
            if ipt.verbose ge 2 then print,'First pixel: PIKAIA'
            method(*)=0
            iptbest=ipt
            piktry=0
          endif else begin
            if piktry eq 1 then begin
              if ipt.verbose ge 2 then $
                print,'PIKAIA-retry (bad-fitness for LMDIFF)'
              method(*)=0
            endif else begin
              if ipt.verbose ge 2 then $
                print,'LMDIFF'
              method(*)=2
            endelse
            ipt=iptbest
          endelse
        endif
        
                                ;get initial condition / sythesis info from ATMINPUT keyword.
;        ipt=atminput_fill(xp-ipt.x[0],yp-ipt.y[0])
        ipt=atminput_fill(xp,yp)
        
                                ;use initial value as provided with keyword
                                ;atm_ini
        gen=ipt.gen
        blend=ipt.blend(0:ipt.nblend-1)
        total_atm=ipt.atm(0:ipt.ncomp-1)
        scale=ipt.scale(0:ipt.ncomp-1)
        if ibb lt n_ini then begin      
          for ia=0,n_atm-1 do begin
            tmpa=total_atm(ia)
            if n_atm_ini eq 1 then struct_assign,atm_ini(ibb),tmpa $
            else struct_assign,atm_ini(ia,ibb),tmpa
            total_atm(ia)=tmpa
          endfor
          if ipt.verbose ge 1 then begin
            print,'Initial Atmosphere: '
            print_atm,total_atm,scale=scale,line=ipt.line(0:ipt.nline-1), $
              blend=ipt.blend(0:ipt.nblend-1),gen=ipt.gen
          endif
        endif

                                ;test filling factors
        if total(total_atm.par.ff) eq 0 then total_atm(0).par.ff=1.
        total_atm.par.ff=norm_ff(total_atm.par.ff,total_atm.fit.ff,total_atm.linid)
;     if abs(total(total_atm.par.ff)-1) gt 1e-4 then $
;       total_atm.par.ff=total_atm.par.ff/total(total_atm.par.ff)
        
        old_totalatm=total_atm

        
                                ;calculate array use: determines if
                                ;line / atmosphere is used for calculations
                                ;flag if a atmosphere should be used for a line
        natm=fix(n_atm) & nline=fix(ipt.nline)
        use=intarr(nline,natm)
        for il=0,ipt.nline-1 do for ia=0,n_atm-1 do $
          use(il,ia)=max(id2s(total_atm(ia).use_line) eq $
                         id2s(all_lines(il).id))
        
                                ;use calculated profile as observation
;print,synth_mode,'HIER'   & synth_mode=1      
        if synth_mode eq 1 then begin
                                ;define no of eavelength points with
                                ;approc. WL resolution of 30 mA:
;      nwl=((fix((ipt.wl_range(1)-ipt.wl_range(0))/0.01))>50)<(maxwl)
                                ;use info from input file
          magopt=fix(ipt.magopt(0))
          hanle_azi=ipt.hanle_azi
          norm_stokes_val=ipt.norm_Stokes_val
          old_norm=fix(ipt.old_norm)
          old_voigt=fix(ipt.old_voigt)
          use_geff=fix(ipt.use_geff(0))
          use_pb=fix(ipt.use_pb(0))
          pb_method=fix(ipt.pb_method(0))
          
                                ;set straypol-amp & width (important
                                ;for creating a synthetic spectrum
                                ;with straypol)
          if ipt.straypol_amp ne 0 then begin
            all_lines.straypol_par(0:ipt.ncomp-1).width= $
              ipt.atm(0:ipt.ncomp-1).par.width
            all_lines.straypol_par(0:ipt.ncomp-1).vlos= $
              ipt.atm(0:ipt.ncomp-1).par.vlos
            case ipt.modeval of
              1: all_lines.straypol_par(0:ipt.ncomp-1).damp= $
                ipt.atm(0:ipt.ncomp-1).par.damp
              2: all_lines.straypol_par(0:ipt.ncomp-1).damp= $
                ipt.atm(0:ipt.ncomp-1).par.gdamp
              3: all_lines.straypol_par(0:ipt.ncomp-1).damp= $
                ipt.atm(0:ipt.ncomp-1).par.gdamp
              4: all_lines.straypol_par(0:ipt.ncomp-1).damp= $
                ipt.atm(0:ipt.ncomp-1).par.gdamp
              5: begin
                print,'No straylight correction for hanle-slab mode.'
                reset
              end
              else: begin
                print,'Unknown modeval: ',ipt.modeval
                reset
              end
            endcase
            all_lines.straypol_par(0:ipt.ncomp-1).dopp= $
              ipt.atm(0:ipt.ncomp-1).par.dopp
;        for ia=0,n_atm-1 do begin
            all_lines.par.straypol_amp=ipt.straypol_amp ;0.0015 ;*all_lines.f
            all_lines.par.straypol_eta0=ipt.straypol_amp ;use same amplitude
                                ;value for voigt and gauss straypol synthesis
;        endfor
          endif
          
          old_norm=ipt.old_norm ;flag for old normalization
          old_voigt=ipt.old_voigt ;flag for old normalization
          voigt=fix(ipt.profile eq 'voigt')
          modeval=fix(ipt.modeval)
          
          nwl=ipt.wl_num
          wlvec=findgen(nwl)/(nwl-1)* $
            (ipt.wl_range(1)-ipt.wl_range(0))+ipt.wl_range(0)
          conv=read_conv(ipt.wl_range,ipt.conv_func,ipt.conv_nwl, $
                         ipt.conv_wljitter,ipt.conv_mode, $
                         ipt.prefilter,wlvec,ipt.verbose)
          cnwl=conv.nwl
          if cnwl ge 1 then begin
            convval=conv.val(0:cnwl-1)
            compwl=conv.wl(0:cnwl-1)
          endif else convval=-1

                                ;prefilter_wlerr set to 0.
          prefilter=read_prefilter(ipt.prefilter,0.0, $
                                   wlvec,ipt.verbose)
          prefilterval=prefilter.val
          doprefilter=prefilter.doprefilter

        
                                ;use new call_cp routine
                                ;(automatically does fortran/IDL
                                ;switch)
          synthprof=call_cp(ipt=ipt,line=all_lines,atm=total_atm, $
                            blend=blend,gen=gen, $
                            obs_par=obs_par,wl=wlvec,conv=conv, $
                            prefilterval=prefilterval, $
                            doprefilter=doprefilter,dols=dols, $
                            ret_wlidx=0,nret_wlidx=-1)
          observation=synthprof
          if ipt.ic_level ge 1e-5 then begin
            observation.ic=ipt.ic_level
            observation.i=observation.i*observation.ic
            icont=ipt.ic_level
          endif else icont=all_lines(0).icont
          if ipt.verbose ge 1 then begin
            print,'======== Synthetic Profile for atmosphere: ' + $
              '==================='
            print_atm,total_atm,show=ipt.parset, $
              scale=ipt.scale(0:ipt.ncomp-1), $
              blend=ipt.blend(0:ipt.nblend-1),gen=ipt.gen
            print,'============================================' + $
              '=================='
          endif
                                ;do not allow for straypol-amplitude
                                ;if straypol._corr is not set, even if
                                ;the synthesis was done with straypol
          if ipt.straypol_corr eq 0 then begin
            all_lines.par.straypol_amp=0
            all_lines.par.straypol_eta0=0
          endif
          spinor_mode=0
          fits_mode=0
          hinode_mode=0
          imax_mode=0
          fits4d_mode=0
                                ;use spinor format
        endif else begin
          observation= $
            read_observation(ipt,spinor_mode=spinor_mode,fits_mode=fits_mode, $
                             hinode_mode=hinode_mode,imax_mode=imax_mode, $
                             fits4d_mode=fits4d_mode,$
                             x=xp,y=yp,icont=icont,error=thiserror)
        endelse
        
                                ;get some pixel string info
        get_profidx,x=xp,y=yp,stepx=ipt.stepx,stepy=ipt.stepy, $
          rgx=ipt.x,rgy=ipt.y, $
          vecx=xpvec,vecy=ypvec,average=ipt.average,scansize=ipt.scansize
        nxp=n_elements(xpvec) & nyp=n_elements(ypvec)
        xpmm=remove_multi([min(xpvec),max(xpvec)])
        ypmm=remove_multi([min(ypvec),max(ypvec)])
        xpstr=add_comma(n2s(xpmm,format=pfmt),sep='-')
        ypstr=add_comma(n2s(ypmm,format=pfmt),sep='-')
;-------------------------------------------------------  
        if n_elements(thiserror) eq 0 then thiserror=0
        if thiserror eq 0 then begin ;profile read error
          
                                ;add artificial noise according to
                                ;input file
        observation=add_noise(observation,noise=ipt.noise,inoise=ipt.inoise)
          
;    print,observation.ic,observation.i(0:4)
        if ipt.verbose ge 2 and ipt.display_profile ge 2 then $
          plot_profiles,observation,title='Input Profile',win_nr=10
                                ;adjust continuum level!
        
        all_lines.icont=icont
        
;reset straypol parameters - start with 0 for fit    
;        all_lines.par.straypol_amp=0.
;        all_lines.par.straypol_eta0=0.

                                ;loop for multi-iteration
        if max(tag_names(ipt) eq 'NMI') eq 1 then nmi=ipt.nmi else nmi=1
        
        t=systime(1)
        for im1=0,nmi-1 do begin
          
                                ;reread scaling (scaling is changed for
                                ;straypol-run)
          scale=ipt.scale(0:ipt.ncomp-1)
          dols=fix(ipt.localstray_rad ge 1e-5)
          
          if n_elements(method_str) ne 0 then dummy=temporary(method_str)
          
          if ipt.profile eq 'voigt' then voigt=1 else voigt=0
          modeval=ipt.modeval
                                ;for strapol-correction: Do Gaussfit
                                ;to I only before starting iterations
                                ;start with -1: gauss fit to I only

          straypol_run=0
          iprof_only=0
          if ipt.straypol_corr gt 0 then begin
            if im1 eq 0 then begin
              im=nmi-1
              iprof_only=1
              straypol_run=1
;          voigt=0
;          modeval=0
              tg=tag_names(total_atm.par)
                                ;set all non-Gauss Par to no-fit
              for it=0,n_elements(tg)-1 do $
                if max(ipt.parset eq tg(it)) eq 0 then begin
                total_atm.fit.(it)=0
              endif
              total_atm.fit.b=0 & total_atm.par.b=0
              total_atm.fit.inc=0 & total_atm.par.inc=0
              total_atm.fit.azi=0 & total_atm.par.azi=0
;          total_atm.fit.sgrad=0 & total_atm.par.sgrad=1
              
                                ;use same range & fit settings as for
                                ;first multi-iteration run. This is
                                ;defined in read_ipt (*)
              for il=0,ipt.nline-1 do $
                if (all_lines(il).straypol_use eq 1) then begin
                case modeval of
                  0: begin      ;gauss 
                    all_lines(il).fit.straypol_amp=1
                  end
                  1: begin      ;voigt 
                    all_lines(il).fit.straypol_eta0=2
                  end
                  2: begin      ;voigt-phys
                    message,'voigt-physical mode not implemented for' + $
                      ' straylight-correction.'
                  end
                  3: begin      ;voigt-gdamp
                    all_lines(il).fit.straypol_eta0=2
                  end
                  4: begin      ;voigt-szero
                    all_lines(il).fit.straypol_eta0=2
                  end
                  5: begin      ;hanle-slab
                    message,'hanle-slab mode no straylight correction'
                  end
                  else: message,'unknown modeval.'
                endcase
              endif
              
              cmax=0
              for it=0,n_tags(ipt.atm(0).fit)-1 do $
                cmax=max(abs(ipt.atm.fit.(it)))>cmax
              
              
                                ;couple 2 blended he-lines
              astr=string(all_lines.id)
              he3=where(astr eq 'He1 10830.34')
              he2=where(astr eq 'He1 10830.25')
              he1=where(astr eq 'He1 10829.09')
              if he3(0) ne -1 and he2(0) ne -1 then begin
;            all_lines(he3).fit.straypol_amp=all_lines(he2).fit.straypol_amp
;            all_lines(he3).fit.straypol_eta0=all_lines(he2).fit.straypol_eta0
                case (ipt.modeval) of
                  0: begin      ;GAUSS
                    all_lines(he1).fit.straypol_amp=-cmax-1
                    all_lines(he2).fit.straypol_amp=-cmax-2
                    all_lines(he3).fit.straypol_amp=-cmax-2
                  end 
                  1: begin      ;VOIGT
                    all_lines(he1).fit.straypol_eta0=-cmax-1
                    all_lines(he2).fit.straypol_eta0=-cmax-2
                    all_lines(he3).fit.straypol_eta0=-cmax-2
                  end 
                  3: begin      ;VOIGT-GDAMP
                    all_lines(he1).fit.straypol_eta0=-cmax-1
                    all_lines(he2).fit.straypol_eta0=-cmax-2
                    all_lines(he3).fit.straypol_eta0=-cmax-2
                  end 
                  4: begin      ;VOIGT-SZERO
                    all_lines(he1).fit.straypol_eta0=-cmax-1
                    all_lines(he2).fit.straypol_eta0=-cmax-2
                    all_lines(he3).fit.straypol_eta0=-cmax-2
                  end 
                  5: begin      ;hanle-slab
                    print,'unknown mode for straylight-correction:',ipt.modeval
                    reset
                  end
                  else:
                endcase 
                cmax=cmax+2
              endif
            endif else begin
              if im1 eq 1 then total_atm=old_totalatm
              im=im1-1
            endelse 
          endif else im=im1

                                ;define weights (wl-dependent and
                                ;iquv dependent)
                                ;weight only I for pre-Gauss
                                ;define weighting
;(old function without wgt-file)    
;  get_wgt,obs=observation,line=all_lines,wgti=wgt,wgt_mag=wgt_mag,icont=icont
; (For old weighting function)      
;       weight={i:wgt * ipt.iquv_weight(0,im), $
;             q:wgt_mag * ipt.iquv_weight(1,im), $
;             u:wgt_mag * ipt.iquv_weight(2,im), $
;             v:wgt_mag * ipt.iquv_weight(3,im) } 
;new (using wgt-file)      
          get_wgt,obs=observation,wgt=wgt,im=im
          weight={i:wgt.i, $
                  q:wgt.q, $
                  u:wgt.u, $
                  v:wgt.v, $
                  wl:wgt.wl}
;----------- end of weighting part -----------------------------
          
          if im gt 0 and straypol_run eq 0 then begin
                                ;define new input atmosphere and set new
                                ;inversion parameters
            
                                ;set new initial values
            total_atm=atm
            
                                ;set new fit flag & new scaling
            tn=tag_names(total_atm(0).par)
            for ic=0,n_atm-1 do for it=0,n_elements(tn)-1 do begin
              total_atm(ic).fit.(it)=ipt.mi(ic).(it).fit(im-1)
              
              oldsc=[scale(ic).(it).min,scale(ic).(it).max]
              val= total_atm(ic).par.(it)
              newsc=val + ipt.mi(ic).(it).perc_rg(im-1)/100.* $
                [-1,1]*(max(oldsc)-min(oldsc))
              scale(ic).(it).min=newsc(0)>oldsc(0)
              scale(ic).(it).max=newsc(1)<oldsc(1)
            endfor
          endif
          
                                ;check if magnetic signal is below MIN_QUV
          if ipt.min_quv ge 1e-6 then begin
            wg2=(wgt.v^2+wgt.u^2+wgt.q^2)
            nn=where(wg2 gt 0)
            if nn(0) ne -1 then begin
              quv=determ_quv(observation,index=nn)
              if verbose ge 2 then $
                print,'QUV-Strength: '+n2s(quv,format='(e15.2)')
              if quv lt ipt.min_quv then begin
                total_atm.par.b=0
                total_atm.par.inc=0
                total_atm.par.azi=0
                total_atm.fit.b=0
                total_atm.fit.inc=0
                total_atm.fit.azi=0
                if verbose ge 1 then $
                  print,'QUV-Strength ('+n2s(quv,format='(e15.2)')+ $
                  ') is less than MIN_QUV. Fit is done with B=0.'
              endif
            endif
          endif
          
          
          magopt=fix(ipt.magopt(im))      
          hanle_azi=ipt.hanle_azi
          norm_stokes_val=ipt.norm_Stokes_val
          old_norm=fix(ipt.old_norm)      
          old_voigt=fix(ipt.old_voigt)      
          use_geff=fix(ipt.use_geff(im))      
          use_pb=fix(ipt.use_pb(im))      
          pb_method=fix(ipt.pb_method(im))      
          stray=(total_atm(0:n_atm-1).par.b eq 0 and $
                 total_atm(0:n_atm-1).fit.b eq 0)
          stray_idx=where(stray eq 1)
          nostray_idx=where(stray eq 0)
          
                                ;do not use unno approximation for
                                ;straylight component
          if ipt.approx_dir(im) or ipt.approx_azi(im) then begin
            if total(stray) eq ipt.ncomp then begin
              nostray_idx=indgen(n_atm)
              message,/cont, $
                'Approximation for angles not for straylight comp.' + $
                ' Setting approx_dir to zero.'
              ipt.approx_dir(im)=0
              ipt.approx_azi(im)=0
            endif
          endif
          
          if nostray_idx(0) ne -1 then $
            if (ipt.approx_dir(im) or ipt.approx_azi(im)) and $
            ipt.approx_dev(im) eq 0 then begin
                                ;do not fit Q and U profiles to obtain
                                ;azimuthal information, use
                                ;approximation only;
                                ;use angle approximation also for getting
                                ;inlcination angle. Since only the
                                ;V-profile is fitted (not q and u),
                                ;the cos(inc) enters the equation only
                                ;as a factor of V and therefore is
                                ;redundant to the filling factor of
                                ;this component. Therefore we do not
                                ;fit the inclination angle and
                                ;determine it after the pikaia routine
                                ;by applying the angle approximation
            total_atm(nostray_idx).fit.azi=0
            if ipt.approx_dir(im) then total_atm(nostray_idx).fit.inc=0
          endif

                                ;do azi/inc fit seperately using approximation
                                ;for angles
          
          if (ipt.approx_dir(im) or ipt.approx_azi(im)) $
            and synth_mode eq 0  then begin
            common for_amoeba,u_signal,q_signal,quv_weight
            mag_weight=weight.v ;do use standard mag-weighting for unno
                                ;approximation, ignore iquv_weight keyword
            
                                ;calculate unno approximation where weight
                                ;for magnetic components is large
                                ;enough and only in +-(rg_el) A range around
                                ;dominant line (largest log_gf)
            rg_wl=0.5
            dummy=max(all_lines.f) & idom=!c
            dummy=min(abs(observation.wl-all_lines(idom).wl)) & wl_idom=!c
                                ;check for min in I to reflect
                                ;velocity shift
            dummy=max((icont-observation.i)* $
                      (observation.wl ge (all_lines(idom).wl-0.5) and $
                       observation.wl le (all_lines(idom).wl+0.5) ))
            wlv_idom=!c
            
            ui=where(mag_weight gt 0.5*max(mag_weight) and $
                     (observation.wl ge observation.wl(wlv_idom)-rg_wl and $
                      observation.wl le observation.wl(wlv_idom)+rg_wl))
            if ui(0) eq -1 then ui=wlv_idom
            unno_wl=observation.wl(ui)
            
            quv_weight=wgt_unno(ui) ;unno approximation (see Auer, SolPhys 1977)
            
            u_signal=observation.u(ui)
            q_signal=observation.q(ui)
            alpha2=amoeba(1.0d-5,scale=0.01d,p0=1d,function_value=diff, $
                          function_name='fit_azi_amoeba',ncalls=n_amoeba,nmax=100)
            azi=(alpha2(0)/2./!dtor mod 360)
                                ;sign at center: use sign of u at
                                ;center of line to eliminate ambiguity
            dummy=min(observation.i(ui)) & icntr=!c
            cidx=[icntr-2>0,(icntr+2)<(n_elements(ui)-1)]
;        nci=n_elements(cidx)
;        q_sign_center= $
;          (float(total(q_signal(cidx(0):cidx(1)) gt 0))/nci gt 0.)*2-1
;        u_sign_center= $
;          (float(total(u_signal(cidx(0):cidx(1)) gt 0))/nci gt 0.)*2-1        
;         if u_sign_center eq 1 then azi=-90+azi
;         print,q_signal(cidx(0):cidx(1)),q_sign_center
;         print,u_signal(cidx(0):cidx(1)),u_sign_center        
            
                                ;cyclic azi (-90 = +90)
            azi=(azi+270) mod 180 -90
                                ;dominant signs (not used)
;        dom_sign_q=(float(total(q_signal gt 0))/n_elements(ui) gt 0.5)*2-1
;        dom_sign_u=(float(total(u_signal gt 0))/n_elements(ui) gt 0.5)*2-1
;print,azi,dom_sign_q,dom_sign_u,q_sign_center,u_sign_center & stop
                                ;set azimuthal angle for all
                                ;components of the atmosphere to the
                                ;fitted value
            total_atm(nostray_idx).par.azi=azi
            
            
                                ;calculate inclination using unno
                                ;approximation:
                                ;use s1_hat and s3_hat from Auer (1977
                                ;SolPhys), do not need to divide by
                                ;S0_cont-S0 because only ration of
                                ;S1/S3 is used. This relation is only
                                ;valid in wings of line!
            common for_amoeba_inc,s1_hat,s3_hat,s13_weight
            s13_weight=quv_weight
            
            s3_hat=observation.v(ui)*1e3
            s1_hat=(observation.q(ui)*cos(2*azi*!dtor) + $
                    observation.u(ui)*sin(2*azi*!dtor))*1e3       
                                ;best if used in line wings, not in
                                ;line center. Therfore: instead of
                                ;using weighting used for pikaia (line
                                ;center of I is weighted highest) we
                                ;divide by the I-profile to get the
                                ;wings weighted higher.
            
            mnv=min(abs(s3_hat)) & mxv=max(abs(s3_hat)) ;weight according to
                                ;v-profile
            s13_weight=(abs(s3_hat)-mnv)/(mxv-mnv)*0.8+0.2
                                ;set weight to zero where absorption
                                ;is largest (+- npix pixels)
            npix=1
            s13_weight((icntr-npix)>0:(icntr+npix)<(n_elements(ui)-1))=0.
            
            inc=amoeba(1.0e-6,scale=.01,p0=1.,function_value=diff, $
                       function_name='fit_inc_amoeba',ncalls=n_amoeba,nmax=100)
                                ;depending on the sign of i_plus and
                                ;i_minus (pi-components of V-signal)
                                ;the inclination angle is in the range
                                ;[0,90] or [90,180].
            inc=inc(0)/!dtor
            inc=abs(inc) mod 180
            
;         wset,1 & !p.multi=0
;         R=2*s1_hat/s3_hat
;         ninc=acos(0.5*(sqrt(R^2+4)-abs(R)))/!dtor
; ;        print,(sin(ninc*!dtor)^2/2/cos(ninc*!dtor)),s1_hat/s3_hat,ninc
;         plot,observation.wl(ui),abs(ninc),charsize=1 
;         wset,0
;         stop       
            
            dummy=max(mag_weight)  & iwmax=!c
            sign_lo=(total(observation.v(0:iwmax)*mag_weight(0:iwmax)) ge 0)*2-1
            sign_hi=(total(observation.v(iwmax:*)*mag_weight(iwmax:*)) ge 0)*2-1
            if sign_lo lt 0 and sign_hi gt 0 then inc=180-inc
            
;      plot,observation.wl,mag_weight & oplot,color=1,observation.wl(ui),mag_weight(ui) & oplot,color=2,observation.wl(ui),quv_weight & oplot,color=3,observation.wl(ui),s13_weight & print,azi,inc
;      plot,atan(u_signal/q_signal)/!dtor/2
;stop      
            
                                ;set inclination angle for all
                                ;components of the atmosphere to the
                                ;fitted value        
            total_atm(nostray_idx).par.inc=inc
            
            approx_flag=1 
            if ipt.verbose gt 0 then $
              print,'Approximation for direction: inc='+n2s(inc)+' , azi='+n2s(azi)
          endif else approx_flag=0
          
          if approx_flag and ipt.approx_dev(im) and synth_mode eq 0 then begin
                                ;set scaling for azi and inc
                                ;according to Unno approximation
            
                                ;allow pikaia solution for azi to
                                ;deviate rg_azi degreesfrom unno approximation
            rg_azi=ipt.approx_dev_azi(im)
                                ;allow pikaia solution for inc to deviate
                                ;rg_inc degreesfrom unno approximation
            if ipt.approx_azi(im) then rg_inc=180 $
            else rg_inc=ipt.approx_dev_inc(im)
            
            inc_rg=((inc+[-1,1]*rg_inc)>0)<180
            azi_rg=((azi+[-1,1]*rg_azi)>(-90))<90
            if verbose gt 0 then $
              print,'Scale using approximation for direction: ' + $
              add_comma(n2s(inc_rg,format='(f15.1)'),sep=' < inc < ')+'  ' + $
              add_comma(n2s(azi_rg,format='(f15.1)'),sep=' < azi < ')     
            
                                ;do not apply for straylight (0th)
                                ;component
            scale(nostray_idx).azi.min=azi_rg(0)
            scale(nostray_idx).azi.max=azi_rg(1)
            scale(nostray_idx).inc.min=inc_rg(0)
            scale(nostray_idx).inc.max=inc_rg(1)
          endif
          
                                ;fill idx-variable with indices for pikaia
                                ;par-variable
          total_atm=get_pikaia_idx(total_atm)
          
                                ;check if scaling for coupled
                                ;parameters is equal
          for ia=1,natm-1 do for it=0,n_tags(total_atm(ia).idx)-1 do begin
            if total_atm(ia).idx.(it) eq total_atm(ia-1).idx.(it) and $
              total_atm(ia).idx.(it) ne 0 and $
              total_atm(ia-1).idx.(it) ne 0 then begin
              if (scale(ia).(it).min ne scale(ia-1).(it).min or $
                  scale(ia).(it).max ne scale(ia-1).(it).max) then begin
                if verbose ge 1 then begin 
                  print,'Scaling of coupled param. '+ $
                    (tag_names(total_atm(ia).idx))(it)+' is different - '+$
                    'use scaling of 1st comp.'
                endif
                scale(ia).(it).min=scale(ia-1).(it).min
                scale(ia).(it).max=scale(ia-1).(it).max
              endif
            endif
          endfor
                                ;initialize call to compute_profile
          if ipt.code eq 'IDL' then begin
            fill_localstray,lsi,lsq,lsu,lsv,dols, $
              observation.wl,n_elements(observation.wl)
            inprof=compute_profile(/init,line=all_lines,atm=total_atm, $
                                   obs_par=obs_par,blend=blend,gen=gen, $
                                   lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv, $
                                   dols=dols,lspol=ipt.localstray_pol, $
                                   convval=0.,doconv=0,conv_mode=0, $
                                   prefilterval=0,doprefilter=0,$
                                   ret_wlidx=0,nret_wlidx=-1, $
                                   wl=observation.wl)
          endif
          
          
                                ;call pikaia
          
                                ;normalize weight
          if 1 eq 0 then begin ;do not normalize weight, use function as defined
                                ;in wgt-file
            tot_weight=100.
            wgt_sum=0.
            for it=0,3 do wgt_sum=wgt_sum+total(weight.(it))
            if wgt_sum eq 0 then wgt_sum=1.
            for it=0,3 do weight.(it)=weight.(it)*tot_weight/wgt_sum
          endif
          statdim=intarr(2)
          statpar=fltarr(pik_NMAX,pik_PMAX)
          statfit=fltarr(pik_PMAX)
          statifit=fltarr(maxwl,pik_PMAX)
          statqfit=fltarr(maxwl,pik_PMAX)
          statufit=fltarr(maxwl,pik_PMAX)
          statvfit=fltarr(maxwl,pik_PMAX)
          
          all_lines=get_pikaia_idx(all_lines,nadd=total_atm(0).npar)
          nblend=ipt.nblend
          blend=ipt.blend(0:nblend-1)
          blend=get_pikaia_idx(blend,nadd=total_atm(0).npar+all_lines(0).npar)
          gen=ipt.gen
          gen=get_pikaia_idx(gen,nadd=total_atm(0).npar+all_lines(0).npar+ $
                             blend(0).npar)      
                                ;calculate strength for quv-signal:
                                ;used for scaling of fitness (fitness
                                ;should be independent of signal
                                ;strength)
          if ipt.ncalls(im) gt 0 or total(ipt.ncalls) eq 0  then begin
            fill_localstray,lsi,lsq,lsu,lsv,dols, $
              observation.wl,n_elements(observation.wl)
            if ipt.code eq 'FORTRAN' then begin
              iobs=float(observation.i) & qobs=float(observation.q)
              uobs=float(observation.u) & vobs=float(observation.v)
              wlvec=observation.wl & nwl=fix(n_elements(observation.wl))
              iwgt=float(weight.i) & qwgt=float(weight.q)
              uwgt=float(weight.u) & vwgt=float(weight.v)
              natm=fix(n_elements(total_atm)) & nline=fix(ipt.nline)
              atm=total_atm 
              verbose=fix(ipt.verbose)
              fitness=0.
              ncalls=ipt.ncalls(im)
              random=fix(randomu(seed)*10000)
              ifit=fltarr(maxwl) & qfit=fltarr(maxwl)
              ufit=fltarr(maxwl) & vfit=fltarr(maxwl)   
              bytconvfunc=bytarr(maxstr)
              if strlen(ipt.conv_func) gt 0 then $
                bytconvfunc(0:strlen(ipt.conv_func)-1)=byte(ipt.conv_func) $
              else bytconvfunc(0)=32b
              bytprefunc=bytarr(maxstr)
              if strlen(ipt.prefilter) gt 0 then $
                bytprefunc(0:strlen(ipt.prefilter)-1)=byte(ipt.prefilter) $
              else bytprefunc(0)=32b
              cnwl=ipt.conv_nwl
              wljitter=ipt.conv_wljitter
              prefilter_wlerr=ipt.prefilter_wlerr
              wlfit=dblarr(maxwl) & nwlfit=0
              iwl_compare=intarr(maxwl) & nwl_compare=-1
              conv_output=ipt.conv_output
              ilp=ipt.localstray_pol
              iwlrg=ipt.wl_range
              nfree=0
              pikaia_pop=ipt.pikaia_pop
              methodim=method(im)
              cmode=ipt.conv_mode
              fcall=call_external(idlso_dir+'call_pikaia.so', $
                                  'call_pikaia_pro_',/unload, $
                                  all_lines,nline, $
                                  total_atm,natm, $
                                  wlvec,nwl, $
                                  blend,nblend, $
                                  gen, $
                                  lsi,lsq,lsu,lsv,dols,ilp, $
                                  iwlrg,bytconvfunc,cnwl,cmode,wljitter, $
                                  bytprefunc,prefilter_wlerr, $
                                  obs_par, $
                                  iobs,qobs,uobs,vobs, $
                                  iwgt,qwgt,uwgt,vwgt, $
                                  scale,voigt,magopt,old_norm, $
                                  use_geff,use_pb,pb_method, $
                                  modeval,iprof_only, $
                                  verbose,methodim,ncalls,pikaia_pop,use, $
                                  atm,blend,gen,fitness,random, $
                                  ifit,qfit,ufit,vfit,wlfit,nwlfit, $
                                  iwl_compare,nwl_compare,conv_output, $
                                  hanle_azi,norm_stokes_val,nfree,chi2mode, $
                                  old_voigt, $
                                  statdim,statfit,statpar, $
                                  statifit,statqfit,statufit,statvfit)
              fitprof={wl:wlfit(0:nwlfit-1),i:ifit(0:nwlfit-1), $
                       q:qfit(0:nwlfit-1), $
                       u:ufit(0:nwlfit-1),v:vfit(0:nwlfit-1)}
              if nwl_compare ge 1 then $
                iwl_compare=iwl_compare(0:nwl_compare-1)-1
              tsuff='FORTRAN'
            endif else begin
              fitprof=call_pikaia(line=all_lines,atm=total_atm, $
                                  observation=observation, $
                                  ncalls=ipt.ncalls(im), $
                                  pikaia_pop=ipt.pikaia_pop, $
                                  blend=blend(0:ipt.nblend-1),gen=gen, $
                                  lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv, $
                                  dols=dols,lspol=ipt.localstray_pol, $
                                  wlrg=ipt.wl_range, $
                                  conv_func=ipt.conv_func, $
                                  conv_nwl=ipt.conv_nwl, $
                                  conv_mode=ipt.conv_mode, $
                                  conv_wljitter=ipt.conv_wljitter, $
                                  prefilter_func=ipt.prefilter, $
                                  prefilter_wlerr=ipt.prefilter_wlerr, $
                                  weight=weight,scale=scale,voigt=voigt, $
                                  magopt=magopt,old_norm=old_norm, $
                                  use_geff=use_geff,hanle_azi=hanle_azi, $
                                  use_pb=use_pb, $
                                  pb_method=pb_method, $
                                  norm_stokes_val=norm_stokes_val,$
                                  obs_par=obs_par, $
                                  ret_atm=atm,ret_blend=blend,ret_gen=gen, $
                                  fitness=fitness,verbose=ipt.verbose, $
                                  iwl_compare=iwl_compare, $
                                  nwl_compare=nwl_compare, $
                                  conv_output=ipt.conv_output, $
                                  old_voigt=old_voigt,statdim=statdim, $
                                  statfit=statfit,statpar=statpar, $
                                  statifit=statifit,statqfit=statqfit, $
                                  statufit=statufit,statvfit=statvfit)
              tsuff='IDL'
            endelse
          endif
                                ;print atm
          show=ipt.parset
          if straypol_run then begin
            for il=0,n_elements(all_lines)-1 do begin
              all_lines(il).straypol_par(0:natm-1).vlos=atm.par.vlos
              all_lines(il).straypol_par(0:natm-1).width=atm.par.width
              all_lines(il).straypol_par(0:natm-1).dopp=atm.par.dopp
              case modeval of
                1: all_lines(il).straypol_par(0:natm-1).damp=atm.par.damp
                2: all_lines(il).straypol_par(0:natm-1).damp=atm.par.gdamp
                3: all_lines(il).straypol_par(0:natm-1).damp=atm.par.gdamp
                4: all_lines(il).straypol_par(0:natm-1).damp=atm.par.gdamp
                5: all_lines(il).straypol_par(0:natm-1).damp=atm.par.gdamp
                else:
              endcase
            endfor
          endif
          case 1  of
            ipt.verbose ge 2 and ipt.ncalls(im) ge 1: begin
              print_atm,atm,scale=scale,show=show,line=all_lines, $
                blend=blend(0:ipt.nblend-1),gen=gen, $
                pixx=xpvec(0),pixy=ypvec(0)
              if ipt.display_profile ge 1 and $
                (im1 lt nmi-1 or straypol_run) then begin
                plot_profiles,observation,fitprof,weight=weight,win_nr=10, $
                  unno_weight=unno_weight,line=all_lines,blend=blend,gen=gen, $
                  ipt=ipt,atm=atm,scale=scale,fitness=fitness,x=xpmm,y=ypmm
              endif
            end
            ipt.verbose ge 1 and ipt.ncalls(im) ge 1: $
              print_atm,atm,scale=scale,show=show, $
              line=all_lines,pixx=xpvec(0),pixy=ypvec(0)
            else:
          endcase
          mstr=(['Pikaia','Powell','LMDiff','PIK_LM'])(method(im))
          if n_elements(method_str) eq 0 then method_str=mstr $
          else method_str=[method_str,mstr]
;----------------------------------------------------      
        endfor                  ;multi iteration part
;----------------------------------------------------    

                                ;get final weighting function (total
                                ;weight of all multi iteration steps)
                                ;(for fitness calculation)
        get_wgt,obs=observation,wgt=totwgt,im=im
        get_wgtstr,observation=observation,line=all_lines,weight=totwgt
        
        if n_elements(nwl_compare) eq 0 then nwl_compare=-1
        if nwl_compare ge 1 then begin
          iwc=iwl_compare(0:nwl_compare-1)
          fcalc_prof={wl:fitprof.wl(iwc),i:fitprof.i(iwc),q:fitprof.q(iwc), $
                      u:fitprof.u(iwc),v:fitprof.v(iwc)}
        endif else fcalc_prof=fitprof
                                ;calculate nfree
        par=struct2pikaia(atm=atm,par=par,line=all_lines,$
                          blend=blend,gen=gen,/noscale)
        ntotal=total(totwgt.i ge 1e-5)+total(totwgt.q ge 1e-5)+ $
          total(totwgt.u ge 1e-5)+total(totwgt.v ge 1e-5)
        nfree=(ntotal-n_elements(par))>1
        fitness=c_fitness(fcalc_prof,observation,totwgt,all_lines(0).icont)
        if n_elements(iptbest) ne 0 then $
          if piklmfirst or fitness gt fitness_old or piktry eq 1 then begin
          iptbest.atm=atm
          iptbest.gen=gen
          iptbest.blend=blend
          iptbest.line=all_lines
          fitness_old=fitness
        endif
;         prog=float((ipr-1+ibx*pixelrep/ipt.stepx+iby*nbx*pixelrep/ipt.stepy/ipt.stepx)+1)/ $
;           (long(nbx)*nby/ipt.stepx/ipt.stepy*pixelrep)
        prog=(ipr+float((ibb))*pixelrep)/(nlst*pixelrep)
;        print,ipr,ipixcnt,prog        
        tend=(systime(1)-tlstart)/prog+tlstart
        print,'('+add_comma([xpstr,ypstr],sep='|')+'): '+ $
          strmid(add_comma(method_str,sep='/'),0,3)+ $
          '-Time ('+strmid(tsuff,0,3)+'): '+ $
          n2s(systime(1)-t,format='(f15.3)')+' s'+ $
          ', Fit: '+n2s(fitness,format='(f15.3)')+ $
          ', End: '+strmid(systime(0,tend),4,16)
        
        xyinfo='.x'+n2s(xp,format=pfmt)+ $
          '.y'+n2s(yp,format=pfmt)
        prf={ic:icont,wlref:0d,wl:fitprof.wl, $
             i:fitprof.i,q:fitprof.q,u:fitprof.u,v:fitprof.v}
        fitprof=prf
        ;; if sfp_par then if ipt.save_fitprof eq 1 $
        ;;   and keyword_set(noresult) eq 0 then begin
        ;;   fpsav=ipt.dir.sav+red_nopath+xyinfo+'.fit.sav'
        ;;   save,/compress,/xdr,file=fpsav,fitprof,weight,fitness,all_lines
        ;;   if ipt.verbose gt 0 then print,'Fit-profile written to: '+fpsav
        ;;                         ;also save observed profile
        ;;   obsav=ipt.dir.sav+red_nopath+xyinfo+'.obs.sav'
        ;;   save,/compress,/xdr,file=obsav,observation
        ;;   if ipt.verbose gt 0 then print,'Observed-profile written to: '+obsav
        ;; endif
        
        if ipt.display_profile ge 1 or keyword_set(comp_store) or $
          n_elements(compprof) ne 0 then begin
          @comm_comp.pro
          cprof_icont=icont
          iia=-1
          showcomp=ipt.display_comp ge 1 and ipt.ncomp gt 1
          common prof_set,prof_set
          if n_elements(prof_set) ne 0 then showcomp=prof_set.show_comps
          if showcomp then begin
            conv=read_conv(ipt.wl_range,ipt.conv_func,ipt.conv_nwl, $
                           ipt.conv_mode, $
                           ipt.conv_wljitter, $
                           ipt.prefilter,observation.wl,ipt.verbose)
            compwl=observation.wl
            rwl=0
            rnwl=-1
            if conv.doconv eq 1 then begin
              cnwl=conv.nwl
              convval=conv.val(0:cnwl-1)
              compwl=conv.wl(0:cnwl-1)
              if cnwl ge 1 then begin
                if ipt.conv_output eq 0 or dols eq 1 then begin
                  rwl=conv.iwl_compare
                  rnwl=conv.nwl_compare
;                  compwl=conv.wl(conv.iwl_compare(0:conv.nwl_compare-1))
                endif
              endif
            endif

                                ;prefilter_wlerr set to 0.
            prefilter=read_prefilter(ipt.prefilter,0.0, $
                                     observation.wl,ipt.verbose)
            prefilterval=prefilter.val
            doprefilter=prefilter.doprefilter
;            fill_localstray,lsi,lsq,lsu,lsv,dols,compwl,n_elements(compwl)
            for iia=0,ipt.ncomp-1 do begin              
              cprof= $
                compute_profile(init=1, $
                                atm=atm,line=all_lines, $
                                wl=compwl, $ ; wl=observation.wl, $
                                obs_par=obs_par,blend=blend,gen=gen, $
                                lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv, $
                                dols=dols,lspol=ipt.localstray_pol, $
                                convval=convval,doconv=conv.doconv, $
                                conv_mode=ipt.conv_mode, $
                                prefilterval=prefilter.val,$
                                doprefilter=prefilter.doprefilter, $
                                ret_wlidx=rwl, $
                                nret_wlidx=rnwl, $
                                normff=0,comp=iia)
              dummy=execute('cprof'+n2s(iia)+'=cprof')              
              if iia eq 0 then compprof=replicate(cprof,ipt.ncomp)
              compprof[iia]=cprof
            endfor
          endif
          
                                ;compute profile with more wl-points
          lstyle=intarr(20)
          if (n_elements(observation.wl) le 4 and ipt.conv_nwl eq 0 and $
              dols eq 0) then begin
            iptwl=dindgen(256)/255*(ipt.wl_range(1)-ipt.wl_range(0))+ $
              ipt.wl_range(0)
            fill_localstray,lsi,lsq,lsu,lsv,dols,iptwl,n_elements(iptwl)
            cprof=compute_profile(/init, $
                                   atm=atm,line=all_lines,wl=iptwl, $
                                   obs_par=obs_par,blend=blend,gen=gen, $
                                   lsi=lsi,lsq=lsq,lsu=lsu,lsv=lsv, $
                                   dols=dols,lspol=ipt.localstray_pol, $
                                   convval=0,doconv=0,conv_mode=0, $
                                   prefilterval=0,$
                                   doprefilter=0, $
                                   ret_wlidx=0,nret_wlidx=-1)
            
            dummy=execute('cprof'+n2s(iia>0)+'=cprof')              
            lstyle(iia>0)=1
          endif
        endif  

        if ipt.display_profile ge 1 then begin
          if max(ipt.approx_dir) and synth_mode eq 0 then $
            unno_weight={inc:s13_weight,azi:quv_weight,wl:unno_wl}
          
            if synth_mode then xyinfo=''
            if ipt.dir.atm_suffix ne '' then atmsuff='_'+ipt.dir.atm_suffix $
            else atmsuff=''
            if n_elements(psfile) ne 0 then psf=psfile $
            else psf=ipt.dir.ps+filestr+xyinfo+atmsuff+'.ps'
          if ipt.ps then begin
            psset,ps=ipt.ps,file=psf,size=[18.,26.],no_x=no_x,win_nr=10, $
              modal=n_elements(psfile) ne 0,group_leader=group_leader
          ;   hg=26.
          ;   wd=18.
            
          ;   psout=ps_wg(size=[wd,hg],psname=psn,file=psf,no_x=no_x, $
          ;               modal=n_elements(psfile) ne 0, $
          ;               group_leader=group_leader)
          ;   if psout(1) eq 1 then reset
          ;   !p.font=0    
           endif else begin
             !p.font=-1
           endelse
          
          plot_profiles,observation,fitprof,$
            cprof0,cprof1,cprof2,cprof3,cprof4,cprof5, $
            cprof6,cprof7,cprof8,cprof9, $
            cprof10,cprof11,cprof12,cprof13,cprof14, $
            cprof15,cprof16,cprof17, $
            weight=totwgt,iic=1,/icont,lstyle=lstyle,win_nr=10, $
            unno_weight=unno_weight,line=all_lines, $
            title=ipt.dir.atm_suffix,blend=blend,gen=gen, $
            ipt=ipt,atm=atm,scale=scale,fitness=fitness,x=xpmm,y=ypmm, $
            wintitle='HeLIx+ - FIT'
          init=0
;      wgt_str='.'+add_comma(sep='',n2s(fix(ipt.iquv_weight)))
          if n_elements(savconstruct) eq 0 then $
            savconstruct='comp'+n2s(n_elements(atm))+ $
            (['','.approx_dir'])(ipt.approx_dir)+ $
;        wgt_str+ $
          (['','.approx_azi'])(ipt.approx_azi)+ $
            (['','.voigt'])(voigt)+ $
            (['','.approx_dev'])(ipt.approx_dev)+ $
            (['','.stray'])(max(stray))
          xyouts,0,1,/normal,charsize=0.9,'!C'+ipt.file ;savconstruct
          
          xyouts,/normal,charsize=1.2,alignment=1,1,0,'Perc: '+ $
            n2s(prog*100.,format='(f15.2)')+' end: '+ $
            conv_time(old='sec',new='date_dot',tend,/idl)
          
                                ;close PS-file
          psset,/close
          ; if !d.name eq 'PS' and n_elements(psn) ne 0 then begin
          ;   xyouts,0,0,/normal,'!C!C'+psn,charsize=1
          ;   print_ipt,ipt,/erase
          ;   device,/close
          ;   if ipt.verbose gt 0 then print,'Created PS-file: '+psn
          ; endif
          ; !p.font=-1 & set_plotx
        endif
        
        
                                ;store profiles
        thisresult=result_struc(nx=0,ny=1,ipt=ipt)
        thisresult.fit.x=xp
        thisresult.fit.y=yp
        tatm=thisresult.fit.atm
        struct_assign,atm,tatm
        thisresult.fit.atm=tatm
        tblend=thisresult.fit.blend
        struct_assign,blend,tblend
        thisresult.fit.blend=tblend
        tgen=thisresult.fit.gen
        struct_assign,gen,tgen
        thisresult.fit.gen=tgen
        thisresult.fit.fitness=fitness

        
        tgt=thisresult.fit.linepar
        struct_assign,all_lines.par,tgt
        thisresult.fit.linepar=tgt

        tgt=thisresult.fit.straypol_par
        struct_assign,all_lines.straypol_par,tgt
        thisresult.fit.straypol_par=tgt
        
        for il=0,ipt.nline-1 do begin
;;           tgt=thisresult.fit.straypol_par(*,il)
;;           struct_assign,all_lines(il).straypol_par,tgt
;;           thisresult.fit.straypol_par(*,il)=tgt
          nprl=thisresult.line(il)
          struct_assign,all_lines(il),nprl
          thisresult.line(il)=nprl
        endfor
        
        noresult=keyword_set(noresult)
        if n_elements(profile) ne 0 then begin
          if max(tag_names(profile) eq 'X') ne 1 and $
            (fits_mode eq 0 and imax_mode eq 0 and $
             hinode_mode eq 0 and fits4d_mode eq 0) then noresult=1
        endif else noresult=1
        if (noresult eq 0 or (keyword_set(force_write)) or $
            ipt.save_fitprof ne 0) then begin
          if (spinor_mode eq 0 and struct_mode eq 0) or $
            keyword_set(force_write) then begin
            
            if ipt.fitsout eq 1 then begin
              catm=atm
              catm.fit=catm.fitflag
              catm=get_pikaia_idx(catm,/silent)
              cline=get_pikaia_idx(all_lines(0:ipt.nline-1),nadd=catm(0).npar, $
                                   /silent)
              cblend=get_pikaia_idx(blend(0:ipt.nblend-1), $
                                    nadd=catm(0).npar+cline(0).npar,/silent)
              cgen=get_pikaia_idx(gen,nadd=catm(0).npar+cline(0).npar+ $
                                  cblend(0).npar,/silent)
              if statdim[1] eq 0 or (ipt.pikaia_stat eq 'NONE') then begin
                irep0=ipr
                irep1=ipr
                isort=0
              endif else begin
                irep0=1
                irep1=ipt.pikaia_stat_cnt
                isort=reverse(sort(statfit))
              endelse                
              par=struct2pikaia(atm=catm,par=par,line=cline,$
                                blend=cblend,gen=cgen,reverse=0,noscale=1)
              tprof=fitprof
              tpar=par
              tfitness=thisresult.fit.fitness
              tnwl=n_elements(fitprof.i)
              for iipr=irep0,irep1 do begin
                if ipt.pikaia_stat_cnt ge 1 then begin
                  tpar=statpar[0:(n_elements(par)-1),isort(iipr-1)]
                  tfitness=statfit[isort(iipr-1)]
                  tprof.i=statifit[0:tnwl-1,isort(iipr-1)]
                  tprof.q=statqfit[0:tnwl-1,isort(iipr-1)]
                  tprof.u=statufit[0:tnwl-1,isort(iipr-1)]
                  tprof.v=statvfit[0:tnwl-1,isort(iipr-1)]
                endif
                fitsout_write,xp,yp,iipr,tpar,tprof,tfitness,ipt.keepbest, $
                              nowrite=iipr lt irep1
              endfor
              fitsopen=1
            endif else begin
                                ;output of atm and profile
              xstr='x'+n2s(xp,format=pfmt)
              ystr='y'+n2s(yp,format=pfmt)
              xyinfo=xstr+ystr
              resultname="/"+xstr+'/'+xyinfo+'.profile.dat'
              atm_path=ipt.dir.atm+'atm_'+ $
                ipt.observation
              if strmid(atm_path,strlen(atm_path)-1,1) eq '/' then $
                atm_path=strmid(atm_path,0,strlen(atm_path)-1)
              if strcompress(ipt.dir.atm_suffix, $
                             /remove_all) ne '' then $
                atm_path=atm_path+'_'+ipt.dir.atm_suffix 
              if ipr ge 2 then xyinfo=xyinfo+'.'+n2s(ipr-1,format='(i5.5)')
              write_atm,atm=thisresult.fit.atm,line=thisresult.line, $
                fitness=thisresult.fit.fitness,fitprof=fitprof, $
                resultname=atm_path+resultname,xyinfo=xyinfo, $
                ipt=thisresult.input, $
                blend=thisresult.fit.blend,gen=thisresult.fit.gen

              
            endelse
            atm_created=1
          endif
        endif 
        
        if (piktry ne 1) and (fitness le (ipt.piklm_minfit*fitness_old)) then begin
          piktry=1
;          ii=(ii-1)>0
        endif else begin
          piktry=0
        endelse
        
      endif                     ;thiserror (profile read error)
;-------------------------------------------------------        
        
      endfor                    ;loop for pixel repetitions  
;      ii=ii+1
;    endwhile                    ;loop for inbox
  endfor                        ;loop for x/y on obesrvation image
  
  
  if made_sav then begin
    if spinor_mode eq 0 and synth_mode eq 0 and struct_mode eq 0 then $
      savall=concat_colsav(sav=psav,delete=(keyword_set(keep_colsav) eq 0))
    if ipt.display_map and struct_mode eq 0 then begin
      call='xdisplay,'''+savall+'''' ;+(['',',/ps'])(ipt.ps)
      dummy=execute(call)
      if ipt.ps then begin
        print,'Create postscript map by using command: '
        print,call+',/ps'
      endif
    endif
  endif
  
  if atm_created and ipt.fitsout eq 0 then begin
    if verbose ge 1 then print,'Atmospheres and profiles created.'
    write_input,atm_path+'/input.ipt',ipt=thisresult.input,error=error
    mksav_cmd='make_sav,data_dir='''+atm_path+''''
    if ipt.display_map and struct_mode eq 0 and $
      n_elements(plist(0,*)) gt 1 then begin
      print,'Creating sav file for xdisplay ...'
      dummy=execute(mksav_cmd)
    endif else begin
      if verbose ge 1 then begin
        print,'To create sav file for xdisplay use the command:'
        print,mksav_cmd
      endif
    endelse
  endif
  
  if fitsopen then begin
    fitsout_close
  endif
  
                                ;create nice input-file
  if keyword_set(nice) then $
    if n_elements(input_file) eq 1 then begin
    nname=strmid(input_file,0,strpos(input_file,'.ipt'))+'.nice.ipt'
    write_input,ipath+input_file,ipt=ipt,error=error
  endif
  
;  if n_elements(proforig) ne 0 then profile=temporary(proforig)
end
