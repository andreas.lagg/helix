pro soup2tip,mask=mask,ipos=ipos,verbose=verbose,wlref=wlref,show=show,dir_wide=dir_wide,ps=ps;,wlvec=wlvec ;,mpos=mpos
  
  
  
  userlct,coltab=0,/full,/reverse
  
  if (n_elements(mask) eq 0 or n_elements(ipos) eq 0) then begin
    print,'Converting SOUP fits files to TIP format for HeLIx+ inversions.'
    print,'Usage:'
    print,'  soup2tip,mask=''/scratch/lagg/SOUP/stokes?c_26Jun2006.*.fits'', $'
    print,'           ipos=6,wlref=6302.4936d,dir_wide=''./wideband/'''
    print,' mask ... defines file mask of SUP files to be converted'
    print,' ipos ... position of identifier for I,Q,U,V'
;    print,' mpos ... position of identifier for WL-Bin (filter position)'
    print,' wlref ... reference wavelength for ''.m'' and ''.p'' in filename'
    print,' dir_wide ... directory containing wide-band images'
    print
    print,'example:'
    print,' soup2tip,mask=''/scratch/lagg/SOUP/stokes?c_26Jun2006.006303.*.f0.fits'',ipos=6'
    retall
  endif
  
                                ;path to anasoft fzcompress.so
;  linkimage,'fzread','/home/lagg/idl/anaidl/fzcompress.so',0,'ana_fzread', $
;    min_args=2,max_args=3

  
  if n_elements(verbose) eq 0 then verbose=1
  if n_elements(show) eq 0 then show=1
  if n_elements(wlref) eq 0 then wlref=6302.4936d
  
                                ;find all I-profiles
  slpos=strpos(/reverse_search,mask,'/')
  dir=strmid(mask,0,slpos)
  if n_elements(dir_wide) eq 0 then dir_wide=dir+'/wideband'
  fmask=strmid(mask,slpos+1,strlen(mask))
  ifile=strmid(fmask,0,ipos)+'I'+strmid(fmask,ipos+1,strlen(fmask))
  
  
  iff=file_search(dir+'/'+ifile,count=icnt)
  print,'Found '+n2s(icnt)+' SOUP Stokes I files.'
  if icnt eq 0 then reset
  
  init=1
  idone=bytarr(icnt)
  iquv=['I','Q','U','V']
  for i=0,icnt-1 do if idone(i) eq 0 then begin
    slpos=strpos(/reverse_search,iff(i),'/')
    dir=strmid(iff(i),0,slpos)
    file=strmid(iff(i),slpos+1,strlen(iff(i)))
                                ;check for position of '.m' or '.p' in
                                ;filename
    mpos=-1
    nwl=-1
    ws=['.m','.p']
    for im=0,1 do begin
      tpos=(strpos(file,ws(im)))(0)
      if tpos ne -1 then mpos=tpos+1
    endfor
    
    if mpos ne -1 then begin
                                ;check for WL-Bins
      ndot=strpos(strmid(file,mpos,strlen(file)),'.')
      mmask=strmid(file,0,mpos)+'*'+strmid(file,mpos+ndot,strlen(file))
      mff=file_search(dir+'/'+mmask,count=mcnt)
      if verbose ge 1 then print,'Processing: '+file
      if verbose ge 1 then print,'Found '+n2s(mcnt)+' WL-positions'
      ccname=strmid(file,0,ipos)+'IQUV'+ $
          strmid(file,ipos+1,mpos-ipos-1)+ $
          strmid(file,mpos+ndot,strlen(file))+'cc'
      psset,ps=ps,size=[18,20],file='./ps/'+ccname+'.ps',/no_x
      
      ok=0

      for ii=0,3 do begin
        iimask=dir+'/'+strmid(file,0,ipos)+iquv(ii)+ $
          strmid(file,ipos+1,mpos-ipos-1)+'*'+ $
          strmid(file,mpos+ndot,strlen(file))
        ff=file_search(iimask,count=cnt)
        if nwl eq -1 then nwl=cnt
        if nwl ne cnt then begin
          message,/cont,'Wrong number of WL-bins for '+iimask
          message,/cont,'Expected: '+n2s(nwl)
          message,/cont,'   Found: '+n2s(cnt)
        endif else begin
                                ;look for corresponding wideband image
          dpos=where(byte(ff(0)) eq (byte('.'))(0))
          ddpos=strpos(ff(0),'..')
          if ddpos ge 0 then if min(dpos) lt ddpos then begin
            frmask=strmid(ff(0),dpos(max(where(dpos lt ddpos))),strlen(ff(0)))
            wfile=file_search(dir_wide+'/*'+frmask,count=wcnt)
            if wcnt eq 0 then message,/cont,'Could not find wideband image.' $
            else begin
              if wcnt gt 1 then message,/cont,'Found '+n2s(wcnt)+ $
              ' wide band images. Using first image '+wfile(0)
              icimg=transpose(readfits_ssw(wfile(0),header,/silent))
            endelse
          endif
          
          wlvec=dblarr(cnt)
          wlbin=intarr(cnt)
          for iw=0,cnt-1 do begin
            for im=0,1 do begin
              tpos=(strpos(ff(iw),ws(im)))(0)
              if tpos ne -1 then begin
                tstr=strmid(ff(iw),tpos+2,strlen(ff(iw)))
                wlbin(iw)=fix(strmid(tstr,0,strpos(tstr,'.')))*([-1.,1.])(im)
                dwl=wlbin(iw)
                wlvec(iw)=wlref+dwl/1000.
              endif
            endfor
            img=readfits_ssw(ff(iw),header,/silent)
            sz=size(img)
            if ii eq 0 and iw eq 0 then $
              data=make_array(type=sz(sz(0)+1),nwl,sz(2),sz(1)*4)
            data(iw,*,indgen(sz(1))*4+ii)=transpose(img)
            ok=ok+1
            idx=where(iff eq ff(iw))
            if idx(0) ne -1 then idone(idx)=1
            if show then begin
              if init then if !d.name ne 'PS' then $
                window,0,xsize=sz(1)*.8,ysize=sz(2)*.6
              init=0
              zrange=minmaxp(img)
              if iquv(ii) ne 'I' then zrange=[-1,1]*max(abs(zrange))
              image_cont_al,img,contour=0,/aspect,zrange=zrange,$
                title=iquv(ii)+' '+n2s(dwl)
            endif
          endfor
        endelse
      endfor
      
                                ;display ratio sqrt((Q^2+U^2+V^2)/I^2)
      quv=sqrt(total(data(*,*,indgen(sz(1))*4+1),1)^2+ $
               total(data(*,*,indgen(sz(1))*4+2),1)^2+ $
               total(data(*,*,indgen(sz(1))*4+3),1)^2)/nwl
      if show then begin
        image_cont_al,quv,contour=0,/aspect, $
          zrange=minmaxp(quv),title='sqrt(Q^2+U^2+V^2)/I'
      endif
      
      
      if ok eq 4*nwl then begin
                                ;determine icont        
        iquiet=where(quv lt 1e-3)
        iii=(total(data(*,*,indgen(sz(1))*4),1))/nwl
                                ;rough estimate: assume that
                                ;+-50 mA around line center are at 50%
                                ;of Ic (better: use FTS to determine
                                ;this value!)
        ratio=icimg(iquiet)/iii(iquiet)*0.50
        icont=icimg/(total(ratio)/n_elements(ratio))
        if show then begin
          image_cont_al,transpose(icont),contour=0,/aspect, $
            zrange=minmaxp(icont),title='ICONT'
          if n_elements(wfile) ne 0 then $
            xyouts,0,0,/normal,'From wideband image: '+wfile(0), $
            charsize=0.8*!p.charsize
        endif
        
                                ;normalize i to icont
        for iw=0,nwl-1 do $
          data(iw,*,indgen(sz(1))*4)=data(iw,*,indgen(sz(1))*4)/float(icont)
        icont(*)=1.

        newfits=dir+'/'+ccname
        print,'Writing TIP fits file: '+newfits
        writefits,newfits,data,append=0
        newccx=newfits+'x'
        
                                ;write aux file
                                ;add offset to wlvec
        wloff=total(wlbin)/2.*1d-3
        wlvec=wlvec-wloff
        header=''
        print,'Writing Auxiliary file: '+newccx
        writefits,newccx,icont,header,append=0
        naxpos=max(where(strpos(header,'NAXIS') eq 0))
        wloff=0. & wlbin=0.
        header=[header(0:naxpos), $
                'WL_NUM  = '+string(nwl,format='(i20)')+ $
                ' / WL-bins', $
                'WL_OFF  = '+string(wloff,format='(d20.8)')+' / WL-Offset', $
                'WL_DISP = '+string(wlbin,format='(d20.8)')+ $
                ' / WL-Dispersion',$
                header(naxpos+1:*)]
        writefits,newccx,icont,header,append=0        
        
                                ;write out wl-vector as extension
        writefits,newccx,wlvec,append=1
      endif
      
      psset,/close
      stop
    endif
  endif
end
