function def_atminputst,npar,nx,ny
  @common maxpar
  
  if npar eq 0 then data=fltarr(1,1,1) else data=fltarr(npar,nx,ny)
  
  atminput={npar:npar,nx:nx,ny:ny, $
            dummy:0, $
            file:'',par:strarr(npar>1), $
            data:data}
  
  return,atminput
end
