pro comp12,ps=ps,new=new
   common profile,profile
   @common_cppar
   common resc,c1,c2
   
   idl53,prog_dir='./epd_dps_prog/' 

  ipt=['voigt_1comp','voigt_2comp']
  list=[40,25]
  
  if n_elements(c1) eq 0 or keyword_set(new) then $
    helix,ipt=ipt(0)+'.ipt',result=c1,list=list
  if n_elements(c2) eq 0 or keyword_set(new) then $
    helix,ipt=ipt(1)+'.ipt',result=c2,list=list
  
  xpvec=list(0) & ypvec=list(1)
  if c1.input.average then begin
    xpvec=(list(0)+indgen(c1.input.stepx))
    ypvec=(list(1)+indgen(c1.input.stepy))
  endif
  obs=get_profile(profile=profile, $
                  ix=xpvec,iy=ypvec,icont=icont, $
                    wl_range=c1.input.wl_range)
  
  voigt=strupcase(c1.input.profile) eq 'VOIGT'
  magopt=c1.input.magopt(0)
  hanle_azi=c1.input.hanle_azi
  old_norm=c1.input.old_norm
  old_voigt=c1.input.old_voigt
  use_geff=c1.input.use_geff(0)
  modeval=c1.input.modeval(0)
  prof1=compute_profile(/init,line=c1.line,atm=c1.fit.atm, $
                        obs_par=c1.input.obs_par(0),wl=obs.wl)
  
  voigt=strupcase(c2.input.profile) eq 'VOIGT'
  magopt=c2.input.magopt(0)
  hanle_azi=c2.input.hanle_azi
  old_norm=c2.input.old_norm
  old_voigt=c2.input.old_voigt
  use_geff=c2.input.use_geff(0)
  modeval=c2.input.modeval(0)
  prof2=compute_profile(/init,line=c2.line,atm=c2.fit.atm, $
                        obs_par=c2.input.obs_par(0),wl=obs.wl)
  
  
  
  !p.font=-1 & set_plotx
  if keyword_set(ps) then begin
    psout=ps_widget(default=[0,0,1,0,8,0,0],size=[20,10],psname=psn, $
                    file='~/work/aa_2003/figures/comp12.eps',/no_x)
    !p.font=0
  endif
  
  plot_profiles,obs,prof1,prof2,line=c1.line,/iic,iquv='IV', $
    color=[9,9,9],lstyle=[0,1,1],psym=[0,1,4],symsize=1.,/bw;, $
;    wl_range=[10827.0,10832.5]
  
  if !d.name eq 'PS' then begin
    device,/close
    print,'Created PS-file: '+psn
  endif
  
  ;output as table
  noshow=strupcase(['SGRAD'])
  for ip=0,n_tags(c1.fit.atm.par)-1 do begin
    par=(tag_names(c1.fit.atm.par))(ip)
    if max(c1.input.parset eq par) eq 1 and max(noshow eq par) eq 0 then begin
      
      out=string((tag_names(c1.fit.atm.par))(ip),format='(a6,'' & '')')+ $
        string(c1.fit.atm(0).par.(ip),format='(f8.2,'' & '')')+ $
        string(c2.fit.atm(0).par.(ip),format='(f8.2,'' & '')')+ $
        string(c2.fit.atm(1).par.(ip),format='(f8.2,'' \\ '')')
      print,out
    endif
  endfor
  print,'    \noalign{\smallskip}'
  print,'    \hline'
  print,'    \noalign{\smallskip}'
  
  !p.font=-1 & set_plotx
 
end
