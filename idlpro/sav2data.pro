;reads in sav file (solar mhd or helix format) and writes out data
;in stopro format
pro sav2data,savfile
  
  if n_elements(savfile) eq 0 then begin
    print,'sav2data - reads in sav file (solar mhd or helix format) and'
    print,'           writes out data in stopro format'
    print,'Usage:'
    print,'           sav2data,''profile_archive/5250_bin05_nwl5.sav'''
    reset
  endif
  
  print,'restoring '+savfile
  dummy=findfile(savfile,count=cnt)
  if cnt eq 0 then begin
    message,/cont,savfile+' not found.'
    reset
  endif

  restore,savfile
  if n_elements(CONTIN_SM) ne 0 then begin
    print,'Format: Solar MHD'
    profile=read_solmhd(ic=contin_sm,stokes=stokes_sm, $
                        label_pol=l_pol,w0=w0,wl=wl,verbose=1) 
    wlref=double(w0)
  endif else begin
    print,'Format: Helix'
    sObj = OBJ_NEW('IDL_Savefile',savfile)
    profname=(strlowcase(sobj->Names()))(0)
    OBJ_DESTROY,sObj
    if profname ne 'profile' then dummy=execute('profile='+profname)
    tn=tag_names(profile)
    profok=max(tn eq 'I') and max(tn eq 'WL')
    if profok eq 0 then begin
      message,/cont,'Invalid profile in '+savfile
      reset     
    endif
    wlref=profile.wlref
    wl=profile.wl
  endelse
  sz=size(profile.i)
  nx=sz(2) & ny=sz(3)
  
  slpos=strpos(/reverse_search,savfile,'/')
  if slpos ne -1 then odir=strmid(savfile,0,slpos) else odir=''
  savnp=strmid(savfile,slpos+1,strlen(savfile))
  
  print,format='(a,$)','Output directory: '+odir+' - Accept [Y/n]'
  repeat key=strupcase(get_kbrd(1)) until key eq 'Y' or key eq 'N'
  print
  if key eq 'N' then read,'Output directory: ',odir
  
  
  sr=['.profiles.sav','.sav']
  i=0
  repeat begin
    pos=strpos(savnp,sr(i),/reverse_search)
    i=i+1
  endrep until i ge n_elements(sr) or pos ne -1
  if pos ne -1 then idstr=strmid(savnp,0,pos) else idstr=savnp
  
  
  print,'Writing out '+n2s(nx)+'x'+n2s(ny)+' profiles to '+odir+'/'+idstr
  
  doall=0
  for ix=0,nx-1 do begin
    xstr='x'+n2s(format='(i4.4)',profile.x(ix))
    xdir=odir+'/'+idstr+'/'+xstr
;    spawn,'mkdirhier '+xdir,result
    file_mkdir,xdir
    for iy=0,ny-1 do begin
      ystr='y'+n2s(format='(i4.4)',profile.y(iy))
      
      pname=xstr+ystr+'.profile.dat'
      header=''''+idstr+' - Pixel '+xstr+' | '+ystr+''''
      tprof={i:reform(profile.i(*,ix,iy)),v:reform(profile.v(*,ix,iy)), $
             q:reform(profile.q(*,ix,iy)),u:reform(profile.u(*,ix,iy)), $
             ic:profile.ic(ix,iy),wl:wl+wlref, $
             prof_name:xdir+'/'+pname, $
             header:header,pdir:xdir}
      
      fi=file_search(tprof.prof_name,count=cnt)
      dowrite=0
      if cnt ne 0 and doall ne 1 then begin
        print,'Overwrite '+tprof.prof_name+' [Y/N/All]?'
        repeat key=strupcase(get_kbrd(1)) $
          until key eq 'Y' or key eq 'N' or key eq 'A'
        if key eq 'A' then doall=1
        if key eq 'Y' then dowrite=1
      endif else dowrite=1
      if dowrite or doall then begin
        error=0
        write_stopro,file=tprof.prof_name,profile=tprof, $
          wlref=wlref,header=tprof.header,verbose=0,error=error
        if error ne 0 then begin
          print,'Check write permissions in '+odir
          reset
        endif
        
        prog0=(1.*ix*nx+iy)/(1.*nx*ny)
        prog1=(1.*ix*nx+iy+1)/(1.*nx*ny)
        if fix(prog0*80) ne fix(prog1*80) then print,'*',format='(a,$)'
      endif
      
    endfor
  endfor
  print
  
  print,'Done.'
end
