;check if scaling lies outside of predefined range
function check_scaling,cnt=cnt,ipt=ipt,predef=predef
  common name,name
  
  scale_out=ipt.scale
  
                                ;define boundaries
  pre=scale_out(0)
  pre.b.min=0          &   pre.b.max=10000
  pre.azi.min=-180     &   pre.azi.max=180
  pre.inc.min=0        &   pre.inc.max=180
  pre.vlos.min=-5e5    &   pre.vlos.max=5e5
  pre.width.min=0.001  &   pre.width.max=5.0
  pre.a0.min=0         &   pre.a0.max=10
  pre.szero.min=-10.   &   pre.szero.max=10.
  pre.sgrad.min=-100.  &   pre.sgrad.max=100
  pre.gdamp.min=1e-3   &   pre.gdamp.max=20.
  pre.vmici.min=0.0    &   pre.vmici.max=100000.
  pre.densp.min=0.0    &   pre.densp.max=100
  pre.tempe.min= 000.  &   pre.tempe.max=20000.
  pre.dslab.min=0.01   &   pre.dslab.max=5.
  pre.height.min=0.01  &   pre.height.max=100.
  pre.etazero.min=0.   &   pre.etazero.max=100000.
  pre.ff.min=0.001     &   pre.ff.max=0.999
  pre.damp.min=1e-4    &   pre.damp.max=500.0
  pre.dopp.min=1e-4    &   pre.dopp.max=50.
  
  if keyword_set(predef) then return,pre
  
  tn=tag_names(pre)
  for it=0,n_elements(tn)-1 do if max(ipt.atm.fit.(it)) ne 0 then begin
    
    if cnt.(it) gt ipt.ncomp then begin
      print,'Too many parameters: '+tn(it)
      print,'You defined only NCOMP='+n2s(ipt.ncomp)+' atmospheres.'
      reset
    endif
    
    for ia=0,ipt.ncomp-1 do begin ;no scaling range
      cs=0
      if min(scale_out(ia).(it).min) lt pre.(it).min or $
        max(scale_out(ia).(it).max) gt pre.(it).max then begin
        scale_out(ia).(it).min= $
          (scale_out(ia).(it).min>pre.(it).min)<pre.(it).max
        cs=1
      endif
      if max(scale_out(ia).(it).max) gt pre.(it).max or $
        min(scale_out(ia).(it).max) lt pre.(it).min then begin
        scale_out(ia).(it).max= $
          (scale_out(ia).(it).max<pre.(it).max)>pre.(it).min
        cs=1
      endif
      if cs then begin
        message,/cont, $
          'Changed scaling of '+name.ipt(it)+', Comp. '+n2s(ia+1)+': '+ $
          add_comma(n2s([scale_out(ia).(it).min, $
                         scale_out(ia).(it).max]),sep=' to ')
      endif

      if min(scale_out(ia).(it).min) eq max(scale_out(ia).(it).max) then $
        if ipt.atm(ia).fit.(it) then begin
        message,/cont,'Zero scaling range for parameter '+name.ipt(it)+ $
          ', Comp. '+n2s(ia+1) 
        print,'Suggestion:'
        print,name.ipt(it),(pre.(it).min+pre.(it).max)/2., $
          pre.(it).min,pre.(it).max,1,format='(a5,3f12.2,i3)'
        scale_out(ia).(it).min=pre.(it).min
        scale_out(ia).(it).max=pre.(it).max
      endif
      ipt.atm(ia).par.(it)= $
        (ipt.atm(ia).par.(it)>scale_out(ia).(it).min)<scale_out(ia).(it).max

    endfor
  endif
                                ;set sum of FF to one
;   ipt.atm(0:ipt.ncomp-1).par.ff= $
;     norm_ff(ipt.atm(0:ipt.ncomp-1).par.ff, $
;             ipt.atm(0:ipt.ncomp-1).fit.ff*0+1, $
;             ipt.atm(0:ipt.ncomp-1).linid)
  
                                ;source function has to be the same
                                ;for all components: set coupling to
                                ;one and scaling to max val in input file
;   cmax=0
;   if (ipt.verbose ge 1) then print,'Couple SGRAD for all components'
;   for it=0,n_tags(ipt.atm(0).fit)-1 do cmax=max(abs(ipt.atm.fit.(it)))>cmax
;   for ia=0,ipt.ncomp-1 do if ipt.atm(ia).fit.sgrad ne 0 then begin
;     ipt.atm(ia).fit.sgrad=-cmax-1
;     scale_out(ia).sgrad.min=min(scale_out(0:ipt.ncomp-1).sgrad.min)
;     scale_out(ia).sgrad.max=max(scale_out(0:ipt.ncomp-1).sgrad.max)
;   endif
  
                                ;do not couple eta0
  if 1 eq 0 then begin
  
                                ;eta_zero has to be the same
                                ;for all components, relative ratio is
                                ;treated by loggf: set coupling to
                                ;one and scaling to max val in input file
  cmax=0
  fl=0
  for it=0,n_tags(ipt.atm(0).fit)-1 do cmax=max(abs(ipt.atm.fit.(it)))>cmax
  for ia=0,ipt.ncomp-1 do begin
    if ipt.atm(ia).fit.etazero ne 0 then begin
      ipt.atm(ia).fit.etazero=-cmax-1
      scale_out(ia).etazero.min=min(scale_out(0:ipt.ncomp-1).etazero.min)
      scale_out(ia).etazero.max=max(scale_out(0:ipt.ncomp-1).etazero.max)
      if fl eq 0 then if (ipt.verbose ge 1) then begin
        print,'Couple ETAZERO for all components'
        fl=1
      endif
    endif
  endfor
  endif
  
  ipt.scale=scale_out
  return,ipt
end
