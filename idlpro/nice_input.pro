;make input file 'nice'
pro nice_input,iptfile
  
  ipt=read_ipt(iptfile)
  write_input,iptfile,ipt=ipt
end
