pro find_solcoord
  window,0
  userlct
  
;   x=48 & y=96
;   b=90. & theta=110. & chi=124.
;   x=36 & y=112
;   b=70. & theta=104. & chi=146.
  x=56 & y=112
  b=60. & theta=92. & chi=166.
  obs='/data/slam/VTT-data/VTT-Oct05/19Oct05/19oct05.002cc'
  
  nel=180.
  inc=(findgen(nel)/(nel-1)*180) ## (fltarr(nel)+1)
  azi=(fltarr(nel)+1) ## (findgen(nel)/(nel-1)*180.-90)
  barr=fltarr(nel,nel)+b
  los2solar,btot=barr,gamma=inc,chi=azi,xval=x,yval=y, $
    obs=obs,ambig='0',inc_sol=inc0,azi_sol=azi0
  los2solar,btot=barr,gamma=inc,chi=azi,xval=x,yval=y, $
    obs=obs,ambig='180',inc_sol=inc180,azi_sol=azi180
  
  diff0=sqrt((inc0-theta)^2+(azi0-chi)^2)
  diff180=sqrt((inc180-theta)^2+(azi180-chi)^2)
  dummy=min(diff0,min0)
  dummy=min(diff180,min180)
  
  window,0
  userlct
  !p.multi=[0,1,2]
  plot,inc0,yrange=[-180.,180],/xst,/yst & oplot,color=1,azi0,psym=1
  oplot,!x.crange,[0,0]+theta & oplot,color=1,!x.crange,[0,0]+chi
  oplot,[0,0]+min0,!y.crange,color=4
  plot,inc180,yrange=[-180.,180],/xst,/yst & oplot,color=1,azi180,psym=1
  oplot,!x.crange,[0,0]+theta & oplot,color=1,!x.crange,[0,0]+chi
  oplot,[0,0]+min180,!y.crange,color=4
  
  print,'Best solution   0�: INC=',inc(min0),'  AZI=',azi(min0),' diff=',diff0(min0)
  print,'Best solution 180�: INC=',inc(min180),'  AZI=',azi(min180),' diff=',diff180(min180)
end
