pro replace_sav,sav=sav,x=x,y=y,result=result,fitprof=fitprof;,atm=atm,fitness=fitness
  common wgst,wgst
  common pikaia,pikaia_result,oldsav,prpar
  common param,param
  common profile,profile
  common store,store
  
  
  natm=pikaia_result.fit(x,y).atm
  ncorig=n_elements(natm)
  ncnew=n_elements(result.fit.atm)
  if ncorig lt ncnew then begin
    message,/cont,'  Number of Components of new atmosphere does not agree'
    message,/cont,'  with original atmosphere.'
    message,/cont,'  No replacement done!!!'
    return
  endif
  
  
  if pikaia_result.fit(x,y).fitness lt result.fit.fitness then add='better' $
  else add='worse'
  yn=dialog_message(/question,dialog_parent=wgst.base.id,/default_no, $
                    ['Replace result in original sav-file?', $
                     'Old fitness: '+ $
                     n2s(pikaia_result.fit(x,y).fitness,format='(f10.3)')+ $
                     ', new fitness: '+n2s(result.fit.fitness, $
                                           format='(f10.3)'),'', $
                     'New fit is '+add+'!'])
  
  if yn eq 'Yes' then begin
    if n_elements(store) ne 0 then dummy=temporary(store)
    print,'Replace original result ('+n2s(ncorig)+ $
      ' components) by new result ('+n2s(ncnew)+' components)'
    old=pikaia_result.fit(x,y)
    struct_assign,result.fit,old
    pikaia_result.fit(x,y)=old
    
    print,'Result of this run replaced original result in sav file: '+sav
    save,file=sav,pikaia_result,profile,/compress,/xdr
    
                                ;replace also profile in atm_archive
    atm_path=pikaia_result.input.dir.atm+'atm_'+pikaia_result.input.observation
    pfmt='(i4.4)'
    dummy=file_search(atm_path+'/x????',count=cnt4)
    dummy=file_search(atm_path+'/x???',count=cnt3)
    if cnt4 eq 0 and cnt3 ne 0 then pfmt='(i3.3)'
    xstr='x'+n2s(fix(pikaia_result.fit(x,y).x),format=pfmt)
    ystr='y'+n2s(fix(pikaia_result.fit(x,y).y),format=pfmt)
    xyinfo=xstr+ystr
    resultname="/"+xstr+'/'+xyinfo+'.profile.dat'
    if strcompress(pikaia_result.input.dir.atm_suffix,/remove_all) ne '' then $
      atm_path=atm_path+'_'+pikaia_result.input.dir.atm_suffix
    write_atm,atm=pikaia_result.fit(x,y).atm,line=pikaia_result.line, $
      fitness=pikaia_result.fit(x,y).fitness,fitprof=fitprof, $
      resultname=atm_path+resultname,xyinfo=xyinfo,ipt=result.input, $
      blend=pikaia_result.fit(x,y).blend,gen=pikaia_result.fit(x,y).gen
    
    ishow=where(param.show)
    display_data,/no_sav,/widget,parshow=ishow
    
  endif
end

