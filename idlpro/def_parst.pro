function def_parst,scale=scale,multi_iter=multi_iter,obs_par=obs_par
  @common_maxpar

                                ;define parameter set for one
                                ;component
  mm={min:0.,max:0.}

  parst={  b:0., $              ;mag. field strength [G]
           azi:0., $            ;b azimuthal angle [�]
           inc:0., $            ;b inclination angle [�]
           vlos:0., $           ;LOS-velocity [m/s]
           width:0., $          ;line width [Angstrom]
           damp:0., $           ;damping (only for voigt)
           dopp:0., $           ;doppler broadening (only for voigt)
           a0: 0., $            ;amplitude []
           szero: 0., $         ;source function at tau=0 (Planck)
           sgrad: 0., $         ;gradient of source function (Planck)
           etazero: 0., $       ;opacity ratio at line center
           gdamp:0., $          ;damping constant
           vmici:0., $          ;micro turbulence velocity
           densp:0., $          ;density parameter
           tempe:0., $          ;temperature
           dslab:0., $          ;optical thickness of slab (hanle-slab mode)
           height:0., $         ;He absorption height (hanle-slab mode)
           ff:0.}               ;filling factor []
  fitst={  b:0, $               ;mag. field strength [G]
           azi:0, $             ;b azimuthal angle [�]
           inc:0, $             ;b inclination angle [�]
           vlos:0, $            ;LOS-velocity [m/s]
           width:0, $           ;line width [Angstrom]
           damp:0, $            ;damping (only for voigt)
           dopp:0, $            ;doppler broadening (only for voigt)
           a0: 0, $             ;amplitude []
           szero: 0, $          ;source function at tau=0 (Planck)
           sgrad: 0, $          ;gradient of source function (Planck)
           etazero: 0, $        ;opacity ratio at line center
           gdamp:0, $           ;damping constant
           vmici:0, $           ;micro turbulence velocity
           densp:0, $           ;density parameter
           tempe:0, $           ;temperature
           dslab:0, $          ;optical thickness of slab (hanle-slab mode)
           height:0, $         ;He absorption height (hanle-slab mode)
           ff:0}                ;filling factor []
  
                                ;define scaling vector
  scale={b:mm, azi:mm, inc:mm, vlos:mm, width:mm, damp:mm, dopp:mm, $
         a0:mm,szero:mm, sgrad:mm, etazero:mm, $
         gdamp:mm,vmici:mm,densp:mm,tempe:mm,dslab:mm,height:mm, $
         ff:mm}
  
                                ;define multi-iteration information
  mi={perc_rg:fltarr(maxmi),fit:intarr(maxmi)}
  multi_iter={b:mi, azi:mi, inc:mi, vlos:mi, width:mi, damp:mi, dopp:mi,  $
              a0:mi,szero:mi, sgrad:mi, etazero:mi, $
              gdamp:mi,vmici:mi,densp:mi,tempe:mi,dslab:mi,height:mi, $
              ff:mi}

  
  par={fit:fitst, $             ;do not fit parameter if set to 0,
                                ;couple parameters if set to -i (same
                                ;as for SPINOR)
  fitflag:fitst, $              ;0 or 1 depending if parameter is fitted
  idx:fitst, $              ;contains the index of the one-dimensional
                                ;par-variable to be used in the pikaia
                                ;routine. Filled in get_pikaia_idx.pro
  par:parst, $                  ;contains values for atmosphere
    ratio:parst, $             ;contains initial values for atmosphere
    npar:0l, $          ;number of independent parameters to be fitted
    linid:0l, $                 ;line identifier
    straypol_use:0, $           ;flag if component is straypol-capable
    dummy1:0, $
    use_line:bytarr(idlen,maxlin),$ ;id of lines to be used with this atmosphere
                                ;(this allows to use photospheric
                                ;atmospheres for photospheric lines
                                ;only and simultaneously fit
                                ;chromospheric atmospheres with
                                ;chromospheric lines)
       atm_input:bytarr(maxstr)} ;filename for input atmosphere
  
  obs_par={slit_orientation:0.,m1angle:0.,posx:0.,posy:0.,solar_radius:950., $
           heliocentric_angle:0.,posq2limb:0., $
           hin_scannr:0,dummy:0}
  
                                ;set default value: fit=1 to fit the parameters
;  for in=0,n_tags(par.fit)-1 do par.fit.(in)=1
  return,par
end
