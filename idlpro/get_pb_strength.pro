function get_pb_strength,line,B,pb_method
  
  
  strength=fltarr(line.pb.nr_zl)
  
  case pb_method of
    1: begin ;tabulated version
      for i=0,line.pb.nr_zl-1 do begin
        strength(i)=interpol(line.pb.strength(i,0:line.pb.nr_b-1), $
                             line.pb.b(0:line.pb.nr_b-1),b,/quadratic)
      endfor
    end
    0: begin ;polynomial
      x1=(b*1e-3)
      x2=(b*1e-3)^2
      x3=(b*1e-3)^3
      x4=(b*1e-3)^4
      for i=0,line.quan.n_sig-1 do begin
        strength(i)= $
          line.quan.WER(i)*line.f + $
          (line.pb.dr(i,0)*x1 + line.pb.dr(i,1)*x2 + $
           line.pb.dr(i,2)*x3 + line.pb.dr(i,3)*x4)
        strength(i+line.quan.n_sig+line.quan.n_pi)= $
          line.quan.WEB(i)*line.f + $
          (line.pb.db(i,0)*x1 + line.pb.db(i,1)*x2 + $
           line.pb.db(i,2)*x3 + line.pb.db(i,3)*x4)
      endfor
      for i=0,line.quan.n_pi-1 do begin
        strength(i+line.quan.n_sig)= $
          line.quan.WEP(i)*line.f + $
          (line.pb.dp(i,0)*x1 + line.pb.dp(i,1)*x2 + $
           line.pb.dp(i,2)*x3 + line.pb.dp(i,3)*x4)
      endfor
    end
    -1: begin ;no pb, only zeeman (test purposes only)
      for i=0,line.quan.n_sig-1 do begin
        strength(i)=line.quan.WER(i)*line.f 
        strength(i+line.quan.n_sig+line.quan.n_pi)=line.quan.WEB(i)*line.f
      endfor
      for i=0,line.quan.n_pi-1 do begin
        strength(i+line.quan.n_sig)=line.quan.WEP(i)*line.f
      endfor
    end
  endcase
  
  return,strength
end
