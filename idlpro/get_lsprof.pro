; Mail of G. Scharmer, June 8, 2008
; 2) Local stray light correction. I am GUESSING the following: 

; If this is to work, the local stray light representation must EXCLUDE pixels 
; corresponding to the core of the psf and INCLUDE only a representation 
; corresponding to the WING of the psf, else this becomes a very 
; ill-conditioned inversion problem. By including only the WING of the psf, you 
; mimic the effects of residual high-order aberrations neither compensated by 
; the AO system nor by the MOMFBD processing. Such a psf has a diffraction 
; limited core but with lower "weight" plus extended wings lowering the 
; contrast (and giving straylight) but leaving the FWHM of the psf (the spatial 
; resolution) intact. The trick here is to avoid deconvolution but to keep the 
; straylight correction.

; To make local straylight correction work, I would therefore define an 
; appropriate "straylight" psf that ONLY includes pixels outside the main core 
; of the psf. To make that distinct, I would define the location of the first 
; minimum in the (theroretical) diffraction pattern as the separation betwen 
; the core and the wing. This corresponds to seeting all straylight psf pixels 
; within a RADIUS of 1.22 Lambda/D (diameter nearly 2.5 lambda/D) to zero. Some 
; sort of appropriate form for the wing psf is then needed. In fact, AO theory 
; may provide a good approximate form of this but I cannot check that at 
; home -- all my AO literature is at work.  

; So the model would then correspond to a psf that is partly a delta function 
; (representing the entire core of the psf) plus a "wing" part, the straylight 
; psf that "we" want to implement.

; Of course, all Stokes parameters must be included in this straylight 
; correction in the same way. I see no justification for correcting Stokes I 
; only.


;define function for weighting scheme used for the PSF.
function ls_weight,x,y,widthx,widthy
  
  wgt=exp(-(x)^2/(widthx/2.)^2) * exp(-(y)^2/(widthy/2.)^2)
  
  return,wgt
end


;mode=1: TIP
;mode=2: Hinode
function get_lsprof,fcc,xpvec,ypvec,wlvec,nwl,ipt=ipt,mode=mode,do_ls=do_ls;,icont=icont
  common hinodedat,hinfo
  common imaxinfo,imaxinfo,imaxdata,imaxheader,imaxwl
  
  lsprof={ic:0.,wl:double(wlvec),nwl:nwl, $
          i:fltarr(nwl),q:fltarr(nwl),u:fltarr(nwl),v:fltarr(nwl)} 
  
  
  if n_elements(do_ls) eq 0 then do_ls=1
  if do_ls eq 0 then return,lsprof
  
  if ipt.localstray_rad ge 1e-5 then begin
    if ipt.verbose ge 2 then print,'Retrieving local straylight profile ...'
    xnew=fix(ipt.stepx*ipt.localstray_rad*[-1,1]*sqrt(2.)+xpvec(0))
    ynew=fix(ipt.stepy*ipt.localstray_rad*[-1,1]*sqrt(2.)+ypvec(0))
    totwgt=0.
    nls=0
    
    wgtarr=fltarr(xnew(1)-xnew(0)+1,ynew(1)-ynew(0)+1)
    for ix=xnew(0),xnew(1) do begin
      case mode of
        1: begin ;TIP fits file
          xy_fits,fcc,/silent,x=ix,i=i,q=q,u=u,v=v,struct=xyfst, $
            /norm2ic,data_error=data_error,/continue_error, $
            norm_cont=ipt.norm_cont
          ny=xyfst.ny
          if data_error eq 0 then begin
            icimg=reform(xyfst.ic(ix,*))
            icarr=icimg ## (fltarr(nwl)+1)
            i=reform(i)/icarr
            q=reform(q);/icarr
            u=reform(u);/icarr
            v=reform(v);/icarr
          endif
        end
        2: begin ;hinode mode
          if hinfo.fixed_slit eq 0 then begin
            inidx=(where(hinfo.scannr eq ipt.obs_par.hin_scannr and $
                         hinfo.header.slitindx eq ix))(0)
          endif else begin
            inidx=(where(hinfo.scannr eq ix))(0)
          endelse
          if inidx eq -1 then begin
            data_error=1 
            ny=0
          endif else begin
            hf=hinfo.files(inidx)
            data=readfits_ssw(hf,hdr,/silent)
;            data=hinode_corr(data,hinfo.header)
            data=hinode_corr(data,hdr)
            data_error=0
            read_ccx,hf+'.ccx',norm_cont=ipt.norm_cont,icont=icimg,verbose=0
            icarr=reform(icimg) ## (fltarr(nwl)+1)
            i=reform(data(*,*,0))/icarr
            q=reform(data(*,*,1))/icarr
            u=reform(data(*,*,2))/icarr
            v=reform(data(*,*,3))/icarr
            ny=(size(i))(2)
          endelse
        end
        3: begin                ;Imax mode
          icimg=reform(imaxdata(ix,*,imaxinfo.icidx,0))
          if ipt.norm_cont ne 0 then icimg(*)=imaxinfo.imgcont
          icarr=icimg ## (fltarr(imaxinfo.nwl)+1)
          i=transpose(reform(imaxdata(ix,*,0:imaxinfo.nwl-1,0)))/icarr
          q=transpose(reform(imaxdata(ix,*,0:imaxinfo.nwl-1,1)))/icarr
          u=transpose(reform(imaxdata(ix,*,0:imaxinfo.nwl-1,2)))/icarr
          v=transpose(reform(imaxdata(ix,*,0:imaxinfo.nwl-1,3)))/icarr
          ny=(size(i))(2)
          data_error=0
        end
        5: begin                ;Fits4D
          if ipt.code eq 'spinor' or ipt.code eq 'helix' then begin
            read_fitsstokes,fcc,fits4data,error=error,x=ix
          endif else begin
            read_fits4d,fcc,fits4data,error=error,x=ix
          endelse
          if error eq 0 then begin
            icimg=reform(fits4data.icont)
;            icarr=reform(icimg) ## ((size(fits4data.wl))(1)+1)
            i=reform((*fits4data.data)(*,fits4data.order(0),*,*))
            q=reform((*fits4data.data)(*,fits4data.order(1),*,*))
            u=reform((*fits4data.data)(*,fits4data.order(2),*,*))
            v=reform((*fits4data.data)(*,fits4data.order(3),*,*))
            ; i=reform(fits4data.i)
            ; q=reform(fits4data.q)
            ; u=reform(fits4data.u)
            ; v=reform(fits4data.v)
            ny=(size(i))(2)
          endif else ny=0
            data_error=error
        end
      endcase
      for iy=ynew(0),ynew(1) do begin
        if data_error eq 0 and (iy ge 0 and iy lt ny) then begin
          rad=sqrt(((float(ix)-xpvec(0))/ipt.stepx)^2.+$
                   ((float(iy)-ypvec(0))/ipt.stepy)^2.)
          if (rad le ipt.localstray_rad and $
              rad ge ipt.localstray_core) then begin
            wgt=ls_weight(ix-xpvec(0),iy-ypvec(0), $
                          ipt.stepx*ipt.localstray_rad, $
                          ipt.stepy*ipt.localstray_rad)
            wgtarr(ix-xnew(0),iy-ynew(0))=wgt
            totwgt=totwgt+wgt
            lsprof.ic=lsprof.ic+icimg(iy)*wgt
            lsprof.i=lsprof.i+i(*,iy)*wgt
            lsprof.q=lsprof.q+q(*,iy)*wgt
            lsprof.u=lsprof.u+u(*,iy)*wgt
            lsprof.v=lsprof.v+v(*,iy)*wgt
            ;; lsprof.ic=lsprof.ic+icimg[ix,iy]*wgt
            ;; lsprof.i=lsprof.i+i[*,ix,iy]*wgt
            ;; lsprof.q=lsprof.q+q[*,ix,iy]*wgt
            ;; lsprof.u=lsprof.u+u[*,ix,iy]*wgt
            ;; lsprof.v=lsprof.v+v[*,ix,iy]*wgt
            nls=nls+1
          endif
        endif
      endfor
    endfor
    if totwgt gt 1e-5 then begin
      lsprof.ic=lsprof.ic/totwgt
      lsprof.i=lsprof.i/totwgt
      lsprof.q=lsprof.q/totwgt
      lsprof.u=lsprof.u/totwgt
      lsprof.v=lsprof.v/totwgt
      if ipt.verbose ge 2 then $
        print,'LOCALSTRAY profile computed from '+n2s(nls)+' profiles.'
    endif else begin
      message,/cont,'No profile found for LOCALSTRAY'
    endelse
  endif
 ; plot_profiles,lsprof 
;   print,'LSPROF',xpvec,ypvec,lsprof.ic,lsprof.i(0:4),lsprof.v(0:4)  
 ;  userlct,coltab=0,/full
 ;  image_cont_al,wgtarr,contour=0,/aspect,zrange=minmaxp(wgtarr) & stop
;  print,lsprof.ic,lsprof.i

  return,lsprof
end
