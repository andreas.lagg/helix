function remove_noise,prof,filter=filter,mode=mode,median=median
  common verbose,verbose
 
  
  if n_elements(mode) eq 0 then mode=0
  oprof=prof
                                ;first do median filtering
  if n_elements(median) ne 0 then if median gt 1 then $
    if n_elements(prof.i) gt median-1 then begin
      prof.i=median(prof.i,median)
      prof.q=median(prof.q,median)
      prof.u=median(prof.u,median)
      prof.v=median(prof.v,median)
    
  endif

  case mode of
    1: begin                    ;fourier low pass filter
      nwl=n_elements(prof.i)
      nwl2=nwl/2.
      ff=nwl2-abs(findgen(nwl)-nwl/2.)
      filtfunc=float(ff le nwl2-filter)      
      prof.i=fft(fft(prof.i,1)*filtfunc,-1)
      prof.q=fft(fft(prof.q,1)*filtfunc,-1)
      prof.u=fft(fft(prof.u,1)*filtfunc,-1)
      prof.v=fft(fft(prof.v,1)*filtfunc,-1)
      fstr='fourier-low-pass'
    end
    else: begin                 ;IDL smooth function
      if n_elements(prof.i) ge filter+2 and filter ge 2 then begin
      prof.i=smooth(prof.i,filter)
      prof.q=smooth(prof.q,filter)
      prof.u=smooth(prof.u,filter)
      prof.v=smooth(prof.v,filter)
      fstr='smooth'
      endif else fstr='NONE'
    end
  endcase
  
;  if verbose eq 2 then $
;    plot_profiles,prof,oprof,title='Noise Reduction ('+fstr+')'
  if verbose ne 0 then $
    print,'Applied filter '''+fstr+''' to data: '+n2s(filter)

  return,prof
end
