pro locate_obs
  common wgst
  common pikaia
  common statistic
  common current_sav,current_sav
  
  fi=dialog_pickfile(dialog_parent=wgst.base.id, $
                     filter='./profile_archive/*', $
                     title='Please select observation (directory or file)')
  if fi eq '' then return
  
  lfi=strlen(fi)
  if strmid(fi,lfi-1,1) eq '/' then fi=strmid(fi,0,lfi-1)
  
  lslpos=max(where(byte(fi) eq (byte('/'))(0)))
  if lslpos ne -1 then begin
    dir=strmid(fi,0,lslpos+1)
  endif else begin
    dir=''
  endelse
  obs=strmid(fi,lslpos+1,lfi)
  
  header=read_tip_header(fi,verbose=1)
  
                                ;recreate structure since header tags are
                                ;possibly different  
                                ;first check if structure contains other tags
                                ;than the listed ones
  if min(tag_names(pikaia_result) eq $
         ['LINE','INPUT','FIT','HEADER']) eq 0 then begin
    message,/cont,'Incompatible Result. Ask A. Lagg for assistance.'
    stop
    reset
  endif
  
  pikaia_result=temporary({line:pikaia_result.line,input:pikaia_result.input, $
                           fit:pikaia_result.fit,header:header})
  
  
  pikaia_result.input.dir.profile=dir
  pikaia_result.input.observation=obs
  xy_fits,/reset
  
  yn=dialog_message(['Do you want to write the new observation',dir+'/'+obs, $
                     'into the sav-file '+current_sav+'?','', $
                     '(''No'' will leave the current sav-file untouched)'], $
                    /question,dialog_parent=wgst.base.id)
  
  if yn eq 'Yes' then begin
    print,'Replacing location of observation in sav file:'
    print,'   ',current_sav
    save,/xdr,/compress,pikaia_result,statistic,file=current_sav
    print,'Done.'
  endif
  
end
