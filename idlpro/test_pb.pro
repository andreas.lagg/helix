pro test_pb
  
  atom=read_atom('he1083.0.dat','./atom/',nl,1)
  
  line=atom(0)
  ns=line.quan.n_sig
  np=line.quan.n_pi
  
  nel=30
  b=findgen(nel)/(nel-1.)*3000.
  userlct & erase
  !p.multi=[0,1,2]
  plot,xrange=[0,3000],yrange=[0,20],/nodata,[0,1],/yst
  for i=0,n_elements(b)-1 do begin
    pb=get_pb_splitting(line,b(i),opb,z)
    for ip=0,np-1 do $
      plots,psym=1,color=1+ip,b(i),(pb(ns+ip)-z(ns+ip))*1e3
    for ip=0,ns-1 do begin
      plots,psym=4,color=1+ip,b(i),(pb(ip)-z(ip))*1e3
      plots,psym=6,color=1+ip,b(i),(pb(ns+np+ip)-z(ns+np+ip))*1e3
    endfor
  endfor
  
  plot,xrange=[0,3000],yrange=[-.04,.04],/nodata,[0,1],/yst
  for i=0,n_elements(b)-1 do begin
    pb=get_pb_strength(line,b(i),opb,z)
    for ip=0,np-1 do $
      plots,psym=1,color=1+ip,b(i),(pb(ns+ip)-z(ns+ip))
    for ip=0,ns-1 do begin
      plots,psym=4,color=1+ip,b(i),(pb(ip)-z(ip))
      plots,psym=6,color=1+ip,b(i),(pb(ns+np+ip)-z(ns+np+ip))
    endfor
  endfor
  stop
  
end
  
