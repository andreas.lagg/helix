function def_convst
  @common_maxpar
  
  conv={wl:dblarr(maxwl),val:fltarr(maxwl),normval:0., $
        iwl_compare:intarr(maxwl),nwl_compare:-1, $
        nwl:0,doconv:0,mode:0,dummy:intarr(2)}
  
  return,conv
end
