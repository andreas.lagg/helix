function def_line,line=line,empty=empty
    @common_maxpar
  
  maxpar
  mm={min:0.,max:0.}
  fit={straypol_amp:0,straypol_eta0:0}
  par={straypol_amp:0.,straypol_eta0:0.}
  scale={straypol_amp:mm,straypol_eta0:mm}
  
  parst={fit:0,val:0.,par:0}
  strst={vlos:0.,width:0.,damp:0.,dopp:0.,sign:0,dummy:0} ;dummy=alignment parameter
  lst={wl:0d,geff:0.,f:1.,icont:1., $
       ju:0.,jl:0.,mass:0., $
       fit:replicate(fit,maxatm), $
       idx:replicate(fit,maxatm), $
       par:replicate(par,maxatm), $
       scale:scale, $
       straypol_par:replicate(strst,maxatm), $
       npar:0, $
       dummy:0, $               ;alignment parameter
       id:bytarr(idlen)}
  if keyword_set(empty) then return,lst

  sl=strlowcase(line)
  
  for i=0,n_elements(sl)-1 do if sl(i) ne '' then begin
    case sl(i) of
      'he': begin
        line=replicate(lst,3)   ;he-triplet
        ids=[ 'HeI 10829.1', 'HeI 10830.2', 'HeI 10830.3']
        for il=0,2 do line(il).id(0:strlen(ids(il))-1)=byte(ids(il))
        line.wl=[ 10829.0911d , 10830.2501d , 10830.3397d ]
        line.f= [ 0.111, 0.333, 0.556] ;
;        line.f=10^[-0.745,-.268,-.047] ;kurucz loggf value
;       line.geff=[ 2.0, 1.75, 0.875]  ;Tr3 = 1.25?
        line.geff=[ 2.0, 1.75, 1.25]  ;Tr3 = 1.25?
        line.jl=[1,1,1]
        line.ju=[0,1,2]
        line.mass=4
        
                                ;parameters for correction of
                                ;straypolarization
        line.fit.straypol_amp=0 ;fit flag for straypol
        line.par.straypol_amp=0 ;gauss amplitude for straypol correction
        line(0).straypol_par.sign=-1 ;parameter defining sign of correction
        line(1:2).straypol_par.sign=+1
        line.scale.straypol_amp.min=0;-0.02
        line.scale.straypol_amp.max=+0.004
        
        line.fit.straypol_eta0=0 ;fit flag for straypol
        line.par.straypol_eta0=0 ;gauss amplitude for straypol correction
        line.scale.straypol_eta0.min=0;-0.02
        line.scale.straypol_eta0.max=+0.008
;        line=line(2)
      end
      'si': begin
        line=replicate(lst,1)   ;silicon line
        ids='SiI 10827.1'
        line.id(0:strlen(ids)-1)=byte(ids)
        line.wl=10827.088d
        line.f=10.^0.220
        line.geff=1.5
        line.mass=28
      end
      'ca': begin
        line=replicate(lst,1)   ;calcium line
        ids='CaI 10829.3'
        line.id(0:strlen(ids)-1)=byte(ids)
        line.wl=10829.268d 
        line.f=10.^(-0.944)
        line.geff=1.0
        line.mass=40
      end
      else: message,'undefined line.'
    endcase
    
    line.f=line.f/total(line.f) ;normalize f
    
    if n_elements(retline) eq 0 then retline=line else retline=[retline,line]
  endif
  
  return,retline
end
