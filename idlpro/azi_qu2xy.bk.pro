function azi_qu2xy,azi,reverse=reverse,slit_orientation=slitori, $
                   verbose=verbose,header=header,obs=obs, $
                   hanle=hanle,par=par
  common azimsgfirst,first
  common pikaia,pikaia_result,oldsav,prpar
  
  headflag=0
  if n_elements(header) ne 0 then begin
    nt=n_tags(header)
    if nt le 1 then message,/cont,'No valid header information.' else headflag=1
  endif
  if headflag then begin
    if max(tag_names(header) eq 'SLITORIE') eq 1 then hso=header.slitorie(0) $
    else begin
      hso=slitori
      message,/cont,'TAG name SLITORIE not found in header. ' + $
        'Using value from input file: '+n2s(slitori)
    endelse
    if n_elements(slitori) ne 0 then if abs(slitori-hso) gt 1. then begin
      print,'Differnt values for slit_orientation in input file and' +$
        ' observation header.'
      print,'Header:     ',hso
      print,'Input-File: ',slitori
      print,'Using input file value: '+n2s(slitori)
    endif
  endif
    
  if n_elements(first) eq 0 then first=1
  if keyword_set(hanle) then begin
    if first then begin
      message,/cont,'hanle-slab mode. Rotate azimuth to map coordinates.'
      message,/cont,'THIS NEEDS TO BE CHECKED!!!!!'
    endif
    first=0
    ret_azi=azi + par.posq2limb + slitori ;should be correct.
    ret_azi=-ret_azi            ;tip image flipped is upside down
;    ret_azi=azi+ par.posq2limb 
;et_azi=-azi-43.    
    return,ret_azi
  endif
  
  if n_elements(verbose) eq 0 then verbose=0
                                ;check if observation is TIP obs, if
                                ;not then return azi image unchanged
  istip=0
  if n_elements(header) ne 0 then begin
    if max(tag_names(header) eq 'TELESCOP') eq 1 then $
      istip=strpos(header.telescop,'VTT') ne -1
  endif
  if istip eq 0 then if n_elements(obs) ne 0 then begin
    istip=check_tipmask(obs,verbose=verbose)
    if istip eq 0 then begin
;      if verbose ge 1 then  $
;        message,/cont,'No TIP file - return -azimuth+90.'
        message,/cont,'No TIP file - return +azi'
;      return,-azi
;      return,-azi+90
        return,azi        
    endif
  endif
  
  yy=1900
  scan_dir=0.
  if headflag then begin
    if max(tag_names(header) eq 'STEPANGL') then $
      stepangle=header.stepangl(0) $
    else stepangle=0.
    if stepangle eq 0 then message,/cont, $
      'WARNING! Stepangle undefined in obs-header. AZI may be wrong!!!'
    if n_elements(slitori) eq 0 then begin
      message,/cont, $
        'WARNING! Slitori undefined in obs-header. AZI may be wrong!!!'
      slitori=180.
    endif
    scan_angle=(slitori - stepangle) mod 360.
    scan_dir=round((scan_angle)/180)
;      print,scan_dir,scan_angle
    yy=fix(strmid(header.dateobs(0),0,4))
  endif
  
  if yy eq 1900 then begin
    message,/cont,'No valid header information! QU -> XY conversion is wrong!'
  endif
  
  if yy le 2003 then begin
    scan_dir=0
    if verbose ge 1 then $
      message,/cont,'2003 and older data: SCAN DIRECTION = 0'
  endif
  if yy ge 2004 then begin
    scan_dir=round((scan_angle)/180) mod 2
    if abs(scan_dir*180 -scan_angle) gt 10. then $
      message,/cont,'SCAN_DIRECTION NOT UNIQUELY DEFINED IN HEADER.' + $
      ' PLEASE CHECK!'
  endif
    
  if n_elements(slitori) eq 0 then slitori=0
  if slitori eq 0 then begin
    message,/cont,'AzimuthCorrection: Slitorientation is ZERO,' + $
      ' pleas specify it in your input file!!!'
  endif
  
  rev=keyword_set(reverse)
  ret_azi=azi
  if 1 eq 1 then begin          ; do no azi correction!!!!!!  
    if n_elements(slitori) ne 1 then begin
      message,/cont,'Slit orientation for azimuth correction needed!'
      message,/cont,'(See header of observation file)'
      message,/cont,'Returning unchanged azimuth angle.'
      return,azi
    endif

                                ;The +Q direction of TIP is always
                                ;Earth NS. The slit orientation is
                                ;always along the y-axis of the
                                ;maps. The azimuthal angle converted
                                ;from the +Q-direction to the map-y
                                ;direction is therefore given by:
    str=(['from QU to xy','from xy to QU'])(rev)
    case 1 of
      yy le 2002: begin
;        ret_azi=azi + (slitori) * ([1.,-1.])(rev)
        ret_azi=azi + (slitori +90) * ([1.,-1.])(rev)
      end
      yy eq 2003: begin
        ret_azi=azi+(slitori+90) * ([1.,-1.])(rev) 
;        ret_azi=azi + (slitori) * ([1.,-1.])(rev)
      end
      yy eq 2004: begin
        if scan_dir eq 0 then ret_azi=azi+(slitori) * ([1.,-1.])(rev) $
        else ret_azi=-azi - (slitori) 
               
      end
      yy eq 2005: begin
;        message,/cont,'AZI conversion not verified!!!! Please check!'
        ret_azi=-azi-(slitori+90) * ([1.,-1.])(rev) 
      end
      yy ge 2006: begin
                                ;definitely solved! using data sets
                                ;21oct06.009-01cc_si_1comp.pikaia.sav
                                ;and 22oct06.002-01cc_si_1comp.pikaia.sav
        ret_azi=-azi-(slitori+90) * ([1.,-1.])(rev) 
      end
      else: begin
        message,/cont,'AZI conversion not verified!!!! Please check!'
        if scan_dir eq 0 then ret_azi=azi+(slitori) * ([1.,-1.])(rev) $
        else ret_azi=-azi - (slitori) 
      end
    endcase
    
;    print,'SLITORI: ',slitori
;    print,'SCANANGLE: ',scan_angle,'   SCANDIR: ',scan_dir
;    ret_azi=360-slitori-(azi-90) ;ok for 2006
;    ret_azi=azi + (slitori) ;ok for 2006
;   stop
    
;     col='O'
;     case col of
;       'G': ret_azi=azi
;       'H': ret_azi=-azi
;       'I': ret_azi=-azi+90
;       'J': ret_azi=azi+90
;       'K': ret_azi=azi+slitori
;       'L': ret_azi=azi+slitori+90
;       'M': ret_azi=-azi+slitori
;       'N': ret_azi=-azi+slitori+90
;       'O': ret_azi=-azi-slitori
;     endcase
;     print,col,header.slitposx(0),header.slitposy(0),header.slitorie(0),header.stepangl(0)
    
                                ;After the correction for the slit
                                ;orientation the 0� direction points
                                ;towards the +y-axis of the map. To
                                ;align +x with 0� we have to:
    if n_elements(verbose) eq 1 then if verbose gt 0 then $
      print,'Rotating Azimuth '+str
                                ;scale azimuth in +-90� range
    ret_azi=((ret_azi + 270) mod 180) -90.
  endif else begin  
    print,'No azi-correction!!'
  endelse
  
  return,ret_azi
  
end

;=============================================================================
;Implementation (Feb 05), A. Lagg
; - 2001 and 2002 data: azi_map =  azi_uq + slit_ori
; - 2003 data:          azi_map =  azi_uq + slit_ori +90
; - 2004 data: azi_map = -azi - slitori   if scandir=180� (=slitori-stepangle) 
;              azi_map = +azi + slitori   if scandir=  0� (=slitori-stepangle) 


;=============================================================================
;e-mail to Manolo, date: Oct 05, 2004
; Hi Manolo,

; I am somehow still confused. I did some tests and I cannot find a unique conversion from the azimuth derived by atan(U/Q) to the azimuth converted to the scan direction (map). I attach a files with a table showing the possible azimuth conversions.
; The conversions work like follows: I use the azimuthal angle derived directly from the inversion (basically atan(U/Q)) and then I add slit orientation angle, 90� or I invert the angle. I try to match observations of sunspot to find out which conversion to use:

; These are the conversions which work (in the attached tabel they are marked in green - a ps-file showing the spot and the derived azimuth angle using this conversion is attached):

; - 2001 and 2002 data: azi_map =  azi_uq + slit_ori
; - 2003 data:          azi_map =  azi_uq + slit_ori +90
; - 2004 data:          azi_map = -azi_uq + slit_ori +90

; I reduced all data with the same acum2iquv11 and xtalk2e routines. I used the same inversion code for all data sets.

; This is the current status of my investigation - I am a little bit lost now. Maybe you have an idea?

; Thanks,
; Andreas


;=============================================================================
;e-mail  on azimuth information:

; Dear Shibu

; the direction of positive Q in the TIP data is a little bit tricky:

; i) Q positive direction is always the terrestrial N-S line.

; ii) the slit direction is variable, because the whole spectrograph
;     can be rotated. The slit direction is always vertical in the maps.
;    This means that to find out the direction of positive Q you
;    must know the spectrograph (slit) orientation.

; In your case, if you look at the spot, the slit is vertical and positive Q is along a line which forms 333.2 degrees with the slit (positive angles clockwise) and the limb direction is at 322.4 degrees. So, positive Q is almost parallel to the limb (10.8 degrees).


;     \        _
;      \     /   \     |
;       \    |    |    |
;        \    \   /    |
;         \    \_/     |
;          \    SPOT        SLIT
;           \             DIRECTION
;            \            (0 DEGREES)
;             \
;        LIMB (322.4 DEGREES)

; In this diagram positive Q direction is 10.8 degrees more vertical than the limb.

; Please tell me if you have any other doubt

; Regards

; Manolo

; From mcv@ll.iac.es Fri May 16 15:58:53 2003
; Date: Tue, 12 Mar 2002 12:14:55 +0000 (WET)
; From: Manolo Collados Vera <mcv@ll.iac.es>
; To: mathew@linmpi.mpg.de
; Subject: Re: your mail

; Hi Shibu


; >> I have few questions about the 27Se99, NOAA 8706 observation,
; >>
; >> 1) In the data header, SLITPOSX and SLITPOSY are indicated at two 
; >> places, which are slightly differ in values (x=155, y=357, in the 
; >> begining and x=157, y=361 at the end), as I understood, these two 
; >> co-ordinates tells the x and y position of the slit center for center 
; >> of the whole image frame, am I right?
; >>


; Yes, you are right. And they should be the same. In fact, they
; represent the real coordinates of the slit center before the
; scanning is started (before going to the first scanning position)
; and after it has finished (when the telescope returns to the central
; position of the scan). They are slightly different because some
; roundoff errors may acumulate when moving the telescope at small
; steps to produce the scan.


; >> 2) Does the scaning is done always in the EW (earth's) direction, and 
; >> what does the value for SLITORIE (slit orientation, in this data it is 
; >> 206.77) mean?
; >>


; No. The scanning is always done perpendicularly to the slit. And the slit can be oriented at any angle on the sky. A slit orientation of 180 degrees means the slit is oriented NS and the scan is done E->W. Increasing the slit angle means rotating it N--> W (and equivalently with the scanning
; direction)

; Regards,

; Manolo


; From mcv@ll.iac.es Fri May 16 15:59:26 2003
; Date: Fri, 5 Apr 2002 19:55:16 +0100 (WET DST)
; From: Manolo Collados Vera <mcv@ll.iac.es>
; To: mathew@linmpi.mpg.de
; Subject: Re: your mail

; Hi Shibu

; Yes, the +Q direction in the 27SEP99 data is the same as in 21SEP99.

; To find that out, get the keyword SLITORIE (slit orientation) from the header and evalaute

; +Q direction in the map = 360 - SLITORIE

; (Note that changing this value by � 180 degrees, the orientation is the same)

; This angle is with respect to the slit, which is always vertical in the maps.

; Regards

; Manolo



; >> Dear Manolo,
; >>
; >> I have appended a mail, in which you explained slit and +Q direction 
; >> for the 21 SEP 1999 data set. Is this the same for the 27th SEP 99 
; >> data set also ? Is this information is available in the header file?
; >>
; >> regards,
; >> shibu
; >>
; >>
; >>
; >> On Wed, 25 Apr 2001, Manolo Collados Vera wrote:
; >>
; >
; >>> >
; >>> > Dear Shibu
; >>> >
; >>> > the direction of positive Q in the TIP data is a little bit tricky:
; >>> >
; >>> > i) Q positive direction is always the terrestrial N-S line.
; >>> >
; >>> > ii) the slit direction is variable, because the whole spectrograph
; >>> >     can be rotated. The slit direction is always vertical in the maps.
; >>> >    This means that to find out the direction of positive Q you
; >>> >    must know the spectrograph (slit) orientation.
; >>> >
; >>> > In your case, if you look at the spot, the slit is vertical and 
; >>> > positive Q is along a line which forms 333.2 degrees with the slit 
; >>> > (positive angles clockwise) and the limb direction is at 322.4 
; >>> > degrees. So, positive Q is almost parallel to the limb (10.8 
; >>> > degrees).
; >>> >
; >>> >
; >>> >     \        _
; >>> >      \     /   \    |
; >>> >       \   |     |   |
; >>> >        \   \    /   |
; >>> >         \   \__/    |
; >>> >          \    SPOT        SLIT
; >>> >           \             DIRECTION
; >>> >            \            (0 DEGREES)
; >>> >             \
; >>> >        LIMB (322.4 DEGREES)
; >>> >
; >>> > In this diagram positive Q direction is 10.8 degrees more vertical 
; >>> > than the limb.
; >>> >
; >>> > Please tell me if you have any other doubt
; >>> >
; >>> > Regards
; >>> >
; >>> > Manolo
; >>> >
; >
; >>
; >>



; From mcv@ll.iac.es Fri May 16 15:59:44 2003
; Date: Mon, 8 Apr 2002 18:33:03 +0100 (WET DST)
; From: Manolo Collados Vera <mcv@ll.iac.es>
; To: mathew@linmpi.mpg.de
; Subject: Re: your mail

; Dear Shibu

; due to another thing I am revising the positive Q direction
; in the data and I am not quite sure the information I sent you is correct. There might be a bug in the reduction program, and the positive Q direction may be different from what I told you. Do you have problems to interpret the data with the reference system I gave you? To understand some other data, I need to make some change in the reference system. I know what I have to do but do not understand why. It would help me if you tell me whether you find some inconsistency with the reference system I gave you.

; Best regards

; Manolo


; From mathew@linmpi.mpg.de Fri May 16 16:00:05 2003
; Date: Fri, 12 Apr 2002 16:35:50 +0200 (MEST)
; From: Shibu Mathew <mathew@linmpi.mpg.de>
; To: Manolo Collados Vera <mcv@ll.iac.es>
; Subject: Re: your mail

; Dear Manolo,

; Could you fix the problem with the positive Q-direction? In that case does I have to do a correction for 27Sep99 data also?

; regards,
; shibu
; -------------------------------------------------------------------------
; Shibu K. Mathew              Phone: +49-5556-979-408 (o)
; Post-Doctoral Fellow           : +49-5556-979-193 (r)
; Max Planck Institute fur Aeronomie      e-mail: mathew@linmpi.mpg.de
; Max Planck Str. 2, D-37191
; Katlenburg-Lindau
; Germany
; --------------------------------------------------------------------------



; From mcv@ll.iac.es Fri May 16 16:00:14 2003
; Date: Fri, 12 Apr 2002 20:07:38 +0100 (WET DST)
; From: Manolo Collados Vera <mcv@ll.iac.es>
; To: mathew@linmpi.mpg.de
; Subject: Re: your mail

; Hi Shibu

; I still have not found the solution to the problem of the Q-positive direction. I have been working almost full time on that during the last three days. I have revised all the equations and the data reduction. For me everything is right, but it works with some files but it does not with others. I must confess I feel lost, because I cannot guess where the problem is. I will continue until next monday. On Tuesday I am flying for a meeting for the whole week. I will tell you in case I find the mistake.

; Regards

; Manolo


; From mathew@linmpi.mpg.de Fri May 16 16:00:40 2003
; Date: Thu, 23 Jan 2003 23:48:04 +0100 (CET)
; From: Shibu Mathew <mathew@linmpi.mpg.de>
; To: Dr. Manolo Collados <mcv@ll.iac.es>
; Cc: Shibu Mathew <mathew@linmpi.mpg.de>

; Dear Manolo,

; Wishing you a very happy new year!

; Did you make any new changes in the reduction routine regarding the +Q direction? It seems when we use your new reduction program, on the old data set,(which we already have reduced data with your old program), compairing with reduced data using your old version of  reduction programs, the Q and U signals change sign.

; waiting for your reply,

; with regards,
; shibu

; -------------------------------------------------------------------------
; Shibu K. Mathew              Phone: +49-5556-979-408 (o)
; Post-Doctoral Fellow           : +49-5556-979-193 (r)
; Max Planck Institute fur Aeronomie      e-mail: mathew@linmpi.mpg.de
; Max Planck Str. 2, D-37191
; Katlenburg-Lindau
; Germany
; --------------------------------------------------------------------------


; From mcv@ll.iac.es Fri May 16 16:01:14 2003
; Date: Fri, 24 Jan 2003 10:52:19 +0000 (WET)
; From: Manolo Collados Vera <mcv@ll.iac.es>
; To: mathew@linmpi.mpg.de
; Subject: Re: Your Message Sent on Thu, 23 Jan 2003 23:48:04 +0100 (CET)

; Dear Shibu



; >> Wishing you a very happy new year!


; The same for you and your family.


; >>
; >> Did you make any new changes in the reduction routine regarding the +Q 
; >> direction? It seems when we use your new reduction program, on the old 
; >> data set,(which we already have reduced data with your old program), 
; >> compairing with reduced data using your old version of  reduction 
; >> programs, the Q and U signals change sign.


; Yes, there was a wrong rotation in the old programs. Now it is OK (or, at least, I think it is)


; Best wishes

; Manolo


