;convert array of structures into one structure to be used in fortran
;shared object
function convert2fortran,atm=atm,line=line,scale=scale,struct,reverse=reverse
  @common_maxpar
  
  if keyword_set(atm) then begin ;structure containing atmosphere
    
    if keyword_set(reverse) then begin
      retval=struct.atm(0:struct.n-1)
    endif else begin
      empty=def_parst()
      if n_elements(struct) gt maxatm $
        then message,'Max # of atmospheres: '+n2s(maxatm)
      n0=(fix(n_elements(struct)))(0)
      fa={n:n0,atm:replicate(empty,maxatm)}
      for i=0,fa.n-1 do begin
        structi=fa.atm(i)
        struct_assign,struct(i),structi
        fa.atm(i)=structi
      endfor
      retval=fa
    endelse
    
  endif else if keyword_set(line) then begin ;structure containing line
                                ;parameters
    if keyword_set(reverse) then begin
      retval=struct.line(0:struct.n-1)
    endif else begin
      empty=def_line(/empty)
      if n_elements(struct) gt maxlin $
        then message,'Max # of lines: '+n2s(maxlin)
      n0=(fix(n_elements(struct)))(0)
      fl={n:n0,line:replicate(empty,maxlin)}
       for i=0,fl.n-1 do begin
         structi=fl.line(i)
         struct_assign,struct(i),structi
         fl.line(i)=structi
       endfor
      retval=fl
    endelse
    
  endif  else if keyword_set(scale) then begin ;structure with scaling info
    if keyword_set(reverse) then begin
    endif else begin
    endelse
  endif else message,'Pleas specify atm or line keyword'

  return,retval
end
