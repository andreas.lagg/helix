pro time_series,ps=ps,par=par,contpar=contpar
  common ts,pr14,pr18,pr19,time_14,time_18,time_19,tsec_14,tsec_18,tsec_19
  
  print,'Old routine. Use tempanal.pro instead'
  
  
  idl53,prog_dir='~/epd_dps/prog/'  
  
;-------------- user input part ---------------------------------  
;----------------------------------------------------------------  
  if n_elements(par) ne 1 then par='VLOS' ;parameter for time series
  if n_elements(contpar) ne 1 then contpar='' ;parameter for time series

  
  if n_elements(pr14) eq 0 then begin
    restore,'sav/13may01.014_v06.pikaia.sav'
    time_14=get_times('/samfs/sun/irpolar/data_may01/13may01/13may01.014', $
                     tsec=tsec_14)
    pr14=temporary(pikaia_result)
  endif
  
  if n_elements(pr18) eq 0 then begin
    restore,'sav/13may01.018_v06.pikaia.sav'
    time_18=get_times('/samfs/sun/irpolar/data_may01/13may01/13may01.018', $
                      tsec=tsec_18)
    pr18=temporary(pikaia_result)
  endif
  
  if n_elements(pr19) eq 0 then begin
    restore,'sav/13may01.019_v06.pikaia.sav'
    time_19=get_times('/samfs/sun/irpolar/data_may01/13may01/13may01.019', $
                      tsec=tsec_19)
    pr19=temporary(pikaia_result)
  endif
  
  prnam=['pr14','pr18','pr19']  ;name of variables containing data
  datnam=['time_14','time_18','time_19']  ;name of variables containing data
  tsnam=['tsec_14','tsec_18','tsec_19']  ;name of variables containing data
  
  offset_x=[29,0,11]            ;offset of observations in pixel
  offset_y=[-10,0,2]
  
  steps=[150,10,20]             ;step size
  nstep=[1,5,5]                 ;number of scans per observation
  tot_step=total(nstep)
  
  region=[0,20,9,50]           ;region for which time series is wanted
                                ;(llx,lly,urx,ury)
;----------------------------------------------------------------  
;----------------------------------------------------------------  
  
  ncomp=n_elements(pr14.fit(0).atm) ;number of components
                                ;(must be same for all observations)
  
  
  wd=19. & hg=26.
  wx=480.
  !p.font=-1 & set_plot,'X'
  if keyword_set(ps) then begin
    psf='ps/timeseries_'+par+'.ps'
    psout=ps_widget(default=[0,0,0,0,8,0,0],size=[wd,hg],psname=psn, $
                    file=psf,/no_x)
    if psout(1) eq 1 then reset
    !p.font=0    
  endif else begin
    !p.font=-1
  endelse
  
  print,'available parameters:'
  print,tag_names(pr18.fit.atm.par)
  ip=(where(tag_names(pr18.fit.atm.par) eq par))(0)
  if ip eq -1 then message,'unknown parameter: '+par
  if contpar ne '' then begin
    ipc=(where(tag_names(pr18.fit.atm.par) eq contpar))(0)
    if ipc eq -1 then message,'unknown parameter: '+contpar
  endif
  
  xs=region(2)-region(0)+1      ;define output structure
  ys=region(3)-region(1)+1
  
  rst={img:fltarr(xs,ys),val:bytarr(xs,ys),date:'',tsec:0d, $
      contimg:fltarr(xs,ys),contval:bytarr(xs,ys)}
  ts=replicate(rst,ncomp,tot_step)
  
  it=0                          ;get data from region
  zrg=fltarr(ncomp,2)
  zrgcont=fltarr(ncomp,2)
  for in=0,n_elements(nstep)-1 do begin
    dummy=execute('pr='+prnam(in))
    dummy=execute('tsec='+tsnam(in))
    dummy=execute('date='+datnam(in))
    sz=size(pr.fit)
    ix=((region([0,2])+offset_x(in)-min(pr.fit.x))<(sz(1)-1))>0
; ix0=min(where(pr.fit.x ge ix(0)))
; ix1=max(where(pr.fit.x le ix(1)))
; ix=[ix0,ix1]
    iy=((region([1,3])+offset_y(in)-min(pr.fit.y))<(sz(2)-1))>0
; iy0=min(where(pr.fit.y ge iy(0)))
; iy1=max(where(pr.fit.y le iy(1)))
; iy=[iy0,iy1]
    parimg=pr.fit.atm.par.(ip)
    if contpar ne '' then cparimg=pr.fit.atm.par.(ipc)
    for is=0,nstep(in)-1 do begin
      ts(*,it).date=date(ix(0))
      ts(*,it).tsec=tsec(ix(0))
      for ic=0,ncomp-1 do begin
        dx=ix(1)-ix(0) & dy=iy(1)-iy(0)
        img=parimg(ic,ix(0):ix(1),iy(0):iy(1))
        ts(ic,it).img(0:dx,0:dy)=parimg(ic,ix(0):ix(1),iy(0):iy(1))
        ts(ic,it).val(0:dx,0:dy)=1
        simg=img(sort(img))
        simg=simg(10:n_elements(simg)-10)
        zrg(ic,0)=zrg(ic,0)<min(simg)
        zrg(ic,1)=zrg(ic,1)>max(simg)
        if contpar ne '' then begin
          cimg=cparimg(ic,ix(0):ix(1),iy(0):iy(1))
          ts(ic,it).contimg(0:dx,0:dy)=cparimg(ic,ix(0):ix(1),iy(0):iy(1))
          ts(ic,it).contval(0:dx,0:dy)=1
          simg=cimg(sort(cimg))
          simg=simg(10:n_elements(simg)-10)
          zrgcont(ic,0)=zrgcont(ic,0)<min(simg)
          zrgcont(ic,1)=zrgcont(ic,1)>max(simg)
        endif  
      endfor
      it=it+1
      ix=((ix+steps(in))<(sz(1)-1))>0
    endfor
  endfor
  
  
  xcexact=findgen(xs+1)+region(0)
  ycexact=findgen(ys+1)+region(1)
  
  wnr=[0,2,3]
  for ic=0,ncomp-1 do begin
    
    if !d.name ne 'PS' then window,wnr(ic),xsize=wx,ysize=wx/wd*hg
    if par eq 'VLOS' then begin
      userlct,/neutral,glltab=7, $
        center=256.*(0.-zrg(ic,0))/(zrg(ic,1)-zrg(ic,0)),/nologo
    endif else userlct,/nologo
    erase
    
    title='Time Series, Comp '+n2s(ic+1)+', '+par
    
    nx=4 & ny=3
    zt=par
    msk=mask([nx,ny],title=title, $ ;subtitle=subt,headtitle=hdt, $
             xformat='(i3)',yformat='(i3)',zformat='(f15.2)', $
             xrange=[min(xcexact),max(xcexact)], $
             yrange=[min(ycexact),max(ycexact)], $
             fix_margins=[0.10,0.75],zrange=reform(zrg(ic,*)),label=1, $
             xtitle='x [pix]',ytitle='y [pix]', $
             ztitle=zt,isotropic=1, $
             charsize=!p.charsize*1.5)
    
    for iy=0,ny-1 do begin
      msk_plot,0,msk.label(iy),/nocontour
      for ix=0,nx-1 do begin
        it=iy*nx+ix
        if it lt n_elements(ts(ic,*)) then begin
          
          nv=where(ts(ic,it).val eq 0)
          
          if ix eq 0 and iy eq 0 then begin
            tstr=ts(ic,it).date
            tsec0=ts(ic,it).tsec
          endif else begin
            tstr=n2s((ts(ic,it).tsec-tsec0)/60.,format='(f15.2)')+' m'
          endelse

          msk_plot,ts(ic,it).img,msk.plot(ix,iy), $
            xcoord=xcexact,ycoord=ycexact, $
            additional_text=tstr, $
            novalid=nv,ex_ps_quality=0.5,exact=1,/nocontour,nv_color=15
          
          if contpar ne '' then begin
            nv=where(ts(ic,it).contval eq 0)
            msk_plot,smooth(ts(ic,it).contimg,7),msk.plot(ix,iy), $
              xcoord=xcexact,ycoord=ycexact, $
              levels=[.3,.5,.7],color=[9,15,10],c_labels=[1,1,1], $
              additional_text=tstr, $
              novalid=nv,ex_ps_quality=0.5,exact=1,/noimage,nv_color=15
          endif
        endif
      endfor
    endfor
  endfor
  
  if !d.name eq 'PS' then begin
    xyouts,0,0,/normal,'!C!C'+psn,charsize=!p.charsize*1.
    device,/close
    spawn,'psnup -'+n2s(ncomp)+' '+psn+' > '+psn+n2s(ncomp)
    spawn,'gv -scale -1 '+psn+n2s(ncomp)+'&'
    print,'Created PS-file: '+psn
  endif
  !p.font=-1 & set_plot,'X'
end
