pro xyplot_fill,ps=ps,all=all,comp=comp
  common xywg,xywg
  common pikaia
  common xypar,xypar
  common param
  common store
  common diswg
  common region,region
  
  widget_control,xywg.base.id,iconify=0,bad_id=bad_id
  if bad_id ne 0 then return

                                ;call get_img to make sure that data
                                ;are uo-to-date
  ishow=where(param.show)
  display_data,/no_sav,/widget,parshow=ishow,/noshow

  
  if keyword_set(all) then par=where(param.show) else par=xywg.par(0).val
  if par(0) eq -1 then par=xywg.par(0).val
  np=n_elements(par)
  if keyword_set(comp) then par=[[par],[par]] $
  else par=[[par],[xywg.par(1).val+intarr(np)]]
  
  
  
  owin=!d.window 
  widget_control,xywg.draw.id,get_value=widx
  wset,widx
  thick=1
  if keyword_set(ps) then begin
    id=add_comma([add_comma(strsplit(pikaia_result.input.observation,'/', $
                                     /extract),sep='_'), $
                  pikaia_result.input.dir.atm_suffix],/no_empty,sep='_')
    if keyword_set(comp) then cst='_comp' else cst=''
    psf='xy_'+id+'_'+add_comma(xypar.name(par(*,0)),sep='_')+'_vs_'+ $
      xypar.name(xywg.par(1).val)+cst+'.ps'
                                ;remove '/' in filename
    remchar=['/']
    for i=0,n_elements(remchar)-1 do $
      psf=strjoin(strsplit(/extract,psf,remchar(i)))
    psfile='./ps/'+psf
    
    
    psset,/ps,size=[18.,(13.5*diswg.pspages.val)<26],file=psfile
    thick=2
    !p.multi=[0,1,diswg.pspages.val]
  endif
  userlct & erase 
  
  for ii=0,np-1 do begin
    sz=size(reform(store.img(0,0,*,*)))
    data=fltarr(2,sz(1)*sz(2))
    rg=fltarr(2,2)
    axtitle=strarr(2)
    
    if ii eq 0 then begin
      valarr=bytarr(sz(1)*sz(2))+1b
        rx=[0,0,sz(1)-1,sz(1)-1,0]
        ry=[0,sz(2)-1,sz(2)-1,0,0]
      if n_elements(region) ne 0 then if region.n ge 3 then begin
        rx=[region.x(0:region.n-1),region.x(0)]-fix(store.window(0))
        ry=[region.y(0:region.n-1),region.y(0)]-fix(store.window(1))
        rx=(rx>0)<(fix(store.window(2))-1)
        ry=(ry>0)<(fix(store.window(3))-1)
        inside = polyfillv(rx/pikaia_result.input.stepx, $
                           ry/pikaia_result.input.stepy,sz(1),sz(2))
        if inside(0) ne -1 then begin
          valarr(*)=0b
          valarr(inside)=1b
        endif
      endif
    endif
    
    
    for i=0,1 do begin
      ic=xywg.comp(i).val
      pnup=strupcase(xypar.name(par(ii,i)))
      if n_elements(diswg) ne 0 then begin
        quvpar=diswg.applyquv.val(0) ne 0
        quvpar=quvpar or $      ;check B flag
          (diswg.applyquv.val(1) eq 1 and $
           max(['B','BX_SOLAR','BY_SOLAR','BZ_SOLAR'] eq pnup))
        quvpar=quvpar or $      ;check AZI flag
          (diswg.applyquv.val(2) eq 1 and $
           max(['AZI','AZI_SOLAR','BX_SOLAR','BY_SOLAR','BZ_SOLAR'] eq pnup))
        quvpar=quvpar or $      ;check INC flag
          (diswg.applyquv.val(3) eq 1 and $
           max(['INC','INC_SOLAR','BX_SOLAR','BY_SOLAR','BZ_SOLAR'] eq pnup))
      endif else quvpar=0
      azipar=strpos(pnup,'AZI') ne -1
      if xypar.name(par(ii,i)) ne 'Histogram' then begin
        idx=where(param.name eq xypar.name(par(ii,i)))
        data(i,*)=store.img(idx,ic,*,*)
        valpar=store.val(ic,*,*)
        if azipar then valpar=valpar and store.valazi(ic,*,*)
        if quvpar then valpar=valpar and store.valquv(ic,*,*)
        val=(finite(data) and valpar eq 1 and  $
             valarr eq 1b)
        noval=where(val eq 0)
        if noval(0) ne -1 then data(i,noval)=!values.f_nan
        rg(i,*)=[diswg.zmin(idx).val,diswg.zmax(idx).val]
        if abs(rg(i,0)-rg(i,1)) lt 1e-6 then $
          rg(i,*)=store.zrg(idx,*)
        axtitle(i)=xypar.name(par(ii,i))+' Comp #'+n2s(fix(ic+1))
      endif
    endfor
    
    psym=xywg.psym.val & lstyle=0
    if total(xypar.name(xywg.par.val) eq 'Histogram') eq 2 then begin
      message,/cont,'Only one axis can be Histogram.' 
    endif else begin
      hidx=(where(xypar.name(xywg.par.val) eq 'Histogram'))(0)
      if hidx ne -1 then begin
        psym=-xywg.psym.val & lstyle=0
        ih=(hidx+1) mod 2
        nbins=xywg.nbins.val<((n_elements(data(ih,*))/3.)>3)
        if nbins ne xywg.nbins.val then begin
          xywg.nbins.val=nbins
          widget_control,xywg.nbins.id,set_value=fix(xywg.nbins.val)
        endif
        hist=histogram(data(ih,*),min=rg(ih,0),max=rg(ih,1),nbins=nbins, $
                       locations=loc)
        data=fltarr(2,n_elements(loc))
        data(hidx,*)=float(hist)
        data(ih,*)=loc
        rg(hidx,*)=[0,max(hist)]
        axtitle(hidx)='PDF'
      endif
      
      xr=rg(0,*)
      yr=rg(1,*)
      if xywg.rgflag.val eq 1 then begin
        xr(*)=0 & yr(*)=0
      endif
      plot,/xst,/yst,xrange=xr,yrange=yr,data(0,*),data(1,*), $
        xtitle=axtitle(0),ytitle=axtitle(1),psym=psym, $
        charsize=1.25*diswg.charsize.val, $
        title=pikaia_result.input.observation+' '+ $
        pikaia_result.input.dir.atm_suffix, $
        linestyle=lstyle

    endelse
    if ii mod diswg.pspages.val eq 0 then begin
      nelval=total(val)
      rgstr='Box: x=['+add_comma(n2s(fix(rx+store.window(0))),sep=',')+ $
        '], y=['+add_comma(n2s(fix(ry+store.window(1))),sep=',')+ $
        '], n='+n2s(long(nelval))
      xyouts,0.,0.01,/normal,charsize=1.*diswg.charsize.val, $
        ' '+add_comma(get_dispset(),sep='(, )',/no_empty)
      xyouts,1.,0.01,/normal,alignment=1.,charsize=1.*diswg.charsize.val,rgstr
    endif
      
    
  endfor
  if keyword_set(ps) then psset,/close
  wset,owin & !p.multi=0
end

pro xywg_event,event
  common xywg,xywg
  
  widget_control,event.id,get_uvalue=uval
  case uval of
    'control': begin
      case event.value of
        'done': widget_control,xywg.base.id,/destroy
        'refresh': ;do nothing - xyplot_fill is called anyway
                                ;xyplot_fill
        'print': xyplot_fill,/ps
        'printall': xyplot_fill,/ps,/all
        'comparecomp': xyplot_fill,/ps,/all,/comp
      endcase
    end
    'xcomp': xywg.comp(0).val=event.index
    'ycomp': xywg.comp(1).val=event.index
    'nbins': xywg.nbins.val=event.value
    'psym': xywg.psym.val=event.index
    'rgflag': xywg.rgflag.val=event.select
    'xpar': xywg.par(0).val=event.index
    'ypar': xywg.par(1).val=event.index
    else: begin
      help,/st,event
    end
  endcase
  xyplot_fill
end

pro xyplot_widget,init=init
  common wgst,wgst
  common xywg,xywg
  common param
  common xypar,xypar
  common pikaia
  
  xypar={name:[param.name,'Histogram']}
  compstr='Comp '+n2s(indgen(pikaia_result.input.ncomp)+1)
  
  if n_elements(xywg) eq 0 then begin
    subst={id:0l,val:0.,str:''}
    xywg={base:subst,control:subst,par:replicate(subst,2),draw:subst, $
          comp:replicate(subst,2),nbins:subst,psym:subst,rgflag:subst}
    xywg.par(0).val=where(xypar.name eq 'B')
    xywg.par(1).val=where(xypar.name eq 'Histogram')
    xywg.comp(0).val=0
    xywg.comp(1).val=0
    xywg.rgflag.val=0
    xywg.nbins.val=100
    xywg.psym.val=3
  endif
  if keyword_set(init) then return
  
  screen=get_screen_size()
  xywg.base.id=widget_base(title='XY-Plot Module',/col)  
  
  lab=widget_label(xywg.base.id,value='Plot uses same averaging, binning,' + $
                   ' ... as images in xdisplay-widget')
  par=widget_base(xywg.base.id,/frame,row=1)
  tit=['x','y']
  for i=0,1 do begin
    xywg.par(i).id=widget_droplist(par,uvalue=tit(i)+'par',/dynamic, $
                                   value=xypar.name,title=tit(i)+'-axis:')
    widget_control,xywg.par(i).id,set_droplist_select=xywg.par(i).val
    xywg.comp(i).id=widget_droplist(par,uvalue=tit(i)+'comp',/dynamic, $
                                    value=compstr)
    widget_control,xywg.comp(i).id,set_droplist_select=xywg.comp(i).val
  endfor
  
  xywg.nbins.id=cw_field(par,/all_events,/integer,xsize=4,title='Bins:', $
                             uvalue='nbins',value=fix(xywg.nbins.val))
  psymval=n2s(indgen(7))
  xywg.psym.id=widget_droplist(par,uvalue='psym',/dynamic, $
                               value=psymval,title='psym=')
  widget_control,xywg.psym.id,set_droplist_select=xywg.psym.val
  xywg.psym.id=cw_bgroup(par,uvalue='rgflag',/nonexclusive, $
                         set_value=xywg.rgflag.val,'Auto-Range')
  
  
  xywg.draw.id=widget_draw(xywg.base.id,xs=640*1.2,ys=480*1.2,/frame, $
                           uvalue='xydraw')
  
  xywg.control.id=cw_bgroup(xywg.base.id, $
                            ['Done','Refresh','Print','Print All', $
                            'Compare Components'], $
                            uvalue='control',row=1,button_uvalue= $
                            ['done','refresh','print','printall','comparecomp'])
  
  widget_control,xywg.base.id,/realize
  xmanager,'xywg',xywg.base.id,no_block=1
  
  xyplot_fill
end


pro xyplot_module,init=init
  common wgst,wgst
  common xywg,xywg
  
  if n_elements(xywg) ne 0 then begin
    widget_control,xywg.base.id,iconify=0,bad_id=bad_id
    if bad_id eq 0 then return
  endif
  
  xyplot_widget,init=init
  
end
