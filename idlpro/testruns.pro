pro testruns
  common success,success
  
  success=0b
  helix,ipt='test_fits4d.ipt',x=2020,y=220,/ps
  helix,ipt='test_hinode.ipt',x=10,y=220,/ps
  helix,ipt='test_imax.ipt',x=10,y=200,/ps
  
  
  
  print
  print,strjoin(replicate('-',80))
  print,'All test runs completed successfully'
  print,strjoin(replicate('-',80))
  success=1b
end
