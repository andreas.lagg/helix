;create a ccx file directly from the cc files.
;set WL scale manually.
pro make_ccx_tip,ccfile=ccfile
  
  if n_elements(ccfile) eq 0 then begin
    print,'make_ccx_tip - creates ccx file for TIP data from cc files'
    print,'usage:'
    print,'make_ccx_tip,ccfile=''14jul13.000-0?cc'''
    reset
  endif
  
  xy_fits,ccfile,/new_cont,struct=xyfst
  
  wl_off=0d & read,'Enter WL_OFF = ',wl_off
  wl_disp=0d & read,'Enter WL_DISP = ',wl_disp
  
  ncc=n_elements(xyfst)
  for i=0,ncc-1 do begin
    filex=xyfst[i].file[i]+'x'
    icont=xyfst[i].ic[0:xyfst[i].nx-1,0:xyfst[i].ny-1]
    mkhdr,header,4,[xyfst[i].nx,xyfst[i].ny]
    sxaddpar,header,'WL_NUM',xyfst[i].nbin,'# of WL-bins'
    sxaddpar,header,'WL_OFF',wl_off,'WL-Offset'
    sxaddpar,header,'WL_DISP',wl_disp,'WL-Dispersion'
    writefits,filex,icont,header,append=0
    print,'Wrote '+filex
   endfor

end
