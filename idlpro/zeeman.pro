;calculate zeeman strangth for a given line
function zeeman,line
  
  physical_constants
  all_lines=def_line(line='he') 
  
  for il=0,2 do begin
    
  line=all_lines(il)
  
;  ml=indgen(line.jl*2+1)-line.jl
;  mu=indgen(line.ju*2+1)-line.ju
  
                                ;absorption: l is initial state (lower energy)
                                ;and u =final state (upper)
;   nl=n_elements(ml)
;   sigma_cmp_minus=(ml-1)>(-line.ju)
;   pi_cmp=ml
;   sigma_cmp_plus=(ml+1)<(line.ju)
;   sigma_cmp_plus=sigma_cmp_plus(uniq(sigma_cmp_plus,sort(sigma_cmp_plus)))
;   sigma_cmp_minus=sigma_cmp_minus(uniq(sigma_cmp_minus,sort(sigma_cmp_minus)))
  
  
  
                                ;absorption: l is initial state (lower energy)
                                ;and u =final state (upper)
  J=line.jl
  
  M=indgen(J*2+1)-J
  
  case line.ju-line.jl of
    -1: begin                   ;delta J = -1
      Sbm=((J-M)*(J-M-1))
      Spm=(J^2-M^2)
      Srm=((J+M)*(J+M-1))
    end
    0: begin                   ;delta J = 0
      Sbm=((J-M)*(J+M+1))
      Spm=(M^2)
      Srm=((J+M)*(J-M+1))
    end
    1: begin                   ;delta J = 1
      Sbm=((J+M+1)*(J+M+2))
      Spm=((J+1)^2-M^2)
      Srm=((J-M+1)*(J-M+2))
    end
    else: stop
  endcase
  
  jl=1              &  sl=1  &  ll=0
  ju=([0,1,2])(il)  &  su=1  &  lu=1

  ;factor for wl-shift in Angstroem for B in Gauss
  wlfact=!e_charge*(line.wl*1e-10)^2/(2*!me*!c_light)*1e10/1e4
  
; gl=1.5 + (line.sl*(line.sl+1)-line.ll*(line.ll+1))/(2*line.jl*(line.jl+1))
; gu=1.5 + (line.su*(line.su+1)-line.lu*(line.lu+1))/(2*line.ju*(line.ju+1))
 gl=1.5 + (sl*(sl+1)-ll*(ll+1))/(2*jl*(jl+1))
 gu=1.5 + (su*(su+1)-lu*(lu+1))/(2*ju*(ju+1))
 if finite(gl) eq 0 then gl=0
 if finite(gu) eq 0 then gu=0
 
  gm_pi=m*(gl - gu)
  gm_sb=(m*(gl - gu) - gu)
  gm_sr=(m*(gl - gu) + gu)
  
  geff=0.5*(gu-gl) + 0.25*(gu-gl)*(ju*(ju+1) - jl*(jl+1))
  print
  print,string(line.id)
  
  print,'pi: ',spm*2
  print,'sb: ',sbm/2
  print,'sr: ',srm/2
  print,total(srm/2),total(spm*2),total(sbm/2)
  
  print,'Lande-factors:'
  
  print,'pi: ',gm_pi
  print,'sb: ',gm_sb
  print,'sr: ',gm_sr
  print,'gu, gl, geff: ',gu,gl,geff
  endfor
    
end
