;compares profile computation between IDL and Fortran version.
;also checks fitness computation.
pro comp_f90idl
  
  fipt=['comp_hanle.ipt','comp_zeeman.ipt']
  
  fidl=['FORTRAN','IDL']
  for i=0,n_elements(fipt)-1 do begin
    for j=0,1 do begin
      ipt=read_ipt(fipt(i))
      
      ipt.display_profile=1
      ipt.verbose=0
      
                                ;first create profile for  fitness comparison
      if j eq 0 then begin
        ipt.noise=1e-4
        ipt.synth=1
        ipt.save_fitprof=1
        helix,struct_ipt=ipt,x=0,y=0
      endif
      
      obs=strmid(fipt(i),0,strpos(fipt(i),'.ipt'))+'.x0000.y0000.obs.sav'
      ipt.dir.profile='./sav/'
      ipt.observation=obs
      ipt.noise=0
      ipt.synth=0
      ipt.code=(fidl)(j)
      ipt.save_fitprof=0
      helix,struct_ipt=ipt,fitprof=fitprof,result=tres,x=0,y=0

      if j eq 0 then begin
        fit=fitprof
        result=tres
      endif else begin
        fit=[fit,fitprof]
        result=[result,tres]
      endelse
    endfor
    
    plot_profiles,fit(0),fit(1),win_nr=9, $
      title=fipt(i)+': FORTRAN: red, IDL: blue',color=[1,3],charsize=1.3
    
    diff=fit(0)
    diff.i=fit(0).i-fit(1).i
    diff.q=fit(0).q-fit(1).q
    diff.u=fit(0).u-fit(1).u
    diff.v=fit(0).v-fit(1).v
    
    plot_profiles,diff,win_nr=8,title=fipt(i)+': difference Fortran-IDL', $
      range=[[minmax(diff.i)],[minmax(diff.q)], $
             [minmax(diff.u)],[minmax(diff.v)]],charsize=1.3
    
    print,'FITNESS: '
    for j=0,1 do print,fidl(j)+': ',result(j).fit.fitness
    stop
     
    
  endfor
  
end
