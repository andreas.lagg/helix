;create movies of maps from pikaia-sav files
pro movie_maps
  common pikaia
  common wgst
  common param
  common diswg
  common psset,psset
 
  psav=dialog_pickfile(filter='*.pikaia.sav',path='sav/',dialog_parent=wgst.base.id,/multiple_files,title='Select several pikaia-sav files')
  
  if psav(0) eq '' then return
  
;  if 1 eq 0 then begin
  ns=n_elements(psav)
  print,'Creating movie of xdisplay-map for '+n2s(ns)+' sav-files.'
  ioval=diswg.inputout.val
  diswg.inputout.val=0
  for i=0,ns-1 do begin
    xdisplay,psav(i)
    ishow=where(param.show)
    display_data,/no_sav,/widget,parshow=ishow,/ps,/no_x,/encaps
    if i eq 0 then eps=psset.file else eps=[eps,psset.file]
  endfor
  diswg.inputout.val=ioval
;  endif else eps=file_search('./ps/imax_163_3*.pikaia.sav.eps')
  
                                ;use imagemagick convert to creat jpg
                                ;files from the eps files
                                ;create tmp directory for jpg files
  path=strmid(eps(0),0,strpos(eps(0),'/',/reverse_search)+1)
  mtmp=path+'movietmp'
  spawn,'mkdir -p '+mtmp+' ; rm -rf '+mtmp+'/*'
  jpg=eps
  for i=0,n_elements(eps)-1 do begin
    jpg(i)=mtmp+'/'+string(i,format='(i5.5)')+'.jpg'
    tojpg='convert -quality 99 +antialias -density 150 '+eps(i)+' '+jpg(i)
    print,'Convert to jpg: '+tojpg
    spawn,tojpg,res
  endfor
  
                                ;use mencoder to create movie from jpg files
  szx=800.                      ;xsize of movie
  szy=szx/psset.size(0)*psset.size(1)
  scl=n2s(fix(szx))+':'+n2s(fix(szy))
                                ;filename of movie
  imax=0
  for i=1,strlen(eps(0)) do $
    if min(strmid(eps,0,i) eq strmid(eps(0),0,i)) then imax=i
  if imax eq 0 then movie='movie.wmv' else begin
    movie=strmid(eps(0),0,imax)
    if strmid(movie,strlen(movie)-1,1) ne '.' then movie=movie+'.'
    pnm=add_comma(param.name(ishow),sep='_')
    bpnm=byte(pnm)
    pnm=string(bpnm(where(bpnm ne (byte('/'))(0))))
    movie=movie+pnm+'.wmv'
  endelse  
  moviecommand='mencoder mf://'+mtmp+'/*.jpg -mf fps=6:type=jpg -ovc ' + $
    'lavc -lavcopts vcodec=wmv2:vbitrate=5000 -vf scale='+scl+ $
    ' -oac copy -o '+movie
  print,'Executing Movie command:'
  print,moviecommand
  spawn,moviecommand
  print,'Movie created: ',movie
end
