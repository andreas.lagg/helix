pro fill_header
  
  savin='/data/slam/VTT-data/atm_sav/27aug03.004_v02.pikaia.sav'
  savout='./sav/test.sav'
  obs='/data/slam/VTT-data/data_aug03/27aug03/27aug03.004'
  
  
  restore,/v,savin
  header=read_tip_header(obs,verbose=1)
  sz=size(pikaia_result.fit)
  prnew=result_struc(nx=sz(1),ny=sz(2),ipt=pikaia_result.input,header=header)
  
  struct_assign,pikaia_result,prnew,/nozero
  pikaia_result=temporary(prnew)
  pikaia_result.header=header
  
                                ;do changes, eg: old_norm keyword:
  pikaia_result.input.old_norm=1
  
  save,pikaia_result,/compress,/xdr,file=savout
  print,'Wrote new sav-file: '+savout
end
