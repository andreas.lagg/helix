function get_times,rawdat,tsec=tsec,error=error,verbose=verbose
  
  if n_elements(verbose) eq 0 then verbose=1
  
  if verbose ge 1 then print,'get observing times from '+rawdat
  openr,unit,rawdat,/get_lun,error=err
  if err ne 0 then begin
    message,/cont,'Cannot read file: '+rawdat
    error=1
    time=strarr(999)+'no time info'
    tsec=dblarr(999)
    return,time
  endif
  
  line1=''
  readf,unit,line1
  free_lun,unit
  
  datpos=strpos(line1,'DATE-OBS=')
  date=strmid(line1,datpos+11,10)
  year=strmid(date,0,4)
  month=strmid(date,5,2)
  day=strmid(date,8,2)
  
                                ;search first line for UT occurence
  time=''
  repeat begin
    utpos=strpos(line1,'UT      =')
    if utpos ne -1 then begin
      time=[time,strmid(line1,utpos+11,8)]
      line1=strmid(line1,utpos+19,strlen(line1))
    endif
  endrep until utpos eq -1
  if n_elements(time) gt 1 then time=time(1:*)
  
  date_dot=day+'.'+month+'.'+year+';'+time
  tsec=double(conv_time(old='date_dot',new='sec',date_dot))
  
  return,time
end
