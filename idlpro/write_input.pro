function add_spc,str,len
  
  if strlen(str) lt len then retstr=str+strjoin(strarr(len-strlen(str))+' ') $
  else retstr=str
  return,retstr
end

function split_cmt,cmt,len
  
  pos=79
  pre=strjoin(strarr(26)+' ')+';  '
  retcmt=''
  spc=[strsplit(cmt,' '),strlen(cmt)]
  nw=0
  il=0
  repeat begin
    tw=strmid(cmt,spc(nw),spc(nw+1)-spc(nw))
    if strlen(retcmt(il))+strlen(tw)+len*(il eq 0) gt pos then begin
      retcmt=[retcmt,pre+tw]
      il=il+1
;print,retcmt & stop      
    endif else retcmt(il)=retcmt(il)+tw
    nw=nw+1
  endrep until nw ge n_elements(spc)-1
  return,retcmt
end

pro write_input,file,ipt=iptin,error=error,verbose=vrb
  common iptgroup,iptgroup
  common verbose,verbose
  
  ipt=iptin
  if n_elements(verbose) eq 0 then verb=1 else verb=0
  if n_elements(vrb) ne 0 then verb=keyword_set(vrb)
  
  ulidx=0
  if n_elements(iptgroup) eq 0 then def_iptgroup
  name=def_parname(tag_names(ipt.atm.par))
  blendok=max(tag_names(ipt) eq 'BLEND') eq 1
  genok=max(tag_names(ipt) eq 'GEN') eq 1
  linparok=max(tag_names(ipt.line.par) eq 'STRENGTH') eq 1
  
  maxlen_name=max(strlen(iptgroup.element))<12
  maxlen_par=max(strlen(iptgroup.default))<20
  
  prascii=(ipt.ascii)
  nl=n_elements(prascii)
  done=strmid(strtrim(prascii,2),0,1) eq ';'
  sep=';'+strjoin(replicate('-',70))
  iline=''
  asc=strtrim(strupcase(prascii),2)
  fw=strarr(nl)
  for il=0,nl-1 do begin
    fw(il)=(strsplit(/extract,asc(il)))(0)
  endfor
  
  use_at=0
  tni=tag_names(ipt)
  if ipt.localstray_rad ge 1e-5 then ipt.ncomp=(ipt.ncomp-1)>1
  for ig=1,n_elements(iptgroup)-1 do begin
    tit=sep & strput,tit,' '+iptgroup(ig).name+' ',10
    iline=[iline,tit]
    atmmode=ig eq 5
    lineparmode=ig eq 6 and linparok eq 1
    blendmode=ig eq 7 and blendok eq 1
    genmode=ig eq 8 and genok eq 1    
    ncmp=(ipt.ncomp*atmmode-1)
    if lineparmode eq 0 and blendmode eq 0 then $
      for ia=0,ncmp>0 do begin
      for ie=0,n_elements(iptgroup(ig).element)-1 do begin
        if max(['','LINES'] eq iptgroup(ig).element(ie)) ne 1 then  begin
                                ;treat atmosphere parameters differently
;          pos=where(strpos(asc,iptgroup(ig).element(ie)) eq 0)
          pos=where(fw eq iptgroup(ig).element(ie))
          

          defdone=0
          mln=maxlen_name
          show=iptgroup(ig).show(ie)
          if atmmode and iptgroup(ig).element(ie) ne 'NCOMP' then begin
            if ie eq 1 then begin
              iline=[iline,'; --- atmospheric component '+n2s(ia+1)+' ---', $
                     '; NAME      Value    SCL_MIN    SCL_MAX FIT %RG FIT']
            endif
            tnr=(where(name.ipt eq iptgroup(ig).element(ie)))(0)
            if iptgroup(ig).element(ie) ne 'USE_LINE' and $
              iptgroup(ig).element(ie) ne 'USE_ATOM'  and $
              iptgroup(ig).element(ie) ne 'ATM_INPUT' then begin
              mln=5
              if tnr(0) eq -1 then $
                message,'Undefined element '+iptgroup(ig).element(ie)
              default=string([ipt.atm(ia).par.(tnr),ipt.scale(ia).(tnr).min, $
                              ipt.scale(ia).(tnr).max,ipt.atm(ia).fit.(tnr)], $
                             format='(f11.3,2(f11.3),i4)')
              for im=0,ipt.nmi-2-(ipt.straypol_corr gt 0) do begin
                default=default + ' ' + string([ipt.mi(ia).(tnr).perc_rg(im), $
                                                ipt.mi(ia).(tnr).fit(im)], $
                                               format='(i4,i4)')
              endfor
              defdone=1
              if max(ipt.parset eq name.idl(tnr)) eq 0 then show=0
            endif
          endif
          
          if genmode then begin
            show=1
            genname=def_genname(tag_names(ipt.gen.par))
            if ie eq 0 then begin
              iline=[iline,'; NAME            Value   SCL_MIN   SCL_MAX FIT']
            endif
            tnr=(where(genname.ipt eq iptgroup(ig).element(ie)))(0)
            if tnr(0) eq -1 then $
              message,'Undefined element '+iptgroup(ig).element(ie)
            default=string([ipt.gen.par.(tnr), $
                            ipt.gen.scale.(tnr).min, $
                            ipt.gen.scale.(tnr).max, $
                            ipt.gen.fit.(tnr)], $
                           format='(3(f10.2),i4)')
            defdone=1
          endif
          
          if ia ge n_elements(pos) or pos(0) eq -1 then begin
            if defdone eq 0 then default=iptgroup(ig).default(ie) 
            if (default eq '' and $
                iptgroup(ig).element(ie) ne 'CONV_FUNC' and $
                iptgroup(ig).element(ie) ne 'PROFILE_LIST') then show=0
          endif else begin
            done(pos(ia))=1
            if defdone eq 0 then begin
              ii=(where(tni eq iptgroup(ig).element(ie)))(0)
              
              default=''
              case iptgroup(ig).element(ie) of
                'STRAYPOL_CORR': begin
                  default=string(ipt.(ii),format='(i5)')+' B'
                  ii=-1
                end
                'PS': begin
                  default=ipt.dir.ps
                  ii=-1
                end
                'PROFILE_ARCHIVE': default=ipt.dir.profile
                'SAV': default=ipt.dir.sav
                'WGT': default=ipt.dir.wgt
                'ATOM': default=ipt.dir.atom
                'SMOOTH': begin
                  default=n2s(ipt.smooth) +' '+n2s(ipt.filter_mode)
                end
                'OUTPUT': default=(['X','PS'])(ipt.ps)
                'XPOS': ii=where(tni eq 'X')
                'YPOS': ii=where(tni eq 'Y')
                'HIN_SCANNR': default= $
                  string(ipt.obs_par.hin_scannr,format='(i5)')
                'SLIT_ORIENTATION': default= $
                  string(ipt.obs_par.slit_orientation,format='(f7.2)')
                'M1ANGLE': default= $
                  string(ipt.obs_par.m1angle,format='(f7.2)')
                'SOLAR_POS': default= $
                  string([ipt.obs_par.posx,ipt.obs_par.posy], $
                         format='(f8.2)')
                'SOLAR_RADIUS': $
                  default= string(ipt.obs_par.solar_radius,format='(f8.2)')
                'HELIO_ANGLE': $
                  default=string(ipt.obs_par.heliocentric_angle,format='(f8.2)')
                '+Q2LIMB': $
                  default=string(ipt.obs_par.posq2limb,format='(f8.2)')
                'USE_ATOM': begin
                  default=remove_multi(string(ipt.atom_file(ia,*)),/no_empty)
                  use_at=1
                end
                'USE_LINE': begin                    
                  default=remove_multi(string(ipt.atm(ia).use_line(0:1,*)), $
                                       /no_empty)
                  if use_at eq 1 then show=0
                end
                'ATM_INIT': begin
                  default=remove_multi(string(ipt.atm(ia).atm_init),/no_empty)
                end
                'ATM_ARCHIVE': default=ipt.dir.atm
                'ATM_SUFFIX': default=ipt.dir.atm_suffix
                else: if ii eq -1 then $
                  message,'unknown keyword - tag name '+ $
                  iptgroup(ig).element(ie)
              endcase
              
              ii=ii(0)
              if ii ne -1 then begin
                typ=size(ipt.(ii),/type)
;                nli=n_elements(ipt.(ii))
                case typ of 
                  1: fmt='i5'
                  2: fmt='i5'
                  3: fmt='i5'
                  5: fmt='f16.6'
                  7: fmt='a'
                  else: fmt='f15.5'
                endcase
                
                if iptgroup(ig).nmipar(ie) eq 0 then begin
                  case iptgroup(ig).element(ie) of
                    'SMOOTH': begin
                      default= $
                        string([ipt.smooth,ipt.filter_mode],format='(2a)')
                    end
                    'LOCALSTRAY_RAD': $
                      default=string([ipt.localstray_rad],format='(f6.1)')
                    'LOCALSTRAY_FWHM': $
                      default=string([ipt.localstray_fwhm],format='(f6.1)')
                    'LOCALSTRAY_CORE': $
                      default=string([ipt.localstray_core],format='(f6.1)')
                    'LOCALSTRAY_POL': $
                      default=string([ipt.localstray_pol],format='(i2)')
                    'NORM_CONT': begin
                      default=(['LOCAL','SLIT','IMAGE'])(ipt.norm_cont)
                    end
                    'CONV_MODE': begin
                      default=(['FFT','MUL'])(ipt.conv_mode)
                    end
                    'CHI2MODE': begin
                      if ipt.chi2mode eq 1 then default='JM' else default='0'
                    end
                    else: default=string(ipt.(ii),format='('+fmt+')')
                  endcase
                endif else begin
                  default=''
                  for im=0,ipt.nmi-1-(ipt.straypol_corr gt 0) do begin
                    im0= im*iptgroup(ig).nmipar(ie)
                    im1=im0+iptgroup(ig).nmipar(ie)-1
                    case iptgroup(ig).element(ie) of
                      'METHOD': begin
                        tdef=(['PIKAIA','POWELL', $
                               'LMDIFF','PIK_LM'])(ipt.method(im))
                        if ipt.method(0) eq 3 then tdef=tdef+'  '+ $
                          n2s(ipt.piklm_bx)+'  '+n2s(ipt.piklm_by)+'  '+ $
                          n2s(ipt.piklm_minfit,format='(f15.2)')
                      end
                      'PB_METHOD': tdef=(['poly','table'])(ipt.pb_method(im))
                      else: tdef=string((ipt.(ii))(im0:im1), $
                                        format='('+fmt+')')
                    endcase
                    default=[default,tdef]
                  endfor
                  default=default(1:*)
                  
                endelse
                default=strtrim(strcompress(default),2)
                for i1=0,n_elements(default)-1 do begin
                  kommapos=strpos(default(i1),'.')
                  if kommapos(0) ne -1 then repeat begin
                    zero=strmid(default(i1),strlen(default(i1)),1) eq '0'
                    if zero then $
                      default(i1)=strmid(default(i1),0,strlen(default(i1)))
                  endrep until zero eq 0
                endfor
              endif
              default=add_comma(default,sep=' ')
            endif
          endelse

          comment=';'+iptgroup(ig).comment(ie)
          element=add_spc(iptgroup(ig).element(ie),mln)          
          default=add_spc(default,maxlen_name)          
          
          eldef=element+' '+default+' '
          comment=split_cmt(comment,strlen(eldef))
          if show then $
            if iptgroup(ig).element(ie) ne 'NCOMP' or ia eq 0 then begin
            if ia gt 0 then comment=''
            iline=[iline,eldef+comment(0)]
            if n_elements(comment) gt 1 then iline=[iline,comment(1:*)]
          endif
        endif
      endfor
    endfor
    
    if lineparmode eq 1 then begin
      linename=def_linename(tag_names(ipt.line.par))
      for il=0,ipt.nline-1 do begin
        for ie=0,n_elements(iptgroup(ig).element)-1 do $
          if iptgroup(ig).element(ie) ne '' then begin
          pos=where(fw eq iptgroup(ig).element(ie))
          if ie eq 0 then begin
            iline=[iline, $
                   '; --- fit parameters for line '+n2s(il+1)+' ---', $
                   '; NAME            Value   SCL_MIN   SCL_MAX  FIT']
            iline=[iline, $
                   'LINE_ID '+n2s(ipt.line(il).wl,format='(f15.4)')+ $
                   '  ;'+iptgroup(ig).comment(ie)]
          endif else if iptgroup(ig).element(ie) ne 'LINE_ID' then begin
            tnr=(where(linename.ipt eq iptgroup(ig).element(ie)))(0)
            if tnr(0) eq -1 then $
              message,'Undefined element '+iptgroup(ig).element(ie)
            
            default=string([ipt.line(il).par.(tnr), $
                            ipt.line(il).scale.(tnr).min, $
                            ipt.line(il).scale.(tnr).max, $
                            ipt.line(il).fit.(tnr)], $
                           format='(3(f10.2),i4)')
            comment=';'+iptgroup(ig).comment(ie)
            element=add_spc(iptgroup(ig).element(ie),mln)          
            default=add_spc(default,maxlen_name)
            
            eldef=element+' '+default+' '
            comment=split_cmt(comment,strlen(eldef))
            iline=[iline,eldef+comment(0)]
            if n_elements(comment) gt 1 then iline=[iline,comment(1:*)]
          endif
          if pos(0) ne -1 then done(pos)=1
        endif
      endfor
    endif
    
    if blendmode  then begin
      blendname=def_blendname(tag_names(ipt.blend.par))
      for ib=0,ipt.nblend-1 do begin
        if ib eq 0 then $
          iline=[iline, $
                 'NBLEND       '+n2s(ipt.nblend)+' ;'+iptgroup(ig).comment(ib)]
        for ie=0,n_elements(iptgroup(ig).element)-1 do $
          if iptgroup(ig).element(ie) ne '' then begin
          pos=where(fw eq iptgroup(ig).element(ie))
          if iptgroup(ig).element(ie) ne 'NBLEND' then begin
            if ie eq 1 then begin
              iline=[iline,'; --- telluric blend '+n2s(ib+1)+' ---', $
                     '; NAME            Value   SCL_MIN   SCL_MAX FIT']
            endif
            tnr=(where(blendname.ipt eq iptgroup(ig).element(ie)))(0)
            if tnr(0) eq -1 then $
              message,'Undefined element '+iptgroup(ig).element(ie)
            default=string([ipt.blend(ib).par.(tnr), $
                            ipt.blend(ib).scale.(tnr).min, $
                            ipt.blend(ib).scale.(tnr).max, $
                            ipt.blend(ib).fit.(tnr)], $
                           format='(3(f11.3),i4)')
            comment=';'+iptgroup(ig).comment(ie)
            element=add_spc(iptgroup(ig).element(ie),mln)          
            default=add_spc(default,maxlen_name)

            eldef=element+' '+default+' '
            comment=split_cmt(comment,strlen(eldef))
            iline=[iline,eldef+comment(0)]
            if n_elements(comment) gt 1 then iline=[iline,comment(1:*)]
          endif
          if pos(0) ne -1 then done(pos)=1
        endif
      endfor
    endif
  endfor 

  openw,unit,/get_lun,file,error=error
  if error eq 0 then begin
    tit=sep & strput,tit,' END OF INPUT FILE ',10
    iline= [iline(1:*),tit]
    tit=sep & strput,tit,' '+iptgroup(0).name+' ',10
    cmtidx=where(done eq 0)
    if cmtidx(0) ne -1 then $
      iline=[strtrim(prascii(cmtidx),2),iline]
    iline=[tit,iline]
    for i=0,n_elements(iline)-1 do if iline(i) ne '' then printf,unit,iline(i)
    free_lun,unit
    
    if verb ge 1 then print,'Wrote nice input-file: '+file
  endif else begin
    message,/cont,'Error in writing input file: '+file
  endelse
end
