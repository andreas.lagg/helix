function get_pikaia_idx,inatm,nadd=nadd,silent=silent
  common verbose,verbose
  
  silent=keyword_set(silent)
  atm=inatm
  nt=n_tags(atm.par)
  tn=tag_names(atm.par)
  na=n_elements(atm)
  if n_elements(verbose) eq 0 then verbose=1
  
  if n_elements(nadd) eq 0 then nadd=0
  
  isatm=max(tag_names(atm) eq 'RATIO')
  
  ip=1
  cidx=0 & pidx=0 & ffdone=0
  if isatm then for it=0,nt-1 do if tn(it) ne 'DUMMY' then atm.ratio.(it)=1.
  for ia=0,na-1 do for ift=0,(na<n_elements(atm(ia).fit))-1 do $
    for it=0,nt-1 do if tn(it) ne 'DUMMY' then begin
                                ;set idx to 0 if parameter is not fitted
    if atm(ia).fit(ift).(it) eq 0 then atm(ia).idx(ift).(it)=0 else begin
      new=0
      if atm(ia).fit(ift).(it) gt 0 then new=1 $ ;normal parameter to be fitted
      else begin                ;treat coupled parameters: 
        ci=(where(cidx eq abs(atm(ia).fit(ift).(it))))(0)
        
        if ci(0) eq -1 then begin
          new=1 
        endif else begin
          atm(ia).idx(ift).(it)=pidx(ci)+nadd(0)
          if isatm then if ia ge 1 then begin
            iii=(where(atm(0:ia-1).fit(0).(it) eq atm(ia).fit(ift).(it)))(0)
            if iii(0) ne -1 then begin
              if (abs(atm(ia).par.(it)) gt 1e-4 or $
                  atm(iii).par.(it) eq atm(ia).par.(it)) then begin
                if atm(iii).par.(it) eq atm(ia).par.(it) then $
                  atm(ia).ratio.(it)=1. $
                else $
                  atm(ia).ratio.(it)=atm(iii).par.(it)/atm(ia).par.(it) 
                if verbose ge 1 and silent eq 0 then $
                  message,/cont,tn(it)+', Comp. '+n2s(ia)+ $
                  ' coupled to Comp '+n2s(iii(0))+'. Ratio: '+ $
                  n2s(atm(ia).ratio.(it))
              endif else begin
                message,/cont,'Comp '+n2s(ia)+', '+tn(it)+ $
                  ': Cannot couple parameters with component '+ $
                  n2s(iii(0))+' (ratio is infinity).'
                reset
              endelse              
            endif
          endif
        endelse
        cidx=[cidx,abs(atm(ia).fit(ift).(it))]
        pidx=[pidx,ip]
      endelse
                                ;special case: do not fit ff of last
                                ;component
      if 1 eq 0 then $
        if tn(it) eq 'FF' then if ia eq na-1 then begin
        new=0
        atm(ia).fit(ift).ff=0
      endif
      
      if new eq 1 then begin
        atm(ia).idx(ift).(it)=ip+nadd(0)
        ip=ip+1
      endif
    endelse
  endif
  atm.npar=ip-1
  
  return,atm
end
